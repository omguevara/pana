DROP TABLE IF EXISTS a; CREATE TABLE a AS
SELECT 'YV3458' as matricula, 'C-441' as modelo, 4.47 as peso, '2023-02-16'::date as f1 , '2023-05-16'::date as f2, 'TIPO: CONVENIO, '||chr(13)||' EMPRESA: AEROMIRABEL,'||chr(13)||' EXONERAR: DOSA Y TASA' as observacion UNION ALL
SELECT 'YV2653' as matricula, 'TC90' as modelo, 5 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: CONVENIO, '||chr(13)||' EMPRESA: ALAS DE ORIENTE,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3288' as matricula, 'LJ-25' as modelo, 6.8 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: CONVENIO, '||chr(13)||' EMPRESA: OK AVIATION, C.A.,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3509' as matricula, 'LJ55' as modelo, 8 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: CONVENIO, '||chr(13)||' EMPRESA: TRANS LUBRICADOS TRANS LUBE, C.A,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3532' as matricula, 'LJ-35' as modelo, 4.59 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: CONVENIO, '||chr(13)||' EMPRESA: TRANS LUBRICADOS TRANS LUBE, C.A,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3469' as matricula, 'AIRBUS A318' as modelo, 68 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: DGCIM,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3315' as matricula, 'C560' as modelo, 16.2 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: INAC,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV2079' as matricula, 'B-350' as modelo, 7.48 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: LAPA,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV2671' as matricula, 'C-550' as modelo, 7 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: MPPRIJP,'||chr(13)||' EXONERAR: DOSA Y TASA' as observacion UNION ALL
SELECT 'YV3062' as matricula, 'AC-690' as modelo, 7.5 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: MPPRIJP,'||chr(13)||' EXONERAR: DOSA Y TASA' as observacion UNION ALL
SELECT 'YV2716' as matricula, 'LI-45' as modelo, 9.75 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: PDVSA,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV2763' as matricula, 'BE-19000' as modelo, 6 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: PDVSA,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3380' as matricula, 'BE-40' as modelo, 7.3 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: SATA,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV3381' as matricula, 'BE-40' as modelo, 7.3 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: SATA,'||chr(13)||' EXONERAR: DOSA' as observacion UNION ALL
SELECT 'YV1054' as matricula, 'BE-90' as modelo, 5.08 as peso, '2023-04-01'::date as f1 , '2023-06-30'::date as f2, 'TIPO: OFICIAL, '||chr(13)||' EMPRESA: SISTEMA DEFENSIVO TERRITORIAL,'||chr(13)||' EXONERAR: DOSA' as observacion ;

ALTER TABLE a ADD PRIMARY KEY(matricula);


INSERT INTO maest_aeronaves (matricula, nombre, peso_maximo, fecha_registro, user_id, ip)
SELECT a.matricula, a.modelo, a.peso*1000, now(), 1, '127.0.0.1'
FROM a
LEFT JOIN maest_aeronaves AS B ON B.matricula = a.matricula
WHERE B.matricula IS NULL
GROUP BY 1,2,3;

ALTER TABLE maest_membresias ADD COLUMN observacion TEXT;

INSERT INTO maest_membresias(aeronave_id, fecha_desde, fecha_hasta, ip, fecha_registro, user_id, observacion)
SELECT 
    B.id, a.f1, a.f2, '127.0.0.1', now(), 1, observacion
FROM a 
LEFT JOIN maest_aeronaves AS B ON B.matricula = a.matricula;

DROP TABLE a;
INSERT INTO seg_processes(name_process, description, icon, route, actions, requiere_fiscal,"order",menu_id) VALUES('Consulta de Aeronaves', '', 'fa-plana', 'search_plane', '*', false, 1, 5);



