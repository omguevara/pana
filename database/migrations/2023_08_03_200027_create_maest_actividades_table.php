<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    /**
     * Run the migrations.
     */
    public function up(): void {
        Schema::create('maest_actividades', function (Blueprint $table) {
            $table->id();
            $table->string('actividad', 100);
            $table->integer('orden');
        });
        DB::table('maest_actividades')->insert([
            ['actividad' => 'AGENCIA DE VIAJES', 'orden' => 1],
            ['actividad' => 'ALMACENADORAS', 'orden' => 1],
            ['actividad' => 'ALQUILER DE VEHÍCULO', 'orden' => 1],
            ['actividad' => 'ARRENDADORA DE VEHICULOS', 'orden' => 1],
            ['actividad' => 'CARGA', 'orden' => 1],
            ['actividad' => 'EQUIPOS DE TELECOMUNICACIONES Y TECNOLOGÍA', 'orden' => 1],
            ['actividad' => 'ESTACIONAMIENTO', 'orden' => 1],
            ['actividad' => 'FBO', 'orden' => 1],
            ['actividad' => 'LÍNEA AÉREA', 'orden' => 1],
            ['actividad' => 'OMAC', 'orden' => 1],
            ['actividad' => 'PLASTIFICADORAS DE EQUIPAJES', 'orden' => 1],
            ['actividad' => 'RESTAURANTE COMIDA FORMAL', 'orden' => 1],
            ['actividad' => 'RESTAURANTE COMIDA RÁPIDA', 'orden' => 1],
            ['actividad' => 'SALONES DE BELLEZA Y MASAJES', 'orden' => 1],
            ['actividad' => 'SERVICIO DE TRANSPORTE', 'orden' => 1],
            ['actividad' => 'SERVICIOS ESPECIALIZADOS AEROPORTUARIOS Y AÉREOS', 'orden' => 1],
            ['actividad' => 'SERVICIOS ESPECIALIZADOS VARIOS', 'orden' => 1],
            ['actividad' => 'SERVICIOS FINANCIEROS', 'orden' => 1],
            ['actividad' => 'SUMINISTRO DE COMBUSTIBLE AVIÓN', 'orden' => 1],
            ['actividad' => 'SUMINISTRO DE COMBUSTIBLE VEHÍCULO', 'orden' => 1],
            ['actividad' => 'TIENDA CON EXPENDIO DE LICORES', 'orden' => 1],
            ['actividad' => 'TIENDAS Y REVISTAS', 'orden' => 1],
            ['actividad' => 'OTRO', 'orden' => 2],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void {
        Schema::dropIfExists('maest_actividades');
    }
};
