<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcInfoCerfiticacionesLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_info_cerfiticaciones_log', function (Blueprint $table) {
            $table->id();
            $table->boolean('fase_all')->default(false)->nullable();
            $table->boolean('end_certificado')->default(false)->nullable();
            $table->boolean('f1')->default(false)->nullable();
            $table->string('ff1')->nullable();
            $table->boolean('f2')->default(false)->nullable();
            $table->string('ff2')->nullable();
            $table->boolean('f3')->default(false)->nullable();
            $table->string('ff3')->nullable();
            $table->boolean('f4')->default(false)->nullable();
            $table->string('ff4')->nullable();
            $table->boolean('f5')->default(false)->nullable();
            $table->string('ff5')->nullable();
            $table->boolean('c_sms')->default(false)->nullable();
            $table->string('f_c_sms')->nullable();
            $table->boolean('c_avsev')->default(false)->nullable();
            $table->string('f_c_avsev')->nullable();
            $table->boolean('c_mp')->default(false)->nullable();
            $table->string('f_c_mp')->nullable();

            $table->boolean('c_tpd')->default(false)->nullable();
            $table->string('f_c_tpd')->nullable();

            $table->boolean('p_pm')->default(false)->nullable();
            $table->string('f_p_pm')->nullable();
            $table->boolean('p_pc')->default(false)->nullable();
            $table->string('f_p_pc')->nullable();
            $table->boolean('p_pe')->default(false)->nullable();
            $table->string('f_p_pe')->nullable();
            $table->boolean('p_tai')->default(false)->nullable();
            $table->string('f_p_tai')->nullable();
            $table->boolean('p_cpaf')->default(false)->nullable();
            $table->string('f_p_cpaf')->nullable();
            $table->boolean('p_ca')->default(false)->nullable();
            $table->string('f_p_ca')->nullable();
            $table->boolean('p_gma')->default(false)->nullable();
            $table->string('f_p_gma')->nullable();
            $table->boolean('p_cc')->default(false)->nullable();
            $table->string('f_p_cc')->nullable();
            $table->boolean('p_lf')->default(false)->nullable();
            $table->string('f_p_lf')->nullable();
            $table->boolean('p_sl')->default(false)->nullable();
            $table->string('f_p_sl')->nullable();
            $table->boolean('e_ef')->default(false)->nullable();
            $table->string('f_e_ef')->nullable();
            $table->boolean('e_e')->default(false)->nullable();
            $table->string('f_e_e')->nullable();
            $table->string('ip', 20);
            $table->bigInteger('user_id');
            $table->bigInteger('aeropuerto_id');
            $table->timestamp('register_date')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_info_cerfiticaciones_log');
    }
}
