<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVuelosDetalleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proc_vuelos_detalle', function (Blueprint $table) {
            $table->id();

            $table->boolean('aplica_categoria');
            $table->boolean('aplica_estacion');
            $table->boolean('aplica_peso');
            $table->string('aplicable', 50);
            $table->decimal('bs', 10, 2);
            $table->decimal('cant', 10, 4);
            $table->decimal('categoria_aplicada', 10, 2);
            $table->string('codigo', 50);
            $table->unsignedBigInteger('prodservicio_id');
            $table->boolean('default_carga');
            $table->boolean('default_dosa');
            $table->boolean('default_tasa');
            $table->string('descripcion', 200);

            $table->string('formula', 200);
            $table->string('formula2', 200);
            $table->string('full_descripcion', 200);

            $table->decimal('iva', 10, 2);
            $table->string('iva2', 10);
            $table->decimal('iva_aplicado', 10, 2);
            $table->string('moneda', 100);
            $table->unsignedBigInteger('nacionalidad_id')->nullable();
            $table->string('nomenclatura', 200);
            $table->integer('peso_inicio');
            $table->integer('peso_fin');
            $table->decimal('precio', 10, 2);
            $table->boolean('prodservicio_extra');
            $table->decimal('tasa', 10, 2);
            $table->integer('tipo_vuelo_id');
            $table->integer('turno_id')->nullable();

            $table->decimal('valor_dollar', 10, 2);
            $table->decimal('valor_euro', 10, 2);
            $table->decimal('valor_petro', 10, 2);

            $table->unsignedBigInteger('vuelo_id');
            $table->integer('orden');

            $table->foreign('prodservicio_id')->references('id')->on('maest_prod_servicios');
            $table->foreign('vuelo_id')->references('id')->on('proc_vuelos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proc_vuelos_detalle');
    }

}
