<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestPuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_puestos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->unsignedBigInteger('lugar_id');
            $table->unsignedBigInteger('aeronave_id')->nullable();
            $table->boolean('activo')->default(true);
            
            $table->foreign('lugar_id')->references('id')->on('maest_lugares');
        });
        
        
        DB::table('maest_puestos')->insert([
            /*********** GENERAL  ****/
            [
                'nombre' => 'P1',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P2',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P3',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P4',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P5',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P6',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P7',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P8',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P9',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P10',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P11',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P12',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P13',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P14',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P15',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P16',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P17',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P18',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P19',
                'lugar_id' => '1'
            ],
            [
                'nombre' => 'P20',
                'lugar_id' => '1'
            ],
            /***** BOMBREROS *****/
            [
                'nombre' => 'B7',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B8',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B9',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B10',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B11',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B12',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B13',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B14',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B15',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B16',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B17',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B18',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B19',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'B20',
                'lugar_id' => '2'
            ],
            /************* MILITAR *****/
            [
                'nombre' => 'P1',
                'lugar_id' => '3'
            ],
            [
                'nombre' => 'P2',
                'lugar_id' => '3'
            ],
            [
                'nombre' => 'P3',
                'lugar_id' => '3'
            ],
            [
                'nombre' => 'P4',
                'lugar_id' => '2'
            ],
            [
                'nombre' => 'P5',
                'lugar_id' => '2'
            ]
            
            
            
            
        ]);
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_puestos');
    }
}
