<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestMembresiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_membresias', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('aeronave_id');
            $table->date('fecha_desde');
            $table->date('fecha_hasta');
            
            $table->unsignedBigInteger('tipo_membresia_id'); //1: Oficial,  2: convenio  3:Privada
            $table->string('empresa', 200);
            
            $table->string('exonerar', 200);
            
            
            $table->string('observacion');
            
            
            $table->string('ip', 20);
            $table->date('fecha_registro');
            $table->unsignedBigInteger('user_id');
            
            
            $table->foreign('user_id')->references('id')->on('seg_users');
            $table->foreign('aeronave_id')->references('id')->on('maest_aeronaves');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_membresias');
    }
}
