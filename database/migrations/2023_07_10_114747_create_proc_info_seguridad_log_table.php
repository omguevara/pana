<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcInfoSeguridadLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_info_seguridad_log', function (Blueprint $table) {
            $table->id();
            $table->text('s_aad')->nullable();
            $table->text('s_ona')->nullable();
            $table->text('s_onm')->nullable();
            $table->text('s_ats')->nullable();
            $table->text('s_ac')->nullable();
            $table->text('s_seh')->nullable();
            $table->text('s_avsec')->nullable();
            $table->text('s_gnb_a')->nullable();
            $table->text('s_seniat')->nullable();
            $table->text('s_saime')->nullable();
            $table->text('s_ds')->nullable();
            $table->text('s_gnb_rn')->nullable();
            $table->text('s_pnb')->nullable();
            $table->text('s_sebin')->nullable();

            $table->string('s_ns_avsec', 10)->nullable();
            $table->string('s_na_avsec', 10)->nullable();

            $table->text('s_edmp_o', 10)->nullable();
            $table->text('s_edmp_i', 10)->nullable();

            $table->text('s_edmm_o', 10)->nullable();
            $table->text('s_edmm_i', 10)->nullable();

            $table->text('s_mrpc_o', 10)->nullable();
            $table->text('s_mrpc_i', 10)->nullable();

            $table->text('s_mb_o', 10)->nullable();
            $table->text('s_mb_i', 10)->nullable();

            $table->text('s_rp_o', 10)->nullable();
            $table->text('s_rp_i', 10)->nullable();

            $table->text('s_rb_o', 10)->nullable();
            $table->text('s_rb_i', 10)->nullable();

            $table->text('s_c_o', 10)->nullable();
            $table->text('s_c_i', 10)->nullable();

            $table->text('s_g_o', 10)->nullable();
            $table->text('s_g_i', 10)->nullable();

            $table->text('s_mech_o', 10)->nullable();
            $table->text('s_mech_i', 10)->nullable();

            $table->text('s_nsavseci', 10)->nullable();
            $table->text('s_nsavsecr', 10)->nullable();
            $table->text('s_naavseci', 10)->nullable();
            $table->text('s_naavsecr', 10)->nullable();

            $table->boolean('s_cls')->default(false)->nullable();;
            $table->boolean('s_pls')->default(false)->nullable();;
            $table->boolean('s_pimsacad')->default(false)->nullable();;
            $table->boolean('s_pccmsacad')->default(false)->nullable();

            $table->string('ip', 20);
            $table->bigInteger('user_id');
            $table->bigInteger('aeropuerto_id');

            $table->timestamp('register_date')->default(DB::raw('CURRENT_TIMESTAMP'));
            
            $table->string('tipo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_info_seguridad_log');
    }
}
