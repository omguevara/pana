<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfTaquillasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('conf_taquillas', function (Blueprint $table) {
            $table->id();

            $table->string('codigo', 10);
            $table->string('nombre', 100);
            $table->unsignedBigInteger('aeropuerto_id');
            $table->string('seguridad', 100);
            $table->string('numero_factura', 20);
            $table->string('numero_control', 20);
            $table->string('modelo_factura', 100);
            $table->boolean('activo')->default(true);

            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);

            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('user_id')->references('id')->on('seg_users');
        });

        DB::table('conf_taquillas')->insert([
            [
                "codigo" => "0001",
                "nombre" => "TAQUILLA BAER CENTRAL",
                "aeropuerto_id" => 15,
                "seguridad" => "abcde",
                "numero_factura" => 0,
                "numero_control" => 0,
                "modelo_factura" => "",
                "user_id" => 1,
                "fecha_registro" => now(),
                "ip" => "127.0.0.1",
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('conf_taquillas');
    }

}
