<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfFormaPagosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('conf_forma_pagos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 50)->unique();
            $table->boolean('vuelto')->default(false);
            $table->string('calculo_formula', 50);
            $table->string('reverso_formula', 50);
            $table->boolean('form')->default(true);
            $table->boolean('activo')->default(false);
            
        });

        DB::table('conf_forma_pagos')->insert([
            [
                'nombre' => 'Eféctivo Bs.',
                'calculo_formula'=>'',
                'reverso_formula'=>'',
                'vuelto'=>true,
                'form'=>'1',
                'activo' => '1'
            ],
            [
                'nombre' => 'T. Débito',
                'calculo_formula'=>'',
                'reverso_formula'=>'',
                'vuelto'=>false,
                'form'=>'1',
                'activo' => '1' 
            ],
            [
                'nombre' => 'T. Crédito',
                'calculo_formula'=>'',
                'reverso_formula'=>'',
                'vuelto'=>false,
                'form'=>'1',
                'activo' => '1'
            ],
            [
                'nombre' => '$ Eféctivo',
                'calculo_formula'=>'[VALOR_DOLLAR]*[VALOR]',
                'reverso_formula'=>'[VALOR]/[VALOR_DOLLAR]',
                'vuelto'=>true,
                'form'=>'1',
                'activo' => '1'
            ],
            [
                'nombre' => '€ Euro',
                'calculo_formula'=>'[VALOR_EURO]*[VALOR]',
                'reverso_formula'=>'[VALOR]/[VALOR_EURO]',
                'vuelto'=>true,
                'form'=>'1',
                'activo' => '1'
            ],
            [
                'nombre' => 'Online',
                'calculo_formula'=>'',
                'reverso_formula'=>'',
                'vuelto'=>false,
                'form'=>'0',
                'activo' => '1'
            ],
            [
                'nombre' => 'Pago Movil',
                'calculo_formula'=>'',
                'reverso_formula'=>'',
                'vuelto'=>false,
                'form'=>'1',
                'activo' => '1'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('conf_forma_pagos');
    }

}
