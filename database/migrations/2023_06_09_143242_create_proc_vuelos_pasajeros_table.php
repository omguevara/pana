<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVuelosPasajerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_vuelos_pasajeros', function (Blueprint $table) {
            $table->id();
            
            $table->string('documento', 50);
            $table->string('nombres', 100);
            $table->string('apellidos', 100);
            $table->string('genero', 50);
            $table->unsignedBigInteger('vuelo_id');
            
            $table->foreign('vuelo_id')->references('id')->on('proc_vuelos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_vuelos_pasajeros');
    }
}
