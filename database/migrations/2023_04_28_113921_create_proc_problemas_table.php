<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcProblemasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proc_problemas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('estado_id')->default(1); // 1: Registrado, 2:en Evaluacion, 3:Resuelto , 
            $table->string('modulo', '50');
            $table->text('descripcion');
            $table->text('respuesta')->nullable();
            
            
            

            $table->timestamp('fecha_registro');
            $table->string('ip', 20);

            $table->foreign('user_id')->references('id')->on('seg_users');
            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proc_problemas');
    }

}
