<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcHangaresTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proc_hangares', function (Blueprint $table) {
            $table->id();


            $table->string('nombre', 100);
            $table->string('dimension', 10);
            $table->unsignedBigInteger('ubicacion_id');
            
            
            
            $table->string('rif', 20)->nullable();
            $table->string('razon_social', 100)->nullable();
            $table->string('representante_legal', 20)->nullable();
            $table->string('nombres_apellidos', 200)->nullable();
            $table->string('numero_contacto', 20)->nullable();
            $table->string('numero_contrato', 20)->nullable();
            
            $table->unsignedBigInteger('objeto_id')->nullable();
            $table->unsignedBigInteger('uso_id')->nullable();
            $table->date('fecha_suscripcion')->nullable();
            $table->date('fecha_vencimiento')->nullable();
            
            $table->unsignedBigInteger('estatus_id')->nullable();
            
            $table->decimal('canon', 10, 4)->nullable();
            
            $table->text('observacion')->nullable();
            
            $table->boolean('activo')->default(true);


            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);


            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('ubicacion_id')->references('id')->on('maest_ubicaciones_hangares');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proc_hangares');
    }

}
