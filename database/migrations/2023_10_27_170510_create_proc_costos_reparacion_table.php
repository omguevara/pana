<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProcCostosReparacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_costos_reparacion', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inventario_id');
            $table->timestamp('fecha_factura');
            $table->string('numero_factura');
            $table->string('precio_unitario');
            $table->string('total');
        });
        DB::table('proc_costos_reparacion')->insert([
            ['inventario_id' => '6', 'fecha_factura' => '2017-04-27 00:00:00', 'numero_factura' => 'CFEC17001', 'precio_unitario' => '328300.00', 'total' => '328300.00'],
            ['inventario_id' => '8', 'fecha_factura' => '2015-09-01 00:00:00', 'numero_factura' => 'CFEC44103', 'precio_unitario' => '362500.00', 'total' => '362500.00'],
            ['inventario_id' => '10', 'fecha_factura' => '2015-09-01 00:00:00', 'numero_factura' => 'CFEC44103', 'precio_unitario' => '362500.00', 'total' => '362500.00'],
            ['inventario_id' => '12', 'fecha_factura' => '2015-04-26 00:00:00', 'numero_factura' => 'CFEC44101', 'precio_unitario' => '566250.00', 'total' => '566250.00'],
            ['inventario_id' => '14', 'fecha_factura' => '2017-04-27 00:00:00', 'numero_factura' => 'CFEC17001', 'precio_unitario' => '566250.00', 'total' => '566250.00'],
            ['inventario_id' => '200', 'fecha_factura' => '2015-09-01 00:00:00', 'numero_factura' => 'CFEC44103', 'precio_unitario' => '35800.00', 'total' => '35800.00'],
            ['inventario_id' => '202', 'fecha_factura' => '2015-09-01 00:00:00', 'numero_factura' => 'CFEC44103', 'precio_unitario' => '35800.00', 'total' => '35800.00'],
            ['inventario_id' => '203', 'fecha_factura' => '2015-09-01 00:00:00', 'numero_factura' => 'CFEC44103', 'precio_unitario' => '35800.00', 'total' => '35800.00'],
            ['inventario_id' => '215', 'fecha_factura' => '2015-09-01 00:00:00', 'numero_factura' => 'CFEC44104-1', 'precio_unitario' => '85000.00', 'total' => '85000.00'],            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_costos_reparacion');
    }
}
