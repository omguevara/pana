<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seg_menus', function (Blueprint $table) {
            $table->id();
            $table->string('name_menu', 50);
            $table->string('description', 200);
            $table->string('icon', 50);
            $table->integer('order');
        });

        DB::table('seg_menus')->insert([
            [ //1
                "name_menu" => "Seguridad",
                "description" => "",
                "icon" => "lock",
                "order" => "1"
            ]
            ,
            [  // 2
                "name_menu" => "Parametros",
                "description" => "",
                "icon" => "cogs",
                "order" => "2"
            ],
            [  //3
                "name_menu" => "Operaciones",
                "description" => "",
                "icon" => "tools",
                "order" => "4"
            ],
            [  //4
                "name_menu" => "Recaudación",
                "description" => "",
                "icon" => "chart-line",
                "order" => "5"
            ],
            [  //5
                "name_menu" => "Consultas/Reportes",
                "description" => "",
                "icon" => "chart-line",
                "order" => "5"
            ]
            ,
            [ //6
                "name_menu" => "Configuraciones",
                "description" => "",
                "icon" => "cogs",
                "order" => "3"
            ]
        ]);

        $sql = '
                CREATE OR REPLACE  FUNCTION eval(formula text) RETURNS DECIMAL(10,4) AS
                $BODY$
                        DECLARE
                                resultado DECIMAL(10,4);
                        BEGIN
                                BEGIN
                                        EXECUTE \'SELECT \' || formula INTO resultado;
                                        EXCEPTION 
                                                WHEN OTHERS THEN  resultado :=-1;
                                END;	
                                RETURN resultado;
                        END;
                $BODY$
                LANGUAGE PLPGSQL;	
                ';

        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seg_menus');
    }

}
