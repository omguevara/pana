<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAeropuertosAerolineasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_aeropuertos_aerolineas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('aerolinea_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_aeropuertos_aerolineas');
    }
}
