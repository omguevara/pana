<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestCategoriasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('maest_categorias', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 50);
            $table->decimal('porcentaje', 10, 2);
            $table->boolean('activo')->default(true);
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);
        });

        DB::table('maest_categorias')->insert([
            [ //1
                'nombre' => 'CATEGORÍA A',
                'porcentaje' => '1',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ],
            [ //2
                'nombre' => 'CATEGORÍA B',
                'porcentaje' => '0.85',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ],
            [ //2
                'nombre' => 'CATEGORÍA C',
                'porcentaje' => '0.6',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ],
            [ //4
                'nombre' => 'CATEGORÍA D',
                'porcentaje' => '0.3',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ],
            [ //5
                'nombre' => 'CATEGORÍA DE AEROPUERTOS DE INTERÉS TURÍSTICO A',
                'porcentaje' => '0.9',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ],
            [
                'nombre' => 'CATEGORÍA DE AEROPUERTOS DE INTERÉS TURÍSTICO B',
                'porcentaje' => '0.8',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ],
            [
                'nombre' => 'CATEGORÍA DE AEROPUERTOS DE INTERÉS TURÍSTICO C',
                'porcentaje' => '0.6',
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' => \Request::ip(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('maest_categorias');
    }

}
