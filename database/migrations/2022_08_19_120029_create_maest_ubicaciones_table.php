<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestUbicacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_ubicaciones', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->boolean('activo')->default(true);
            $table->unsignedBigInteger('aeropuerto_id');
            
            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
        });
        
        DB::table('maest_ubicaciones')->insert([
            [
                'nombre' => 'PLATAFORMA',
                'aeropuerto_id' => '1'
            ],
            [
                'nombre' => 'OMA',
                'aeropuerto_id' => '1'
            ],
            [
                'nombre' => 'HANGARES',
                'aeropuerto_id' => '1'
            ]
            
        ]);    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_ubicaciones');
    }
}
