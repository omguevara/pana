<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcFacturasTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proc_facturas', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha_factura');

            $table->unsignedBigInteger('cliente_id');
            $table->string('nro_factura');
            $table->string('nro_documento');
            $table->unsignedBigInteger('tipo_pago_id')->default(1); //1:contado   2:credito  3:credito 15 dias ...


            $table->decimal('tasa_dolar', 10, 4);
            $table->decimal('tasa_euro', 10, 4);
            $table->decimal('tasa_petro', 10, 4);




            $table->string('formato_factura');
            $table->text('observacion')->nullable();
            $table->boolean('anulada')->default(false);
            $table->boolean('impresa')->default(false);

            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);

            $table->foreign('cliente_id')->references('id')->on('maest_clientes');
            $table->foreign('user_id')->references('id')->on('seg_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proc_facturas');
    }

}
