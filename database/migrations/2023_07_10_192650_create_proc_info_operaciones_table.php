<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcInfoOperacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //limit 100
    public function up()
    {
        Schema::create('proc_info_operaciones', function (Blueprint $table) {
            $table->id();

            //1
            $table->string('orientacion',100)->nullable();
            $table->string('largo',100)->nullable();
            $table->string('ancho',100)->nullable();
            $table->string('pc1_pstas',100)->nullable();
            $table->string('pc2_pstas',100)->nullable();
            $table->string('pc3_pstas',100)->nullable();
            $table->string('tora',100)->nullable();
            $table->string('toda',100)->nullable();
            $table->string('asda',100)->nullable();
            $table->string('lda',100)->nullable();
            $table->boolean('flexible')->default(false)->nullable();
            $table->boolean('rigido')->default(false)->nullable();
            $table->boolean('otro')->default(false)->nullable();

            $table->integer('demarcacion')->default(false)->nullable();
            
            $table->integer('condicion')->default(false)->nullable();
            
            //2
            $table->boolean('agrietamiento')->default(false)->nullable();
            $table->boolean('ahuellamiento')->default(false)->nullable();
            $table->boolean('disgregacion')->default(false)->nullable();
            $table->boolean('cont_neumatica')->default(false)->nullable();
            $table->boolean('piel_cocodrilo')->default(false)->nullable();
            $table->boolean('pres_vegetacion')->default(false)->nullable();
            $table->boolean('detritos')->default(false)->nullable();
            $table->boolean('drenaje')->default(false)->nullable();
            $table->boolean('otras')->default(false)->nullable();
            $table->string('otras_text',100)->nullable();
            $table->string('comercial_area',100)->nullable();
            $table->string('comercial_puesto',100)->nullable();
            $table->string('comercial_pcn1',100)->nullable();
            $table->string('comercial_pavimento',100)->nullable();
            $table->string('comercial_condicion',100)->nullable();
            $table->string('general_area',100)->nullable();
            $table->string('general_puesto',100)->nullable();
            $table->string('general_pcn2',100)->nullable();
            $table->string('general_pavimento',100)->nullable();
            $table->string('general_condicion',100)->nullable();
            $table->string('carga_area',100)->nullable();
            $table->string('carga_puesto',100)->nullable();
            $table->string('carga_pcn3',100)->nullable();
            $table->string('carga_pavimento',100)->nullable();
            $table->string('carga_condicion',100)->nullable();

            //3
            $table->string('calle_a',100)->nullable();
            $table->string('calle_b',100)->nullable();
            $table->string('calle_c',100)->nullable();
            $table->string('calle_d',100)->nullable();
            $table->string('calle_e',100)->nullable();
            $table->string('calle_f',100)->nullable();

            //4
            $table->boolean('lbp')->default(false)->nullable();
            $table->boolean('lep')->default(false)->nullable();
            $table->boolean('papi')->default(false)->nullable();
            $table->boolean('lcr')->default(false)->nullable();
            $table->boolean('lp')->default(false)->nullable();
            $table->boolean('lp2')->default(false)->nullable();
            $table->string('obs_balizaje',100)->nullable();

            //5
            $table->boolean('vor')->default(false)->nullable();
            $table->boolean('dme')->default(false)->nullable();
            $table->boolean('ils')->default(false)->nullable();
            $table->boolean('ndb')->default(false)->nullable();
            $table->boolean('radio_o')->default(false)->nullable();
            $table->string('na_otras_esp',100)->nullable();

            //6
            $table->string('ser_comb_avgas_cap',100)->nullable();
            $table->string('ser_comb_avgas_exis',100)->nullable();
            $table->string('ser_comb_jeta_cap',100)->nullable();
            $table->string('ser_comb_jeta_exis',100)->nullable();
            $table->string('serv_comb_otro_cap',100)->nullable();
            $table->string('serv_comb_otro_exis',100)->nullable();
            $table->string('sev_comb_otro_text',100)->nullable();
            $table->string('serv_comb_cs_op',100)->nullable();
            $table->string('serv_comb_cs_ino',100)->nullable();
            $table->string('serv_comb_hdtt_op',100)->nullable();
            $table->string('serv_comb_hdtt_ino',100)->nullable();
            $table->string('serv_comb_obs',100)->nullable();
            
            //7
            $table->string('emp_serv_empre',100)->nullable();
            $table->string('emp_serv_cant_equi',100)->nullable();

            //8
            $table->string('cerc_per_tip_mtl',100)->nullable();
            $table->string('cerc_per_long_ttl',100)->nullable();
            $table->string('cerc_per_long_atl',100)->nullable();
            $table->string('cerc_per_obs',100)->nullable();

            //9
            $table->string('tml_pax_lbby',100)->nullable();
            $table->string('tml_pax_cheq',100)->nullable();
            $table->string('tml_pax_emb',100)->nullable();
            $table->string('tml_pax_desm',100)->nullable();
            $table->string('tml_pax_illum',100)->nullable();
            $table->string('tml_pax_sani',100)->nullable();
            $table->string('tml_pax_estaci',100)->nullable();
            $table->string('tml_pax_pot',100)->nullable();
            $table->string('tml_pax_trat',100)->nullable();
            $table->string('tml_pax_clima',100)->nullable();
            $table->string('tml_pax_area_verde',100)->nullable();
            $table->string('tml_pax_serv_trans',100)->nullable();

            //10
            $table->boolean('ssa')->default(false)->nullable();
            $table->boolean('saime')->default(false)->nullable();
            $table->boolean('ve')->default(false)->nullable();
            $table->boolean('sta')->default(false)->nullable();
            $table->boolean('sa')->default(false)->nullable();
            $table->boolean('vm')->default(false)->nullable();
            $table->boolean('oais')->default(false)->nullable();
            $table->boolean('insai')->default(false)->nullable();
            $table->boolean('spe')->default(false)->nullable();
            $table->boolean('sc')->default(false)->nullable();
            $table->boolean('sds')->default(false)->nullable();

            $table->string('ip', 20)->nullable();
            $table->bigInteger('user_id');
            $table->bigInteger('aeropuerto_id');
            $table->timestamp('register_date')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_info_operaciones');
    }
}
