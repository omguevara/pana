<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestLugaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_lugares', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->unsignedBigInteger('ubicacion_id');
            $table->boolean('activo')->default(true);
            
            $table->foreign('ubicacion_id')->references('id')->on('maest_ubicaciones');
        });
        
        
        DB::table('maest_lugares')->insert([
            [
                'nombre' => 'GENERAL',
                'ubicacion_id' => '1'
            ],
            [
                'nombre' => 'BOMBEROS',
                'ubicacion_id' => '1'
            ],
            [
                'nombre' => 'MILITAR',
                'ubicacion_id' => '1'
            ]
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_lugares');
    }
}
