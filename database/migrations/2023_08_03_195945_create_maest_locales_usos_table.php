<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('maest_locales_usos', function (Blueprint $table) {
            $table->id();
            $table->string('uso', 100);
            $table->integer('orden');
        });
        DB::table('maest_locales_usos')->insert([
            ['uso' => 'ALMACÉN', 'orden' => 1],
            ['uso' => 'ÁREA EN EDIFICIO PARA USO COMERCIAL ', 'orden' => 1],
            ['uso' => 'ÁREA EN EDIFICIO PARA USO COMERCIAL  (EDIFICIO ADMINISTRATIVO)', 'orden' => 1],
            ['uso' => 'ÁREA EN EDIFICIO PARA USO COMERCIAL (HOTEL Y OFICINA)', 'orden' => 1],
            ['uso' => 'ÁREA EN EDIFICIO PARA USO COMERCIAL (TALLER MECÁNICO)', 'orden' => 1],
            ['uso' => 'COUNTER', 'orden' => 1],
            ['uso' => 'DEPÓSITO', 'orden' => 1],
            ['uso' => 'DEPOSITO (EQUIPAJE)', 'orden' => 1],
            ['uso' => 'ESTACIONAMIENTO INTERNO', 'orden' => 1],
            ['uso' => 'HANGAR', 'orden' => 1],
            ['uso' => 'LOCAL COMERCIAL', 'orden' => 1],
            ['uso' => 'LOCAL COMERCIAL RESTAURANTE', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (ADMINISTRATIVO)', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (AREA ADMINISTRATIVA)', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (AREA DE DESCANSO)', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (DEPARTAMENTO DE EQUIPAJE)', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (DESPACHO DE VUELO)', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (GERENCIAS)', 'orden' => 1],
            ['uso' => 'LOCAL OFICINA (PERSONAL DE TRANSFERENCIA)', 'orden' => 1],
            ['uso' => 'MODULO DE BIENES Y SERVICIOS', 'orden' => 1],
            ['uso' => 'TERRENO', 'orden' => 1],
            ['uso' => 'TERRENO (ESTACIONAMIENTO FRONTAL)', 'orden' => 1],
            ['uso' => 'TERRENO (ESTACIONAMIENTO PRESIDENCIA)', 'orden' => 1],
            ['uso' => 'OTRO', 'orden' => 2],

        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('maest_locales_usos');
    }
};
