<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMaestBancosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('maest_bancos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->string('codigo', 10);
            $table->boolean('activo')->default(true);

            $table->unsignedBigInteger('user_id');
            $table->string('ip', 20);
            $table->timestamp('fecha_creacion')->default(Carbon::now());
        });

        DB::table('maest_bancos')->insert([
            ['codigo' => '0000 ', 'nombre' => 'PAGO MOVIL ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0102 ', 'nombre' => 'Banco de Venezuela ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0104 ', 'nombre' => 'Banco Venezolano de Crédito ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0105 ', 'nombre' => 'Banco Mercantil ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0108 ', 'nombre' => 'BBVA Provincial ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0114 ', 'nombre' => 'Bancaribe ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0115 ', 'nombre' => 'Banco Exterior ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0116 ', 'nombre' => 'B.O.D. ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0128 ', 'nombre' => 'Banco Caroní ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0134 ', 'nombre' => 'Banesco ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0137 ', 'nombre' => 'Banco Sofitasa ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0151 ', 'nombre' => 'BFC Banco Fondo Común ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0156 ', 'nombre' => '100% Banco ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0163 ', 'nombre' => 'Banco Del Tesoro ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0166 ', 'nombre' => 'Banco Agrícola de Venezuela ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0168 ', 'nombre' => 'Bancrecer ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0169 ', 'nombre' => 'Mi Banco ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0171 ', 'nombre' => 'Banco Activo ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0175 ', 'nombre' => 'Banco Bicentenario del Pueblo ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0177 ', 'nombre' => 'Banfanb ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0190 ', 'nombre' => 'Citibank ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0191 ', 'nombre' => 'Banco Nacional de Crédito (BNC) ', 'user_id' => 1, 'ip' => '127.0.0.1'],
            ['codigo' => '0172 ', 'nombre' => 'Banca Amiga', 'user_id' => 1, 'ip' => '127.0.0.1']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('maest_bancos');
    }

}
