<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestCuentaBancariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_cuenta_bancarias', function (Blueprint $table) {
            $table->id();
            $table->string('numero_cuenta')->unique();
            $table->unsignedBigInteger('banco_id');
            $table->boolean('activo')->default(true);
            
            $table->unsignedBigInteger('user_id');
            $table->string('ip', 20);
            $table->timestamp('fecha_registro')->default(Carbon::now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_cuenta_bancarias');
    }
}
