<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestTiempoTicketsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('maest_tiempo_tickets', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 100);
            $table->decimal('precio', 10, 2);
            $table->string('icono', 10);
        });

        DB::table('maest_tiempo_tickets')->insert([
            [
                'nombre' => '1 DÍA',
                'precio' => '1',
                'icono' => 'clock',
                
            ],
            [
                'nombre' => '1 SEMANA',
                'precio' => '5',
                'icono' => 'week',
            ],
            [
                'nombre' => '1 MES',
                'precio' => '15',
                'icono' => 'month',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('maest_tiempo_tickets');
    }

}
