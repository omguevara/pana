<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVuelosGeneralesEstadisticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_vuelos_generales_estadisticas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vuelos_general_id');
            $table->unsignedBigInteger('tipo_id'); //1: TASA   2: DOSA  
            $table->boolean('procesado')->default(false);
            
            
            
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_vuelos_generales_estadisticas');
    }
}
