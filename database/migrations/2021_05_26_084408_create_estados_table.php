<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('conf_estados', function (Blueprint $table) {
            $table->id();
            $table->string('name_state', 50);

            $table->unsignedBigInteger('pais_id')->nullable();

            //$table->foreign('pais_id')->references('id')->on('paises');
        });

        DB::table('conf_estados')->insert([
            ['name_state' => 'Amazonas',
                'pais_id' => '1']
            , ['name_state' => 'Anzoátegui',
                'pais_id' => '1']
            , ['name_state' => 'Apure',
                'pais_id' => '1']
            , ['name_state' => 'Aragua',
                'pais_id' => '1']
            , ['name_state' => 'Barinas',
                'pais_id' => '1']
            , ['name_state' => 'Bolívar',
                'pais_id' => '1']
            , ['name_state' => 'Carabobo',
                'pais_id' => '1']
            , ['name_state' => 'Cojedes',
                'pais_id' => '1']
            , ['name_state' => 'Delta Amacuro',
                'pais_id' => '1']
            , ['name_state' => 'Distrito Capital',
                'pais_id' => '1']
            , ['name_state' => 'Falcón',
                'pais_id' => '1']
            , ['name_state' => 'Guárico',
                'pais_id' => '1']
            , ['name_state' => 'La Guaira',
                'pais_id' => '1']
            , ['name_state' => 'Lara',
                'pais_id' => '1']
            , ['name_state' => 'Mérida',
                'pais_id' => '1']
            , ['name_state' => 'Miranda',
                'pais_id' => '1']
            , ['name_state' => 'Monagas',
                'pais_id' => '1']
            , ['name_state' => 'Nueva Esparta',
                'pais_id' => '1']
            , ['name_state' => 'Portuguesa',
                'pais_id' => '1']
            , ['name_state' => 'Sucre',
                'pais_id' => '1']
            , ['name_state' => 'Táchira',
                'pais_id' => '1']
            , ['name_state' => 'Trujillo',
                'pais_id' => '1']
            , ['name_state' => 'Yaracuy',
                'pais_id' => '1']
            , ['name_state' => 'Zulia',
                'pais_id' => '1']
            , ['name_state' => 'Otro',
                'pais_id' => '0']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('conf_estados');
    }

}
