<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcLotesTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_lotes_tickets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('tiempo_id');
            $table->decimal('precio', 10, 2);
            
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);
            
            $table->foreign('tiempo_id')->references('id')->on('maest_tiempo_tickets');
            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('user_id')->references('id')->on('seg_users');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_lotes_tickets');
    }
}
