<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVentaTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_venta_tickets', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha');
            $table->unsignedBigInteger('cliente_id');
            $table->unsignedBigInteger('aeropuerto_id');
            $table->decimal('tasa', 10, 2);
            
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('cierre_id')->nullable();
            $table->string('ip', 20);
            
            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('user_id')->references('id')->on('seg_users');
            $table->foreign('cliente_id')->references('id')->on('maest_clientes');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_venta_tickets');
    }
}
