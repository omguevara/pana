<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_videos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aeropuerto_id');
            $table->string('comercio')->default('BAER');
            $table->timestamp('fecha_expiracion')->default(Carbon::now()->endOfYear());
            $table->string('nombre_video')->nullable();
            $table->boolean('activo')->default(true);
            $table->boolean('publicidad');

            $table->unsignedBigInteger('user_id');
            $table->string('ip', 20);
            $table->timestamp('fecha_creacion')->default(Carbon::now());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_videos');
    }
}
