<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcProformaPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_proforma_pagos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('proforma_id');
            $table->timestamp('fecha_pago');
            $table->unsignedBigInteger('user_id');
            $table->decimal('monto', 10,4);

            $table->foreign('user_id')->references('id')->on('seg_users');
            $table->foreign('proforma_id')->references('id')->on('proc_proforma');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_proforma_pagos');
    }
}
