<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcInfoBomberos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_info_bomberos', function (Blueprint $table) {

            //1
            $table->id();
            $table->string('baer_cant',100)->nullable();
            $table->string('otros_cant',100)->nullable();
            $table->string('requi_cant',100)->nullable();
        
            $table->string('bomba_a',100)->nullable();
            $table->string('hidro_a',100)->nullable();
            $table->string('tanqu_a',100)->nullable();
               
     

            $table->string('mam1_vci',100)->nullable();
            $table->string('mam2_vci',100)->nullable();
            $table->string('mam3_vci',100)->nullable();
            $table->string('mam4_vci',100)->nullable();
            $table->string('mam5_vci',100)->nullable();
            $table->string('mam6_vci',100)->nullable();

            $table->string('mam_obs1_vci',100)->nullable();
            $table->string('mam_obs2_vci',100)->nullable();
            $table->string('mam_obs3_vci',100)->nullable();
            $table->string('mam_obs4_vci',100)->nullable();
            $table->string('mam_obs5_vci',100)->nullable();
            $table->string('mam_obs6_vci',100)->nullable();

            $table->string('mam1_ambu',100)->nullable();
            $table->string('mam2_ambu',100)->nullable();
            $table->string('mam3_ambu',100)->nullable();
            $table->string('mam4_ambu',100)->nullable();
            $table->string('mam5_ambu',100)->nullable();
            $table->string('mam6_ambu',100)->nullable();

            $table->string('mam_obs1_ambu',100)->nullable();
            $table->string('mam_obs2_ambu',100)->nullable();
            $table->string('mam_obs3_ambu',100)->nullable();
            $table->string('mam_obs4_ambu',100)->nullable();
            $table->string('mam_obs5_ambu',100)->nullable();
            $table->string('mam_obs6_ambu',100)->nullable();

            //2

            $table->boolean('con_a')->default(false)->nullable();
            $table->boolean('con_b')->default(false)->nullable();
            $table->boolean('con_c')->default(false)->nullable();
            $table->string('pqseco',100)->nullable();

            $table->string('traalu_dis',100)->nullable();
            $table->string('traalu_req',100)->nullable();

            $table->string('traest_dis',100)->nullable();
            $table->string('traest_req',100)->nullable();

            $table->string('eqprocres_dis',100)->nullable();
            $table->string('eqprocres_req',100)->nullable();

            $table->string('sisci_op',100)->nullable();
            $table->string('sisci_inop',100)->nullable();
            $table->string('sisci_nopose',100)->nullable();
           
            //3
            $table->string('herrdesc1',100)->nullable();
            $table->string('herrdesc2',100)->nullable();
            $table->string('herrdesc3',100)->nullable();
            $table->string('herrdesc4',100)->nullable();
            $table->string('herrdesc5',100)->nullable();
            $table->string('herrdesc6',100)->nullable();
            $table->string('herrdesc7',100)->nullable();
            $table->string('herrdesc8',100)->nullable();
            $table->string('herrdesc9',100)->nullable();
            $table->string('herrdesc10',100)->nullable();            
            
            $table->string('herrcond1',100)->nullable(); 
            $table->string('herrcond2',100)->nullable(); 
            $table->string('herrcond3',100)->nullable(); 
            $table->string('herrcond4',100)->nullable(); 
            $table->string('herrcond5',100)->nullable();
            $table->string('herrcond6',100)->nullable(); 
            $table->string('herrcond7',100)->nullable(); 
            $table->string('herrcond8',100)->nullable(); 
            $table->string('herrcond9',100)->nullable(); 
            $table->string('herrcond10',100)->nullable();
            $table->string('ip', 20);
            $table->bigInteger('user_id');
            $table->bigInteger('aeropuerto_id');
            $table->timestamp('register_date')->default(DB::raw('CURRENT_TIMESTAMP'));


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_info_bomberos');
    }
}
