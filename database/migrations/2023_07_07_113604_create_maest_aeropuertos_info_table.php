<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestAeropuertosInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_aeropuertos_info', function (Blueprint $table) {
            $table->id();
            $table->text('mapa_estado');
            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_aeropuertos_info');
    }
}
