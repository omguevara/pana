<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVentaTicketsCierreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_venta_tickets_cierre', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('usuario_cierre_id');
            $table->timestamp('fecha_registro');
            $table->unsignedBigInteger('usuario_id');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_venta_tickets_cierre');
    }
}
