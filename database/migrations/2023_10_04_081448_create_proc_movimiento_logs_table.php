<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcMovimientoLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_movimientos_logs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('aerolinea_id');
            $table->unsignedBigInteger('tipo_operacion_id');
            $table->string('numero_vuelo', 50);
            $table->unsignedBigInteger('origen_destino_id');
            $table->date('fecha');
            $table->string('hora', 10);
            $table->string('puerta');
            $table->text('observaciones');
            $table->boolean('activo')->default(false);
            $table->string('tipo_registro')->default('create');

            $table->unsignedBigInteger('user_id');
            $table->string('ip', 20);
            $table->timestamp('fecha_creacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_movimientos_logs');
    }
}
