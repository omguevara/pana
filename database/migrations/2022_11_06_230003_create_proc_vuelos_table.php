<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVuelosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proc_vuelos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('aeronave_id');
            $table->unsignedBigInteger('piloto_id');
            $table->bigInteger('numero_operacion');
            $table->unsignedBigInteger('tipo_vuelo_id');  //1: NACIONAL   2:INTERNACIONAL
            $table->unsignedBigInteger('tipo_aviacion_id')->default(1);  //1: NACIONAL   2:INTERNACIONAL
            $table->unsignedBigInteger('tipo_operacion_id')->nullable(); // 1:ARRIVO  2:DESPEGUE
            
            $table->date('fecha_operacion');
            $table->integer('pasajeros_desembarcados')->nullable();
            $table->integer('pasajeros_embarcados')->nullable();
            
            
            $table->timestamp('hora_llegada')->nullable();
            $table->timestamp('hora_salida')->nullable();
            
            $table->boolean('cobrar_tasa')->default(true);
            $table->boolean('cobrar_dosa')->default(true);
            
            $table->boolean('exonerado')->default(false);
            
            $table->text('observaciones')->nullable();
            
            $table->text('observaciones_facturacion')->nullable();
            
            $table->text('prodservicios_extra')->nullable();
            $table->unsignedBigInteger('puesto_id')->nullable();

            $table->unsignedBigInteger('factura_id')->nullable();
            $table->unsignedBigInteger('proforma_id')->nullable();
            
            $table->boolean('activo')->default(true);
            $table->boolean('aprobado')->default(true);
            $table->decimal('carga')->nullable();
            
            
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);

            /*
              $table->bigInteger('numero_operacion');
              $table->date('fecha_operacion');
              $table->string('hora_operacion');
              $table->unsignedBigInteger('aeropuerto_id');
              $table->unsignedBigInteger('tipo_operacion_id'); // 1:ARRIVO  2:DESPEGUE
              $table->unsignedBigInteger('tipo_vuelo_id');  //1: NACIONAL   2:INTERNACIONAL
              $table->unsignedBigInteger('aeronave_id');
              $table->unsignedBigInteger('piloto_id');
              $table->unsignedBigInteger('origen_destino_id')->nullable();
              $table->integer('cant_pasajeros');
              $table->unsignedBigInteger('factura_id')->nullable();
              $table->unsignedBigInteger('proforma_id')->nullable();
              $table->boolean('activo')->default(true);
              $table->boolean('procesado_recaudacion')->default(false);
              $table->text('observaciones')->nullable();
              $table->text('prodservicios_extra')->nullable();
              $table->unsignedBigInteger('puesto_id')->nullable();




              // -.-.-.-.-.-.-.-.-.-.--.-.-.-.-.
              //$table->unsignedBigInteger('origen_registro_id');  //SISTEMA ONLINE

              $table->foreign('user_id')->references('id')->on('seg_users');
              $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
              $table->foreign('aeronave_id')->references('id')->on('maest_aeronaves');
              $table->foreign('piloto_id')->references('id')->on('maest_pilotos');
              //$table->foreign('origen_destino_id')->references('id')->on('maest_aeropuertos');

             */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proc_vuelos');
    }

}
