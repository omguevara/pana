<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seg_profiles', function (Blueprint $table) {
            $table->id();
            $table->string('code', 20);
            $table->string('name_profile', 50);
            $table->string('description', 200);
            $table->boolean('status')->default(true);
        });
        DB::table('seg_profiles')->insert([
            [
                "code" => "0001",
                "name_profile" => "Administrador",
                "description" => "El Administrador posee privilegios completos sobre el sistema.",
                "status" => true
            ],
            [
                "code" => "0002",
                "name_profile" => "Parametrización",
                "description" => "Asignar valores a los parámetros declarados para modificar o influir en su comportamiento.",
                "status" => true
            ],
            [
                "code" => "0003",
                "name_profile" => "Usuarios",
                "description" => "Usuarios son las personas que utilizan el sistema en sus distintos perfiles.",
                "status" => true
            ],
            [
                "code" => "0004",
                "name_profile" => "Operaciones Aeropuertos",
                "description" => "Operaciones garantiza que las funciones diarias fluyan sin inconvenientes. se centran en mantener la eficiencia del proceso de arrivo y despegue al nutrir al sistema correctamente.",
                "status" => true
            ],
            [
                "code" => "0005",
                "name_profile" => "Recaudación Aeropuerto",
                "description" => "La facturación es una acción que se refiere a todos los actos relacionados con la elaboración, registro, envío y cobro de las facturas.",
                "status" => true
            ],
            [
                "code" => "0006",
                "name_profile" => "Reportes",
                "description" => "Son las personas que utilizan el sistema con autorización para ver reportes, acceso a información generada por el sistema.",
                "status" => true
            ],
            [
                "code" => "0007",
                "name_profile" => "Operaciones Baer",
                "description" => "Operaciones garantiza que las funciones diarias fluyan sin inconvenientes. se centran en mantener la eficiencia del proceso de arrivo y despegue al nutrir al sistema correctamente.",
                "status" => true
            ],
            [
                "code" => "0008",
                "name_profile" => "Recaudación Baer",
                "description" => "La facturación es una acción que se refiere a todos los actos relacionados con la elaboración, registro, envío y cobro de las facturas.",
                "status" => true
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seg_profiles');
    }

}
