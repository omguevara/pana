<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcAlarmasInventarioDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_alarmas_inventario_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('inventario_equipo_id');
            $table->date('fecha_revision');
            $table->string('descripcion');
            $table->boolean('procesado')->default(false);
            $table->json('revision')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_alarmas_inventario_detalle');
    }
}
