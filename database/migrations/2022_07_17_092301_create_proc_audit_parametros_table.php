<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcAuditParametrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_audit_parametros', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha');
            $table->string('parametro', 50);
            $table->string('valor', 50);
            $table->unsignedBigInteger('user_id');
            $table->string('ip', 20);
            
            $table->foreign('user_id')->references('id')->on('seg_profile_processes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_audit_parametros');
    }
}
