<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seg_users', function (Blueprint $table) {
            $table->id();
            $table->string('document', 50)->unique();
            $table->string('name_user', 200);
            $table->string('surname_user', 200);

            $table->string('username', 20)->unique();
            $table->string('password', 60);

            $table->string('phone', 20);
            $table->string('email', 200)->unique();

            $table->boolean('change_password')->default(false);
            $table->boolean('active')->default(true);

            $table->string('avatar', 20)->default('0001');

            $table->unsignedBigInteger('profile_id');
            $table->unsignedBigInteger('aeropuerto_id')->nullable();
            $table->integer('theme')->default(1);

            $table->unsignedBigInteger('user_id');
            $table->timestamp('register_date');
            $table->string('ip', 20);

            $table->foreign('profile_id')->references('id')->on('seg_profiles');
            //$table->foreign('user_id')->references('id')->on('seg_users');

            $table->index('username');
        });

        DB::table('seg_users')->insert([
            [
                'document' => 'V99999999',
                'name_user' => 'Administrador',
                'surname_user' => 'Administrador Master',
                'username' => 'master',
                'password' => Hash::make('Mart.1010'),
                'phone' => '2329356',
                'email' => 'admin@pana.com',
                
                'change_password' => 0,
                'active' => 1,
                'aeropuerto_id' => 1,
                
                'profile_id' => 1,
                'aeropuerto_id' => null,
                'theme' => 1,
                'user_id' => 1,
                'register_date' => now(),
                'ip' => \Request::ip(),
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seg_users');
    }

}
