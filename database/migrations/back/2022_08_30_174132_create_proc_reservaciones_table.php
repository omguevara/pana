<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcReservacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_reservaciones', function (Blueprint $table) {
            $table->id();
            $table->string('numero_vuelo');
            $table->timestamp('fecha');
            $table->string('hora_inicio');
            $table->string('hora_fin');
            $table->unsignedBigInteger('solicitud_id');
            
            $table->decimal('valor_dolar', 10,2)->default(0);
            $table->decimal('valor_euro', 10,2)->default(0);
            $table->decimal('valor_petro', 10,2)->default(0);
            
            
            
            $table->unsignedBigInteger('origen_id');
            $table->string('otro_origen')->nullable();
            $table->unsignedBigInteger('destino_id');
            $table->string('otro_destino')->nullable();
            
            
            $table->unsignedBigInteger('aeronave_id')->nullable();
            $table->unsignedBigInteger('piloto_id')->nullable();
            
            
            $table->integer('cantidad_pasajeros')->nullable();
            $table->integer('carga_embarque')->nullable();
            $table->integer('carga_desembarque')->nullable();
            $table->decimal('precio', 10,2)->default(0);
            $table->string('observaciones')->nullable();
            
            $table->boolean('pagada')->default(false);
            $table->boolean('activo')->default(true);
            $table->boolean('facturado')->default(false);
            
            
            
            $table->foreign('solicitud_id')->references('id')->on('proc_solicitudes');
            $table->foreign('origen_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('destino_id')->references('id')->on('maest_aeropuertos');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_reservaciones');
    }
}
