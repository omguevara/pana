<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcVuelosTortugaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_vuelos_tortuga', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('vuelo_general_id');
            $table->string('hora_llegada')->nullable();
            $table->string('hora_salida')->nullable();
            
            
            
            $table->foreign('vuelo_general_id')->references('id')->on('proc_vuelos_generales');
            
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_vuelos_tortuga');
    }
}
