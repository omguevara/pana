<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_solicitudes', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha');
            $table->string('numero_solicitud')->nullable();
            $table->string('codigo_aprobacion')->nullable();
            $table->unsignedBigInteger('responsable_id');
            $table->string('correo', 100)->nullable();
            
            
            $table->integer('tipo_vuelo_id'); //1: comercial     2: general
            $table->integer('nacionalidad_id'); //1: nacional     2: internacional
            $table->integer('turno_id'); //1: general    2: carga
            
            
            $table->boolean('aprobado_aero')->default(null); 
            $table->timestamp('fecha_aprobado_aero')->default(null); 
            
            $table->boolean('aprobado_baer')->default(null); 
            $table->timestamp('fecha_aprobado_baer')->default(null); 
            
            $table->boolean('activo')->default(true); 
            
            
            
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);
            
            $table->foreign('responsable_id')->references('id')->on('maest_clientes');
            $table->foreign('user_id')->references('id')->on('seg_users');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_solicitudes');
    }
}
