<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcReservacionesProdserviciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_reservaciones_prodservicios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('reservacion_id');
            $table->unsignedBigInteger('prodservicio_id');
            $table->decimal('monto', 10, 4);
            $table->integer('tipo_moneda_id');
            $table->decimal('valor_moneda', 10, 4);
            
            
            $table->foreign('reservacion_id')->references('id')->on('proc_reservaciones');
            $table->foreign('prodservicio_id')->references('id')->on('maest_prod_servicios');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_reservaciones_prodservicios');
    }
}
