<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcSolicitudesAeronavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_solicitudes_aeronaves', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('solicitud_id');
            $table->unsignedBigInteger('aeronave_id');
            
            
            $table->foreign('solicitud_id')->references('id')->on('proc_solicitudes');
            $table->foreign('aeronave_id')->references('id')->on('maest_aeronaves');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_solicitudes_aeronaves');
    }
}
