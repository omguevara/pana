<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTriggerUpdateMovimientosLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE FUNCTION actualizar_movimientos_logs()
            RETURNS TRIGGER AS $$
            BEGIN
            INSERT INTO proc_movimientos_logs (aeropuerto_id, aerolinea_id, tipo_operacion_id, numero_vuelo, origen_destino_id, fecha, hora, puerta, observaciones, activo, user_id, ip, fecha_creacion, tipo_registro)
            VALUES (NEW.aeropuerto_id, NEW.aerolinea_id, NEW.tipo_operacion_id, NEW.numero_vuelo, NEW.origen_destino_id, NEW.fecha, NEW.hora, NEW.puerta, NEW.observaciones, NEW.activo, NEW.user_id, NEW.ip, NEW.fecha_creacion, 'update');
            RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
        ");
        
        DB::statement('
            CREATE TRIGGER actualizar_movimientos_logs
            AFTER UPDATE ON proc_movimientos
            FOR EACH ROW
            EXECUTE FUNCTION actualizar_movimientos_logs();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TRIGGER IF EXISTS actualizar_movimientos_logs ON proc_movimientos;');
        DB::statement('DROP FUNCTION IF EXISTS actualizar_movimientos_logs();');
    }
}
