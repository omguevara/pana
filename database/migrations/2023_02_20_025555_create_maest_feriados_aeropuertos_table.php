<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestFeriadosAeropuertosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('maest_feriados_aeropuertos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->string('nombre', 100);
            $table->unsignedBigInteger('aeropuerto_id');



            $table->string('ip', 20);
            $table->date('fecha_registro');
            $table->unsignedBigInteger('user_id');


            $table->foreign('user_id')->references('id')->on('seg_users');
            $table->foreign('aeropuerto_id')->references('id')->on('maest_aeropuertos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('maest_feriados_aeropuertos');
    }

}
