<?php

use Dompdf\FrameDecorator\Table;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    /**
     * Run the migrations.
     */
    public function up(): void {
        Schema::create('maest_locales', function (Blueprint $table) {
            $table->id();
            $table->string('numero', 30);
            $table->string('metros', 20);
            $table->text('observacion')->nullable();
            $table->unsignedBigInteger('uso_id');
            $table->string('otro_uso')->nullable();
            $table->unsignedBigInteger('aeropuerto_id');
            $table->timestamp('fecha_registro');
            $table->unsignedBigInteger('user_id');
            $table->string('ip', 20);
            $table->boolean('verificado')->nullable()->default(false);
        });
        DB::table('maest_locales')->insert([
            ['numero' => 'AE-19', 'metros' => '20,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DI-05', 'metros' => '3,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DI-06', 'metros' => '7,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DI-08', 'metros' => '7,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DI-10', 'metros' => '3,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DN-01', 'metros' => '138,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DN-09', 'metros' => '22,00', 'uso_id' => '13', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DN-15', 'metros' => '44,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DN-16', 'metros' => '153,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'EI-05A', 'metros' => '4,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'EN-01', 'metros' => '160,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'EN-05', 'metros' => '10,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'FC-07', 'metros' => '49,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'FC-08', 'metros' => '49,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'HN-13', 'metros' => '84,26', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'HN-18', 'metros' => '110,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'HN-32A', 'metros' => '3,00', 'uso_id' => '26', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'HN-32B', 'metros' => '3,00', 'uso_id' => '26', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'IH-01', 'metros' => '42,00', 'uso_id' => '6', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'IH-07', 'metros' => '35,00', 'uso_id' => '6', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'PE-00', 'metros' => '5,00', 'uso_id' => '26', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'R-05', 'metros' => '8,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'R-08', 'metros' => '6,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'R-18', 'metros' => '6,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'ZC-12', 'metros' => '449,86', 'uso_id' => '13', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'ZC-40', 'metros' => '960,00', 'uso_id' => '22', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'ZT-04', 'metros' => '45,00', 'uso_id' => '13', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'EI-01,02', 'metros' => '20,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'HN-30', 'metros' => '24,00', 'uso_id' => '13', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'FC-09', 'metros' => '22,56', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DN-05', 'metros' => '8,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'R-04', 'metros' => '7,00', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'HN-06', 'metros' => '9,18', 'uso_id' => '11', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DI-11', 'metros' => '3,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1'],
            ['numero' => 'DI-12', 'metros' => '2,00', 'uso_id' => '7', 'aeropuerto_id' => '17', 'fecha_registro' => now(), 'user_id' => '1', 'ip' => '127.0.0.1']
            
            
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void {
        Schema::dropIfExists('maest_locales');
    }
};
