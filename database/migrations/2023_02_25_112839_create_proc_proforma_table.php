<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcProformaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_proforma', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha_proforma');

            $table->unsignedBigInteger('cliente_id');
            $table->unsignedBigInteger('aeronave_id');
            $table->unsignedBigInteger('aeropuerto_id');
            $table->unsignedBigInteger('factura_id')->nullable();
            $table->string('nro_proforma');
            $table->string('nro_factura')->nullable();
            
            


            $table->decimal('tasa_dolar', 10, 4);
            $table->decimal('tasa_euro', 10, 4);
            $table->decimal('tasa_petro', 10, 4);




            $table->string('formato_proforma');
            $table->text('observacion')->nullable();
            $table->boolean('anulada')->default(false);
            $table->boolean('pagado')->default(false);
            

            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);

            $table->foreign('cliente_id')->references('id')->on('maest_clientes');
            $table->foreign('user_id')->references('id')->on('seg_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_proforma');
    }
}
