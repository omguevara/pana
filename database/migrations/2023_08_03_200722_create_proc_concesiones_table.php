<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('proc_concesiones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('local_id'); 
            $table->unsignedBigInteger('empresa_id')->nullable();
            $table->unsignedBigInteger('actividad_id');
            $table->string('nombre_empresa')->nullable();
            $table->string('otra_actividad', 100)->nullable();
            $table->string('ingresos_brutos', 20);
            $table->string('canon_fijo', 30);
            $table->string('convenio_euro_integrado', 20);
            $table->date('fecha_inicio');
            $table->date('fecha_fin');
            $table->unsignedBigInteger('user_id');
            $table->string('cedula', 20);
            $table->string('nombre', 60);
            $table->string('telefono');
            $table->string('correo')->nullable();
            
            $table->string('cedula2', 20)->nullable();
            $table->string('nombre2', 60)->nullable();
            $table->string('telefono2')->nullable();
            $table->string('correo2')->nullable();
            
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);
        });
        /*
        DB::table('proc_concesiones')->insert([
            
        ]);
         * 
         */
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('proc_concesiones');
    }
};
