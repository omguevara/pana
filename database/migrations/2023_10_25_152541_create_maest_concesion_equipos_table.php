<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMaestConcesionEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_concesiones_equipos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
        });
        
        DB::table('maest_concesiones_equipos')->insert([ 
            ['nombre' => 'SIN CONTRATO'],
            ['nombre' => 'ALIANZA IKARO'],
            ['nombre' => 'BAER'],
            ['nombre' => 'CONVIASA'],
            ['nombre' => 'AEROPOSTAL'],
            ['nombre' => 'SOLAR CARGO'],
            ['nombre' => 'VERSECA'],
            ['nombre' => 'VOR'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_concesiones_equipos');
    }
}
