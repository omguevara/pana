<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcRegistroOnlineTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('proc_registro_online', function (Blueprint $table) {
            $table->id();
            $table->timestamp('fecha_registro');
            $table->decimal('tasa_petro', 10,2);
            $table->decimal('tasa_euro', 10,2);
            $table->unsignedBigInteger('piloto_id');
            $table->unsignedBigInteger('tipo_id'); //Nacional Internacional
            $table->date('fecha');
            $table->string('hora', 15);
            $table->unsignedBigInteger('aeronave_id');
            $table->integer('pasajeros');
            
            $table->unsignedBigInteger('origen_id');
            $table->unsignedBigInteger('destino_id');

            $table->boolean('procesado_origen')->default(false);
            $table->boolean('procesado_destino')->default(false);
            
            $table->boolean('visto_origen')->default(false);
            $table->boolean('visto_destino')->default(false);
            
            
            
            $table->text('observacion')->nullable();
            $table->string('img', 20)->nullable();
            

            $table->text('prodservicios_extra')->nullable();



            $table->foreign('origen_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('destino_id')->references('id')->on('maest_aeropuertos');
            $table->foreign('aeronave_id')->references('id')->on('maest_aeronaves');
            $table->foreign('piloto_id')->references('id')->on('maest_pilotos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('proc_registro_online');
    }

}
