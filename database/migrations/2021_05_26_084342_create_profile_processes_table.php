<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProfileProcessesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seg_profile_processes', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date_register')->nullable();

            $table->unsignedBigInteger('process_id');
            $table->unsignedBigInteger('profile_id');
            $table->string('actions', 200);

            $table->foreign('process_id')->references('id')->on('seg_processes');
            $table->foreign('profile_id')->references('id')->on('seg_profiles');
        });

        $sql = "
                INSERT INTO seg_profile_processes(date_register, process_id, profile_id, actions)
                SELECT now(), id, 1, actions
                FROM seg_processes 
                ";

        DB::insert($sql);

        DB::table('seg_profile_processes')->insert([

            //Parametrización
            [
                "date_register" => now(),
                "process_id" => 4,
                "profile_id" => 2,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 8,
                "profile_id" => 2,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 3,
                "profile_id" => 2,
                "actions" => ""
            ],
            //Usuarios
            [
                "date_register" => now(),
                "process_id" => 11,
                "profile_id" => 3,
                "actions" => ""
            ],
            [
                "date_register" => now(),
                "process_id" => 2,
                "profile_id" => 3,
                "actions" => "create|edit|disable|active|change_password"
            ],
            [
                "date_register" => now(),
                "process_id" => 1,
                "profile_id" => 3,
                "actions" => "create|edit|disable|active|permissions"
            ],
            [
                "date_register" => now(),
                "process_id" => 9,
                "profile_id" => 3,
                "actions" => "kill"
            ],
            [
                "date_register" => now(),
                "process_id" => 10,
                "profile_id" => 3,
                "actions" => "detail"
            ],
            //OPERACIONES AEROPUERTOS
            [
                "date_register" => now(),
                "process_id" => 17,
                "profile_id" => 4,
                "actions" => "create"
            ],
       
            [
                "date_register" => now(),
                "process_id" => 18,
                "profile_id" => 4,
                "actions" => "see|edit|disable|position"
            ],
            //OPERACIONES BAER
            [
                "date_register" => now(),
                "process_id" => 17,
                "profile_id" => 7,
                "actions" => "create"
            ],
            [
                "date_register" => now(),
                "process_id" => 18,
                "profile_id" => 7,
                "actions" => "see|edit|disable|position"
            ],
            [
                "date_register" => now(),
                "process_id" => 14,
                "profile_id" => 7,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 7,
                "profile_id" => 7,
                "actions" => "create|edit|disable|active"
            ],
            //RECAUDACION AEROPUERTO
            [
                "date_register" => now(),
                "process_id" => 19,
                "profile_id" => 5,
                "actions" => "create"
            ],
            //RECAUDACION BAER
            [
                "date_register" => now(),
                "process_id" => 19,
                "profile_id" => 8,
                "actions" => "create"
            ],
            [
                "date_register" => now(),
                "process_id" => 3,
                "profile_id" => 8,
                "actions" => ""
            ],
            [
                "date_register" => now(),
                "process_id" => 4,
                "profile_id" => 8,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 5,
                "profile_id" => 8,
                "actions" => "create|edit|disable|ubicaciones|lugares|puestos|active|holidays"
            ],
            [
                "date_register" => now(),
                "process_id" =>6,
                "profile_id" => 8,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 7,
                "profile_id" => 8,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 8,
                "profile_id" => 8,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 12,
                "profile_id" => 8,
                "actions" => "create|edit|disable|active|add_points"
            ],
            [
                "date_register" => now(),
                "process_id" => 13,
                "profile_id" => 8,
                "actions" => "create|delete"
            ],
            [
                "date_register" => now(),
                "process_id" => 14,
                "profile_id" => 8,
                "actions" => "create|edit|disable|active"
            ],
            [
                "date_register" => now(),
                "process_id" => 15,
                "profile_id" => 8,
                "actions" => "create|edit"
            ],
            [
                "date_register" => now(),
                "process_id" => 16,
                "profile_id" => 8,
                "actions" => "create"
            ],
            [
                "date_register" => now(),
                "process_id" => 19,
                "profile_id" => 8,
                "actions" => ""
            ],
            [
                "date_register" => now(),
                "process_id" => 20,
                "profile_id" => 8,
                "actions" => ""
            ],
            [
                "date_register" => now(),
                "process_id" => 21,
                "profile_id" => 8,
                "actions" => ""
            ]
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seg_profile_processes');
    }

}
