<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfTaquillasPuntosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conf_taquillas_puntos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('taquilla_id');
            $table->unsignedBigInteger('banco_id');
            $table->string('serial', 20);
            $table->boolean('activo')->default(true);
            
            
            $table->foreign('taquilla_id')->references('id')->on('conf_taquillas');
            $table->foreign('banco_id')->references('id')->on('maest_bancos');
        });
        
        
        
        DB::table('conf_taquillas_puntos')->insert([
            [
                "taquilla_id" => "1",
                "banco_id" => "1",
                "serial" => "PAGO MOVIL"
                
            ],
            [
                "taquilla_id" => "1",
                "banco_id" => "2",
                "serial" => "TRANSFERENCIA"
                
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conf_taquillas_puntos');
    }
}
