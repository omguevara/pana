<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaestAeropuertosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maest_aeropuertos', function (Blueprint $table) {
            $table->id();
            $table->string('codigo_oaci', 10);
            $table->string('codigo_iata', 10);
            $table->string('nombre', 100);
            $table->boolean('activo')->default(true);
            $table->boolean('pertenece_baer')->default(true);
            
            $table->decimal('iva', 10, 2)->default(16);
            
            $table->string('hora_ocaso', 5)->default('1900');
            $table->string('hora_amanecer', 5)->default('600');
            
            $table->decimal('tasa_euro', 10,2)->default(0);
            $table->decimal('tasa_petro', 10,2)->default(0);
            
            $table->unsignedBigInteger('categoria_id');
            $table->unsignedBigInteger('pais_id')->default(1);
            
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);

            $table->foreign('categoria_id')->references('id')->on('maest_categorias');
            $table->foreign('pais_id')->references('id')->on('conf_paises');
            $table->foreign('user_id')->references('id')->on('seg_users');
            
        });
        
         DB::table('maest_aeropuertos')->insert([
            [
                'codigo_oaci' => 'SVMG',
                'codigo_iata' => 'PMV',
                'nombre' => 'G/J SANTIAGO MARIÑO',
                'categoria_id' => '5',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'0',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVBC',
                'codigo_iata' => 'BLA',
                'nombre' => 'GRAL. JOSÉ ANTONIO ANZOÁTEGUI',
                'categoria_id' => '2',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVSA',
                'codigo_iata' => 'SVZ',
                'nombre' => 'GENERAL JUAN VICENTE GÓMEZ',
                'categoria_id' => '3',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVSO',
                'codigo_iata' => '---',
                'nombre' => 'MAYOR BUENAVENTURA VIVAS',
                'categoria_id' => '3',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVCS',
                'codigo_iata' => 'OMZ',
                'nombre' => 'CARACAS',
                'categoria_id' => '2',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVLF',
                'codigo_iata' => 'LFR',
                'nombre' => 'FRANCISCO GARCÍA DE HEVIA',
                'categoria_id' => '3',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVST',
                'codigo_iata' => 'SOM',
                'nombre' => 'DON EDMUNDO BARRIOS',
                'categoria_id' => '4',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVVG',
                'codigo_iata' => 'VIG',
                'nombre' => 'JUAN PABLO PÉREZ ALFONZO',
                'categoria_id' => '2',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVPA',
                'codigo_iata' => 'PYH',
                'nombre' => 'CACIQUE ARAMARE',
                'categoria_id' => '3',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVMD',
                'codigo_iata' => 'MRD',
                'nombre' => 'ALBERTO CARNEVALLI',
                'categoria_id' => '4',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVSE',
                'codigo_iata' => 'SNV',
                'nombre' => 'SANTA ELENA DE UAIRÉN',
                'categoria_id' => '3',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVSP',
                'codigo_iata' => 'SNF',
                'nombre' => 'STTE. NÉSTOR ARIAS',
                'categoria_id' => '4',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVHG',
                'codigo_iata' => 'HIU',
                'nombre' => 'HIGUEROTE',
                'categoria_id' => '4',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVIE',
                'codigo_iata' => 'ICC',
                'nombre' => 'TCNEL. ANDRÉZ SALAZAR MARCANO',
                'categoria_id' => '4',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVIT',
                'codigo_iata' => '---',
                'nombre' => 'PISTA LA TORTUGA',
                'categoria_id' => '7',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVMP',
                'codigo_iata' => '---',
                'nombre' => 'METROPOLITANO',
                'categoria_id' => '4',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVMC',
                'codigo_iata' => 'MAR',
                'nombre' => 'AEROPUERTO INTERNACIONAL LA CHINITA',
                'categoria_id' => '2',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => 'SVBI',
                'codigo_iata' => 'BNS',
                'nombre' => 'AEROPUERTO NACIONAL DE BARINAS',
                'categoria_id' => '3',
                'pais_id' => '1',                'pertenece_baer' => 1,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'codigo_oaci' => '0000',
                'codigo_iata' => '0000',
                'nombre' => 'OTRO',
                'categoria_id' => '1',
                'pais_id' => '1',                'pertenece_baer' => 0,
                'iva'=>'16',
                
                
                'user_id' => 1,
                "fecha_registro" => now(),
                'ip' =>  \Request::ip(),
                
            ]
             
        ]);
    }
 
 





/*
"SVPM";"SCI";241;"PARAMILLO";"";6;1;"2021-03-02 16:56:15.730191-06";FALSE;
"SVAN";"AAO";241;"ANACO";"";6;1;"2021-03-02 16:56:26.610876-06";FALSE;
"SVCL";"CLZ";241;"CALABOZO";"";6;1;"2021-03-02 16:56:37.458892-06";FALSE;
"SVGD";"GTO";241;"VARA DE MARÍA";"";6;1;"2021-03-02 16:58:04.826852-06";FALSE;
"SVPF";"---";241;"PARIAGUÁN";"";6;1;"2021-03-02 16:58:24.827973-06";FALSE;
*/
  
  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maest_aeropuertos');
    }
}
