<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcAeronavesPuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_aeronaves_puestos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('vuelo_id');
            $table->unsignedBigInteger('puesto_id');
            
            $table->timestamp('fecha_entrada');
            $table->timestamp('fecha_salida')->nullable();
            
            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_registro');
            $table->string('ip', 20);
            
            
            $table->foreign('user_id')->references('id')->on('seg_users');
            $table->foreign('puesto_id')->references('id')->on('maest_puestos');
            $table->foreign('vuelo_id')->references('id')->on('proc_vuelos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_aeronaves_puestos');
    }
}
