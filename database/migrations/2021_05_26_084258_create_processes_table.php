<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcessesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seg_processes', function (Blueprint $table) {
            $table->id();
            $table->string('name_process', 50);
            $table->string('description', 200);
            $table->string('icon');
            $table->string('route', 50);
            $table->string('actions', 200);
            $table->integer('order');
            $table->boolean('requiere_fiscal')->default(false);

            $table->unsignedBigInteger('menu_id');

            $table->foreign('menu_id')->references('id')->on('seg_menus');
        });

        DB::table('seg_processes')->insert([
            //1
            [
                'name_process' => 'Perfiles',
                'description' => 'Profiles',
                'icon' => 'user-friends',
                'route' => 'profiles',
                "actions"=>'create|edit|disable|active|permissions',
                "requiere_fiscal"=>false,
                'order' => '1',
                'menu_id' => 1,
            ],
            //2
            [
                'name_process' => 'Usuarios',
                'description' => 'Usuarios',
                'icon' => 'user',
                'route' => 'users',
                "actions"=>'create|edit|disable|active|change_password',
                "requiere_fiscal"=>false,
                'order' => '2',
                'menu_id' => 1,
            ],
            //3
            [
                'name_process' => 'IVA / EURO / PETRO / DOLAR',
                'description' => 'Control de Parametros',
                'icon' => 'tag',
                'route' => 'params',
                "actions"=>'',
                "requiere_fiscal"=>false,
                'order' => '1',
                'menu_id' => 2,
            ],
            //4
            [
                'name_process' => 'Categoría de Aeropuertos',
                'description' => 'Categorías',
                'icon' => 'random',
                'route' => 'categorias',
                "actions"=>'create|edit|disable|active',
                "requiere_fiscal"=>false,
                'order' => '2',
                'menu_id' => 2,
            ],
            //5
            [
                'name_process' => 'Aeropuertos',
                'description' => 'Aeropuertos',
                'icon' => 'paper-plane',
                'route' => 'aeropuertos',
                "actions"=>'create|edit|disable|ubicaciones|lugares|puestos|active|holidays',
                "requiere_fiscal"=>false,
                'order' => '3',
                'menu_id' => 2,
            ],
            //6
            [
                'name_process' => 'Aerolineas',
                'description' => 'Aerolineas',
                'icon' => 'plane',
                'route' => 'aerolineas',
                "actions"=>'create|edit|disable|active',
                "requiere_fiscal"=>false,
                'order' => '4',
                'menu_id' => 2,
            ],
            //7
            [
                'name_process' => 'Aeronaves',
                'description' => 'Aeronaves',
                'icon' => 'plane',
                'route' => 'aeronaves',
                "actions"=>'create|edit|disable|active',
                "requiere_fiscal"=>false,
                'order' => '5',
                'menu_id' => 2,
            ],
            //8
            [
                'name_process' => 'Servicios',
                'description' => 'Productos y Servicios',
                'icon' => 'plane',
                'route' => 'prodservicios',
                "actions"=>'create|edit|disable|active',
                "requiere_fiscal"=>false,
                'order' => '6',
                'menu_id' => 2,
            ],
            //9
            [
                'name_process' => 'Usuarios Conectados',
                'description' => 'Usuarios Conectados',
                'icon' => 'user-check',
                'route' => 'logged_users',
                "actions"=>'kill',
                "requiere_fiscal"=>false,
                'order' => '3',
                'menu_id' => 1,
            ],
            //10
            [
                'name_process' => 'Accesos Restringidos',
                'description' => 'Accesos Restringidos',
                'icon' => 'user-tag',
                'route' => 'restricted_access',
                "actions"=>'detail',
                "requiere_fiscal"=>false,
                'order' => '3',
                'menu_id' => 1,
            ],
            //11
            [
                'name_process' => 'Monitoreo de Usuarios',
                'description' => 'Monitoreo de Usuarios',
                'icon' => 'user-clock',
                'route' => 'access_history',
                "actions"=>'',
                "requiere_fiscal"=>false,
                'order' => '3',
                'menu_id' => 1,
            ],
            //12
            //  Configuraciones
            [
                'name_process' => 'Control de Taquillas',
                'description' => 'Administrar Taquillas',
                'icon' => 'plane',
                'route' => 'taquillas',
                "actions"=>'create|edit|disable|active|add_points',
                "requiere_fiscal"=>false,
                'order' => '1',
                'menu_id' => 6,
            ],
            //13
            [
                'name_process' => 'Feriados',
                'description' => 'Administrar Días Feriados',
                'icon' => 'fa-calendar',
                'route' => 'feriados',
                "actions"=>'create|delete',
                "requiere_fiscal"=>false,
                'order' => '5',
                'menu_id' => 2,
            ],
            //14
            [
                'name_process' => 'Pilotos',
                'description' => 'Administrar Pilotos',
                'icon' => 'user-tie',
                'route' => 'pilotos',
                "actions"=>'create|edit|disable|active',
                "requiere_fiscal"=>false,
                'order' => '5',
                'menu_id' => 2,
            ],
            [ //15
                'name_process' => 'Clientes',
                'description' => 'Administrar Clientes',
                'icon' => 'users',
                'route' => 'clientes',
                "actions"=>'create|edit',
                "requiere_fiscal"=>false,
                'order' => '4',
                'menu_id' => 2,
            ],
            //16
            [
                'name_process' => 'Membresías',
                'description' => 'Administrar Membresías',
                'icon' => 'fa-id-card',
                'route' => 'membresias',
                "actions"=>'create|reporte',
                "requiere_fiscal"=>false,
                'order' => '6',
                'menu_id' => 2,
            ],
            //17
            [
                'name_process' => 'Registro de Operación',
                'description' => 'Registro de los Arribos y Despegues del Aeropuerto',
                'icon' => 'fa-plane',
                'route' => 'vuelos',
                "actions"=>'create',
                "requiere_fiscal"=>false,
                'order' => '1',
                'menu_id' => 3,
            ],
            //18
            [
                'name_process' => 'Consulta de Operación',
                'description' => 'Consulta de los Arribos y Despegues del Aeropuerto',
                'icon' => 'fa-plane',
                'route' => 'get_vuelos',
                "actions"=>'see|edit|disable|position',
                "requiere_fiscal"=>false,
                'order' => '1',
                'menu_id' => 3,
            ],
           //19
            [
                'name_process' => 'Emisión de Proforma',
                'description' => 'Registrar las Aeronaves para su Factura',
                'icon' => 'fa-plane',
                'route' => 'facturar_pospago',
                "actions"=>'',
                "requiere_fiscal"=>false,
                'order' => '2',
                'menu_id' => 4,
            ],
            //20
            [
                'name_process' => 'Emisión de Factura',
                'description' => '',
                'icon' => 'fa-plane',
                'route' => 'emitir_factura',
                "actions"=>'',
                "requiere_fiscal"=>false,
                'order' => '3',
                'menu_id' => 4,
            ],
            //21
            [
                'name_process' => 'Libro de Ventas',
                'description' => '',
                'icon' => 'fa-book',
                'route' => 'facturar_pospago.libro_ventas',
                "actions"=>'',
                "requiere_fiscal"=>false,
                'order' => '3',
                'menu_id' => 4,
            ]
            
            
           
            
            
        ]);
    }
    
   
    
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seg_processes');
    }

}
