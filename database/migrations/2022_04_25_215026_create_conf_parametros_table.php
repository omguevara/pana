<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfParametrosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('conf_parametros', function (Blueprint $table) {
            $table->id();
            $table->string('nombre', 50)->unique();
            $table->string('descripcion', 200);
            $table->string('valor', 50);
            $table->boolean('variable')->default(false);

            $table->unsignedBigInteger('user_id');
            $table->timestamp('fecha_act');
            $table->string('ip', 20);

            $table->foreign('user_id')->references('id')->on('seg_users');
        });

        DB::table('conf_parametros')->insert([
            [
                'nombre' => 'IVA',
                'descripcion' => 'Iva',
                'valor' => '16.00',
                'variable' => true,
                
                'user_id' => 1,
                "fecha_act" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'nombre' => 'TASA_CAMBIO_DOLLAR',
                'descripcion' => 'Tasa de Cambio del Dolar',
                'valor' => '24.35',
                'variable' => true,
                
                'user_id' => 1,
                "fecha_act" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'nombre' => 'TASA_CAMBIO_EURO',
                'descripcion' => 'Tasa de Cambio del Euro',
                'valor' => '25.92',
                'variable' => false,
                
                'user_id' => 1,
                "fecha_act" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'nombre' => 'TASA_CAMBIO_PETRO',
                'descripcion' => 'Tasa de Cambio del Petro',
                'valor' => 1461.42,
                'variable' => false,
                
                'user_id' => 1,
                "fecha_act" => now(),
                'ip' =>  \Request::ip(),
                
            ],
            [
                'nombre' => 'GET_PARAMS_ONLINE',
                'descripcion' => 'Obtiene los Parametros En Línea',
                'valor' => 0,
                'variable' => false,
                
                'user_id' => 1,
                "fecha_act" => now(),
                'ip' =>  \Request::ip(),
                
            ]
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('conf_parametros');
    }

}
