<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProcVuelosCarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_vuelos_carga', function (Blueprint $table) {
            $table->id();
            $table->string('vuelo_id');
            $table->string('tipo_carga_id');
            $table->decimal('peso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_vuelos_carga');
    }
}
