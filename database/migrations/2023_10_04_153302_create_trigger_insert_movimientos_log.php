<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTriggerInsertMovimientosLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::statement('
            CREATE OR REPLACE FUNCTION insertar_movimiento_log()
            RETURNS TRIGGER AS $$
            BEGIN
                INSERT INTO proc_movimientos_logs (aeropuerto_id, aerolinea_id, tipo_operacion_id, numero_vuelo, origen_destino_id, fecha, hora, puerta, observaciones, activo, user_id, ip, fecha_creacion)
                VALUES (NEW.aeropuerto_id, NEW.aerolinea_id, NEW.tipo_operacion_id, NEW.numero_vuelo, NEW.origen_destino_id, NEW.fecha, NEW.hora, NEW.puerta, NEW.observaciones, NEW.activo, NEW.user_id, NEW.ip, NEW.fecha_creacion);
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
        ');
        DB::statement('
            CREATE TRIGGER insertar_movimiento_log
            AFTER INSERT ON proc_movimientos
            FOR EACH ROW
            EXECUTE FUNCTION insertar_movimiento_log();
        ');
    }
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement('DROP TRIGGER IF EXISTS insertar_movimiento_log ON proc_movimientos;');
        DB::statement('DROP FUNCTION IF EXISTS insertar_movimiento_log();');
    }
}
