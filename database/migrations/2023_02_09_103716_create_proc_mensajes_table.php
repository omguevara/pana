<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_mensajes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('de_id');
            $table->unsignedBigInteger('para_id');
            $table->text('mensaje');
            
            $table->boolean('visto')->default(false);
            
            
            
            
            $table->string('ip', 20);
            $table->timestamp('fecha_registro')->default('now()');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_mensajes');
    }
}
