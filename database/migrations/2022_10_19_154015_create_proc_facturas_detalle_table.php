<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProcFacturasDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proc_facturas_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('factura_id');
            $table->unsignedBigInteger('prodservicio_id');
            $table->decimal('cantidad', 10, 2);
            $table->string('codigo', 20);
            $table->unsignedBigInteger('tipo_id'); //1: TASA 2: DOSA 3: SERVICIO ADICIONAL
            $table->string('descripcion', 200);
            $table->string('nomenclatura', 100);
            $table->string('formula', 200);
            $table->decimal('precio', 10, 4);
            $table->decimal('iva', 10, 2);
            
            
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proc_facturas_detalle');
    }
}
