create temp table a as
select id, aeronave_id, observaciones_facturacion
from proc_vuelos
where observaciones_facturacion is not null and observaciones_facturacion != ''
order by id desc;

create index index_a on a(aeronave_id);

create temp table  b as select distinct on (aeronave_id) * from a;



update proc_vuelos set observaciones_facturacion = (select observaciones_facturacion from b where b.aeronave_id =
proc_vuelos.aeronave_id)  where observaciones_facturacion is null or observaciones_facturacion = '';