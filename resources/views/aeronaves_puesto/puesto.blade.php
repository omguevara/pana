

<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">


    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Histórico de Puestos Asignados')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"aeronaves_puestos", "targe"=>"_blanck", 'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @method("post")
        

        <div id="card-body-main" class="card-body ">
            <div class="row">


                <div  class="col-sm-12 col-md-3   ">
                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Fechas:</b></span>
                        </div>
                        {{Form::text("rango", '01/'.date('m/Y')." - 30/".date('m/Y'), ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                        <div class="input-group-append">
                            <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>

                    </div>
                </div>
                <div  class="col-sm-12 col-md-1 mt-2   ">
                    {{Form::button('<li class="fa fa-search"></li> ',  [ "type"=>"button", "onClick"=>"getDataResult()" ,"class"=>" btn-sm btn btn-primary"])}}    

                </div>
                <div  class="col-sm-12 col-md-1 mt-2   ">
                    {{Form::button('<li class="fa fa-file-excel"></li> ',  ["onClick"=>"downEx()",  "type"=>"button", "class"=>" btn-sm btn btn-success"])}}    

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 mt-4" >

                    <table id="listaResult" class="table table-bordered table-hover"  >
                        <thead>
                            <tr>

                                <td ><b>Aeronave</b></td>
                                <td ><b>Puesto</b></td>
                                <td ><b>Fecha Entrada</b></td>
                                <td ><b>Fecha Salida</b></td>

                            </tr>
                        </thead>
                    </table>

                </div>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>



















    <script type="text/javascript">
        var table1;



        $(document).ready(function () {
            table1 = $('#listaResult').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,

                serverSide: true,
                processing: true,

                ajax:
                        {
                            "url": "{{route('aeronaves_puestos')}}",
                            "method": "post",
                            data: function (d) {
                                d._token = "{{csrf_token()}}",
                                d._method = "PUT"        
                                

                            }
                        }
                ,

                columns: [
                    {data: 'aeronave.full_nombre_tn'},
                    {data: 'puesto.nombre'},
                    {data: 'fecha_entrada2'},
                    {data: 'fecha_salida2'}
                ]
                , language: {
                    url: "{{url('/')}}/plugins/datatables/es.json"
                }
            });

            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });







            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


        });



    </script>

</div>
