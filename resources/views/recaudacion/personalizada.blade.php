<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <style>
        tr:hover td{
            background-color: #ffe69b ;
        }
        tr.selected td{
            background-color: #DFF2BF;
        }
        li.filaFactura:hover{
            background-color: #ffe69b ;
        }
    </style>

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Emitir Proforma Personalizada')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"facturar_pospago.proforma_personalizada",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @method("post")
        {{Form::select('prod_servd_remove[]', [], "", ["style"=>"display:none", "id"=>"prod_servd_remove" ,"multiple"=>"multiple"])}}

        <div id="card-body-main" class="card-body ">
            <div class="row">


                <div class="col-sm-3 ">
                    <div class="row ">
                        <div class="col-sm-12 border-right">
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeropuerto:</b></span>
                                </div>
                                {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["required"=>'required',"placeholder"=>"Seleccione"  , "class"=>"form-control required select2 ", "id"=>"aeropuerto_id" ])}}

                            </div>

                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeronave:</b></span>
                                </div>
                                {{Form::select('aeronave_id', $Aeronaves, "", [    "placeholder"=>"Seleccione", "class"=>"form-control required select2 ", "id"=>"aeronave_id" ])}}

                            </div>


                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Doc')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1 cliente", "id"=>"type_document" ,"required"=>"required"])}}
                                    {{Form::hidden('cliente_id', "", [ "id"=>"cliente_id"])}}
                                    {{Form::hidden("temp_alt","0", ["class"=>"temp_alt", "id"=>"temp_alt"])}}
                                </div>
                                {{Form::text("document", "", ["onBlur"=>"findDato2()", "data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}    
                                <div class="input-group-append" >
                                    <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i  id="b_s" class="fa fa-search"></i></div>
                                </div>
                            </div>

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Responsable:</b></span>
                                </div>
                                {{Form::text("razon", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
                            </div>

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                                </div>
                                {{Form::text("phone", "", ["required"=>"required", "class"=>"form-control required phone cliente", "id"=>"phone", "placeholder"=>__('Phone')])}}    
                            </div>

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                                </div>
                                {{Form::text("direccion", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                            </div>

                            <div class="input-group mt-2 "  >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Tipo de Vuelo:</b></span>
                                </div>
                                {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], "1" , ["data-msg-required"=>"Campo Requerido", "class"=>"form-control required  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                            </div>

                            {{-- <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span id="labelCantPax" class="input-group-text"><b>Pasajeros Embarcados:</b></span>
                                </div>
                                {{Form::number("pax", "0", ["data-msg-required"=>"Campo Requerido",  "min"=>"0", "max"=>"999", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax", "placeholder"=>__('Cant. PAX')])}}    
                            </div> --}}
                            
                            
                            <div class="input-group mt-2 "  >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>¿Cobrar Estacionamiento?:</b></span>
                                </div>
                                {{Form::select('cobrar_estacionamiento', ["1"=>"Si", "0"=>"No"], "0" , ["data-msg-required"=>"Campo Requerido", "class"=>"form-control ", "onChange"=>"setAeropuerto(this.value)" ,"id"=>"cobrar_estacionamiento" ,"required"=>"required"])}}
                            </div>
                            
                            
                            {{Form::hidden("hora_llegada_est", date('d/m/Y H:i'), [  "id"=>"hora_llegada_est" ])}}
                            {{Form::hidden("hora_salida_est", date('d/m/Y H:i'),  [  "id"=>"hora_salida_est" ])}}
                            
                            <div class="form-outline row mt-2">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <label class="form-label" for="observacion">
                                        Observación
                                    </label>
                                    <div>
                                        {!! Form::textarea('observacion', null, ["rows"=>"4", 'class' => 'form-control', 'id' => 'observacion']) !!}
                                    </div>
                                </div>
                            </div>
                            
                            
                        </div>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-12 border-right">
                            <div class="input-group mt-2 ">

                                <label>
                                    <input checked name="llegada" id="llegada" type="checkbox" onClick="cls(this, 'in')" />
                                    Datos de Llegada
                                </label>
                            </div>
                            <div class="input-group mt-2"  id="iconDate" >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Fecha de Llegada:</b></span>
                                </div>
                                {{Form::text("fecha_llegada", date("d/m/Y"), ["onBlur"=>"setAeropuerto($('#cobrar_estacionamiento').val())", "data-msg-required"=>"Campo Requerido", "class"=>"form-control in  date1 required ", "id"=>"fecha_llegada" ,"required"=>"required"  ])}}

                            </div>

                            <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Hora de Llegada: <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                </div>
                                {{Form::text("hora_llegada", date("H:i"), ["onBlur"=>"setAeropuerto($('#cobrar_estacionamiento').val())", "data-msg-required"=>"Campo Requerido", "class"=>"form-control in  timer required ", "id"=>"hora_llegada" ,"required"=>"required"])}}
                            </div>

                            <div class="row mt-2">
                                <div  class="col-md-12 ">
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>LLeva Carga:</b></span>
                                        </div>
                                        {{Form::select('lleva_carga_llegada', ["1"=>"SI", "0"=>"NO"], "0" , [  "onChange"=>'checkTipoCargaLL(this)', "class"=>"form-control", "id"=>"lleva_carga_llegada"])}}
                                        {{Form::text('carga_ll', null, ['class' => 'form-control money text-right', 'id' => 'carga_ll', 'disabled' => 'disabled']) }}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="input-group mt-4"  >
                                <div class="input-group-prepend">
                                    <span><b>&nbsp;</b></span>
                                </div>
                                <div >&nbsp;</div>
                            </div>

                            <div class="input-group mt-2 ">
                                <label>Servicios Adicionales de la Llegada</label>
                            </div>
                            <table style="width: 100%">
                                @foreach($SERV_ESP as $key=>$value)
                                <tr id="tr_in_{{str_replace('.', '', $key)}}" class="tr_in">

                                    <td style="width: 70%" ><input onClick="addServs(this, 'in')" class="in" data-code="{{str_replace('.', '', $key)}}" type="checkbox" /> COD. ({{$key}}) {{' '.Upper($value)}}</td>
                                    <td style="width: 30%"><input disabled class="form-control in" value="0" name="in[{{$key}}]" id="in_{{str_replace('.', '', $key)}}" type="number" /></td>
                                </tr>
                                @endforeach
                            </table>
                        </div>


                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-12 border-right">
                            <div class="input-group mt-2 ">

                                <label><input name="salida" id="salida" type="checkbox" checked onClick="cls(this, 'ou')" /> 
                                    Datos de Salida
                                </label>
                            </div>
                            <div class="input-group mt-2"  id="iconDate2" >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Fecha de Salida:</b></span>
                                </div>
                                {{Form::text("fecha_salida", date("d/m/Y"), ["onBlur"=>"setAeropuerto($('#cobrar_estacionamiento').val())", "data-msg-required"=>"Campo Requerido", "class"=>"form-control ou  date1  required ", "id"=>"fecha_salida" ,"required"=>"required" ])}}

                            </div>

                            <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Hora de Salida: <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                </div>
                                {{Form::text("hora_salida", date("H:i"), ["onBlur"=>"setAeropuerto($('#cobrar_estacionamiento').val())", "data-msg-required"=>"Campo Requerido", "class"=>"form-control ou timer required ", "id"=>"hora_salida" ,"required"=>"required"])}}
                            </div>
                            <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span id="labelCantPax" class="input-group-text"><b>Pasajeros Embarcados:</b></span>
                                </div>
                                {{Form::number("pax", "0", ["data-msg-required"=>"Campo Requerido",  "min"=>"0", "max"=>"999", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax", "placeholder"=>__('Cant. PAX')])}}    
                            </div>

                            <div class="row mt-2">
                                <div  class="col-md-12 ">
                                    <div class="input-group ">
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>LLeva Carga:</b></span>
                                        </div>
                                        {{Form::select('lleva_carga_salida', ["1"=>"SI", "0"=>"NO"], "0" , [  "onChange"=>'checkTipoCargaSS(this)', "class"=>"form-control", "id"=>"lleva_carga_salida"])}}
                                        {{Form::text('carga_ss', null, ['class' => 'form-control money text-right', 'id' => 'carga_ss', 'disabled' => 'disabled']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mt-2 ">

                                <label>Servicios Adicionales de la Salida</label>
                            </div>
                            <table style="width: 100%">
                                @foreach($SERV_ESP as $key=>$value)
                                <tr id="tr_ou_{{str_replace('.', '', $key)}}" class="tr_ou">

                                    <td style="width: 70%" ><input onClick="addServs(this, 'ou')" class="ou" data-code="{{str_replace('.', '', $key)}}" type="checkbox" /> COD. ({{$key}}) {{' '.Upper($value)}}</td>
                                    <td style="width: 30%"><input disabled class="form-control ou" value="0"  name="ou[{{$key}}]" id="ou_{{str_replace('.', '', $key)}}" type="number" /></td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <button class="btn btn-info btn-block" id="verServicios" onClick="searchProdservs()" type="button"><span><li class="fa fa-file-invoice-dollar"></li> Ver Servicios</span></button>

                        <div id="facturaFiscal" class="row" style=" overflow-x: hidden; overflow-y: auto; height: 100%; width: 97%; font-size: 14px;margin-left: 10px;" >
                            <ul class="" style=" border-radius: 0.2em; list-style: none; padding: 3px;   width:100%">
                                <li  >
                                    <ul class=" list-unstyled" style="list-style: none; font-size: 12px; padding-left:0px ; font-family: ROBOTO;   ">
                                        <li class="row" style="margin-left: 0px">
                                            <span class="col-md-1" style="padding-left: 0px; padding-right: 0px;">&nbsp;</span>
                                            <span class="col-md-2" style="padding-left: 0px; padding-right: 0px;">CANT</span>
                                            <span class="col-md-6" style="padding-left: 0px; padding-right: 0px;">DESC</span>
                                            <span class="col-md-3" style="padding-left: 0px; padding-right: 0px;">SUBTOTAL</span>
                                        </li>
                                        <li style="width:100%; border-top:1px dashed " ></li>
                                        <div  id="detalleFact">
                                            &nbsp;

                                        </div>



                                        <li style="width:100%; border-top:1px dashed " ></li>
                                        <li class="row" style="margin-left: 0px">
                                            <span class="col-md-8" style="padding-left: 0px; padding-right: 0px;">BASE IMPONIBLE</span>
                                            <span class="col-md-4" style="padding-left: 0px; text-align: right;" id="span_base">Bs 0,00</span>
                                            <span class="col-md-8" style="padding-left: 0px; padding-right: 0px;">EXENTO</span>
                                            <span class="col-md-4" style="padding-left: 0px; text-align: right;" id="span_exento">Bs 0,00</span>
                                            <span class="col-md-8" style="padding-left: 0px; padding-right: 0px;">SUBTOTAL</span>
                                            <span class="col-md-4" style="padding-left: 0px; text-align: right;" id="span_subtotal">Bs 0,00</span>
                                            <span class="col-md-8" style="padding-left: 0px; padding-right: 0px;">IVA(16%)</span>
                                            <span class="col-md-4" style="padding-left: 0px; text-align: right;" id="span_iva">Bs 0,00</span>
                                        </li>

                                        <li style="width:100%; border-top:1px dashed " ></li>

                                        <li style="font-size: 14px; margin-left: 0px" class="row">
                                            <span class="col-md-3" style="padding-left: 0px;"><strong>TOTAL</strong></span>
                                            <span class="col-md-9 " style="padding-right: 25px; text-align: right;" id="" ><strong id="totalFactura">Bs 0,00</strong></span>
                                        </li>



                                    </ul>
                                </li>



                            </ul>

                        </div>
                        <button class="btn btn-success btn-block" onClick="saveProf()" type="button"><span><li class="fa fa-save"></li> Guardar</span></button>

                    </div>
                    <div class="row" id="info-membresia"></div>
                </div>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>

    <div class="modal fade" id="modal-princ-emails"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Enviar Proforma por Correo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-3 " >
                            <div  class="row">
                                <label>Ingrese el Correo:</label>
                                <input type="text" id="emails_to_send" name="emails_to_send" class="form-control" value="[]" />
                            </div>  
                            
                            <div  class="row ">
                                <div class="col-sm-12 col-md-12 text-center mt-2" >
                                    {{Form::button('<li class="fa fa-mail-bulk"></li> '.__("Enviar"),  ["onClick"=>"sendProfEmail()", "type"=>"button", "class"=>"btn btn-primary" ])}}    
                                </div>
                                
                            </div>  
                            
                            
                        </div>
                        <div class="col-sm-12 col-md-9 " >
                            <div id="panelIframeProforma" class="row">

                            </div>  
                        </div>

                    </div>
                    

                </div>
                <div class="modal-footer justify-content-between">

                    
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    
    
    
    
    <script type="text/javascript">
        PRODSERVS = null;
        var idFactEmail = "";
        var searchClicked = false;
        $(document).ready(function () {

            $("#aeronave_id").on("change", function(){
                $("#frmPrinc1").prepend(LOADING);
                $("#info-membresia").empty();
                $.get("{{route('membresias.verify')}}/", {id: $(this).val()}, function (response) {
                    $(".overlay-wrapper").remove();
                    if (response.type == 'success') {
                        $("#info-membresia").append(response.html)
                    }
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });


            });

            $('#emails_to_send').multiple_emails({position: "bottom"});
            
            $("#modal-princ-emails").on('shown.bs.modal', function() {
                $(".multiple_emails-input").focus();
            });
            

            $(".select2").select2();
            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
            $('.date1').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });
        function checkTipoCargaLL(obj){
            if (obj.value == 0){
                $('#carga_ll').removeClass('required').val('').prop('disabled', true);
            }else{
                $('#carga_ll').addClass('required').prop('disabled', false);

            }
        }
        function checkTipoCargaSS(obj){
            if (obj.value == 0){
                $('#carga_ss').removeClass('required').val('').prop('disabled', true);
            }else{
                $('#carga_ss').addClass('required').prop('disabled', false);

            }
        }
        function addServs(obj, op) {
            $("#tr_" + op + "_" + $(obj).data("code")).toggleClass('selected');
            input = $('#' + op + '_' + $(obj).data("code"))
                    .prop('disabled', !$(obj).is(":checked"));
            if ($(obj).is(":checked")) {


                input.val(1)
                        .focus()
                        .select();
            } else {
                input.val(0)
                        .focus()
                        .select();
            }
        }
        function cls(obj, op) {

            $('.tr_' + op).removeClass('selected');

            if ($(obj).is(":checked")) {
                $("." + op + ":text").prop('disabled', false);
                $('.' + op + ":checkbox").prop("disabled", false);
                if (op == 'ou') {
                    $("#pax").prop("disabled", false).val(0);
                }
            } else {
                $("." + op + ":text").prop('disabled', true);
                $('.' + op + ":checkbox").prop("checked", false).prop("disabled", true);
                $("." + op + "[type=number]").val(0).prop('disabled', true);
                if (op == 'ou') {
                    $("#pax").prop("disabled", true).val(0);
                }
            }
        }


        function findDato2() {
            if ($("#frmPrinc1 #document").valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#frmPrinc1 #type_document").val() + "/" + $("#frmPrinc1 #document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $("#frmPrinc1 #cliente_id").val(response.data.crypt_id);
                        $("#frmPrinc1 #razon").val(response.data.razon_social);
                        $("#frmPrinc1 #phone").val(response.data.telefono);
                        $("#frmPrinc1 #direccion").val(response.data.direccion);
                        $("#frmPrinc1 #razon, #frmPrinc1 #phone, #frmPrinc1 #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#frmPrinc1 #razon, #frmPrinc1 #phone, #frmPrinc1 #correo, #frmPrinc1 #direccion").prop("readonly", false);
                        $("#frmPrinc1 #cliente_id, #frmPrinc1 #razon, #frmPrinc1 #phone, #frmPrinc1 #direccion").val("");
                        $("#frmPrinc1 #razon").focus();
                    }
                    $("#frmPrinc1").valid();
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#frmPrinc1 #razon, #frmPrinc1 #phone, #frmPrinc1 #direccion").prop("readonly", true);
                    $("#frmPrinc1 #cliente_id, #frmPrinc1 #razon, #frmPrinc1 #phone, #frmPrinc1 #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }
        }

        function searchProdservs() {
            searchClicked = true;
            $("#verServicios").removeClass("info-pulse");
            if ($("input, select").not(".cliente").valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post("{{route('get_total_serv2')}}", $("#frmPrinc1").serialize(), function (response) {
                        $('#temp_alt').val(response.data2);
                    $(".overlay-wrapper").remove();
                    $("#prod_servd_remove").children().remove();
                    PRODSERVS = response.data;

                    displayItems();
                    Toast.fire({
                        icon: 'success',
                        title: '{{("Servicios añadidos correctamente")}}'
                    });
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }else{
                Toast.fire({
                    icon: 'error',
                    title: '{{("Por favor ingrese los datos del formulario")}}'
                });
            }
        }


        function deleteItem(fil) {
            $("." + fil).remove();

            for (i in PRODSERVS) {
                if (PRODSERVS[i]['crypt_id'] == fil) {
                    $("#prod_servd_remove").append('<option selected value="' + PRODSERVS[i]['codigo'] + '">' + PRODSERVS[i]['codigo'] + '</option>');
                    PRODSERVS.splice(i, 1);
                }
            }

            displayItems();
            Toast.fire({
                icon: 'success',
                title: '{{("Servicio removido correctamente")}}'
            });
        }

        function displayItems() {
            $(".filaFactura").remove();
            sum = 0;
            base_imponible = 0;
            excento = 0;
            iva = 0;

            $("#detalleFact").html("");
            for (i in PRODSERVS) {
                sum += parseFloat(PRODSERVS[i]['precio'], 10);
                mult = parseFloat(PRODSERVS[i]['precio'], 10);
                cant = PRODSERVS[i]['cant'];

                if (PRODSERVS[i]['iva_aplicado'] > 0) {
                    base_imponible += mult;
                    iva += mult * PRODSERVS[i]['iva_aplicado'] / 100;
                } else {
                    excento += parseFloat(mult, 10);

                }

                tabla = '<li class="row filaFactura ' + PRODSERVS[i]['crypt_id'] + ' " style="margin-left: 0px;  cursor:pointer; border-top:1px dashed"  title="' + PRODSERVS[i]['formula2'] + '" >';
                tabla += '<span class="col-md-1 item" style="padding-left: 0px; padding-right: 0px;"><span onclick="deleteItem(\'' + PRODSERVS[i]['crypt_id'] + '\')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></span>';
                tabla += '<span class="col-md-2 item" style="padding-left: 0px; padding-right: 0px;">' + muestraFloat(cant) + '</span>';
                tabla += '<span class="col-md-6 item" style="padding-left: 0px; padding-right: 0px;">' + PRODSERVS[i]['full_descripcion'] + ' ' + PRODSERVS[i]['iva2'] + '</span>';
                tabla += '<span class="col-md-3 item" style="padding-left: 0px; padding-right: 0px; text-align:center">' + muestraFloat(mult) + '</span>';



                tabla += '</li>';
                $("#detalleFact").append(tabla);
            }
            TOTAL_FACTURA = sum;
            MontoGlobalBs = sum * VALOR_EURO;

            iva = parseFloat(iva.toFixed(2), 10);

            $("#span_base").html(muestraFloat(base_imponible));
            $("#span_exento").html(muestraFloat(excento));
            $("#span_subtotal").html(muestraFloat(sum));
            $("#span_iva").html(muestraFloat(iva));
            $("#totalFactura").html(muestraFloat(sum + iva));

        }
        
        
                            
                              
                              
        
        function setAeropuerto(valor){
            if (valor==1){
                $("#hora_llegada_est").val($("#fecha_llegada").val()+' '+$("#hora_llegada").val());
                $("#hora_salida_est").val($("#fecha_salida").val()+' '+$("#hora_salida").val());
            }else{
                $("#hora_llegada_est").val("{{date('d/m/Y H:i')}}");
                $("#hora_salida_est").val("{{date('d/m/Y H:i')}}");
            }
        
        }
        
        
        function saveProf() {
            if ($("#frmPrinc1").valid()) {
                if (searchClicked == true){
                    $("#frmPrinc1").prepend(LOADING);
                    $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status != 0) {
                            $("#modal-princ-emails").modal("show");
                            $("#panelIframeProforma").html('<iframe id="seeProfDef" onload="clsDataResp();" name="seeProfDef"  style="zoom: 0.75; -moz-transform: scale(0.75); -moz-transform-origin: 0 0; -o-transform: scale(0.75); -o-transform-origin: 0 0; -webkit-transform: scale(0.75); -webkit-transform-origin: 0 0; width:100%; height: 800px; border:none" src="{{url("view-proforma")}}/'+response.data.crypt_id+'"></iframe>'); 
                            $(".filaFactura").remove();
                            $("#span_base, #span_exento, #span_subtotal, #span_iva, #totalFactura").html(muestraFloat(0));
                            
                            $("#salida, #llegada").prop("checked", false);
                            cls($("#salida"), 'in');
                            cls($("#llegada"), 'in');
                            $("#aeronave_id, #document, #razon, #phone, #direccion, #observacion").val("");
                            $("#aeronave_id").trigger('change');
                            
                            
                        }else{
                            $("#hora_salida, #hora_salida_est").addClass("info-pulse");
                            setTimeout(function() {
                                $("#hora_salida, #hora_salida_est").removeClass("info-pulse");
                            }, 8000);
                        }

                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Consultar los Datos')}}"
                        });
                    });
                }else{
                    $("#verServicios").addClass("info-pulse");
                    Toast.fire({
                        icon: 'error',
                        title: 'Debe hacer click en ver servicios antes de guardar.'
                    });
                }
            }else{
                Toast.fire({
                    icon: 'error',
                    title: '{{("Por favor ingrese los datos del formulario")}}'
                });
            }
        }
        function buscarVuelosFact() {
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $("#frmPrinc1 [name=_method]").val("PUT");
                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    
                    if (response.status == 1){
                    
                        $(".overlay-wrapper").remove();
                        $(".filasFacturas").remove();
                        sum = 0;
                        $("#vuelos").html('');

                        for (i in response.data) {
                            if (response.data[i].total > 0) {
                                $("#all").prop("checked", true);
                                

                                tabla = '<tr style="cursor:pointer"  title="" class="filasFacturas setSelect">';
                                if (response.data[i].editable == true){
                                    sum += response.data[i].total;
                                    $("#vuelos").append('<option selected value="' + response.data[i].crypt_id + '"></option>');
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input class="prodServSelect" type="checkbox" onClick="setServ(this)" checked value="' + response.data[i].crypt_id + '" /></td>';
                                }else{
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "></td>';
                                }
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].action + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].aeronave.full_nombre_tn + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].fecha_operacion2 + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].hora_operacion + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_vuelo + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_operacion + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].piloto.full_name + '</td>';

                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].hora_llegada2) + '</td>';
                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].hora_salida2) + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].pasajeros_desembarcados) + '</td>';
                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].pasajeros_embarcados) + '</td>';





                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(response.data[i].total) + '</td>';
                                tabla += '</tr>';
                                $("#datosFacturas").append(tabla);

                                if (response.data[i].observaciones_facturacion!="" && response.data[i].observaciones_facturacion!=null){
                                    Resp = response.data[i].observaciones_facturacion.split("|");
                                    T_DOC = Resp[0];
                                    DOC = Resp[1];
                                    RAZON = Resp[2];
                                    TEL = Resp[3];
                                    DIR = Resp[4];
                                    //EMA = Resp[0];
                                }



                            }

                        }
                        
                        if (sum > 0) {
                            tabla = '<tr title="" class="filasFacturas no">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right " colspan="9"><b>TOTAL &euro;</b></td>';
                            tabla += '<td id="totalListVuelos" style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(sum) + '</td>';
                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);
                            tabla = '<tr title="" class="filasFacturas no">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; " colspan="9">    </td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right  " colspan="3">{{Form::button('<li class = "fa fa-save" > </li> EMITIR PROFORMA',  ["onClick"=>'setProforma()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    </td>';
                            //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right " colspan="3">{{Form::button('EMITIR FACTURA',  ["onClick"=>'setFactura()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    </td>';

                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);
                        }
                        //$('[data-toggle="tooltip"]').tooltip();
                    }

                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
        }
        function sendProformaEmail(id) {
            $("#modal-princ-emails").modal("show");
            idFactEmail = id;
            $("#panelIframeProforma").html('<iframe onload="setMail()" id="seeProfDef" name="seeProfDef"  style="width:100%; height: 600px; border:none" src="{{url('view-proforma')}}/' + id + '"></iframe>');
        }
        function setMail(){
            $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }
        function sendProfEmail(){
            cant = JSON.parse($("#emails_to_send").val());
            if (cant.length > 0) {
                var emailsString = cant.slice(0, -1).join(", ") + (cant.length > 1 ? ", " : "") + cant.slice(-1);
                Swal.fire({
                    title: 'Esta Seguro que Desea Enviar el Correo?',
                    html: "Se enviará el correo a las siguientes direcciones: " + emailsString,
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-princ-emails").prepend(LOADING);
                        $.post("{{url('send-prof-email')}}/"+document.getElementById("seeProfDef").contentWindow.ID_PROFORMA, $("#emails_to_send").serialize()+"&_token={{csrf_token()}}" , function (response){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            
                            
                        }).fail(function (){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Enviar el Correo')}}"
                            });
                        });
                        
                        
                        
                    }
                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Ingresar el Correo')}}"
                });
                $(".multiple_emails-input").focus();
            }
        }
        function clsDataResp(){
            $("#document, #razon, #phone, #direccion").val("");
            $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }

    </script>

</div>