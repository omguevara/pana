



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturacion Pospago')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"recaudacion",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::select('prod_servd[]', [], "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}
        {{Form::hidden('servicios_facturar',  "", ["id"=>"servicios_facturar" ])}}

        {{Form::hidden('op',  "pospago", ["id"=>"op" ])}}


        {{Form::select('vuelos_checked[]', [], "", ["style"=>"display:none", "id"=>"vuelos_checked" ,"multiple"=>"multiple"])}}

        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-4   ">

                    <div class="row  mt-2 ">
                        <div   class="col-sm-12 col-md-4 text-left  ">
                            {{Form::button('<li class="fa fa-trash"></li> ',  ["onClick"=>'clearAllInputs()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    
                        </div>

                        <div   class="col-sm-12 col-md-8 text-right  ">
                            {{Form::button('<li class="fa fa-cogs"></li> '.__("Importar de Operaciones").' <li class="fa fa-caret-up"></li>',  ["onClick"=>'$("#example1").DataTable().clear().draw();$("#modal-princ-get-operaciones").modal("show");', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, "", [ "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div  title="Agregar Nuevo Piloto" onClick="$('#modal-princ-pilotos').modal('show')" class="input-group-text">



                                    <i class="fa fa-plus"></i>

                                </div>
                            </div>
                        </div>
                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>
                            {{Form::text("fecha_operacion", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora de Vuelo (FORMATO 24H):</b></span>
                            </div>
                            {{Form::text("hora_operacion", "", ["data-msg-required"=>"Campo Requerido", "class"=>"form-control timer required ", "id"=>"hora_operacion" ,"required"=>"required"  ])}}

                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo de Vuela:</b></span>
                            </div>
                            {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], 1, ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", "", ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number  ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad de Pasajeros')])}}    
                        </div>
                        <div  class="input-group mt-2 time_estadia">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Tasa?:</b></span>
                            </div>
                            {{Form::select('cobrar_tasa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_tasa" ,"required"=>"required"])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Dosa?:</b></span>
                            </div>
                            {{Form::select('cobrar_dosa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_dosa" ,"required"=>"required"])}}
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Llegada (Formato 24H):</b></span>
                            </div>

                            {{Form::text("hora_llegada", date("d/m/Y H:i"), ["required"=>"required", "class"=>"form-control  required date_time uno", "id"=>"hora_llegada", "placeholder"=>__('Hora Llegada')])}}    
                        </div>
                        <div class="input-group mt-2  ">

                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Salida (Formato 24H):</b></span>
                            </div>
                            {{Form::text("hora_salida", date("d/m/Y H:i"), ["required"=>"required", "class"=>"form-control  required date_time uno", "id"=>"hora_salida", "placeholder"=>__('Hora Salida')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observación:</b></span>
                            </div>
                            {{Form::text("observacion", "", ["class"=>"form-control ", "id"=>"observacion", "placeholder"=>__('Observación')])}}    
                        </div>
                    </div>  

                    <div class="row  mt-2 ">

                        <div   class="col-sm-12 col-md-6  text-center  ">
                            {{Form::button('<li class="fa fa-plus"></li> '.__("AGREGAR SERV.").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn  btn-primary", "id"=>"addServ", "onClick"=>"setServ()"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-6 text-center  ">
                            {{Form::button('<li class="fa fa-eye"></li> '.__("Ver Proforma").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"getProds()", "type"=>"button", "class"=>"btn btn-primary"])}}    
                        </div>
                    </div>

                </div>  
                <div   class="col-sm-12 col-md-6   ">
                    <h2>PROFORMA</h2>
                    <table style="width:100%" id="datFactura">
                        <tr>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                            <td style="width:70%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>

                    </table>





                </div>
                <div   class="col-sm-12 col-md-2 border-left ">



                    <div class="row  mt-5 ">
                        {{--
                        <div   class="col-sm-12 col-md-12  text-center  ">
                        {{Form::button('<li class="fa fa-lock"></li> '.__("FACTURAR.").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"Facturar()", "type"=>"button", "class"=>"btn  btn-primary", "id"=>"addServ"])}}    
                        </div>
                        --}}
                        <div   class="col-sm-12 col-md-12 text-center mt-2  ">
                            {{Form::button('<li class="fa fa-save"></li> '.__("GUARDAR POSPAGO").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"savePospago()", "type"=>"button", "class"=>"btn btn-primary"])}}    
                        </div>
                    </div>

   </div>


  </div>  

   <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


  </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>




<div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

   <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                        </button>
                    </div>

                    {{Form::open([ "route"=>"aeronaves_plus", 'id'=>'frmNave','autocomplete'=>'Off'])}}
                    <div  class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-6" >
                                <div class="form-group">
                                    <label for="matricula">{{__('Matrícula')}}</label>
                                    {{Form::text("matricula", "", ["required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                                </div>
                            </div>  
                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="nombre">{{__('Modelo')}}</label>
                                    {{Form::text("nombre", "", ["required"=>"required", "maxlength"=>"20" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                                    {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">

                        {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                    </div>
                    {{ Form::close() }} 

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

            <div class="modal-dialog modal-md">
                <div id="modalFact" class="modal-content">
                    <div class="modal-header">
                        <h4 class="">Registrar Nuevo Piloto</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fa fa-times"></span>
                        </button>
                    </div>

                    {{Form::open([ 'route'=>'pilotos_plus',  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                    <div id="modalPrincBodyFact" class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="type_document">{{__('Tipo de Documento')}}</label>
                                    {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="document">{{__('Document')}}</label>
                                    {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                                </div>
                            </div>  
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="name_user">{{__('Name')}}</label>
                                    {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                                </div>
                            </div>  
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="surname_user">{{__('Surname')}}</label>
                                    {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                                </div>
                            </div>  
                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="phone">{{__('Phone')}}</label>
                                    {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                                </div>
                            </div>  
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="email">{{__('Email')}}</label>
                                    {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">

                        {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                    </div>
                    {{ Form::close() }} 

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="modal-princ-serv-extra"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

            <div class="modal-dialog modal-xl">
                <div id="modalExt" class="modal-content">
                    <div class="modal-header">
                        <h4 class="">Agregar Servicios Extra</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fa fa-times"></span>
                        </button>
                    </div>

                    {{Form::open([ "route"=>"get_servs_ext",  'id'=>'frmServ','autocomplete'=>'Off'])}}
                    <div  class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12" >

                                <table style="width:100%" id="tableServExt">
                                    <tr>
                                        <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                        <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                        <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                                    </tr>

                                </table>

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">

                        {{Form::button('<li class="fa fa-check"></li> '.__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicar"])}}    
                    </div>
                    {{ Form::close() }} 

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="modal-princ-get-operaciones"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

            <div class="modal-dialog modal-xl">
                <div id="modalExt" class="modal-content">
                    <div class="modal-header">
                        <h4 class="">Importar Registro de Operaciones</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fa fa-times"></span>
                        </button>
                    </div>

                    {{Form::open([ "route"=>"recaudacion",  'id'=>'frmGetOper','autocomplete'=>'Off'])}}
                    @method('PUT')
                    <div  class="modal-body">
                        <div class="row">
                            <div  class="col-sm-12 col-md-6   ">
                                <div class="row">
                                    <div class="input-group mt-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>Aeropuerto:</b></span>
                                        </div>
                                        {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["required"=>"required",   "class"=>"form-control  required", "id"=>"aeropuerto_id" ])}}

                                    </div>
                                </div>
                            </div>
                            <div  class="col-sm-12 col-md-4 ml-2  ">
                                <div class="row">      
                                    <div class="input-group mt-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>Aeronave:</b></span>
                                        </div>
                                        {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione", "required"=>"required",    "class"=>"form-control  required ", "id"=>"aeronave_id" ])}}

                                    </div>
                                </div>
                            </div>
                            <div  class="col-sm-12 col-md-1 ml-2 mt-2  ">
                                <div class="row">      
                                    {{Form::button('<li class="fa fa-search"></li> ',  ["onClick"=>"getRegOper()", "type"=>"button", "class"=>"btn btn-primary"])}}    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 mt-2" >


                                <table id="example1" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>{{__('SELECCIONAR')}}</th>
                                            <th>NUMERO <br/>OPERACIÓN</th>
                                            <th>FECHA <br/>OPERACIÓN</th>
                                            <th>TIPO<br/>OPERACIÓN</th>

                                            <th>{{__('AERONAVE')}}</th>
                                            <th>{{__('PILOTO')}}</th>
                                            <th>CANT. <br/>PAX</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">

                        {{Form::button('<li class="fa fa-check"></li> '.__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "onClick"=>"selectVueloFacturar()"])}}    
                    </div>
                    {{ Form::close() }} 

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


        <div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

            <div class="modal-dialog modal-md">
                <div id="modalFact" class="modal-content">
                    <div class="modal-header">
                        <h4 class="">Facturar</h4>

                    </div>
                    {{Form::open(array( "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frmCliente"))}}
                    {{Form::hidden("cliente_id", "", ["id"=>"cliente_id"])}}
                    <div id="modalPrincBodyFact" class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 col-md-12 ">
                                <h4 class="">Datos para la Factura</h4>
                                <div class="row">



                                    <div class="col-sm-12 col-md-12">

                                        <div class="form-group">
                                            <label for="document">{{__('Document')}}</label>
                                            <div class="input-group " >
                                                <div class="input-group-prepend" >
                                                    {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                                </div>
                                                {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                                <div class="input-group-append" >
                                                    <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>  

                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            <label for="razon">{{__('Responsable')}}</label>
                                            {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Responsable')])}}    

                                        </div>
                                    </div>  


                                    <div class="col-sm-12 col-md-6">

                                        <div class="form-group">
                                            <label for="phone">{{__('Phone')}}</label>
                                            {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                                        </div>
                                    </div>  



                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label for="correo">{{__('Correo')}}</label>
                                            {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    

                                        </div>  
                                    </div> 
                                    <div class="col-sm-12 col-md-12">

                                        <div class="form-group">
                                            <label for="direccion">{{__('Dirección')}}</label>
                                            {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                                        </div>  
                                    </div>
                                </div>
                            </div>



                        </div>

                    </div>
                    <div class="modal-footer justify-content-between">

                        <button type="button"  id="closeModalPrinc" onClick="$('#frmCliente').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                        <button type="button"  id="setModalPrinc"   onClick="FacturarDef()"                            class="btn btn-primary float-right">Facturar</button>


                    </div>
                    {{Form::close()}}
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>



        <script type="text/javascript">
            var PRODSERVICIOS_AGREGADOS;
            var table1;
            $(document).ready(function () {
                $(".select2").select2();

                $(document).on('select2:open', () => {
                    document.querySelector('.select2-search__field').focus();
                });

                $('#iconDate').datetimepicker({
                    format: 'L'
                });
                $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
                $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
                $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});

                table1 = $('#example1').DataTable({
                    "paging": false,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": false,
                    "info": false,
                    "autoWidth": false,
                    "responsive": true,
                    data: [],
                    columns: [
                        {data: 'action'},
                        {data: 'operacion'},
                        {data: 'fecha_operacion2'},
                        {data: 'tipo_operacion'},
                        {data: 'aeronave.full_nombre_tn'},
                        {data: 'piloto.full_name'},
                        {data: 'cant_pasajeros'}

                    ]
                    @if(app()->getLocale() != "en")
                        , language: {
                        url: "{{url('/')}}/plugins/datatables/es.json"
                        }
                    @endif

                }
                );


                $(".alpha").alphanum({
                    allowNumeric: false,
                    allowUpper: true,
                    allowLower: true

                });

                $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});



                $('.number').numeric({negative: false});


                $("#aplicar").on("click", function () {
                    $("#modal-princ-serv-extra").modal("hide");
                    getProds();
                    //$('#next1').click();
                });

                $('#frmPrinc1').validate({
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.input-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid').addClass("is-valid");
                    }
                });

                $('#frmNave').validate({
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.input-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid').addClass("is-valid");
                    }
                });

                $('#frmPilotos').validate({
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.input-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid').addClass("is-valid");
                    }
                });

                $('#frmGetOper').validate({
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.input-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid').addClass("is-valid");
                    }
                });

                $('#frmCliente').validate({
                    errorElement: 'span',
                    errorPlacement: function (error, element) {
                        error.addClass('invalid-feedback');
                        element.closest('.input-group').append(error);
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).addClass('is-invalid');
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).removeClass('is-invalid').addClass("is-valid");
                    }
                });

                $('#frmNave').on("submit", function (event) {
                    event.preventDefault();
                    if ($('#frmNave').valid()) {
                        $("#frmNave").prepend(LOADING);
                        $.post(this.action, $("#frmNave").serialize(), function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $("#frmNave").find(".overlay-wrapper").remove();
                            if (response.status == 1) {
                                $("#aeronave_id").append('<option  value="' + response.data.crypt_id + '">' + response.data.full_nombre + '</option>');
                                $("#aeronave_id").val(response.data.crypt_id);
                                $("#aeronave_id").trigger("reset");
                                $('#frmNave').trigger("reset");
                                $("#modal-princ-aeronave").modal('hide');
                            }
                            //console.log(response);
                        }).fail(function () {
                            Toast.fire({
                                icon: "error",
                                title: "Error al Guardar"
                            });
                            $(".overlay-wrapper").remove();
                        });
                    }
                    return false;
                });

                $('#frmPilotos').on("submit", function (event) {
                    event.preventDefault();
                    if ($('#frmPilotos').valid()) {
                        $("#frmPilotos").prepend(LOADING);
                        $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $("#frmPilotos").find(".overlay-wrapper").remove();
                            if (response.status == 1) {
                                $("#piloto_id").append('<option value="' + response.data.crypt_id + '">' + response.data.full_name + '</option>');
                                $("#piloto_id").val(response.data.crypt_id);
                                $("#piloto_id").trigger("reset");
                                $('#frmPilotos').trigger("reset");
                                $("#modal-princ-pilotos").modal('hide');


                            }
                            //console.log(response);
                        }).fail(function () {
                            Toast.fire({
                                icon: "error",
                                title: "Error al Guardar"
                            });
                            $(".overlay-wrapper").remove();
                        });
                    }
                    return false;
                });



            });

            function checkValue(obj) {

                if (obj.value == $("#aeropuerto_id").val() && obj.value != '') {
                    obj.value = "";
                    // $(obj).trigger("reset");
                    $(obj).trigger("change.select2");
                    Toast.fire({
                        icon: "error",
                        title: "El Aeropuerto no debe ser el Mismo del Origen"
                    });
                }

                if ($("#aeropuerto_id").val() == "" && obj.value != "") {
                    obj.value = "";
                    $(obj).trigger("change.select2");
                    Toast.fire({
                        icon: "error",
                        title: "Debe Seleccionar el Aeropuerto de Origen"
                    });
                }
            }

            function getProds() {
                if ($("#frmPrinc1").valid()) {




                    $("#frmPrinc1").prepend(LOADING);
                    $.post("{{route('get_prodservicios')}}", $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();


                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $(".overlay-wrapper").remove();

                        if (response.status == 1) {
                            PRODSERVICIOS_AGREGADOS = response.data;
                            construcTable();



                        } else {

                        }





                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar')}}"
                        });
                    });


                }
            }

            function construcTable() {

                $(".filaFactura").remove();
                sum = 0;
                base_imponible = 0;
                excento = 0;
                iva = 0;

                for (i in PRODSERVICIOS_AGREGADOS) {
                    //console.log(response.data[i]['categoria_aplicada']);
                    sum += parseFloat(PRODSERVICIOS_AGREGADOS[i]['precio'], 10);
                    mult = parseFloat(PRODSERVICIOS_AGREGADOS[i]['precio'], 10);
                    cant = PRODSERVICIOS_AGREGADOS[i]['cant'];

                    if (PRODSERVICIOS_AGREGADOS[i]['iva_aplicado'] > 0) {
                        base_imponible += mult;
                        iva += mult * PRODSERVICIOS_AGREGADOS[i]['iva_aplicado'] / 100;
                    } else {
                        excento += parseFloat(mult, 10);

                    }
                    idProdServs = '<input type="hidden" name="prods_extra[]" value="' + PRODSERVICIOS_AGREGADOS[i]['crypt_id'] + '" />';




                    //sum += response.data[i]['cant'] * response.data[i]['precio'] * response.data[i]['categoria_aplicada'] ;
                    tabla = '<tr title="' + PRODSERVICIOS_AGREGADOS[i]['formula2'] + '" class="filaFactura">';
                    //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><span onclick="deleteItem(' + i + ')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + idProdServs + cant + '</td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + PRODSERVICIOS_AGREGADOS[i]['full_descripcion'] + ' ' + PRODSERVICIOS_AGREGADOS[i]['iva2'] + '</td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (cant + ' ' + PRODSERVICIOS_AGREGADOS[i]['nomenclatura'] || '') + '</td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                    tabla += '</tr>';
                    $("#datFactura").append(tabla);
                }
                TOTAL_FACTURA = sum;
                MontoGlobalBs = sum * VALOR_EURO;
                if (TOTAL_FACTURA > 0) {
                    tabla = '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>BASE IMPONIBLE</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                    tabla += '</tr>';

                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>EXENTO</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                    tabla += '</tr>';



                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>SUB TOTAL</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                    tabla += '</tr>';

                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>IVA</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                    tabla += '</tr>';

                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="4"><b>TOTAL</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                    tabla += '</tr>';

                    $("#datFactura").append(tabla);
                }

            }
            function deleteItem(id) {
                return false;
                delete PRODSERVICIOS_AGREGADOS[id];
                construcTable();
            }
            function setServ() {
                if ($("#frmPrinc1").valid()) {
                    $('#modal-princ-serv-extra').modal('show');
                    $("#frmServ").prepend(LOADING);

                    $.post($("#frmServ").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        $(".filaServExt").remove();

                        prods = [];
                        cantidades = [];

                        $($("#prod_servd").serializeArray()).each(function () {
                            detalleServs = this.value.split(":");
                            prods.push(detalleServs[1]);
                            cantidades[detalleServs[1]] = detalleServs[0];


                        });

                        for (i in response.data) {

                            checked = $.inArray(response.data[i]['crypt_id'], prods) == -1 ? false : true;


                            tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input ' + (checked == true ? 'checked' : '') + ' type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data[i]['crypt_id'] + '" /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input type="number" min="1"   id="' + response.data[i]['crypt_id'] + '" onChange="UpItemSelect()" value="' + (checked == true ? cantidades[response.data[i]['crypt_id']] : '0') + '" class="form-control" ' + (checked == true ? '' : 'disabled') + ' /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + '</td>';


                            tabla += '</tr>';
                            $("#tableServExt").append(tabla);
                        }


                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar los Servicios"
                        });
                    });
                }


            }
            function selectItem(obj) {
                id = $(obj).data("crypt_id");
                if (obj.checked == true) {
                    $("#" + id).val("1").prop("disabled", false).focus();
                } else {
                    $("#" + id).val("0").prop("disabled", true);
                }
                UpItemSelect();
            }
            function UpItemSelect() {
                $("#prod_servd").html("");
                $(".selectItem:checked").each(function () {
                    Id = $(this).data('crypt_id');
                    $("#prod_servd").append('<option selected value="' + $("#" + Id).val() + ':' + Id + '"></option>');
                });
            }

            function savePospago() {
                if ($("#frmPrinc1").valid()) {
                    if ($(".filaFactura").length > 0) {

                        $("#servicios_facturar").val(JSON.stringify(PRODSERVICIOS_AGREGADOS));

                        Swal.fire({
                            title: 'Esta Seguro que Desea Continuar?',
                            html: "Confirmaci&oacute;n",
                            icon: 'question',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: '<li class="fa fa-check"></li> Si',
                            cancelButtonText: '<li class="fa fa-undo"></li> No'
                        }).then((result) => {
                            if (result.isConfirmed) {
                                $("#op").val("pospago");
                                $("#frmPrinc1").prepend(LOADING);
                                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                                    $(".overlay-wrapper").remove();
                                    // Swal.close();
                                    //$("#modal-fact").modal('hide');
                                    //$("#main-content").html(response);

                                    Toast.fire({
                                        icon: response.type,
                                        title: response.message
                                    });
                                    $(".overlay-wrapper").remove();
                                    if (response.status == 1) {
                                        clearAllInputs();

                                    } else {

                                    }





                                }).fail(function () {
                                    $(".overlay-wrapper").remove();
                                    Toast.fire({
                                        icon: "error",
                                        title: "{{__('Error al Guardar')}}"
                                    });
                                });

                            }
                        });
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('No Existen Servicios a Facturar')}}"
                        });
                    }
                }
            }

            function Facturar() {
                if ($("#frmPrinc1").valid()) {
                    if ($(".filaFactura").length > 0) {

                        $("#modal-fact").modal('show');

                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('No Existen Servicios a Facturar')}}"
                        });
                    }
                }
            }
            function FacturarDef() {

                if ($("#frmCliente").valid()) {
                    Swal.fire({
                        title: 'Esta Seguro que Desea Facturar?',
                        html: "Confirmaci&oacute;n",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<li class="fa fa-check"></li> Si',
                        cancelButtonText: '<li class="fa fa-undo"></li> No'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $("#op").val("facturar");
                            $("#servicios_facturar").val(JSON.stringify(PRODSERVICIOS_AGREGADOS));

                            $("#frmCliente").prepend(LOADING);

                            $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1, #frmCliente").serialize(), function (response) {
                                $(".overlay-wrapper").remove();
                                // Swal.close();
                                //$("#modal-fact").modal('hide');
                                //$("#main-content").html(response);

                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                                $(".overlay-wrapper").remove();
                                if (response.status == 1) {
                                    $("#modal-fact").modal("hide");
                                    clearAllInputs();
                                    window.open("{{url('view-factura')}}/" + response.data, 'FACTURA');
                                } else {

                                }





                            }).fail(function () {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error al Guardar')}}"
                                });
                            });


                        }
                    });
                }

            }
            function getRegOper() {
                if ($("#frmGetOper").valid()) {
                    $("#frmGetOper").prepend(LOADING);
                    $('#example1').DataTable().clear().draw();
                    $.post($("#frmGetOper").attr("action"), $("#frmGetOper").serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        $('#example1').DataTable().clear().rows.add(response.data).draw();
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
            }
            function selectVuelo(obj) {
                if ($('.selectVueloFact:checked').length > 2) {
                    Toast.fire({
                        icon: "error",
                        title: "Sólo Puede Seleccionar Dos(2) Registros"
                    });
                    $(obj).prop("checked", false);

                }

                if ($('.selectVueloFact:checked').length == 2) {
                    uno = $('.selectVueloFact:checked').eq(0).data('tipo_operacion');
                    dos = $('.selectVueloFact:checked').eq(1).data('tipo_operacion');
                    if (uno == dos) {
                        Toast.fire({
                            icon: "error",
                            title: "No Puede Seleccionar Dos (2) Vuelos del Mismo Tipo"
                        });
                        $(obj).prop("checked", false);
                    }
                }


            }

            function selectVueloFacturar() {

                if ($('.selectVueloFact:checked').length > 0) {
                    id1 = $('.selectVueloFact:checked').eq(0).val();
                    $("#vuelos_checked").html('<option selected value="' + id1 + '"><>');
                    if ($('.selectVueloFact:checked').length == 2) {
                        id2 = $('.selectVueloFact:checked').eq(1).val();
                        $("#vuelos_checked").append('<option selected value="' + id2 + '"><>');
                    } else {
                        id2 = '';
                    }
                    $("#frmGetOper").prepend(LOADING);
                    $.get("{{url('get-vuelos-fact')}}/" + id1 + '/' + id2, function (response) {
                        $(".overlay-wrapper").remove();
                        $("#modal-princ-get-operaciones").modal("hide");

                        $('#aeropuerto_id').val(response.data.aeropuerto_id);
                        $('#piloto_id').val(response.data.piloto_id);
                        $('#aeronave_id').val(response.data.aeronave_id);
                        $('#fecha_operacion').val(response.data.fecha_operacion);
                        $('#hora_operacion').val(response.data.hora_operacion);
                        $('#cant_pasajeros').val(response.data.cant_pasajeros);
                        
                        if (response.data.cant_pasajeros>0){
                            $("#cobrar_tasa").val(1);
                        }else{
                            $("#cobrar_tasa").val(0);
                        }



                        $('#hora_llegada').val(response.data.hora_llegada);
                        $('#hora_salida').val(response.data.hora_salida);




                        $("#prod_servd").html("");
                        for (i in response.data.prodservicios_extra) {
                            $("#prod_servd").append('<option selected="selected" value="' + i + '"></option>');
                        }

                        //$('#aeropuerto_id, #piloto_id, #aeronave_id').prop("disabled", true);



                        $("#aeropuerto_id, #piloto_id, #aeronave_id").trigger("change.select2");

                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });

                } else {
                    Toast.fire({
                        icon: "error",
                        title: "Debe Seleccionar los Vuelos a Importar"
                    });
                }
            }
            function clearAllInputs() {
                $("#vuelos_checked, #prod_servd").html('');
                $("#servicios_facturar").val("");
                $(".filaFactura").remove();
                $("#op").val("pospago");
                $('#aeropuerto_id, #piloto_id, #aeronave_id, #fecha_operacion').val("");
                $('#hora_llegada, #hora_salida, #cant_pasajeros, #hora_operacion').val("");

                $("#aeropuerto_id, #piloto_id, #aeronave_id").trigger("change.select2");
            }

            function findDato2() {


                if ($("#frmCliente #type_document, #frmCliente #document").valid()) {
                    $("#modal-fact").prepend(LOADING);
                    $.get("{{url('get-data-cliente')}}/" + $("#frmCliente #type_document").val() + "/" + $("#frmCliente #document").val(), function (response) {
                        $(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#frmCliente #cliente_id").val(response.data.crypt_id);
                            $("#frmCliente #razon").val(response.data.razon_social);
                            $("#frmCliente #phone").val(response.data.telefono);
                            $("#frmCliente #correo").val(response.data.correo);
                            $("#frmCliente #direccion").val(response.data.direccion);
                            $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", true);
                            Toast.fire({
                                icon: "success",
                                title: "{{__('Datos Encontrados')}}"
                            });
                        } else {
                            Toast.fire({
                                icon: "warning",
                                title: "{{__('Datos no Encontrados, Registrelos')}}"
                            });
                            $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                            $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                        }


                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", true);
                        $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Consultar los Datos')}}"
                        });
                    });
                }


            }
        </script>

    </div>