<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturacion Pospago')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"emitir_factura",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @method("post")


        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div  class="col-sm-12 col-md-6   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["required"=>'required',   "class"=>"form-control required select2 ", "id"=>"aeropuerto_id" ])}}

                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-4 ml-2   ">
                    <div class="row">      


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "required"=>'required',   "placeholder"=>"Seleccione",     "class"=>"form-control  required select2 ", "id"=>"aeronave_id" ])}}

                        </div>
                    </div>
                </div>

                <div  class="col-sm-12 col-md-1 mt-3   ">
                    {{Form::button('<li class="fa fa-search"></li> ',  ["onClick"=>'buscarVuelosProf()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 mt-4" >

                    <table style="width:100%" id="datosFacturas">
                        <tr>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NUMERO<br/> PROFORMA</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>FECHA<br/> PROFORMA</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TIPO<br/> DOCUMENTO</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>ESTADO</b></td>
                            
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>RESPONSABLE PAGO</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AERONAVE</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AEROPUERTO</b></td>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>


    <div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Responsable de Pago</h4>

                </div>
                {{Form::open(array( "enctype"=>"multipart/form-data", "target"=>"_blank",  "route"=>"facturar_pospago", "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frmCliente"))}}
                {{Form::hidden("cliente_id", "", ["id"=>"cliente_id"])}}
                {{Form::select('vuelos[]', [], "", ["style"=>"display:none", "id"=>"vuelos" ,"multiple"=>"multiple"])}}
                <div id="modalPrincBodyFact" class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 ">

                            <div class="row">



                                <div class="col-sm-12 col-md-12">

                                    <div class="form-group">
                                        <label for="document">{{__('Document')}}</label>
                                        <div class="input-group " >
                                            <div class="input-group-prepend" >
                                                {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                            </div>
                                            {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                            <div class="input-group-append" >
                                                <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="razon">{{__('Responsable')}}</label>
                                        {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Responsable')])}}    

                                    </div>
                                </div>  


                                <div class="col-sm-12 col-md-6">

                                    <div class="form-group">
                                        <label for="phone">{{__('Phone')}}</label>
                                        {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                                    </div>
                                </div>  



                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="correo">{{__('Correo')}}</label>
                                        {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    

                                    </div>  
                                </div> 
                                <div class="col-sm-12 col-md-12">

                                    <div class="form-group">
                                        <label for="direccion">{{__('Dirección')}}</label>
                                        {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                                    </div>  
                                </div>
                            </div>
                        </div>



                    </div>

                </div>
                <div class="modal-footer justify-content-between">

                    <button type="button"  id="closeModalPrinc" onClick="$('#frmCliente').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                    <button type="button"  id="setModalPrinc"   onClick="applyProforma()"                            class="btn btn-primary float-right">Procesar</button>


                </div>
                {{Form::close()}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-serv-fly"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Servicios Aplicados</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">
                    <div class="row">
                        <table style="width:100%" id="datFactura">
                            <tr>
                                <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                <td style="width:70%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                            </tr>

                        </table>



                    </div>

                </div>
                <div class="modal-footer justify-content-between">




                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="modal-princ-emails"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Enviar Proforma por Correo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-3 " >
                            <div  class="row">
                                <label>Ingrese el Correo:</label>
                                <input type="text" id="emails_to_send" name="emails_to_send" class="form-control" value="[]" />
                            </div>  
                            
                            <div  class="row ">
                                <div class="col-sm-12 col-md-12 text-center mt-2" >
                                    {{Form::button('<li class="fa fa-mail-bulk"></li> '.__("Enviar"),  ["onClick"=>"sendProfEmail()", "type"=>"button", "class"=>"btn btn-primary" ])}}    
                                </div>
                                
                            </div>  
                            
                            
                        </div>
                        <div class="col-sm-12 col-md-9 " >
                            <div id="panelIframeProforma" class="row">

                            </div>  
                        </div>

                    </div>
                    

                </div>
                <div class="modal-footer justify-content-between">

                    
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    
    <div class="modal fade" id="modal-see-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Ver Factura</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">

                    <div id="modal-see-fact-body" class="row">
                    </div>

                </div>
                <div class="modal-footer justify-content-between">

                    
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

       <div class="modal fade" id="modal-fin-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xs">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Procesar Factura</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                {{Form::open(['class'=>'form-horizontal',   'id'=>'formOpt','autocomplete'=>'Off'])}}
                <div  class="modal-body">
                    <div class="row">
                    <div class="col-sm-12 col-md-6">
                        {{Form::hidden("pagado", null, ["id"=>"pagado"])}}    
                        <div class="form-group">
                            <label for="peso_maximo">{{__('Tasa del Euro')}} </label>
                            {{Form::text("tasa", muestraFloat($VALOR_EURO), ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"tasa", "placeholder"=>__('Tasa del Euro')])}}    

                        </div>
                    </div>
                    
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="numero">{{__('Número de Factura')}} </label>
                            {{Form::text("numero", "", ["required"=>"required", "class"=>"form-control required  number", "maxlength"=>"8" ,"id"=>"numero", "placeholder"=>__('Numero de Factura')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        {{Form::button('<li class="fa fa-save"></li> '.__("GUARDAR").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"finUp()", "type"=>"button", "class"=>"btn btn-primary"])}}    
                    </div>

                        </div>




                </div>
                 {{ Form::close() }} 
                <div class="modal-footer justify-content-between">

                    
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        var urlFact = "";
        var idFactEmail = "";
        var NumeroFactura = parseInt("{{$numero_factura}}", 10);
        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $('#emails_to_send').multiple_emails({position: "bottom"});
            
            $("#modal-princ-emails").on('shown.bs.modal', function() {
                $(".multiple_emails-input").focus();
            });
            
            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});



            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});



            $('#frmCliente').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('.number').numeric({negative: false});



            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            $('#formOpt').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            


        });

        function buscarVuelosProf() {
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $("[name=_method]").val("PUT");
                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status ==1){
                        $(".overlay-wrapper").remove();
                        $(".filasFacturas").remove();


                        for (i in response.data) {



                            tabla = '<tr title="" class="filasFacturas">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].action + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].nro_proforma2 + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].fecha_proforma2 + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_documento + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].estado + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].cliente.rif + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].aeronave.full_nombre_tn + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; " title="'+response.data[i].aeropuerto.full_nombre+'">' + response.data[i].aeropuerto.codigo_oaci + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(response.data[i].total) + '</td>';

                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);


                        }
                    }
                    //$('[data-toggle="tooltip"]').tooltip();


                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
        }
        
        function setProforma() {
            Swal.fire({
                title: 'Esta Seguro que Desea Emitir la Proforma?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<li class="fa fa-save"></li> Si',
                cancelButtonText: '<li class="fa fa-undo"></li>  No'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#modal-fact').modal('show');
                }
            });
        }
        

        function setFactura() {
            Swal.fire({
                title: 'Esta Seguro que Desea Emitir la Factura?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                }
            });
        }

        function findDato2() {


            if ($("#frmCliente #type_document, #frmCliente #document").valid()) {
                $("#modal-fact").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#frmCliente #type_document").val() + "/" + $("#frmCliente #document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#frmCliente #cliente_id").val(response.data.crypt_id);
                        $("#frmCliente #razon").val(response.data.razon_social);
                        $("#frmCliente #phone").val(response.data.telefono);
                        $("#frmCliente #correo").val(response.data.correo);
                        $("#frmCliente #direccion").val(response.data.direccion);
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                        $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", true);
                    $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }


        }
        function applyProforma() {
            $('#modal-fact').modal('hide');
            $(".filasFacturas").remove();
            $("#frmCliente").submit()
        }
        function seeDetalleVuelo(route) {
            $("#modal-serv-fly").modal('show');
            $("#modal-serv-fly").prepend(LOADING);

            $.get(route, function (response) {
                //console.log(response);
                
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                
                $(".overlay-wrapper").remove();
                if (response.status == 1){
                
                    $(".filaFactura").remove();
                    sum = 0;
                    base_imponible = 0;
                    excento = 0;
                    iva = 0;
                    for (i in response.data.get_detalle) {
                        sum += parseFloat(response.data.get_detalle[i]['precio'], 10);
                        mult = parseFloat(response.data.get_detalle[i]['precio'], 10);
                        cant = response.data.get_detalle[i]['cant'];

                        if (response.data.get_detalle[i]['iva_aplicado'] > 0) {
                            base_imponible += mult;
                            iva += mult * response.data.get_detalle[i]['iva_aplicado'] / 100;
                        } else {
                            excento += parseFloat(mult, 10);

                        }



                        tabla = '<tr title="' + response.data.get_detalle[i]['formula2'] + '" class="filaFactura">';

                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + muestraFloat(cant) + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data.get_detalle[i]['full_descripcion'] + ' ' + response.data.get_detalle[i]['iva2'] + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (muestraFloat(cant) + ' ' + response.data.get_detalle[i]['nomenclatura'] || '') + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                        tabla += '</tr>';
                        $("#datFactura").append(tabla);
                    }
                    TOTAL_FACTURA = sum;
                    MontoGlobalBs = sum * VALOR_EURO;
                    if (TOTAL_FACTURA > 0) {
                        tabla = '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>BASE IMPONIBLE</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>EXENTO</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                        tabla += '</tr>';



                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>SUB TOTAL</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>IVA</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="3"><b>TOTAL</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                        tabla += '</tr>';

                        $("#datFactura").append(tabla);
                    }
                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                 Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


        function seeProforma(url) {
            window.open(url, '_blank')
        }
        
        function anulaProforma(url) {
            Swal.fire({
                title: 'Esta Seguro que Desea Anular la proforma?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<li class="fa fa-save"></li> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#frmPrinc1").prepend(LOADING);
                    
                    $.get(url , function (response){
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status==1){
                            buscarVuelosProf();
                        }
                        


                    }).fail(function (){
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesar')}}"
                        });
                    });
                    
                    buscarVuelosProf();
                    
                    //window.open(url, '_blank')
                }
            });
        }
        
        function factProforma(url, pagado) {
            
            $("#modal-fin-fact").modal("show");
            
            $("#formOpt #numero").val(NumeroFactura+1);
            
            urlFact = url;

            $("#pagado").val(pagado);
            
            /*
            Swal.fire({
                title: 'Esta Seguro que Desea Emitir la Factura?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<li class="fa fa-save"></li> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No'
            }).then((result) => {
                if (result.isConfirmed) {
                    
                    
                    
                    
                    
                    $("#modal-see-fact").modal("show");
                    $("#modal-see-fact").prepend(LOADING);
                    $("#modal-see-fact-body").html("<iframe onload=\"closeLoading();\" name=\"iframePdf\" id=\"iframePdf\" style=\"width:100%; height: 600px; border:none\" src=\""+url+"\"></iframe>");
                    buscarVuelosProf();
                    
                    //window.open(url, '_blank')
                }
            });
            */
        }
        function sendProformaEmail(id) {
            $("#modal-princ-emails").modal("show");
            idFactEmail = id;
            $("#panelIframeProforma").html('<iframe onload="setMail()" id="seeProfDef" name="seeProfDef"  style="width:100%; height: 600px; border:none" src="{{url('view-proforma')}}/' + id + '"></iframe>');
        }
        function setMail(){
            $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }
        function sendProfEmail(){
            cant = JSON.parse($("#emails_to_send").val());
            if (cant.length > 0) {

                Swal.fire({
                    title: 'Esta Seguro que Desea Enviar el Correo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-princ-emails").prepend(LOADING);
                        $.post("{{url('send-prof-email')}}/"+idFactEmail, $("#emails_to_send").serialize()+"&_token={{csrf_token()}}" , function (response){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            
                            
                        }).fail(function (){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Enviar el Correo')}}"
                            });
                        });
                        
                        
                        
                    }
                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Ingresar el Correo')}}"
                });
                $(".multiple_emails-input").focus();
            }
        }
        function finUp(){
            
            if ( $("#formOpt").valid() ){
                if ($("#pagado").val() != true) {
                    message = '<i>La factura que está generando no está pagada</i>';
                } else {
                    message = "Confirmaci&oacute;n";
                }
                Swal.fire({
                    title: 'Esta Seguro que Desea Emitir la Factura?',
                    html: message,
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-fin-fact").modal("hide");
                        urlFact = urlFact+"/"+$("#tasa").val()+"/"+$("#numero").val();
                        NumeroFactura++;


                        $("#modal-see-fact").modal("show");
                        $("#modal-see-fact").prepend(LOADING);
                        $("#modal-see-fact-body").html("<iframe onload=\"closeLoading();\" name=\"iframePdf\" id=\"iframePdf\" style=\"width:100%; height: 600px; border:none\" src=\""+urlFact+"\"></iframe>");
                        buscarVuelosProf();

                        //window.open(url, '_blank')
                    }
                });
            }
        }
        
        function seetProforma(url){
            $("#modal-see-fact").modal("show");
            $("#modal-see-fact").prepend(LOADING);
            $("#modal-see-fact-body").html("");
            $("#modal-see-fact-body").html("<iframe onload=\"closeLoading();\" name=\"iframePdf\" id=\"iframePdf\" style=\"width:100%; height: 600px; border:none\" src=\""+url+"\"></iframe>");
        }
        function anularFactura(url){
            Swal.fire({
                title: 'Esta Seguro que Desea Anular la Factura?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<li class="fa fa-save"></li> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#panel_princ").prepend(LOADING);
                    
                    $.get(url , function (response){
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status==1){
                            buscarVuelosProf();
                        }
                        


                    }).fail(function (){
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesar')}}"
                        });
                    });
                    
                    buscarVuelosProf();
                    
                    //window.open(url, '_blank')
                }
            });
        }
        
       
    </script>

</div>