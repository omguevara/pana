<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Pagar Factura')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('facturar_pospago')}}" onClick="setAjax(this, event)" id="backP" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["facturar_pospago.pagar_factura", $Factura->crypt_id],'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
       


        <div id="card-body-main" class="card-body ">
            <h1>EN MANTENIMIENTO</h1>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>






  
    <script type="text/javascript">
        
        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            
         

           

            $('.number').numeric({negative: false});



            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
           
            


        });

   
       
    </script>

</div>