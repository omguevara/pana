<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Pagos de proformas')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#modal-info').modal('show')" class="btn btn-tool" ><i class="fas fa-info"></i></a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(['route' => 'recaudacion.down_pagos','class'=>'form-horizontal', 'id'=>'busqueda_proforma','autocomplete'=>'Off', 'target' => 'view_resumen'])}}
            <div id="card-body-main" class="card-body ">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="row">      
                            <div class="input-group mt-2 mr-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Cliente:</b></span>
                                </div>
                                {{ Form::hidden('correos', "", ['id' => 'correos']) }}
                                {{Form::select('cliente_id', $clientes, "", [ "required"=>'required',   "placeholder"=>"Seleccione",     "class"=>"form-control  required select2 ", "id"=>"cliente_id" ])}}

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <div class="row">      
                            <div class="input-group mt-2 mr-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeropuertos:</b></span>
                                </div>
                                {{Form::select('aeropuerto_id', $Aeropuertos, "", [ "required"=>'required',   "placeholder"=>"Seleccione",     "class"=>"form-control  required select2 ", "id"=>"aeropuerto_id" ])}}

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mt-3">
                        <div class="row">
                                {{Form::button('<li class="fa fa-search"></li>',  ["id"=>"busqueda_factura", "type"=>"button", "class"=>"btn-sm btn btn-primary mx-1", "title" => "Buscar..."])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="row">
                            <div class="input-group mt-2 mr-2">
                                <div class="input-group-append">
                                    <p class="input-group-text"><b>Rango de Fecha</b></p>
                                </div>
                                {!! Form::text('rango_fecha', $RANGO_MES, ['class' => 'form-control required', 'id' => 'rango_fecha']) !!}
                            </div>
                        </div>
                    </div>
                    <div  class="col-sm-4 col-md-4">
                        <div class="row">
                            <div class="input-group mt-2 mr-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Estatus de pago:</b></span>
                                </div>
                                {{Form::select('estatus_id', ["" => "Todas", "1" => "Pagadas", "0" => "No pagadas"], "" , ["class"=>"form-control select2 ", "id"=>"estatus_id" ])}}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 col-md-3 mt-3">
                        <div class="row">
                            {{ Form::button('<i class="far fa-file-pdf"></i>',  ["id"=>"busqueda_factura", "type"=>"button", "class"=>" btn-sm btn btn-primary mx-1", "title" => "Descargar", "onClick" => "downPdf()"]) }}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 mt-4" >

                        <table id="facturas-table" class="display compact table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>ACCIONES</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>RIF</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>FECHA<br/> DE LA PROFORMA</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AEROPUERTO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>MATRÍCULA</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>PROFORMA €</b></th>
                                    <!-- <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CONCEPTO</b></th> -->
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>ABONADO</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">


            </div>
            <!-- /.card -->
        {{ Form::close() }} 
    </div>
    <!-- Modal Proformas -->
    <div class="modal fade" id="proformaModal" tabindex="-1" data-backdrop="static" aria-labelledby="proformaModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="restante_pagar"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="px-3 py-3">
                            <h2 class="title-modalHistorial"></h2>
                            {!! Form::open(["route"=>["recaudacion.pagos"],  'id'=>'formPagar','autocomplete'=>'Off']) !!}
                                {!! Form::hidden('idproforma', '', ['id' => 'idproforma']) !!}
                                <div class="row mb-3">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <p class="input-group-text"><b>Fecha</b></p>
                                            </div>
                                            {!! Form::text('fecha', '', ['class' => 'form-control required', 'id' => 'fecha']) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <p class="input-group-text"><b>Aeropuerto</b></p>
                                            </div>
                                            {!! Form::text('aeropuerto', null, ['class' => 'form-control required', 'id' => 'aeropuerto', 'maxlength' => "30"]) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <p class="input-group-text"><b>Matrícula</b></p>
                                            </div>
                                            {!! Form::text('matricula', null, ['class' => 'telefono form-control required', 'id' => 'matricula', 'placeholder' => '(000)-000.00.00',]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <p class="input-group-text"><b>Proforma</b></p>
                                            </div>
                                            {!! Form::text('proforma', null, ['class' => 'form-control', 'id' => 'proforma']) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <p class="input-group-text"><b>Monto abonado</b></p>
                                            </div>
                                            {!! Form::text('monto_abonado', '0,00', ["disabled", 'class' => 'form-control required money text-right', 'id' => 'monto_abonado' ]) !!}
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <div class="input-group-append">
                                                <p class="input-group-text"><b>Pagar</b></p>
                                            </div>
                                            {!! Form::text('pagar', '0,00', ['class' => 'form-control required money text-right', 'id' => 'pagar']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col-lg-12">
                                        <div class="input-group">
                                            {!! Form::submit('Pagar', ['class' => 'form-control btn btn-success ml-auto', 'id' => 'pagarBtn']) !!}
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal leyenda --}}
    <div class="modal fade" id="modal-info"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" >
        <div class="modal-dialog modal-xs">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">LEYENDA</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div  class="modal-body">
                    <table style="width:100%" >

                        <tr class="alert-warning">
                            <td ></td>
                            <td ><b>Proforma sin pagar.</b></td>
                        </tr>
                        <tr class="alert-success">
                            <td ></td>
                            <td ><b>Proforma pagada.</b></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- Modal envío de correo --}}
    <div class="modal fade" id="modal-princ-emails"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">
        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Enviar Proforma por Correo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-3 " >
                            <div  class="row">
                                <label>Ingrese el Correo:</label>
                                <input type="text" id="emails_to_send" name="emails_to_send" class="form-control" value="[]" />
                            </div>  
                            
                            <div  class="row ">
                                <div class="col-sm-12 col-md-12 text-center mt-2" >
                                    {{Form::button('<li class="fa fa-mail-bulk"></li> '.__("Enviar"),  ["onClick"=>"sendResumenEmail()", "type"=>"button", "class"=>"btn btn-primary" ])}}    
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-9 " >
                            <div id="panelIframeProforma" class="row">
                                <iframe id="view_resumen" name="view_resumen" style="width:100%; height: 600px; border:none" src=""></iframe>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">                   
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        var idFactEmail = "";
        function downPdf() {
            if ($('#busqueda_proforma').valid()) {
                $("#correos").val("");
                $("#panel_princ").prepend(LOADING);
                $("#busqueda_proforma").submit();
                $("#modal-princ-emails").modal("show");
                $(".overlay-wrapper").remove();
            }
        }
        function setMail(){
            // $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }
        function sendResumenEmail(){
            // $("#correos").val()
            cant = JSON.parse($("#emails_to_send").val());
            if (cant.length > 0) {
                Swal.fire({
                    title: 'Esta Seguro que Desea Enviar el Correo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-princ-emails").prepend(LOADING);
                        $.post("{{route('recaudacion.send_resumen_email')}}", $("#emails_to_send").serialize()+"&_token={{csrf_token()}}&"+$("#busqueda_proforma").serialize() , function (response){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });                            
                        }).fail(function (){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Enviar el Correo')}}"
                            });
                        });
                    }
                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Ingresar el Correo')}}"
                });
                $(".multiple_emails-input").focus();
            }
        }
        function clsDataResp(){
            $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }
        $(document).ready(function () {
            tableFacturas = null;
            $('.close').on("click", function (){
                $("#formPagar")[0].reset();
            })
            $('input[name="rango_fecha"]').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

            $('#emails_to_send').multiple_emails({position: "bottom"});
            
            $("#modal-princ-emails").on('shown.bs.modal', function() {
                $(".multiple_emails-input").focus();
            });

            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});

            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            
            tableFacturas = $('#facturas-table').DataTable({
                "paging": true,
                pageLength: 10,
                "deferRender": true,
                "lengthChange": false,
                searching: true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,          
                serverSide: true,
                processing: true,
                "language": {
                    "emptyTable": "No hay datos disponibles en la tabla"
                },
                ajax: {
                    'url': "{{route('recaudacion.obtener_proforma')}}",
                    'dataType': 'json',
                    'type': 'GET',
                    "data":
                        function (d) {
                            d.cliente_id = $('#cliente_id').val(),
                            d.estatus_id = $('#estatus_id').val(),
                            d.rango_fecha = $('#rango_fecha').val()
                        },
                },
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'boton'},
                    {data: 'rif'},
                    {
                        data: 'fecha_proforma2',
                    },
                    {data: 'aeropuerto.nombre'},
                    {data: 'aeronave.full_nombre_tn'},
                    {
                        data: 'total2',
                    },
                    {
                        data: 'total_monto_abonado2',
                    },
                  
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });
            
            $("#busqueda_factura").on("click", function(e){
                if ($('#busqueda_proforma').valid()) {
                    tableFacturas.draw();
                }
            });

            $("#formPagar").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    event.preventDefault();
                    let pagar = $('#pagar').val();
                    let monto_abonado = $('#monto_abonado').val().replace('€ ', '');
                    let proforma = $('#proforma').val().replace('€ ', '');

                    if (usaFloat(pagar) == 0) {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('No es posible registrar esa cantidad, por favor ingrese una cantidad válida')}}"
                        });
                    } else {
                        if ((usaFloat(pagar) + usaFloat(monto_abonado)).toFixed(2) > usaFloat(proforma)) {
                            Toast.fire({
                                icon: "error",
                                title: "{{__('El monto a pagar debe ser menor o igual al restante, por favor ingrese una cantidad válida')}}"
                            });
                        }else{
                            $("#proformaModal").prepend(LOADING);
                            $.post("{{ route('recaudacion.pagos') }}", $('#formPagar').serialize(), function (response) {
                                $(".overlay-wrapper").remove();
                                if (response.status == 1) {
                                    $('#idproforma').val(response.data.crypt_id);
                                    $('#proforma').val('€ ' + new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2}).format(response.data.total)); 
                                    $('#pagar').val('0,00');
                                    $('#monto_abonado').val('€ ' + new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2}).format(response.data.total_monto_abonado));
                                    tableFacturas.draw();
                                }if (response.data.deuda_actual == 0){
                                    $('#pagar').prop('disabled', true);
                                    $('#pagarBtn').prop('disabled', true);
                                }
                                var restante = response.data.total - response.data.total_monto_abonado;
                                $("#restante_pagar").text('Monto restante por pagar: € ' + muestraFloat(restante));
                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                            }).fail(function () {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error en la conexión')}}"
                                });
                            });
                        }
                    }

                }
            });
        });
        function datosModalProforma(id, fecha_proforma, aeropuerto, matricula, proforma, monto_abonado) {
            var proforma_original = proforma;
            var restante = usaFloat(proforma) - usaFloat(monto_abonado);

            $('#idproforma').val(id);
            $('#fecha').val(fecha_proforma).prop("disabled", true);
            $('#aeropuerto').val(aeropuerto).prop("disabled", true);
            $('#matricula').val(matricula).prop("disabled", true);
            $('#proforma').val('€ ' + proforma).prop("disabled", true);
            $('#monto_abonado').val('€ ' + monto_abonado).prop("disabled", true);

            $("#restante_pagar").text('Monto restante por pagar: € ' + muestraFloat(restante));

            $('#pagar').prop('disabled', false);
            $('#pagarBtn').prop('disabled', false);
        }

       
    </script>

</div>