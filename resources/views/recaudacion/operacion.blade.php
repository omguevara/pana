<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">

    <style>
        table.dataTable tbody td {
            word-break: break-word;
            vertical-align: top;
        }

    </style>
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Emisión de Proformas por Operación')}}</h3>

            <div class="card-tools">
                
                <a href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"facturar_pospago.proforma_operacion",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @method("post")


        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div  class="col-sm-12 col-md-5   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["required"=>'required',  "placeholder"=>"Seleccione" ,"class"=>"form-control required select2 ", "id"=>"aeropuerto_id" ])}}

                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-3   ">
                    <div class="row">      
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", [    "placeholder"=>"Todos",     "class"=>"form-control  select2 ", "id"=>"aeronave_id" ])}}
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-3   ">
                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Fechas:</b></span>
                        </div>
                        {{Form::text("rango", $RANGO_MES, ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                        <div class="input-group-append">
                            <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>

                    </div>
                </div>
                <div  class="col-sm-12 col-md-1 mt-2   ">
                    {{Form::button('<li class="fa fa-search"></li> ',  ["onClick"=>'if ( $("#frmPrinc1").valid()){   table1.draw();}', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 mt-4" >

                    <table id="example1" style="width:100%" >
                        <thead>
                            <tr>
                                <td>Responsable</td>
                                <td>Aeronave</td>
                                <td>Tipo</td>
                                <td>Fecha LLegada</td>
                                <td>Hora Llegada</td>
                                <td>Fecha Salida</td>
                                <td>Hora Salida</td>
                                <td>PAX</td>
                                <td></td>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>



    <div class="modal fade" id="modal-dee-detail-fly"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Detalle de Vuelo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div class="modal-body">
                    {{Form::open(["route"=>"facturar_pospago.proforma_operacion",'class'=>'form-horizontal',   'id'=>'frmPrinc2','autocomplete'=>'Off'])}}
                    <div class="row">
                        <div class="col-md-6">
                            {{Form::hidden("vuelos[]", "", ["id"=>"llegada_id"])}}    
                            {{Form::hidden("vuelos[]", "", ["id"=>"salida_id"])}}
                            {{Form::hidden("crypt_aeronave_id", "", ["id"=>"crypt_aeronave_id"])}}
                            <div class="row">
                                <div  class="col-md-12 ">
                                    <div class="input-group mt-2"  >
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>Aeropuerto:</b></span>
                                        </div>
                                        {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>"",   "readonly"=>"readonly",    "class"=>"form-control required  ", "id"=>"aeropuerto_id" ])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div  class="col-md-12 ">
                                    <div class="input-group mt-2 ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>{{__('Document')}}:</b></span>
                                        </div>
                                        <div class="input-group-prepend" >
                                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "V", [ "class"=>"form-control cliente tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                            {{Form::hidden('cliente_id', "", [ "id"=>"cliente_id"])}}
                                        </div>
                                        {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number cliente required tab1", "id"=>"document", "placeholder"=>__('Document')])}}
                                        <div class="input-group-append" >
                                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div  class="col-md-12 ">
                                    <div class="input-group mt-2 ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>Responsable:</b></span>
                                        </div>
                                        {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1 cliente", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div  class="col-md-12 ">
                                    <div class="input-group mt-2 ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                                        </div>
                                        {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone cliente", "id"=>"phone", "placeholder"=>__('Phone')])}}    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div  class="col-md-12 ">
                                    <div class="input-group mt-2 ">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                                        </div>
                                        {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required cliente", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div  class="col-md-6 ">
                                    <div class="input-group "  >
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>Tipo de Vuelo:</b></span>
                                        </div>
                                        {{Form::select('nacionalidad_id', [ "1"=>"Nacional", "2"=>"Internacional"], "1" , ["readonly"=>"readonly", "data-msg-required"=>"Campo Requerido", "class"=>"form-control required  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                                    </div>
                                </div>


                                <div  class="col-md-6 ">
                                    <div class="input-group "  >
                                        <div class="input-group-prepend">
                                            <span id="labelCantPax" class="input-group-text"><b>Pasajeros:</b></span>
                                        </div>
                                        {{Form::number("pax", "0", ["readonly"=>"readonly" ,"data-msg-required"=>"Campo Requerido",  "min"=>"0", "max"=>"999", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax", "placeholder"=>__('Cant. PAX')])}}    
                                    </div>
                                </div>
                            </div>


                            <div class="row mt-2">
                                <div  class="col-md-6 ">

                                    <div class="input-group "  id="iconDate" data-target-input="nearest">
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>Fecha:</b></span>
                                        </div>
                                        {{ Form::text("fecha_operacion", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate", "readonly" => true]) }}
                                        <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                            <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div  class="col-md-6 ">
                                    <div class="input-group "  >
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>Hora: <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                        </div>
                                        {{Form::text("hora_operacion", date("H:i"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  timer required ", "id"=>"hora_operacion" ,"required"=>"required", "readonly" => true])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-6" >
                                    <div class="input-group "  >
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>LLegada:</b></span>
                                        </div>
                                        {{Form::text("hora_llegada", date('d/m/Y H:i'), ["class"=>"form-control    date_time", "id"=>"hora_llegada" , "readonly" => true])}}
                                    </div>
                                </div>
                                <div class="col-md-6" >
                                    <div class="input-group "  >
                                        <div class="input-group-prepend">
                                            <span  class="input-group-text"><b>Salida:</b></span>
                                        </div>
                                        {{Form::text("hora_salida", date('d/m/Y H:i'),  ["class"=>"form-control date_time  ", "id"=>"hora_salida" , "readonly" => true])}}
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card card-primary card-tabs">
                                <div class="card-header p-0 pt-1">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a id="homeClick" class="nav-link active" data-toggle="pill" href="#home" role="tab" aria-controls="home" aria-selected="true">Resumen de servicios</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="listServs" data-toggle="pill" href="#servExtra" role="tab" aria-controls="servExtra" aria-selected="true">Añadir Servicios</a>
                                        </li>
                                      </ul>
                                    </ul>
                                </div>
                                <div class="card-body">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="home">
                                            <table style="width:100%" id="datFactura">
                                                <tr>
                                                    <th>Observaciones:</th>
                                                    <td id="obsVuelo" colspan="4"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>OP.</b></td>
                                                    <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                                    <td style="width:70%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                                                </tr>
                
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="home">
                                            <div id="divServsExtra" class="col-md-12 border-top mt-2" style="display: none">
                                                <div class="col-sm-12 col-md-12" >
                        
                                                    <table style="width:100%" id="tableServExt">
                                                        <tr>
                                                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                                            <td style="width:15%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                                <b>CANT.</b>
                                                            </td>
                                                            <td style="width:30%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                                <b>DESCRIPCI&Oacute;N</b>
                                                            </td>
                                                            <td style="width:45%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                                {{Form::search("serv_extra_text", "", ["class"=>" form-control", "id"=>"selectSearchItem", "placeholder"=>"Ingrese el Servicio"])}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="servExtra">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-12" >
                                                        <div class="input-group mt-2 ">
                                                            <label>Servicios Adicionales de la Llegada</label>
                                                        </div>
                                                        <table style="width: 100%">
                                                            @foreach($SERV_ESP as $key=>$value)
                                                            <tr id="tr_in_{{str_replace('.', '', $key)}}" class="tr_in">
                                                                <td style="width: 70%" ><input onClick="addServs(this, 'in')" class="in" data-code="{{str_replace('.', '', $key)}}" type="checkbox" /> {{$key.' '.Upper($value)}}</td>
                                                                <td style="width: 30%"><input disabled class="form-control in" value="0" name="in[{{$key}}]" id="in_{{str_replace('.', '', $key)}}" type="number" /></td>
                                                            </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row text-center">
                                                    <button class="btn btn-info btn-block" id="verServicios" onClick="agregarServiciosEx()" type="button"><span><li class="fa fa-plus"></li> Agregar Servicios</span></button>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }} 
                </div>
                <div class="modal-footer justify-content-between">
                    {{Form::button('<li class="fa fa-check"></li>  ',  ["onClick"=>"applyProforma()", "type"=>"button", "class"=>"btn btn-success"])}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    {{-- otro modal --}}
    <div class="modal fade" id="set-proforma"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Enviar Proforma por Correo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div id="bodyEmail" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-3 " >
                            <div  class="row">
                                <label>Ingrese el Correo:</label>
                                <input type="text" id="emails_to_send" name="emails_to_send" class="form-control" value="[]" />
                            </div>  
                            
                            <div  class="row ">
                                <div class="col-sm-12 col-md-12 text-center mt-2" >
                                    {{Form::button('<li class="fa fa-mail-bulk"></li> '.__("Enviar"),  ["onClick"=>"sendProfEmail()", "type"=>"button", "class"=>"btn btn-primary" ])}}    
                                </div>
                                
                            </div>  
                            
                            
                        </div>
                        <div class="col-sm-12 col-md-9 " >
                            <div id="panelIframeProforma" class="row">
                                
                            </div>  
                        </div>

                    </div>
                    






                </div>
                <div class="modal-footer justify-content-between">

                    
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">
        var searchClicked = false;
        var RouteDetail = '';
        var table1 = null;
        var LL = null;
        var SS = null;
        $(document).ready(function () {
            $(".select2").select2();

            $('#emails_to_send').multiple_emails({position: "bottom"});
            
            $("#modal-princ-emails").on('shown.bs.modal', function() {
                $(".multiple_emails-input").focus();
            });
            $('#modal-dee-detail-fly').on('shown.bs.modal', function () {
                $('#type_document').val('V');
            });

            // $('#iconDate').datetimepicker({
            //     format: 'L',
            //     maxDate: moment(),
            //     //minDate: moment().add(-9, 'days')
            // });

            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});

            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});

            $('.number').numeric({negative: false});

            $('#frmPrinc2').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#formOpt').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                },

                serverSide: true,
                processing: true,

                ajax: {
                    url: "{{route('facturar_pospago.proforma_operacion_lista')}}",
                    data: function (d) {
                        d.aeropuerto = $('#aeropuerto_id').val(),
                                d.rango = $('#rango').val(),
                                d.nave = $('#aeronave_id').val()


                    }
                },

                columns: [
                    {data: 'responsable'},
                    {data: 'aeronave'},
                    {data: 'tipo'},
                    {data: 'fecha_llegada'},
                    {data: 'hora_llegada'},

                    {data: 'fecha_salida'},
                    {data: 'hora_salida'},
                    {data: 'pax'},
                    {data: 'button'},
                ],

            });
            $("#set-proforma").on('shown.bs.modal', function() {
                $(".multiple_emails-input").focus();
            });
           

        });

        function agregarServiciosEx() {
            $("#homeClick").click();
            // searchClicked = true;
            
            if ($("input, select").not('.cliente').valid()) {

                if ($(".in[type=checkbox]:checked").length > 0) {
                    $("#frmPrinc2").prepend(LOADING);
                    $.post("{{route('get_servicios_operacion')}}", $("#frmPrinc2").serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        P = response.data.llegada.get_detalle.concat(response.data.salida.get_detalle);
                        showDetallesServs(P);

                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Consultar los Datos')}}"
                        });
                    });   
                } else {
                    $("#listServs").addClass("info-pulse");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Debe agregar un servicio extra para ejecutar esta acción')}}"
                    });
                    setTimeout(function() {
                        $("#listServs").removeClass("info-pulse");
                    }, 8000);
                }
            }else{
                $("#document").addClass("info-pulse");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe ingresar la información del responsable')}}"
                });
                setTimeout(function() {
                    $("#document").removeClass("info-pulse");
                }, 8000);
            }
        }

        function setProforma() {
            Swal.fire({
                title: 'Esta Seguro que Desea Emitir la Proforma?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<li class="fa fa-save"></li> Si',
                cancelButtonText: '<li class="fa fa-undo"></li>  No'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#modal-fact').modal('show');
                }
            });
        }
        
        function getDetail2(llegada, salida) {
            $.get("{{url('proforma-operacion-detalle')}}/" + llegada + "/" + salida, function (response) {
                P = response.data.llegada.get_detalle.concat(response.data.salida.get_detalle);
                showDetallesServs(P);
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Buscar el Detalle')}}"
                });
            });
        }
        function getDetail(llegada, salida, aeronave_id) {
            LL = llegada;
            SS = salida;
            $('#llegada_id').val(llegada);
            $('#salida_id').val(salida);
            $('#crypt_aeronave_id').val(aeronave_id);
            $("#modal-dee-detail-fly").modal("show");
            $("#modal-dee-detail-fly").prepend(LOADING);
            $.get("{{url('proforma-operacion-detalle')}}/" + llegada + "/" + salida, function (response) {
                $(".overlay-wrapper").remove();

                $("#frmPrinc2 #aeropuerto_id").val(response.data.llegada.aeropuerto.crypt_id);


                $("#frmPrinc2 #type_document").val(response.data.llegada.observaciones_facturacion2[0]);
                $("#frmPrinc2 #document").val(response.data.llegada.observaciones_facturacion2[1]);
                $("#frmPrinc2 #razon").val(response.data.llegada.observaciones_facturacion2[2]);
                $("#frmPrinc2 #phone").val(response.data.llegada.observaciones_facturacion2[3]);
                $("#frmPrinc2 #direccion").val(response.data.llegada.observaciones_facturacion2[4]);

                $("#frmPrinc2 #nacionalidad_id").val(response.data.llegada.tipo_vuelo_id);
                $("#frmPrinc2 #pax").val(response.data.salida.pasajeros_embarcados);

                $("#frmPrinc2 #hora_llegada").val(response.data.salida.hora_llegada2);
                $("#frmPrinc2 #hora_salida").val(response.data.salida.hora_salida2);

                $("#obsVuelo").html(response.data.llegada.observaciones + ' ' + response.data.salida.observaciones);

                P = response.data.llegada.get_detalle.concat(response.data.salida.get_detalle);
                showDetallesServs(P);



            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Buscar el Detalle')}}"
                });
            });

        }
        function sendProfEmail(){
            cant = JSON.parse($("#emails_to_send").val());
            if (cant.length > 0) {

                Swal.fire({
                    title: 'Esta Seguro que Desea Enviar el Correo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#bodyEmail").prepend(LOADING);
                        $.post("{{url('send-prof-email')}}/"+document.getElementById("seeProfDef").contentWindow.ID_PROFORMA, $("#emails_to_send").serialize()+"&_token={{csrf_token()}}" , function (response){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            
                            
                        }).fail(function (){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Enviar el Correo')}}"
                            });
                        });
                        
                        
                        
                    }
                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Ingresar el Correo')}}"
                });
                $(".multiple_emails-input").focus();
            }
        }

        function showDetallesServs(Prodservs) {
            $(".filaFactura").remove();
            sum = 0;
            base_imponible = 0;
            excento = 0;
            iva = 0;

            for (i in Prodservs) {
                sum += parseFloat(Prodservs[i]['precio'], 10);
                mult = parseFloat(Prodservs[i]['precio'], 10);
                cant = Prodservs[i]['cant'];

                if (Prodservs[i]['iva_aplicado'] > 0) {
                    base_imponible += mult;
                    iva += mult * Prodservs[i]['iva_aplicado'] / 100;
                } else {
                    excento += parseFloat(mult, 10);

                }



                tabla = '<tr style="cursor:pointer" data-toggle="tooltip" title="' + Prodservs[i]['formula2'] + '" class="filaFactura">';

                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><span onclick="deleteProdservs(\'' + Prodservs[i]['crypt_id'] + '\')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></td>';

                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + muestraFloat(cant) + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + Prodservs[i]['full_descripcion'] + ' ' + Prodservs[i]['iva2'] + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (muestraFloat(cant) + ' ' + Prodservs[i]['nomenclatura'] || '') + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                tabla += '</tr>';
                $("#datFactura").append(tabla);
            }
            TOTAL_FACTURA = sum;
            MontoGlobalBs = sum * VALOR_EURO;

            iva = parseFloat(iva.toFixed(2), 10);

            if (TOTAL_FACTURA > 0) {
                tabla = '<tr  class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>BASE IMPONIBLE &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>EXENTO &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                tabla += '</tr>';



                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>SUB TOTAL &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>IVA  &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="4"><b>TOTAL &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                tabla += '</tr>';

                $("#datFactura").append(tabla);
            }


        }
        /* function addServExt(servsExt) {
            sum = 0;
            base_imponible = 0;
            excento = 0;
            iva = 0;

            for (i in servsExt) {
                sum += parseFloat(servsExt[i]['precio'], 10);
                mult = parseFloat(servsExt[i]['precio'], 10);
                cant = servsExt[i]['cant'];

                if (servsExt[i]['iva_aplicado'] > 0) {
                    base_imponible += mult;
                    iva += mult * servsExt[i]['iva_aplicado'] / 100;
                } else {
                    excento += parseFloat(mult, 10);

                }



                tabla = '<tr style="cursor:pointer" data-toggle="tooltip" title="' + servsExt[i]['formula2'] + '" class="filaFactura">';

                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><span onclick="deleteservsExt(\'' + servsExt[i]['crypt_id'] + '\')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></td>';

                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + muestraFloat(cant) + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + servsExt[i]['full_descripcion'] + ' ' + servsExt[i]['iva2'] + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (muestraFloat(cant) + ' ' + servsExt[i]['nomenclatura'] || '') + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                tabla += '</tr>';
                $("#datFactura").append(tabla);
            }
            TOTAL_FACTURA = sum;
            MontoGlobalBs = sum * VALOR_EURO;

            iva = parseFloat(iva.toFixed(2), 10);

            if (TOTAL_FACTURA > 0) {
                tabla = '<tr  class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>BASE IMPONIBLE &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>EXENTO &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                tabla += '</tr>';



                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>SUB TOTAL &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>IVA  &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="4"><b>TOTAL &euro;</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                tabla += '</tr>';

                $("#datFactura").append(tabla);
            }


        } */
        function deleteProdservs(id) {
            Swal.fire({
                title: 'Esta Seguro que Desea Eliminar el Servicio?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No',

            }).then((result) => {
                if (result.isConfirmed) {
                    $("#modal-serv-fly").prepend(LOADING);
                    $.get("{{url('delete-item')}}/" + id, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            getDetail2(LL, SS);
                        }

                        //$("#modal-serv-fly").modal("hide");

                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesarlo')}}"
                        });
                    });
                }
            });
        }
        function buscarVuelosFact() {
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $("#frmPrinc1 [name=_method]").val("PUT");
                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    
                    if (response.status == 1){
                    
                        $(".overlay-wrapper").remove();
                        $(".filasFacturas").remove();
                        sum = 0;
                        $("#vuelos").html('');

                        for (i in response.data) {
                            if (response.data[i].total > 0) {
                                $("#all").prop("checked", true);
                                

                                tabla = '<tr style="cursor:pointer"  title="" class="filasFacturas setSelect">';
                                if (response.data[i].editable == true){
                                    sum += response.data[i].total;
                                    $("#vuelos").append('<option selected value="' + response.data[i].crypt_id + '"></option>');
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input class="prodServSelect" type="checkbox" onClick="setServ(this)" checked value="' + response.data[i].crypt_id + '" /></td>';
                                }else{
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "></td>';
                                }
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].action + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].aeronave.full_nombre_tn + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].fecha_operacion2 + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].hora_operacion + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_vuelo + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_operacion + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].piloto.full_name + '</td>';

                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].hora_llegada2) + '</td>';
                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].hora_salida2) + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].pasajeros_desembarcados) + '</td>';
                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].pasajeros_embarcados) + '</td>';





                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(response.data[i].total) + '</td>';
                                tabla += '</tr>';
                                $("#datosFacturas").append(tabla);

                                if (response.data[i].observaciones_facturacion!="" && response.data[i].observaciones_facturacion!=null){
                                    Resp = response.data[i].observaciones_facturacion.split("|");
                                    T_DOC = Resp[0];
                                    DOC = Resp[1];
                                    RAZON = Resp[2];
                                    TEL = Resp[3];
                                    DIR = Resp[4];
                                    //EMA = Resp[0];
                                }



                            }

                        }
                        
                        if (sum > 0) {
                            tabla = '<tr title="" class="filasFacturas no">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right " colspan="9"><b>TOTAL &euro;</b></td>';
                            tabla += '<td id="totalListVuelos" style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(sum) + '</td>';
                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);
                            tabla = '<tr title="" class="filasFacturas no">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; " colspan="9">    </td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right  " colspan="3">{{Form::button('<li class = "fa fa-save" > </li> EMITIR PROFORMA',  ["onClick"=>'setProforma()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    </td>';
                            //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right " colspan="3">{{Form::button('EMITIR FACTURA',  ["onClick"=>'setFactura()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    </td>';

                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);
                        }
                        //$('[data-toggle="tooltip"]').tooltip();
                    }

                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
        }
        
        function applyProforma() {
            Swal.fire({
                title: 'Esta Seguro que Desea Emitir la Proforma?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<li class="fa fa-save"></li> Si',
                cancelButtonText: '<li class="fa fa-undo"></li>  No'
            }).then((result) => {
                if (result.isConfirmed) {
                    if ($("#frmPrinc2").valid()) {
                        $("#frmPrinc2").prepend(LOADING);
                        $.post("{{route('facturar_pospago.proforma_operacion')}}", $("#frmPrinc2").serialize(), function (response){
                            $(".overlay-wrapper").remove();
                            $('#modal-fact').modal('hide');
                            $("#modal-dee-detail-fly").modal("hide");
                            $("#set-proforma").modal("show");
                            $("#panelIframeProforma").html('<iframe id="seeProfDef" name="seeProfDef" onload="clsDataResp();" style="zoom: 0.75; -moz-transform: scale(0.75); -moz-transform-origin: 0 0; -o-transform: scale(0.75); -o-transform-origin: 0 0; -webkit-transform: scale(0.75); -webkit-transform-origin: 0 0; width:100%; height: 800px; border:none" src="{{url("view-proforma")}}/'+response.data.crypt_id+'"></iframe>'); 
                            
                            table1.draw();
                        }).fail(function (){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });
                        
                        
                        
                        
                        //
                        //$(".filasFacturas").remove();
                        
                        //$("#panelIframeProforma").html('<iframe id="seeProfDef" onload="buscarVuelosFact(); clsDataResp();" name="seeProfDef"  style="width:100%; height: 600px; border:none" src=""></iframe>');
                        
                        //$("#set-proforma").modal("show");
                        //$("#frmCliente").submit();
                        //document.getElementById("frmCliente").submit();
                        
                    }
                }
            });
        }
        function findDato2() {
            if ($("#frmPrinc2 #type_document, #frmPrinc2 #document").valid()) {
                $("#modal-dee-detail-fly").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#frmPrinc2 #type_document").val() + "/" + $("#frmPrinc2 #document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    $('#frmPrinc2').validate().resetForm();
                    if (response.status == 1) {
                        $("#frmPrinc2 #cliente_id").val(response.data.crypt_id);
                        $("#frmPrinc2 #razon").val(response.data.razon_social);
                        $("#frmPrinc2 #phone").val(response.data.telefono);
                        $("#frmPrinc2 #correo").val(response.data.correo);
                        $("#frmPrinc2 #direccion").val(response.data.direccion);
                        $("#frmPrinc2 #razon, #frmPrinc2 #phone, #frmPrinc2 #correo, #frmPrinc2 #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#frmPrinc2 #razon, #frmPrinc2 #phone, #frmPrinc2 #correo, #frmPrinc2 #direccion").prop("readonly", false);
                        $("#frmPrinc2 #cliente_id, #frmPrinc2 #razon, #frmPrinc2 #phone, #frmPrinc2 #correo, #frmPrinc2 #direccion").val("");
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#frmPrinc2 #razon, #frmPrinc2 #phone, #frmPrinc2 #correo, #frmPrinc2 #direccion").prop("readonly", true);
                    $("#frmPrinc2 #cliente_id, #frmPrinc2 #razon, #frmPrinc2 #phone, #frmPrinc2 #correo, #frmPrinc2 #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }
        }
        function clsDataResp(){
            $("#document, #razon, #phone, #direccion").val("");
            $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }
        function setServ() {
            $(".filaServExt").remove();
            if ($("#frmPrinc1").valid()) {
                    $(".filaServExt").remove();
                    $("#modalCreateFly").prepend(LOADING);

                    $.post("/get-servs-ext", $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();


                        if (response.status == 1) {
                            prods = [];
                            cantidades = [];
                            $($("#prod_servd").serializeArray()).each(function () {
                                detalleServs = this.value.split(":");
                                prods.push(detalleServs[1]);
                                cantidades[detalleServs[1]] = detalleServs[0];
                            });

                            for (i in response.data) {
                                checked = $.inArray(response.data[i]['crypt_id'], prods) == -1 ? false : true;

                                tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input ' + (checked == true ? 'checked' : '') + ' type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data[i]['crypt_id'] + '" /></td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input min="1" type="number" id="' + response.data[i]['crypt_id'] + '" onChange="UpItemSelect()" value="' + (checked == true ? cantidades[response.data[i]['crypt_id']] : '0') + '" required class="form-control required" ' + (checked == true ? '' : 'disabled') + ' /></td>';
                                tabla += '<td colspan="2"  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "> ' + response.data[i]['full_descripcion'] + '</td>';


                                tabla += '</tr>';
                                $("#tableServExt").append(tabla);
                            }
                        } else {
                            Toast.fire({
                                icon: response.status,
                                title: response.message
                            });
                        }
                        $("#idServ").click();
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar los Servicios"
                        });
                    });
            }else{
                $("#document")[0].scrollIntoView();
                $("#document").focus();

            }
        }
        function addServs(obj, op) {
            $("#tr_" + op + "_" + $(obj).data("code")).toggleClass('selected');
            input = $('#' + op + '_' + $(obj).data("code"))
                    .prop('disabled', !$(obj).is(":checked"));
            if ($(obj).is(":checked")) {


                input.val(1)
                        .focus()
                        .select();
            } else {
                input.val(0)
                        .focus()
                        .select();
            }
        }
    </script>

</div>