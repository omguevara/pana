<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">


    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Emitir Proforma Masiva')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"facturar_pospago",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @method("post")


        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div  class="col-sm-12 col-md-5   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["required"=>'required',   "class"=>"form-control required select2 ", "id"=>"aeropuerto_id" ])}}

                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-3   ">
                    <div class="row">      





                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", [    "placeholder"=>"Seleccione",     "class"=>"form-control  select2 ", "id"=>"aeronave_id" ])}}

                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-3   ">
                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Fechas:</b></span>
                        </div>
                        {{Form::text("rango", $RANGO_MES, ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                        <div class="input-group-append">
                            <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>

                    </div>
                </div>
                <div  class="col-sm-12 col-md-1 mt-2   ">
                    {{Form::button('<li class="fa fa-search"></li> ',  ["onClick"=>'buscarVuelosFact()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    

                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-12 mt-4" >

                    <table style="width:100%" id="datosFacturas">
                        <tr>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{Form::checkbox("all", "1", false, ["id"=>"all", "onClick"=>"checkAllServs(this)" ,"class"=>""])}} <b>TODOS</b></td>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>SERVICIOS</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AERONAVE</b></td>
                            <td style="width:8%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>FECHA VUELO</b></td>
                            <td style="width:8%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>HORA VUELO</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TIPO VUELO</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TIPO OPERACIÓN</b></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>PILOTO</b></td>


                            {{--<td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>HORA LLEG.</b></td> 
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>HORA SAL.</b></td>--}}

                            <td style="width:2%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>PAX </b></td>
                            {{--<td style="width:2%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>PAX EMB</b></td> --}}

                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>

                    </table>

                </div>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>


    <div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Responsable de Pago</h4>

                </div>
                {{Form::open(array( "enctype"=>"multipart/form-data", "method"=>"post" ,"target"=>"seeProfDef",  "route"=>"facturar_pospago", "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frmCliente"))}}
                {{Form::hidden("cliente_id", "", ["id"=>"cliente_id"])}}
                {{Form::select('vuelos[]', [], "", ["style"=>"display:none", "id"=>"vuelos" ,"multiple"=>"multiple"])}}
                  
                <div id="modalPrincBodyFact" class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 ">

                            <div class="row">



                                <div class="col-sm-12 col-md-12">

                                    <div class="form-group">
                                        <label for="document">{{__('Document')}}</label>
                                        <div class="input-group " >
                                            <div class="input-group-prepend" >
                                                {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                            </div>
                                            {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                            <div class="input-group-append" >
                                                <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  

                                <div class="col-sm-12 col-md-12 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>{{__('Responsable')}}:</b></span>
                                        {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Responsable')])}}                                            
                                    </div>
                                </div>


                                <div class="col-sm-12 col-md-6 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                                        {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}
                                    </div>
                                </div>  



                                <div class="col-sm-12 col-md-6 mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>{{__('Correo')}}:</b></span>
                                        {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    
                                    </div>
                                </div> 
                                <div class="col-sm-12 col-md-12">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                                        {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>

                </div>
                <div class="modal-footer justify-content-between">

                    <button type="button"  id="closeModalPrinc" onClick="$('#frmCliente').trigger('reset');" class="btn btn-warning" data-dismiss="modal"><li class="fa fa-undo"></li> Cancelar</button>
                    <button type="button"  id="setModalPrinc"   onClick="applyProforma()"                            class="btn btn-primary float-right"><li class="fa fa-save"></li> Procesar</button>


                </div>
                {{Form::close()}}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-serv-fly"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Servicios Aplicados</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-xl-12 " >
                            <table style="width:100%" id="infoFactura">

                                <tr>
                                    <th style="width:150px">Hora de Llegada:</th>
                                    <td style="width:200px" id="infoHllegada"></td>
                                    <th  style="width:150px">Membresía:</th>
                                    <td colspan="3"  id="infoMembresia"></td>
                                </tr>    
                                <tr>    
                                    <th  >Hora de Salida:</th>
                                    <td id="infoHsalida"></td>
                                    <th  >Tipo de Operación:</th>
                                    <td id="infoTipoOper"></td>
                                    <th  >Usuario:</th>
                                    <td id="infoUsuario"></td>
                                </tr>
                                <tr>
                                    <th>Observaciones:</th>
                                    <td colspan="5" id="infoObs" ></td>
                                </tr> 
                            </table>
                            <table style="width:100%" id="datFactura">
                                <tr>
                                    <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>OP.</b></td>
                                    <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                    <td style="width:70%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                                </tr>

                            </table>
                        </div>
                        <div class="col-sm-12 col-md-12 col-xl-12 " >
                            <h1>Agregar Servicios</h1>
                            
                            {{Form::open([ "route"=>"get_servs_ext",  'id'=>'frmServ','autocomplete'=>'Off'])}}
                            <div class="row">
                                
                                <div class="col-sm-12 col-md-12" >

                                    <table style="width:100%" id="tableServExt">
                                        <tr>
                                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                            <td style="width:15%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                <b>CANT.</b>
                                            </td>
                                            <td  style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                <b>DESCRIPCI&Oacute;N</b>
                                            </td>
                                            <td style="width:30%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                {{Form::search("serv_extra_text", "", ["class"=>" form-control", "id"=>"selectSearchItem", "placeholder"=>"Ingrese el Nombre del Servicio"])}}
                                                
                                            </td>
                                            <td style="width:15%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                {{Form::button('<li class="fa fa-check"></li> '.__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicarServExt"])}}    
                                            </td>
                                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                                                <b>MONTO EURO</b>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="row text-center">
                            
                            </div>
                            {{ Form::close() }} 
                            
                            
                        </div>


                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    
                    


                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="modal-edit-fly"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Datos del Vuelo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div id="modal-edit-fly-body" class="modal-body">


                </div>
                <div  class="modal-footer justify-content-between">




                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="see-nota"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-lg">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Datos del Responsable de Pago</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div id="nota-body" class="modal-body">


                </div>
                <div  class="modal-footer justify-content-between">




                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>  

    <div class="modal fade" id="set-proforma"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Enviar Proforma por Correo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div id="bodyEmail" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-3 " >
                            <div  class="row">
                                <label>Ingrese el Correo:</label>
                                <input type="text" id="emails_to_send" name="emails_to_send" class="form-control" value="[]" />
                            </div>  
                            
                            <div  class="row ">
                                <div class="col-sm-12 col-md-12 text-center mt-2" >
                                    {{Form::button('<li class="fa fa-mail-bulk"></li> '.__("Enviar"),  ["onClick"=>"sendProfEmail()", "type"=>"button", "class"=>"btn btn-primary" ])}}    
                                </div>
                                
                            </div>  
                            
                            
                        </div>
                        <div class="col-sm-12 col-md-9 " >
                            <div id="panelIframeProforma"  class="row">
                             
                            </div>  
                        </div>

                    </div>
                    






                </div>
                <div class="modal-footer justify-content-between">

                    
                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    
  


    <script type="text/javascript">
        var RouteDetail = '';
        var idFactEmail = "";
        
        var T_DOC = "";
        var DOC = "";
        var RAZON = "";
        var TEL = "";
        var DIR = "";
        var EMA = "";
        
        
        
        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            $('#emails_to_send').multiple_emails({position: "bottom"});
            $('#modal-edit-fly').on('shown.bs.modal', function (e) {
                $("#modal-edit-fly-body").find(".select2").select2({dropdownParent: $("#modal-edit-fly")});
            });
            
            /*
            $('#set-proforma').on('shown.bs.modal', function (e) {
                document.getElementById("frmCliente").submit();
           });
            */
            
            
            
            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});



            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});

            
            
            
            $("#set-proforma").on('shown.bs.modal', function() {
                $(".multiple_emails-input").focus();
            });

            $('#frmCliente').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('.number').numeric({negative: false});

            
            $("#selectSearchItem").on("input", function (){
                $(".filaServExt").each(function (){
                     item = $(this).children().eq(2).text().toUpperCase();
                     if (item.indexOf( $("#selectSearchItem").val().toUpperCase()) >= 0){
                         $(this).show();
                     }else{
                         $(this).hide();
                     }
                 });
            });
            
            $("#aplicarServExt").on("click", function (){
                if ($(".selectItem:checked").length>0){
                    
                    Swal.fire({
                        title: 'Esta Seguro que Desea Agregar los Servicio?',
                        html: "Confirmaci&oacute;n",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<i class="fa fa-save"></i> Si',
                        cancelButtonText: '<li class="fa fa-undo"></li> No',

                    }).then((result) => {
                        if (result.isConfirmed) {
                            itemServEst = "";
                            $(".selectItem:checked").each(function (){
                                itemServEst += "data[]="+$(this).attr("data-crypt_id")+':'+$(this).parents("tr").find("input[type=number]").val()+"&";
                            });
                            
                            
                            get1 = RouteDetail.split("/");
                            data = itemServEst+"id="+get1[get1.length-1]+"&_token={{csrf_token()}}";
                            
                            
                            $.post("{{route('facturar_pospago.add_item')}}", data, function (response) {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                                if (response.status==1){
                                    seeDetalleVuelo(RouteDetail);
                                }

                                //$("#modal-serv-fly").modal("hide");

                            }).fail(function () {
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error al Procesarlo')}}"
                                });
                            });
                        }
                    });
                    
                    
                }
            });
            

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


        });

        function buscarVuelosFact() {
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $("#frmPrinc1 [name=_method]").val("PUT");
                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    
                    if (response.status == 1){
                    
                        $(".overlay-wrapper").remove();
                        $(".filasFacturas").remove();
                        sum = 0;
                        $("#vuelos").html('');

                        for (i in response.data) {
                            if (response.data[i].total > 0) {
                                $("#all").prop("checked", true);
                                

                                tabla = '<tr style="cursor:pointer"  title="" class="filasFacturas setSelect">';
                                if (response.data[i].editable == true){
                                    sum += response.data[i].total;
                                    $("#vuelos").append('<option selected value="' + response.data[i].crypt_id + '"></option>');
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input class="prodServSelect" type="checkbox" onClick="setServ(this)" checked value="' + response.data[i].crypt_id + '" /></td>';
                                }else{
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "></td>';
                                }
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].action + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].aeronave.full_nombre_tn + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].fecha_operacion2 + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].hora_operacion + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_vuelo + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].tipo_operacion + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i].piloto.full_name + '</td>';

                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].hora_llegada2) + '</td>';
                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].hora_salida2) + '</td>';
                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].pasajeros_desembarcados) + '</td>';
                                //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i].pasajeros_embarcados) + '</td>';





                                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(response.data[i].total) + '</td>';
                                tabla += '</tr>';
                                $("#datosFacturas").append(tabla);

                                if (response.data[i].observaciones_facturacion!="" && response.data[i].observaciones_facturacion!=null){
                                    Resp = response.data[i].observaciones_facturacion.split("|");
                                    T_DOC = Resp[0];
                                    DOC = Resp[1];
                                    RAZON = Resp[2];
                                    TEL = Resp[3];
                                    DIR = Resp[4];
                                    //EMA = Resp[0];
                                }



                            }

                        }
                        
                        if (sum > 0) {
                            tabla = '<tr title="" class="filasFacturas no">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right " colspan="9"><b>TOTAL &euro;</b></td>';
                            tabla += '<td id="totalListVuelos" style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(sum) + '</td>';
                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);
                            tabla = '<tr title="" class="filasFacturas no">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; " colspan="9">    </td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right  " colspan="3">{{Form::button('<li class = "fa fa-save" > </li> EMITIR PROFORMA',  ["onClick"=>'setProforma()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    </td>';
                            //tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right " colspan="3">{{Form::button('EMITIR FACTURA',  ["onClick"=>'setFactura()', "type"=>"button", "class"=>" btn-sm btn btn-primary"])}}    </td>';

                            tabla += '</tr>';
                            $("#datosFacturas").append(tabla);
                            
                            
                            
                            
                            
                        }
                        //$('[data-toggle="tooltip"]').tooltip();
                    }

                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
        }
        function setProforma() {
            if ($(".prodServSelect:checked").length > 0) {


                Swal.fire({
                    title: 'Esta Seguro que Desea Emitir la Proforma?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class="fa fa-save"></i> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No',

                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#document, #razon, #phone, #direccion").val("");
                        if (T_DOC!=''){
                            $("#type_document").val(T_DOC);
                            $("#document").val(DOC);
                            $("#razon").val(RAZON);
                            $("#phone").val(TEL);
                            $("#direccion").val(DIR);
                            findDato2();
                        }
                        
                        $('#modal-fact').modal('show');
                    }
                });
            } else {
                Toast.fire({
                    icon: "error",
                    title: "{{__('No Existen Vuelos Seleccionados')}}"
                });
            }
        }

        function setFactura() {
            Swal.fire({
                title: 'Esta Seguro que Desea Emitir la Factura?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No'
            }).then((result) => {
                if (result.isConfirmed) {
                }
            });
        }

        function findDato2() {


            if ($("#frmCliente #type_document, #frmCliente #document").valid()) {
                $("#modal-fact").prepend(LOADING);
                
                $.get("{{url('get-data-cliente')}}/" + $("#frmCliente #type_document").val() + "/" + $("#frmCliente #document").val(), function (response) {
                    
                    $(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#frmCliente #cliente_id").val(response.data.crypt_id);
                        $("#frmCliente #razon").val(response.data.razon_social);
                        $("#frmCliente #phone").val(response.data.telefono);
                        $("#frmCliente #correo").val(response.data.correo);
                        $("#frmCliente #direccion").val(response.data.direccion);
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                        //$("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", true);
                    $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }


        }
        function clsDataResp(){
            $("#document, #razon, #phone, #direccion").val("");
            $(".multiple_emails-input").val(document.getElementById("seeProfDef").contentWindow.CLI_EMAIL);
        }
            
        
        function applyProforma() {
            if ($("#frmCliente").valid()) {
                $("#frmCliente").prepend(LOADING);
                $.post("{{route('facturar_pospago')}}", $("#frmCliente").serialize(), function (response){
                    $(".overlay-wrapper").remove();
                    $('#modal-fact').modal('hide');
                    $("#set-proforma").modal("show");
                    $("#panelIframeProforma").html('<iframe id="seeProfDef" onload="buscarVuelosFact(); clsDataResp();" name="seeProfDef"  style=" zoom: 0.75; -moz-transform: scale(0.75); -moz-transform-origin: 0 0; -o-transform: scale(0.75); -o-transform-origin: 0 0; -webkit-transform: scale(0.75); -webkit-transform-origin: 0 0; width:100%; height: 800px; border:none" src="{{url("view-proforma")}}/'+response.data.crypt_id+'"></iframe>'); 
                    
                }).fail(function (){
                    $(".overlay-wrapper").remove();
                     Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Guardar')}}"
                    });
                });
                
                
                
                
                //
                //$(".filasFacturas").remove();
                
                //$("#panelIframeProforma").html('<iframe id="seeProfDef" onload="buscarVuelosFact(); clsDataResp();" name="seeProfDef"  style="width:100%; height: 600px; border:none" src=""></iframe>');
                
                //$("#set-proforma").modal("show");
                //$("#frmCliente").submit();
                //document.getElementById("frmCliente").submit();
                
            }
        }
        function seeDetalleVuelo(route) {
            RouteDetail = route;
            $("#modal-serv-fly").modal('show');
            $("#modal-serv-fly").prepend(LOADING);

            $.get(route, function (response) {
                //console.log(response);
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                buscarVuelosFact();
                if (response.status==1){
                    $(".overlay-wrapper").remove();
                    
            
                    $(".filaFactura").remove();
                    sum = 0;
                    base_imponible = 0;
                    excento = 0;
                    iva = 0;
                    
                    $("#infoHsalida").html(response.data.hora_salida2);
                    $("#infoHllegada").html(response.data.hora_llegada2);  
                    $("#infoMembresia").html((response.data.membresia!=null ? response.data.membresia.tipo+' '+response.data.membresia.exonerar2:''));
                    $("#infoUsuario").html(response.data.usuario );
                    $("#infoTipoOper").html(response.data.tipo_operacion );
                    
                    $("#infoObs").html(response.data.observaciones);

                    
                    
                    for (i in response.data.get_detalle) {
                        sum += parseFloat(response.data.get_detalle[i]['precio'], 10);
                        mult = parseFloat(response.data.get_detalle[i]['precio'], 10);
                        cant = response.data.get_detalle[i]['cant'];

                        if (response.data.get_detalle[i]['iva_aplicado'] > 0) {
                            base_imponible += mult;
                            iva += mult * response.data.get_detalle[i]['iva_aplicado'] / 100;
                        } else {
                            excento += parseFloat(mult, 10);

                        }



                        tabla = '<tr style="cursor:pointer" data-toggle="tooltip" title="' + response.data.get_detalle[i]['formula2'] + '" class="filaFactura">';
                        if (response.data.factura_id == null && response.data.proforma_id == null && response.data.exonerado == false){
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><span onclick="deleteItem(\'' + response.data.get_detalle[i]['crypt_id'] + '\')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></td>';
                        }else{
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">&nbsp;</td>';
                        }
                        
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + muestraFloat(cant) + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data.get_detalle[i]['full_descripcion'] + ' ' + response.data.get_detalle[i]['iva2'] + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (muestraFloat(cant) + ' ' + response.data.get_detalle[i]['nomenclatura'] || '') + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                        tabla += '</tr>';
                        $("#datFactura").append(tabla);
                    }
                    TOTAL_FACTURA = sum;
                    MontoGlobalBs = sum * VALOR_EURO;
                    
                    iva = parseFloat(iva.toFixed(2), 10);
                    
                    if (TOTAL_FACTURA > 0) {
                        tabla = '<tr  class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>BASE IMPONIBLE &euro;</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>EXENTO &euro;</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                        tabla += '</tr>';



                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>SUB TOTAL &euro;</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>IVA  &euro;</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="4"><b>TOTAL &euro;</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                        tabla += '</tr>';

                        $("#datFactura").append(tabla);
                    }
                    
                    $(".filaServExt").remove();
                    
                    //console.log(response.data.factura_id);
                    //console.log(response.data.proforma_id);
                    //console.log(response.data.exonerado);
                    if (response.data.factura_id == null && response.data.proforma_id == null && response.data.exonerado == false){
                        
                    
                        for (i in response.data_extra) {
                            checked = false;

                            tabla = '<tr title="' + response.data_extra[i]['formula2'] + '" class="filaServExt">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input ' + (checked == true ? 'checked' : '') + ' type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data_extra[i]['crypt_id'] + '" /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input min="1" type="number" id="' + response.data_extra[i]['crypt_id'] + '" value="' + (checked == true ? cantidades[response.data_extra[i]['crypt_id']] : '0') + '" required class="form-control required" ' + (checked == true ? '' : 'disabled') + ' /></td>';
                            tabla += '<td colspan="3"  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "> ' + response.data_extra[i]['full_descripcion'] + '</td>';
                            tabla += '<td   style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "> ' + muestraFloat(response.data_extra[i]['precio']) + '</td>';


                            tabla += '</tr>';
                            $("#tableServExt").append(tabla);
                        }
                    }
                    
                    
                    
                }
                //$('[data-toggle="tooltip"]').tooltip();
            }).fail(function () {
                $(".overlay-wrapper").remove();

            });
        }
        function setServ(obj) {
            $("#vuelos").children("[value=" + obj.value + "]").prop("selected", $(obj).is(":checked"));

            $(obj).parents("tr").toggleClass("alert-danger");



            if ($(".prodServSelect:checked").length == $(".prodServSelect").length) {
                $("#all").prop({"checked": true, "indeterminate": false});
            } else {
                if ($(".prodServSelect:checked").length > 0) {
                    $("#all").prop({"checked": false, "indeterminate": true});
                } else {
                    $("#all").prop({"checked": false, "indeterminate": false});
                }
            }
            recalcTotal();

        }

        function checkAllServs(obj) {
            $(".prodServSelect").prop("checked", $(obj).is(":checked"));
            $("#vuelos").children().prop("selected", $(obj).is(":checked"));

            if ($(obj).is(":checked")) {
                $(".alert-danger").removeClass("alert-danger");
            } else {
                $(".filasFacturas").not(".no").addClass("alert-danger");
            }
            recalcTotal();

        }
        function recalcTotal() {
            var monto = 0;
            $(".filasFacturas").not(".no").not(".alert-danger").each(function () {
                if ($(this).children(":first").find("input").length>0){
                    monto += usaFloat($(this).children(":last").text());
                }
                
            });
            $("#totalListVuelos").text(muestraFloat(monto));

        }

        function deleteItem(id) {
            Swal.fire({
                title: 'Esta Seguro que Desea Eliminar el Servicio?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No',

            }).then((result) => {
                if (result.isConfirmed) {
                    $("#modal-serv-fly").prepend(LOADING);
                    $.get("{{url('delete-item')}}/" + id, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status==1){
                            seeDetalleVuelo(RouteDetail);
                        }
                            
                        //$("#modal-serv-fly").modal("hide");
                        
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesarlo')}}"
                        });
                    });
                }
            });
        }
        function editVuelo(ruta) {
            $("#panel_princ").prepend(LOADING);
            $.get(ruta, function (response) {
                $(".overlay-wrapper").remove();
                
                

                $("#modal-edit-fly").modal("show");
                $("#modal-edit-fly-body").html(response);
                //$("#modal-serv-fly").modal("hide");
                //seeDetalleVuelo(RouteDetail); 
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Procesarlo')}}"
                });
            });
        }

        function exonerarVuelo(ruta) {
            Swal.fire({
                title: 'Esta Seguro que Desea Exonerar el Vuelo?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No',

            }).then((result) => {
                if (result.isConfirmed) {
                    $("#panel_princ").prepend(LOADING);
                    $.get(ruta, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        //$("#modal-serv-fly").modal("hide");
                        if (response.status==1){
                            buscarVuelosFact();
                        }
                        
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesar')}}"
                        });
                    });
                }
            });
        }
        function seeNota(Nota) {
            
            texto = Nota.split("|");
            
            if (texto.length > 1) {
                html = '<p>';
                html += '<b>RIF:</b> ' + texto[0] + texto[1] + '<br/>';
                html += '<b>NOMBRE:</b> ' + texto[2] + '<br/>';
                html += '<b>TELÉFONO:</b> ' + texto[3] + '<br/>';
                html += '<b>DIRECCIÓN:</b> ' + texto[4];




                html += '</p>';
            } else {
                html = '';
            }
            $("#nota-body").html(html);

            $("#see-nota").modal("show");

        }
        
        function selectItem(obj) {
            id = $(obj).data("crypt_id");
            if (obj.checked == true) {
                $("#" + id).val("1").prop("disabled", false).focus();
            } else {
                $("#" + id).val("0").prop("disabled", true);
            }
            //UpItemSelect();
        }
        
        function sendProfEmail(){
            cant = JSON.parse($("#emails_to_send").val());
            if (cant.length > 0) {

                Swal.fire({
                    title: 'Esta Seguro que Desea Enviar el Correo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#bodyEmail").prepend(LOADING);
                        $.post("{{url('send-prof-email')}}/"+document.getElementById("seeProfDef").contentWindow.ID_PROFORMA, $("#emails_to_send").serialize()+"&_token={{csrf_token()}}" , function (response){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            
                            
                        }).fail(function (){
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Enviar el Correo')}}"
                            });
                        });
                        
                        
                        
                    }
                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Ingresar el Correo')}}"
                });
                $(".multiple_emails-input").focus();
            }
        }
    </script>

</div>