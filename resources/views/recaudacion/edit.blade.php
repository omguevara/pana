{{Form::open(["route"=>["facturar_pospago.edit_vuelo", $Vuelo->crypt_id],'class'=>'form-horizontal',   'id'=>'frmPrincEdit1','autocomplete'=>'Off'])}}
<div class="row">
    <div  class="col-sm-4">
        <div class="row">
            <div class="input-group mt-2"  >
                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Tipo de Operación:</b></span>
                </div>
                {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], $Vuelo->nacionalidad_id, ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
            </div>

            <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                <div class="input-group-prepend">
                    <span id="labelFecha" class="input-group-text"><b>Fecha de Operaci&oacute;n:</b></span>
                </div>
                {{Form::text("fecha_operacion", $Vuelo->fecha_operacion2, ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                    <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>

            <div  class="input-group mt-2 time_estadia">
                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Cobrar Tasa?:</b></span>
                </div>
                {{Form::select('cobrar_tasa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_tasa" ,"required"=>"required"])}}
                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Cobrar Dosa?:</b></span>
                </div>
                {{Form::select('cobrar_dosa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_dosa" ,"required"=>"required"])}}
            </div>
            <div class="input-group mt-2"  >


                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Pasajeros Desembarcados:</b></span>
                </div>
                {{Form::text("pax_desembarcados", $Vuelo->pasajeros_desembarcados, ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3",  "class"=>"form-control number  ", "id"=>"pax_desembarcados", "placeholder"=>__('Cant. PAX')])}}    
            </div>

            <div class="input-group mt-2"  >


                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Pasajeros Embarcados:</b></span>
                </div>
                {{Form::text("pax_embarcados", $Vuelo->pasajeros_embarcados, ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3",  "class"=>"form-control  number  ", "id"=>"pax_embarcados", "placeholder"=>__('Cant. PAX')])}}    
            </div>
            <div class="input-group mt-2"  >
                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Hora de Llegada <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                </div>
                {{Form::text("hora_llegada", $Vuelo->hora_llegada2, ["data-msg-required"=>"Campo Requerido", "class"=>"form-control    date_time", "id"=>"hora_llegada" ])}}

            </div>

            <div class="input-group mt-2"  >
                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Hora de Salida <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                </div>
                {{Form::text("hora_salida", $Vuelo->hora_salida2, ["data-msg-required"=>"Campo Requerido", "class"=>"form-control date_time  ", "id"=>"hora_salida" ])}}

            </div>
            <div class="input-group mt-2 ">
                <div class="input-group-prepend">
                    <span class="input-group-text"><b>Observación:</b></span>
                </div>
                {{Form::text("observacion", "", ["class"=>"form-control ", "id"=>"observacion", "placeholder"=>__('Observación')])}}    
            </div>
        </div>  
    </div>  
    <div  class="col-sm-8">
        <div class="row">
            <div class="col-sm-12 col-md-12" >

                <table style="width:100%" id="tableServExt">
                    <tr>
                        <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                        <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                        <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                    </tr>
                    @foreach($prodserv_extra as $value)

                    @php
                    $checked = "";
                    $cant = "0";
                    if  (in_array($value['crypt_id'], $Vuelo->servicios_extra_select)){
                    $checked = 'checked';

                    $cant = array_search($value['crypt_id'], $Vuelo->servicios_extra_select);
                    $cant =explode(":", $cant); 
                    $cant =$cant[0]; 
                    }
                    @endphp
                    <tr title="{{$value['formula2']}}" class="filaServExt">
                        <td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input name="item[{{$value['crypt_id']}}][item]" {{$checked}} type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="{{$value['crypt_id']}}" /></td>
                        <td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input name="item[{{$value['crypt_id']}}][cant]" {{($checked=='checked'? '':'disabled')}} type="number" min="1" max="10"   id="{{$value['crypt_id']}}"  value="{{$cant}}" class="form-control"  /></td>
                        <td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">{{$value['full_descripcion']}}</td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
    <div   class="col-sm-12 text-center mt-2   ">
        {{Form::button('<li class="fa fa-save"></li> '.__("GUARDAR").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"saveData()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"save"])}}    
    </div>
</div>  
<!-- /.card -->
{{ Form::close() }} 
<script type="text/javascript">

    $(document).ready(function () {
        $('#iconDate').datetimepicker({
            format: 'L'
        });
        $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
        $('#frmPrincEdit1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

    function saveData() {
        if ($("#frmPrincEdit1").valid()) {

            if ($("#cobrar_tasa").val() == 1) {
                if ($("#hora_salida").val() == "") {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Debe Agregar la Hora de Salida')}}"
                    });
                    return false;
                }
            }
            if ($("#cobrar_tasa").val() == 0 &&  $("#cobrar_dasa").val() == 0) {
                
                Toast.fire({
                    icon: "warning",
                    title: "{{__('Debe Seleccionar Cobrar la Dosa o Tasa')}}"
                });
                return false;
                
            }
            
            if ($("#cobrar_dosa").val() == 1) {
                if ($("#hora_llegada").val() == "") {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Debe Agregar la Hora de Llegada')}}"
                    });
                    return false;
                }
            }
            
            if ($("#hora_llegada").val()==""){
                f1 = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
            }else{
                f1 = moment($("#hora_llegada").val().substr(0, 10), "DD/MM/YYYY");
            }
            
            
            if ($("#hora_salida").val()==""){
                f2 = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
            }else{
                f2 = moment($("#hora_salida").val().substr(0, 10), "DD/MM/YYYY");
            }
            
           
            f = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
            
            
            if (f>=f1 && f<=f2){
                $("#frmPrincEdit1").prepend(LOADING);
                $.post($("#frmPrincEdit1").attr("action"), $("#frmPrincEdit1").serialize(), function () {
                    $(".overlay-wrapper").remove();
                    $("#modal-edit-fly").modal("hide");
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Actualizado Correctamente')}}"
                    });
                    buscarVuelosFact();

                }).fail(function () {
                    $(".overlay-wrapper").remove();
                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error en lass Fechas')}}"
                });
            }

        }
    }

</script>            