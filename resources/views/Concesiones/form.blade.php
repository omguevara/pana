<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ asset('js/jquery370.min.js') }}" ></script>
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" >
    <script defer src="{{ asset('js/fontawesome.js') }}" ></script>
    <link rel="stylesheet" href="{{asset("css/jquery.dataTables.css")}}" />


    <style>
        .default-row {
            background-color: transparent;
        }

        .highlight-row {
            background-color: #f2dede;

        }

        .boton-derecha {
            float: right;
        }
    </style>


</head>

<body class="antialiased">
    <h5 align="center">Formuario</h5>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                @if ($errors->any())
                    <div class="flex justify-center bg-red-500 text-white px-4 py-2 w-full rounded my-2" role="alert">
                        <ul class="list-disc">
                            @foreach ($errors->all() as $error)
                                <li>
                                    <span class="text-red">{{ $error }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-12">

                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title"></h3>
                        </div>


                        {!! Form::open(['url' => '/concesion/registrar', 'method' => 'post', 'id' => 'formRegistrarConcesion']) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {!! Form::label('Local') !!}
                                    <select name="local_id" id="local_id" class="form-control">
                                        <option value="">Seleccione uno</option>
                                        @foreach ($locales as $local)
                                        <option value="{{ $local->id }}">{{ $local->numero }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Empresa') !!}
                                    <select name="empresa_id" id="empresa_id" class="form-control">
                                        <option value="">Seleccione uno</option>
                                        @foreach ($empresas as $empresa)
                                        <option value="{{ $empresa->id }}">{{ $empresa->razon_social }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Actividad') !!}
                                    <select name="actividad_id" id="actividad_id" class="form-control">
                                        <option value="">Seleccione uno</option>
                                        @foreach ($actividades as $actividad)
                                        <option value="{{ $actividad->id }}">{{ $actividad->actividad }}</option>
                                        @endforeach
                                    </select>
                                </div>



                                <div class="form-group">
                                    {!! Form::label('Ingreso Bruto') !!}
                                    {!! Form::text('ingresos_brutos', null, ['class' => 'form-control', 'maxlength' => '20']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Canon Fijo') !!}
                                    {!! Form::text('canon_fijo', null, ['class' => 'form-control', 'maxlength' => '20']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Fecha Inicio') !!}
                                    <input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control">
                                </div>
                                <div class="form-group">
                                    {!! Form::label('Fecha Fin') !!}
                                    <input type="date" name="fecha_fin" id="fecha_fin" class="form-control">
                                </div>
                                <div class="form-group mb-0">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                                        <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        {!! Form::close() !!}
                    </div>

                </div>


                <div class="col-md-6">
                </div>

            </div>

        </div>
    </section>

</body>
<script src="{{asset("js/adminlte.min.js")}}"></script>
<script src="{{asset("js/bootstrap.bundle.min.js")}}"></script>
<script src="{{asset("js/popper.min.js")}}"></script>
<script src="{{asset("js/bootstrap.min.js")}}"></script>
<script src="{{asset("js/jquery.dataTables.js")}}"></script>
<script src="{{asset("js/sweetalert2.js")}}"></script>
<script>
    $(document).ready(function() {
        $("#formRegistrarConcesion").submit(function(event) {
            event.preventDefault();
            var datos = $(this).serialize();
            $.post({
                url: "{{ route('concesiones.store') }}",
                data: datos,
                success: function(response) {
                    Swal.fire({
                        icon: response.icon,
                        title: response.title, 
                        text: response.message, 
                    })
                },
                error: function(xhr) {
                }
            });
        });
    });
</script>
</html>