<div id="panel_princ" class="col-sm-12  mt-1">
    <style>
        .subir {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .subir .file-upload {
            height: 100px;
            width: 100px;
            border-radius: 100px;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 4px solid #FFFFFF;
            overflow: hidden;
            background-image: linear-gradient(to bottom, #2590EB 50%, #FFFFFF 50%);
            background-size: 100% 200%;
            transition: all 1s;
            color: #FFFFFF;
            font-size: 50px;
        }
        .subir .file-upload input[type=file] {
            height: 100px;
            width: 100px;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            cursor: pointer;
        }

        .subir .file-upload:hover {
            background-position: 0 -100%;
            color: #2590EB;
        }
        .file-name {
            font-size: 16px;
            font-weight: bold;
            color: #333;
            background-color: #f5f5f5;
            padding: 5px 10px;
            border-radius: 5px;
            }
    </style>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Concesiones')}} </h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->




        <div id="card-body-main" class="card-body ">
            <div class="row">
                {!! Form::open() !!}
                {!! Form::hidden('local_id','', ['id' => 'local_id']) !!}
                {!! Form::hidden('disponibles', '0', ['id' => 'disponibles']) !!}
                <div class="btn-group">
                    <div class="input-group">

                        {{Form::select('aeropuerto_id', $aeropuertos, "" , ["placeholder"=>(count($aeropuertos)>1 ? 'Seleccione':null),   "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ])}}

                    </div>
                </div>
                <div class="btn-group">
                    <button type="button" class="btn btn-primary searchDisponibles">Disponibles</button>
                    <button type="button" class="btn btn-primary" onClick="deleteFiltersAll()">Borrar filtros <i class="fas fa-trash-alt"></i></button>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="row">
                <table id="clientes-table" class="display compact table-hover">
                    <thead>
                        <tr>
                            <th>OP</th>
                            <th>N° LOCAL</th>
                            <th>Mts <sup>2</sup></th>
                            <th>USO</th>
                            <th>CONCESIÓN</th>
                            <th>ACTIVIDAD</th>
                            <th title="CANON FIJO EN PETRO MENSUAL SEGUN GACETA">CANON FIJO</th>
                            <th title="CONVENIO INCENTIVO EURO">INCENTIVO</th>
                            <th>FECHA INICIO</th>
                            <th>FECHA FIN</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>


            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->

    </div>








    <!-- Modal -->
    <div class="modal fade" id="modalEditar" tabindex="-1"  data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Información Del Local Y Aeropuerto</h5>
                    <button type="button" class="close closeDatosModal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#infoLocal" role="tab" aria-controls="infoLocal" aria-selected="true">{{__('Información del Local')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#infoConcesion" role="tab" aria-controls="infoConcesion" aria-selected="false">{{__('Concesión')}}</a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#infoOtro" role="tab" aria-controls="infoOtro" aria-selected="false">{{__('Archivos')}}</a>
                                </li>

                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="infoLocal" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                    {!! Form::open(['route' => 'concesiones.updateLocal', 'id' => 'editarLocal' , 'method' => 'post']) !!}
                                    <div class="form-outline row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>N° Local</b></p>
                                                    </div>
                                                    {!! Form::text('numero_local', null, ['class' => 'form-control required', 'id' => 'numero_local']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>M<sup>2</sup></b></p>
                                                    </div>
                                                    {!! Form::text('metros', null, ['class' => 'form-control required money text-right', 'id' => 'metros']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-outline row mt-2">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>Uso</b></p>
                                                    </div>
                                                    {{Form::select('uso', $usos, "", ['class' => 'form-control required', 'placeholder' => 'Seleccione', 'id' => 'uso']) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>Otro Uso</b></p>
                                                    </div>
                                                    {!! Form::text('otro_uso', null, ['class' => 'form-control', 'id' => 'otro_uso', 'readonly' => 'true']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-outline row mt-2">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <label class="form-label" for="observacion">
                                                Observación
                                            </label>
                                            <div>
                                                {!! Form::textarea('observacion', null, ["rows"=>"4", 'class' => 'form-control', 'id' => 'observacion']) !!}
                                            </div>
                                        </div>
                                    </div>



                                    <p>
                                        <input type="radio" name="verificado" id="verificado1" value="1">Verificado<br>
                                        <input type="radio" name="verificado" id="verificado2" value="0">En modificación<br>
                                        <input type="radio" name="verificado" id="verificado3" value="-1">Por Verificar
                                    </p>
                                    {!! Form::button('Guardar', ['class' => 'btn btn-danger mt-2', 'type' => 'submit']) !!}
                                    {!! Form::close() !!}





                                </div>
                                <div class="tab-pane fade" id="infoConcesion" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    {!! Form::open(['route' => 'concesiones.store', 'id' => 'formConcesion', 'method' => 'post']) !!}
                                    {!! Form::hidden('concesion_id', null, ['class' => 'form-control required', 'id' => 'concesion_id']) !!}
                                    {!! Form::hidden('empresa_id', null, ['class' => 'form-control required', 'id' => 'empresa_id']) !!}
                                    <div class="form-outline row">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                            <div class="input-group ">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Rif</b></span>
                                                </div>
                                                <div class="input-group-prepend">
                                                    {{Form::select('tipo_rif', ["J"=>"J","G"=>"G","V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control required", "id"=>"tipo_rif" ,"required"=>"required"])}}

                                                </div>
                                                {!! Form::text('rif', null, ['class' => 'form-control required', 'id' => 'rif','placeholder' => 'INGRES EL RIF DE LA EMPRESA',]) !!}
                                                <div class="input-group-append">
                                                    {!! Form::button('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary', 'id' => 'addEmpresa']) !!}
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <p class="input-group-text"><b>Razón Social</b></p>
                                                </div>
                                                {{Form::text('razon_social', null, ['class' => 'form-control required', 'id' => 'razon_social', 'readonly' => 'true']) }}
                                            </div>

                                        </div>
                                    </div>




                                    <div class="form-outline row mt-2">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>Actividad</b></p>
                                                    </div>
                                                    {{Form::select('actividad', $actividades, "", ['class' => 'form-control required', 'placeholder' => 'Seleccione', 'id' => 'actividad']) }}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>Otra Actividad</b></p>
                                                    </div>
                                                    {{ Form::text('otro_actividad', null, ['class' => 'form-control', 'id' => 'otro_actividad', 'readonly' => 'true']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-outline row mt-2">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <p class="input-group-text"><b>Dirección</b></p>
                                                </div>
                                                {!! Form::textarea('direccion', null, ['rows'=>'2', 'class' => 'form-control required', 'id' => 'direccion', 'readonly' => 'true']) !!}
                                            </div>

                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div>
                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>Duración</b></p>
                                                    </div>
                                                    {!! Form::text('duracion', null, ['class' => 'form-control required', 'id' => 'duracion']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="form-outline row mt-2">
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <p class="input-group-text"><b>Ingreso Brutos</b></p>
                                                </div>
                                                {!! Form::text('ingresos_brutos', null, ['class' => 'form-control required money  text-right', 'id' => 'ingresos_brutos']) !!}
                                            </div>

                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <p class="input-group-text"><b>Canon Fijo</b></p>
                                                </div>
                                                {!! Form::text('canon_fijo', null, ['class' => 'form-control required money  text-right', 'id' => 'canon_fijo']) !!}
                                            </div>

                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

                                            <div class="input-group">
                                                <div class="input-group-append">
                                                    <p class="input-group-text"><b>Convenio Euro</b></p>
                                                </div>
                                                {!! Form::text('convenio_euro_integrado', null, ['class' => 'form-control required money  text-right', 'id' => 'convenio_euro_integrado']) !!}
                                            </div>

                                        </div>

                                    </div>


                                    <h2>Responsable</h2>
                                    <hr>
                                    <div class="form-outline row">

                                        <div class="form-outline row">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Cédula</b></p>
                                                        </div>
                                                        {!! Form::text('cedula', null, ['class' => 'form-control required', 'id' => 'cedula']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Nombre</b></p>
                                                        </div>
                                                        {!! Form::text('nombre', null, ['class' => 'form-control required', 'id' => 'nombre', 'maxlength' => "30"]) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Teléfono</b></p>
                                                        </div>
                                                        {!! Form::text('telefono', null, ['class' => 'telefono form-control required', 'id' => 'telefono', 'placeholder' => '(000)-000.00.00',]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Correo</b></p>
                                                        </div>
                                                        {!! Form::text('correo', null, ['class' => 'form-control', 'id' => 'correo']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div> 
                                    <div class="form-outline row mt-2">

                                        <div class="form-outline row">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Cédula 2</b></p>
                                                        </div>
                                                        {!! Form::text('cedula2', null, ['class' => 'form-control ', 'id' => 'cedula2']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Nombre 2</b></p>
                                                        </div>
                                                        {!! Form::text('nombre2', null, ['class' => 'form-control ', 'id' => 'nombre2', 'maxlength' => "30"]) !!}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Teléfono 2</b></p>
                                                        </div>
                                                        {!! Form::text('telefono2', null, ['class' => 'telefono form-control ', 'id' => 'telefono2', 'placeholder' => '(000)-000.00.00',]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Correo 2</b></p>
                                                        </div>
                                                        {!! Form::text('correo2', null, ['class' => 'form-control', 'id' => 'correo2']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                    </div> 
                                    {!! Form::button('Actualizar', ['class' => 'btn btn-danger mt-2', 'id' => 'concesionButton', 'type' => 'submit']) !!}
                                    {!! Form::close() !!}
                                </div>
                                <div class="tab-pane fade" id="infoOtro" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    <div class="row">
                                        <div class="col-lg-7">


                                            <div class="card card-widget widget-user-2 shadow-sm">

                                                <div class="widget-user-header bg-warning">
                                                    <h3>Archivos</h3>
                                                </div>
                                                <div class="card-footer p-0">
                                                    <ul  class="nav flex-column">

                                                        <div  id="listFilesUploads" class="timeline">

                                                        </div>



                                                    </ul>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-lg-5">
                                            {{Form::open(["route"=>"concesiones.uploadFile", 'files'=>'true',  'id'=>'frmUploadFile','autocomplete'=>'Off'])}}
                                            <h3>Subir Archivos</h3>
                                            <div class="row">
                                                <div class="col-12 col-sm-12">
                                                    <div class="input-group mt-2"  >
                                                        <div class="input-group-prepend">
                                                            <span  class="input-group-text"><b>Nombre del Archivo:</b></span>
                                                        </div>
                                                        {{Form::text("nombre", "", ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  required ", "id"=>"nombre" ,"required"=>"required"])}}
                                                    </div>
                                                </div>

                                                
                                                
                                                
                                                
                                                
                                                <div class="col-6 col-sm-6 mt-3">                                                       
                                                        <div class="subir">
                                                            <div class="file-upload">
                                                            <input type="file" class="custom-file-input required" accept="application/pdf" id="archivo_pdf" name="archivo_pdf" />
                                                            <i class="far fa-file-pdf"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-6 col-sm-6 mt-5">                                                       
                                                        <span class="file-name" id="name_file"></span>
                                                    </div>
                                                


                                                <div class="row">
                                                    <div class="col-12 col-sm-12 text-center mt-2">

                                                        {{Form::button('<i class="fa fa-save"></i> '.__("Subir Archivo"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}



                                                    </div>
                                                </div>
                                            </div>
                                            {{ Form::close() }}

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- /.card -->
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- Modal Historial -->
    <div class="modal fade" id="historialModal" tabindex="-1" data-backdrop="static" aria-labelledby="historialModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Historial de concesiones</h5>
                    <a href="" class="btn btn-success ml-auto" id="descargarExcel" target="_blank"><i class="far fa-file-excel"></i></a>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">  
                        <h2 class="title-modalHistorial"></h2>
                        <hr>     
                        <table id="concesion-table" class="display compact table-hover">
                            <thead>
                                <tr>
                                    <th>CONCESIÓN</th>
                                    <th>ACTIVIDAD</th>
                                    <th title="CANON FIJO EN PETRO MENSUAL SEGUN GACETA">CANON FIJO</th>
                                    <th title="CONVENIO INCENTIVO EURO">CONVENIO EURO</th>
                                    <th>FECHA INICIO</th>
                                    <th>FECHA FIN</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script type="text/javascript">
        var tablaHistorial = null;
        var tableConcesiones = null;
        $(document).ready(function () {
            $('.select2').select2();
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});

            $('input[name="duracion"]').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });
            $('.telefono').inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});

            $('#actividad').on("change", function () {
                if ($('#actividad').val() == '{{ $OTRA_ACTIVIDAD }}') {
                    $('#otro_actividad').removeAttr('readonly');
                    $('#otro_actividad').prop('required', true);
                    $('#otro_actividad').focus();
                } else {
                    $('#otro_actividad').prop('readonly', true).removeAttr('required').val('');
                }
            });
            $("#archivo_pdf").on("change", function () {
                $("#name_file").html(this.value.replace(/.*(\/|\\)/, ''));
            });

            $('#uso').on("change", function () {
                if ($('#uso').val() == '{{ $OTRO_USO }}') {
                    $('#otro_uso').removeAttr('readonly');
                    $('#otro_uso').prop('required', true);
                    $('#otro_uso').focus();
                } else {
                    $('#otro_uso').prop('readonly', true).removeAttr('required').val('');
                }
            });

            tableConcesiones = $('#clientes-table').DataTable({
                "paging": true,
                pageLength: 10,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                serverSide: true,
                processing: true,
                ajax: {
                    'url': "{{route('concesiones.data_concesiones')}}",
                    'dataType': 'json',
                    'type': 'GET',
                    "data":
                        function (d) {
                            d.disponibles = $('#disponibles').val(),
                            d.aeropuerto_id = $('#aeropuerto_id').val()
                        },

                },
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'boton'},
                    {data: 'numero'},
                    {data: 'metros'},
                    {data: 'usos_local.uso'},
                    {data: 'empresa'},
                    {data: 'actividad'},
                    {data: 'canon_fijo'},
                    {data: 'ingresos_brutos'},
                    {data: 'fecha_inicio'},
                    {data: 'fecha_fin'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });


            $('#addEmpresa').on("click", function (e) {
                let rif = $('#rif').val();
                if ($("#rif, #tipo_rif").valid() == false) {
                    $('#empresa_id').val('');
                    $('#razon_social, #telefonoEmpresa, #direccion').val('').attr('readonly', true);
                } else {
                    $("#modalEditar").prepend(LOADING);
                    $.get("{{ url('concesiones/buscarCliente/') }}/" + $('#tipo_rif').val() + '/' + rif, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.icon,
                            title: response.message
                        });
                        if (response.icon == 'success') {
                            $('#empresa_id').val(response.data.crypt_id);
                            $('#razon_social').val(response.data.full_razon_social).attr('readonly', true);
                            $('#telefonoEmpresa').val(response.data.telefono).attr('readonly', true);
                            $('#direccion').val(response.data.direccion).attr('readonly', true);
                        } else {
                            $('#empresa_id').val('');
                            $('#razon_social').val('').removeAttr('readonly').focus();
                            $('#telefonoEmpresa').val('').removeAttr('readonly');
                            $('#direccion').val('').removeAttr('readonly');
                        }
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar"
                        });
                        $(".overlay-wrapper").remove();
                    });


                }
            });
            $('.closeDatosModal').on("click", function () {
                $('#formConcesion input:not([name="_token"]').val('');
                $('#formConcesion textarea').val('');
                $('#formConcesion select').val('');
            });

            $("#formConcesion").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    event.preventDefault();
                    $("#modalEditar").prepend(LOADING);
                    $.post("{{ route('concesiones.store') }}", $('#formConcesion').serialize() + "&" + $('#local_id').serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: response.type,
                            title: "Información",
                            text: response.message,
                        });
                        if (response.status == 1) {
                            $('#concesionButton').text('Actualizar');
                            $('#concesion_id').val(response.data.crypt_id);
                            $('#local_id').val(response.data.locales.crypt_id);

                            /*
                             $('#rif').val(response.data.empresas.rif);
                             $('#razon_social').val(response.data.empresas.razon_social);
                             $('#convenio_euro_integrado').val(response.data.convenio_euro_integrado);
                             $('#actividad').val(response.data.actividad_id);
                             $('#telefonoEmpresa').val(response.data.empresas.telefono);
                             $('#direccion').val(response.data.empresas.direccion);
                             $('#ingresos_brutos').val(response.data.ingresos_brutos);
                             $('#canon_fijo').val(response.data.canon_fijo);
                             $('#duracion').val(response.data.fecha_inicio + ' - ' + response.data.fecha_fin);
                             */
                            tableConcesiones.draw();
                        }


                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops',
                            text: 'Error en la conexión...',
                        })
                    });

                }
            });
            $("#editarLocal").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    event.preventDefault();
                    $("#modalEditar").prepend(LOADING);

                    $.post("{{ route('concesiones.updateLocal') }}", $('#editarLocal').serialize() + "&" + $('#local_id').serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: response.icon,
                            title: response.title,
                            text: response.message,
                        })/* .then((result) => { 
                         location.reload();
                         }); */
                        $('#numero_local').val(response.data.numero);
                        $('#uso').val(response.data.uso_id);
                        $('#metros').val(response.data.metros);
                        $('#observacion').val(response.data.observacion);
                        $('#verificado1, #verificado2, #verificado3').removeAttr('checked');
                        if (response.data.verificado == true) {
                            $('#verificado1').prop('checked', true);
                        } else if (response.data.verificado == false) {
                            $('#verificado2').prop('checked', true);
                        } else {
                            $('#verificado3').prop('checked', true);
                        }
                        tableConcesiones.draw();
                    }).fail(function (error) {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops',
                            text: 'Error en la conexión...',
                        })
                    });

                }
            });


            $('#frmUploadFile').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            $('.searchDisponibles').on("click", function () {
                $('#disponibles').val('1');
                tableConcesiones.draw();
            });
            $('#aeropuerto_id').on("change", function () {
                tableConcesiones.draw();
            });
            


            $('#frmUploadFile').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmUploadFile').valid()) {
                    if ($("#concesion_id").val() != "") {
                        $("#frmUploadFile").prepend(LOADING);
                        var formData = new FormData(document.getElementById("frmUploadFile"));
                        formData.append('conc', $("#concesion_id").val());

                        $.ajax({
                            url: this.action,
                            type: "post",
                            dataType: "json",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });

                                if (response.status == 1) {
                                    $("#frmUploadFile  #nombre, #frmUploadFile  #archivo_pdf").val("");
                                    $("#name_file").html("");
                                }
                                getListFiles($("#concesion_id").val());


                            },
                            error: function () {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error al Subir el Archivo')}}"
                                });

                            }
                        });
                    }else{
                        Toast.fire({
                                icon: 'warning',
                                title: "Debe Registrar una Concesión"
                            });
                    }




                }else{
                    Toast.fire({
                        icon: 'error',
                        title: "Faltan Campos Requeridos"
                    });
                }
                return false;
            });

        });




        function datosModalHistorial(id, numero) {
            $('.title-modalHistorial').text('Local número: ' + numero)
            $('#descargarExcel').attr('href', "{{url('concesiones/ExcelHistorial')}}/" + id);
            if (tablaHistorial != null) {
                tablaHistorial.destroy();
            }
            $("#historialModal").prepend(LOADING);
            tablaHistorial = $('#concesion-table').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                serverSide: true,
                processing: true,
                ajax: {
                    url: "{{ url('concesiones/listHistorialConcesiones') }}/" + id,
                    "dataSrc": function (json) {
                        $(".overlay-wrapper").remove();
                        return json.data;
                    }
                },
                columns: [
                    {data: 'empresa'},
                    {data: 'actividad'},
                    {data: 'canon_fijo'},
                    {data: 'ingresos_brutos'},
                    {data: 'fecha_inicio'},
                    {data: 'fecha_fin'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });
        }


        function getListFiles(id) {
            $("#modalEditar").prepend(LOADING);
            $("#listFilesUploads").html("");
            $.get("{{ url('concesiones/lista-archivos/') }}/" + id, function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    showFiles(response.data.files);

                }
            }).fail(function () {
                Toast.fire({
                    icon: "error",
                    title: "Error al Consultar"
                });
                $(".overlay-wrapper").remove();
            });
        }


        function showFiles(data) {
            $("#listFilesUploads").html("");
            for (i in data) {
                //$("#listFilesUploads").append('<li class="nav-item"><a href="{{url("concesiones/lista-archivos")}}/' + $("#concesion_id").val() + '/' + data[i] + '" class="nav-link">' + data[i] + ' </a><span onClick="deleteFile(\'' + $("#concesion_id").val() + '\', \'' + data[i] + '\')" class="float-right badge bg-danger">X</span></li>');


                $("#listFilesUploads").append('<div>'
                        + '<i class="far fa-file-pdf bg-green"></i>'
                        + '<div class="timeline-item">'
                        + '<span style="padding:2px 5px 5px 5px !important" class="time"><button type="button" onClick="deleteFile(\'' + $("#concesion_id").val() + '\', \'' + data[i] + '\')" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i></button></span>'
                        + '<h3 class="timeline-header no-border"><a href="{{url("concesiones/lista-archivos")}}/' + $("#concesion_id").val() + '/' + data[i] + '">' + data[i] + '</a></h3>'
                        + '</div>'
                        + '</div>');

            }
        }
        function deleteFile(id, nombre) {
            $("#modalEditar").prepend(LOADING);
            $("#listFilesUploads").html("");
            $.get('{{url("concesiones/borrar-archivos")}}/' + id + '/' + nombre, function (response) {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    showFiles(response.data.files);
                }
            }).fail(function () {
                Toast.fire({
                    icon: "error",
                    title: "Error al Consultar"
                });
                $(".overlay-wrapper").remove();
            });

        }




        function datosModal(id, concesion) {
            $("#modalEditar").modal("show");
            $("#frmUploadFile  #nombre, #frmUploadFile  #archivo_pdf").val("");
            $("#name_file").html("");
            $('#local_id').val(id);
            $("#modalEditar").prepend(LOADING);
            $("#listFilesUploads").html("");
            $.get("{{ url('concesiones/buscarLocal/') }}/" + id, function (response) {
                $(".overlay-wrapper").remove();
                if (response.data.uso_id == '{{ $OTRO_USO }}') {
                    $('#otro_uso').val(response.data.otro_uso).removeAttr('readonly').prop('required', true).focus();
                } else {
                    $('#otro_uso').prop('readonly', true).removeAttr('required').val('');
                }
                $('#numero_local').val(response.data.numero);
                $('#uso').val(response.data.uso_id);
                $('#metros').val(response.data.metros);
                $('#observacion').val(response.data.observacion);
                $('#verificado1, #verificado2, #verificado3').removeAttr('checked');
                if (response.data.verificado === true) {
                    $('#verificado1').prop('checked', true);
                } else if (response.data.verificado === false) {
                    $('#verificado2').prop('checked', true);
                } else {
                    $('#verificado3').prop('checked', true);
                }
                if (response.data.concesion != null) {
                    if (response.data.concesion.actividad_id == '{{ $OTRA_ACTIVIDAD }}') {
                        $('#otro_actividad').val(response.data.concesion.otra_actividad).removeAttr('readonly');
                    }


                    $('#concesion_id').val(response.data.concesion.crypt_id);
                    if (response.data.concesion.empresa_id !=null){
                        $('#empresa_id').val(response.data.concesion.empresas.crypt_id);
                        $('#tipo_rif').val(response.data.concesion.empresas.tipo_documento);
                        $('#rif').val(response.data.concesion.empresas.documento);
                        $('#razon_social').val(response.data.concesion.empresas.razon_social);
                        $('#telefonoEmpresa').val(response.data.concesion.empresas.telefono);
                        $('#direccion').val(response.data.concesion.empresas.direccion);
                    }else{
                        $('#razon_social').val(response.data.concesion.nombre_empresa);
                    }
                    
                    $('#convenio_euro_integrado').val(response.data.concesion.convenio_euro_integrado);
                    $('#actividad').val(response.data.concesion.actividad_id);
                    $('#actividad').change();
                    
                    
                    $('#ingresos_brutos').val(response.data.concesion.ingresos_brutos);
                    $('#canon_fijo').val(response.data.concesion.canon_fijo);
                    $('#duracion').val(response.data.concesion.fecha_inicio2 + ' - ' + response.data.concesion.fecha_fin2);
                    $('#cedula').val(response.data.concesion.cedula);
                    $('#nombre').val(response.data.concesion.nombre);
                    $('#telefono').val(response.data.concesion.telefono);
                    $('#correo').val(response.data.concesion.correo);

                    showFiles(response.data.concesion.files);



                }
            }).fail(function () {
                Toast.fire({
                    icon: "error",
                    title: "Error al Consultar"
                });
                $(".overlay-wrapper").remove();
            });


            $('#concesion_id').val(concesion);
            if (concesion == '') {
                $('#concesionButton').text('Registrar');
            } else {
                $('#concesionButton').text('Actualizar');
            }
        }
        
        function deleteFiltersAll(){
                $('#aeropuerto_id').val("");
                $('#disponibles').val('0');
                tableConcesiones.search('').draw();
                $('#aeropuerto_id').trigger("change");
                //$('input[type=search]').val('').change();;
                //tableConcesiones.draw();
                
            };
        
    </script>


</div>


