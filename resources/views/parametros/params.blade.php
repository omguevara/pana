

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Parametros')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">

                @foreach($params as $key => $value)
                    @php
                        if ( $value->nombre == 'IVA'){
                            $properties_input = [ "style"=>"text-align:right", "required"=>"required", "class"=>"form-control required money", "id"=>"strtolower($value->nombre)", "placeholder"=>$value->descripcion];
                            $properties_button = [ "type"=>"submit", "class"=>"btn btn-primary mt-2"];
                        }else{
                            if ($GET_PARAMS_ONLINE->valor!=1 ){
                                $properties_input = ["style"=>"text-align:right", "required"=>"required", "class"=>"form-control required money blocked", "id"=>"strtolower($value->nombre)", "placeholder"=>$value->descripcion];
                                $properties_button = ["type"=>"submit", "class"=>"btn btn-primary mt-2 blocked"];
                            }else{
                                $properties_input = ["disabled"=>"disabled", "style"=>"text-align:right", "required"=>"required", "class"=>"blocked form-control required money", "id"=>"strtolower($value->nombre)", "placeholder"=>$value->descripcion];
                                $properties_button = ["disabled"=>"disabled", "type"=>"submit", "class"=>"btn btn-primary mt-2 blocked"];
                            }
                        }
                    @endphp
                <div class="form-group col-md-6">
                    {{Form::open(["route"=>"params", "enctype"=>"multipart/form-data", "class"=>"form-inline ", 'id'=>'frmPrinc'.$key,'autocomplete'=>'Off'])}}
                    {{Form::hidden("param", $value->nombre )}}
                    <div class="col-form-label">
                        <label for="value">{{    $value->descripcion}}</label>
                        {{Form::text('value', muestraFloat($value->valor), $properties_input)}}
                        {{Form::button(__("Actualizar"), $properties_button )}}
                    </div>
                    {{ Form::close() }}
                </div>
                @endforeach
            </div>
            <div class="row">


                <div class="form-group offset-md-3 col-md-6">
                    {{Form::open(["route"=>"params", "enctype"=>"multipart/form-data", "class"=>"form-inline ", 'id'=>'frmPrinc'.($key+1),'autocomplete'=>'Off'])}}
                    {{Form::hidden("param", 'GET_PARAMS_ONLINE' )}}
                    <div class="col-form-label">
                        <label for="value">{{    $GET_PARAMS_ONLINE->descripcion}}</label>
                        <input type="checkbox" value="{{$GET_PARAMS_ONLINE->valor}}" name="value" id="online" {{($GET_PARAMS_ONLINE->valor == 1 ? 'checked':'')}} data-bootstrap-switch data-off-color="danger" data-on-color="success" />

                    </div>
                     {{Form::button(__("Actualizar"),  ["type"=>"submit", "class"=>"btn btn-primary mt-2"])}}
                    {{ Form::close() }}
                </div>

            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->

    </div>






    <script type="text/javascript">

        //$(".money").maskMoney({"decimal": ",", "thousands": "."});
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true, precision:2});



        $(document).ready(function () {

            $("input[data-bootstrap-switch]").each(function(){
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
              });

            @foreach($params as $key => $value)
            $('#frmPrinc{{$key}}').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc{{$key}}').valid()) {
                    $("#frmPrinc{{$key}}").prepend(LOADING);
                    $.post(this.action, $("#frmPrinc{{$key}}").serialize(), function (response) {
                        $("#frmPrinc{{$key}}").find(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                    }).fail(function () {
                        $("#frmPrinc{{$key}}").find(".overlay-wrapper").remove();
                    });
                }

                return false;
            });
            $('#frmPrinc{{$key}}').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            @endforeach




            $('#frmPrinc{{($key+1)}}').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc{{$key+1}}').valid()) {
                    $("#frmPrinc{{$key+1}}").prepend(LOADING);
                    if ($("#online").prop("checked")==false){
                        p = $("#frmPrinc{{($key+1)}}").serialize()+"&value=0";
                        $(".blocked").prop("disabled", false);

                    }else{
                        $("#online").val(1);
                        p = $("#frmPrinc{{($key+1)}}").serialize();
                        $(".blocked").prop("disabled", true);
                    }
                    $.post(this.action, p, function (response) {
                        $("#frmPrinc{{($key+1)}}").find(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                    }).fail(function () {
                        $("#frmPrinc{{($key+1)}}").find(".overlay-wrapper").remove();
                    });
                }

                return false;
            });
            $('#frmPrinc{{($key+1)}}').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


        })
        ;

    </script>
</div>
