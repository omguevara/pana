<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Auditoria de Parametros')}}</h3>

            <div class="card-tools">
                 <a href="{{route('auditorias')}}" onClick="setAjax(this, event)"   class="form-control bg-success">
                    <li class="fa fa-arrow-left "></li> Regresar 
                </a>  

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            {{Form::open([ "route"=>"auditorias.parametros",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
            <div class="row">


                <div class="form-group">
                    <label>Rango de Fechas: </label>

                    
                </div>
                
                <div class="form-group">
                    

                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>
                        {{Form::text('rango', '', ["readonly"=>"readonly", "id"=>"rango", "class"=>"form-control float-right required", "placeholder"=>"Rango"])}}

                    </div>
                    <!-- /.input group -->
                </div>
                
                
                
                
                
                

                <div class="col-md-3  ">
                    <span class="fa fa-search" id="b1"></span>
                </div>



            </div>
            {{ Form::close() }} 
            <div class="row">


                <div class="col-md-12">


                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{__('N')}}</th>
                                <th>{{__('Usuario')}}</th>
                                <th>{{__('IP')}}</th>
                                <th>{{__('Fecha')}}</th>
                                <th>{{__('Parametro')}}</th>
                                <th>{{__('Valor')}}</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>







            </div>
        </div>
    </div>





    <script type="text/javascript">
        var data1 = @json($data1)
                ;

        $(document).ready(function () {
            moment.locale("es");
            $('#rango').daterangepicker({
               "maxDate":moment(),
               "locale":{
                   "applyLabel":"Aceptar",
                   "cancelLabel":"Cancelar"
               } 
            });
            
            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    //error.addClass('invalid-feedback');
                    //element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            
            $("#b1").on("click", function () {
                if ($('#frmPrinc1').valid()) {
                    params = $('#frmPrinc1').serialize();
                    $("#main-content").html(LOADING);
                    AJAX_ACTIVE = true;
                    
                    $.post("{{route('auditorias.parametros')}}", params, function (response) {
                        AJAX_ACTIVE = false;
                        $("#main-content").html(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        AJAX_ACTIVE = false;
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar"
                        });
                    });
                }
            });

            


            var table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                columns: [
                    {data: 'DT_RowIndex'},
                    {data: 'user'},
                    {data: 'ip'},
                    {data: 'fecha2'},
                    {data: 'parametro'},
                    {data: 'valor'}

                ]

                @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
                @endif

            });




        });

    </script>
</div>