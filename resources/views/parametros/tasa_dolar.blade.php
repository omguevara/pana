

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Tasa del Dolar')}}</h3>

            <div class="card-tools">



            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"tasa_dolar", "enctype"=>"multipart/form-data" , 'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="tasa">{{__('Tasa del Dolar')}}</label>
                    {{Form::text("tasa", muestraFloat($TASA), ["style"=>"text-align:right", "required"=>"required", "class"=>"form-control required money", "id"=>"tasa", "placeholder"=>__('Tasa del Dolar')])}}    

                </div>

            </div>


            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button(__("Actualizar"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    

        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>






    <script type="text/javascript">

        $(".money").maskMoney({"decimal": ",", "thousands": "."});


        $(document).ready(function () {


            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    $("#frmPrinc1").prepend(LOADING);

                    $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                    }).fail(function () {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();

                    });

                }

                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });

    </script>
</div>