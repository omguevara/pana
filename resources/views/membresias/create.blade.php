

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Agregar Aeronave de Membresía')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('membresias')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"membresias.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::hidden("aeronave_id", '', ["id"=>"aeronave_id"])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Matrícula:</b></span>
                            </div>
                            {{Form::text("matricula", "", ["data-msg-required"=>"Campo Requerido",  "maxlength"=>10 ,"onblur"=>"findPlaca()", "required"=>"required", "class"=>"form-control required  matricula ", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}
                            <div role="button" class="input-group-append">
                                <div title="Buscar " onClick="findPlaca()" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                            <div role="button" class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::text("aeronave", "", ["data-msg-required"=>"Campo Requerido",  "required"=>"required", "class"=>"form-control required   ", "id"=>"aeronave", "placeholder"=>__('Aeronave'), "readonly"=>"readonly"])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Rango de Fechas de la Membresía:</b></span>
                            </div>
                            {{Form::text("rango", "", ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Empresa:</b></span>
                            </div>
                            {{Form::text("empresa", "", ["data-msg-required"=>"Campo Requerido",  "required"=>"required", "class"=>"form-control required   ", "id"=>"empresa", "placeholder"=>__('Empresa')])}}
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo:</b></span>
                            </div>
                            {{Form::select('tipo_membresia_id', ["1"=> "Oficial",  "2"=> "Convenio",  "3"=>"Privada"], "" , ["placeholder"=>'Seleccione',   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"tipo_membresia_id" ,"required"=>"required"])}}
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Activo?:</b></span>
                            </div>
                            {{ Form::select('opciones', ['1' => 'Sí', '0' => 'No'], null, ['placeholder' => 'Seleccione', 'data-msg-required' => 'Campo Requerido', 'class' => 'form-control required select2', 'id' => 'opciones', 'required' => 'required']) }}

                        </div>

                        <div class="input-group mt-2">
                            <div id="exonera" class="input-group-prepend">
                                <span class="input-group-text"><b>Exonerar:</b></span>
                            </div> 
                            <div id="divExonerar"  class="input-group-text">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" name="exonerar[]" value="1" class="custom-control-input" id="tasa">
                                    <label class="custom-control-label" for="tasa">Tasa</label>
                                </div>
                            
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success ml-5">
                                    <input type="checkbox" name="exonerar[]" value="2" class="custom-control-input" id="dosa">
                                    <label class="custom-control-label" for="dosa">Dosa</label>
                                </div>
                            </div>

                        </div>
                        

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observaciones:</b></span>
                            </div>
                            {{Form::textarea("observacion", "", ["class"=>"form-control  ", "id"=>"observacion", "placeholder"=>__('Observaciones')])}}

                        </div>


                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>



    <div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ "route"=>"aeronaves_plus", 'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6" >
                            <div class="form-group">
                                <label for="matricula">{{__('Matrícula')}}</label>
                                {{Form::text("matricula", "", ["maxlength"=>"10", "minlength"=>"5", "required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required matricula", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="nombre">{{__('Modelo')}}</label>
                                {{Form::text("nombre", "", ["maxlength"=>"20", "required"=>"required" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en TON</span></label>
                                {{Form::text("peso_maximo", "", ["required"=>"required", "maxlength"=>"8", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button('<li class="fa fa-save"></li> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>




    <script type="text/javascript">




        $(document).ready(function () {
            $("#divExonerar ").css("min-width", "calc(100% - "+$("#exonera").width()+"px)");
            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

            $("#matricula").on("keypress", function (event) {
                //console.log(event.which);
                if (parseInt(event.which, 10) == 13) {

                    findPlaca();

                }
            });

            $(".matricula").alphanum({
                allowNumeric: true,
                allowUpper: true,
                allowLower: true,
                allowSpace: false,
                allowOtherCharSets: false


            });
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    peso = parseFloat(  $("#peso_maximo").val()  , 10 );
                    if (isNaN(peso)){
                        Toast.fire({
                            icon: "error",
                            title: "Debe Agregar el Peso de la Aeronave"
                        });
                        return false;
                    }
                    
                    if (peso==0){
                        Toast.fire({
                            icon: "error",
                            title: "Debe Agregar el Peso de la Aeronave"
                        });
                        return false;
                    }
                    
                    $("#frmNave").prepend(LOADING);
                    $.post(this.action, $("#frmNave").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmNave").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#frmPrinc1 #matricula").val(response.data.matricula);
                            findPlaca();
                            /*
                             $("#aeronave_id").append('<option  value="' + response.data.crypt_id + '">' + response.data.full_nombre + '</option>');
                             $("#aeronave_id").val(response.data.crypt_id);
                             $("#aeronave_id").trigger("reset");
                             
                             */
                            $('#frmNave').trigger("reset");
                            $("#modal-princ-aeronave").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar"
                        });
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    $("#frmPrinc1").prepend(LOADING);
                    $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('membresias')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                        //console.log(response);
                    }).fail(function () {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });


        function findPlaca() {
            if ($("#frmPrinc1 #matricula").valid()) {
                $("#panel_princ").prepend(LOADING);
                $.get("{{url('get-placa')}}/" + $("#frmPrinc1 #matricula").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    if (response.status == 1) {


                        $("#frmPrinc1 #aeronave").val(response.data.nombre + " TON: " + muestraFloat(response.data.peso_maximo / 1000, 2));


                        $("#frmPrinc1 #aeronave_id").val(response.data.crypt_id);
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Aeronave no Encontrada')}}"
                        });
                        $("#frmPrinc1 #aeronave, #frmPrinc1 #aeronave_id").val("");


                    }
                }).fail(function () {
                    $("#frmPrinc1 #aeronave, #frmPrinc1 #aeronave_id").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar la Placa')}}"
                    });
                });
            }
        }
    </script>
</div>