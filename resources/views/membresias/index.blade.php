<div id="panel_princ" class="col-sm-12  mt-3">
    <style>
        



    </style>
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Control de Membresía')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-md-12 text-center">
                    <a  class="actions_users btn btn-primary " onClick="setAjax(this, event)" href="{{route('membresias.create')}}"><li class="fa fa-plus"></li> {{__("Create")}}</a>

                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">{{__('Activos')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">{{__('Inactivos')}}</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="custom-tabs-one-tabContent">
                        <div class="tab-pane active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                            <div class="col-md-12">
                                <a  class=" btn btn-success float-right ml-5"  href="{{route('membresias.reporte')}}"><li class="fa fa-file-excel"></li> {{__("Reporte")}}</a>
                            </div>
                            <table id="example1" class="display compact ">
                                <thead>
                                    <tr>
                                        <th>{{__('Matrícula')}}</th>
                                        <th>{{__('Aeronave')}}</th>
                                        <th>{{__('Fecha Desde')}}</th>
                                        <th>{{__('Fecha hasta')}}</th>
                                        <th>{{__('Tipo')}}</th>
                                        <th>{{__('Empresa')}}</th>
                                        <th>{{__('Exonerar')}}</th>
                                        <th>{{__('Activo')}}</th>
                                        <th>{{__('Opciones')}}</th>

                                    </tr>
                                </thead>

                            </table>
                        </div>
                        <div class="tab-pane fade " id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                            <table id="example2" class="display compact ">
                                <thead>
                                    <tr>
                                        <th>{{__('Matrícula')}}</th>
                                        <th>{{__('Aeronave')}}</th>
                                        <th>{{__('Fecha Desde')}}</th>
                                        <th>{{__('Fecha hasta')}}</th>
                                        <th>{{__('Tipo')}}</th>
                                        <th>{{__('Empresa')}}</th>
                                        <th>{{__('Exonerar')}}</th>
                                        <th>{{__('Activo')}}</th>
                                        <th>{{__('Opciones')}}</th>

                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                </div>    




            </div>
        </div>
    </div>
</div>




<script type="text/javascript">


    $(function () {



        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,

            ajax: "{{route('membresias.datatable')}}",
            serverSide: true,
            processing: true,

            columns: [
                {data: 'matricula'},
                {data: 'nombre'},
                {data: 'fecha_desde2'},
                {data: 'fecha_hasta2'},
                {data: 'tipo'},
                {data: 'empresa'},
                {data: 'exonerar2'},
                {
                    data: 'activo',
                    render: function(data, type, row) {
                    if (data == true) {
                        return '<i class="fa fa-check"></i>';
                    } else {
                        return '<i class="fa fa-times"></i>';
                    }
                    }
                },
                {data: 'action'}
            ],
            language: {
                url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
            }
        });

        var table2 = $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,

            ajax: "{{route('membresias.datatable2')}}",
            serverSide: true,
            processing: true,

            columns: [
                {data: 'matricula'},
                {data: 'nombre'},
                {data: 'fecha_desde2'},
                {data: 'fecha_hasta2'},
                {data: 'tipo'},
                {data: 'empresa'},
                {data: 'exonerar2'},
                {data: 'action'}
            ],
            language: {
                url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
            }
        });




    });

</script>