

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Editar Membresía')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('membresias')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["membresias.edit", $Membresia->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}

        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-12 col-sm-12">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Matrícula:</b></span>
                            </div>
                            {{Form::text("matricula", $Membresia->matricula, ["readonly"=>"readonly",  "maxlength"=>10 ,"class"=>"form-control    ", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}


                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::text("aeronave", $Membresia->nombre, ["readonly"=>"readonly",   "class"=>"form-control    ", "id"=>"aeronave", "placeholder"=>__('Aeronave')])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Rango de Fechas de la Membresía:</b></span>
                            </div>
                            {{Form::text("rango", showDate($Membresia->fecha_desde).' - '.showDate($Membresia->fecha_hasta), ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Empresa:</b></span>
                            </div>
                            {{Form::text("empresa", $Membresia->empresa, ["data-msg-required"=>"Campo Requerido",  "required"=>"required", "class"=>"form-control required   ", "id"=>"empresa", "placeholder"=>__('Empresa')])}}
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo:</b></span>
                            </div>
                            {{Form::select('tipo_membresia_id', ["1"=> "Oficial",  "2"=> "Convenio",  "3"=>"Privada"], $Membresia->tipo_membresia_id , ["placeholder"=>'Seleccione',   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"tipo_membresia_id" ,"required"=>"required"])}}
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Activo?:</b></span>
                            </div>
                            {{ Form::select('opciones', ['1' => 'Sí', '0' => 'No'], $Membresia->activo, ['placeholder' => 'Seleccione', 'data-msg-required' => 'Campo Requerido', 'class' => 'form-control required select2', 'id' => 'opciones', 'required' => 'required']) }}

                        </div>

                        <div class="input-group mt-2">
                            <div id="exonera" class="input-group-prepend">
                                <span class="input-group-text"><b>Exonerar:</b></span>
                            </div> 
                            <div id="divExonerar"  class="input-group-text">
                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                    <input type="checkbox" {{( !(strpos($Membresia->exonerar, '1')===false) ? 'checked':'')}} name="exonerar[]" value="1" class="custom-control-input" id="tasa">
                                    <label class="custom-control-label" for="tasa">Tasa</label>
                                </div>

                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success ml-5">
                                    <input type="checkbox" {{( !(strpos($Membresia->exonerar, '2')===false) ? 'checked':'')}} name="exonerar[]" value="2" class="custom-control-input" id="dosa">
                                    <label class="custom-control-label" for="dosa">Dosa</label>
                                </div>
                            </div>

                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observaciones:</b></span>
                            </div>
                            {{Form::textarea("observacion", $Membresia->observacion, ["class"=>"form-control  ", "id"=>"observacion", "placeholder"=>__('Observaciones')])}}

                        </div>


                    </div>
                </div>
            </div>

        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>







    <script type="text/javascript">




        $(document).ready(function () {
            $("#divExonerar ").css("min-width", "calc(100% - " + $("#exonera").width() + "px)");
            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });



            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    $("#frmPrinc1").prepend(LOADING);
                    $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('membresias')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                        //console.log(response);
                    }).fail(function () {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });


     
    </script>
</div>