<div id="panel_princ" class="col-sm-12  mt-1">
    <style>
        .subir {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .subir .file-upload {
            height: 100px;
            width: 100px;
            border-radius: 100px;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 4px solid #FFFFFF;
            overflow: hidden;
            background-image: linear-gradient(to bottom, #2590EB 50%, #FFFFFF 50%);
            background-size: 100% 200%;
            transition: all 1s;
            color: #FFFFFF;
            font-size: 50px;
        }
        .subir .file-upload input[type=file] {
            height: 100px;
            width: 100px;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            cursor: pointer;
        }

        .subir .file-upload:hover {
            background-position: 0 -100%;
            color: #2590EB;
        }
        .file-name {
            font-size: 16px;
            font-weight: bold;
            color: #333;
            background-color: #f5f5f5;
            padding: 5px 10px;
            border-radius: 5px;
            }
    </style>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Inventario de Equipos')}} </h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>

            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->




        <div id="card-body-main" class="card-body ">
            <div class="row">
                {!! Form::open(['route' => 'inventario_equipos', 'id' => 'FiltrosInventario' , 'method' => 'post']) !!}
                {!! Form::hidden('local_id','', ['id' => 'local_id']) !!}
                {!! Form::hidden('disponibles', '0', ['id' => 'disponibles']) !!}
                {{-- <div class="btn-group">
                    <div class="input-group">

                        {{Form::select('aeropuerto_id', $aeropuertos, "" , ["placeholder"=>(count($aeropuertos)>1 ? 'Seleccione':null),   "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ])}}

                    </div>
                </div> --}}
                
                <div class="d-flex">
                    <div class="input-group mr-3 h-50">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-filter"></i></span>
                        </div>
                        {!! Form::select('filtro[]', [
                            '1' => 'Perdida total',
                        ], '', ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="btn-group h-50" style="width: 315px;">
                        <button type="button" class="btn btn-primary addEquipo " data-toggle="modal" data-target="#modalRegistrar">Registrar <i class="fas fa-plus"></i></button>
                        <button type="button" id="down_inventario" class="btn btn-success "><i class="fas fa-file-download"></i> Excel Inventario</button>
                        {{-- <button type="button" class="btn btn-primary" onClick="deleteFiltersAll()">Borrar filtros <i class="fas fa-trash-alt"></i></button> --}}
                    </div>
                </div>
                
                {!! Form::close() !!}
            </div>
            <div class="row">
                <table id="inventario-table" class="display compact table-hover">
                    <thead>
                        <tr>
                            <th>OP</th>
                            <th>Serial</th>
                            <th>Modelo</th>
                            <th>Descripción</th>
                            <th>Cantidad</th>
                            <th>Concesión</th>
                            <th>Ubicación Actual</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>


            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->

    </div>

    <!-- Modal Registrar -->
    <div class="modal fade" id="modalRegistrar" tabindex="-1"  data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar un nuevo equipo al inventario</h5>
                    <button type="button" class="close closeDatosModal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#addEquipoTab" role="tab" aria-controls="addEquipoTab" aria-selected="true">{{__('Registro de equipo')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#addConcesionTab" role="tab" aria-controls="addConcesionTab" aria-selected="false">{{__('Registro de Concesión')}}</a>
                                </li>

                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="addEquipoTab" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                    {!! Form::open(['route' => 'inventario_equipos', 'id' => 'addInventario' , 'method' => 'post']) !!}
                                        {!! Form::hidden('equipo_id', null, ['id' => 'equipo_id']) !!}
                                        {!! Form::hidden('_method', null, ['id' => '_method']) !!}
                                        <div class="form-outline row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Serial</b></p>
                                                        </div>
                                                        {!! Form::text('serial', null, ['class' => 'form-control required', 'id' => 'serial']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Modelo</b></p>
                                                        </div>
                                                        {!! Form::text('modelo', null, ['class' => 'form-control required', 'id' => 'modelo']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-outline row mt-2">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Descripcion</b></p>
                                                        </div>
                                                        {!! Form::text('descripcion', null, ['class' => 'form-control required', 'id' => 'descripcion']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Cantidad</b></p>
                                                        </div>
                                                        {!! Form::number('cantidad', null, ['class' => 'form-control required text-right', 'id' => 'cantidad']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outline row mt-2">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Concesión</b></p>
                                                        </div>
                                                        {{Form::select('concesion', $concesionEquipos, "", ['class' => 'form-control required', 'placeholder' => 'Seleccione', 'id' => 'concesion']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Ubicación actual</b></p>
                                                        </div>
                                                        {!! Form::text('ubicacion_actual', null, ['class' => 'form-control required', 'id' => 'ubicacion_actual']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outline row mt-3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Estado del equipo</b></p>
                                                        </div>
                                                        {{Form::select('estado', ["1" => "Operativo", "0" => "No Operativo", "-1" => "No aplica"], "", ['class' => 'form-control required', 'placeholder' => 'Seleccione', 'id' => 'estado']) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="input-group"  >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b>Pérdida Total:</b></span>
                                                    </div>
                                                    <div  id="aplicaEstCheck" class="input-group-text">
                                                        <input type="checkbox" id="irreparable" name="irreparable" data-bootstrap-switch="" data-on-text="SI" data-off-text="NO"  data-off-color="danger" data-on-color="success" value="1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="d-none perdida form-outline row mt-3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Fecha Factura</b></p>
                                                        </div>
                                                        {!! Form::text('fecha_factura', null, ['class' => 'form-control required', 'id' => 'fecha_factura']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>N° Factura</b></p>
                                                        </div>
                                                        {!! Form::text('factura', null, ['class' => 'form-control required', 'id' => 'factura']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Precio Unitario $</b></p>
                                                        </div>
                                                        {!! Form::text('unitario', null, ['class' => 'form-control required money text-right', 'id' => 'unitario']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-3 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Total $</b></p>
                                                        </div>
                                                        {!! Form::text('totalP', null, ['class' => 'form-control money text-right required', 'id' => 'totalP']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        {!! Form::button('Guardar', ['class' => 'saveEquipo btn btn-danger mt-2', 'type' => 'submit']) !!}
                                    {!! Form::close() !!}





                                </div>
                                <div class="tab-pane fade" id="addConcesionTab" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    {!! Form::open(['route' => 'inventario_equipos.get_inventario', 'id' => 'formConcesion', 'method' => 'post']) !!}
                                        {!! Form::hidden('concesion_id', null, ['class' => 'form-control required', 'id' => 'concesion_id']) !!}
                                        <div class="form-outline row mt-2">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="input-group ">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b>Nombre</b></span>
                                                    </div>
                                                    {{-- <div class="input-group-prepend">
                                                        {{Form::select('tipo_rif', ["J"=>"J","G"=>"G","V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control required", "id"=>"tipo_rif" ,"required"=>"required"])}}

                                                    </div> --}}
                                                    {!! Form::text('nombre_concesion', null, ['class' => 'form-control required', 'id' => 'nombre_concesion','placeholder' => 'INGRESE EL NOMBRE DE LA CONCESIÓN',]) !!}
                                                    <div class="input-group-append">
                                                        {!! Form::button('<i class="fa fa-search searchConcesion"></i>', ['class' => 'btn btn-primary', 'id' => 'searchConcesion']) !!}
                                                    </div>
                                                </div>

                                            </div>
                                            {{-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                                                <div class="input-group">
                                                    <div class="input-group-append">
                                                        <p class="input-group-text"><b>Razón Social</b></p>
                                                    </div>
                                                    {{Form::text('razon_social', null, ['class' => 'form-control required', 'id' => 'razon_social', 'readonly' => 'true']) }}
                                                </div>

                                            </div> --}}
                                        </div>
                                        {!! Form::button('Guardar', ['class' => 'd-none btn btn-danger mt-4', 'id' => 'concesionButton', 'type' => 'submit']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- Modal SetAlarm -->
    <div class="modal fade" id="modalSetAlarm" tabindex="-1"  data-backdrop="static" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Establecer una alarma</h5>
                    <button type="button" class="close closeDatosModal" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#infoEquiposTab" role="tab" aria-controls="infoEquiposTab" aria-selected="true">{{__('Información del equipo')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#addAlarmTab" role="tab" aria-controls="addAlarmTab" aria-selected="false">{{__('Gestión de Alarmas')}}</a>
                                </li>

                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="infoEquiposTab" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                                    <div class="infoEquipoWrapper">
                                        {!! Form::hidden('equipo_id', null, ['id' => 'equipo_id-alarm']) !!}
                                        {!! Form::hidden('_method', null, ['id' => '_method-alarm']) !!}
                                        <div class="form-outline row">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Serial</b></p>
                                                        </div>
                                                        {!! Form::text('serial', null, ['class' => 'form-control required', 'id' => 'serial-alarm', 'disabled' => true]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Modelo</b></p>
                                                        </div>
                                                        {!! Form::text('modelo', null, ['class' => 'form-control required', 'id' => 'modelo-alarm', 'disabled' => true]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-outline row mt-2">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Descripcion</b></p>
                                                        </div>
                                                        {!! Form::text('descripcion', null, ['class' => 'form-control required', 'id' => 'descripcion-alarm', 'disabled' => true]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Cantidad</b></p>
                                                        </div>
                                                        {!! Form::number('cantidad', null, ['class' => 'form-control required text-right', 'id' => 'cantidad-alarm', 'disabled' => true]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-outline row mt-2">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Concesión</b></p>
                                                        </div>
                                                        {{Form::select('concesion', $concesionEquipos, "", ['class' => 'form-control required', 'placeholder' => 'Seleccione', 'id' => 'concesion-alarm', 'disabled' => true]) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Ubicación actual</b></p>
                                                        </div>
                                                        {!! Form::text('ubicacion_actual', null, ['class' => 'form-control required', 'id' => 'ubicacion_actual-alarm', 'disabled' => true]) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-outline row mt-3">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div>
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Estado del equipo</b></p>
                                                        </div>
                                                        {{Form::select('estado', ["1" => "Operativo", "0" => "No Operativo", "-1" => "No aplica"], "", ['class' => 'form-control required', 'placeholder' => 'Seleccione', 'id' => 'estado-alarm', 'disabled' => true]) }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="input-group"  >
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><b>Pérdida Total:</b></span>
                                                    </div>
                                                    <div  id="aplicaEstCheck" class="input-group-text">
                                                        <input type="checkbox" id="irreparable-alarm" name="irreparable" data-bootstrap-switch="" data-on-text="SI" data-off-text="NO"  data-off-color="danger" data-on-color="success" value="1">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="addAlarmTab" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    <div id="register-wrapper">
                                        {!! Form::open(['route' => 'inventario_equipos.get_inventario', 'id' => 'formSetAlarm', 'method' => 'post']) !!}
                                            {!! Form::hidden('alarm_id', null, [ 'id' => 'alarm_id']) !!}
                                            {!! Form::hidden('_method', null, [ 'id' => '_method-second']) !!}
                                            {!! Form::hidden('inventario_equipo_id', null, ['class' => 'form-control required', 'id' => 'inventario_equipo_id']) !!}
                                            <label class="form-group">Registrar una alarma</label>
                                            <div class="form-outline row mt-2">
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Fecha:</b></p>
                                                        </div>
                                                        {{Form::text('fecha_revision', null, ['class' => 'form-control required', 'id' => 'fecha_revision']) }}
                                                    </div>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">

                                                    <div class="input-group">
                                                        <div class="input-group-append">
                                                            <p class="input-group-text"><b>Descripción</b></p>
                                                        </div>
                                                        {{Form::text('descripcion', null, ['class' => 'form-control required', 'id' => 'descripcion-second']) }}
                                                    </div>

                                                </div>
                                            </div>
                                            {!! Form::button('Guardar', ['class' => 'btn btn-danger mt-4', 'id' => 'setAlarmButton', 'type' => 'submit']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="observacion-wrapper d-none form-outline row mt-3">
                                        {!! Form::open(['route' => 'inventario_equipos.get_inventario', 'id' => 'formSetStatus', 'method' => 'post']) !!}
                                            <label class="form-group">Registrar una observación</label>
                                            <div class="form-group">
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <label class="form-group">Revisión realizada?</label>
                                                    <input type="checkbox" id="revision" class="form-group" name="revision" data-bootstrap-switch="" data-on-text="SI" data-off-text="NO"  data-off-color="danger" data-on-color="success">
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mt-3">
                                                    <label class="form-group">Observaciones de la Revisión</label>
                                                    <div class="summernote form-group"></div>
                                                </div>
                                            </div>
                                            {!! Form::button('Guardar', ['class' => 'btn btn-danger mt-4', 'id' => 'setAlarmButton', 'type' => 'submit']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="row mt-2">
                                        <table id="alarms-table" class="display compact table-hover">
                                            <thead>
                                                <tr>
                                                    <th>OP</th>
                                                    <th>Fecha de Revisión</th>
                                                    <th>Descripción</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>


                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var tablaHistorial = null;
        var tableInventario = null;
        var tableAlarms = null;
        $(document).ready(function () {
            $('.summernote').summernote();
            $("input[data-bootstrap-switch]").each(function(){
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
                })
                var switchElement = $('#irreparable');
                switchElement.bootstrapSwitch();
                switchElement.on('switchChange.bootstrapSwitch', function(event, state) {

                if (state == true) {
                    $('.perdida').removeClass('d-none');
                    $("#factura, #unitario, #totalP").val('').addClass('required');
                }else {
                    $('.perdida').addClass('d-none');
                    $("#factura, #unitario, #totalP").val('').removeClass('required');
                }
            });
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('.select2').select2();
            $('input[name="fecha_factura"], input[name="fecha_revision"]').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },
                "opens": "center",
                "singleDatePicker": true
            });

            tableInventario = $('#inventario-table').DataTable({
                "paging": true,
                pageLength: 10,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                serverSide: true,
                processing: true,
                ajax: {
                    'url': "{{route('inventario_equipos.get_inventario')}}",
                    'dataType': 'json',
                    'type': 'GET',
                    "data":
                        function (d) {
                            // d.disponibles = $('#disponibles').val(),
                            // d.aeropuerto_id = $('#aeropuerto_id').val()
                        },

                },
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'boton'},
                    {data: 'serial'},
                    {data: 'descripcion'},
                    {data: 'modelo'},
                    {data: 'cant'},
                    {data: 'concesiones.nombre'},
                    {data: 'ubicacion_actual'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });

            @if ($alarmaEquipos->count() > 0)
                let data = @json($alarmaEquipos)
                ;
                $("#see_information .modalTitle").text("Equipos con alertas de mantenimiento en las próximas 2 semanas");
                $("#see_information #body_information").html(
                    '<table id="AlarmEquiposTable"><th>Fecha de revisión</th><th>Descripción</th></table>',
                );
                $("#see_information #AlarmEquiposTable").DataTable({
                    "paging": true,
                    pageLength: 10,
                    "lengthChange": false,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                    "responsive": true,
                    data: data,
                    rowCallback: function (row, data) {
                        $(row).addClass(data.class);
                    },
                    columns: [
                        {data: 'equipos.serial', title: 'Serial'},
                        {data: 'equipos.modelo', title: 'Modelo'},
                        {data: 'equipos.descripcion', title: 'Descripción'},
                        {data: 'equipos.ubicacion_actual', title: 'Ubicación Actual'},
                        {data: 'fecha_revision', title: 'Fecha del mantenimiento', render : function(data){
                            return showDate(data);
                        }},
                        {data: 'descripcion', title: 'Descripción del mantenimiento'},
                    ],
                    language: {
                        url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                    }
                });

                $('#see_information').modal('show');
            @endif

            tableAlarms = $('#alarms-table').DataTable({
                "paging": true,
                pageLength: 10,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                columns: [
                    {data: 'fecha_revision'},
                    {data: 'descripcion'},
                    {data: 'descripcion'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });


            $("#down_inventario").on("click", function(){
                $("#FiltrosInventario").attr('action', '{{ route("ExcelInventarioEquipo") }}');
                $("#FiltrosInventario").submit();
            });

            $('.addEquipo').on('click', function(){
                $(".modal-title").text('Registrar un nuevo equipo al inventario');
                $("#custom-tabs-one-home-tab").text('Registro de equipo');
                $(".saveEquipo").text('Guardar');
                $("#_method").val('POST');
            });

            $('.closeDatosModal').on("click", function () {
                $('input:not([name="_token"]').val('');
                $('textarea, select' ).val('');
                $('#irreparable').prop('checked', false).val('1');
                $('#irreparable').bootstrapSwitch();
                // Establecer el valor del switch en false
                $('#irreparable').bootstrapSwitch('state', false);

            });

            $("#searchConcesion").on("click", function(){
                if ($("#nombre_concesion").val() == '') {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Debe ingresar el nombre de una concesión para poder buscar.')}}"
                    });
                } else {
                    $.get("{{ route('inventario_equipos.search_concesion') }}", $("#formConcesion").serialize(), function(response){
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if(response.type == 'success'){
                            $("#concesionButton").addClass("info-pulse").removeClass('d-none');
                            setTimeout(function() {
                                $("#concesionButton").removeClass("info-pulse");
                            }, 8000);
                        }else{
                            $("#custom-tabs-one-home-tab, #concesion").addClass("info-pulse").removeClass('d-none');
                            setTimeout(function() {
                                $("#custom-tabs-one-home-tab, #concesion").removeClass("info-pulse");
                            }, 8000);
                            $("#concesionButton").addClass('d-none');
                        }
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Consultar la información')}}"
                        });
                    });
                }
            });
            $("#formConcesion").on("submit", function(event){
                event.preventDefault(); // Prevenir el envío del formulario
                if($("#nombre_concesion").val() == ''){
                    $("#searchConcesion").addClass("info-pulse");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Debe ingresar una concesión antes de guardar')}}"
                    });
                    setTimeout(function() {
                        $("#searchConcesion").removeClass("info-pulse");
                    }, 8000);
                }else{
                    $.post("{{ route('inventario_equipos.get_inventario') }}", $('#formConcesion').serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.type == 'success') {
                            $('#concesion_id').val(response.data.crypt_id);
                            $('#concesion').append('<option value="' + response.data.crypt_id + '" selected>' + response.data.nombre + '</option>');
                            tableInventario.draw();
                            $("#custom-tabs-one-home-tab").addClass("info-pulse").removeClass('d-none');
                            setTimeout(function() {
                                $("#custom-tabs-one-home-tab").removeClass("info-pulse");
                            }, 8000);
                            $("#concesionButton").addClass('d-none');
                        }else{
                            $("#custom-tabs-one-home-tab, #concesion").addClass("info-pulse").removeClass('d-none');
                            setTimeout(function() {
                                $("#custom-tabs-one-home-tab, #concesion").removeClass("info-pulse");
                            }, 8000);
                            $("#concesionButton").addClass('d-none');
                        }
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops',
                            text: 'Error en la conexión...',
                        })
                    });
                }

            });
            $("#formSetAlarm").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    // form.preventDefault();
                    $("#modalSetAlarm").prepend(LOADING);

                    $.post("{{ route('inventario_equipos.set_alarm') }}", $('#formSetAlarm').serialize() , function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.title,
                            text: response.message,
                        })

                        $('#inventario_equipo_id').val(response.data.crypt_id);
                        $('#fecha_revision').val(response.data.fecha_revision);
                        $('#descripcion').val(response.data.descripcion);

                        // $(".modal-title").text('Editar Equipo');
                        // $("#custom-tabs-one-home-tab").text('Edición de Equipos');
                        // $(".saveEquipo").text('Editar');
                        // $("#_method").val('PUT');

                        tableAlarms.draw();
                    }).fail(function (error) {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops',
                            text: 'Error en la conexión...',
                        })
                    });

                }
            });
            // $("#formConcesion").submit(function(event) {
            // });
            $("#addInventario").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    event.preventDefault();
                    $("#modalRegistrar").prepend(LOADING);

                    $.post("{{ route('inventario_equipos') }}", $('#addInventario').serialize() , function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.title,
                            text: response.message,
                        })
                        if(response.type != 'success'){
                            $("#unitario, #totalP").addClass("info-pulse").removeClass('d-none');
                            setTimeout(function() {
                                $("#unitario, #totalP").removeClass("info-pulse");
                            }, 8000);
                        }else{
                            $('#equipo_id').val(response.data.crypt_id);
                            $('#serial').val(response.data.serial);
                            $('#descripcion').val(response.data.descripcion);
                            $('#modelo').val(response.data.modelo);
                            $('#cantidad').val(response.data.cant);
                            $('#concesion').val(response.data.concesiones.crypt_id).prop('selected', true);
                            $('#ubicacion_actual').val(response.data.ubicacion_actual);
                            $('#estado').val(response.data.estado).prop('selected', true);

                            $(".modal-title").text('Actualizar Datos del Equipo');
                            $("#custom-tabs-one-home-tab").text('Actualización de Equipos');
                            $(".saveEquipo").html('<i class="fas fa-save"></i> Actualizar');
                            $("#_method").val('PUT');
                        }

                        tableInventario.draw();
                    }).fail(function (error) {
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops',
                            text: 'Error en la conexión...',
                        })
                    });

                }
            });


            $('.searchDisponibles').on("click", function () {
                $('#disponibles').val('1');
                tableInventario.draw();
            });
            $('#aeropuerto_id').on("change", function () {
                tableInventario.draw();
            });

        });

        function editModal(id, serial, modelo, descripcion, cantidad, concesion, ubicacion, estado, irreparable, fecha, factura, unitario, totalP) {
            $('#equipo_id').val(id);
            $('#serial').val(serial);
            $('#modelo').val(modelo);
            $('#descripcion').val(descripcion);
            $('#cantidad').val(cantidad);
            $('#concesion').val(concesion).prop('selected', true);
            $('#ubicacion_actual').val(ubicacion);
            $('#estado').val(estado).prop('selected', true);
            $(".modal-title").text('Actualizar Equipo');
            $("#custom-tabs-one-home-tab").text('Edición de Equipos');
            $(".saveEquipo").html('<i class="fas fa-save"></i> Actualizar');
            $("#_method").val('PUT');


            if(irreparable == true){
                $('.perdida').removeClass('d-none');
                $('#irreparable').bootstrapSwitch('state', true);
                $('#fecha_factura').val(showDate(fecha));
                $('#factura').val(factura);
                $('#unitario').val(muestraFloat(unitario));
                $('#totalP').val(muestraFloat(totalP));
            }else $('.perdida').addClass('d-none');$("#irreparable, #fecha_factura, #factura, #unitario, #totalP").val('');


            $("#modalRegistrar").modal("show");

            // $("#frmUploadFile  #nombre, #frmUploadFile  #archivo_pdf").val("");
            // $("#name_file").html("");
            // $('#local_id').val(id);
            // $("#modalEditar").prepend(LOADING);
            // $("#listFilesUploads").html("");
            // $.get("{{ url('concesiones/buscarLocal/') }}/" + id, function (response) {
            //     $(".overlay-wrapper").remove();
            //     if (response.data.uso_id == '{{ $OTRO_USO }}') {
            //         $('#otro_uso').val(response.data.otro_uso).removeAttr('readonly').prop('required', true).focus();
            //     } else {
            //         $('#otro_uso').prop('readonly', true).removeAttr('required').val('');
            //     }
            //     $('#numero_local').val(response.data.numero);
            //     $('#uso').val(response.data.uso_id);
            //     $('#metros').val(response.data.metros);
            //     $('#observacion').val(response.data.observacion);
            //     $('#verificado1, #verificado2, #verificado3').removeAttr('checked');
            //     if (response.data.verificado === true) {
            //         $('#verificado1').prop('checked', true);
            //     } else if (response.data.verificado === false) {
            //         $('#verificado2').prop('checked', true);
            //     } else {
            //         $('#verificado3').prop('checked', true);
            //     }
            //     if (response.data.concesion != null) {
            //         if (response.data.concesion.actividad_id == '{{ $OTRA_ACTIVIDAD }}') {
            //             $('#otro_actividad').val(response.data.concesion.otra_actividad).removeAttr('readonly');
            //         }


            //         $('#concesion_id').val(response.data.concesion.crypt_id);
            //         if (response.data.concesion.empresa_id !=null){
            //             $('#empresa_id').val(response.data.concesion.empresas.crypt_id);
            //             $('#tipo_rif').val(response.data.concesion.empresas.tipo_documento);
            //             $('#rif').val(response.data.concesion.empresas.documento);
            //             $('#razon_social').val(response.data.concesion.empresas.razon_social);
            //             $('#telefonoEmpresa').val(response.data.concesion.empresas.telefono);
            //             $('#direccion').val(response.data.concesion.empresas.direccion);
            //         }else{
            //             $('#razon_social').val(response.data.concesion.nombre_empresa);
            //         }

            //         $('#convenio_euro_integrado').val(response.data.concesion.convenio_euro_integrado);
            //         $('#actividad').val(response.data.concesion.actividad_id);
            //         $('#actividad').change();


            //         $('#ingresos_brutos').val(response.data.concesion.ingresos_brutos);
            //         $('#canon_fijo').val(response.data.concesion.canon_fijo);
            //         $('#duracion').val(response.data.concesion.fecha_inicio2 + ' - ' + response.data.concesion.fecha_fin2);
            //         $('#cedula').val(response.data.concesion.cedula);
            //         $('#nombre').val(response.data.concesion.nombre);
            //         $('#telefono').val(response.data.concesion.telefono);
            //         $('#correo').val(response.data.concesion.correo);

            //         showFiles(response.data.concesion.files);



            //     }
            // }).fail(function () {
            //     Toast.fire({
            //         icon: "error",
            //         title: "Error al Consultar"
            //     });
            //     $(".overlay-wrapper").remove();
            // });


            // $('#concesion_id').val(concesion);
            // if (concesion == '') {
            //     $('#concesionButton').text('Registrar');
            // } else {
            //     $('#concesionButton').text('Actualizar');
            // }
        }
        function setAlarmModal(id, serial, modelo, descripcion, cantidad, concesion, ubicacion, estado) {
            $('#inventario_equipo_id').val(id);
            $('#serial-alarm').val(serial);
            $('#modelo-alarm').val(modelo);
            $('#descripcion-alarm').val(descripcion);
            $('#cantidad-alarm').val(cantidad);
            $('#concesion-alarm').val(concesion).prop('selected', true);
            $('#ubicacion_actual-alarm').val(ubicacion);
            $('#estado-alarm').val(estado).prop('selected', true);
            $("#card-body-main").prepend(LOADING);
            $("#setAlarmButton").text('Guardar');
            $("#_method-second").val('POST');
            tableAlarms.destroy();
            // $.get("{{ route('inventario_equipos.search_alarm') }}", {id:id}, function (response){
                $(".overlay-wrapper").remove();
                // if (response.type == 'success') {
                    tableAlarms = $("#alarms-table").DataTable({
                        "paging": true,
                        pageLength: 10,
                        "lengthChange": false,
                        "searching": true,
                        "ordering": true,
                        "info": true,
                        "autoWidth": false,
                        "responsive": true,
                        serverSide: true,
                        processing: true,
                        ajax: {
                            'url': "{{route('inventario_equipos.search_alarm')}}/" + id,
                            'dataType': 'json',
                            'type': 'GET',
                            "data":
                                function (d) {
                                    // d.disponibles = $('#disponibles').val(),
                                    // d.aeropuerto_id = $('#aeropuerto_id').val()
                                },

                        },
                        rowCallback: function (row, data) {
                            $(row).addClass(data.class);
                        },
                        columns: [
                            {data: 'boton'},
                            {data: 'fecha'},
                            {data: 'descripcion'},
                        ],
                        language: {
                            url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                        }
                    });
                // }
            // });

            // $(".modal-title").text('Edición de Alarmas');
            // $("#custom-tabs-one-home-tab").text('Edición de Equipos');
            // $(".saveEquipo").text('Editar');
            // $("#_method-alarm").val('PUT');
            $("#modalSetAlarm").modal("show");

        }
        function editAlarmModal(id, fecha, descripcion){
            $("#alarm_id").val(id);
            $("#fecha_revision").val(fecha);
            $("#descripcion-second").val(descripcion);
            $("#setAlarmButton").text('Actualizar');
            $("#_method-second").val('PUT');
        }
        function deleteAlarmModal(id){
            Swal.fire({
                title: 'Está Seguro que Desea Eliminar la Alerta?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.post({
                        url: "{{ route('inventario_equipos.set_alarm') }}",
                        type: "DELETE",
                        data: { 'id' : id, "_token" : "{{ csrf_token() }}" },
                        success: function(response){
                            Toast.fire({
                                icon: response.type,
                                title: response.title,
                                text: response.message,
                            })
                            tableAlarms.draw();
                        }
                    }).fail(function(){
                        $(".overlay-wrapper").remove();
                        Swal.fire({
                            icon: "error",
                            title: "{{__('Error al consultar')}}"
                        });
                    });
                }
            });
        }
        function deleteFiltersAll(){
            $('#aeropuerto_id').val("");
            $('#disponibles').val('0');
            tableInventario.search('').draw();
            $('#aeropuerto_id').trigger("change");
            //$('input[type=search]').val('').change();;
            //tableInventario.draw();

        }
        function setObservacionModal(){
            $("#register-wrapper").toggleClass('d-none');
            $(".observacion-wrapper").toggleClass('d-none');
        }

    </script>


</div>


