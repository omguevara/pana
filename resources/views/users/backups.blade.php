<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Respaldos')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
               
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">
                <table id="example1" class="display compact">
                    <thead>
                        <tr>
                           
                            <th>{{__('Name')}}</th>
                            <th>{{__('Op')}}</th>
                           

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

          



            </div>
        </div>
    </div>





    <script type="text/javascript">
        var data1 = @json($files)
                ;
       
        $(document).ready(function () {






            var table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                columns: [
                    {data: 'nombre'},
                    {data: function (row){
                       result = '<form action="{{route("list_backups")}}" method="post">';
                       result += '<input type="hidden" name="file" value="'+row.nombre+'" />';
                       result += '<input type="hidden" name="_token" value="{{csrf_token()}}" />';
                       result += '<button title="Descargar" type="submit" class="btn btn-primary btn-sm"><span class="fa fa-download"></span></button>';
                       result += '</form>';
                      return result;      
                    }},
                    
                ]
                        
                @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
                @endif
                         
            });
            
        });

    </script>
</div>