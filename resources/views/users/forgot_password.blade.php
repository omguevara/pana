@extends('layouts.login')
@section('content')
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="{{route('login')}}" class="h1"><b>P.A.N.A.</b></a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">¿Olvidaste tu contraseña? Aquí puede recuperar fácilmente una nueva contraseña.</p>
            {{Form::open(array("route"=>"user.forgot_password", "method"=>"post", "autocomplete"=>"off", "id"=>"frmLogin"))}}

            <div class="input-group mb-3">
                <input type="email" name="email" id="email" class="form-control" placeholder="Ingrese su Correo">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block">Solicitar Nueva Contraseña</button>
                </div>
                <!-- /.col -->
            </div>
            </form>
            <p class="mt-3 mb-1">
                <a href="{{route('login')}}">Login</a>
            </p>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- jQuery -->
<script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- SweetAlert2 -->
<script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>

<script src="{{url('/')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('/')}}/plugins/jquery-validation/additional-methods.min.js"></script>


<!-- AdminLTE App -->
<script src="{{url('/')}}/dist/js/adminlte.min.js"></script>


<script type="text/javascript">
$(function () {
    @if ($msg !='')
    Swal.fire(
            'Excelente!',
            '{{$msg}}',
            'info'
            );
    @endif

});

</script>



@endsection