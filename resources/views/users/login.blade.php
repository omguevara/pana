@extends('layouts.login')
@section('content')

<div class="login-box">
<style>
    #password{
        background: linear-gradient(to right, white 50%, yellow 50%);
        background-position: left bottom;
        background-size: 200% 100%;
        transition: all 0.1s ease-out;
        transition-timing-function: linear;
    }

    .pShow{
        background-position: right bottom !important;
        font-weight: bold;
    }
    
</style>

    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">

            <img src="{{url('/')}}/dist/img/logo.png" alt="AdminLTE Logo" class="img-fluid" >


        </div>
        <div class="card-body">
            <div id="boxLogin">
            <p class="login-box-msg">{{__('Sign in to start your session')}}</p>

            {{Form::open(array("route"=>"login", "method"=>"post", "autocomplete"=>"off", "id"=>"frmLogin"))}}

            <div class="input-group mb-3">
                {{Form::text('username','',  array('required'=>"required", "data-msg-required"=>__("Required Field"), "class"=>"form-control", "placeholder"=>__("User")))}}

                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-user"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                {{Form::password('password',  array('id'=>'password', 'required'=>"required", "data-msg-required"=>__("Required Field"), "class"=>"form-control", "placeholder"=>__("Password")))}}

                <div class="input-group-append">
                    <div onmousedown="pShow()" onmouseup="pHide()"  class="input-group-text">
                        <span id="ojo" class="fas fa-eye-slash"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-7">

                    <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-user-lock"></i> {{__('Sign In')}}</button>

                </div>
                <!-- /.col -->
                <div class="col-5">

                </div>
                <!-- /.col -->
            </div>
            <p class="mb-1 mt-2">
                    <a onClick="showRP()" href="javascript:void(0)">Olvidé mi Contraseña</a>
            </p>
            {{Form::close()}}
            </div>

            <div style="display:none" id="boxPassw">
                <p class="login-box-msg">¿Olvidaste tu contraseña? Aquí puede recuperar fácilmente una nueva contraseña.</p>
                {{Form::open(array("route"=>"user.forgot_password", "method"=>"post", "autocomplete"=>"off", "id"=>"frmLostPwd"))}}

                <div class="input-group mb-3">
                    <input type="email" name="email" id="email" data-msg-required="Campo Requerido" data-msg-email="Correo Errado" class="form-control required" placeholder="Ingrese su Correo">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-block">Solicitar Nueva Contraseña</button>
                    </div>
                    <!-- /.col -->
                </div>
                </form>
                <p class="mt-3 mb-1">
                    <a onClick="showlog()"  href="javascript:void(0)">Login</a>
                </p>
            </div>
            
            

            <div class="input-group mb-3">
                <!-- Google reCaptcha v3 -->
                {!! RecaptchaV3::field('register') !!}

            </div>







            <!-- /.social-auth-links -->

        </div>
        <!-- /.card-body -->
    </div>





    <!-- /.card -->
</div>
<!-- /.login-box -->




<!-- jQuery -->
<script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- SweetAlert2 -->
<script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>

<script src="{{url('/')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('/')}}/plugins/jquery-validation/additional-methods.min.js"></script>


<!-- AdminLTE App -->
<script src="{{url('/')}}/dist/js/adminlte.min.js"></script>


<script type="text/javascript">
    //document.getElementById("password").value= '123456';
    //document.getElementById("frmLogin").submit();
    
    
        $(function () {
        
        @if (isset($msg) != '')
            @if ($msg != '')
                @if (!is_array($msg) )
                Swal.fire(
                        'Excelente!',
                        '{{$msg}}',
                        'info'
                        );
                @endif
            @endif
        @endif

            @if (Session::has('msg'))
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            Toast.fire({
                icon: "{{Session::get('msg')['type']}}",
                title: "{{Session::get('msg')['message']}}"
            });
            @endif
            
            @if (isset($msg))
        @if (is_array($msg))
                Swal.fire(
                'INFORMACIÓN',
                '{{$msg["message"]}}',
                '{{$msg["type"]}}'
                );
        @endif
        @endif
                
                
            $('#frmLogin').validate({

                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        $('#frmLostPwd').validate({

            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        
        });
        function pShow() {
            $("#ojo").removeClass('fa-eye-slash');
            $("#ojo").addClass('fa-eye');
            $("#password").attr("type", "text").addClass("pShow");
        }
        function pHide() {
                $("#ojo").removeClass('fa-eye');
                $("#ojo").addClass('fa-eye-slash');
            $("#password").attr("type", "password").removeClass("pShow");
    }
            
            
    function showRP(){
        $("#boxLogin").slideUp("fast", function (){
            $("#boxPassw").slideDown("fast");
        });
        }
    function showlog(){
        $("#boxPassw").slideUp("fast", function (){
            $("#boxLogin").slideDown("fast");
        });
    }
    
</script>


@endsection
