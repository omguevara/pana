

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Create')}} Usuario</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('users')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"users.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="type_document">{{__('Tipo de Documento')}}</label>
                        {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="document">{{__('Document')}}</label>
                        {{Form::text("document", "", ["maxlength"=>"12", "required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="name_user">{{__('Name')}}</label>
                        {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required", "id"=>"name_user", "placeholder"=>__('Name')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="surname_user">{{__('Surname')}}</label>
                        {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required", "id"=>"surname_user", "placeholder"=>__('Surname')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="profile_id">{{__('Perfil')}}</label>
                        {{Form::select("profile_id", $profiles,  "", ["required"=>"required", "class"=>"form-control required select2", "id"=>"profile_id", "placeholder"=>__('Select')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="phone">{{__('Phone')}}</label>
                        {{Form::text("phone", "", ["class"=>"form-control required  phone", "required"=>"required",  "id"=>"phone", "placeholder"=>__('Phone')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="email">{{__('Email')}}</label>
                        {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="aeropuerto_id">{{__('Aeropuerto')}}</label>
                        {{Form::select("aeropuerto_id", $aeropuertos,  "", [ "class"=>"form-control select2 ", "id"=>"aeropuerto_id", "placeholder"=>__('Select')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="password">{{__('Password')}}</label>
                        {{Form::password("password", ["required"=>"required", "class"=>"form-control required", "id"=>"password", "placeholder"=>__('Password')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="confirm_password">{{__('Confirm Password')}}</label>
                        {{Form::password("confirm_password",  ["required"=>"required", "class"=>"form-control required", "id"=>"confirm_password", "placeholder"=>__('Confirm Password')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-12">

                    <div class="form-group">
                        <label for="aeropuerto_extra_id">{{__('Aeropuertos Extra')}}</label>
                        {{Form::select("aeropuerto_extra_id[]", $aeropuertos,  "", ["multiple"=>"multiple",  "class"=>"form-control select2 ", "id"=>"aeropuerto_extra_id"])}}

                    </div>
                </div>
            </div>



            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});

            $('.number').numeric({negative: false});

            $(".select2").select2();

            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    $("#frmPrinc1").prepend(LOADING);
                    $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('users')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                        //console.log(response);
                    }).fail(function () {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });

    </script>
</div>
