@extends('layouts.login')
@section('content')
<div class="login-box">
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="{{route('login')}}" class="h1"><b>P.A.N.A.</b></a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">Ingrese su Nueva Contraseña.</p>
            {{Form::open(array("route"=>["change_password", $Usuario->crypt_id] , "method"=>"post", "autocomplete"=>"off", "id"=>"frmLogin"))}}

            <div class="input-group mb-3">
                <input type="password" name="password" id="password" class="form-control" placeholder="Ingrese su Nueva Contraseña">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" name="password2" id="password2" class="form-control" placeholder="Vuelva a Ingresar su Contraseña">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block">Aceptar</button>
                </div>
                <!-- /.col -->
            </div>
            </form>
            <p class="mt-3 mb-1">
                <a href="{{route('login')}}">Login</a>
            </p>
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- jQuery -->
<script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- SweetAlert2 -->
<script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>

<script src="{{url('/')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{url('/')}}/plugins/jquery-validation/additional-methods.min.js"></script>


<!-- AdminLTE App -->
<script src="{{url('/')}}/dist/js/adminlte.min.js"></script>


<script type="text/javascript">
$(function () {
   

});

</script>



@endsection