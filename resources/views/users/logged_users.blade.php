<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Usuarios Conectados')}}</h3>

            <div class="card-tools">
                
                <a href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">


                <div class="col-md-12">


                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{__('N')}}</th>
                                <th>{{__('Actions')}}</th> 
                                <th>{{__('Usuario')}}</th>
                                <th>{{__('IP')}}</th>
                                <th>{{__('Fecha Entrada')}}</th>
                                <th>{{__('Plataforma')}}</th>
                                <th>{{__('Navegador')}}</th>
                               

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>







            </div>
        </div>
    </div>





    <script type="text/javascript">
        var data1 = @json($data1)
                ;
    
        $(document).ready(function () {






            var table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                columns: [
                    {data: 'DT_RowIndex'},
                    {data: 'action'},
                    {data: 'user'},
                    {data: 'ip'},
                    {data: 'fecha'},
                    {data: 'platform'},
                    {data: 'browser'},
                    
                    
                ]

                @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
                @endif

            });




        });

    </script>
</div>