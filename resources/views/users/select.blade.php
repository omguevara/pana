@extends('layouts.principal')
@section('content')

<div class="col-sm-12">
    <div class="row mb-2">

        <h1>Seleccione Su Opci&oacute;n</h1>
    </div>

</div>




<div  class="col-sm-12  col-md-6 offset-xl-2 col-xl-4">
    <div style="min-height: 410px" id="card1" class="card card-default">
        <div class="card-header">
            <h3 class="card-title">
                <i class="fas fa-bullhorn"></i>
                {{__('Noticias')}}
            </h3>
        </div>
        <div class="card-body  ">
            <div class="row ">
                <div class="col-lg-12 marquee-up">
                    <div class="callout callout-info ">
                        <h5>Noticias </h5>

                        <p>Bienvenido a la Nueva Plataforma Administrativa Nacional Aeroportuaria</p>
                        <p>Sistema PANA!.</p>
                    </div>
                </div>


            </div>

        </div>
    </div>
</div>


<div class="col-sm-12  col-md-6  col-xl-4">




    <div id="card1" class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Opciones</h3>
        </div>
        <div class="card-body ">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>Pantallas</h3>

                            <p>Pantallas</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-tv"></i>
                        </div>
                        <a href="{{route('informacionAeropuerto')}}" class="small-box-footer">Ir <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 offset-lg-3">
                    <!-- small card -->
                    <div class="small-box bg-primary">
                        <div class="inner">
                            <h3>%</h3>

                            <p>Sistema</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="{{route('login')}}" class="small-box-footer">
                            Ir <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>

                <div class="col-lg-6 offset-lg-3">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>Proforma</h3>

                            <p>Proforma</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-shopping-cart"></i>
                        </div>
                        <a href="{{route('reserva_general')}}" class="small-box-footer">Ir <i class="fas fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- /.card-body -->

</div>
<!-- /.card -->




<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function () {


    });


</script>  




@endsection