

<div id="panel_princ" class="col-sm-12 col-md-5 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Clave')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"change_pwd",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">


            <div class="row">
                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Usuario:</b></span>
                    </div>
                    {{Form::text("usuario", $data->document.' '. $data->full_nombre, ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required ", "id"=>"usuario", "placeholder"=>__('Usuario')])}}    
                </div>
                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Perfil:</b></span>
                    </div>
                    {{Form::text("perfil", $data->profile->name_profile, ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required ", "id"=>"perfil", "placeholder"=>__('Perfil')])}}    
                </div>
                
                <div class="input-group mt-2"  >


                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Contraseña Actual')}}</b></span>
                    </div>
                    {{Form::password("before_passwd",  [ "required"=>"required", "class"=>"form-control required", "id"=>"before_passwd", "placeholder"=>__('Contraseña Actual')])}}
                </div>
                
                <div class="input-group mt-2"  >


                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Nueva Contraseña')}}</b></span>
                    </div>
                    {{Form::password("passwd", [ "required"=>"required", "class"=>"form-control required", "id"=>"passwd", "placeholder"=>__('Nueva Contraseña')])}}
                </div>

                <div class="input-group mt-2"  >


                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Confirmar Contraseña')}}</b></span>
                    </div>
                    {{Form::password("re_passwd",  ["data-rule-equalTo"=>"#passwd",  "required"=>"required", "class"=>"form-control required", "id"=>"re_passwd", "placeholder"=>__('Confirmar Contraseña')])}}
                </div>

               
                

            </div>


            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Actualizar"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>


    <script type="text/javascript">

        $(document).ready(function () {

            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($("#frmPrinc1").valid()) {
                    if ($("#passwd").val() == $("#re_passwd").val()) {

                        $("#frmPrinc1").prepend(LOADING);
                        $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });

                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                            if (response.status==1){
                                $('#panel_princ').remove();
                            }
                            //console.log(response);
                        }).fail(function () {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Las Contraseña No son Iguales')}}"
                        });
                    }
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });

    </script>
</div>
