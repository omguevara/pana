<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Users')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
               
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">

                @if (in_array("create", Auth::user()->getActions()[$route_id])) 
                <a  class="actions_users btn btn-primary float-left" onClick="setAjax(this, event)" href="{{route('users.create')}}"><li class="fa fa-plus"></li> {{__("Create")}}</a>
                @endif
                <div class="col-md-12">
                    <div class="card card-primary card-tabs">
                        <div class="card-header p-0 pt-1">
                            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">{{__('Usuarios Activos')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">{{__('Usuarios Inactivos')}}</a>
                                </li>

                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-one-tabContent">
                                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">

                                    <table id="example1" class="display compact">
                                        <thead>
                                            <tr>
                                                <th>{{__('Document')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Surname')}}</th>
                                                <th>{{__('Phone')}}</th>
                                                <th>{{__('Perfil')}}</th>
                                                <th>{{__('Aeropuerto')}}</th>
                                                <th>{{__('Actions')}}</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>




                                </div>
                                <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                                    <table id="example2" class="display compact">
                                        <thead>
                                            <tr>
                                                <th>{{__('Document')}}</th>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Surname')}}</th>
                                                <th>{{__('Phone')}}</th>
                                                <th>{{__('Perfil')}}</th>
                                                <th>{{__('Aeropuerto')}}</th>
                                                <th>{{__('Actions')}}</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>







            </div>
        </div>
    </div>





    <script type="text/javascript">
        var data1 = @json($data1)
                ;
        var data2 = @json($data2)
                ;
        $(document).ready(function () {






            var table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                columns: [
                    {data: 'document'},
                    {data: 'name_user'},
                    {data: 'surname_user'},
                    {data: 'phone'},
                    {data: 'profile.name_profile'},
                    {data: function (dat){
                        if (dat.aeropuerto!=null){
                            return dat.aeropuerto.nombre
                        }else{
                            return "";
                        }
                    }},
                    {data: 'action'}
                ],language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
                
                         
            });
            var table2 = $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data2,
                columns: [
                    {data: 'document'},
                    {data: 'name_user'},
                    {data: 'surname_user'},
                    {data: 'phone'},
                    {data: 'profile.name_profile'},
                    {data: function (dat){
                        if (dat.aeropuerto!=null){
                            return dat.aeropuerto.nombre
                        }else{
                            return "";
                        }
                    }},
                    {data: 'action'}
                ]
                        
                @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
                @endif
                         
                         
            });
        });

    </script>
</div>