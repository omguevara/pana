@extends('layouts.principal')
@section('content')


<div class="row mb-2">
    <div class="col-sm-12">
        <h1>Registro de Solicitud</h1>
    </div>

</div>


<div class="col-md-12">
    <div id="card1" class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Reserva</h3>
        </div>
        <div class="card-body p-0">
            <div class="bs-stepper">
                <div class="bs-stepper-header" role="tablist">
                    <!-- your steps here -->
                    <div class="step" data-target="#form1">
                        <button type="button" class="step-trigger" role="tab" aria-controls="form1" id="form1-trigger">
                            <span class="bs-stepper-circle">1</span>
                            <span class="bs-stepper-label">Datos B&aacute;sicos</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#form2">
                        <button type="button" class="step-trigger" role="tab" aria-controls="form2" id="form2-trigger">
                            <span class="bs-stepper-circle">2</span>
                            <span class="bs-stepper-label">Datos del Vuelo</span>
                        </button>
                    </div>
                    <div class="line"></div>
                    <div class="step" data-target="#form3">
                        <button type="button" class="step-trigger" role="tab" aria-controls="form3" id="form3-trigger">
                            <span class="bs-stepper-circle">3</span>
                            <span class="bs-stepper-label">FIN</span>
                        </button>
                    </div>

                </div>
                <div class="bs-stepper-content">

                    <div id="form1" class="content" role="tabpanel" aria-labelledby="form1-trigger">
                        <div class="card card-info  col-sm-12 offset-md-2 col-md-8 offset-xl-3 col-xl-6 ">
                            <div class="card-header">
                                <h3 class="card-title">Datos B&aacute;sicos</h3>
                            </div>
                            {{Form::open(array("route"=>"reserva_general", "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"tabFrm1"))}}
                            @method('PUT')
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="tipo_vuelo_id" class="col-sm-4 col-form-label">Tipo de Vuelo</label>
                                    <div class="col-sm-8">
                                        {{Form::select('tipo_vuelo_id', $TIPOS_VUELOS, "", [ "class"=>"form-control ", "id"=>"tipo_vuelo_id" ,"required"=>"required"])}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="condicion_id" class="col-sm-4 col-form-label">Condici&oacute;n</label>
                                    <div class="col-sm-8">
                                        {{Form::select('condicion_id', $TIPOS_CONDICIONES, "", [ "class"=>"form-control ", "id"=>"condicion_id" ,"required"=>"required"])}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="numero_vuelo" class="col-sm-4 col-form-label">N&uacute;mero de Vuelo</label>
                                    <div class="col-sm-8">
                                        {{Form::text("numero_vuelo", "", ["required"=>"required", "class"=>"form-control required", "id"=>"numero_vuelo", "placeholder"=>__('Numero de Vuelo')])}}    
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">{{__('Fecha')}}</label>
                                    <div class="col-sm-8">
                                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                                            {{Form::text("fecha", date("d/m/Y"), ["class"=>"form-control datetimepicker-input required", "readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="origen_id" class="col-sm-4 col-form-label">Origen</label>
                                    <div class="col-sm-8">
                                        {{Form::select('origen_id', $ORIGEN, "", [ "class"=>"form-control ", "id"=>"origen_id" ,"required"=>"required"])}}
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="hora_inicio" class="col-sm-4 col-form-label"  >Hora Salida</label>
                                    <div class="col-sm-8">
                                        <div class="input-group date" id="timepicker" data-target-input="nearest">

                                            {{Form::text("hora_inicio", "", ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control datetimepicker-input required", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                                            <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>



                                <div class="form-group row">
                                    <label for="destino_id" class="col-sm-4 col-form-label">Destino</label>
                                    <div class="col-sm-8">
                                        {{Form::select('destino_id', $DESTINO, "", [ "class"=>"form-control ", "id"=>"destino_id" ,"required"=>"required"])}}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="hora_fin" class="col-sm-4 col-form-label"  >Hora Llegada</label>
                                    <div class="col-sm-8">
                                        <div class="input-group date" id="timepicker2" data-target-input="nearest">

                                            {{Form::text("hora_fin", "", ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control datetimepicker-input required", "id"=>"hora_fin", "placeholder"=>__('Hora Llegada')])}}    
                                            <div class="input-group-append" data-target="#timepicker2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.input group -->
                                </div>




                            </div>
                            <div class="card-footer text-center">
                                {{Form::button(__("Siguiente"),  ["type"=>"button", "class"=>"btn float-right btn-primary", "id"=>"sig1"])}}    

                            </div>
                        </div>

                        {{Form::close()}}

                    </div>
                    <div id="form2" class="content" role="tabpanel" aria-labelledby="form2-trigger">

                        <button class="btn btn-primary" onclick="stepper.previous()">Previous</button>
                        <button class="btn btn-primary" onclick="stepper.next()">Next</button>
                    </div>
                    <div id="form3" class="content" role="tabpanel" aria-labelledby="form3-trigger">
                        <button class="btn btn-primary" onclick="stepper.previous()">Previous</button>
                        <button type="submit" class="btn btn-primary">Fin</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">

        </div>
    </div>
    <!-- /.card -->
</div>



<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        window.stepper = new Stepper(document.querySelector('.bs-stepper'));
        $('#iconDate').datetimepicker({
            format: 'L',
            "minDate": moment()
        });
        $('#timepicker, #timepicker2').datetimepicker({
            format: 'LT'
        });

        $("#sig1").on("click", function () {
            if ($("#tabFrm1").valid()) {
                $("#card1").prepend(LOADING);

                $.post("{{route('reserva_general')}}", $("#tabFrm1").serialize(), function (response) {
                    console.log(response);
                }).fail(function () {

                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error')}}"
                    });
                });
            }


        });
        
        $('#tabFrm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });
</script>  


@endsection