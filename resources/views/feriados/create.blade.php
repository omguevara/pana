

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Agregar Feriado')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('feriados')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"feriados.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label >{{__('Fecha ')}}</label>

                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.form-group -->
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="nombre">{{__('Descripción')}}</label>
                        {{Form::text("nombre", "", ["required"=>"required", "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Nombre')])}}

                    </div>
                </div>

            </div>
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>





<script type="text/javascript">




    $(document).ready(function () {

         $('#iconDate').datetimepicker({
            format: 'L',

        });


        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('feriados')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
