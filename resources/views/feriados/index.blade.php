<div id="panel_princ" class="col-sm-4 offset-md-4 mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Feriados')}}</h3>

            <div class="card-tools">
                
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

             
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">
                @if (in_array("create", Auth::user()->getActions()[$route_id])) 

                <a  class="actions_users btn btn-primary float-left" onClick="setAjax(this, event)" href="{{route('feriados.create')}}"><li class="fa fa-plus"></li> {{__("Create")}}</a>
                @endif
                <div class="col-md-12">

                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{__('Fecha')}}</th>
                                <th>{{__('Descripción')}}</th>
                                <th>{{__('Opciones')}}</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>


             

                </div>

            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    var data1 = @json($data1)
            ;
    
    $(function () {






        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            columns: [
                {data: 'fecha2'},
                {data: 'nombre'},
                {data: 'action'}
            ]
            @if (app()-> getLocale() != "en")
            ,language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
            }
            @endif

        });
        
        

    });
    
</script>