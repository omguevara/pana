<script>

    $(document).ready(function () {

        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $(".money, .money_v").maskMoney({"decimal": ",", "thousands": ".", "allowZero": true});
        $("#finalize").on("click", function () {
            limpiar();
            $("#modal-data-print").modal('hide');
        });
        $("#saveDataCategory").on("click", function () {

            if ($("#frmPrinc2").valid()) {

                $("#modal-content-data-categorias").prepend(LOADING);
                $.post("{{route('boletos')}}", $("#frmPrinc2").serialize(), function (response) {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $("#bodyDataCategorias").html("");
                        //$("#finalize").prop("disabled", true);
                        $("#modal-data-categorias").modal('hide');
                        $("#modal-data-print").modal('show');
                        $("#modal-content-data-print").prepend(LOADING);
                        $("#finalize").prop("disabled", true);
                        facturaPrinted = false;
                        boletoPrinted = false;
                        $("#iframeBol").html(' <iframe onload="javascript:boletoLoad =true; displayBotons()" id="boletoPrint"   style=" overflow-x: hidden; overflow-y: auto; border:none; height: 285px; width: 100%;" src="{{url("view-boleto")}}/' + $("#frmPrinc2 #boleto_id").val() + '" ></iframe>');
                        $("#iframeFact").html('<iframe onload="javascript:facturaLoad=true; displayBotons()" id="facturaPrint"  style=" overflow-x: hidden; overflow-y: auto; border:none; height: 285px; width: 100%;" src="{{url("view-factura")}}/' + $("#frmPrinc2 #boleto_id").val() + '" ></iframe>');
                    } else {

                    }

                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Guardar')}}"
                    });
                });
            }



        });
        
        
        


        $("#selectTimer").on("click", function () {

            if ($(".timerSet:checked").length > 0) {
                $("#modal-content-horarios").prepend(LOADING);
                $("#selectTimer").hide();
                $("#horario_ascenso").val($(".timerSet:checked").eq(0).val());
                $(".overlay-wrapper").remove();
                $("#modal-horarios").modal('hide');
                t = $("#horario_ascenso").val().split(":");
                h = t[0];
                m = t[1];
                $("#timerSelected").html(showTime(h, m));
                $(".data_fact").prop("disabled", false);
            } else {
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Seleccionar un Horario')}}"
                });
            }



        });
        $("#borrar").on("click", function () {

            Swal.fire({
                title: 'Esta Seguro?',
                text: "Seguro que Desea Limpiar los Datos!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {
                    limpiar();
                }
            });
        });
        $("#full-screen").click();
        $(".phone").inputmask({"mask": "(9999)-999.99.99", "clearIncomplete": true});
        $(".money").maskMoney({"decimal": ",", "thousands": ".", "allowZero": true});
        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                //error.addClass('invalid-feedback');
                //element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        $('#frmPrinc2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                //error.addClass('invalid-feedback');
                //element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        $("input:text").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {
                if ($(this).hasClass("money") && this.value == '') {
                    this.value = '0,00';
                }
                if ($(this).valid()) {
                    var nextI = $("input:text").index(this) + 1;
                    next = $("input:text").eq(nextI);
                    next.focus();
                }
                getMontoAbonado();
            }
        });
        $("#estado_id").on("change", function () {

            if (this.value == 0) {
                $("#pais_id").prop("disabled", false);
                $("#direccion").val('');
            } else {
                $("#direccion").val($(this).children(":selected").text());
                $("#pais_id").val(1).prop("disabled", true);
            }

        });
    });

</script>