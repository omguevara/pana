<script type="text/javascript">



    var Taquilla = @json($Taq)
            ;
    var TASA_CAMBIO_DOLLAR = parseFloat('{{$tasa}}', 10);
    var TASA_CAMBIO_EURO = parseFloat('{{$euro}}', 10);
    var IVA = parseFloat('{{$iva}}', 10);
    var Bolivares = 0;
    var Dolares = 0;
    var Cant = 0;
    var boletoLoad = false;
    var facturaLoad = false;
    var boletoPrinted = false;
    var facturaPrinted = false;
    
    
    
    
    
    
    
    function displayBotons() {
        if (boletoLoad == true && facturaLoad == true) {
            boletoLoad = false;
            facturaLoad = false;
            $(".overlay-wrapper").remove();
            $("#impBol, #impFact").prop("disabled", false);
            $("#finalize").prop("disabled", true);
        }
    }

    

    function selectRuta(obj) {
        $("#ruta").children().remove();
        $("#timerSelected").html("00:00 XX");
        $("#horario_ascenso, #cantidad_personas").val("");
        $(".data_fact").prop("disabled", true);
        $("#montoVuelto").html("");
        $("#ruta").append('<option value="' + $(obj).attr("data-id") + '" selected="selected">' + obj.dataset.nombre + '</option>');
        $(".badgeRuta, .catAgregada").remove();
        $(obj).prepend('<span class="badgeRuta select-category-badge-num  badge bg-success float-left " style="position:absolute;" title=""><li class="fa fa-check fa-1x"></li></span>');
        $("#ruta_seleccionada").html(obj.dataset.nombre);
        if ($(".descCat").length == 0) {
            $("#contentCategorias").append('<div  class="widget-user-image descCat"><img src="{{url("dist/img/categorias.webp")}}" alt="Categorias" class="img-fluid elevation-2"></div>');
            $("#contentCategorias").append('<h3  class="widget-user-username descCat">Seleccione la Categoría</h3>');
        }


        //$("#detalleFact, #dataTur").empty();
        //$("#categoria").val("");
        //totales();
        $("#bodyCategorias").empty();
        for (i in Taquilla['rutas']) {
            if (Taquilla['rutas'][i]['crypt_id'] == obj.dataset.id) {
                content = '';
                for (j in Taquilla['rutas'][i]['categorias']) {
                    content += '<div class="border-2 border-primary pointer-event" onClick="selectCategoria(1, this, 0)" style="float:left; max-width:100px; cursor:pointer; border:solid 1px blue;" data-nombre="' + Taquilla['rutas'][i]['categorias'][j]['nombre'] + '" data-id="' + Taquilla['rutas'][i]['categorias'][j]['crypt_id'] + '" data-img="' + Taquilla['rutas'][i]['categorias'][j]['img'] + '" data-precio="' + Taquilla['rutas'][i]['categorias'][j]['precio'] + '" >';
                    content += '<img   class="img-fluid " src="{{url("uploads/category/img_")}}' + Taquilla['rutas'][i]['categorias'][j]['crypt_id'] + '.' + Taquilla['rutas'][i]['categorias'][j]['img'] + '" alt="' + Taquilla['rutas'][i]['categorias'][j]['nombre'] + '">';
                    content += '</div>';
                }
                $("#bodyCategorias").html(' <div style="max-width: 400px; margin:auto;" > ' + content + '</div>');
                break;
            }
        }
        totales();
    }
    function selectCategoria(cantidad, obj, option) {
        // <span class="select-category-badge-num badge bg-danger float-left" style="position:absolute;" title="">1</span>
        //cantActual = parseInt($("#cantidad").val(), 10);
        //if (cantActual < 50) {

        $("#montoVuelto").html("0,00");
        $("#timerSelected").html("00:00 XX");
        $("#horario_ascenso").val("");
        $(".descCat").remove();
        o = "";
        if ($(".badgeRuta").length > 0) {


            cant = 0;
            if ($("#contentCategorias").find('.' + obj.dataset.id).length > 0) {
                cant = parseInt($("#contentCategorias").find('.' + obj.dataset.id).find('span').text(), 10);
                if (option == 0) {
                    cant++;
                } else {
                    cant = cantidad;
                }
                o = obj.dataset.id + ":" + cant;
                $("#contentCategorias").find('.' + obj.dataset.id).find('span').text(cant);
            } else {
                if (option == 0) {
                    cant++;
                } else {
                    cant = cantidad;
                }
                o = obj.dataset.id + ":" + cant;
                newCat = '<div data-nombre="' + obj.dataset.nombre + '" data-id="' + obj.dataset.id + '"  data-precio="' + obj.dataset.precio + '" class="catAgregada widget-user-image ' + obj.dataset.id + ' ">';
                newCat += '<span class="badgeCategoria select-category-badge-num totales badge bg-success float-left " style="position:absolute; " title="' + cant + '">' + cant + '</span>';
                newCat += '<img src="{{url("uploads/category/img_")}}' + obj.dataset.id + '.' + obj.dataset.img + '" alt="Categoria" class="img-fluid elevation-2 ml-2">';
                newCat += '</div>';
                $("#contentCategorias").append(newCat);
                if ($("#contentCategorias").height() > 65) {
                    o = "";
                    $(".catAgregada:last").remove();
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Supero lo Máximo Permitido')}}"
                    });
                    return false;
                }
                //num = 1;



            }

            if ($(obj).find(".badgeCategoria").length > 0) {
                //num = parseInt($(obj).find(".badgeCategoria").text(), 10);
                //num++;
                $(obj).find(".badgeCategoria").text(cant);
            } else {
                $(obj).prepend('<span class="badgeCategoria select-category-badge-num totales badge bg-success float-left" style="position:absolute;" title="">' + cant + '</span>');
            }
            if (o != "") {
                if ($("#categoria").children("[data-val=" + obj.dataset.id + "]").length > 0) {
                    $("#categoria").children("[data-val=" + obj.dataset.id + "]").remove();
                }
                $("#categoria").append('<option selected="selected" data-val="' + obj.dataset.id + '" value="' + o + '"></option>');
            }
            totales();
        } else {
            Toast.fire({
                icon: "error",
                title: "{{__('You must select a route')}}"
            });
        }



    }

    function totales() {
        Bolivares = 0;
        Dolares = 0;
        Cant = 0;
        if ($(".catAgregada").length > 0) {
            $("#detalleFact").html('');
        } else {
            $("#detalleFact").html('&nbsp;');
        }

        $("#bodyDataCategorias").html("");
        $(".catAgregada").each(function (i) {

            cant = parseInt($(this).find("span").text(), 10);
            //console.log(this.dataset.nombre);

            precio = parseFloat(this.dataset.precio, 10);
            nombre = this.dataset.nombre;
            id = this.dataset.id;
            monto = precio * cant * TASA_CAMBIO_DOLLAR;
            Cant += cant;
            Bolivares += precio * cant * TASA_CAMBIO_DOLLAR;
            Dolares += precio * cant;
            $("#detalleFact").append('<li>' + cant + ' X Bs ' + muestraFloat(precio * TASA_CAMBIO_DOLLAR) + ' </li><li > ' + $("#ruta :selected").text() + ' (' + nombre + ') <span style="float: right; margin-right: 25px;">Bs ' + muestraFloat(monto) + '</span></li> ');
            addDataTur(nombre, cant, id);
        });
        $("#cantPers").html(Cant);
        $("#cantidad_personas").val(Cant);
        $("#montoTot").html(muestraFloat(Bolivares));
        $("#montoDiv").html(muestraFloat(Dolares));
        $("#totalExcento, #totalFactura").html("<strong>Bs " + muestraFloat(Bolivares) + '</strong>');
    }

    function checkReturn(obj, ev) {
        if (parseInt(ev.which, 10) == 13) {
            if (obj.value != "") {
                var nextI = $("input:text").index(obj) + 1;
                next = $("input:text").eq(nextI);
                next.focus();
            }

        }
    }

    function addDataTur(nombre, cant, id) {
        $("#bodyDataCategorias").append('<h3>' + nombre + '</h3>');
        data = "";
        for (i = 0; i < cant; i++) {
            data += '<div class="col-md-12 mb-3">';
            data += '<div class="input-group input-group-sm ">';
            data += '<div class="input-group-prepend">';
            data += '<select  name="data[turista][' + id + '][' + i + '][tipo]" class="data_tur">';
            data += '<option selected="selected" value="1">CEDULA</option>';
            data += '<option value="2">PASAPORTE</option>';
            data += '<option value="3">{{__("NOMBRE")}}</option>';
            data += '</select>';
            data += '</div>';
            data += '<input type="text" name="data[turista][' + id + '][' + i + '][nombre]" onKeyPress="checkReturn(this, event)" data-msg-required="{{__("Required Field")}}" class="form-control clsData required data_tur" required="required" />';
            data += '</div>';
            data += '</div>';
        }
        $("#bodyDataCategorias").append(data);
    }

    function showFormDataTur() {
        $("#dataTur").empty();
        $("#listCategorias").children().each(function () {
            if ($(this).find('span.badge').length > 0) {
                //$("#dataTur").append("<h5>"+$(this).attr('data-nombre')+"</h5>");
                cant = parseInt($(this).find('span.badge').text(), 10);
                data = '<h5>' + $(this).attr('data-nombre') + '</h5><div class="row">';
                for (i = 1; i <= cant; i++) {
                    data += '<div class="col-md-12 mb-3">';
                    data += '<div class="input-group input-group-sm ">';
                    data += '<div class="input-group-prepend">';
                    data += '<select onChange="checkDoc(this)" name="Data[' + this.dataset.id + '][' + i + '][nac]" class="data_tur">';
                    data += '<option selected="selected" value="CEDULA">CEDULA</option>';
                    data += '<option value="EXTRANJERO">EXTRANJERO</option>';
                    data += '<option value="PASAPORTE">PASAPORTE</option>';
                    data += '<option value="NOMBRE">{{__("NOMBRE")}}</option>';
                    data += '</select>';
                    data += '</div>';
                    data += '<input type="text" name="Data[' + this.dataset.id + '][' + i + '][nombre]" onKeyPress="checkReturn(this, event)" data-msg-required="{{__("Required Field")}}" class="form-control clsData required data_tur" required="required" />';
                    data += '</div>';
                    data += '</div>';
                }

                data += '</div>';
                $("#dataTur").append(data);
            }
        });
        /*
         $("#listCategorias").children().each(function () {
         if ($(this).find('span.badge').length > 0) {
         $("#dataTur").append("<h5>"+$(this)attr('data-nombre')+"</h5>");
         
         
         //console.log($(this).find('span.badge').text());
         }
         
         
         });
         */

    }
    function searchTime() {
        if (Cant > 0) {
            $("#selectTimer").show();
            $(".listadoTimers").remove();
            $("#modal-content-horarios").prepend(LOADING);
            $.get("{{url('get-horarios')}}/" + $("#ruta").val() + "/" + Cant, function (response) {
                $(".overlay-wrapper").remove();
                $("#bodyHorarios").html(response);
                //console.log(response);
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#bodyHorarios").html(response);
            });
        } else {
            $("#modal-horarios").modal('hide');
            $("#selectTimer").hide();
            Toast.fire({
                icon: "error",
                title: "{{__('Debe Agregar las Categorías')}}"
            });
        }

    }
    function seleccionarHorario(n) {
        $("#t" + n).click();
    }



    function getMontoAbonado() {
        m = 0;
        $(".money").each(function () {
            if (this.dataset.formula == "") {
                m += usaFloat(this.value);
            } else {
                mul = this.dataset.formula.split("*");
                f = "";
                for (i in mul) {
                    if (mul[i] == "VALOR") {
                        f += usaFloat(this.value) + "*";
                    } else {
                        f += mul[i] + "*";
                    }

                }
                f = f.substr(0, f.length - 1);
                m += eval(f);
            }
        });
        $("#montoVuelto").html(muestraFloat(m - Bolivares));
        return m;
    }


    function getReintegroDinero() {
        m = 0;
        $(".money_v").each(function () {
            if (this.dataset.formula == "") {
                m += usaFloat(this.value);
            } else {
                mul = this.dataset.formula.split("*");
                f = "";
                for (i in mul) {
                    if (mul[i] == "VALOR") {
                        f += usaFloat(this.value) + "*";
                    } else {
                        f += mul[i] + "*";
                    }

                }
                f = f.substr(0, f.length - 1);
                m += eval(f);
            }
        });
        //$("#montoVuelto").html(muestraFloat(m - Bolivares));

        return m;
    }

    function limpiar() {
        selectRuta(document.getElementById("ruta_{{$Taq['rutas'][0]['crypt_id']}}"));
        $("#categoria").html("");
        $("input[type=text]:visible").val("");
    }

    function printFact() {
        $("#modal-content-data-print").prepend(LOADING);
        data = {};
        data['total'] = usaFloat($("#montoTot").text());
        data['nombre'] = $("#razon").val().toUpperCase();
        data['cedula'] = $("#tipo_doc").val() + "-" + $("#documento").val();
        data['direccion'] = $("#direccion").val().toUpperCase();
        data['cajero'] = "{{$Taq['codigo']}} {{Upper(Auth::user()->name_user.' '.Auth::user()->surname_user) }}";
        data['telefono'] = $("#telefono").val();
        data['items'] = {};
        $(".catAgregada").each(function (i) {
            data['items'][i] = {};
            cant = parseInt($(this).find("span").text(), 10);
            //console.log(this.dataset.nombre);

            precio = parseFloat(this.dataset.precio, 10);
            nombre = this.dataset.nombre;
            nombre = $("#ruta :selected").text() + ' (' + nombre + ')';
            monto = precio * TASA_CAMBIO_DOLLAR;
            data['items'][i]['precio'] = monto;
            data['items'][i]['cantidad'] = cant;
            data['items'][i]['descripcion'] = nombre;
        });
        d = "func=setFacturaFiscal&jdata=" + JSON.stringify(data);
        $.post("http://localhost/controlps/ajax.php", d, function (response) {
            $(".overlay-wrapper").remove();
            Toast.fire({
                icon: response.type,
                title: response.message
            });
            if (response.status == 1) {
                $("#impFact").prop("disabled", true);
                facturaPrinted = true;
                if (boletoPrinted == true) {
                    $("#finalize").prop("disabled", false);
                }

            }
        }).fail(function () {
            $(".overlay-wrapper").remove();
        });
    }
    function printBoleto() {
        document.getElementById('boletoPrint').contentWindow.imprimir_boleto();
        $("#impBol").prop("disabled", true);
        boletoPrinted = true;
        if (facturaPrinted == true) {
            $("#finalize").prop("disabled", false);
        }






    }
</script>