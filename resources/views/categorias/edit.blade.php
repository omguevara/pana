
<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Categoría')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('categorias')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["categorias.edit", $Clasificacion->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="form-group">
                <label for="nombre">{{__('Category')}}</label>
                {{Form::text("nombre", $Clasificacion->nombre, ["required"=>"required", "class"=>"form-control required", "id"=>"nombre", "placeholder"=>__('Nombre')])}}

            </div>

            <div class="form-group">
                <label for="porcentaje">{{__('Porcentaje')}}</label>
                {{Form::text("porcentaje", muestraFloat($Clasificacion->porcentaje*100), ["maxlength"=>"6", "required"=>"required", "class"=>"form-control required money text-right", "id"=>"porcentaje", "placeholder"=>__('Porcentaje')])}}

            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {
        $(".money").maskMoney({"decimal": ",", "thousands": ".", "allowZero": true});
        $('#frmPrinc1').on("submit", function (event){
            event.preventDefault();
            if ($('#frmPrinc1').valid()){
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response){
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status==1){
                        $.get("{{route('categorias')}}", function (response){
                            $("#main-content").html(response);

                        }).fail(function (xhr){
                             $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    }else{
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function (){
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
