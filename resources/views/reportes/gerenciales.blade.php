
<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Reportes Gerenciales del Sistema')}}</h3>

            <div class="card-tools">



            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-gen" class="card-body ">

            <ul id="listReport" class="nav flex-column">
                <li class="nav-item">
                    <a href="{{route('gerenciales.cierres')}}" onClick="setAjax(this, event)"   class="nav-link">
                        {{__("Cierres")}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('gerenciales.estadisticos')}}" onClick="setAjax(this, event)"   class="nav-link">
                        {{__("Estadísticos")}}
                    </a>
                </li>
            </ul>
            <div style="display:none" id="divIframe">

            </div>

        </div>
    </div>
</div>




<script type="text/javascript">

</script>