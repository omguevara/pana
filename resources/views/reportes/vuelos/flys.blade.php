<div id="panel_princ" class="col-sm-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Vuelos')}}</h3>
            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div id="card-body-main" class="card-body ">
            {{Form::open(['id'=>'form_filter_flys'])}}
                <div class="row">
                    <div class="col-12 col-sm-2">
                        <div class="form-group">
                            <label>Tipo de Aviación</label>
                            {{Form::select('tipo_aviacion', [""=>"Todos", "1"=>"General", "2"=>"Oficial", "3"=>"Militar"], "" , ["data-msg-required"=>"Campo Requerido",  "onChange"=>'$(".filaServExt").remove();',  "class"=>"form-control required  select2 ", "id"=>"tipo_aviacion" ,"required"=>"required"])}}
                        </div>
                    </div>
                    <div class="col-12 col-sm-2">
                        <div class="form-group">
                            <label>Tipo de Operación</label>
                            {{Form::select('tipo_operacion', [""=>"Todos", "1"=>"llegada", "2"=>"salida"], "" , ["data-msg-required"=>"Campo Requerido",  "onChange"=>'$(".filaServExt").remove();',  "class"=>"form-control required  select2 ", "id"=>"tipo_operacion" ,"required"=>"required"])}}
                        </div>
                    </div>
                    <div class="col-12 col-sm-2">
                        <div class="form-group">
                            <label>Tipo de Vuelo</label>
                            {{Form::select('tipo_vuelo', [""=>"Todos", "1"=>"Nacional", "2"=>"Internacional"], "" , ["data-msg-required"=>"Campo Requerido",  "onChange"=>'$(".filaServExt").remove();',  "class"=>"form-control required  select2 ", "id"=>"tipo_vuelo" ,"required"=>"required"])}}
                        </div>
                    </div>
                    <div class="col-12 col-sm-2">
                        <div class="form-group">
                            <label>Fecha</label>
                            {!! Form::text('duracion', $date, ['class' => 'form-control required', 'id' => 'duracion']) !!}
                        </div>
                    </div>
                    <div class="col-12 col-sm-2">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            
                            {!! Form::button('<i class="fa fa-search"></i> Buscar', ['class' => 'form-control btn btn-primary', 'id' => 'filter_flys']) !!}
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
            <div class="row">
                <h3 class="card-title" id="total_vuelos"></h3>
                <canvas id="vuelos1" style="min-width: 100%; height: 250px;" ></canvas>
            </div>
            <div class="row">
                <h3 class="card-title mt-2" id="total_pax"></h3>
                <canvas id="pax_chart"   style="min-width: 100%; height: 250px;" ></canvas>
            </div>
        </div>
    </div>


<script type="text/javascript">
    
    $(function () {
        $("#total_vuelos").text("Total de vuelos: "+ @json($total_vuelos));
        G_1 = new Chart("vuelos1", {
            type: "bar",
            data: {
                labels: @json($aeropuertos),
                datasets: [{
                    label: "Cant. de Vuelos",
                    data:@json($cant_vuelos),
                    backgroundColor: @json($cant_vuelos_color)
                }]
            },

            options: {
                responsive: false,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
            }
        });
        $("#total_pax").text("Total de pasajeros: "+ @json($total_pax));
        PAX_CHART = new Chart("pax_chart", {
            type: "bar",
            data: {
                labels: @json($aeropuertos),
                datasets: [{
                    label: "Cant. de Pasajeros",
                    data:@json($cant_pax),
                    backgroundColor: @json($cant_pax_color)
                }]
            },
            options: {
                responsive: false,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
            }
        });
        
        $('#filter_flys').on("click", function(){
            var form_data = $('#form_filter_flys').serialize();
            $("#panel_princ").prepend(LOADING);
            $.post({
                url: "{{route('flys')}}",
                data: form_data,
                dataType: 'json',
                success: function (response) {
                    var data_1 = G_1.config.data;
                    data_1.datasets[0].data = response.data.cant_vuelos;
                    $("#total_vuelos").text("Total de vuelos: "+ response.data.total_vuelos);
                    G_1.update();
                    
                    var data_2 = PAX_CHART.config.data;
                    $("#total_pax").text("Total de pasajeros: "+ response.data.total_pax);
                    data_2.datasets[0].data = response.data.cant_pax;
                    PAX_CHART.update();
                    $(".overlay-wrapper").remove();
                },
                error: function (xhr, status, error) {
                    
                    $(".overlay-wrapper").remove();
                }
            });
        });



        $('input[name="duracion"]').daterangepicker({
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },

            "opens": "center"
        });

        // SET_INTERVAL = setInterval(function () {
        //     $.post("{{route('flys')}}", "_token={{csrf_token()}}&_method=PUT" ,function (response) {
                
        //         var data_1 = G_1.config.data;
        //         data_1.datasets[0].data = response.data.vuelos;
        //         G_1.update();

        //         var data_2 = PAX_CHART.config.data;
        //         data_2.datasets[0].data = response.data.cant_pax;
        //         PAX_CHART.update();               

        //     });
        // }, 5000);


        $('input[name="duracion"]').daterangepicker({
            "locale": {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Guardar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizar",
                "daysOfWeek": [
                    "Do",
                    "Lu",
                    "Ma",
                    "Mi",
                    "Ju",
                    "Vi",
                    "Sa"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Setiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "firstDay": 1
            },

            "opens": "center"
        });
    });

</script>
</div>