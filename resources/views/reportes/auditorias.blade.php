<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Reportes de Auditorías')}}</h3>

            <div class="card-tools">

                

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
             <h2>{{__("Listado de Reportes")}}</h2>
            <ul class="nav flex-column">
                <li class="nav-item">
                    <a href="{{route('auditorias.cierres')}}" onClick="setAjax(this, event)"   class="nav-link">


                        {{__("Cierres")}}
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{route('auditorias.parametros')}}" onClick="setAjax(this, event)"   class="nav-link">


                        {{__("Parametros")}}
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{route('auditorias.perifericos')}}" onClick="setAjax(this, event)"   class="nav-link">
                        

                        {{__("Periféricos")}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('auditorias.torniquetes')}}" onClick="setAjax(this, event)"   class="nav-link">
                        

                        {{__("Torniquetes")}}
                    </a>
                </li>
                
                
                
            </ul>
        </div>
    </div>
</div>




<script type="text/javascript">
    
</script>