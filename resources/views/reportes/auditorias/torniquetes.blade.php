@extends('layouts.reportes')
@section('content')

<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            
            <th  style="border:solid 1px"   >{{__('Torniquete')}}</th>
            <th  style="border:solid 1px"   >{{__('Fecha Acción')}}</th>
            <th  style="border:solid 1px"   >{{__('Fecha Boleto')}}</th>
            <th  style="border:solid 1px"   >{{__('Code')}}</th>
            <th  style="border:solid 1px; width:80px"   >{{__('Cant. Personas')}}</th>
            

        </tr>

    </thead>

    <tbody>
        
        @foreach($data as $value)
        <tr>
            
            <td style="border:solid 1px" >{{$value->getTorniquete->display_field_min}}</td>
            <td style="border:solid 1px" >{{showDate($value->fecha, "full")}}</td>
            <td style="border:solid 1px" >{{showDate($value->getBoleto->fecha_compra, "full")}}</td>
            <td style="border:solid 1px" >{{$value->getBoleto->getCodigo()}}</td>
            
            <td style="border:solid 1px; text-align: right" >{{muestraFloat($value->getBoleto->cantidad, 0)}}</td>
        </tr>
        @endforeach
        
    </tbody>

</table>


@endsection