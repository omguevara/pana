@extends('layouts.reportes')
@section('content')

<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            
            <th  style="border:solid 1px"   >{{__('Periférico')}}</th>
            <th  style="border:solid 1px"   >{{__('Usuario')}}</th>
            <th  style="border:solid 1px"   >{{__('Estatus')}}</th>
            <th  style="border:solid 1px"   >{{__('Observación')}}</th>
            
            

        </tr>

    </thead>

    <tbody>
        @foreach($data as $value)
        <tr>
            
            <td style="border:solid 1px" >{{Upper($value->getPeriferico->nombre)}}</td>
            <td style="border:solid 1px" >{{$value->getUser->full_nombre}}</td>
            <td style="border:solid 1px" >{{($value->estatus==true ? "ACTIVO":"CON FALLA")}}</td>
            <td style="border:solid 1px" >{{$value->observacion}}</td>
            
            
        </tr>
        @endforeach
    </tbody>

</table>


@endsection