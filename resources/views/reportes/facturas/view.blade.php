




<div id="panel_princ_fact" class="col-sm-12 col-md-12  mt-3">

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturas')}}</h3>

            <div class="card-tools">
                <button type="button"  onClick="goBack()" class="btn btn-tool" ><i class="fas fa-undo"></i>Regresar</button>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">

            <div class="row">
                <iframe id="inlineFrameExample"
                        title="Inline Frame Example"
                        width="100%"
                        height="600"
                        @if (isset($urlFactura))
                        src="{{$urlFactura}}">
                        @else
                        src="{{route('facturas.view_factura', $Factura->crypt_id)}}">
                        @endif
                        
                </iframe>




            </div>


        </div>
    </div>
</div>





<script type="text/javascript">
    function goBack() {
        $("#main-content").prepend(LOADING);
        $.get("{{route($back)}}", function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });
    }
</script>