<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FACTURA {{showCode($Factura->nro_factura)}}</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 100%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
    bottom: 0;
    margin-bottom: 110px;
    position: absolute;
    width:100%
    
    
    
}
    </style>
    <body>
        <button onClick="setPint()" class="block">IMPRIMIR</button>

        <table style="margin-top:150px" width="100%" cellspacing="0" >
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TIPO DE DOCUMENTO</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">FACTURA </td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="2" >{{$Factura->getDetalleVuelo()->aeropuerto->codigo_oaci.'-'.showCode($Factura->nro_factura)}}</td>

            </tr>
            <tr>

                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >FECHA DE EMISI&Oacute;N</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{showDate($Factura->fecha_factura)}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONDICI&Oacute;N DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$Factura->getCliente1Attribute->getRifAttribute()}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$Factura->getCliente1Attribute->telefono}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{$Factura->getCliente1Attribute->razon_social}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{Upper($Factura->getCliente1Attribute->direccion)}}</td>

            </tr>










            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >
                    <p style="text-align: justify">

                        <b>Matrícula:</b>  {{$Factura->getDetalleVuelo()->aeronave->matricula}}, 
                        <b>Aeronave:</b> {{$Factura->getDetalleVuelo()->aeronave->nombre}}, 


                        <b>Aeronave TN:</b> {{muestraFloat($Factura->getDetalleVuelo()->aeronave->peso_maximo/1000)}},
                        <b>Fecha de Vuelo:</b> {{showDate($Factura->getDetalleVuelo()->fecha_llegada)}}, 
                        <b>Aeropuerto:</b> {{$Factura->getDetalleVuelo()->aeropuerto->nombre}}, 








                        <b>Tasa {{$TIPOS_VUELOS_NOMEDAS[$Factura->moneda_aplicada]}}:</b> {{muestraFloat($Factura->monto_moneda_aplicada)}},

                        @foreach($Factura->getPagos as $value)
                        @if ($value->monto > 0)
                        <b>{{$value->getFormaPago->nombre}}:</b>  {{muestraFloat($value->monto)}},  

                        @endif
                        @endforeach


                    </p>
                </td>

            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="6" ><strong>DETALLES DE LA FACTURA</strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >

            <tr>
                <td clsss="a" style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%" ><strong>CANT.</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>CÓDIGO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:60%"><strong>CONCEPTO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>NOMENCLATURA</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> BOLIVARES</strong></td>
                {{-- <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> {{$TIPOS_VUELOS_NOMEDAS[$Factura->moneda_aplicada]}}</strong></td>--}}
            </tr>
            @php
            $sum = 0;
            $base_imponible = 0;
            $excento = 0;
            $iva = 0;
            //dd($Factura->getDetalle);
            @endphp


            @foreach($Factura->getDetalle as $value)
            @php

            if ($value['codigo'] == '2.5.1.1') {//SI ES TASA

            $sum += $value['precio'];
            $mult = $value['precio'];
            $cant =  $Factura->getDetalleVuelo()->pax_desembarcados ;
            } else {

            $sum += $value['cantidad'] * $value['precio'];
            $mult = $value['cantidad'] * $value['precio'];
            $cant = $value['cantidad'] ;


            }
            if ($value['iva'] > 0) {
            $base_imponible += $mult;
            $iva += $mult * $value['iva'] / 100;
            } else {
            $excento += $mult;
            }
            @endphp
            <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >{{($cant)}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{$value['codigo']}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:60%"  >{{$value['descripcion']. ' '.$value['iva2']}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{Upper(($value['cantidad'])."  ".$value['nomenclatura'])}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($mult)}}</td>
                    {{-- <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;" align="right" >{{muestraFloat($value['precio2'])}}</td> --}}
                </tr>
                @endforeach
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">BASE IMPONIBLE</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($base_imponible)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($base_imponible2)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">EXENTO</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($excento)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($excento2)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">SUBTOTAL</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sub_total2)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">IVA (16%)</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($iva)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($iva2)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">TOTAL</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum+$iva)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sub_total2+$iva2)}}</td>--}}
                </tr>


        </table>
        <b>REF EURO: {{muestraFloat(($sum+$iva)/$Factura->monto_moneda_aplicada)}}</b> 
        <div id="text_copia" style="display:none">
@php
    if ($copia==1){
        for($i = 0; $i <10; $i++){
	for($j = 0; $j <10; $j++){
		echo '<div style="top:'.(100*$i).'px ; left:'.(100*$j).'px" class="rotar">COPIA</div>';
	}	
    }
}
@endphp
</div>

        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                setPint()
                //window.close();

            });

            function setPint() {
                window.print();
            }
        </script>

<div class="foot_pie">
    @if ($copia==1)
    <h1 style="text-align: center; font-weight: bold; font-size: 25px">COPIA</h1>
    @endif
</div>

    </body>
</html>
