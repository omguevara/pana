<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FACTURA {{showCode($Factura->nro_factura)}}</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 100%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
    bottom: 0;
    margin-bottom: 110px;
    position: absolute;
    width:100%
    
    
    
}
    </style>
    <body>
        <button onClick="setPint()" class="block">IMPRIMIR</button>

        <table style="margin-top:200px" width="100%" cellspacing="0" >
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TIPO DE DOCUMENTO</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">FACTURA </td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="2" >{{showCode($Factura->nro_factura)}}</td>

            </tr>
            <tr>

                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >FECHA DE EMISI&Oacute;N</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{showDate($Factura->fecha_factura)}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONDICI&Oacute;N DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$Factura->getCliente1Attribute->getRifAttribute()}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$Factura->getCliente1Attribute->telefono}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{$Factura->getCliente1Attribute->razon_social}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{Upper($Factura->getCliente1Attribute->direccion)}}</td>

            </tr>










            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >
                    <p style="text-align: justify">
                        <table style="width:100%">
                            @foreach($Factura->getDetalleVuelo() as $value)
                            <tr>
                                <td>
                                    <b>Matrícula:</b>  {{$value->aeronave->matricula}}, 
                                    <b>Aeronave:</b> {{$value->aeronave->nombre}}, 
                                    <b>Aeronave TN:</b> {{muestraFloat($value->aeronave->peso_maximo/1000)}},
                                    <b>Fecha de Vuelo:</b> {{showDate($value->fecha_operacion)}}, 
                                    <b>Aeropuerto:</b> {{$value->aeropuerto->nombre}},
                                    <b>Piloto:</b> {{$value->piloto->full_name}}
                                    
                                </td>
                            </tr>
                            @endforeach
                            <tr>
                                <td>
                                     <b>TASA EURO: </b> {{muestraFloat($Factura->tasa_euro)}}
                                     <b>REF EURO: </b> {{muestraFloat($Factura->getTotal()/$Factura->tasa_euro)}}
                                </td>
                            </tr>
                        </table>
                        
                        
                        

                    </p>
                </td>

            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="6" ><strong>DETALLES DE LA FACTURA</strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >

            <tr>
                <td clsss="a" style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%" ><strong>CANT.</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>CÓDIGO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:60%"><strong>CONCEPTO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>NOMENCLATURA</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> BOLIVARES</strong></td>
                
            </tr>
            @php
            $sum = 0;
            $base_imponible = 0;
            $excento = 0;
            $iva = 0;
            
            @endphp


            @foreach($Factura->getDetalle as $value)
            @php
            if ($value['iva']>0){
                $base_imponible += $value['precio'];
            }else{
                $excento = $value['precio'];
            }
            $sum += $value['precio'];
            
            
            $iva += $value['precio']*( ($value['iva']/100));
            
            @endphp
            <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >{{($value['cantidad'])}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{$value['codigo']}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:60%"  >{{$value['descripcion']. ' '.$value['iva2']}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{Upper(($value['cantidad'])."  ".$value['nomenclatura'])}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($value['precio'])}}</td>
                    
                </tr>
                @endforeach
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">BASE IMPONIBLE</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($base_imponible)}}</td>
                    
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">EXENTO</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($excento)}}</td>
                    
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">SUBTOTAL</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum)}}</td>
                   
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">IVA (16%)</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($iva)}}</td>
                    
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">TOTAL</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum+$iva)}}</td>
                   
                </tr>


        </table>
        
        <div id="text_copia" style="display:none">

</div>

        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                setPint()
                //window.close();

            });

            function setPint() {
                window.print();
            }
        </script>

<div class="foot_pie">
   
</div>

    </body>
</html>
