<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FACTURA</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }

      
    </style>
    <body>



        <table style="margin-top:150px" width="100%" cellspacing="0" >
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TIPO DE DOCUMENTO</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">Factura {{$TIPOS_VUELOS[$Factura->getSolicitud->tipo_vuelo_id]}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%"></td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%"></td>

            </tr>
            <tr>

                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >FECHA DE EMISION</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{showDate($Factura->fecha_factura)}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONDICION DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$Factura->getClienteAttribute->getRifAttribute()}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$Factura->getClienteAttribute->telefono}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{Upper($Factura->getClienteAttribute->razon_social)}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{Upper($Factura->getClienteAttribute->direccion)}}</td>

            </tr>










            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >
                    <p style="text-align: justify">
                        @foreach($Reservaciones as $value)
                        <b>Fecha de Vuelo:</b>  {{showDate($value->fecha)}},  
                        <b>Matrícula del Avión:</b>  {{$value->aeronave->matricula}}, 
                        <b>Aeronave:</b> {{$value->aeronave->nombre}}, 
                        <b>Tipo de Vuelo:</b> {{$TIPOS_VUELOS[$value->solicitud->tipo_vuelo_id]}}, 
                        <b>Aeropuerto de Salida:</b> {{$value->origen->nombre}}, 
                        <b>Aeropuerto de Llegada:</b> {{$value->destino->nombre}},
                        @endforeach
                        <b>Tasa {{$Factura->moneda_aplicada}}:</b> {{muestraFloat($Factura->monto_moneda_aplicada)}},
                        
                        @foreach($Factura->getPagos as $value)
                        @if ($value->monto > 0)
                        <b>{{$value->getFormaPago->nombre}}:</b>  {{muestraFloat($value->monto)}},  
                        
                        @endif
                        @endforeach
                        
                        
                    </p>
                </td>

            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="6" ><strong>DETALLES DE LA FACTURA</strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >

            <tr>
                <td clsss="a" style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%" ><strong>CANT.</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>CÓDIGO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:45%"><strong>CONCEPTO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>NOMENCLATURA</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> BOLIVARES</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> {{$Factura->moneda_aplicada}}</strong></td>
            </tr>
            @foreach($Factura->getDetalle as $value)
            <tr>
                <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >1</td>
                <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{$value['codigo']}}</td>
                <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:45%"  >{{$value['descripcion'].' ('.($value['iva']>0 ? 'G':'E').')'}}</td>
                <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" ></td>
                <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($value['precio'])}}</td>
                <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;" align="right" >{{muestraFloat($value['precio2'])}}</td>
            </tr>
            @endforeach










            <script type="text/javascript">
                 document.addEventListener("DOMContentLoaded", function (){
                     window.print();
                     //window.close();
                             
                 });
                </script>



    </body>
</html>
