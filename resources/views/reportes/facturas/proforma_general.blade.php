<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PROFORMA N-{{showCode($VuelosGenerales->id)}}</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 100%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
        }

    </style>
    <body>
        <button onClick="setPint()" class="block">IMPRIMIR</button>

        <table style="margin-top:150px" width="100%" cellspacing="0" >
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TIPO DE DOCUMENTO</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">PROFORMA </td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px"  >PROFORMA</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">N-{{showCode($VuelosGenerales->id)}}</td>

            </tr>
            <tr>

                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >FECHA DE EMISION</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{showDate(date("Y-m-d"))}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONDICION DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{request('prof_type_document').request('prof_document')}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{request('prof_phone')}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{Upper(request('prof_razon'))}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{Upper(request('prof_direccion'))}}</td>

            </tr>



   






            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >
                    <p style="text-align: justify">

                        <b>Matrícula:</b>  {{$VuelosGenerales->aeronave->matricula}}, 
                        <b>Aeronave:</b> {{$VuelosGenerales->aeronave->nombre}}, 
                        <b>Aeronave TN:</b> {{muestraFloat($VuelosGenerales->aeronave->peso_maximo/1000)}},
                        <b>Fecha de Vuelo:</b> {{showDate($VuelosGenerales->fecha_llegada)}}, 
                        <b>Aeropuerto:</b> {{$VuelosGenerales->aeropuerto->nombre}}, 


                    

                        <b>Tasa {{$TIPOS_VUELOS_NOMEDAS[2]}}:</b> {{muestraFloat($VALOR_EURO)}},

                        


                    </p>
                </td>

            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="6" ><strong>DETALLES DE LA FACTURA</strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >

            <tr>
                <td clsss="a" style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%" ><strong>CANT.</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>CÓDIGO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:60%"><strong>CONCEPTO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%"><strong>NOMENCLATURA</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> EUROS</strong></td>
            {{-- <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%"><strong>MONTO <br/> {{$TIPOS_VUELOS_NOMEDAS[$Factura->moneda_aplicada]}}</strong></td>--}}
            </tr>
            
            @php
            $sum = 0;
            $base_imponible = 0;
            $excento = 0;
            $iva = 0;
            @endphp
            
            @foreach($prodserv_dosa['data'] as $value)
            
            @php
           
            if ($value['codigo'] == '2.5.1.1') {//SI ES TASA
             
                $sum += $value['precio'];
                $mult = $value['precio'];
                $cant =  $VuelosGenerales->pax_desembarcados ;
            } else {

                $sum += $value['cant'] * $value['precio'];
                $mult = $value['cant'] * $value['precio'];
                $cant = $value['cant'] ;


            }
            if ($value['iva_aplicado'] > 0) {
                $base_imponible += $mult;
                $iva += $mult * $value['iva_aplicado'] / 100;
            } else {
                $excento += $mult;
            }
            @endphp
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >{{($cant)}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{$value['codigo']}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:60%"  >{{$value['descripcion']. ' '.$value['iva2']}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{Upper(($cant)."  ".$value['nomenclatura'])}}</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($mult)}}</td>
                    {{-- <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;" align="right" >{{muestraFloat($value['bs'])}}</td> --}}
                </tr>
                @endforeach
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">BASE IMPONIBLE</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($base_imponible)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($base_imponible)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">EXENTO</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($excento)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($excento)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">SUBTOTAL</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">IVA (16%)</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($iva)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($iva)}}</td>--}}
                </tr>
                <tr>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:70%; text-align:right" colspan="4">TOTAL</td>
                    <td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum+$iva)}}</td>
                    {{--<td style="border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 10px; width:15%;"  align="right" >{{muestraFloat($sum+$iva)}}</td>--}}
                </tr>
                @if (isset($prodserv_dosa['observacion']))
                <tr>
                    <td colspan="5">
                        
                        <b>Observaciones:</b>
                        {{$prodserv_dosa['observacion']}}
                    </td>
                </tr>
                @endif

        </table>
         





        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                setPint()
                //window.close();

            });

            function setPint() {
                window.print();
            }
        </script>



    </body>
</html>
