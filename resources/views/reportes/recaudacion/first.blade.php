<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>RESUMEN DE PROFORMAS</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 40%;
            float:left;
            margin-left: 5%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
            #footer_prof{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
            bottom: 0;
            margin-bottom: 110px;
            position: absolute;
            width:100%



        }
    </style>
    <body>
        <table  width="100%" cellspacing="0" >
            <tr>

                <td colspan="4" ><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>

            </tr>
            <tr>
                <td colspan="4" >&nbsp;</td>
            </tr>
           
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="2" ></td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$cliente['rif']}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$cliente['telefono']}}</td>

            </tr>
            
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{$cliente['razon_social']}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >CORREO:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{$cliente['correo']}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{Upper($cliente['direccion'])}}</td>

            </tr>

        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="6" ><strong>DETALLES DEL PAGO</strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >
            <thead>
                <tr>
                    <td clsss="a" style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:5%" ><strong>FECHA</strong></td>
                    <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%"><strong>MATRÍCULA</strong></td>
                    <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:60%"><strong>AEROPUERTO</strong></td>
                    <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%"><strong>MONTO</strong></td>
                    <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%"><strong>MONTO ABONADO</strong></td>
    
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0;
                    $total_abonado = 0;
                    
                    $base_imponible = 0;
                    $excento = 0;
                    $sum = 0;
                    $iva = 0;
                @endphp
                @foreach($proforma as $value)
                    @php
                        $total += $value->total;
                        $total_abonado += $value->total_monto_abonado;


                        if ($value->iva>0){
                            $base_imponible += $value->total;
                        }else{
                            $excento += $value->total;
                        }
                        $sum += $value->total;

                        $iva += $value->total*( ($value->iva/100));
                    @endphp
                    <tr>
                        <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:5%; text-align:center">{{ $value->fecha_proforma2 }}</td>
                        <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%; text-align:center" >{{ $value->aeronave->matricula }}</td>
                        <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:60%">{{ $value->aeropuerto->full_nombre }}</td>
                        <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%; text-align:center">€ {{ muestraFloat($value->total) }}</td>
                        <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right">€ {{ muestraFloat($value->total_monto_abonado) }}</td>
                    </tr>
                @endforeach
                    <tfoot>

                        <tr>
                            <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">TOTAL &euro;</td>
                            <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($total)}}</td>
            
                        </tr>
                        <tr>
                            <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">TOTAL ABONADO &euro;</td>
                            <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($total_abonado)}}</td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">RESTANTE POR PAGAR &euro;</td>
                            <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{ muestraFloat($total - $total_abonado)  }}</td>
                        </tr>
                    </tfoot>
            </tbody>
        </table>
    </body>
</html>