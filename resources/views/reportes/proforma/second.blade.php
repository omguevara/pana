<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>FACTURA {{showCode($Proforma->nro_factura)}}</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 40%;
            float:left;
            margin-left: 10%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
            bottom: 0;
            margin-bottom: 110px;
            position: absolute;
            width:100%



        }
    </style>
    <body>
        @if (!isset($hideBotonPrint))
        <button onClick="setPint()" class="block">IMPRIMIR</button>
        <button onClick="downPdf()" class="block">PDF</button>
        @endif
        <table style="margin-top:150px" width="100%" cellspacing="0" >
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >TIPO DE DOCUMENTO</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">FACTURA </td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="2" >{{showCode($Proforma->nro_factura)}}</td>

            </tr>
            <tr>

                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >FECHA DE EMISI&Oacute;N</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{date('d/m/Y')}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">CONDICI&Oacute;N DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$Proforma->cliente->rif}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$Proforma->cliente->telefono}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{$Proforma->cliente->razon_social}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >CORREO:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{$Proforma->cliente->correo}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{Upper($Proforma->cliente->direccion)}}</td>

            </tr>



            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >
                    <p style="text-align: justify">

                        <b>Matrícula:</b>  {{$Proforma->aeronave->matricula}}, 
                        <b>Modelo:</b> {{$Proforma->aeronave->nombre}}, 
                        <b>Peso TON:</b> {{muestraFloat($Proforma->aeronave->peso_maximo/1000)}},

                        <b>Aeropuerto:</b> {{$Proforma->aeropuerto->full_nombre}},
                        <b>Tasa del Euro:</b> {{muestraFloat($Proforma->tasa_euro)}},
                        <b>Ref. Euro:</b> {{muestraFloat($Proforma->getTotal())}}





                    </p>
                </td>

            </tr>







        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="6" ><strong></strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >

            <tr>
                <td clsss="a" style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:5%" ><strong>CANT.</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%"><strong>CÓDIGO</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:60%"><strong>CONCEPTO</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%"><strong>NOMENCLATURA</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%"><strong>MONTO BS</strong></td>

            </tr>
            @php
            $sum = 0;
            $base_imponible = 0;
            $excento = 0;
            $iva = 0;

            @endphp



            @foreach($Proforma->getDetalle2() as $value)
            @php
            if ($value['iva']>0){
            $base_imponible += ($value['precio']);
            }else{
            $excento += ($value['precio']);
            }
            $sum += ($value['precio']);


            $iva += ($value['precio'])*( ($value['iva']/100));

            @endphp
            <tr>
                <td style="font-weight: bold; border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:5%; text-align:center" >{{(muestraFloat($value['cantidad']))}}</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%; text-align:center" >{{$value['codigo']}}</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:60%"  >{{$value['descripcion']. ' '.$value['iva2']}}</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%; text-align:center" >{{Upper(muestraFloat($value['cantidad'])."  ".$value['nomenclatura'])}}</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($value['precio']*$Proforma->tasa_euro)}}</td>

            </tr>

            @endforeach
            @php
            $BASE_IMPONIBLE = round($base_imponible*$Proforma->tasa_euro, 2);
            $EXENTO = round($excento*$Proforma->tasa_euro, 2);
            $SUBTOTAL=round($sum*$Proforma->tasa_euro, 2);
            $IVA = round($iva, 2)*$Proforma->tasa_euro;
            $TOTAL = round(($SUBTOTAL+$IVA), 2);
            @endphp
            <tr>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">BASE IMPONIBLE</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($BASE_IMPONIBLE)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">EXENTO</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($EXENTO)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">SUBTOTAL</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($SUBTOTAL)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">IVA (16%)</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($IVA)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">TOTAL</td>
                <td style="font-weight: bold; border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($TOTAL)}}</td>

            </tr>

        </table>
        @if ($Proforma->anulada==true)
        <b style="font-size: 24px">FACTURA ANULADA</b>
        @endif
        <div id="text_copia" style="display:none">

        </div>

        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                //setPint()
                //window.close();

            });

            function setPint() {
                window.print();
            }
            function downPdf() {
                window.open("{{route('down_factura', $Proforma->crypt_id)}}");
            }
        </script>

        <div class="foot_pie">

        </div>

    </body>
</html>
