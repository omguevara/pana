<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PROFORMA {{showCode($Proforma->id)}}</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 40%;
            float:left;
            margin-left: 5%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
            #footer_prof{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
            bottom: 0;
            margin-bottom: 110px;
            position: absolute;
            width:100%



        }
    </style>
    <body>
        @if (!isset($hideBotonPrint))
        <button onClick="setPint()" class="block">IMPRIMIR</button>
        <button onClick="downPdf()" class="block">DESCARGAR PDF</button>
        @endif
        <table width="100%" cellspacing="0" >
            <tr>

                @if (!isset($hideBotonPrint))
                    <td colspan="4" ><img style="width:100%" src="{{url('/dist/img/banner.jpeg')}}"/></td>
                @else
                    <td colspan="4" ><img style="width:100%" src="{{url('dist/img/banner.jpeg')}}"/></td>
                @endif
                
            </tr>
            <tr>
                <td colspan="4" >&nbsp;</td>
            </tr>
           
            <tr>

                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >TIPO DE DOCUMENTO</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">PROFORMA </td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="2" >{{showCode($Proforma->id)}}</td>

            </tr>
            <tr>

                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >FECHA DE EMISI&Oacute;N</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$Proforma->fecha_proforma2}}</td>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">CONDICI&Oacute;N DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$Proforma->cliente->rif}}</td>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%">{{$Proforma->cliente->telefono}}</td>

            </tr>
            
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >RAZ&Oacute;N SOCIAL:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{$Proforma->cliente->razon_social}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >CORREO:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{$Proforma->cliente->correo}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >DIRECCI&Oacute;N:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{Upper($Proforma->cliente->direccion)}}</td>

            </tr>
            <tr>
                <td style=" font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >Observación:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >{{Upper($Proforma->observacion)}}</td>

            </tr>



            <tr>
                <td style="font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="3" >
                    <p style="text-align: justify">

                        <b>Matrícula:</b>  {{$Proforma->aeronave->matricula}}, 
                        <b>Modelo:</b> {{$Proforma->aeronave->nombre}}, 
                        <b>Peso TON:</b> {{muestraFloat($Proforma->aeronave->peso_maximo/1000)}},
                        
                        <b>Aeropuerto:</b> {{$Proforma->aeropuerto->full_nombre}},
                        





                    </p>
                </td>

            </tr>







        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 14px" colspan="6" ><strong>DETALLES DE LA PROFORMA</strong></td>
            </tr>
        </table>
        <table width="100%" style="margin-top: 5px;" cellspacing="0" >

            <tr>
                <td clsss="a" style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:5%" ><strong>CANT.</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%"><strong>CÓDIGO</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:60%"><strong>CONCEPTO</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%"><strong>NOMENCLATURA</strong></td>
                <td style="font-weight: bold;  text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%"><strong>MONTO EURO</strong></td>

            </tr>
            @php
            $sum = 0;
            $base_imponible = 0;
            $excento = 0;
            $iva = 0;

            @endphp

            
            
            @foreach($Proforma->getDetalle2() as $value)
            
            @php
            if ($value['iva']>0){
            $base_imponible += $value['precio'];
            }else{
            $excento += $value['precio'];
            }
            $sum += $value['precio'];


            $iva += $value['precio']*( ($value['iva']/100));

            @endphp
            
            <tr>
                <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:5%; text-align:center" >{{($value['cantidad'])}}</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%; text-align:center" >{{$value['codigo']}}</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:60%"  >{{$value['descripcion']. ' '.$value['iva2']}}</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:10%; text-align:center" >{{Upper($value['cantidad']."  ".$value['nomenclatura'])}}</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:10px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($value['precio'])}}</td>

            </tr>

            @endforeach
            @php
            $iva = round($iva, 2);
            @endphp
            <tr>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">BASE IMPONIBLE &euro;</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($base_imponible)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">EXENTO &euro;</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($excento)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">SUBTOTAL &euro;</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($sum)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">IVA (16%) &euro;</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($iva)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:70%; text-align:right" colspan="4">TOTAL &euro;</td>
                <td style="font-weight: bold;  border:solid 1px; padding-right:12px; font-family: 'Ubuntu', sans-serif; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($sum+$iva)}}</td>

            </tr>

        </table>
        @if ($Proforma->anulada==true)
        <b style="font-size: 24px">PROFORMA ANULADA</b>
        @endif


        @if ($Proforma->temp_alt==true)
        <b style="font-size: 24px">ALTA MOVILIZACION ART 28</b>
        <p>Las tarifas por servicios aeroportuarios que sean prestados a la aviación general en los días considerados de alta movilización, tendra un recargo del (20%).</p>
        @endif

        <div id="text_copia" style="display:none">

        </div>
        <div id="foot" >

        </div>

        <script type="text/javascript">
            var ID_PROFORMA = "{{$Proforma->crypt_id}}";
            var CLI_EMAIL = "{{$Proforma->cliente->correo}}";
            document.addEventListener("DOMContentLoaded", function () {
                //setPint()
                //window.close();

            });

            function setPint() {
                window.print();
            }
            
            function downPdf() {
                window.open("{{route('down_proforma', $Proforma->crypt_id)}}");
            }
            
        </script>
 
        <div id="footer_prof" style=" {{ isset($hideBotonPrint) ? '':'display: none;'}}   clear:both; left:0px; bottom: 40;  position: fixed;  width:95%; text-align: center; font-family: 'Ubuntu', sans-serif; font-size: 10px; border:solid 1px; padding: 5px ">
            <div style="text-align: justify; font-size: 8px">
                NOTA: ARTICULO 4 SISTEMA DE TARIFAS POR EL EMPLEO DE INSTALACIONES Y SERVÍCIOS AEROPORTUARIOS, PRODUCTO DE LA ACTIVIDAD AERONAUTICA Y NO AERONAUTICA
                <br/>
                Los montos resultantes podrán ser pagados alternativamente en cualquier otra divisa convertible aceptada en el sistema financiero venezolano, distinta a la moneda en curso legal en la República Bolivariana de Venezuela o en
                criptomonedas, al tipo de cambio establecido para ello correspondiente al día de la operación del pago, de conformidad con la normativa vigente en materia cambiaria
            </div>
            
            RIF: G20008992-0
            <br>
            Teléfonos:: (0212)-905.37.01 (0212)-905.07.41
            <br>
            
            Av. Venezuela, Urb. El Rosal, Edif. Antigua sede FONTUR, Municipio Chacao, Caracas Distrito Capital, Venezuela

        </div>

    </body>
</html>
