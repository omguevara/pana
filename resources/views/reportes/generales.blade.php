
<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Reportes Generales del Sistema')}}</h3>

            <div class="card-tools">

                {{Form::button('<li   class="fa fa-arrow-left "></li> Regresar ', ["onClick"=>"goBack()", "style"=>"display:none" ,"id"=>"btnGoBack", "class"=>"form-control bg-success "])}}

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-gen" class="card-body ">

            <ul id="listReport" class="nav flex-column">
                <li class="nav-item">
                    <a href="{{route('generales.users')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Users")}}
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{route('generales.taquillas')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Taquillas")}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('generales.estaciones')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Estaciones")}}
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('generales.categorias')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Categorías")}}
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('generales.impresoras')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Impresoras")}}
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('generales.areas_espera')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Areas de Espera")}}
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('generales.torniquetes')}}" onClick="visualize(this, event)"   class="nav-link">


                        {{__("Torniquetes")}}
                    </a>
                </li>
            </ul>
            <div style="display:none" id="divIframe">
                
            </div>

        </div>
    </div>
</div>




<script type="text/javascript">
    function visualize(obj, ev) {
        ev.preventDefault();
        $("#divIframe").append('<iframe onload="closeLoading()" id="browserPdf" name="browserPdf" style="width:100%; height: 600px; border:none" src="'+obj.href+'"></iframe>' );
        $("#card-body-gen").prepend(LOADING);


        $("#listReport").slideUp("fast", function () {
            $("#divIframe").slideDown("fast");
            $("#btnGoBack").show();
        });
    }
    function goBack(){
        $("#divIframe").slideUp("fast", function () {
            $("#browserPdf").remove();
            $("#listReport").slideDown("fast");
            $("#btnGoBack").hide();
        });
    }
    function closeLoading(){
        $(".overlay-wrapper").remove();
    }
</script>