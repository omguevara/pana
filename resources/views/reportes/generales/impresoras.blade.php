@extends('layouts.reportes')
@section('content')

<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            <th  style="border:solid 1px"   >{{__('Modelo')}}</th>
            <th  style="border:solid 1px"   >{{__('Serial')}}</th>
            <th  style="border:solid 1px"   >{{__('Numero Factura')}}</th>
            <th  style="border:solid 1px"   >{{__('Nota Debito')}}</th>
            <th  style="border:solid 1px"   >{{__('Nota Credito')}}</th>
            <th  style="border:solid 1px"   >{{__('Estatus')}}</th>

        </tr>

    </thead>

    <tbody>
        @foreach($data as $value)
        <tr>
            <td style="border:solid 1px" >{{$value->modelo}}</td>
            <td style="border:solid 1px" >{{$value->serials}}</td>
            <td style="border:solid 1px" >{{$value->numero_factura}}</td>
            <td style="border:solid 1px" >{{$value->nota_debito}}</td>
            <td style="border:solid 1px"  >{{$value->nota_credito}}</td>
            <td style="border:solid 1px" >{{($value->activo == true ? 'Activa':'Inactiva')}}</td>
        </tr>
        @endforeach
    </tbody>

</table>


@endsection