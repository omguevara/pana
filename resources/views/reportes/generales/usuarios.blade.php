@extends('layouts.reportes')
@section('content')

<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            <th  style="border:solid 1px"   >{{__('Cedula')}}</th>
            <th  style="border:solid 1px"   >{{__('Nombre')}}</th>
            <th  style="border:solid 1px"   >{{__('Teléfono')}}</th>
            <th  style="border:solid 1px"   >{{__('Correo')}}</th>
            <th  style="border:solid 1px"   >{{__('Perfil')}}</th>
            

        </tr>

    </thead>

    <tbody>
        @foreach($data as $value)
        <tr>
            <td style="border:solid 1px" >{{$value->document}}</td>
            <td style="border:solid 1px" >{{$value->full_nombre}}</td>
            <td style="border:solid 1px" >{{$value->phone}}</td>
            <td style="border:solid 1px" >{{$value->email}}</td>
            <td style="border:solid 1px"  >{{$value->getProfile->name_profile}}</td>
            
        </tr>
        @endforeach
    </tbody>

</table>


@endsection