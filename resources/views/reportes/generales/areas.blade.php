@extends('layouts.reportes')
@section('content')



<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            
            <th  style="border:solid 1px"   >{{__('Nombre')}}</th>
            
            <th  style="border:solid 1px"   >{{__('Estación')}}</th>
            <th  style="border:solid 1px"   >{{__('Capacidad')}}</th>
            <th  style="border:solid 1px"   >{{__('Estatus')}}</th>

        </tr>

    </thead>

    <tbody>
        
        @foreach($data as $value)
        <tr>
            
            <td style="border:solid 1px" >{{$value->nombre}}</td>
            <td style="border:solid 1px" >{{$value->estacion->nombre}}</td>
            <td style="border:solid 1px" >{{$value->capacidad_def}}</td>
            
            <td style="border:solid 1px" >{{($value->activo == true ? 'Activa':'Inactiva')}}</td>
        </tr>
        @endforeach
        
    </tbody>

</table>
@endsection