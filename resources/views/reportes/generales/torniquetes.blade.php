@extends('layouts.reportes')
@section('content')

<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            <th  style="border:solid 1px"   >{{__('Código')}}</th>
            <th  style="border:solid 1px"   >{{__('Ip')}}</th>
            <th  style="border:solid 1px"   >{{__('Area')}}</th>
            <th  style="border:solid 1px"   >{{__('Sentido')}}</th>
            <th  style="border:solid 1px"   >{{__('Estatus')}}</th>

        </tr>

    </thead>

    <tbody>
        @foreach($data as $value)
        <tr>
            <td style="border:solid 1px" >{{$value->codigo}}</td>
            
            <td style="border:solid 1px" >{{$value->ip_pc}}</td>
            <td style="border:solid 1px" >{{$value->area->nombre}}</td>
            <td style="border:solid 1px"  >{{$SENTIDOS[$value->sentido]}}</td>
            <td style="border:solid 1px" >{{($value->activo == true ? 'Activa':'Inactiva')}}</td>
        </tr>
        @endforeach
    </tbody>

</table>


@endsection