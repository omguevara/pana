@extends('layouts.reportes')
@section('content')

<table style="width:100%;  border-collapse: collapse; border:solid 1px; ">
    <thead>
        <tr>
            <th  style="border:solid 1px"   >{{__('Imagen')}}</th>
            <th  style="border:solid 1px"   >{{__('Código')}}</th>
            <th  style="border:solid 1px"   >{{__('Nombre')}}</th>
            <th  style="border:solid 1px"   >{{__('Precio $')}}</th>
            <th  style="border:solid 1px"   >{{__('Estatus')}}</th>

        </tr>

    </thead>

    <tbody>
        @foreach($data as $value)
        <tr>
            <td style="border:solid 1px" ><img id="show1" src="{{url("uploads/category/img_".$value->crypt_id.".".$value->img)}}" class="img-fluid" style="width:50px" alt="*" /></td>
            <td style="border:solid 1px" >{{$value->codigo}}</td>
            <td style="border:solid 1px" >{{Upper($value->nombre)}}</td>
            <td style="border:solid 1px; text-align: right" >{{muestraFloat($value->precio)}}</td>
            
            <td style="border:solid 1px" >{{($value->activo == true ? 'Activa':'Inactiva')}}</td>
        </tr>
        @endforeach
    </tbody>

</table>


@endsection