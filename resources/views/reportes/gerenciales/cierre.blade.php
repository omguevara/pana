@extends('layouts.reportes')
@section('content')

<table style="width:40%; border:solid 1px; border-collapse: collapse">

    <tr>
        <td style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">Fecha</td>
        <td style="border:solid 1px; font-size:10px">{{$text_rango}}</td>
    </tr>

    <tr>
        <td style=" border:solid 1px; background-color: grey;  font-size: 12px; font-weight: bold " >Tipo</td>
        <td style="border:solid 1px; font-size:10px" >{{$TIPOS_CIERRE_NOMBRE[$tipo]}}</td>
    </tr>
    
</table>

<table style="width:100%; border-collapse: collapse ; margin-top: 10px">
    <thead>
        <tr>
            <th style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">N&deg; Cierre</th>
            <th style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">{{__('Fecha')}}</th>
            <th style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">{{__('Taquilla')}}</th>
            <th style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">{{__('Responsable')}}</th>
            <th style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">{{__('Operador')}}</th>
            
            @foreach($campos_monedas as $ind=>$value)
            <th style=" border:solid 1px; background-color: grey; font-size: 12px; font-weight: bold  ">{{$value}}</th>    
            @endforeach
        </tr>
    </thead>
    <tr>
    <tbody>
        @foreach($data as  $value)
       
        <tr>
        <td style=" border:solid 1px;  font-size: 12px;   ">{{$value->codigo}}</td>
        <td style=" border:solid 1px;  font-size: 12px;   ">{{showDate($value->fecha, "full")}}</td>
        <td style=" border:solid 1px;  font-size: 12px;   ">{{$value->getTaquilla->nombre}}</td>
        <td style=" border:solid 1px;  font-size: 12px;   ">{{Upper($value->getResponsable->name_user.' '.$value->getResponsable->surname_user)}}</td>
        <td style=" border:solid 1px;  font-size: 12px;   ">{{Upper($value->getOperador->name_user.' '.$value->getResponsable->surname_user)}}</td>
        
        @foreach($value->getDataCierre()['TotalesD'] as $value2)
            @foreach($campos_monedas as $ind3=>$value3)
            @php
                $campo = "moneda".$ind3;
                
            @endphp
            <td style=" border:solid 1px; text-align:right; font-size: 12px;  ">{{muestraFloat($value2->{$campo})  }}</td>    
            @endforeach
        @endforeach
        </tr>
        @endforeach
    </tbody>
        
    </tr>
</table>

@endsection