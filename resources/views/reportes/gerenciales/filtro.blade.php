<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Graficos Estadísticos')}}</h3>

            <div class="card-tools">
                <a href="{{route('gerenciales')}}" onClick="setAjax(this, event)"   class="form-control bg-success">
                    <li class="fa fa-arrow-left "></li> Regresar 
                </a> 


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            {{Form::open([ "route"=>"gerenciales.estadisticos",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
            <div class="row">


                <div class="form-group">
                    <label>Rango de Fechas: </label>


                </div>

                <div class="form-group">


                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                        </div>
                        {{Form::text('rango', '', ["readonly"=>"readonly", "id"=>"rango", "class"=>"form-control float-right required", "placeholder"=>"Rango"])}}

                    </div>
                    <!-- /.input group -->
                </div>







                <div class="col-md-3  ">
                    <span style="cursor:pointer" class="fa fa-search" id="b1"></span>
                </div>



            </div>
            {{ Form::close() }} 
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Tipos de Monedas Recaudadas</h3>


                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Totales por Categorías</h3>


                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="barChart2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Bar Chart</h3>


                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="barChart3" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="card card-info">
                        <div class="card-header">
                            <h3 class="card-title">Bar Chart</h3>


                        </div>
                        <div class="card-body">
                            <div class="chart">
                                <canvas id="barChart4" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>



            </div>
        </div>
    </div>





    <script type="text/javascript">
        var A;
        
        function a(img){
            $.post("{{route('create_img')}}", "img="+img+"&_token={{csrf_token()}}" ,function (response){
                
            });
        }
        
        
        $(document).ready(function () {

            moment.locale("es");
            $('#rango').daterangepicker({
                "maxDate": moment(),
                "locale": {
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Cancelar"
                }
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    //error.addClass('invalid-feedback');
                    //element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


            $("#b1").on("click", function () {
                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    money_total = [];
                    for (i in response.money.total) {
                        money_total.push(response.money.total[i]);
                    }
                    money_label = [];
                    for (i in response.money.label) {
                        money_label.push(response.money.label[i]);
                    }
                    setChartMoney(money_total, money_label);

                    /**************************************************/
                    cat_total = [];
                    for (i in response.cat.total) {
                        cat_total.push(response.cat.total[i]);
                    }
                    cat_label = [];
                    for (i in response.cat.label) {
                        cat_label.push(response.cat.label[i]);
                    }


                    setChartCat(cat_total, cat_label);
                    /***********************************/
                    est_total = [];
                    for (i in response.est.total) {
                        est_total.push(response.est.total[i]);
                    }
                    est_label = [];
                    for (i in response.est.label) {
                        est_label.push(response.est.label[i]);
                    }


                    setChartEst(est_total, est_label);

                }, 'json').fail(function () {

                });
            });









        });
        function setChartMoney(data, labels) {
            var donutChartCanvas = $('#barChart').get(0).getContext('2d');
            var donutData = {
                labels: labels,
                datasets: [
                    {
                        data: data,
                        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc']
                    }
                ]
            };
            var donutOptions = {
                maintainAspectRatio: false,
                responsive: true,
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            new Chart(donutChartCanvas, {
                type: 'doughnut',
                data: donutData,
                options: donutOptions
            });

        }

        function setChartCat(data, labels) {



            var donutData = {
                labels: labels,
                datasets: [
                    {
                        data: data,
                        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de']
                    }
                ]
            };
            var pieChartCanvas = $('#barChart2').get(0).getContext('2d');
            var pieData = donutData;
            var pieOptions = {
                maintainAspectRatio: false,
                responsive: true
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            new Chart(pieChartCanvas, {
                type: 'pie',
                data: pieData,
                options: pieOptions
            })
        }

        function setChartEst(data, labels) {

            var areaChartData = {
                labels: labels,
                datasets: [
                    {
                        label: 'Totales por Estados',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: data
                    }
                ]
            };


            var barChartCanvas = $('#barChart3').get(0).getContext('2d');
            var barChartData = $.extend(true, {}, areaChartData);
            var temp0 = areaChartData.datasets[0];
            
            barChartData.datasets[0] = temp0;
            

            var barChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                datasetFill: false
            };

           A =  new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
            });

           
        }
    </script>
</div>