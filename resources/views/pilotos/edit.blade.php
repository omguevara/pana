

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Editar Piloto')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('pilotos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["pilotos.edit", $Pilotos->crypt_id], 'files'=>'true',  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="type_document">{{__('Tipo de Documento')}}</label>
                        {{Form::select('type_document', ["V"=>"V-", "E"=>"E-", "P"=>"P-"], $Pilotos->tipo_documento, [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="document">{{__('Document')}}</label>
                        {{Form::text("document", $Pilotos->documento, ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="name_user">{{__('Name')}}</label>
                        {{Form::text("name_user", $Pilotos->nombres, ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="surname_user">{{__('Surname')}}</label>
                        {{Form::text("surname_user", $Pilotos->apellidos, ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="phone">{{__('Phone')}}</label>
                        {{Form::text("phone", $Pilotos->telefono, [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="email">{{__('Email')}}</label>
                        {{Form::text("email", $Pilotos->correo, [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}

                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>





<script type="text/javascript">




    $(document).ready(function () {



        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $('.number').numeric({negative: false});
         $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                var formData = new FormData(document.getElementById("frmPrinc1"));


                $.ajax({
                    url: this.action,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('pilotos')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });




            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
