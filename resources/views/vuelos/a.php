<table cellspacing="0" cellpadding="1">
    <thead>    
        <tr valign="top">
            <td >
                N°
            </td>
            <td >
                NORMATIVA
            </td>
            <td >
                N° GACETA OFICIAL
            </td>
            <td >
                FECHA
            </td>
        </tr>
    </thead>    
    <tbody>    |
        <tr valign="top">
            <td >
                1
            </td>
            <td >
                Reglamento de la Ley de Correos
            </td>
            <td >
                S/N
            </td>
            <td >
                31-12-1938
            </td>
        </tr>
        <tr valign="top">
            <td >
                2
            </td>
            <td >
                Reglamento del Servicio Postal Interno de Giros Postales y Postales Telegráficos,
            </td>
            <td >
                20.063
            </td>
            <td >
                22-12-1939
            </td>
        </tr>
        <tr valign="top">
            <td >
                3
            </td>
            <td >
                Decreto por el cual se modifica el Decreto reglamentario del servicio postal interno de valores declarados
            </td>
            <td >
                20.019
            </td>
            <td >
                01-11-1939
            </td>
        </tr>
        <tr valign="top">
            <td >
                4
            </td>
            <td >
                Decreto por el cual se dicta el reglamento del Servicio Postal Interno de Envíos contra Reembolso
            </td>
            <td >
                20.050
            </td>
            <td >
                03-08-1940
            </td>
        </tr>
        <tr valign="top">
            <td >
                5
            </td>
            <td >
                Reglamento Interno del Servicio del Bultos Postales. G. O. 20.404 de fecha 1 de febrero de 1941.
            </td>
            <td >
                20.404
            </td>
            <td >
                01-02-1941
            </td>
        </tr>
        <tr valign="top">
            <td >
                6
            </td>
            <td >
                Reglamento Orgánico de Correo
            </td>
            <td >
                S/N
            </td>
            <td >
                02-05-1941
            </td>
        </tr>
        <tr valign="top">
            <td >
                7
            </td>
            <td >
                Reglamento del Servicio Público de Telégrafos
            </td>
            <td >
                Extraordinaria
            </td>
            <td >
                03-05-1941
            </td>
        </tr>
        <tr valign="top">
            <td >
                8
            </td>
            <td >
                Reglamento de las Inspectorías Técnicas Postales.
            </td>
            <td >
                55.192
            </td>
            <td >
                20-12-1946
            </td>
        </tr>
        <tr valign="top">
            <td >
                9
            </td>
            <td >
                Decreto por la cual se crea en la Administración Postal Venezolana en los Servicios Interior y Urbano, el denominado “Expreso”.
            </td>
            <td >
                22.478
            </td>
            <td >
                29-11-1947
            </td>
        </tr>
        <tr valign="top">
            <td >
                10
            </td>
            <td >
                Aviso: Obligatoriedad de usar el correo para enviar correspondencia de primera clase.
            </td>
            <td >
                23.426
            </td>
            <td >
                10-01-1951
            </td>
        </tr>
        <tr valign="top">
            <td >
                11
            </td>
            <td >
                Reglamento sobre Franqueo Mecánico de Correspondencia.
            </td>
            <td >
                24.432
            </td>
            <td >
                05-05-1954
            </td>
        </tr>
        <tr valign="top">
            <td >
                12
            </td>
            <td >
                Ley de Correos.
            </td>
            <td >
                25.841
            </td>
            <td >
                18-12-1958
            </td>
        </tr>
        <tr valign="top">
            <td >
                13
            </td>
            <td >
                Decreto N° 568, por el cual se dicta el Reglamento del Servicio de Porte a Pagar para Propaganda Comercial
            </td>
            <td >
                25.881
            </td>
            <td >
                06-02-1959
            </td>
        </tr>
        <tr valign="top">
            <td >
                14
            </td>
            <td >
                Decreto N° 38 por el cual se dicta el Reglamento del Servicio Interior de Porte Pagado
            </td>
            <td >
                588 Extraordinario
            </td>
            <td >
                06-05-1959
            </td>
        </tr>
        <tr valign="top">
            <td >
                15
            </td>
            <td >
                Resolución por la cual se dictan las bases generales que regirán en los contratos de transporte de Piezas Postales.
            </td>
            <td >
                30.294
            </td>
            <td >
                03-01-1974
            </td>
        </tr>
        <tr valign="top">
            <td >
                16
            </td>
            <td >
                Resolución por la cual se fijan los diferentes Servicios y sus tarifas correspondientes en el Servicio Público de Telegrama.
            </td>
            <td >
                30.527
            </td>
            <td >
                17-10-1974
            </td>
        </tr>
        <tr valign="top">
            <td >
                17
            </td>
            <td >
                Resolución por la cual los empleados de las oficinas telegráficas, deberán recibir y tramitar con la debida prontitud y diligencia todo tipo de mensajes telegráficos que por ante ellos deben ser cursados, dentro del horario de atención al público normal o habilitado y durante todos los días del año, feriados o no
            </td>
            <td >
                31.011
            </td>
            <td >
                28-06-1976
            </td>
        </tr>
        <tr valign="top">
            <td >
                18
            </td>
            <td >
                Decreto N° 2.875. mediante el cual se dicta el Reglamento Parcial de la Ley que Crea el Instituto Postal Telegráfico de Venezuela,
            </td>
            <td >
                31.595
            </td>
            <td >
                18-10-1978
            </td>
        </tr>
        <tr valign="top">
            <td >
                19
            </td>
            <td >
                Resolución reglamentaria sobre recepción y remisión de documentos por correo
            </td>
            <td >
                32.385
            </td>
            <td >
                04-01-1982
            </td>
        </tr>
        <tr valign="top">
            <td >
                20
            </td>
            <td >
                Reglamento del Servicio de Apartados de Correos
            </td>
            <td >
                32.860
            </td>
            <td >
                24-11-1983
            </td>
        </tr>
        <tr valign="top">
            <td >
                21
            </td>
            <td >
                Reglamento sobre Concesión de los Servicios de Correos
            </td>
            <td >
                32.784
            </td>
            <td >
                08-08-1983
            </td>
        </tr>
        <tr valign="top">
            <td >
                22
            </td>
            <td >
                Reglamento de Servicio Interno “Entrega Especial”
            </td>
            <td >
                33.317
            </td>
            <td >
                27-09-1985
            </td>
        </tr>
        <tr valign="top">
            <td >
                23
            </td>
            <td >
                Resolución por la cual se provee el Reglamento Interno para Citaciones y Notificaciones Judiciales por Correo
            </td>
            <td >
                33.678
            </td>
            <td >
                16-03-1987
            </td>
        </tr>
        <tr valign="top">
            <td >
                24
            </td>
            <td >
                Providencia por la cual se dispone que cada pieza de correspondencia de hasta dos kilogramos, enviada a través de servicios de correos privados, el remitente pagará el importe del franqueo postal obligatorio, a favor de IPOSTEL
            </td>
            <td >
                34.854
            </td>
            <td >
                03-12-1991
            </td>
        </tr>
        <tr valign="top">
            <td >
                25
            </td>
            <td >
                Resolución por la cual se dicta la Reforma parcial de la Resolución que regula los Servicios de mensajería Internacional “Courier”
            </td>
            <td >
                36.244
            </td>
            <td >
                09-07-1997
            </td>
        </tr>
        <tr valign="top">
            <td >
                26
            </td>
            <td >
                Decreto con Rango y Fuerza de Ley sobre Promoción de la Inversión Privada bajo Régimen de Concesiones, que reforma el Decreto Ley N° 138 de fecha 20 de abril de 1994 sobre Concesiones de Obras Públicas y Servicios Públicos Nacionales
            </td>
            <td >
                5.394 Extraordinario
            </td>
            <td >
                25-10-1999
            </td>
        </tr>
        <tr valign="top">
            <td >
                27
            </td>
            <td >
                Decreto con Rango y Fuerza de Ley de Reforma Parcial de la Ley que Crea el Instituto Postal Telegráfico de Venezuela
            </td>
            <td >
                5.398 Extraordinario
            </td>
            <td >
                26-10-1999
            </td>
        </tr>
        <tr valign="top">
            <td >
                28
            </td>
            <td >
                Resolución mediante la cual se dicta la Normativa para la Apertura de Cuenta Auxiliar para el registro del Franqueo Postal Obligatorio a favor de IPOSTEL percibidos por los Operadores Postales Privados.
            </td>
            <td >
                40.165
            </td>
            <td >
                13-05-2013
            </td>
        </tr>
        <tr valign="top">
            <td >
                29
            </td>
            <td >
                Providencia mediante la cual se establece el ajuste de las Tarifas del Franqueo Postal Obligatorio para los Operadores Postales Privados referente a los pagos de obligaciones y deberes formales o materiales, otorgamiento o renovación de Concesión Postal y su correspondiente clasificación. G.O N° 41.912 de fecha 01 de julio de 2020
            </td>
            <td >
                41.912
            </td>
            <td >
                01-07-2020
            </td>
        </tr>
        <tr valign="top">
            <td >
                30
            </td>
            <td >
                Decreto por la cual se dicta el Reglamento de los Transporte de Correos
            </td>
            <td ></td>
            <td ></td>
        </tr>
    </tbody>
</table>