<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>VUELO</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 40%;
            float:left;
            margin-left: 5%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
            bottom: 0;
            margin-bottom: 110px;
            position: absolute;
            width:100%



        }
    </style>
    <body>

        <table  width="100%" cellspacing="0" >
            <tr>

                <td colspan="4" ><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>

            </tr>
            <tr>
                <td colspan="4" >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" align="center" ><h1>REGISTRO DE VUELO</h1></td>
            </tr>
        </table>
        <table  width="100%" cellspacing="0" >
            <tr>
                <td>Fecha de Operación:</td>
                <td>{{$Vuelo->fecha_operacion2}}</td>
                <td>Hora de Operación:</td>
                <td>{{$Vuelo->hora_operacion}}</td>
            </tr>
            <tr>
                <td>Tipo de Operación:</td>
                <td>{{$Vuelo->tipo_operacion}}</td>
                <td>Tipo de Vuelo:</td>
                <td>{{$Vuelo->tipo_vuelo}}</td>
            </tr>
            <tr>
                <td>Piloto:</td>
                <td>{{$Vuelo->piloto->full_name}}</td>
                <td>Aeronave:</td>
                <td>{{$Vuelo->aeronave->full_nombre_tn}}</td>
            </tr>
        </table>
        <table  width="100%" cellspacing="0" >
            @if ($Vuelo->tipo_operacion_id==1)
            <tr>
                <td>Aeropuerto Origen:</td>
                <td>{{ $Vuelo->origen_destino->full_nombre}}</td>
            </tr>
            <tr>
                <td>Aeropuerto Destino:</td>
                <td>{{$Vuelo->aeropuerto->full_nombre}}</td>
            </tr>
            <tr>
                <td>Pasajeros Desembarcados:</td>
                <td>{{$Vuelo->pasajeros_embarcados>0 ? $Vuelo->pasajeros_embarcados:$Vuelo->pasajeros_desembarcados}}</td>
            </tr>

            @else
            <tr>
                <td>Aeropuerto Origen:</td>
                <td>{{ $Vuelo->aeropuerto->full_nombre}}</td>
            </tr>
            <tr>
                <td>Aeropuerto Destino:</td>
                <td>{{$Vuelo->origen_destino->full_nombre}}</td>
            </tr>
            <tr>
                <td>Pasajeros Embarcados:</td>
                <td>{{$Vuelo->pasajeros_embarcados>0 ? $Vuelo->pasajeros_embarcados:$Vuelo->pasajeros_desembarcados}}</td>
            </tr>
            @endif
        </table>


        <table  width="100%" cellspacing="0" >
            <tr>
                <td colspan="4" align="center" ><h4>SERVICIOS APLICADOS</h4></td>
            </tr>
        </table>

        <table style="width:100%" >

            <tr>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">N&deg;</td>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

            </tr>
            @php
            $estacionamiento = false;
            @endphp
            @foreach($Vuelo->getDetalle as $key=>$value)
            @if (Str::contains($value->full_descripcion, '2.2'))
            @php
            $estacionamiento = true;
            @endphp
            @endif
            <tr>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$key+1}}</td>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{muestraFloat($value->cant, 2).' '.$value->nomenclatura }}</td>
                <td style="width:80%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$value->full_descripcion}}</td>

            </tr>
            @endforeach
        </table>
        @if ($estacionamiento==true)
        <div class="input-group mt-2"  >
            <div class="input-group-prepend">
                <span class="input-group-text"><b>Hora de Llegada a Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
            </div>
            {{Form::text("hora_llegada", date('d/m/Y H:i',strtotime($Vuelo->hora_llegada)), ["class"=>"form-control ", "disabled"=>"disabled"])}}

        </div>
        <div class="input-group mt-2"  >
            <div class="input-group-prepend">
                <span class="input-group-text"><b>Hora de Salida de Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
            </div>
            {{Form::text("hora_salida", date('d/m/Y H:i',strtotime($Vuelo->hora_salida)), ["class"=>"form-control ", "disabled"=>"disabled"])}}

        </div>
        @endif


        <table  width="100%" cellspacing="0" >
            <tr>
                <td colspan="4" align="center" ><h4>Pasajeros</h4></td>
            </tr>
        </table>
     
        <table style="width:100%" >

            <tr>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">N&deg;</td>

                <td style="width:90%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

            </tr>
            @foreach($Vuelo->pasajeros as $key=>$value)
            <tr>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$key+1}}</td>

                <td style="width:80%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$value->pasajero}}</td>

            </tr>
            @endforeach
        </table>




        <table  width="100%" cellspacing="0" >
            <tr>
                <td>Observación:</td>
                <td>{{ $Vuelo->observaciones}}</td>
            </tr>
        </table>


        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                //setPint()
                //window.close();

            });


        </script>



    </body>
</html>
