

<div class="card card-primary card-outline card-outline-tabs">
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="#listDataTablePilotos" role="tab" aria-controls="listDataTablePilotos" aria-selected="true">Listado de Pilotos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#formCreatePilotos" role="tab" aria-controls="formCreatePilotos" aria-selected="false">Crear Piloto</a>
            </li>

        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">
            <div class="tab-pane  active show" id="listDataTablePilotos" role="tabpanel" >
                <table id="listPilotos" class="display compact">
                    <thead>
                        <tr>
                            <th style="width:50px">#</th>
                            <th>Piloto</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane  " id="formCreatePilotos" role="tabpanel" >
                {{Form::open([ 'route'=>'pilotos_plus',  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                <div class="row">
                    <div class="col-sm-12 col-md-12 " >
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Document')}}:</b></span>
                            </div>
                            <div class="input-group-prepend" >
                                {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}
                            </div>
                            {{Form::text("document", "", ["required"=>"required", "maxlength"=>"12" ,"class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 " >
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Name')}}:</b></span>
                            </div>

                            {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 " >
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Surname')}}:</b></span>
                            </div>

                            {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 " >
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                            </div>

                            {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12 " >
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Email')}}:</b></span>
                            </div>

                            {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-3 offset-md-5" >
                    <div class="input-group mt-2">
                        {{Form::button('<li class="fa fa-save"></li> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                    </div>
                </div>

                {{ Form::close() }} 
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>





