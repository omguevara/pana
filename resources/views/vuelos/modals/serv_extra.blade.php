
{{Form::open([ "route"=>"get_servs_ext",  'id'=>'frmServ','autocomplete'=>'Off'])}}
<div class="row">
    <div class="col-sm-12 col-md-12" >

        <table style="width:100%" id="tableServExt">
            <tr>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                <td style="width:15%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                    <b>CANT.</b>
                </td>
                <td style="width:30%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                    <b>DESCRIPCI&Oacute;N</b>
                </td>
                <td style="width:45%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">
                    {{Form::search("serv_extra_text", "", ["class"=>" form-control", "id"=>"selectSearchItem", "placeholder"=>"Ingrese el Servicio"])}}
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="row text-center">
{{Form::button('<li class="fa fa-check"></li> '.__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicar"])}}    
</div>
{{ Form::close() }} 
