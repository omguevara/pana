<div class="modal fade" id="modal-vuelo"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalCreateFly" class="modal-content">
            <div class="modal-header">
                <h4 class="">Vuelo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>
            <div id="modal-body-princ-vuelo"  class="modal-body">
                <div class="row">
                    
                    <div class="col-md-5 border-right">
                        {{Form::open(["route"=>"vuelos",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
                        {{Form::hidden('tipo_id', "", ["id"=>"tipo_id"])}}
                        {{Form::hidden('id', "", ["id"=>"id"])}}
                        {{Form::hidden('nacionalidad_id', "", ["id"=>"nacionalidad_id"])}}        
                        {{Form::hidden('aeronave_id', "", ["id"=>"aeronave_id"])}}        
                        {{Form::hidden('piloto_id', "", ["id"=>"piloto_id"])}}        
                        {{Form::hidden('newPortal', "1", ["id"=>"newPortal"])}}        
                        {{Form::hidden('proc_dest_id', "", ["id"=>"proc_dest_id"])}}        

                        
                        
                        {{Form::select('prod_servd[]', [], "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}
                        {{Form::select('prod_servd_remove[]', [], "", ["style"=>"display:none", "id"=>"prod_servd_remove" ,"multiple"=>"multiple"])}}
                        <h5 class="border-bottom">Datos del Vuelo</h5>

                        @if ($Aeropuertos->count()>1)
                        <div class="row">
                            <div  class="col-md-12 ">
                                <div class="input-group mt-2"  >
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Aeropuerto:</b></span>
                                    </div>
                                    {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                                </div>

                            </div>

                        </div>
                        @endif

                        <div class="row">
                            <div  class="col-md-11 ">
                                <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                    <div class="input-group-prepend">
                                        <span  class="input-group-text"><b>Fecha de Operación:</b></span>
                                    </div>
                                    {{Form::text("fecha_operacion", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                                    <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                        <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div  class="col-md-1 mt-2">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Fecha cuando se genero el vuelo" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div  class="col-md-11 ">
                                <div class="input-group mt-2"  >
                                    <div class="input-group-prepend">
                                        <span  class="input-group-text"><b>Hora de Operación: <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                    </div>
                                    {{Form::text("hora_operacion", date("H:i"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  timer required ", "id"=>"hora_operacion" ,"required"=>"required"])}}
                                </div>
                            </div>
                            <div  class="col-md-1 mt-2">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Hora cuando se genero el vuelo, Recuerde Registrar la Hora el Formato 24H" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>
                        <div id="contentTipo" class="row mt-2">
                            <div  class="col-md-11">
                                <div class="row ">
                                    <div id="btn_llegada" class="col-md-6">
                                        <button id="arrivo" type="button" onclick="llegada()"  class="btn btn-outline-primary btn-block">
                                            <i class="fas fa-plane-arrival"></i> Llegada
                                        </button>
                                    </div>
                                    <div id="btn_salida" class="col-md-6">
                                        <button id="despegue" type="button" onclick="salida()" class="btn btn-outline-primary btn-block">
                                            <i class="fas fa-plane-departure"></i> Salida
                                        </button>  
                                    </div>
                                </div>
                            </div>
                            <div  class="col-md-1">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Selecione si el Tipo de Operación de <b>Llegada</b> o <b>Salida</b>" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>
                        <div id="contentNacionalidad"  class="row mt-2">

                            <div  class="col-md-11">
                                <div class="row ">
                                    <div id="btn_nacional" class="col-md-6">
                                        <button id="vuelo_nacional" type="button" onclick="nacional()"  class="btn btn-outline-primary btn-block">
                                            <i class="fas fa-flag"></i> Nacional
                                        </button>
                                    </div>
                                    <div id="btn_internacional" class="col-md-6">
                                        <button id="vuelo_internacional" type="button" onclick="internacional()" class="btn btn-outline-primary btn-block">
                                            <i class="fas fa-globe"></i> Internacional
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div  class="col-md-1">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Selecione si el Tipo <b>Nacional</b> o <b>Internacional</b>" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>


                        </div>

                        <div class="row mt-2">
                            <div  class="col-md-11 ">
                                <div class="input-group "  >
                                    <div class="input-group-prepend">
                                        <span  class="input-group-text"><b>Tipo de Aviación:</b></span>
                                    </div>
                                    {{Form::select('tipo_aviacion_id', ["1"=>"General", "2"=>"Oficial", "3"=>"Militar"], "1" , ["data-msg-required"=>"Campo Requerido",  "onChange"=>'$(".filaServExt").remove();',  "class"=>"form-control required  select2 ", "id"=>"tipo_aviacion_id" ,"required"=>"required"])}}
                                </div>

                            </div>
                            <div  class="col-md-1">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Selecione si el Tipo Aviación" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>

                        <div id="contentAeronave"  class="row mt-2">
                            <div  class="col-md-11">
                                <button id="getSelectAeronave" onClick="$('#idAeronave').click();" type="button"  class="btn btn-outline-primary btn-block">
                                    <i class="fas fa-plus"></i> <i class="fas fa-plane"></i> Seleccionar Aeronave
                                </button>



                            </div>
                            <div  class="col-md-1">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Seleccione La Aeronave" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>
                        <div id="contentPiloto"  class="row mt-2">
                            <div  class="col-md-11 mt-2">
                                <button id="getSelectPilot"  type="button" onClick="$('#idPiloto').click();"  class="btn btn-outline-primary btn-block ">
                                    <i class="fas fa-plus"></i> <i class="fa fa-user"></i> Seleccionar Piloto
                                </button>
                            </div>
                            <div  class="col-md-1 mt-2">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Seleccione al Piloto" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div  class="col-md-11 ">
                                <div class="input-group "  >
                                    <div class="input-group-prepend">
                                        <span  class="input-group-text"><b>LLeva Carga:</b></span>
                                    </div>
                                    {{Form::select('lleva_carga', ["1"=>"SI", "0"=>"NO"], "0" , [  "onChange"=>'checkTipoCarga(this)',  "class"=>"form-control", "id"=>"lleva_carga"])}}
                                    {{Form::text('carga', null, ['class' => 'form-control required money text-right', 'id' => 'carga', 'disabled' => 'disabled']) }}
                                </div>
                            

                            </div>
                            <div  class="col-md-1">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Marque <b>Si</b>, en el caso que la aeronave lleva alguna carga" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                        </div>


                        {{-- <div id="panel_tipo_carga" style="display: none;" class="row mt-2">
                            <div class="row">
                                <h3>Tipos de Carga</h3>

                                <div class="input-group">
                                    <div class="input-group-append">
                                        <p class="input-group-text"><b>Razón Social</b></p>
                                    </div>
                                    {{Form::text('combustible', null, ['class' => 'form-control required', 'id' => 'combustible']) }}
                                </div>
                                
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <p class="input-group-text"><b>Razón Social</b></p>
                                    </div>
                                    {{Form::text('limientos', null, ['class' => 'form-control required', 'id' => 'limientos']) }}

                                {{Form::text('medicamentos', null, ['class' => 'form-control required', 'id' => 'medicamentos']) }}

                                {{Form::text('Herramientas', null, ['class' => 'form-control required', 'id' => 'Herramientas']) }}
                               
    
                            </div>
                        </div> --}}


                        <div   class="row mt-2">    
                            <div  class="col-md-11 mt-2">
                                <div class="input-group mt-2"  >
                                    <div class="input-group-prepend">
                                        <span id="labelCantPax" class="input-group-text"><b>Pasajeros:</b></span>
                                    </div>
                                    {{Form::number("pax", "0", ["data-msg-required"=>"Campo Requerido",  "min"=>"0", "max"=>"999", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax", "placeholder"=>__('Cant. PAX')])}}    
                                </div>
                            </div>

                            <div  class="col-md-1 mt-3">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Cantidad de Pasajeros del Vuelo, si no posee pasajeros ingrese cero (0)" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                            
                            
                            <div  class="col-md-11 mt-2">
                                <div class="input-group mt-2"  >
                                    <div class="input-group-prepend">
                                        <span id="labelOrigDest" class="input-group-text"><b>Aeropuerto:</b></span>
                                    </div>
                                    
                                    <select name="orig_dest_id" id="orig_dest_id"  class="form-control select2 " >
                                        <option value="">Seleccione</option>
                                        @foreach($AeropuertosAll as $value)
                                        <option data-pais="{{$value->pais_id}}" value="{{$value->crypt_id}}">{{$value->full_nombre}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>

                            <div  class="col-md-1 mt-3">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Seleccione el Origen o Destino de la Operación" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                            
                            

                            <div  class="col-md-11 mt-2">

                                <div class="input-group mt-2"  >
                                    <div id="aplicaEst" class="input-group-prepend">
                                        <span  class="input-group-text"><b>Estacionamiento:</b></span>
                                    </div>
                                    <div  id="aplicaEstCheck" class="input-group-text">
                                        <input type="checkbox" class="switch" id="estacionamiento" value="1" name="estacionamiento" data-on-text="SI" data-off-text="NO"  data-off-color="danger" data-on-color="success">
                                    </div>
                                </div>
                            </div>
                            <div  class="col-md-1 mt-3">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Marque <b>Si</b>, en el caso que amerite cobrar estacionamiento" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                            <div  class="col-md-11 mt-2">
                                <div class="input-group mt-2"  >
                                    <div style="width:100%" class="input-group-prepend">
                                        <span style="width:100%" class="input-group-text"><b>Hora Llegada a Plataforma: </b></span>
                                    </div>


                                </div>
                            </div>
                            <div  class="col-md-1 mt-3">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Hora de Llegada a Plataforma de la Aeronave" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                            <div  class="col-md-12 ">
                                <div class="input-group "  >
                                    {{Form::text("hora_llegada", date('d/m/Y H:i'), ["readonly"=>"readonly", "class"=>"form-control    date_time", "id"=>"hora_llegada" ])}}
                                </div>
                            </div>
                            <div  class="col-md-11 mt-2">
                                <div class="input-group mt-2"  >
                                    <div style="width:100%" class="input-group-prepend">
                                        <span style="width:100%" class="input-group-text"><b>Hora Salida de Plataforma: </b></span>
                                    </div>
                                </div>
                            </div>
                            <div  class="col-md-1 mt-3">
                                <div class="row ">
                                    <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Hora de Salida de la Plataforma de la Aeronave" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                        <i class="fas fa-info"></i>
                                    </button>  
                                </div>
                            </div>
                            <div  class="col-md-12 ">
                                <div class="input-group "  >
                                    {{Form::text("hora_salida", date('d/m/Y H:i'),  ["readonly"=>"readonly",  "class"=>"form-control date_time  ", "id"=>"hora_salida" ])}}
                                </div>
                            </div>
                            <div  class="col-md-12 mt-2 ">
                                <div class="input-group  ">
                                    <label style="margin-bottom: unset !important" class="border-bottom">Observación:</label>
                                </div>
                            </div>
                            <div  class="col-md-12 ">
                                <div class="input-group mt-2 ">
                                    {{Form::textarea("observacion", "", ["class"=>"form-control ", "rows"=>"3", "id"=>"observacion", "placeholder"=>__('Observación')])}}    
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }} 
                    </div>
                    <div class="col-md-1 border-right">
                        <h5 class="border-bottom">Opciones</h5>
                        {{Form::button('<li class="fa fa-eye"></li> '.__("VER"),  ["onClick"=>"seeServ()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"save"])}}    
                        
                        {{Form::button('<li class="fa fa-plus"></li> '.__("SERV."),  ["type"=>"button", "class"=>"btn btn-primary mt-4", "id"=>"addServ", "onClick"=>"setServ()"])}}    
                    </div>
                    <div class="col-md-6">
                        <h5 class="border-bottom">Datos Complementarios</h5>
                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs"  role="tablist">
                                    <li class="nav-item" id="liIdServicios">
                                        <a class="nav-link active" data-toggle="pill" id="idServicios" href="#tabServicios" role="tab" aria-controls="tabServicios" aria-selected="true">Servicios</a>
                                    </li>
                                    <li class="nav-item" id="liIdAeronave">
                                        <a class="nav-link" data-toggle="pill" id="idAeronave" href="#tabAeronave" role="tab" aria-controls="tabAeronave" aria-selected="false">Aeronave</a>
                                    </li>
                                    <li class="nav-item" id="liIdPiloto">
                                        <a class="nav-link" data-toggle="pill" id="idPiloto" href="#tabPiloto" role="tab" aria-controls="tabPiloto" aria-selected="false">Piloto</a>
                                    </li>
                                    <li class="nav-item" id="liServ">
                                        <a class="nav-link" data-toggle="pill" id="idServ" href="#tabServ" role="tab" aria-controls="tabServ" aria-selected="false">Servicios Extra</a>
                                    </li>
                                    <li class="nav-item" id="liIdInformacion">
                                        <a class="nav-link" data-toggle="pill" id="idInformacion" href="#tabInformacion" role="tab" aria-controls="tabInformacion" aria-selected="false">Información</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" >
                                    <div class="tab-pane  active show" id="tabServicios" role="tabpanel" >
                                        <div class="row">
                                            <table id="listServApp" style="width:100%" >
                                                <tr>
                                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                                    <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>SERVICIOS</b></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="tabAeronave" role="tabpanel" >
                                        @include('vuelos.modals.aeronaves')
                                    </div>
                                    <div class="tab-pane  " id="tabPiloto" role="tabpanel" >
                                        @include('vuelos.modals.pilotos')
                                    </div>
                                    <div class="tab-pane  " id="tabServ" role="tabpanel" >
                                        @include('vuelos.modals.serv_extra')
                                    </div>
                                    <div class="tab-pane  " id="tabInformacion" role="tabpanel" >
                                        <div id="info" class="info-box bg-gradient-warning mt-2 ">
                                            <span class="info-box-icon"><i class="fa fa-info"></i></span>
                                            <div class="info-box-content">
                                                <span class="info-box-text">{{__('INFORMACIÓN:')}}</span>
                                                <span id="text-info" class="info-box-number">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer justify-content-between">
                </div>
            </div>
        </div>
    </div>
</div>

