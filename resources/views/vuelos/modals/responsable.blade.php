<h5 class="border-bottom">Responsable de la Aeronave</h5>
<div class="row">
    <div  class="col-md-12 ">
        <div class="input-group mt-2 ">
            <div class="input-group-prepend">
                <span class="input-group-text"><b>{{__('Document')}}:</b></span>
            </div>
            <div class="input-group-prepend" >
                {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
            </div>
            {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}    
            <div class="input-group-append" >
                <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
            </div>
        </div>
    </div>
    <div  class="col-md-12 ">
        <div class="input-group mt-2 ">
            <div class="input-group-prepend">
                <span class="input-group-text"><b>Responsable:</b></span>
            </div>
            {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required cliente", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
        </div>
    </div>
    <div  class="col-md-12 ">
        <div class="input-group mt-2 ">
            <div class="input-group-prepend">
                <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
            </div>
            {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone cliente", "id"=>"phone", "placeholder"=>__('Phone')])}}    
        </div>
    </div>
    <div  class="col-md-12 ">
        <div class="input-group mt-2 ">
            <div class="input-group-prepend">
                <span class="input-group-text"><b>{{__('Email')}}:</b></span>
            </div>
            {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  cliente", "id"=>"correo", "placeholder"=>__('Correo')])}}    
        </div>
    </div>
    <div  class="col-md-12 ">
        <div class="input-group mt-2 ">
            <div class="input-group-prepend">
                <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
            </div>
            {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required cliente", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
        </div>
    </div>
</div>