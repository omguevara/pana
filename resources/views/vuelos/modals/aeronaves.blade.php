

<div class="card card-primary card-outline card-outline-tabs">
    <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs"  role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="#listDataTable" role="tab" aria-controls="listDataTable" aria-selected="true">Listado de Aeronaves</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#formCreateAeronave" role="tab" aria-controls="formCreateAeronave" aria-selected="false">Crear Aeronave</a>
            </li>

        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">
            <div class="tab-pane  active show" id="listDataTable" role="tabpanel" >
                <table id="listAeronaves" class="display compact">
                    <thead>
                        <tr>
                            <th style="width:50px">#</th>
                            <th>Aeronave</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane fade" id="formCreateAeronave" role="tabpanel">
                {{Form::open([ "route"=>"aeronaves_plus", 'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-info" role="alert">
                            Por favor comuníquese con Operaciones de BAER Central para el registro de nuevas aeronaves.
                        </div>                          
                    </div>
                    {{-- <div class="col-sm-12 " >
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Matr&iacute;cula:</b></span>
                            </div>
                            {{Form::text("matricula", "", ["maxlength"=>"10", "minlength"=>"5", "required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required matricula", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}    
                        </div>
                    </div>
                    <div class="col-sm-12 " >
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Modelo:</b></span>
                            </div>
                            {{Form::text("nombre", "", ["maxlength"=>"20", "required"=>"required" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    
                        </div>
                    </div>
                    <div class="col-sm-12 " >
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Peso Máximo de Despegue')}} <span title="" class="badge bg-primary">TON</span>:</b></span>
                            </div>
                            {{Form::text("peso_maximo", "", ["required"=>"required", "maxlength"=>"8", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    
                        </div>
                    </div>
                    <div class="col-sm-12 " >
                        <div class="input-group mt-2 text-center">
                            {{Form::button('<li class="fa fa-save"></li> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                        </div>
                    </div> --}}

                </div>
                {{ Form::close() }} 
            </div>
        </div>

    </div>
    <!-- /.card -->
</div>

