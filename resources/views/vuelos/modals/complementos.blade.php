<div class="modal fade" id="modal-princ-comp"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalExt" class="modal-content">
            <div class="modal-header">
                <h4 class="">Imformación Complementaría</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>


            <div  class="modal-body">
                <div class="card card-primary card-tabs">
                    <div class="card-header p-0 pt-1">
                        <ul class="nav nav-tabs" id="tabCompl" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#data1" role="tab" aria-controls="data1" aria-selected="true">Responsable de la Aeronave</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " data-toggle="pill" href="#data2" role="tab" aria-controls="data2" aria-selected="true">Datos Complementario</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#data3" role="tab" aria-controls="data3" aria-selected="false">Pasajeros</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#data4" role="tab" aria-controls="data4" aria-selected="false">Plataforma</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-one-tabContent">
                            <div class="tab-pane  active show" id="data1" role="tabpanel" >
                                <div class="row">
                                    <div  class="col-md-6 offset-md-3">
                                        {{Form::open([ "route"=>"vuelos.save_complementos",  'id'=>'frmCompResp','autocomplete'=>'Off'])}}
                                        {{Form::hidden('id', "", ["id"=>"id"])}}
                                        {{Form::hidden('complemento', "responsable", ["id"=>"complemento"])}}
                                        @include('vuelos.modals.responsable')
                                        {{Form::button('<li class="fa fa-save"></li> '.__("Guardar"),  ["type"=>"button", "onClick"=>"saveComplementos(this)", "class"=>"btn btn-primary"])}}    
                                        {{ Form::close() }} 
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane  " id="data2" role="tabpanel" >
                                <div class="row">

                                    <div  class="col-md-6 offset-md-3">
                                        {{Form::open([ "route"=>"vuelos.save_complementos",  'id'=>'frmCompOrigDest1','autocomplete'=>'Off'])}}
                                        {{Form::hidden('id', "", ["id"=>"id"])}}
                                        {{Form::hidden('complemento', "orig_dest", ["id"=>"complemento"])}}
                                        {{Form::hidden('op', "1", ["id"=>"op"])}}
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="orig_dest_text" class="input-group-text"><b>Aeropuerto:</b></span>
                                            </div>
                                            <select name="orig_dest" id="orig_dest" data-msg-required="Campo Requerido" class="form-control required  select2 " required="required">
                                                <option value="">Seleccione</option>
                                                @foreach($AeropuertosAll as $value)
                                                <option data-pais="{{$value->pais_id}}" value="{{$value->crypt_id}}">{{$value->full_nombre}}</option>
                                                @endforeach
                                            </select>
                                            
                                        </div>
                                        <div    class="rows text-center mt-2  ">
                                            {{Form::button('<li class="fa fa-save"></li> '.__("Guardar"),  ["type"=>"button", "onClick"=>"saveComplementos(this)", "class"=>"btn btn-success"])}}    
                                        </div>
                                        {{ Form::close() }}
                                    </div>



                                    


                                </div>
                            </div>
                            <div class="tab-pane   " id="data3" role="tabpanel" >
                                {{Form::open([ "route"=>"vuelos.save_complementos",  'id'=>'frmCompPax','autocomplete'=>'Off'])}}
                                {{Form::hidden('id', "", ["id"=>"id"])}}
                                {{Form::hidden('complemento', "pax", ["id"=>"complemento"])}}
                                <table class="table table-sm">
                                    <thead>
                                        <tr>
                                            <th style="width: 50px">#</th>
                                            <th>Documento:</th>
                                            <th>Nombres</th>
                                            <th>Apellidos</th>
                                            <th>Genero</th>
                                        </tr>
                                    </thead>
                                    <tbody id="listDataPax">

                                    </tbody>
                                </table>
                                {{Form::button('<li class="fa fa-save"></li> '.__("Guardar"),  ["type"=>"button", "onClick"=>"saveComplementos(this)", "class"=>"btn btn-primary"])}}    
                                {{ Form::close() }} 
                            </div>
                            <div class="tab-pane   " id="data4" role="tabpanel" >
                                {{Form::open([ "route"=>"vuelos.save_complementos",  'id'=>'frmCompPlat','autocomplete'=>'Off'])}}
                                {{Form::hidden('id', "", ["id"=>"id"])}}
                                {{Form::hidden('complemento', "plat", ["id"=>"complemento"])}}

                                <div id="plataformas"   class="rows text-center   ">
                                    {{Form::button('<li class="fa fa-reload"></li> '.__("Cargar Plataformas"),  ["type"=>"button", "onClick"=>"cargarPlat()", "class"=>"btn btn-success"])}}    
                                </div>

                                {{ Form::close() }} 
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">


            </div>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

