




<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Anular Vuelo ')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{$back}}" id="backP" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["get_vuelos.disable", $Vuelo->crypt_id],'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}



        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-6 offset-md-3  ">
                    
                    <h3>Datos del Vuelo</h3>
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::text('aeropuerto_id',  $Vuelo->aeropuerto->full_nombre , [   "class"=>"form-control ", "disabled"=>"disabled"])}}

                        </div>


                        <div class="input-group mt-2"  >
                            
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Nacionalidad:</b></span>
                            </div>
                            {{Form::text('tipo_vuelo_id',  $Vuelo->tipo_vuelo , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>

                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Operaci&oacute;n:</b></span>
                            </div>
                            {{Form::text('fecha_operacion',  $Vuelo->fecha_operacion2 , [   "class"=>"form-control ", "disabled"=>"disabled"])}}

                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora de Operaci&oacute;n:</b></span>
                            </div>
                            {{Form::text('hora_operacion',  $Vuelo->hora_operacion , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::text('aeronave_id',  $Vuelo->aeronave->full_nombre_tn , [   "class"=>"form-control ", "disabled"=>"disabled"])}}


                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::text('piloto_id',  $Vuelo->piloto->full_name , [   "class"=>"form-control ", "disabled"=>"disabled"])}}


                        </div>
                        
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text('cant_pasajeros',  $Vuelo->cant_pasajeros , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observación:</b></span>
                            </div>
                            {{Form::text('observaciones',  $Vuelo->observaciones , [   "class"=>"form-control ", "disabled"=>"disabled"])}}

                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Motivo de la Anulación:</b></span>
                            </div>
                            {{Form::textarea('motivo_anulacion',  "" , ["rows"=>"4", "id"=>"motivo_anulacion",   "class"=>"form-control ", "required"=>"required"])}}

                        </div>
                        <div   class="rows text-center mt-2  ">
                            {{Form::button('<li class="fa fa-trash"></li> '.__("Anular Vuelo"),  ["type"=>"button", "class"=>"btn btn-danger",  "id"=>"anular"])}}    
                        </div>
                        
                    </div>  
                </div>  
                


            </div>  

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>





    <script type="text/javascript">

        $(document).ready(function () {
            $("#motivo_anulacion").focus();

            $("#anular").on("click", function () {
                if (   $("#frmPrinc1").valid()   ){
                    Swal.fire({
                        title: 'Esta Seguro que Desea Anular el Vuelo?',
                        html: "Confirmaci&oacute;n",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<li class="fa fa-save"></li> Si',
                        cancelButtonText: '<li class="fa fa-undo"></li>  No'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $("#frmPrinc1").prepend(LOADING);
                            $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                                $(".overlay-wrapper").remove();
                                if (response.status == 1) {
                                    $("#frmPrinc1").trigger("reset");
                                    $("#backP").click();
                                } else {
                                }

                            }).fail(function () {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error al Guardar')}}"
                                });
                            });


                        }
                    });
                }else{
                    $("#motivo_anulacion").focus();
                }

            });


        });


    </script>

</div>

