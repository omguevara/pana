<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Consulta Registro de Vuelo ')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#modal-info').modal('show')" class="btn btn-tool" ><i class="fas fa-info"></i></a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"get_vuelos",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{ method_field('POST') }}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div  class="col-sm-12 col-md-12 col-lg-4 mt-2">
                    <div class="form-group row ">
                        <label for="aeropuerto_id" class="col-sm-3 col-form-label">Aeropuerto</label>
                        <div class="col-sm-9">
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "class"=>"form-control select2 ", "id"=>"aeropuerto_id" ])}}
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-12  col-lg-3 mt-1  ">
                    <div class="form-group row ">
                        <label for="fecha_operacion" class="col-sm-3 col-form-label">Fechas</label>
                        <div class="col-sm-9">
                            {{Form::text("fecha_operacion", $RANGO_MES, ["data-msg-required"=>"Campo Requerido",    "class"=>"form-control ", "id"=>"fecha_operacion", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-12 col-lg-4 mt-1">
                    <div class="form-group row ">
                        <label for="aeronave_id" class="col-sm-3 col-form-label">Aeronave</label>
                        <div class="col-sm-9">
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",     "class"=>"form-control  select2 ", "id"=>"aeronave_id" ])}}
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-12 col-lg-1 mt-1">
                    {{Form::button('<li class="fa fa-search"></li>  ',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"searchData"])}}
                    {{Form::button('<li class="fa fa-file-excel"></li>  ',  ["type"=>"button", "class"=>"btn btn-success", "id"=>"downExcel1"])}}
                </div>
            </div>
            <div class="row">
                <table id="example1" class="display compact table-hover">
                    <thead>
                        <tr>
                            <th>{{__('Actions')}}</th>
                            <th>{{__('NRO. OPERACIÓN')}}</th>
                            <th>{{__('FECHA')}}</th>

                            <th>{{__('AEROPUERTO')}}</th>
                            <th>{{__('AERONAVE')}}</th>

                            <th>{{__('PILOTO')}}</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>


        <!-- /.card-body -->
    </div>

    {{ Form::close() }}


    <div class="modal fade" id="modal-see-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Ver Factura</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div  class="modal-body">
                    <div id="modal-see-fact-body" class="row">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-info"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xs">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">LEYENDA</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div  class="modal-body">
                    <table style="width:100%" >

                        <tr class="alert-danger">
                            <td ></td>
                            <td ><b>VUELO ANULADO.</b></td>
                        </tr>
                        <tr class="alert-success">
                            <td ></td>
                            <td ><b>VUELO APROBADO.</b></td>
                        </tr>
                        <tr class="alert-info">
                            <td ></td>
                            <td ><b>VUELO EN PROFORMA.</b></td>
                        </tr>
                        <tr class="alert-warning">
                            <td ></td>
                            <td ><b>VUELO EXONERADO.</b></td>
                        </tr>
                        <tr>
                            <td ></td>
                            <td ><b>VUELO EN PROCESO DE VERIFICACIÓN.</b></td>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-serv-fly"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Servicios Aplicados</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                <div  class="modal-body">
                    <div class="row">
                        <table style="width:100%" id="datFactura">
                            <tr>
                                <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>N&deg;</b></td>
                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>OP.</b></td>
                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                <td style="width:75%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                            </tr>

                        </table>



                    </div>

                </div>
                <div class="modal-footer justify-content-between">




                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <script type="text/javascript">
      
        var table1 = null;
        var RouteDetail = '';
        $(document).ready(function () {
            $(".select2").select2();
            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            $('#fecha_operacion').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });
            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true
            });
            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
            $('.number').numeric({negative: false});


            table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                order: [[3, 'desc']],

                ajax: {
                    url:"{{route('get_vuelos.datatable')}}",
                    data: function (d) {
                        d.aeropuerto = $('#aeropuerto_id').val(),
                        d.fechas = $('#fecha_operacion').val(),
                        d.nave = $('#aeronave_id').val(),
                        d.search = $('input[type="search"]').val()

                    }
                },
                serverSide: true,
                processing: true,
                
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'action'},
                    {data: 'operacion'},
                    {data: 'fecha_operacion2'},
                    {data: 'aeropuerto.full_nombre'},
                    {data: 'aeronave.full_nombre_tn'},
                    {data: 'piloto.full_name'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }


            });

            /*
             var table1 = $('#example1').DataTable({
             "paging": true,
             "lengthChange": false,
             "searching": true,
             "ordering": true,
             "info": true,
             "autoWidth": false,
             "responsive": true,
             order: [[3, 'desc']],
             data: data1,
             rowCallback: function (row, data) {
             $(row).addClass(data.class);
                 },
             columns: [
             {data: 'action'},
             {data: 'operacion'},
             {data: 'fecha_operacion2'},
             // {data: 'tipo_operacion'},
             {data: 'aeropuerto.full_nombre'},
             {data: 'aeronave.full_nombre_tn'},
             {data: 'piloto.full_name'},
             ], 
             language: {
             url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
             }
             
             
             });
             */
            $("#downExcel1").on("click", function () {
                $("[name=_method]").val("PUT");
                $("#frmPrinc1").submit();
                $("[name=_method]").val("POST");
            });


            $("#searchData").on("click", function () {
                table1.draw();
            });

        });
        function seetProforma(url) {
            $("#modal-see-fact").modal("show");
            $("#modal-see-fact").prepend(LOADING);
            $("#modal-see-fact-body").html("");
            $("#modal-see-fact-body").html("<iframe onload=\"closeLoading();\" name=\"iframePdf\" id=\"iframePdf\" style=\"width:100%; height: 600px; border:none\" src=\"" + url + "\"></iframe>");
        }

        function exonerarVuelo(ruta) {
            Swal.fire({
                title: 'Esta Seguro que Desea Exonerar el Vuelo?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No',

            }).then((result) => {
                if (result.isConfirmed) {
                    $("#panel_princ").prepend(LOADING);
                    $.get(ruta, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        //$("#modal-serv-fly").modal("hide");
                        if (response.status == 1) {
                            $("#searchData").click();
                        }

                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesar')}}"
                        });
                    });
                }
            });
        }

        function aprobarVuelo(ruta) {
            Swal.fire({
                title: 'Esta Seguro que Desea Aprobar el Vuelo para Su Facturación?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No',

            }).then((result) => {
                if (result.isConfirmed) {
                    $("#panel_princ").prepend(LOADING);
                    $.get(ruta, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        //$("#modal-serv-fly").modal("hide");
                        if (response.status == 1) {
                            $("#searchData").click();
                        }

                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesar')}}"
                        });
                    });
                }
            });
        }



        function seeDetalleVuelo(route) {
            RouteDetail = route;
            $("#modal-serv-fly").modal('show');
            $("#modal-serv-fly").prepend(LOADING);

            $.get(route, function (response) {
                //console.log(response);
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $(".overlay-wrapper").remove();
                    $(".filaFactura").remove();
                    sum = 0;
                    base_imponible = 0;
                    excento = 0;
                    iva = 0;
                    for (i in response.data.get_detalle) {
                        cant = response.data.get_detalle[i]['cant'];

                        tabla = '<tr style="cursor:pointer" data-toggle="tooltip" title="' + response.data.get_detalle[i]['formula2'] + '" class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (parseInt(i, 10) + 1) + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><span onclick="deleteItem(\'' + response.data.get_detalle[i]['crypt_id'] + '\')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></td>';

                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + muestraFloat(cant) + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data.get_detalle[i]['descripcion'] + '</td>';

                        tabla += '</tr>';
                        $("#datFactura").append(tabla);
                    }

                }
                //$('[data-toggle="tooltip"]').tooltip();
            }).fail(function () {
                $(".overlay-wrapper").remove();

            });
        }
        function deleteItem(id) {
            Swal.fire({
                title: 'Esta Seguro que Desea Eliminar el Servicio?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '<i class="fa fa-save"></i> Si',
                cancelButtonText: '<li class="fa fa-undo"></li> No',

            }).then((result) => {
                if (result.isConfirmed) {
                    $("#modal-serv-fly").prepend(LOADING);
                    $.get("{{url('delete-item')}}/" + id, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            seeDetalleVuelo(RouteDetail);
                        }

                        //$("#modal-serv-fly").modal("hide");

                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Procesarlo')}}"
                        });
                    });
                }
            });
        }


    </script>

</div>
