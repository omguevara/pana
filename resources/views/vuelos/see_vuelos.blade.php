<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Ver Vuelo')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{$routeBack}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->


        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-8 offset-sm-2  ">
                    <div class="row">




                        <div class="card card-primary card-tabs">
                            <div class="card-header p-0 pt-1">
                                <ul class="nav nav-tabs"  role="tablist">
                                    <li class="nav-item" id="liIdVuelo">
                                        <a class="nav-link active" data-toggle="pill" id="idVuelo" href="#tabVuelo" role="tab" aria-controls="tabVuelo" aria-selected="true">Vuelo</a>
                                    </li>
                                    <li class="nav-item" id="liIdResponsable">
                                        <a class="nav-link" data-toggle="pill" id="idAeronave" href="#tabResponsable" role="tab" aria-controls="tabResponsable" aria-selected="false">Responsable</a>
                                    </li>
                                    <li class="nav-item" id="liIdServ">
                                        <a class="nav-link" data-toggle="pill" id="idServ" href="#tabServ" role="tab" aria-controls="tabServ" aria-selected="false">Servicios</a>
                                    </li>
                                    <li class="nav-item" id="liServ">
                                        <a class="nav-link" data-toggle="pill" id="idPax" href="#tabPax" role="tab" aria-controls="tabPax" aria-selected="false">Pasajeros</a>
                                    </li>

                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" >
                                    <div class="tab-pane  active show" id="tabVuelo" role="tabpanel" >
                                        <h2>Datos del Vuelo</h2>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Tipo de Operación:</b></span>
                                            </div>
                                            {{Form::text('tipo_operacion', $Vuelo->tipo_operacion  , ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Tipo de Vuelo:</b></span>
                                            </div>
                                            {{Form::text('tipo_vuelo',  $Vuelo->tipo_vuelo , ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                            <div class="input-group-prepend">
                                                <span id="labelFecha" class="input-group-text"><b>Fecha de Operaci&oacute;n:</b></span>
                                            </div>
                                            {{Form::text('fecha_operacion', $Vuelo->fecha_operacion2, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="" class="input-group-text"><b>Hora de Operaci&oacute;n:</b></span>
                                            </div>
                                            {{Form::text('hora_operacion', $Vuelo->hora_operacion, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Aeronave:</b></span>
                                            </div>
                                            {{Form::text('aeronave_id',  $Vuelo->aeronave->full_nombre_tn , ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Piloto:</b></span>
                                            </div>
                                            {{Form::text('piloto_id',  $Vuelo->piloto->full_name , ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        
                                        @if ($Vuelo->tipo_operacion_id==1)
                                            <div class="input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Aeropuerto Origen:</b></span>
                                                </div>
                                                {{Form::text('origen_destino', $Vuelo->origen_destino->full_nombre, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                            </div>
                                            <div class="input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Aeropuerto Destino:</b></span>
                                                </div>
                                                {{Form::text('aeropuerto_id', $Vuelo->aeropuerto->full_nombre, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                            </div>
                                            <div class="input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Pasajeros Desembarcados:</b></span>
                                                </div>
                                                {{Form::text('pax', $Vuelo->pasajeros_embarcados>0 ? $Vuelo->pasajeros_embarcados:$Vuelo->pasajeros_desembarcados, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                            </div>
                                        @else
                                            <div class="input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Aeropuerto Origen:</b></span>
                                                </div>
                                                {{Form::text('aeropuerto_id', $Vuelo->aeropuerto->full_nombre, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                            </div>
                                            <div class="input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Aeropuerto Destino:</b></span>
                                                </div>
                                                {{Form::text('origen_destino', $Vuelo->origen_destino->full_nombre, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                            </div>
                                            <div class="input-group mt-2">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><b>Pasajeros Embarcados:</b></span>
                                                </div>
                                                {{Form::text('pax', $Vuelo->pasajeros_embarcados>0 ? $Vuelo->pasajeros_embarcados:$Vuelo->pasajeros_desembarcados, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                            </div>
                                        @endif
                                        
                                        <div class="input-group mt-2 ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Observación:</b></span>
                                            </div>
                                            {{Form::textarea('observaciones',  $Vuelo->observaciones , ["rows"=>"4", "class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Vuelo Registrado Por:</b></span>
                                            </div>
                                            {{Form::text('usuario', $Vuelo->usuario_create->full_nombre, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Vuelo Modificado Por:</b></span>
                                            </div>
                                            {{Form::text('usuario', $Vuelo->usuario_edit->full_nombre, ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="tab-pane  " id="tabResponsable" role="tabpanel" >
                                        <h2>Responsable de la Aeronave</h2>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Rif:</b></span>
                                            </div>
                                            {{Form::text('rif', $Vuelo->observaciones_facturacion2[0].$Vuelo->observaciones_facturacion2[1], ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>

                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Responsable:</b></span>
                                            </div>
                                            {{Form::text('responsable', $Vuelo->observaciones_facturacion2[2], ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Teléfono:</b></span>
                                            </div>
                                            {{Form::text('cel', $Vuelo->observaciones_facturacion2[3], ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Dirección:</b></span>
                                            </div>
                                            {{Form::text('dir', $Vuelo->observaciones_facturacion2[4], ["class"=>"form-control ", "disabled"=>"disabled"])}}
                                        </div>
                                    </div>
                                    <div class="tab-pane  " id="tabServ" role="tabpanel" >
                                        <h2>Servicios Aplicados</h2>
                                        <table style="width:100%" >

                                            <tr>
                                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">N&deg;</td>
                                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                                <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                                            </tr>
                                            @php
                                            $estacionamiento = false;
                                            @endphp
                                            @foreach($Vuelo->getDetalle as $key=>$value)
                                            @if (Str::contains($value->full_descripcion, '2.2'))
                                             @php
                                                $estacionamiento = true;
                                                @endphp
                                            @endif
                                            <tr>
                                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$key+1}}</td>
                                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{muestraFloat($value->cant, 2).' '.$value->nomenclatura }}</td>
                                                <td style="width:80%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$value->full_descripcion}}</td>

                                            </tr>
                                            @endforeach
                                        </table>
                                        @if ($estacionamiento==true)
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Hora de Llegada a Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                            </div>
                                            {{Form::text("hora_llegada", date('d/m/Y H:i',strtotime($Vuelo->hora_llegada)), ["class"=>"form-control ", "disabled"=>"disabled"])}}

                                        </div>
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><b>Hora de Salida de Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                            </div>
                                            {{Form::text("hora_salida", date('d/m/Y H:i',strtotime($Vuelo->hora_salida)), ["class"=>"form-control ", "disabled"=>"disabled"])}}

                                        </div>
                                        @endif
                                    </div>
                                    <div class="tab-pane  " id="tabPax" role="tabpanel" >
                                        
                                        <h2>Pasajeros</h2>
                                        <table style="width:100%" >

                                            <tr>
                                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">N&deg;</td>
                                                
                                                <td style="width:90%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                                            </tr>
                                            @foreach($Vuelo->pasajeros as $key=>$value)
                                            <tr>
                                                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$key+1}}</td>
                                                
                                                <td style="width:80%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$value->pasajero}}</td>

                                            </tr>
                                            @endforeach
                                        </table>
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        <div class="card-footer text-center">
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {});
    </script>

</div>

