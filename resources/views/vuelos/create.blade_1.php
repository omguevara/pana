



<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registro de Vuelo')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"vuelos",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::select('prod_servd[]', [], "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}
        

        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                        </div>
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>
                        
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo de Operaci&oacute;n:</b></span>
                            </div>
                            {{Form::select('tipo_operacion_id', ["1"=>"LLEGADA", "2"=>"SALIDA"], "", [  "data-msg-required"=>"Campo Requerido",  "placeholder"=>"Seleccione" ,"class"=>"form-control   ", "id"=>"tipo_operacion_id" ,"required"=>"required"])}}



                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo de Vuelo:</b></span>
                            </div>
                            {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], 1, ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                        </div>

                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span id="labelFecha" class="input-group-text"><b>Fecha de Operaci&oacute;n:</b></span>
                            </div>
                            {{Form::text("fecha_operacion", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>

                        </div>
                        <div class="input-group mt-2"  >




                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora de Operaci&oacute;n (24H):</b></span>
                            </div>
                            {{Form::text("hora_operacion", "", ["data-msg-required"=>"Campo Requerido", "class"=>"form-control timer required ", "id"=>"hora_operacion" ,"required"=>"required"  ])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", "", ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number  ", "id"=>"cant_pasajeros", "placeholder"=>__('Cant. PAX')])}}    
                        </div>






                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, "", [ "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div  title="Agregar Nuevo Piloto" onClick="$('#modal-princ-pilotos').modal('show')" class="input-group-text">



                                    <i class="fa fa-plus"></i>

                                </div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span id="labelOrigenLlegada" class="input-group-text"><b>Destino/Origen:</b></span>
                            </div>
                            {{Form::select('origen_destino_id', $OrigenDestino, "" , ["onChange"=>"checkValue(this)", "data-msg-required"=>"Campo Requerido",  "placeholder"=>"Seleccione"   ,"class"=>"form-control required  select2 ", "id"=>"origen_destino_id" ,"required"=>"required"])}}

                        </div>
                        
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observación:</b></span>
                            </div>
                            {{Form::text("observacion", "", ["class"=>"form-control ", "id"=>"observacion", "placeholder"=>__('Observación')])}}    
                        </div>














                    </div>  
                    <div class="row  mt-2 ">
                        <div   class="col-sm-12 col-md-4  text-center   " style="display:none">
                            {{Form::button('<li class="fa fa-home"></li> '.__("PUESTO").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addPuesto"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-3  text-center  " style="display:none">
                            {{Form::button('<li class="fa fa-user"></li> '.__("PAX").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addPax"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-6  text-center  ">
                            {{Form::button('<li class="fa fa-plus"></li> '.__("AGREGAR SERV.").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addServ", "onClick"=>"setServ()"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-6 text-center  ">
                            {{Form::button('<li class="fa fa-save"></li> '.__("GUARDAR").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"saveData()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"save"])}}    
                        </div>
                    </div>

                </div>  
                <div   class="col-sm-12 col-md-6   ">





                </div>


            </div>  

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>




    <div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ "route"=>"aeronaves_plus", 'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6" >
                            <div class="form-group">
                                <label for="matricula">{{__('Matrícula')}}</label>
                                {{Form::text("matricula", "", ["required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="nombre">{{__('Modelo')}}</label>
                                {{Form::text("nombre", "", ["required"=>"required", "maxlength"=>"20" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                                {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nuevo Piloto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ 'route'=>'pilotos_plus',  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                <div id="modalPrincBodyFact" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="type_document">{{__('Tipo de Documento')}}</label>
                                {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>
                                {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="name_user">{{__('Name')}}</label>
                                {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="surname_user">{{__('Surname')}}</label>
                                {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                            </div>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-princ-serv-extra"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalExt" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Agregar Servicios Extra</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ "route"=>"get_servs_ext",  'id'=>'frmServ','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-12" >

                            <table style="width:100%" id="tableServExt">
                                <tr>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                    <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                                </tr>

                            </table>

                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicar"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">

        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            
            $('#iconDate').datetimepicker({
                format: 'L',
            });
            
            $("#tipo_operacion_id").on("click", function (){
                if (this.value==''){
                    $("#labelOrigenLlegada").html('<b>Destino/Origen:</b>');  
                    $("#labelFecha").html('<b>Fecha de Operación:</b>');
                }else{
                    if (this.value==1){
                        $("#labelOrigenLlegada").html('<b>Origen:</b>');  
                        $("#labelFecha").html('<b>Fecha de LLegada:</b>');
                    }else{
                        $("#labelOrigenLlegada").html('<b>Destino:</b>');  
                        $("#labelFecha").html('<b>Fecha de Salida:</b>');
                    }
                }
            });
            
            
            $('#fecha_operacion').on('input', function(e){ console.log(this.value) })
            
            
            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});




            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});



            $('.number').numeric({negative: false});


            $("#aplicar").on("click", function () {
                $("#modal-princ-serv-extra").modal("hide");
                $('#next1').click();
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmPilotos').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    $("#frmNave").prepend(LOADING);
                    $.post(this.action, $("#frmNave").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmNave").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#aeronave_id").append('<option  value="' + response.data.crypt_id + '">' + response.data.full_nombre + '</option>');
                            $("#aeronave_id").val(response.data.crypt_id);
                            $("#aeronave_id").trigger("reset");
                            $('#frmNave').trigger("reset");
                            $("#modal-princ-aeronave").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar"
                        });
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPilotos').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPilotos').valid()) {
                    $("#frmPilotos").prepend(LOADING);
                    $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmPilotos").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#piloto_id").append('<option value="' + response.data.crypt_id + '">' + response.data.full_name + '</option>');
                            $("#piloto_id").val(response.data.crypt_id);
                            $("#piloto_id").trigger("reset");
                            $('#frmPilotos').trigger("reset");
                            $("#modal-princ-pilotos").modal('hide');
                            
                            
                        }
                        //console.log(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Guardar"
                        });
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });



        });

        function checkValue(obj) {

            if (obj.value == $("#aeropuerto_id").val() && obj.value != '') {
                obj.value = "";
               // $(obj).trigger("reset");
                $(obj).trigger("change.select2");
                Toast.fire({
                    icon: "error",
                    title: "El Aeropuerto no debe ser el Mismo del Origen"
                });
            }

            if ($("#aeropuerto_id").val() == "" && obj.value!="") {
                obj.value = "";
                $(obj).trigger("change.select2");
                Toast.fire({
                    icon: "error",
                    title: "Debe Seleccionar el Aeropuerto de Origen"
                });
            }
        }

        function saveData() {
            if ($("#frmPrinc1").valid()) {
                Swal.fire({
                    title: 'Esta Seguro que Desea Guardar el Vuelo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-check"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li> No'
                }).then((result) => {
                    if (result.isConfirmed) {

                        

                        $("#frmPrinc1").prepend(LOADING);
                        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                            $(".overlay-wrapper").remove();


                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $(".overlay-wrapper").remove();

                            if (response.status == 1) {

                                $("#frmPrinc1").trigger("reset");
                                $("#prod_servd").html();
                                $("#piloto_id, #origen_destino_id, #aeronave_id, #aeropuerto_id").trigger("change.select2");
                                $("#fecha_operacion").val("");
                                

                            } else {

                            }





                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });
            }
        }
        function setServ() {
            if ($("#frmPrinc1").valid()) {
                $('#modal-princ-serv-extra').modal('show');
                $("#frmServ").prepend(LOADING);

                $.post($("#frmServ").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    $(".overlay-wrapper").remove();
                    $(".filaServExt").remove();

                    prods = [];
                    cantidades = [];

                    $($("#prod_servd").serializeArray()).each(function () {
                        detalleServs = this.value.split(":");
                        prods.push(detalleServs[1]);
                        cantidades[detalleServs[1]] = detalleServs[0];


                    });

                    for (i in response.data) {

                        checked = $.inArray(response.data[i]['crypt_id'], prods) == -1? false:true;


                        tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input '+(checked==true? 'checked':'')+' type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data[i]['crypt_id'] + '" /></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input type="number" id="' + response.data[i]['crypt_id'] + '" onChange="UpItemSelect()" value="'+(checked==true? cantidades[response.data[i]['crypt_id']]:'0')+'" class="form-control" '+(checked==true? '':'disabled')+' /></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + '</td>';


                        tabla += '</tr>';
                        $("#tableServExt").append(tabla);
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "Error al Consultar los Servicios"
                    });
                });
            }


        }
        function selectItem(obj) {
            id = $(obj).data("crypt_id");
            if (obj.checked == true) {
                $("#" + id).val("1").prop("disabled", false).focus();
            } else {
                $("#" + id).val("0").prop("disabled", true);
            }
            UpItemSelect();
        }
        function UpItemSelect() {
            $("#prod_servd").html("");
            $(".selectItem:checked").each(function () {
                Id = $(this).data('crypt_id');
                $("#prod_servd").append('<option selected value="' + $("#" + Id).val() + ':' + Id + '"></option>');
            });
        }
    </script>

</div>