
<div id="panel_princ" class="col-sm-12  mt-1">
    <style>
        .dataTables_wrapper{
            width:100%;
        }
        .paginate_button{
            padding: 0px !important;
        }
        .error_extra{
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #dc3545;
        }

        .tooltip-inner {
            background-color: #419bde;
        }
        .tooltip.bs-tooltip-right .arrow:before {
            border-right-color: #419bde !important;
        }
        .tooltip.bs-tooltip-left .arrow:before {
            border-right-color: #419bde !important;
        }
        .tooltip.bs-tooltip-bottom .arrow:before {
            border-right-color: #419bde !important;
        }
        .tooltip.bs-tooltip-top .arrow:before {
            border-right-color: #419bde !important;
        }


        tr.listServFact:hover{
            background-color: #ffe69b;
        }

        .circulo {
            border-radius: 50%;
            font-size: 16px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            width: 30px;
            padding: unset;
        }

    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Operaciones')}}  {{($Aeropuertos->count()==1 ? ": ".$Aeropuertos->first():'')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        



        <div id="card-body-main" class="card-body ">
            <div class="row">
                <table id="example1" class="display compact table-hover">
                    <thead>
                        <tr>
                            <th style="width: 50px; text-align: center">{{__('Actions')}}</th>
                            <th style="width: 80px; text-align: center">{{__('FECHA OPERACIÓN')}}</th>
                            <th style="width: 100px; text-align: center">{{__('TIPO OPERACIÓN')}}</th>
                            <th style="width: 100px; text-align: center">{{__('TIPO AVIACIÓN')}}</th>
                            <th style="width: 100px; text-align: center">{{__('TIPO VUELO')}}</th>
                            <th>{{__('AERONAVE')}}</th>
                            <th>{{__('PILOTO')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>



            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        
    </div>
    @include('vuelos.modals.vuelo')
    @include('vuelos.modals.complementos')

    

    

    <script type="text/javascript">
       
                
        $(document).ready(function () {
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('.number').numeric({negative: false});
            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
            $(".matricula").alphanum({
                        allowNumeric: true,
                        allowUpper: true,
                        allowLower: true,
                        allowSpace: false,
                        allowOtherCharSets: false
                    });
            
            $('.switch').bootstrapSwitch();

            $('.tips').tooltip({"html": true, "template": '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'});
            $('#iconDate').datetimepicker({
                format: 'L',
                maxDate: moment(),
                minDate: moment().add(-9, 'days')
            });
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM", "clearIncomplete": true});
            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM", "clearIncomplete": true});

            $("#estacionamiento").on("switchChange.bootstrapSwitch", function (event, state) {
                //console.log(this); // DOM element
                //console.log(event); // jQuery event
                //console.log(state); // true | false

                //$(".date_time").val(  $("#fecha_operacion").val()+' '+$("#hora_operacion").val()  )
                //console.log(state)
                /*
                HORA = moment($("#fecha_operacion").val() + ' ' + $("#hora_operacion").val(), "DD/MM/YYYY hh:mm");
                $("#hora_llegada").val(HORA.subtract(5, 'minutes').format('DD/MM/YYYY HH:mm'))
                $("#hora_salida").val(HORA.add(10, 'minutes').format('DD/MM/YYYY HH:mm'));
                */
                //$(".date_time").val(  $("#fecha_operacion").val()+' '+$("#hora_operacion").val()  )
                $(".date_time")
                        .prop({"readonly": !state, "required": state})
                        .eq(0).focus()
                        ;


            });
            
            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    if (usaFloat($("#peso_maximo").val()) > 0) {
                        $("#frmNave").prepend(LOADING);
                        $.post(this.action, $("#frmNave").serialize(), function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $("#frmNave").find(".overlay-wrapper").remove();
                            if (response.status == 1) {
                                DataTable1.row.add({
                                    "selection": '<buttom onclick="selectNave(\''+response.data.crypt_id+'\', \''+response.data.full_nombre_tn+'\')" class="btn  bg-success"><i class="fas fa-check"></i></buttom>',
                                    "full_nombre_tn": response.data.full_nombre_tn

                                }).draw();
                                selectNave(response.data.crypt_id, response.data.full_nombre_tn);
                            }
                            //console.log(response);
                        }).fail(function () {
                            Toast.fire({
                                icon: "error",
                                title: "Error al Guardar"
                            });
                            $(".overlay-wrapper").remove();
                        });
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "Debe Ingresar las Toneladas de la Aeronave"
                        });
                        $("#peso_maximo").focus();
                    }
                }
                return false;
            });
            
            
            $('#frmPilotos').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            $('#frmCompResp').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            $('#frmCompOrigDest1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            $('#frmCompOrigDest2').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            

            $('#frmPilotos').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPilotos').valid()) {
                    $("#frmPilotos").prepend(LOADING);
                    $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmPilotos").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            DataTable2.row.add({
                                    "selection": '<buttom onclick="selectPilot(\''+response.data.crypt_id+'\', \''+response.data.full_name+'\')" class="btn  bg-success"><i class="fas fa-check"></i></buttom>',
                                    "full_name": response.data.full_name
                            }).draw();
                                
                            
                            
                            selectPilot(response.data.crypt_id, response.data.full_name)
                        }
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Guardar el Piloto"
                        });
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });
            
            $("#tabCompl").on('shown.bs.tab', function (e) {
                //ref = e.target.href.split("#")[1];
                //console.log(ref);
                $("#orig_dest, #pais_id").select2({dropdownParent: $('#modal-princ-comp') });
            });
            
           
            
            
            
            
            table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                //data: data,
                
                serverSide: true,
                processing: true,
                
                ajax: "{{route('vuelos.get_vuelos_hoy')}}",
                
                dom: 'Bfrtip',
                buttons: {
                    buttons: [
                        {
                            text: '<li class="fa fa-plane"></li> REGISTRAR OPERACIÓN',
                            action: function (e, dt, node, config) {
                                clearFrom();
                                $("#modal-vuelo").modal('show');
                            }
                        }
                    ],
                    dom: {
                        button: {className: "btn btn-info"},

                    }
                },
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'action'},
                    {data: 'fecha_operacion_full'},
                    {data: 'tipo_operacion'},
                    {data: 'tipo_aviacion'},
                    {data: 'tipo_vuelo'},

                    {data: 'aeronave.full_nombre_tn'},
                    {data: 'piloto.full_name'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }


            });
            
            DataTable1 = $('#listAeronaves').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": true,
                        "ordering": true,
                        "info": false,
                        "autoWidth": false,
                        "responsive": true,
                        "pageLength": 5,
                        ajax: {
                            type:"post",
                            url:"{{route('get_aeronaves')}}",
                            data: function (d) {
                                d.search = $("#listDataTable").find('input[type="search"]').val(),
                                d._token = "{{ csrf_token() }}"

                            }
                        },
                        serverSide: true,
                        processing: true,
                        
                        
                        
                        columns: [
                            {data: 'selection'},
                            {data: 'full_nombre_tn'}
                        ],
                        language: {
                            url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                        }


                    });
            
            DataTable2 = $('#listPilotos').DataTable({
                        "paging": true,
                        "lengthChange": false,
                        "searching": true,
                        "ordering": true,
                        "info": false,
                        "autoWidth": false,
                        "responsive": true,
                        "pageLength": 5,


                        ajax: {
                            type:"post",
                            url:"{{route('get_pilotos')}}",
                            data: function (d) {
                                d.search = $("#listDataTablePilotos").find('input[type="search"]').val(),
                                d._token = "{{ csrf_token() }}"

                            }
                        },
                        serverSide: true,
                        processing: true,

                        columns: [
                            {data: 'selection'},
                            {data: 'full_name'}
                        ],
                        language: {
                            url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                        }


                    });
            
            $("#modal-vuelo").on('shown.bs.modal', function () {
                $("#aplicaEstCheck ").css("min-width", "calc(98% - " + $("#aplicaEst").width() + "px)");
            });
            
            $("#aplicar").on("click", function () {
                if ($("#frmServ").valid()) {
                    seeServ();
                }
            });
            
            $("#selectSearchItem").on("input", function (){
                $(".filaServExt").each(function (){
                     item = $(this).children().eq(2).text().toUpperCase();
                     if (item.indexOf( $("#selectSearchItem").val().toUpperCase()) >= 0){
                         $(this).show();
                     }else{
                         $(this).hide();
                     }
                 });
            });
            
            $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
                content = $(e.target).attr('aria-controls');
                $("#" + content).find("input[type=text], input[type=search]").eq(0).focus();
                /*
                e.target // newly activated tab
                e.relatedTarget // previous active tab
                */
            })
            
          
        });

        function salida() {
            if ($("#tipo_id").val() != 2) {
                $("#tipo_id").val(2);
                $("#labelCantPax").html("<b>Pasajeros Embarcados:</b>");
                $("#labelOrigDest").html("<b>Aeropuerto Destino:</b>");
                
                /*
                 $("#btn_llegada").slideUp('fast', function () {
                 //$("#btn_salida").addClass("col-md-12").removeClass("col-md-6");
                 $("#btn_salida").animate({"min-width": "100%"}, 'fast', function () {
                 $("#arrivo, #despegue").prop("disabled", true);
                 $("#panel_nacionalidad").slideDown("fast");
                 
                 });
                 });
                 */
                $("#contentTipo").removeClass("info-pulse");
                $("#arrivo").html('<i class="fas fa-plane-arrival"></i>').addClass('btn-outline-primary').removeClass('btn-primary');
                $("#btn_llegada").removeAttr("style");
                $("#btn_llegada").animate({"max-width": "20%"}, 'fast', function () {
                    $("#btn_salida").animate({"min-width": "80%"}, 'fast', function () {
                        $("#despegue").html('<i class="fas fa-plane-departure"></i> Salida').addClass('btn-primary').removeClass('btn-outline-primary');
                        //$("#panel_nacionalidad").slideDown("fast");

                    });
                });
            }




        }
        function llegada() {
            if ($("#tipo_id").val() != 1) {
                $("#tipo_id").val(1);
                $("#labelCantPax").html("<b>Pasajeros Desembarcados:</b>");
                
                $("#labelOrigDest").html("<b>Aeropuerto Origen:</b>");
                /*
                 $("#btn_salida").slideUp('fast', function () {
                 //$("#btn_llegada").addClass("col-md-12").removeClass("col-md-6");
                 $("#btn_llegada").animate({"min-width": "100%"}, 'fast', function () {
                 $("#arrivo, #despegue").prop("disabled", true);
                 $("#panel_nacionalidad").slideDown("fast");
                 
                 });
                 });
                 */
                $("#contentTipo").removeClass("info-pulse");
                $("#btn_salida").removeAttr("style");
                $("#despegue").html('<i class="fas fa-plane-departure"></i> ').addClass('btn-outline-primary').removeClass('btn-primary');

                $("#btn_salida").animate({"max-width": "20%"}, 'fast', function () {
                    $("#btn_llegada").animate({"min-width": "80%"}, 'fast', function () {
                        $("#arrivo").html('<i class="fas fa-plane-arrival"></i> Llegada').addClass('btn-primary').removeClass('btn-outline-primary');
                        ;
                        //$("#panel_nacionalidad").slideDown("fast");

                    });
                });
            }
        }
        function nacional() {
            if ($("#nacionalidad_id").val() != 1) {
                $("#nacionalidad_id").val(1);
                $("#contentNacionalidad").removeClass("info-pulse");
                $("#btn_internacional").removeAttr("style");
                $("#vuelo_internacional").html('<i class="fas fa-globe"></i> ').addClass('btn-outline-primary').removeClass('btn-primary');

                $("#btn_internacional").animate({"max-width": "20%"}, 'fast', function () {
                    $("#btn_nacional").animate({"min-width": "80%"}, 'fast', function () {
                        $("#vuelo_nacional").html('<i class="fas fa-flag"></i> Nacional').addClass('btn-primary').removeClass('btn-outline-primary');
                        ;
                        //$("#panel_data").slideDown("fast");

                    });
                });
                $("#orig_dest_id").children().show();
                $("#orig_dest_id").children('[data-pais!=1]').not('[value=""]').hide();
                $("#orig_dest_id").trigger('change');
                
                    
                
                
                

                /*$("#btn_internacional").slideUp('fast', function () {
                 //$("#btn_salida").addClass("col-md-12").removeClass("col-md-6");
                 $("#btn_nacional").animate({"min-width": "100%"}, 'fast', function () {
                 $("#vuelo_internacional, #vuelo_nacional").prop("disabled", true);
                 $("#panel_data").slideDown("fast");
                 
                 
                 });
                 });
                 */
            }
        }
        function internacional() {
            if ($("#nacionalidad_id").val() != 2) {
                $("#nacionalidad_id").val(2);
                $("#contentNacionalidad").removeClass("info-pulse");
                $("#btn_nacional").removeAttr("style");
                $("#vuelo_nacional").html('<i class="fas fa-flag"></i> ').addClass('btn-outline-primary').removeClass('btn-primary');

                $("#btn_nacional").animate({"max-width": "20%"}, 'fast', function () {
                    $("#btn_internacional").animate({"min-width": "80%"}, 'fast', function () {
                        $("#vuelo_internacional").html('<i class="fas fa-globe"></i> Internacional').addClass('btn-primary').removeClass('btn-outline-primary');
                        ;
                        //$("#panel_data").slideDown("fast");
                    });
                });
                $("#orig_dest_id").children().show();
                $("#orig_dest_id").children('[data-pais=1]').not('[value=""]').hide();
                $("#orig_dest_id").trigger('change');
                
                
                
                /*
                 $("#btn_nacional").slideUp('fast', function () {
                 //$("#btn_salida").addClass("col-md-12").removeClass("col-md-6");
                 $("#btn_internacional").animate({"min-width": "100%"}, 'fast', function () {
                 $("#vuelo_internacional, #vuelo_nacional").prop("disabled", true);
                 $("#panel_data").slideDown("fast");
                 
                 });
                 });
                 */
            }
        }
        
        
        function selectNave(id, nave) {
            $("#aeronave_id").val(id);
            $("#contentAeronave").removeClass("info-pulse");
            //$("#getSelectAeronave").prop("disabled", true);
            $("#getSelectAeronave").removeClass('btn-outline-primary').addClass('btn-primary').html('<i class="fas fa-plane"></i> ' + nave);
            $("#modal-list-aeronaves").modal('hide');
            getDataNave(id);
        }
        function getDataNave(nave) {
            $("#text-info").slideUp("fast");
            $("#info, #liIdInformacion").removeClass("info-pulse");
            $("#modalCreateFly").prepend(LOADING);
            $.get("{{url('get-aeronave')}}/" + nave , function (response) {
                $(".overlay-wrapper").remove();
                if (   $("#tipo_id").val()==2  ){ // If IS ARRIVAL
                    if (   $("#aeropuerto_id").length>0   ){
                        if ($("#aeropuerto_id").val()== response.data.last_arrival.aeropuerto.crypt_id  ){
                            $("#hora_llegada").val(response.data.last_arrival.fecha_operacion2+' '+response.data.last_arrival.hora_operacion );
                        }
                    }else{
                        @if (Auth::user()->aeropuerto_id!=null)
                        if ( '{{Auth::user()->aeropuerto->crypt_id}}' ==response.data.last_arrival.aeropuerto.crypt_id){
                             $("#hora_llegada").val(response.data.last_arrival.fecha_operacion2+' '+response.data.last_arrival.hora_operacion );
                        }
                        @endif
                    }
                }
                if (response.data.membresia.length > 0) {
                    msg = "{{__('La Aeronave Posee Membresía')}}<br>";
                    for (i in response.data.membresia) {
                        //membresia_observacion = response.data.membresia[i]['observacion'] || '';
                        msg += "Fecha Desde: " + response.data.membresia[i]['fecha_desde2'] + " , Fecha Hasta: " + response.data.membresia[i]['fecha_hasta2'] + "<br>";
                        msg += "Empresa: " + response.data.membresia[i]['empresa'] + "<br>";
                        msg += "Tipo: " + response.data.membresia[i]['tipo'] + "<br>";
                        msg += "Exonerar: " + response.data.membresia[i]['exonerar2'] + "<br>";
                        //msg += "{{__('Información')}}: " + membresia_observacion.replace(/\r\n/g, "<br/>") + "<br/>";
                    }
                    $("#text-info").html(msg);
                    $("#text-info").slideDown("fast", function (){
                        $("#info").addClass("info-pulse");
                        $("#liIdInformacion").addClass("info-pulse");
                    });
                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
            });
        }
        
        function selectPilot(id, piloto) {
            $("#piloto_id").val(id);
            $("#contentPiloto").removeClass("info-pulse");
            //$("#getSelectPilot").prop("disabled", true);
            $("#getSelectPilot").removeClass('btn-outline-primary').addClass('btn-primary').html('<i class="fas fa-user"></i> ' + piloto);
            $("#modal-princ-pilotos").modal('hide');
            $("#pax").focus();

        }
        
        function checkForm(){
            if ($("#tipo_id").val()==''){
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Indicar si el Vuelo es de LLegada o Salida')}}"
                });
                $("#contentTipo").addClass("info-pulse");
                $("#contentTipo")[0].scrollIntoView();
                return false;    
            }else{
                $("#contentTipo").removeClass("info-pulse");
            }
            if ($("#nacionalidad_id").val()==''){
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Indicar si el Vuelo es Nacional o Internacional')}}"
                });
                $("#contentNacionalidad").addClass("info-pulse");
                $("#contentNacionalidad")[0].scrollIntoView();
                return false;    
            }else{
                $("#contentNacionalidad").removeClass("info-pulse");
            }

            if ($("#aeronave_id").val()==''){
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Seleccionar la Aeronave')}}"
                });
                $("#contentAeronave").addClass("info-pulse");
                $("#contentAeronave")[0].scrollIntoView();
                return false;    
            }else{
                $("#contentAeronave").removeClass("info-pulse");
            }
            if ($("#piloto_id").val()==''){
                Toast.fire({
                    icon: "error",
                    title: "{{__('Debe Seleccionar Al Piloto')}}"
                });
                $("#contentPiloto").addClass("info-pulse");
                $("#contentPiloto")[0].scrollIntoView();
                return false;    
            }else{
                $("#contentPiloto").removeClass("info-pulse");
            }
            
            if ($("#estacionamiento").is(":checked")){
                f  = moment($("#fecha_operacion").val()+' '+$("#hora_operacion").val(), "DD/MM/YYYY hh:mm");
                fi = moment($("#hora_llegada").val(), "DD/MM/YYYY hh:mm");
                ff = moment($("#hora_salida").val(), "DD/MM/YYYY hh:mm");
                if (fi > ff) {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('La Hora de Llegada no Puede Ser Mayor a la Hora de Salida')}}"
                    });
                    return false;
                }
                if(! (f >= fi && f <= ff)) {
                //if (! f.isBetween(fi, ff)) {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('La Fecha de Vuelo No Esta en el Intervalo de Hora de Llegada y Hora de Salida')}}"
                    });
                    return false;
                }
            }
            
            return true;
        }
        
        function setServ() {
            $(".filaServExt").remove();
            if ($("#tipo_aviacion_id").val()==1){//SI ES AVIACION GENERAL
                if ($("#frmPrinc1").valid()) {
                    if (checkForm()==true){
                        $(".filaServExt").remove();
                        $("#modalCreateFly").prepend(LOADING);

                        $.post($("#frmServ").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                            $(".overlay-wrapper").remove();


                            if (response.status == 1) {
                                prods = [];
                                cantidades = [];
                                $($("#prod_servd").serializeArray()).each(function () {
                                    detalleServs = this.value.split(":");
                                    prods.push(detalleServs[1]);
                                    cantidades[detalleServs[1]] = detalleServs[0];
                                });

                                for (i in response.data) {
                                    checked = $.inArray(response.data[i]['crypt_id'], prods) == -1 ? false : true;

                                    tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input ' + (checked == true ? 'checked' : '') + ' type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data[i]['crypt_id'] + '" /></td>';
                                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input min="1" type="number" id="' + response.data[i]['crypt_id'] + '" onChange="UpItemSelect()" value="' + (checked == true ? cantidades[response.data[i]['crypt_id']] : '0') + '" required class="form-control required" ' + (checked == true ? '' : 'disabled') + ' /></td>';
                                    tabla += '<td colspan="2"  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "> ' + response.data[i]['full_descripcion'] + '</td>';


                                    tabla += '</tr>';
                                    $("#tableServExt").append(tabla);
                                }
                            } else {
                                Toast.fire({
                                    icon: response.status,
                                    title: response.message
                                });
                            }
                            $("#idServ").click();
                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "Error al Consultar los Servicios"
                            });
                        });
                    }
                }else{
                    $("#document")[0].scrollIntoView();
                    $("#document").focus();

                }
            }else{
                Toast.fire({
                    icon: "error",
                    title: "Servicio No Disponible para la Aviacion: "+$("#tipo_aviacion_id").children(":selected").text()
                });
            }
        }
        
        function selectItem(obj) {
            id = $(obj).data("crypt_id");
            if (obj.checked == true) {
                $("#" + id).val("1").prop("disabled", false).focus();
            } else {
                $("#" + id).val("0").prop("disabled", true);
            }
            UpItemSelect();
        }
        
        function UpItemSelect() {
            $("#prod_servd").html("");
            $(".selectItem:checked").each(function () {
                Id = $(this).data('crypt_id');
                $("#prod_servd").append('<option selected value="' + $("#" + Id).val() + ':' + Id + '"></option>');
            });
        }
        
        function saveData() {
            if ($("#frmPrinc1").valid()) {
                if (checkForm()==true){
                    Swal.fire({
                        title: 'Esta Seguro que Desea Guardar el Vuelo?',
                        html: '',
                        icon: 'question',
                        //customClass: 'swal-wide',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: '<li class="fa fa-check"></li> Si',
                        cancelButtonText: '<li class="fa fa-undo"></li> No'
                    }).then((result) => {
                        if (result.isConfirmed) {

                            $("#modalCreateFly").prepend(LOADING);
                            $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                                $(".overlay-wrapper").remove();
                                if (response.status == 1) {
                                    clearFrom();
                                    table1.draw();
                                    if (response.close_modal){
                                        $("#modal-vuelo").modal("hide");
                                    }
                                    /*
                                    $("#frmPrinc1").trigger("reset");
                                    $("#prod_servd").html();
                                    $("#info").slideUp("fast");
                                    $("#piloto_id, #aeronave_id, #aeropuerto_id").trigger("change.select2");
                                    $("#fecha_operacion").val("");
                                    */
                                } else {

                                }
                            }).fail(function () {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error al Guardar')}}"
                                });
                            });



                        }
                    });
                }
            }
        }
        
        function clearFrom(){
            $("#id").val("");
            
            d = new Date();
            
            
            $("#fecha_operacion").val("{{date('d/m/Y')}}");
            $("#hora_operacion").val(d.getHours()+":"+(d.getMinutes()<10?'0':'') + d.getMinutes());
            
            
            $("#btn_llegada, #btn_salida, #btn_nacional, #btn_internacional").removeAttr("style");
            $("#arrivo, #despegue, #vuelo_nacional, #vuelo_internacional, #getSelectAeronave, #getSelectPilot").removeClass("btn-primary")
                    .addClass("btn-outline-primary");
            
            $("#arrivo").html('<i class="fas fa-plane-arrival"></i> Llegada');
            $("#despegue").html('<i class="fas fa-plane-departure"></i> Salida');
            
            $("#vuelo_nacional").html('<i class="fas fa-flag"></i> Nacional');
            $("#vuelo_internacional").html('<i class="fas fa-globe"></i> Internacional');
            
            $("#getSelectAeronave").html('<i class="fas fa-plus"></i> <i class="fas fa-plane"></i> Seleccionar Aeronave');
            $("#getSelectPilot").html(' <i class="fas fa-plus"></i> <i class="fa fa-user"></i> Seleccionar Piloto');
            
            $("#tipo_id, #nacionalidad_id").val("");
            $("#tipo_aviacion_id").val("1");
            $("#pax").val("0");
            $("#aeropuerto_id").val("");
            
            $("#estacionamiento").prop("checked", false).bootstrapSwitch("destroy").bootstrapSwitch();
            $("#hora_llegada, #hora_salida").prop("readonly", true).removeAttr("required");
            
            $("#estacionamiento").on("switchChange.bootstrapSwitch", function (event, state) {
                //console.log(this); // DOM element
                //console.log(event); // jQuery event
                //console.log(state); // true | false

                //$(".date_time").val(  $("#fecha_operacion").val()+' '+$("#hora_operacion").val()  )
                /*
                HORA = moment($("#fecha_operacion").val() + ' ' + $("#hora_operacion").val(), "DD/MM/YYYY hh:mm");
                $("#hora_llegada").val(HORA.subtract(5, 'minutes').format('DD/MM/YYYY HH:mm'))
                $("#hora_salida").val(HORA.add(10, 'minutes').format('DD/MM/YYYY HH:mm'));
                */
                //$(".date_time").val(  $("#fecha_operacion").val()+' '+$("#hora_operacion").val()  )
                $(".date_time")
                        .prop({"readonly": !state, "required": state})
                        .eq(0).focus()
                        ;


            });
            
            
            $(".listServFact, .filaServExt").remove();
            $("#text-info").html("");
            
            $("#aeronave_id, #piloto_id, #proc_dest_id").val("");
            $("#prod_servd, #prod_servd_remove").html("");
            
            $(".info-pulse").removeClass("info-pulse");
            $(".overlay-wrapper").remove();
        }
        
        function seeServ(){
            if ($("#frmPrinc1").valid()) {
                if (checkForm()==true){
                    
                    $("#prod_servd_remove").children().remove();
                    $("#modalCreateFly").prepend(LOADING);
                    $("#icon_remove").remove();
                    $.post("{{route('get_total_serv')}}", $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();

                        tr = "";
                        $(".listServFact").remove();
                        for(i in response.data){
                            tr += '<tr class="listServFact"><td style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000"><span  onclick="removeServ(this, \''+response.data[i].codigo+'\')" class="icon_remove fa fa-times"></span></td><td style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000" >'+response.data[i].cant+' '+response.data[i].nomenclatura+'</td><td   style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000">'+response.data[i].full_descripcion+'</td></tr>';
                        }
                        tr += '<tr class="listServFact"><td colspan="3" style="text-align: right;padding-top: 10px;"><button type="button" class="btn btn-success info-pulse" onClick="saveData()" id="save"> <li class="fa fa-save"></li> GUARDAR </button></td></tr>';
                        $("#listServApp").append(tr);
                        window.scrollTo(0, 0);
                        $("#idServicios").click();

                    }).fail(function (){
                        $(".overlay-wrapper").remove();
                    });


                }
            }else{
                $("#document")[0].scrollIntoView();
                $("#document").focus();

            }    
        }
        
        function removeServ(obj, cod) {
            $(obj).parents("tr").remove();
            $("#prod_servd_remove").append('<option selected value="' + cod + '">' + cod + '</option>');

        }
                
        function getDataEdit(obj, event){
            event.preventDefault();
            $("#card-body-main").prepend(LOADING);
            $.get(obj.href, function (response){
                $(".overlay-wrapper").remove();
                $("#id").val(response.data.crypt_id);
                if ( $("#aeropuerto_id").length >0){
                    $("#aeropuerto_id").val(response.data.aeropuerto.crypt_id);
                }
                
                $("#fecha_operacion").val(response.data.fecha_operacion2);
                $("#hora_operacion").val(response.data.hora_operacion);
                
                $("#modal-vuelo").modal("show");
                
                $("#modal-body-princ-vuelo").prepend(LOADING);
                
                if (response.data.tipo_operacion_id==1){//si es llegada
                    llegada();
                }else{
                    salida();
                }
                if (response.data.tipo_vuelo_id==1){//si es nacional
                    nacional();
                }else{
                    internacional();
                }
                $("#tipo_aviacion_id").val(response.data.tipo_aviacion_id);
                selectNave(response.data.aeronave.crypt_id, response.data.aeronave.full_nombre_tn);
                selectPilot(response.data.piloto.crypt_id, response.data.piloto.full_name);
                
                $("#pax").val((response.data.pasajeros_desembarcados>0? response.data.pasajeros_desembarcados:response.data.pasajeros_embarcados));
                
                if (response.data.hora_llegada2 != response.data.hora_salida2){
                    $("#estacionamiento").prop("checked", true).bootstrapSwitch("destroy").bootstrapSwitch();
                    $("#hora_llegada, #hora_salida").prop("readonly", false).attr("required", 'required');
                }
                console.log(response.data.get_detalle[1].codigo.startsWith("2.2"))
                var encontrado = false;

                $.each(response.data.get_detalle, function(index, value) {
                console.log(value)
                if (value.codigo.startsWith("2.2") == true){
                    encontrado = true;
                }                  
                });

                if (encontrado == true){
                $("#estacionamiento").prop("checked", true).bootstrapSwitch("destroy").bootstrapSwitch();
                $("#hora_llegada, #hora_salida").prop("readonly", false).attr("required", 'required');
                }else{
                $("#estacionamiento").prop("checked", false).bootstrapSwitch("destroy").bootstrapSwitch();
                $("#hora_llegada, #hora_salida").prop("readonly", true).removeAttr("required", 'required');
                }


                
                $("#hora_llegada").val(response.data.hora_llegada2);
                $("#hora_salida").val(response.data.hora_salida2);
                
                //seeServ();
                
                $(".overlay-wrapper").remove();
                
            }, 'json').fail(function (){
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: 'error',
                    title: 'Error al Consultar.'
                });
            });
            
            return false;
        }
        
        function saveComplementos(obj){
            
            if ($(obj).parents("form").valid()){
                $("#modal-princ-comp").prepend(LOADING);
                $.post($(obj).parents("form").attr("action"),$(obj).parents("form").serialize(),  function (response){
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    table1.draw();
                    form = $(obj).parents("form").attr("id");
                    
                    if ( $("#"+form+" #complemento").val()=="plat" ){
                        cargarPlat();
                    }
                }).fail(function (){
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: 'error',
                        title: 'Error al Guardar.'
                    });
                })
            }
        }
        
        function setAddAdons(obj, event){
            event.preventDefault();
            $("#card-body-main").prepend(LOADING);
            $.get(obj.href, function (response){
                $(".overlay-wrapper").remove();
                $("#modal-princ-comp").modal("show");
                $("#modal-princ-comp").prepend(LOADING);
                
                $("#frmCompPax #id").val(response.data.crypt_id);
                $("#frmCompResp #id").val(response.data.crypt_id);
                $("#frmCompPlat #id").val(response.data.crypt_id);
                $("#frmCompOrigDest1 #id").val(response.data.crypt_id);
                $("#frmCompOrigDest2 #id").val(response.data.crypt_id);
                
                $(".listDataPax").remove();
                cant = parseInt(response.data.pasajeros_desembarcados, 10);
                if (cant>0){
                    
                    for(i=0; i< cant; i++){
                        doc = response.data.pasajeros.length > 0 ? response.data.pasajeros[i].documento : '';
                        nom = response.data.pasajeros.length > 0 ? response.data.pasajeros[i].nombres : '';
                        ape = response.data.pasajeros.length > 0 ? response.data.pasajeros[i].apellidos : '';
                        gen = response.data.pasajeros.length > 0 ? response.data.pasajeros[i].genero : '';
                        tr = '<tr class="listDataPax"><td>'+(i+1)+'</td><td><input value="'+doc+'" required="required" name="paxs['+i+'][cedula]" maxlength="20" type="text" class="form-control" /></td><td><input value="'+nom+'" required="required" name="paxs['+i+'][nombre]" maxlength="50" type="text" class="form-control" /></td><td><input value="'+ape+'" required="required" name="paxs['+i+'][apellido]" type="text" maxlength="50" class="form-control" /></td><td><select name="paxs['+i+'][genero]" required="required" class="form-control"><option '+(gen=='M' ? 'selected="selected"':'')+' value="M">Hombre</option><option '+(gen=='F' ? 'selected="selected"':'')+' value="F">Mujer</option></select></td></tr>';
                        $("#listDataPax").append(tr);

                    }
                }
                
                if (response.data.observaciones_facturacion!=""){
                    responsable = response.data.observaciones_facturacion.split('|');
                    $("#frmCompResp #document").val(responsable[1]);
                    $("#frmCompResp #razon").val(responsable[2]);
                    $("#frmCompResp #phone").val(responsable[3]);
                    $("#frmCompResp #correo").val('');
                    $("#frmCompResp #direccion").val(responsable[4]);
                }else{
                    $("#frmCompResp #document").val('');
                    $("#frmCompResp #razon").val('');
                    $("#frmCompResp #phone").val('');
                    $("#frmCompResp #correo").val('');
                    $("#frmCompResp #direccion").val('');
                }
                
                $("#orig_dest").children().prop("disabled", false);
                $("#pais_id").children().prop("disabled", false);
                
                
                if (response.data.tipo_vuelo_id==1){// SI ES NACIONAL
                    $("#orig_dest").children('[data-pais!=1]').not('[value=""]').prop("disabled", true);
                    $("#pais_id").children('[value!={{$Venezuela}}]').prop("disabled", true);
                }else{
                    $("#orig_dest").children('[data-pais=1]').not('[value=""]').prop("disabled", true);
                    $("#pais_id").children('[value={{$Venezuela}}]').prop("disabled", true);
                }
                $("#orig_dest, #pais_id").trigger('change');
                
                if (response.data.tipo_operacion_id==1){// SI ES LLEGADA
                    $("#plataformas").html('<button type="button" onclick="cargarPlat()" class="btn btn-success"><li class="fa fa-reload"></li> Cargar Plataformas</button>');
                    $("#orig_dest_text").html('<b>Aeropuerto Origen</b>');
                }else{
                    $("#plataformas").html('');
                    $("#orig_dest_text").html('<b>Aeropuerto Destino</b>');
                }
                
                $(".overlay-wrapper").remove();
                
            }, 'json').fail(function (){
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: 'error',
                    title: 'Error al Consultar.'
                });
            });
            
            return false;
        }
        
        function cargarPlat(){
            $("#modal-princ-comp").prepend(LOADING);
            $.get("{{url('get-plataformas')}}/"+$("#frmCompPlat #id").val(), function (response){
                $(".overlay-wrapper").remove();
                $("#plataformas").html(response);
            }).fail(function (){
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: 'error',
                    title: 'Error al Consultar.'
                });
            })
        }
        
        function selectPuesto(obj, id, nave) {
            $("#puesto_" + id).prop("checked", true);
            $(".free").find(".progress-description").html("");
            $(".free").find(".bg-warning").removeClass("bg-warning").addClass('bg-success');
            $("#description_" + id).html(nave);
            $("#bg_" + id).removeClass("bg-success").addClass('bg-warning');
        }
        
        function findDato2() {
            if ($("#frmCompResp #document").valid()) {
                $("#modal-princ-comp").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#frmCompResp #type_document").val() + "/" + $("#frmCompResp #document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $("#frmCompResp #cliente_id").val(response.data.crypt_id);
                        $("#frmCompResp #razon").val(response.data.razon_social);
                        $("#frmCompResp #phone").val(response.data.telefono);
                        $("#frmCompResp #correo").val(response.data.correo);
                        $("#frmCompResp #direccion").val(response.data.direccion);
                        $("#frmCompResp #razon, #frmCompResp #phone, #frmCompResp #correo, #frmCompResp #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#frmCompResp #razon, #frmCompResp #phone, #frmCompResp #correo, #frmCompResp #direccion").prop("readonly", false);
                        $("#frmCompResp #cliente_id, #frmCompResp #razon, #frmCompResp #phone, #frmCompResp #correo, #frmCompResp #direccion").val("");
                        $("#frmCompResp #razon").focus();
                    }
                    $("#frmCompResp").valid();
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#frmCompResp #razon, #frmCompResp #phone, #frmCompResp #correo, #frmCompResp #direccion").prop("readonly", true);
                    $("#frmCompResp #cliente_id, #frmCompResp #razon, #frmCompResp #phone, #frmCompResp #correo, #frmCompResp #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }
        }

        function checkTipoCarga(obj){
            if (obj.value == 0){
                $('#carga').val('').prop('disabled', true);
            }else{
                $('#carga').prop('disabled', false);
            }
        }
    </script>

</div>