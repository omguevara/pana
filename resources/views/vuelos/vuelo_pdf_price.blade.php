<! DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>VUELO</title>
    </head>
    <style>
        .page_break {
            page-break-before: always;
        }


        .block {
            display: block;
            width: 40%;
            float:left;
            margin-left: 5%;
            border: none;
            background-color: #17a2b8;
            color: white;
            padding: 14px 28px;
            font-size: 16px;
            cursor: pointer;
            text-align: center;
        }

        .block:hover {
            background-color: #138496;
            color: black;
        }
        @media print
        {
            .block
            {
                display: none !important;
            }
            #text_copia{
                display: block !important;
            }
        }


        .rotar {
            -webkit-transform: rotate(-45deg);
            color:#A4A4A4;
            height: 20px;
            position: absolute;
            text-align: center;
            transform: rotate(-45deg);
            width: 80px;

        }
        .foot_pie{
            bottom: 0;
            margin-bottom: 110px;
            position: absolute;
            width:100%



        }

        table.cuadro tr td, table.cuadro tr th {
            border: 1px solid black;
            border-collapse: collapse;
        }
        td.bac{
            background-color: #A4A4A4;
        }
    </style>
    <body>

        <table  width="100%" cellspacing="0"  >
            <tr>

                <td colspan="4" ><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>

            </tr>
            <tr>
                <td colspan="4" >&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" align="center" ><h1>REGISTRO DE VUELO</h1></td>
            </tr>
        </table>
        <table class="cuadro"  width="100%" cellspacing="0"  >
            <tr>
                <td class="bac">Fecha de Operación:</td>
                <td>{{$Vuelo->fecha_operacion2}}</td>
                <td class="bac">Hora de Operación:</td>
                <td>{{$Vuelo->hora_operacion}}</td>
            </tr>
            <tr>
                <td class="bac">Tipo de Operación:</td>
                <td>{{$Vuelo->tipo_operacion}}</td>
                <td class="bac">Tipo de Vuelo:</td>
                <td>{{$Vuelo->tipo_vuelo}}</td>
            </tr>
            <tr>
                <td class="bac">Piloto:</td>
                <td>{{$Vuelo->piloto->full_name}}</td>
                <td class="bac">Aeronave:</td>
                <td>{{$Vuelo->aeronave->full_nombre_tn}}</td>
            </tr>
        
            @if ($Vuelo->tipo_operacion_id==1)
            <tr>
                <td class="bac">Aeropuerto Origen:</td>
                <td colspan="3">{{ $Vuelo->origen_destino->full_nombre}}</td>
            </tr>
            <tr>
                <td class="bac">Aeropuerto Destino:</td>
                <td colspan="3">{{$Vuelo->aeropuerto->full_nombre}}</td>
            </tr>
            <tr>
                <td class="bac">Pasajeros Desembarcados:</td>
                <td colspan="3">{{$Vuelo->pasajeros_embarcados>0 ? $Vuelo->pasajeros_embarcados:$Vuelo->pasajeros_desembarcados}}</td>
            </tr>

            @else
            <tr>
                <td class="bac">Aeropuerto Origen:</td>
                <td colspan="3">{{ $Vuelo->aeropuerto->full_nombre}}</td>
            </tr>
            <tr>
                <td class="bac">Aeropuerto Destino:</td>
                <td colspan="3">{{$Vuelo->origen_destino->full_nombre}}</td>
            </tr>
            <tr>
                <td class="bac">Pasajeros Embarcados:</td>
                <td colspan="3">{{$Vuelo->pasajeros_embarcados>0 ? $Vuelo->pasajeros_embarcados:$Vuelo->pasajeros_desembarcados}}</td>
            </tr>
            @endif
        </table>


        <table  width="100%" cellspacing="0" >
            <tr>
                <td colspan="4" align="center" ><h4>SERVICIOS APLICADOS</h4></td>
            </tr>
        </table>

        <table style="width:100%" >

            <tr>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">N&deg;</td>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                <td style="width:55%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                <td style="width:15%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>

            </tr>
            @php
            $sum = 0;
            $base_imponible = 0;
            $excento = 0;
            $iva = 0;

            @endphp
            
            
            @php
            $estacionamiento = false;
            @endphp
            @foreach($Vuelo->getDetalle as $key=>$value)
            
            @php
            if ($value->iva > 0){
            $base_imponible += ($value->precio);
            }else{
            $excento += ($value->precio);
            }
            $sum += ($value->precio);


            $iva += ($value->precio)*( ($value->iva/100));

            @endphp
            
            
            @if (Str::contains($value->full_descripcion, '2.2'))
            @php
            $estacionamiento = true;
            @endphp
            @endif
            <tr onclick="alert(this.title)" title="{{$value->formula2}}">
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$key+1}}</td>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{muestraFloat($value->cant, 2).' '.$value->nomenclatura }}</td>
                <td style="width:55%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$value->full_descripcion . ' '.$value->iva2}}</td>
                <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000">{{$value->nomenclatura}}</td>
                <td style="width:15%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align: right">{{muestraFloat($value->precio)}}</td>

            </tr>
            @endforeach
            @php
            $BASE_IMPONIBLE = round($base_imponible, 2);
            $EXENTO = round($excento, 2);
            $SUBTOTAL=round($sum, 2);
            $IVA = round($iva, 2);
            $TOTAL = round(($SUBTOTAL+$IVA), 2);
            @endphp
            <tr>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:70%; text-align:right" colspan="4">BASE IMPONIBLE</td>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($BASE_IMPONIBLE)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:70%; text-align:right" colspan="4">EXENTO</td>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($EXENTO)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:70%; text-align:right" colspan="4">SUBTOTAL</td>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($SUBTOTAL)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:70%; text-align:right" colspan="4">IVA (16%)</td>
                <td style="font-weight: bold; border-bottom: 1px solid #000;  font-family: 'ROBOTO'; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($IVA)}}</td>

            </tr>
            <tr>
                <td style="font-weight: bold; border:solid 1px;  font-family: 'ROBOTO'; font-size: 12px; width:70%; text-align:right" colspan="4">TOTAL</td>
                <td style="font-weight: bold; border:solid 1px;  font-family: 'ROBOTO'; font-size: 12px; width:15%;"  align="right" >{{muestraFloat($TOTAL)}}</td>

            </tr>
            
            
        </table>
        @if ($estacionamiento==true)
        <div class="input-group mt-2"  >
            <div class="input-group-prepend">
                <span class="input-group-text"><b>Hora de Llegada a Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>: {{date('d/m/Y H:i',strtotime($Vuelo->hora_llegada))}}</b></span>
            </div>
            

        </div>
        <div class="input-group mt-2"  >
            <div class="input-group-prepend">
                <span class="input-group-text"><b>Hora de Salida de Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>: {{date('d/m/Y H:i',strtotime($Vuelo->hora_salida))}}</b></span>
            </div>
            

        </div>
        @endif

        <table  width="100%" cellspacing="0" >
            <tr>
                <td>Observación:</td>
                <td>{{ $Vuelo->observaciones}}</td>
            </tr>
        </table>


        <script type="text/javascript">
            document.addEventListener("DOMContentLoaded", function () {
                //setPint()
                //window.close();

            });


        </script>



    </body>
</html>
