
<div id="panel_princ" class="col-sm-12  mt-1">
    <style>
        .dataTables_wrapper{
            width:100%;
        }
    </style>
     <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Vuelos')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"vuelos",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::select('prod_servd[]', [], "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}


        <div id="card-body-main" class="card-body ">
            <div class="row  ">
                <div  class="col-sm-4 mb-2  ">
                    <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                        <div class="input-group-prepend">
                            <span id="labelFecha" class="input-group-text"><b>Fecha de Operaci&oacute;n:</b></span>
                        </div>
                        {{Form::text("fecha_operacion", ($f == null ? date("d/m/Y"):showDate($f) ), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                        <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                            <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                        
                    </div>
                </div>
                <div  class="col-sm-6 mb-2  ">
                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Aeropuerto:</b></span>
                        </div>
                        {{Form::select('aeropuerto_id', $Aeropuertos, ( $aeropuerto==null ? "":$aeropuerto  ) , ["placeholder"=>'Seleccione',   "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                        <div role="button" class="input-group-append" >
                            <div onClick="getDataFilter()"  class="input-group-text"><i class="fa fa-search"></i></div>
                        </div>
                    </div>
                </div>  
                <div  class="col-sm-2 text-right mb-2  ">
                    <a type="button"  href="{{route('vuelos.create')}}" onClick="setAjax(this, event)" class="btn btn-primary" ><i class="fas fa-plane"></i> Registrar Vuelo</a>
                </div>
            </div>
            
            
            <div class="row  ">
                
                
                
                <div  class="col-sm-12  ">
                    <div class="row">
                        <table id="listaVuelosRegistrados" class="display compact table-hover">
                        <thead>
                            <tr>
                                <th>{{__('ACCIONES')}}</th>
                                <th>{{__('NRO. OPERACIÓN')}}</th>
                                <th>{{__('FECHA')}}</th>
                                <th>{{__('TIPO OPERACION')}}</th>
                                <th>{{__('AERONAVE')}}</th>
                                <th>{{__('PILOTO')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>


                    </div>  
                </div>
                

            </div>  

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
</div>


<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-1">
 





    <script type="text/javascript">
         var data1 = @json($datos)
         ;
        $(document).ready(function () {
            
            $('#iconDate').datetimepicker({
                format: 'L',
            });
            
            $(".select2").select2();
            
            var table1 = $('#listaVuelosRegistrados').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'action'},
                    {data: 'operacion'},
                    {data: 'fecha_operacion2'},
                    {data: 'tipo_vuelo'},
                    
                    {data:'aeronave.full_nombre_tn'},
                    {data:'piloto.full_name'},
                    
                    
                ]
                @if (app()->getLocale() != "en")
                    , language: {
                    url: "{{url('/')}}/plugins/datatables/es.json"
                    }
                @endif

            });

            



        });
        function getDataFilter(){
            //setAjax(this, event);
            
            
            $.get("{{url('vuelos')}}/"+saveDate(  $("#fecha_operacion").val()  ) +"/"+$("#aeropuerto_id").val()  , function (response) {
                AJAX_ACTIVE = false;
                $("#main-content").html(response);

            }, 'html').fail(function (xhr) {
                $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
                AJAX_ACTIVE = false;
            });
        }
        
    </script>

</div>