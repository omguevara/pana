



<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Modificar Vuelo')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('get_vuelos')}}" onClick="setAjax(this, event)" id="backP" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["get_vuelos.edit", $Vuelo->crypt_id], "get_vuelos.edit",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::select('prod_servd[]', $Vuelo->servicios_extra_select, "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}
        

        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, $Vuelo->aeropuerto->crypt_id , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, $Vuelo->aeronave->crypt_id,  [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                            <div role='button' class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, $Vuelo->piloto->crypt_id, [ "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div role='button'  title="Agregar Nuevo Piloto" onClick="$('#modal-princ-pilotos').modal('show')" class="input-group-text">
                                    <i class="fa fa-plus"></i>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo de Operaci&oacute;n:</b></span>
                            </div>
                            
                            {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], $Vuelo->tipo_vuelo_id, ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                        </div>
                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Operaci&oacute;n:</b></span>
                            </div>
                            {{Form::text("fecha_operacion",  "", ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <div  class="input-group mt-2 time_estadia">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Tasa?:</b></span>
                            </div>
                            {{Form::select('cobrar_tasa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_tasa" ,"required"=>"required"])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Dosa?:</b></span>
                            </div>
                            {{Form::select('cobrar_dosa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_dosa" ,"required"=>"required"])}}
                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Pasajeros Desembarcados:</b></span>
                            </div>
                            {{Form::text("pax_desembarcados", $Vuelo->pasajeros_desembarcados, ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax_desembarcados", "placeholder"=>__('Cant. PAX')])}}    
                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Pasajeros Embarcados:</b></span>
                            </div>
                            {{Form::text("pax_embarcados", $Vuelo->pasajeros_embarcados, ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax_embarcados", "placeholder"=>__('Cant. PAX')])}}    
                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora de Llegada a Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                            </div>
                            {{Form::text("hora_llegada",( $Vuelo->hora_llegada == null  ? "":showDate($Vuelo->hora_llegada, 'full', true)), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control    date_time", "id"=>"hora_llegada" ])}}

                        </div>
                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora de Salida de Plataforma <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                            </div>
                            {{Form::text("hora_salida",($Vuelo->hora_salida==null ? "": showDate($Vuelo->hora_salida, 'full', true)), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control date_time  ", "id"=>"hora_salida" ])}}
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observación:</b></span>
                            </div>
                            {{Form::text("observacion",  $Vuelo->observaciones, ["class"=>"form-control ", "id"=>"observacion", "placeholder"=>__('Observación')])}}    
                        </div>














                    </div>  
                    <div class="row  mt-2 ">
                        <div   class="col-sm-12 col-md-4  text-center   " style="display:none">
                            {{Form::button('<li class="fa fa-home"></li> '.__("PUESTO").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addPuesto"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-3  text-center  " style="display:none">
                            {{Form::button('<li class="fa fa-user"></li> '.__("PAX").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addPax"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-6  text-center  ">
                            {{Form::button('<li class="fa fa-plus"></li> '.__("AGREGAR SERV.").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addServ", "onClick"=>"setServ()"])}}    
                        </div>
                        <div   class="col-sm-12 col-md-6 text-center  ">
                            {{Form::button('<li class="fa fa-save"></li> '.__("GUARDAR").' <li class="fa fa-caret-up"></li>',  ["onClick"=>"saveData()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"save"])}}    
                        </div>
                    </div>

                </div>  
                <div   class="col-sm-12 col-md-6   ">





                </div>


            </div>  

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>




    <div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ "route"=>"aeronaves_plus", 'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6" >
                            <div class="form-group">
                                <label for="matricula">{{__('Matrícula')}}</label>
                                {{Form::text("matricula", "", ["required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="nombre">{{__('Modelo')}}</label>
                                {{Form::text("nombre", "", ["required"=>"required", "maxlength"=>"20" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                                {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nuevo Piloto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ 'route'=>'pilotos_plus',  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                <div id="modalPrincBodyFact" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="type_document">{{__('Tipo de Documento')}}</label>
                                {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>
                                {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="name_user">{{__('Name')}}</label>
                                {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="surname_user">{{__('Surname')}}</label>
                                {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                            </div>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-princ-serv-extra"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalExt" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Agregar Servicios Extra</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open([ "route"=>"get_servs_ext",  'id'=>'frmServ','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-12" >

                            <table style="width:100%" id="tableServExt">
                                <tr>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                    <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>

                                </tr>

                            </table>

                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicar"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">

        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            $('#fecha_operacion').val("{{showDate($Vuelo->fecha_operacion)}}");
            $('#iconDate').datetimepicker({
                format: 'L'
            });
            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM", "clearIncomplete": true});
            
            $("#prod_servd option").prop("selected", true);


            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});



            $('.number').numeric({negative: false});


            $("#aplicar").on("click", function () {
                if ($("#frmServ").valid() ){
                    $("#modal-princ-serv-extra").modal("hide");
                }
                
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmPilotos').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    $("#frmNave").prepend(LOADING);
                    $.post(this.action, $("#frmNave").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmNave").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#aeronave_id").append('<option  value="' + response.data.crypt_id + '">' + response.data.full_nombre + '</option>');
                            $("#aeronave_id").val(response.data.crypt_id);
                            $("#aeronave_id").trigger("reset");
                            $('#frmNave').trigger("reset");
                            $("#modal-princ-aeronave").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Guardar"
                        });
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPilotos').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPilotos').valid()) {
                    $("#frmPilotos").prepend(LOADING);
                    $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmPilotos").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#piloto_id").append('<option value="' + response.data.crypt_id + '">' + response.data.full_name + '</option>');
                            $("#piloto_id").val(response.data.crypt_id);
                            $("#piloto_id").trigger("reset");
                            $('#frmPilotos').trigger("reset");
                            $("#modal-princ-pilotos").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Guardar"
                        });
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });



        });

        function checkValue(obj) {

            if (obj.value == $("#aeropuerto_id").val()) {
                obj.value = "";
                $(obj).trigger("reset");
                Toast.fire({
                    icon: "error",
                    title: "El Aeropuerto no debe ser el Mismo del Origen"
                });
            }

            if ($("#aeropuerto_id").val() == "") {
                obj.value = "";
                $(obj).trigger("reset");
                Toast.fire({
                    icon: "error",
                    title: "Debe Seleccionar el Aeropuerto de Origen"
                });
            }
        }

        function saveData() {
            if ($("#hora_llegada").val() != "") {
                if ($("#pax_desembarcados").val() == "") {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Debe Ingresar los Pasajeros Desembarcados')}}"
                    });
                    $("#pax_embarcados").focus();
                    return false;
                }
            }

            if ($("#hora_salida").val() != "") {
                if ($("#pax_embarcados").val() == "") {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Debe Ingresar los Pasajeros Embarcados')}}"
                    });
                    $("#pax_embarcados").focus();
                    return false;
                }

                if ($("#hora_llegada").val() == "") {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Debe Ingresar La Hora de Llegada')}}"
                    });
                    $("#hora_llegada").focus();
                    return false;
                }

            }

            if ($("#cobrar_tasa").val() == 1) {
                if ($("#hora_salida").val() == "") {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Debe Agregar la Hora de Salida')}}"
                    });
                    return false;
                }

                if ($("#pax_embarcados").val() == 0) {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Debe Agregar Los Pasajeros Embarcados')}}"
                    });
                    return false;
                }
            }

            if ($("#cobrar_dosa").val() == 1) {
                if ($("#hora_llegada").val() == "") {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Debe Agregar la Hora de Llegada')}}"
                    });
                    return false;
                }
            }

            if ($("#cobrar_tasa").val() == 0 &&  $("#cobrar_dosa").val() == 0) {

                Toast.fire({
                    icon: "warning",
                    title: "{{__('Debe Seleccionar Cobrar la Dosa o Tasa')}}"
                });
                return false;

            }


            if ($("#hora_llegada").val()==""){
                f1 = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
            }else{
                f1 = moment($("#hora_llegada").val().substr(0, 10), "DD/MM/YYYY");
            }


            if ($("#hora_salida").val()==""){
                f2 = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
            }else{
                f2 = moment($("#hora_salida").val().substr(0, 10), "DD/MM/YYYY");
            }


            f = moment($("#fecha_operacion").val(), "DD/MM/YYYY");


            if (f>=f1 && f<=f2){


                if ($("#hora_llegada").val()!=""  &&  $("#hora_salida").val()!=""){
                    fi = moment($("#hora_llegada").val(), "DD/MM/YYYY hh:mm");
                    ff = moment($("#hora_salida").val(), "DD/MM/YYYY hh:mm");
                    if (fi>ff){
                        Toast.fire({
                            icon: "error",
                            title: "{{__('La Hora de Llegada no Puede Ser Mayor a la Hora de Salida')}}"
                        });
                        return false;
                    }
                }
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('La Fecha de Vuelo No Esta en el Intervalo de Hora de Llegada y Hora de Salida')}}"
                });
                return false;
            }
            
            
            if ($("#frmPrinc1").valid()) {
                Swal.fire({
                    title: 'Esta Seguro que Desea Guardar el Vuelo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<li class="fa fa-save"></li> Si',
                    cancelButtonText: '<li class="fa fa-undo"></li>  No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        
                        
                        

                        $("#frmPrinc1").prepend(LOADING);
                        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize()+"&"+$("#frmServ").serialize(), function (response) {
                            $(".overlay-wrapper").remove();


                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $(".overlay-wrapper").remove();

                            if (response.status == 1) {

                                //$("#frmPrinc1").trigger("reset");
                                $("#backP").click();


                            } else {

                            }





                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });
            }
        }
        function setServ() {
            if ($("#frmPrinc1").valid()) {
                $('#modal-princ-serv-extra').modal('show');
                $("#frmServ").prepend(LOADING);

                $.post($("#frmServ").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    $(".overlay-wrapper").remove();
                    $(".filaServExt").remove();

                    prods = [];
                    cantidades = [];

                    $($("#prod_servd").serializeArray()).each(function () {
                        detalleServs = this.value.split(":");
                        prods.push(detalleServs[1]);
                        cantidades[detalleServs[1]] = detalleServs[0];


                    });

                    for (i in response.data) {

                        checked = $.inArray(response.data[i]['crypt_id'], prods) == -1? false:true;


                        tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input name="item['+response.data[i]['crypt_id']+'][item]" '+(checked==true? 'checked':'')+' type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data[i]['crypt_id'] + '" /></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input name="item['+response.data[i]['crypt_id']+'][cant]" min="1" type="number" id="' + response.data[i]['crypt_id'] + '" onChange="UpItemSelect()" value="'+(checked==true? cantidades[response.data[i]['crypt_id']]:'0')+'" required="required" class="form-control required " '+(checked==true? '':'disabled')+' /></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + '</td>';


                        tabla += '</tr>';
                        $("#tableServExt").append(tabla);
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "Error al Consultar los Servicios"
                    });
                });
            }


        }
        function selectItem(obj) {
            id = $(obj).data("crypt_id");
            if (obj.checked == true) {
                $("#" + id).val("1").prop("disabled", false).focus();
            } else {
                $("#" + id).val("0").prop("disabled", true);
            }
            UpItemSelect();
        }
        function UpItemSelect() {
            $("#prod_servd").html("");
            $(".selectItem:checked").each(function () {
                Id = $(this).data('crypt_id');
                $("#prod_servd").append('<option selected value="' + $("#" + Id).val() + ':' + Id + '"></option>');
            });
        }
    </script>

</div>