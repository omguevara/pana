<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Revisión')}}</h3>

            <div class="card-tools">


                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"revision",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{ method_field('put') }}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div  class="col-sm-12 col-md-12 col-lg-4 mt-2">
                    <div class="form-group row ">
                        <label for="aeropuerto_id" class="col-sm-3 col-form-label">Aeropuerto</label>
                        <div class="col-sm-9">
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "class"=>"form-control select2 ", "id"=>"aeropuerto_id" ])}}
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-12  col-lg-3 mt-1  ">
                    <div class="form-group row ">
                        <label for="fecha_operacion" class="col-sm-3 col-form-label">Fechas</label>
                        <div class="col-sm-9">
                            {{Form::text("fecha_operacion", $RANGO_MES, ["data-msg-required"=>"Campo Requerido",    "class"=>"form-control ", "id"=>"fecha_operacion", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-12 col-lg-4 mt-1">
                    <div class="form-group row ">
                        <label for="aeronave_id" class="col-sm-3 col-form-label">Aeronave</label>
                        <div class="col-sm-9">
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",     "class"=>"form-control  select2 ", "id"=>"aeronave_id" ])}}
                        </div>
                    </div>
                </div>
                <div  class="col-sm-12 col-md-12 col-lg-1 mt-1">
                    {{Form::button('<li class="fa fa-search"></li>  ',  ["type"=>"button", "onClick"=>"table1.draw();" ,"class"=>"btn btn-primary", "id"=>"searchData"])}}

                </div>
            </div>
            <div class="row">

            </div>
            <div class="row">
                <table id="example1" style="width:100%" >
                    <thead>
                        <tr>
                            <th>{{__('Actions')}}</th>

                            <th>{{__('FECHA')}}</th>

                            <th>{{__('AEROPUERTO')}}</th>
                            <th>{{__('AERONAVE')}}</th>

                            <th>{{__('PILOTO')}}</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>


        <!-- /.card-body -->
    </div>

    {{ Form::close() }}

    <div class="modal fade" id="modal-dee-detail"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div  class="modal-content">
                <div class="modal-header">
                    <h4 class="">Detalle de Vuelo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div style="height: 100vh" class="modal-body">
                    <iframe src="" id="see_data" style="width: 100%; height: 100%; border:none">

                    </iframe>
                </div>
                <div class="modal-footer justify-content-between">
                    {{Form::button('<li class="fa fa-check"></li>  ',  ["onClick"=>"ok(1)", "type"=>"button", "class"=>"btn btn-primary"])}}
                    {{Form::button('<li class="fa fa-trash"></li>  ',  ["onClick"=>"ok(0)", "type"=>"button", "class"=>"btn btn-danger"])}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>









    <script type="text/javascript">

        var table1 = null;
        var RouteDetail = '';
        var ID_CHECK = 0;
        $(document).ready(function () {
            $(".select2").select2();

            $('#fecha_operacion').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });


            table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                order: [[3, 'desc']],

                ajax: {
                    url: "{{route('revision')}}",
                    type: "post",
                    data: function (d) {
                        d.aeropuerto = $('#aeropuerto_id').val(),
                                d.fechas = $('#fecha_operacion').val(),
                                d.nave = $('#aeronave_id').val(),
                                d._token = "{{csrf_token()}}",
                                d._method = "put",
                                d.search = $('input[type="search"]').val()


                    }
                },
                serverSide: true,
                processing: true,

                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'action'},
                    {data: 'fecha_operacion2'},
                    {data: 'aeropuerto.full_nombre'},
                    {data: 'aeronave.full_nombre_tn'},
                    {data: 'piloto.full_name'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }


            });


        });
        function getDataFly(obj, evt) {
            event.preventDefault();
            ID_CHECK = $(obj).attr("data-id");
            $("#see_data").attr("src", obj.href);
            $("#modal-dee-detail").modal("show");
            return false;
        }

        function ok(op) {
            mensaje = "";
            if (op == 0) {
                while (mensaje == "") {
                    mensaje = prompt("Describa el Error: ");
                }
            }
            if (mensaje != null) {
                if (confirm("Seguro que desea Ejecutar esta Instrucción?")) {
                    $("#modal-dee-detail").prepend(LOADING);
                    $.post("{{route('revision')}}", "_token={{csrf_token()}}&op=" + op + "&msg=" + mensaje + "&id=" + ID_CHECK, function (response) {
                        $(".overlay-wrapper").remove();
                        table1.draw();
                        $("#modal-dee-detail").modal("hide");
                        alert("Procesada Correctamente");
                    }, 'html').fail(function () {
                        $(".overlay-wrapper").remove();

                    });
                }

            }



        }

    </script>

</div>
