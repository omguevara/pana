<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Edición del Estatus de Vuelo')}}</h3>

            <div class="card-tools">

                <a id="refreshMolulo" href="{{route('edit_status_vuelo')}}" onClick="setAjax(this, event)"   ><i class="fas fa-sync"></i></a>  
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div   class="col-sm-12 col-md-12 mt-2   ">

                    <table id="example1" class="display compact table-hover">
                        <thead>
                            <tr>
                                <th>{{__('Actions')}}</th>
                                <th>{{__('NRO. OPERACIÓN')}}</th>
                                <th>{{__('FECHA')}}</th>
                                {{-- <th>{{__('TIPO OPERACION')}}</th> --}}
                                <th>{{__('AEROPUERTO')}}</th>
                                <th>{{__('AERONAVE')}}</th>

                                <th>{{__('PILOTO')}}</th>
                                <th>{{__('ESTATUS')}}</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>



                </div>


            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        
    </div>


      <div class="modal fade" id="modal-edit-status-vuelo"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Editar Estatus del Vuelo</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                {{Form::open(["route"=>"edit_status_vuelo",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
                {{ method_field('POST') }}
                {{Form::hidden("id", "", ["id"=>"id"])}}    
                <div  class="modal-body">
                    <div class="row">
                        
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::text("matricula", "", ["readonly"=>"readonly", "class"=>"form-control ", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}    
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::text("piloto", "", ["readonly"=>"readonly", "class"=>"form-control ", "id"=>"piloto", "placeholder"=>__('Piloto')])}}    
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Carbiar Estatus:</b></span>
                            </div>
                            {{Form::select('status', ["0"=>"Permitir Modificar", "1"=>"NO Permitir Modificar"], "1" , ["placeholder"=>'Seleccione',   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required   ", "id"=>"status" ,"required"=>"required"])}}

                        </div>

                    </div>

                </div>
                <div class="modal-footer justify-content-between">

                     {{Form::button('<li class="fa fa-save"></li> '.__("GUARDAR"),  ["onClick"=>"updateStatusVuelo()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"save"])}}    


                </div>
                {{ Form::close() }}

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <script type="text/javascript">
        var data1 = @json($data)
                ;
                
        
        $(document).ready(function () {
            
            var table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                    },
                columns: [
                    {data: 'action'},
                    {data: 'operacion'},
                    {data: 'fecha_operacion2'},
                    // {data: 'tipo_operacion'},
                    {data: 'aeropuerto.full_nombre'},
                    {data: 'aeronave.full_nombre_tn'},
                    {data: 'piloto.full_name'},
                    {data: function (row){  return (row.aprobado == true ? 'VUELO APROBADO':'VUELO EN EDICIÓN'); }}
                ]
                @if (app()->getLocale() != "en")
                    , language: {
                    url: "{{url('/')}}/plugins/datatables/es.json"
                    }
                @endif

            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            
        });
        
        
        function editStatusVuelo(id, m, p){
            
            $("#id").val(id);
            $("#matricula").val(m);
            $("#piloto").val(p);
            $("#modal-edit-status-vuelo").modal("show");
        }
        function updateStatusVuelo(){
            if ( $("#frmPrinc1").valid()  ){
                $("#frmPrinc1").prepend(LOADING);
                $.post($("#frmPrinc1").attr("action"),  $("#frmPrinc1").serialize(), function (response){
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status==1){
                        $("#modal-edit-status-vuelo").modal("hide");
                        setTimeout(function (){
                            $("#refreshMolulo").click();
                        }, 1000);
                        
                    }
                    
                    
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: 'error',
                        title: "Error en la Conexión"
                    });
                });
            }
        }
    </script>

</div>
