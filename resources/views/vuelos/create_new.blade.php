
<div id="panel_princ" class="col-sm-12  mt-1">
    <style>
        .dataTables_wrapper{
            width:100%;
        }
        .paginate_button{
            padding: 0px !important;
        }
        .error_extra{
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #dc3545;
        }
        
       .tooltip-inner {
            background-color: #419bde;
        }
        .tooltip.bs-tooltip-right .arrow:before {
            border-right-color: #419bde !important;
        }
        .tooltip.bs-tooltip-left .arrow:before {
            border-right-color: #419bde !important;
        }
        .tooltip.bs-tooltip-bottom .arrow:before {
            border-right-color: #419bde !important;
        }
        .tooltip.bs-tooltip-top .arrow:before {
            border-right-color: #419bde !important;
        }
        
        
        tr.listServFact:hover{
            background-color: #ffe69b;
        }
        
        .circulo {
            border-radius: 50%;
            font-size: 16px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            width: 30px;
            padding: unset;
        }

    </style>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registro de Vuelo')}}  {{($Aeropuertos->count()==1 ? ": ".$Aeropuertos->first():'')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('vuelos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Limpiar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"vuelos",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::hidden('tipo_id', "", ["id"=>"tipo_id"])}}
        {{Form::hidden('nacionalidad_id', "", ["id"=>"nacionalidad_id"])}}        
        {{Form::hidden('aeronave_id', "", ["id"=>"aeronave_id"])}}        
        {{Form::hidden('piloto_id', "", ["id"=>"piloto_id"])}}        
        {{Form::hidden('newPortal', "1", ["id"=>"newPortal"])}}        
        {{Form::hidden('proc_dest_id', "", ["id"=>"proc_dest_id"])}}        
        
        
        {{Form::select('prod_servd[]', [], "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}
        {{Form::select('prod_servd_remove[]', [], "", ["style"=>"display:none", "id"=>"prod_servd_remove" ,"multiple"=>"multiple"])}}
        


        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div  class="col-md-4 border-right">
                    
                    <div class="row">
                        
                    </div>
                </div>
                <div  class="col-md-4 border-right">
                    <h5 class="border-bottom">Datos del Vuelo</h5>
                    
                    @if ($Aeropuertos->count()>1)
                        <div class="row">
                            <div  class="col-md-12 ">
                                <div class="input-group mt-2"  >
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Aeropuerto:</b></span>
                                    </div>
                                    {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                                </div>
                               
                            </div>
                             
                        </div>
                    @endif
                    
                    <div class="row">
                        <div  class="col-md-11 ">
                            <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Fecha:</b></span>
                                </div>
                                {{Form::text("fecha_operacion", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha_operacion" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                                <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                    <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        <div  class="col-md-1 mt-2">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Fecha cuando se genero el vuelo" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div  class="col-md-11 ">
                            <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Hora: <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                </div>
                                {{Form::text("hora_operacion", date("H:i"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  timer required ", "id"=>"hora_operacion" ,"required"=>"required"])}}
                            </div>
                        </div>
                        <div  class="col-md-1 mt-2">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Hora cuando se genero el vuelo, Recuerde Registrar la Hora el Formato 24H" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                    <div id="contentTipo" class="row mt-2">
                        <div  class="col-md-11">
                            <div class="row ">
                                <div id="btn_llegada" class="col-md-6">
                                    <button id="arrivo" type="button" onclick="llegada()"  class="btn btn-outline-primary btn-block">
                                        <i class="fas fa-plane-arrival"></i> Llegada
                                    </button>
                                </div>
                                <div id="btn_salida" class="col-md-6">
                                    <button id="despegue" type="button" onclick="salida()" class="btn btn-outline-primary btn-block">
                                        <i class="fas fa-plane-departure"></i> Salida
                                    </button>  
                                </div>
                            </div>
                        </div>
                        <div  class="col-md-1">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Selecione si el Tipo de Operación de <b>Llegada</b> o <b>Salida</b>" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                    <div id="contentNacionalidad"  class="row mt-2">
                        
                        <div  class="col-md-11">
                            <div class="row ">
                                <div id="btn_nacional" class="col-md-6">
                                    <button id="vuelo_nacional" type="button" onclick="nacional()"  class="btn btn-outline-primary btn-block">
                                        <i class="fas fa-flag"></i> Nacional
                                    </button>
                                </div>
                                <div id="btn_internacional" class="col-md-6">
                                    <button id="vuelo_internacional" type="button" onclick="internacional()" class="btn btn-outline-primary btn-block">
                                        <i class="fas fa-globe"></i> Internacional
                                    </button>
                                </div>
                            </div>
                        </div>
                        
                        <div  class="col-md-1">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Selecione si el Tipo <b>Nacional</b> o <b>Internacional</b>" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                        
                        
                    </div>
                    
                    <div class="row mt-2">
                        <div  class="col-md-11 ">
                            <div class="input-group "  >
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Tipo de Aviación:</b></span>
                                </div>
                                {{Form::select('tipo_aviacion_id', ["1"=>"General", "2"=>"Oficial", "3"=>"Militar"], "1" , ["data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"tipo_aviacion_id" ,"required"=>"required"])}}
                            </div>

                        </div>
                         <div  class="col-md-1">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Selecione si el Tipo Aviación" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                    
                    <div id="contentAeronave"  class="row mt-2">
                        <div  class="col-md-11">
                            <button id="getSelectAeronave" data-toggle="modal" data-target="#modal-list-aeronaves"  type="button"  class="btn btn-outline-primary btn-block">
                                <i class="fas fa-plus"></i> <i class="fas fa-plane"></i> Seleccionar Aeronave
                            </button>
                            
                            
                            
                        </div>
                        <div  class="col-md-1">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Seleccione La Aeronave" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                    <div id="contentPiloto"  class="row mt-2">
                        <div  class="col-md-11 mt-2">
                            <button id="getSelectPilot"  type="button" data-toggle="modal" data-target="#modal-princ-pilotos"  class="btn btn-outline-primary btn-block ">
                                <i class="fas fa-plus"></i> <i class="fa fa-user"></i> Seleccionar Piloto
                            </button>
                        </div>
                        <div  class="col-md-1 mt-2">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Seleccione al Piloto" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                    </div>
                    <div   class="row mt-2">    
                        <div  class="col-md-11 mt-2">
                            <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span id="labelCantPax" class="input-group-text"><b>Pasajeros:</b></span>
                                </div>
                                {{Form::text("pax", "0", ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number  ", "id"=>"pax", "placeholder"=>__('Cant. PAX')])}}    
                            </div>
                        </div>
                        
                        <div  class="col-md-1 mt-3">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Cantidad de Pasajeros del Vuelo, si no posee pasajeros ingrese cero (0)" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                        
                        <div  class="col-md-11 mt-2">
                            
                            <div class="input-group mt-2"  >
                                <div id="aplicaEst" class="input-group-prepend">
                                    <span  class="input-group-text"><b>Estacionamiento:</b></span>
                                </div>
                                <div  id="aplicaEstCheck" class="input-group-text">
                                    <input type="checkbox" class="switch" id="estacionamiento" value="1" name="estacionamiento" data-on-text="SI" data-off-text="NO"  data-off-color="danger" data-on-color="success">
                                </div>
                                
                            </div>
                        </div>
                        <div  class="col-md-1 mt-3">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Marque <b>Si</b>, en el caso que amerite cobrar estacionamiento" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                        <div  class="col-md-11 mt-2">
                            <div class="input-group mt-2"  >
                                <div style="width:100%" class="input-group-prepend">
                                    <span style="width:100%" class="input-group-text"><b>Hora Llegada a Plataforma: </b></span>
                                </div>
                              

                            </div>
                        </div>
                        <div  class="col-md-1 mt-3">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Hora de Llegada a Plataforma de la Aeronave" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                        <div  class="col-md-12 ">
                            <div class="input-group "  >
                                {{Form::text("hora_llegada", date('d/m/Y H:i'), ["readonly"=>"readonly", "class"=>"form-control    date_time", "id"=>"hora_llegada" ])}}

                            </div>
                        </div>
                        
                        
                        
                        <div  class="col-md-11 mt-2">
                            <div class="input-group mt-2"  >
                                <div style="width:100%" class="input-group-prepend">
                                    <span style="width:100%" class="input-group-text"><b>Hora Salida de Plataforma: </b></span>
                                </div>
                              

                            </div>
                        </div>
                        <div  class="col-md-1 mt-3">
                            <div class="row ">
                                <button data-bs-custom-class="custom-tooltip" data-placement="right"  title="Ingrese la Hora de Salida de la Plataforma de la Aeronave" style="background-color: #3c99de" type="button" class=" tips btn btn-block circulo">
                                    <i class="fas fa-info"></i>
                                </button>  
                            </div>
                        </div>
                        <div  class="col-md-12 ">
                            <div class="input-group "  >
                                
                                {{Form::text("hora_salida", date('d/m/Y H:i'),  ["readonly"=>"readonly",  "class"=>"form-control date_time  ", "id"=>"hora_salida" ])}}
                            </div>
                        </div>
                        
                        
                        
                        <div  class="col-md-12 mt-2 ">
                            <div class="input-group  ">
                                 <label style="margin-bottom: unset !important" class="border-bottom">Observación:</label>
                            </div>
                        </div>
                        
                        <div  class="col-md-12 ">
                            <div class="input-group mt-2 ">
                                
                                {{Form::textarea("observacion", "", ["class"=>"form-control ", "rows"=>"3", "id"=>"observacion", "placeholder"=>__('Observación')])}}    
                            </div>
                        </div>
                        
                        
                        <div class="col-md-12   mt-2 ">
                            <div class="row">
                            
                                <div   class="col-sm-12 col-md-4  text-center  ">
                                    {{Form::button('<li class="fa fa-plus"></li> '.__("SERV.").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addServ", "onClick"=>"setServ()"])}}    
                                </div>
                                
                                <div   class="col-sm-12 col-md-4 text-center  ">
                                    {{Form::button('<li class="fa fa-paperclip"></li> '.__("EXT").' <li class="fa fa-caret-up"></li>',  [ "type"=>"button", "class"=>"btn btn-primary", "id"=>"complementos"])}}    
                                </div>
                                <div   class="col-sm-12 col-md-4 text-center  ">
                                    {{Form::button('<li class="fa fa-eye"></li> '.__("VER").' <li class="fa fa-caret-right"></li>',  ["onClick"=>"seeServ()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"save"])}}    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                <div class="col-md-4">
                     <h5 class="border-bottom">Servicios Aplicados</h5>
                    
                </div>
            </div>

            <script type="text/javascript">
                
                $(document).ready(function () {
                    
                    
                    
                    $("#frmPrinc1 #document").focus();
                    
                    
                    
                    
                    
                    
                    
                    
                    

                    

                    
                    
                    

                    $("#modal-list-aeronaves, #modal-princ-pilotos, #modal-princ-serv-extra").on('shown.bs.modal', function () {
                        $(this).find("input[type=search]").select().focus();
                    });

                    $('a[role="tab"]').on('shown.bs.tab', function (e) {
                        content = $(e.target).attr('aria-controls');
                        $("#" + content).find("input[type=text], input[type=search]").eq(0).focus();
                    });
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    $("#complementos").on("click", function (){
                        
                    });
                    
                });
            </script>




            
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
    
    @include('vuelos.modals.aeronaves');
    @include('vuelos.modals.pilotos');
    @include('vuelos.modals.serv_extra');
    @include('vuelos.modals.complementos');

    <script type="text/javascript">
        /*
         $(document).ready(function () {
         $(".select2").select2();
         $("#document").focus();
         $(document).on('select2:open', () => {
         document.querySelector('.select2-search__field').focus();
         });
         
         
         
         
         
         $("#tipo_operacion_id").on("click", function () {
         if (this.value == '') {
         $("#labelOrigenLlegada").html('<b>Destino/Origen:</b>');
         $("#labelFecha").html('<b>Fecha de Operación:</b>');
         } else {
         if (this.value == 1) {
         $("#labelOrigenLlegada").html('<b>Origen:</b>');
         $("#labelFecha").html('<b>Fecha de LLegada:</b>');
         } else {
         $("#labelOrigenLlegada").html('<b>Destino:</b>');
         $("#labelFecha").html('<b>Fecha de Salida:</b>');
         }
         }
         });
         
         
         $('#fecha_operacion').on('input', function (e) {
         console.log(this.value)
         })
         
         
         $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
         
         
         
         
         
         $(".alpha").alphanum({
         allowNumeric: false,
         allowUpper: true,
         allowLower: true
         
         });
         
         
         
         
         
         
         
         $('#frmPrinc1').validate({
         errorElement: 'span',
         errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.input-group').append(error);
         },
         highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
         },
         unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid').addClass("is-valid");
         }
         });
         
         
         
         $('#frmPilotos').validate({
         errorElement: 'span',
         errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.input-group').append(error);
         },
         highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
         },
         unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid').addClass("is-valid");
         }
         });
         
         $("#piloto_id, #aeronave_id, #aeropuerto_id").on("change", function () {
         $(this).valid();
         });
         
         $("#aeronave_id").on("change", function () {
         $("#info").slideUp("fast");
         if (this.value != "" && $("#aeropuerto_id").val() != "") {
         
         $("#frmPrinc1").prepend(LOADING);
         $.get("{{url('get-aeronave')}}/" + this.value + "/" + $("#aeropuerto_id").val(), function (response) {
         $(".overlay-wrapper").remove();
         Toast.fire({
         icon: response.type,
         title: response.message
         });
         if (response.status == 1) {
         if (response.data.Vuelos.length > 0) {
         if (response.data.Vuelos[0].hora_llegada != null) {
         $("#hora_llegada").val(response.data.Vuelos[0].hora_llegada2);
         }
         }
         }
         
         if (response.data.membresia.length > 0) {
         msg = "{{__('La Aeronave Posee Membresía')}}<br>";
         for (i in response.data.membresia) {
         membresia_observacion = response.data.membresia[i]['observacion'] || '';
         msg += "Fecha Desde: " + response.data.membresia[i]['fecha_desde2'] + " , Fecha Hasta: " + response.data.membresia[i]['fecha_hasta2'] + "<br>";
         msg += "{{__('Información')}}: " + membresia_observacion.replace(/\r\n/g, "<br/>") + "<br/>";
         }
         $("#text-info").html(msg);
         $("#info").slideDown("fast");
         }
         
         
         
         
         }).fail(function () {
         $(".overlay-wrapper").remove();
         });
         
         
         }
         });
         
         
         
         
         
         
         
         });
         
         function checkValue(obj) {
         
         if (obj.value == $("#aeropuerto_id").val() && obj.value != '') {
         obj.value = "";
         // $(obj).trigger("reset");
         $(obj).trigger("change.select2");
         Toast.fire({
         icon: "error",
         title: "El Aeropuerto no debe ser el Mismo del Origen"
         });
         }
         
         if ($("#aeropuerto_id").val() == "" && obj.value != "") {
         obj.value = "";
         $(obj).trigger("change.select2");
         Toast.fire({
         icon: "error",
         title: "Debe Seleccionar el Aeropuerto de Origen"
         });
         }
         }
         
         function saveData() {
         if ($("#frmPrinc1").valid()) {
         
         if ($("#hora_llegada").val() != "") {
         if ($("#pax_desembarcados").val() == "") {
         Toast.fire({
         icon: "error",
         title: "{{__('Debe Ingresar los Pasajeros Desembarcados')}}"
         });
         $("#pax_embarcados").focus();
         return false;
         }
         }
         
         if ($("#hora_salida").val() != "") {
         if ($("#pax_embarcados").val() == "") {
         Toast.fire({
         icon: "error",
         title: "{{__('Debe Ingresar los Pasajeros Embarcados')}}"
         });
         $("#pax_embarcados").focus();
         return false;
         }
         
         if ($("#hora_llegada").val() == "") {
         Toast.fire({
         icon: "error",
         title: "{{__('Debe Ingresar La Hora de Llegada')}}"
         });
         $("#hora_llegada").focus();
         return false;
         }
         
         }
         
         if ($("#cobrar_tasa").val() == 1) {
         if ($("#hora_salida").val() == "") {
         Toast.fire({
         icon: "warning",
         title: "{{__('Debe Agregar la Hora de Salida')}}"
         });
         return false;
         }
         
         if ($("#pax_embarcados").val() == 0) {
         Toast.fire({
         icon: "warning",
         title: "{{__('Debe Agregar Los Pasajeros Embarcados')}}"
         });
         return false;
         }
         }
         
         if ($("#cobrar_dosa").val() == 1) {
         if ($("#hora_llegada").val() == "") {
         Toast.fire({
         icon: "warning",
         title: "{{__('Debe Agregar la Hora de Llegada')}}"
         });
         return false;
         }
         }
         
         if ($("#cobrar_tasa").val() == 0 && $("#cobrar_dosa").val() == 0) {
         
         Toast.fire({
         icon: "warning",
         title: "{{__('Debe Seleccionar Cobrar la Dosa o Tasa')}}"
         });
         return false;
         
         }
         
         
         if ($("#hora_llegada").val() == "") {
         f1 = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
         } else {
         f1 = moment($("#hora_llegada").val().substr(0, 10), "DD/MM/YYYY");
         }
         
         
         if ($("#hora_salida").val() == "") {
         f2 = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
         } else {
         f2 = moment($("#hora_salida").val().substr(0, 10), "DD/MM/YYYY");
         }
         
         
         f = moment($("#fecha_operacion").val(), "DD/MM/YYYY");
         
         
         if (f >= f1 && f <= f2) {
         
         
         if ($("#hora_llegada").val() != "" && $("#hora_salida").val() != "") {
         fi = moment($("#hora_llegada").val(), "DD/MM/YYYY hh:mm");
         ff = moment($("#hora_salida").val(), "DD/MM/YYYY hh:mm");
         if (fi > ff) {
         Toast.fire({
         icon: "error",
         title: "{{__('La Hora de Llegada no Puede Ser Mayor a la Hora de Salida')}}"
         });
         return false;
         }
         }
         } else {
         Toast.fire({
         icon: "error",
         title: "{{__('La Fecha de Vuelo No Esta en el Intervalo de Hora de Llegada y Hora de Salida')}}"
         });
         return false;
         }
         
         $("#prod_servd_remove").children().remove();
         $("#frmPrinc1").prepend(LOADING);
         $.post("{{route('get_total_serv')}}", $("#frmPrinc1").serialize(), function (response) {
         $(".overlay-wrapper").remove();
         tr = "";
         for (i in response.data) {
         tr += '<tr><td><span  onclick="removeServ(this, \'' + response.data[i].codigo + '\')" class="icon_remove fa fa-times"></span></td><td >' + response.data[i].cant + ' ' + response.data[i].nomenclatura + '</td><td  style="text-align:left">' + response.data[i].full_descripcion + '</td></tr>';
         }
         Swal.fire({
         title: 'Esta Seguro que Desea Guardar el Vuelo con los Siguientes Servicios?',
         html: '<table class="table table-sm"><thead><th colspan="2" style="width:100px">Cant.</th><th>Servicio</th></thead><tbody>' + tr + '</tbody></table>',
         icon: 'question',
         customClass: 'swal-wide',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: '<li class="fa fa-check"></li> Si',
         cancelButtonText: '<li class="fa fa-undo"></li> No'
         }).then((result) => {
         if (result.isConfirmed) {
         
         $("#frmPrinc1").prepend(LOADING);
         $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
         $(".overlay-wrapper").remove();
         Toast.fire({
         icon: response.type,
         title: response.message
         });
         $(".overlay-wrapper").remove();
         if (response.status == 1) {
         $("#frmPrinc1").trigger("reset");
         $("#prod_servd").html();
         $("#info").slideUp("fast");
         $("#piloto_id, #aeronave_id, #aeropuerto_id").trigger("change.select2");
         $("#fecha_operacion").val("");
         } else {
         
         }
         }).fail(function () {
         $(".overlay-wrapper").remove();
         Toast.fire({
         icon: "error",
         title: "{{__('Error al Guardar')}}"
         });
         });
         
         
         
         }
         });
         
         
         
         }).fail(function () {
         $(".overlay-wrapper").remove();
         Toast.fire({
         icon: "error",
         title: "{{__('Error al Guardar')}}"
         });
         });
         
         
         
         
         }
         }
         
         
         
         
         
         
         
         }
         */
    </script>

</div>