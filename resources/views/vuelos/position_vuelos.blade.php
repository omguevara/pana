




<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Agregar Posicion Vuelo ')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('get_vuelos')}}" id="backP" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["get_vuelos.position", $Vuelo->crypt_id],'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}



        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-4   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::text('aeropuerto_id',  $Vuelo->aeropuerto->full_nombre , [   "class"=>"form-control ", "disabled"=>"disabled"])}}

                        </div>


                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Tipo de Operaci&oacute;n:</b></span>
                            </div>
                            {{Form::text('tipo_operacion_id',  $Vuelo->tipo_operacion , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Nacionalidad:</b></span>
                            </div>
                            {{Form::text('tipo_vuelo_id',  $Vuelo->tipo_vuelo , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>

                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Op:</b></span>
                            </div>
                            {{Form::text('fecha_operacion',  $Vuelo->fecha_operacion2 , [   "class"=>"form-control ", "disabled"=>"disabled"])}}

                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora de Op:</b></span>
                            </div>
                            {{Form::text('hora_operacion',  $Vuelo->hora_operacion , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::text('aeronave_id',  $Vuelo->aeronave->full_nombre_tn , [   "class"=>"form-control ", "disabled"=>"disabled"])}}


                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::text('piloto_id',  $Vuelo->piloto->full_name , [   "class"=>"form-control ", "disabled"=>"disabled"])}}


                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Destino/Origen:</b></span>
                            </div>

                            {{Form::text('origen_destino_id',  ( isset($Vuelo->origen_destino->full_nombre) ? $Vuelo->origen_destino->full_nombre:"" ) , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text('cant_pasajeros',  $Vuelo->cant_pasajeros , [   "class"=>"form-control ", "disabled"=>"disabled"])}}
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Observación:</b></span>
                            </div>
                            {{Form::text('observaciones',  $Vuelo->observaciones , [   "class"=>"form-control ", "disabled"=>"disabled"])}}

                        </div>
                    </div>  
                </div>  
                <div   class="col-sm-12 col-md-8   ">
                    <div class="col-sm-12 col-md-12 text-right">
                        {{Form::button("<li class='fa fa-save'></li> Aplicar Puesto ", ["class"=>"btn btn-info", "onClick"=>"aplicarPuesto()"])}}
                    </div>
                    <div class="card-header p-0 pt-1">
                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                            @foreach($Aeropuerto->ubicaciones as $key=>$value)
                            <li class="nav-item">
                                <a class="nav-link {{($key==0? 'active':'')}}" id="tab-{{$value->crypt_id}}" data-toggle="pill" href="#content-tab-{{$value->crypt_id}}" role="tab" aria-controls="content-tab-{{$value->crypt_id}}" aria-selected="true">
                                    {{$value->nombre}}
                                </a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-one-tabContent">
                            @foreach($Aeropuerto->ubicaciones as $key=>$value)
                            <div class="tab-pane fade {{($key==0? 'show active':'')}}" id="content-tab-{{$value->crypt_id}}" role="tabpanel" aria-labelledby="tab-{{$value->crypt_id}}">
                                <div class="row">

                                    <div class="col-5 col-sm-3">
                                        <div class="nav flex-column nav-tabs h-100"  role="tablist" aria-orientation="vertical">
                                            @foreach($value->lugares as $key2=>$value2)
                                            <a class="nav-link {{($key2==0? 'active':'')}}" id="vert-tabs-{{$value2->crypt_id}}" data-toggle="pill" href="#vert-tabs-content-{{$value2->crypt_id}}" role="tab" aria-controls="vert-tabs-content-{{$value2->crypt_id}}" aria-selected="true">
                                                {{$value2->nombre}}
                                            </a>
                                            @endforeach

                                        </div>
                                    </div>
                                    <div class="col-7 col-sm-9">
                                        <div class="tab-content" >
                                            @foreach($value->lugares as $key2=>$value2)
                                            <div class="tab-pane text-left fade {{($key2==0? 'show active':'')}}" id="vert-tabs-content-{{$value2->crypt_id}}" role="tabpanel" aria-labelledby="vert-tabs-{{$value2->crypt_id}}">
                                                <div class="row">
                                                    @foreach($value2->puestos as $key3=>$value3)
                                                    @php
                                                    if ($value3->aeronave_id ==null ){
                                                    $click = "onClick=\"selectPuesto(this, '".$value3->crypt_id."', '".$Vuelo->aeronave->full_nombre."')\" ";
                                                    }else{
                                                    $click = '';
                                                    }
                                                    @endphp
                                                    <div {!!$click!!} style="{{($value3->aeronave_id ==null ? 'cursor:pointer':'')}}"    class="col-md-4 col-sm-6 col-12  {{$value3->aeronave_id==null ? 'free':''}}">
                                                        <div id="bg_{{$value3->crypt_id}}" class="info-box {{ ($value3->aeronave_id ==null ? 'bg-success':'bg-danger') }}">
                                                            <span class="info-box-icon"><i class="fas fa-plane"></i></span>

                                                            <div class="info-box-content">

                                                                <span class="info-box-number">{{$value3->nombre}}</span>

                                                                <div class="progress">
                                                                    <div class="progress-bar" style="width: 100%"></div>
                                                                </div>
                                                                <span id="description_{{$value3->crypt_id}}"  class="progress-description">

                                                                    {{ ($value3->aeronave_id ==null ? '':$value3->aeronave->full_nombre) }}
                                                                </span>
                                                            </div>
                                                            <!-- /.info-box-content -->
                                                        </div>
                                                        <!-- /.info-box -->
                                                    </div>

                                                    @if ($value3->aeronave_id ==null)
                                                    {{Form::radio("puesto", $value3->crypt_id, false, ["style"=>"display:none","class"=>"puesto_aplicado", "id"=>"puesto_".$value3->crypt_id])}}  
                                                    @endif
                                                    @endforeach    
                                                </div>
                                            </div>
                                            @endforeach

                                        </div>
                                    </div>

                                </div>



                            </div>
                            @endforeach




                        </div>
                    </div>


                </div>


            </div>  

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>





    <script type="text/javascript">

        $(document).ready(function () {


            $("#anular").on("click", function () {

                Swal.fire({
                    title: 'Esta Seguro que Desea Anular el Vuelo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {



                        $("#frmPrinc1").prepend(LOADING);
                        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                            $(".overlay-wrapper").remove();


                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $(".overlay-wrapper").remove();

                            if (response.status == 1) {

                                $("#frmPrinc1").trigger("reset");
                                $("#backP").click();


                            } else {

                            }





                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });

            });


        });
        function selectPuesto(obj, id, nave) {
            $("#puesto_" + id).prop("checked", true);
            $(".free").find(".progress-description").html("");
            $(".free").find(".bg-warning").removeClass("bg-warning").addClass('bg-success');
            $("#description_" + id).html(nave);
            $("#bg_" + id).removeClass("bg-success").addClass('bg-warning');
        }

        function aplicarPuesto() {
            Swal.fire({
                title: 'Esta Seguro que Desea Aplicar el Puesto?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                    if ($(".puesto_aplicado:checked").length > 0) {
                        $("#frmPrinc1").prepend(LOADING);
                        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $(".overlay-wrapper").remove();

                            $("#modal-add-pos").modal("hide");
                            $("#backP").click();

                            //console.log(response);
                        }).fail(function () {
                            Toast.fire({
                                icon: "error",
                                title: "Error al Guardar"
                            });
                            $(".overlay-wrapper").remove();
                        });
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "No Ha seleccionado el Puesto"
                        });
                    }
                }
            });
        }

    </script>

</div>

