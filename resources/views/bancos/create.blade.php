

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Banco')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('bancos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"bancos.create", 'files'=>'true',  'id'=>'frmCreateBanco','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">

                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Codigo:</b></span>
                    </div>

                    {{Form::text("codigo", "", ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos ", "id"=>"codigo", "maxlength"=>"4", "placeholder"=>__('Codigo del Banco')])}}
                </div>
               
                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Nombre:</b></span>
                    </div>

                    {{Form::text("nombre", "", ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos ", "id"=>"nombre", "placeholder"=>__('Nombre del Banco')])}}
                </div>
               


            </div>
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {


        $(".select2").select2();
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $('.number').numeric({negative: false});
        $('#frmCreateBanco').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmCreateBanco').valid()) {
                $("#frmCreateBanco").prepend(LOADING);
                var formData = new FormData(document.getElementById("frmCreateBanco"));


                $.ajax({
                    url: this.action,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('bancos')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmCreateBanco").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmCreateBanco").find(".overlay-wrapper").remove();
                        }
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });


            }
            return false;
        });

        $('#frmCreateBanco').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
