

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Activar Banco')}}</h3>

            <div class="card-tools">
                 <a type="button"  href="{{route('bancos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["bancos.active", $Bancos->crypt_id],  'id'=>'frmEnBanco','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Codigo:</b></span>
                    </div>
                    {{Form::text("codigo", $Bancos->codigo, ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos ", "id"=>"codigo", "disabled"=>"disabled", "placeholder"=>__('Codigo del Banco')])}}
                </div>
                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Nombre')}}:</b></span>
                    </div>

                    {{Form::text("nombre", $Bancos->nombre, ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos ", "id"=>"nombre", "disabled"=>"disabled", "placeholder"=>__('Nombre del Banco')])}}
                </div>
            </div>
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-check-circle"></i> '.__("Active"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#frmEnBanco').on("submit", function (event) {
            event.preventDefault();

            $("#frmEnBanco").prepend(LOADING);
            $.post(this.action, $("#frmEnBanco").serialize(), function (response) {
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $.get("{{route('bancos')}}", function (response) {
                        $("#main-content").html(response);

                    }).fail(function (xhr) {
                        $("#frmEnBanco").find(".overlay-wrapper").remove();
                    });
                } else {
                    $("#frmEnBanco").find(".overlay-wrapper").remove();
                }
                //console.log(response);
            }).fail(function () {
                $("#frmEnBanco").find(".overlay-wrapper").remove();
            });

            return false;
        });

        $('#frmEnBanco').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
