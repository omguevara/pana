



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturación de Aviación General')}}</h3>

            <div class="card-tools">



            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"facturacion_aviacion_general",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-4   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, (count($Aeropuertos)==1 ? key($Aeropuertos):"" ), ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 uno", "id"=>"aeropuerto_id" ,"required"=>"required"], $AeropuertosProp)}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Nacionalidad:</b></span>
                            </div>
                            {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], 1, ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"nacionalidad_id" ,"required"=>"required"])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, "", ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div  title="Agregar Nuevo Piloto" onClick="$('#modal-princ-pilotos').modal('show')" class="input-group-text">



                                    <i class="fa fa-plus"></i>

                                </div>
                            </div>
                        </div>
                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required uno", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Vuelo (Formato 24H):</b></span>
                            </div>

                            {{Form::text("hora_vuelo", date("H:m"), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_vuelo", "placeholder"=>__('Hora Vuelo')])}}    
                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", "", ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number uno ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                        </div>

                        <div  class="input-group mt-2 time_estadia">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Tasa?:</b></span>
                            </div>
                            {{Form::select('cobrar_tasa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_tasa" ,"required"=>"required"])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Dosa?:</b></span>
                            </div>
                            {{Form::select('cobrar_dosa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_dosa" ,"required"=>"required"])}}
                        </div>









                    </div>  
                    <div class="rows text-right mt-2 ">
                        
                        {{Form::button('<li class="fa fa-plus"></li> '.__("Servicios").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addProds"])}}    
                        
                        {{Form::button(__("Ver Presupuesto").' <li class="fa fa-caret-right"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"next1"])}}    
                    </div>
                </div>  
                <div   class="col-sm-12 col-md-4   ">
                    <h2>PROFORMA</h2>
                    <table style="width:100%" id="datFactura">
                        <tr>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                            <td style="width:50%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>

                    </table>





                </div>
                <div  class="col-sm-12 col-md-4   ">
                    

                    <h4 class="">Datos para la Factura</h4>

                    


                    <div class="row">
                        <div  class="input-group  ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>N&deg; de Factura:</b></span>
                            </div>

                            {{Form::text("nro_factura", "", ["readonly"=>"readonly", "class"=>"form-control  required ", "id"=>"nro_factura", "placeholder"=>__('Número de Factura')])}}    
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>N&deg; de Control:</b></span>
                            </div>
                            {{Form::text("nro_control", "", ["readonly"=>"readonly", "class"=>"form-control  required ", "id"=>"nro_control", "placeholder"=>__('Número de Control')])}}    
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Documento:</b></span>
                            </div>
                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", ["style"=>"max-width:80px", "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                            {{Form::text("document", "", ["onBlur"=>"findDato2()", "data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required dos", "id"=>"document", "placeholder"=>__('Document')])}}    
                            <div class="input-group-append" >
                                <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Responsable:</b></span>
                            </div>

                            {{Form::text("razon", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos alpha", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                            </div>

                            {{Form::text("phone", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone dos", "id"=>"phone", "placeholder"=>__('Phone')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Correo')}}:</b></span>
                            </div>

                            {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  dos", "id"=>"correo", "placeholder"=>__('Correo')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                            </div>

                            {{Form::text("direccion", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                        </div>






                    </div>
                    <div class="rows text-right mt-2 ">
                        {{Form::button(__("Siguiente").' <li class="fa fa-caret-right"></li>',  ["onClick"=>"Facturar()", "type"=>"button", "class"=>"btn btn-primary", "id"=>"next2"])}}    
                    </div>
                </div>
            </div>  
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>


    <div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nuevo Piloto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"facturacion_aviacion_general.add_piloto",  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                <div id="modalPrincBodyFact" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="type_document">{{__('Tipo de Documento')}}</label>
                                {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>
                                {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="name_user">{{__('Name')}}</label>
                                {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="surname_user">{{__('Surname')}}</label>
                                {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                            </div>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"facturacion_aviacion_general.add_nave",  'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6" >
                            <div class="form-group">
                                <label for="matricula">{{__('Matrícula')}}</label>
                                {{Form::text("matricula", "", ["required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="nombre">{{__('Modelo')}}</label>
                                {{Form::text("nombre", "", ["required"=>"required", "maxlength"=>"20" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                                {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    
     <div class="modal fade" id="modal-princ-serv-extra"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalExt" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Agregar Servicios Extra</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"pospago_aviacion_general",  'id'=>'frmServ','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-12" >
                            
                            <table style="width:100%" id="tableServExt">
                                <tr>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                    <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                                    <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                                </tr>

                            </table>
                        
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicar"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>






    <script type="text/javascript">

        var TOTAL_FACTURA;



        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });
            
            
            $("#addProds").on("click", function () {
                if ($(".uno").valid() ) {
                    
                    $("#modal-princ-serv-extra").modal();
                    $("#modalExt").prepend(LOADING);
                    $("#prod_servd").html("");
                    $.post("{{route('facturacion_aviacion_general.get_serv_extra')}}", $("#frmPrinc1").serialize(), function (response){
                        $(".overlay-wrapper").remove();
                        $(".filaServExt").remove();
                        sum = 0;
                        base_imponible = 0;
                        excento = 0;
                        iva = 0;
                        for (i in response.data) {
                            sum += parseFloat(response.data[i]['precio'], 10);
                            mult = parseFloat(response.data[i]['precio'], 10);
                            cant = response.data[i]['cant'];

                            if (response.data[i]['iva_aplicado'] > 0) {
                                base_imponible += mult;
                                iva += mult * response.data[i]['iva_aplicado'] / 100;
                            } else {
                                excento += parseFloat(mult, 10);

                            }
                            
                            
                            tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="'+response.data[i]['crypt_id']+'" /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input type="number" id="'+response.data[i]['crypt_id']+'" onChange="UpItemSelect()" value="0" class="form-control" disabled /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + ' ' + response.data[i]['iva2'] + '</td>';
                            
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                            tabla += '</tr>';
                            $("#tableServExt").append(tabla);
                        }
                        
                        
                    }).fail(function (){
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar los Servicios"
                        });
                    });
                    

                }
            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('.time').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $('#iconDate').datetimepicker({
                format: 'L'
            });
            $('.number').numeric({negative: false});

            $("#modal-fact").on('shown.bs.modal', function () {
                $("#totalEuro").html(muestraFloat(TOTAL_FACTURA, 2));
                $("#totalBs").html(muestraFloat(TOTAL_FACTURA * VALOR_EURO, 2));
            });
            
            $("#nro_factura").val(  $("#aeropuerto_id").children(":selected").data("numero_factura")  );
            $("#nro_control").val(  $("#aeropuerto_id").children(":selected").data("numero_control")  );
            
            $("#aeropuerto_id").on("change", function (){
                $("#nro_factura").val(  $(this).children(":selected").data("numero_factura")  );
                $("#nro_control").val(  $(this).children(":selected").data("numero_control")  );
                
            });

            $("#next1").on("click", function () {
                if ($(".uno").valid()) {
                    $("#panel_princ").prepend(LOADING);
                    $.post("{{route('facturacion_aviacion_general.get_serv')}}", $(".uno, [name=_token]").serialize(), function (response) {

                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $(".filaFactura").remove();
                        sum = 0;
                        base_imponible = 0;
                        excento = 0;
                        iva = 0;
                        for (i in response.data) {
                            //console.log(response.data[i]['categoria_aplicada']);

                            if (response.data[i]['codigo'] == '2.5.1.1') {//SI ES TASA
                                sum += parseFloat(response.data[i]['precio'], 10);
                                mult = response.data[i]['precio'];
                                cant = $("#cant_pasajeros").val();
                            } else {

                                sum += parseFloat(response.data[i]['cant'] * response.data[i]['precio']);
                                mult = response.data[i]['cant'] * response.data[i]['precio'];
                                cant = response.data[i]['cant'];


                            }
                            if (response.data[i]['iva_aplicado'] > 0) {
                                base_imponible += mult;
                                iva += mult * response.data[i]['iva_aplicado'] / 100;
                            } else {
                                excento += parseFloat(mult, 10);

                            }

                            //sum += response.data[i]['cant'] * response.data[i]['precio'] * response.data[i]['categoria_aplicada'] ;
                            tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaFactura">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + cant + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + ' ' + response.data[i]['iva2'] + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (cant + ' ' + response.data[i]['nomenclatura'] || '') + '</td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                            tabla += '</tr>';
                            $("#datFactura").append(tabla);
                        }
                        TOTAL_FACTURA = sum;
                        MontoGlobalBs = sum * VALOR_EURO;
                        tabla = '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>BASE IMPONIBLE</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>EXCENTO</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                        tabla += '</tr>';



                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>SUB TOTAL</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>IVA</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                        tabla += '</tr>';

                        tabla += '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="3"><b>TOTAL</b></td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                        tabla += '</tr>';

                        $("#datFactura").append(tabla);

                    }, 'json').fail(function () {
                        $(".overlay-wrapper").remove();
                    });

                } else {

                }
            });

            $("#next2").on("click", function () {
                if ($(".uno").valid() && $(".dos").valid()) {
                    $("#modal-fact").modal();

                } else {

                }
            });

            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    $("#frmPrinc1").prepend(LOADING);
                    $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('aerolineas')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                        //console.log(response);
                    }).fail(function () {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


            $('#frmPilotos').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });



            $('#frmPilotos').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPilotos').valid()) {
                    $("#frmPilotos").prepend(LOADING);
                    $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmPilotos").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#piloto_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                            $("#piloto_id").val(response.data.id);
                            $("#piloto_id").trigger("reset");
                            $('#frmPilotos').trigger("reset");
                            $("#modal-princ-pilotos").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    $("#frmNave").prepend(LOADING);
                    $.post(this.action, $("#frmNave").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmNave").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#aeronave_id").append('<option  value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                            $("#aeronave_id").val(response.data.id);
                            $("#aeronave_id").trigger("reset");
                            $('#frmNave').trigger("reset");
                            $("#modal-princ-aeronave").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });


            $('#document').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    findDato2();
                }
            });

        });



        function findDato2() {


            if ($("#type_document, #document").valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#cliente_id").val(response.data.crypt_id);
                        $("#razon").val(response.data.razon_social);
                        $("#phone").val(response.data.telefono);
                        $("#correo").val(response.data.correo);
                        $("#direccion").val(response.data.direccion);
                        $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#razon").focus();
                        $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                        $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                    $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }


        }

        function clsFP() {
            $(".money").val("0,00");
        }

        function clsPOS() {
            $(".pos").val("");
        }

        function getMonto() {
            T = 0;
            $(".money").each(function () {
                T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
            });
            return T;
            // muestra

        }

        function Facturar() {


            //porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
            //if (porcentaje >= 98) {

            if ($("#frmPrinc1").valid()) {



                Swal.fire({
                    title: 'Esta Seguro que Desea Procesar la Factura?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-fact").modal('hide');
                        $("#main-content").prepend(LOADING);
                        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                            $(".overlay-wrapper").remove();
                                // Swal.close();
                                $("#modal-fact").modal('hide');
                                $("#main-content").html(response);
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $(".overlay-wrapper").remove();
                            if (response.status == 1) {
                                
                            } else {

                            }





                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });









            }
            /*} else {
             Toast.fire({
             icon: "error",
             title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
             });
             $(".money").eq(0).focus();
             }*/
        }

    </script>

</div>