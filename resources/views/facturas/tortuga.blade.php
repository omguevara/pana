



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registrar Vuelo a la Tortuga')}}</h3>

            <div class="card-tools">

                <button type="button"  onClick="backPrinc()" class="btn btn-tool" ><i class="fas fa-undo"></i>regresar</button>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"aviacion_general_tortuga.create",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
             
                <div   class="col-sm-12 col-md-6   ">
                    <h2>PROFORMA</h2>
                    <table style="width:100%" id="datFactura">
                        <tr>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                            <td style="width:50%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>

                    </table>





                </div>
                <div  class="col-sm-12 col-md-6   ">
                    <h4 class="">Responsable de Pago</h4>
                    <div class="row">

                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Documento:</b></span>
                            </div>
                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", ["style"=>"max-width:80px", "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                            {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required dos", "id"=>"document", "placeholder"=>__('Document')])}}    
                            <div class="input-group-append" >
                                <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Responsable:</b></span>
                            </div>

                            {{Form::text("razon", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                            </div>

                            {{Form::text("phone", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone dos", "id"=>"phone", "placeholder"=>__('Phone')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Correo')}}:</b></span>
                            </div>

                            {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  dos", "id"=>"correo", "placeholder"=>__('Correo')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                            </div>

                            {{Form::text("direccion", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                        </div>






                    </div>
                    <div class="rows text-right mt-2 ">
                        {{Form::button(__("Siguiente").' <li class="fa fa-caret-right"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"next2"])}}    
                    </div>
                </div>
            </div>  
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
</div>



<div class="modal fade" id="modal-princ-prodservs"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        <div  class="modal-content">
            <div class="modal-header">
                <h4 class="">Agregar Servicios Extras</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general_tortuga",  'id'=>'frmServ','autocomplete'=>'Off'])}}
            <div  class="modal-body">

                <div class="row">

                </div>
            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Aceptar"),  ["type"=>"button", "class"=>"btn btn-primary", ])}}    
            </div>
            {{ Form::close() }} 

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Facturar</h4>

            </div>
            {{Form::open(array( "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frmAddFact2"))}}

            <div id="modalPrincBodyFact" class="modal-body">






                <div class="row">

                    <div class="col-sm-12 col-md-12  ">

                        <div class="row">
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-3"><h4>Total Euro:</h4></div>
                                    <div class="col-md-3 text-right "><h5 id="totalEuro" >0,00</h5></div>

                                    <div class="col-md-3"><h4>Total Bs:</h4></div>
                                    <div class="col-md-3 text-right "><h5 id="totalBs" >0,00</h5></div>
                                </div>
                            </div>
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-7"><h4>Número de Factura:</h4></div>
                                    <div class="col-md-5 text-right "><h3  >{{showCode($TAQUILLA->numero_factura)}}</h3></div>
                                </div>
                            </div>
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-7 "><h4>Número de Control:</h4></div>
                                    <div class="col-md-5 text-right "><h3 >{{showCode($TAQUILLA->numero_control)}}</h3></div>
                                </div>
                            </div>


                        </div>
                    </div>



                    <div class="col-sm-12 col-md-12 border bg-info">
                        <h4 class="text-center">Forma de Pago

                            <button type="button" onClick="clsFP()" title="{{__('Borrar Formas de Pago')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                        </h4>    
                        <div class="row">
                            @foreach($forma_pagos as $key =>$value)


                            <div class="col-sm-12 col-md-2 ">
                                <div class="form-group">
                                    <label for="">{{$value->nombre}}</label>
                                    {{Form::text("money[".$value->crypt_id."]", "0,00", ["data-reverso"=>$value->reverso_formula, "data-formula"=>$value->calculo_formula, "class"=>"text-right form-control money"])}}

                                </div>
                            </div> 


                            @endforeach
                        </div> 
                    </div> 

                    <div class="col-sm-6 col-md-6 border bg-success">
                        <h4 class="text-center">Referencias

                            <button type="button" onClick="clsPOS()" title="{{__('Borrar Monto')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                        </h4>    
                        <div class="row">
                            @foreach($TAQUILLA->puntos as $key =>$value)


                            <div class="col-sm-12 col-md-4 ">
                                <div class="form-group">
                                    <label for="">{{$value->banco->nombre.' ('.$value->serial.')'}}</label>
                                    {{Form::text("pos[".$value->crypt_id."]", "", [ "class"=>"text-right form-control pos"])}}

                                </div>
                            </div> 


                            @endforeach
                        </div> 
                    </div>
                    <div class="col-sm-6 col-md-6 border bg-success">

                        <div class="row">

                            <div class="col-sm-12 col-md-12 ">
                                <div class="form-group">
                                    <label for="">Observación</label>
                                    {{Form::textarea("observacion", "", [ 'rows' =>4,  "class"=>" form-control "])}}

                                </div>
                            </div> 



                        </div> 
                    </div>



                </div>




            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrinc" onClick="$('#frm2').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button type="button"  id="setModalPrinc"   onClick="Facturar()"                            class="btn btn-primary float-right">Facturar</button>


            </div>
            {{Form::close()}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>





<script type="text/javascript">

    var TOTAL_FACTURA;



    $(document).ready(function () {
        $(".select2").select2();

        $(document).on('select2:open', () => {
            document.querySelector('.select2-search__field').focus();
        });
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $('.time').inputmask({alias: "datetime", inputFormat: "HH:MM"});
        $('#iconDate').datetimepicker({
            format: 'L'
        });
        $('.number').numeric({negative: false});

        $("#modal-fact").on('shown.bs.modal', function () {
            $("#totalEuro").html(muestraFloat(TOTAL_FACTURA, 2));
            $("#totalBs").html(muestraFloat(TOTAL_FACTURA * VALOR_EURO, 2));
        });
        $(".money").on("focus", function () {
            if (usaFloat(this.value) == 0) {
                T = 0;
                $(".money").each(function () {
                    T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
                });
                // muestra
                v = MontoGlobalBs - T;
                this.value = muestraFloat(eval(getValor(this.dataset.reverso, v)));
            }
        });

        $("#next1").on("click", function () {
            if ($(".uno").valid()) {
                $("#panel_princ").prepend(LOADING);
                $.post("{{route('aviacion_general_tortuga.get_serv')}}", $(".uno, [name=_token]").serialize(), function (response) {

                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $(".filaFactura").remove();
                    sum = 0;
                    for (i in response.data) {
                        sum += response.data[i]['cant'] * response.data[i]['precio'];
                        tabla = '<tr class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['cant'] + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (response.data[i]['nomenclatura'] || '') + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(response.data[i]['cant'] * response.data[i]['precio']) + '</td>';
                        tabla += '</tr>';
                        $("#datFactura").append(tabla);
                    }
                    TOTAL_FACTURA = sum;
                    MontoGlobalBs = sum * VALOR_EURO;
                    tabla = '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="3"><b>TOTAL EURO</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                    tabla += '</tr>';
                    $("#datFactura").append(tabla);

                }, 'json').fail(function () {
                    $(".overlay-wrapper").remove();
                });

            } else {

            }
        });

        $("#next2").on("click", function () {
            if ($(".uno").valid() && $(".dos").valid()) {
                $("#modal-fact").modal();

            } else {

            }
        });

        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('aerolineas')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });


        $('#frmPilotos').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $('#frmNave').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $('#frmArrive').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });


        $('#frmPilotos').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPilotos').valid()) {
                $("#frmPilotos").prepend(LOADING);
                $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $("#frmPilotos").find(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#piloto_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                        $("#piloto_id").val(response.data.id);
                        $("#piloto_id").trigger("reset");
                        $('#frmPilotos').trigger("reset");
                        $("#modal-princ-pilotos").modal('hide');
                    }
                    //console.log(response);
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmNave').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmNave').valid()) {
                $("#frmNave").prepend(LOADING);
                $.post(this.action, $("#frmNave").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $("#frmNave").find(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#aeronave_id").append('<option  value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                        $("#aeronave_id").val(response.data.id);
                        $("#aeronave_id").trigger("reset");
                        $('#frmNave').trigger("reset");
                        $("#modal-princ-aeronave").modal('hide');
                    }
                    //console.log(response);
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });


        $('#document').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                findDato2();
            }
        });

    });


    function backPrinc() {
        $("#main-content").html(LOADING);
        AJAX_ACTIVE = true;
        $.get("{{route('aviacion_general_tortuga')}}", function (response) {
            AJAX_ACTIVE = false;
            $("#main-content").html(response);

        }, 'html').fail(function (xhr) {
            $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
            AJAX_ACTIVE = false;
        });
    }

    function findDato2() {


        if ($("#type_document, #document").valid()) {
            $("#frmPrinc1").prepend(LOADING);
            $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#cliente_id").val(response.data.crypt_id);
                    $("#razon").val(response.data.razon_social);
                    $("#phone").val(response.data.telefono);
                    $("#correo").val(response.data.correo);
                    $("#direccion").val(response.data.direccion);
                    $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#razon").focus();
                    $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                    $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


    }

    function clsFP() {
        $(".money").val("0,00");
    }

    function clsPOS() {
        $(".pos").val("");
    }

    function getMonto() {
        T = 0;
        $(".money").each(function () {
            T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
        });
        return T;
        // muestra

    }

    function Facturar() {


        porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
        if (porcentaje >= 98) {

            if ($("#frmAddFact2").valid()) {



                Swal.fire({
                    title: 'Esta Seguro que Desea Procesar la Factura?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-fact").modal('hide');
                        $("#main-content").prepend(LOADING);
                        $.post($("#frmAddFact2").attr("action"), $("#frmPrinc1").serialize() + '&' + $("#frmAddFact2").serialize(), function (response) {
                            $(".overlay-wrapper").remove();
                            // Swal.close();



                            //$("#modal-fact").modal('hide');
                            $("#main-content").html(response);



                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });









            }
        } else {
            Toast.fire({
                icon: "error",
                title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
            });
            $(".money").eq(0).focus();
        }
    }

</script>
