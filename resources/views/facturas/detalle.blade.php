




<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Facturas')}}</h3>

            <div class="card-tools">
                <button type="button"  onClick="goBack()" class="btn btn-tool" ><i class="fas fa-undo"></i>Regresar</button>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            {{Form::open(["route"=>"facturas",  'id'=>'frmAddFact','autocomplete'=>'Off'])}}
            {{Form::hidden('tipo', $tipo)}}
            <div class="row">
                <div class="col-sm-12 col-md-8 border-right ">
                    <div class="row">
                        <div class="col-sm-12 col-md-2 ">

                            <div class="form-group">
                                <label for="cant">{{__('Cantidad')}}</label>
                                {{Form::number("cant",  "1", [  "class"=>"form-control",  "min"=>'1', "step"=>"1" ,"id"=>"cant"])}}    

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-10 ">

                            <div class="form-group">
                                <label for="prodservs_id">{{__('Servicios Adicionales')}}</label>
                                {{Form::select("prodservs_id", $listPrdServs, "", [  "class"=>"form-control  select2  ", "placeholder"=>"Seleccione" ,"id"=>"prodservs_id"])}}    

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12 text-center">
                            {{Form::button("<li class='fas fa-save'></li> Aplicar Servicios", ["class"=>"btn btn-success", "onClick"=>"aplicarServs()"])}}

                        </div>

                        <div class="col-sm-12 col-md-12 ">
                            <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
                                <tr>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="7" ><strong>DETALLES DE LA FACTURA</strong></td>
                                </tr>

                                <tr>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="5%" ><strong>OP.</strong></td>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="7%" ><strong>CANT.</strong></td>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="10%"><strong>CÓDIGO</strong></td>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="30%"><strong>CONCEPTO</strong></td>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="11%"><strong>NOMENCLATURA</strong></td>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="19%"><strong>MONTO BOLIVARES</strong></td>
                                    <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="18%"><strong>MONTO {{$MonedaPago}}</strong></td>
                                </tr>
                                @php
                                $base_imponible = 0; // TODO LO QUE TENGA IVA
                                $excento = 0; // TODO LO QUE NO TENGA IVA
                                $sub_total = 0; // SUMAR TODO
                                $iva = 0; 
                                @endphp
                                @foreach($prodserv as $value)
                                @php
                                $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                                $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                                $sub_total += ($value['precio']*$value['categoria_aplicada']);
                                $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
                                @endphp
                                <tr>
                                    <td style="width:5%; text-align:center" ></td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >1</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:8%; text-align:center" >{{$value['codigo']}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width: 52%"  >{{$value['descripcion'].' '.$value['iva2']}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" ></td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right" >{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;" align="right" >{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
                                </tr>
                                @endforeach
                                
                                @foreach($prodservicios_extra as $value)
                                @php
                                $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$cantidades[$value["crypt_id"]]):0); 
                                $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']*$cantidades[$value["crypt_id"]]):0); 
                                $sub_total += ($value['precio']*$value['categoria_aplicada']*$cantidades[$value["crypt_id"]]);
                                $iva +=  ($value['iva_aplicado'] > 0 ? ($cantidades[$value["crypt_id"]]*$value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
                                @endphp
                                <tr>
                                     <td style="width:5%; text-align:center" ><li onClick="quitProdservs('{{$id}}', '{{$value["crypt_id"]}}', '{{$tipo}}')" class="fa fa-times" style="cursor:pointer"></li></td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >{{$cantidades[$value["crypt_id"]]}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:8%; text-align:center" >{{$value['codigo']}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width: 57%"  >{{$value['descripcion'].' '.$value['iva2']}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" ></td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right" >{{muestraFloat($value['bs']*$value['categoria_aplicada']*$cantidades[$value["crypt_id"]])}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;" align="right" >{{muestraFloat($value['precio']*$value['categoria_aplicada']*$cantidades[$value["crypt_id"]])}}</td>
                                </tr>
                                @endforeach




                                <tr>    

                                    <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">BASE IMPONIBLE</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($base_imponible* $EURO)  }}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($base_imponible)}}</td>
                                </tr>

                                <tr>
                                    <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">EXENTO</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($excento* $EURO)}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($excento)}}</td>


                                </tr>

                                <tr>

                                    <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">SUBTOTAL</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($sub_total* $EURO)}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($sub_total)}}</td>


                                </tr>

                                <tr>
                                    <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">DESCUENTO</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat(0)}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat(0)}}</td>


                                </tr>

                                <tr>
                                    <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">IVA (16,00%)</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($iva* $EURO)}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($iva)}}</td>



                                </tr>

                                <tr>

                                    <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">TOTAL</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat(($sub_total+$iva)* $EURO)}}</td>
                                    <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($sub_total+$iva)}}</td>



                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 ">

                    <div class="row border-bottom">

                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>


                            </div>
                        </div>

                        <div class="col-sm-12 col-md-8">

                            <div class="form-group">

                                <div class="input-group " >
                                    <div class="input-group-prepend" >
                                        {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "J", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                    </div>
                                    {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                    <div class="input-group-append" >
                                        <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>  






                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="razon">{{__('Razón Social')}}</label>


                            </div>
                        </div>


                        <div class="col-sm-12 col-md-8">
                            <div class="form-group">

                                {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Razón Social')])}}    

                            </div>
                        </div>  


                        <div class="col-sm-12 col-md-4">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>


                            </div>
                        </div>     
                        <div class="col-sm-12 col-md-8">

                            <div class="form-group">

                                {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  


                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="correo">{{__('Correo')}}</label>


                            </div>  
                        </div> 
                        <div class="col-sm-12 col-md-8">
                            <div class="form-group">

                                {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    

                            </div>  
                        </div> 

                        <div class="col-sm-12 col-md-4">

                            <div class="form-group">
                                <label for="direccion">{{__('Dirección')}}</label>

                            </div>  
                        </div>

                        <div class="col-sm-12 col-md-8">

                            <div class="form-group">

                                {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                            </div>  
                        </div>

                        <div class="col-sm-12 col-md-4">

                            <button type="button" onClick="goBack()" class="btn btn-block btn-warning btn-lg"><li class="fa fa-undo"></li> Regresar</button>

                        </div>
                        <div class="col-sm-12 col-md-8">

                            <button onClick="nextStep()" type="button" class="btn btn-block btn-success btn-lg"><li class="fa fa-file-invoice-dollar"></li> Facturar</button>

                        </div>


                    </div>



                </div>


            </div>

            {{ Form::close() }} 

        </div>
    </div>
</div>


<div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Facturar</h4>

            </div>
            {{Form::open(array("route"=>["facturas.create", $id], "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frmAddFact2"))}}

            <div id="modalPrincBodyFact" class="modal-body">






                <div class="row">

                    <div class="col-sm-12 col-md-12  ">

                        <div class="row">
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-6"><h4>Total Bs:</h4></div>
                                    <div class="col-md-6 text-right "><h3 id="totalBsF" >0,00</h3></div>
                                </div>
                            </div>
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-7"><h4>Número de Factura:</h4></div>
                                    <div class="col-md-5 text-right "><h3  >{{showCode($TAQUILLA->numero_factura)}}</h3></div>
                                </div>
                            </div>
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-7 "><h4>Número de Control:</h4></div>
                                    <div class="col-md-5 text-right "><h3 >{{showCode($TAQUILLA->numero_control)}}</h3></div>
                                </div>
                            </div>


                        </div>
                    </div>

          

                    <div class="col-sm-12 col-md-12 border bg-info">
                        <h4 class="text-center">Forma de Pago

                            <button type="button" onClick="clsFP()" title="{{__('Borrar Formas de Pago')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                        </h4>    
                        <div class="row">
                            @foreach($forma_pagos as $key =>$value)


                            <div class="col-sm-12 col-md-2 ">
                                <div class="form-group">
                                    <label for="">{{$value->nombre}}</label>
                                    {{Form::text("money[".$value->crypt_id."]", "0,00", ["data-reverso"=>$value->reverso_formula, "data-formula"=>$value->calculo_formula, "class"=>"text-right form-control money"])}}

                                </div>
                            </div> 


                            @endforeach
                        </div> 
                    </div> 

                    <div class="col-sm-6 col-md-6 border bg-success">
                        <h4 class="text-center">Referencias

                            <button type="button" onClick="clsPOS()" title="{{__('Borrar Monto')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                        </h4>    
                        <div class="row">
                            @foreach($TAQUILLA->puntos as $key =>$value)


                            <div class="col-sm-12 col-md-4 ">
                                <div class="form-group">
                                    <label for="">{{$value->banco->nombre.' ('.$value->serial.')'}}</label>
                                    {{Form::text("pos[".$value->crypt_id."]", "", [ "class"=>"text-right form-control pos"])}}

                                </div>
                            </div> 


                            @endforeach
                        </div> 
                    </div>
                    <div class="col-sm-6 col-md-6 border bg-success">
                            
                        <div class="row">
                            
                            <div class="col-sm-12 col-md-12 ">
                                <div class="form-group">
                                    <label for="">Observación</label>
                                    {{Form::textarea("observacion", "", [ 'rows' =>4,  "class"=>" form-control "])}}

                                </div>
                            </div> 


                            
                        </div> 
                    </div>



                </div>




            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrinc" onClick="$('#frm2').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button type="button"  id="setModalPrinc"   onClick="Facturar()"                            class="btn btn-primary float-right">Facturar</button>


            </div>
            {{Form::close()}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<script type="text/javascript">
    var MontoGlobal = 0;
    var MontoGlobalBs = parseFloat("{{(($sub_total+$iva)* $EURO)}}", 10);
    $(document).ready(function () {
        $(".select2").select2();
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $('.number').numeric({negative: false});
        $('#frmAddFact').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        $('#frmAddFact2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        
        $("#modal-fact").on('shown.bs.modal', function () {
            $("#totalBsF").html(muestraFloat(MontoGlobalBs, 2));
            


        });

        $(".money").on("focus", function () {
            //MontoGlobalBs

            if (usaFloat(this.value) == 0) {
                T = 0;
                $(".money").each(function () {
                    T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
                });
                // muestra
                v = MontoGlobalBs - T;

                this.value = muestraFloat(eval(getValor(this.dataset.reverso, v)));





            }





        });



    });

    function goBack() {
        $("#main-content").prepend(LOADING);
        $.get("{{route('facturas')}}", function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });
    }

    function aplicarServs() {
        if ($("#prodservs_id").val() != "") {
            if ($("#cant").val() >0) {
                $("#main-content").prepend(LOADING);
                $.post("{{route('facturas.add_prodservs', $id)}}", $("#frmAddFact").serialize(), function (response) {
                    $("#main-content").html(response);
                    $(".overlay-wrapper").remove();


                }).fail(function () {

                    $(".overlay-wrapper").remove();

                });
            }else{
                Toast.fire({
                    icon: "error",
                    title: "{{__('La Cantidad debe Ser Mayor o Igual a Uno')}}"
                });
            }
        }else{
            Toast.fire({
                icon: "error",
                title: "{{__('Debe seleccionar un Servicio')}}"
            });
        }

    }
    function goFact() {
        $("#main-content").prepend(LOADING);
        $.get("{{route('facturas.invoice', $id)}}", function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });
    }

    function nextStep() {
        if ($("#frmAddFact").valid()) {
            $("#modal-fact").modal("show");
        }
    }


    function findDato() {


        if ($("#type_document, #document").valid()) {
            
            $("#main-content").prepend(LOADING);   
            $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#cliente_id").val(response.data.crypt_id);
                    $("#razon").val(response.data.razon_social);
                    $("#phone").val(response.data.telefono);
                    $("#correo").val(response.data.correo);
                    $("#direccion").val(response.data.direccion);
                    $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {

                    $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                    $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#razon").focus();

                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


    }

    function clsFP() {
        $(".money").val("0,00");
    }

    function clsPOS() {
        $(".pos").val("");
    }
    function quitProdservs(id, serv, tipo){
        $("#main-content").prepend(LOADING);
        $.get("{{url('/del-prodservs/')}}/"+id+"/"+serv+"/"+tipo, function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });
    }
    
    
    
    function getMonto() {
        T = 0;
        $(".money").each(function () {
            T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
        });
        return T;
        // muestra

    }
    
    function Facturar() {


        porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
        if (porcentaje >= 98) {

            if ($("#frmAddFact2").valid()) {



                Swal.fire({
                    title: 'Esta Seguro que Desea Procesar la Factura?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-fact").modal('hide');
                        $("#main-content").prepend(LOADING);
                        $.post( $("#frmAddFact2").attr("action"), $("#frmAddFact").serialize() + '&' + $("#frmAddFact2").serialize(), function (response) {
                            $(".overlay-wrapper").remove();
                           // Swal.close();
                            
                            
                            
                            //$("#modal-fact").modal('hide');
                            $("#main-content").html(response);
                            

                            
                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });









            }
        } else {
            Toast.fire({
                icon: "error",
                title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
            });
            $(".money").eq(0).focus();
        }
    }
    
    
    
</script>