
<style>
    .container{
        max-width: 100%;
    }
</style>

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">

    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Finiquitar Facturas')}}</h3>

            <div class="card-tools">
                <button type="button"  onClick="goBack()" class="btn btn-tool" ><i class="fas fa-undo"></i>Regresar</button>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-sm-12 col-md-6 border-right">
                    <h4 class="">Datos para la Factura</h4>
                    <div class="row">



                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>
                                <div class="input-group " >
                                    <div class="input-group-prepend" >
                                        {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                    </div>
                                    {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                    <div class="input-group-append" >
                                        <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>  









                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="razon">{{__('Responsable')}}</label>
                                {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Responsable')])}}    

                            </div>
                        </div>  


                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  



                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="correo">{{__('Correo')}}</label>
                                {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    

                            </div>  
                        </div> 
                        <div class="col-sm-12 col-md-12">

                            <div class="form-group">
                                <label for="direccion">{{__('Dirección')}}</label>
                                {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                            </div>  
                        </div>
                    </div>
                </div>


                <div class="col-sm-12 col-md-6">

                    <div class="row">

                        <div class="col-sm-12 col-md-6 border-right">
                            <h4 class="">Forma de Pago 

                                <button type="button" onClick="clsFP()" title="{{__('Borrar Formas de Pago')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash"></i></button>
                            </h4>    
                            <div class="row">
                                @foreach($forma_pagos as $key =>$value)


                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="">{{$value->nombre}}</label>
                                        {{Form::text("money[".$value->crypt_id."]", "0,00", ["data-reverso"=>$value->reverso_formula, "data-formula"=>$value->calculo_formula, "class"=>"text-right form-control money"])}}

                                    </div>
                                </div> 


                                @endforeach
                            </div> 
                        </div> 
                        <div class="col-sm-12 col-md-6 ">
                            <h4 class="">Monto Total</h4>
                            <div class="row">

                                <div class="col-sm-12 border-bottom">
                                    <div class="row">
                                        <div class="col-md-2"><img src="{{url('/')}}/dist/img/bs.png" class="img-fluid" ></div>
                                        <div class="col-md-4"><h3>Monto.</h3></div>
                                        <div class="col-md-6 text-right"><h3 id="montoTot">{{muestraFloat(0, 2)}}</h3></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 border-bottom mt-2">
                                    <div class="row">
                                        <div class="col-md-2"><img src="{{url('/')}}/dist/img/petro.png" class="img-fluid" ></div>
                                        <div class="col-md-4 "><h4>Ref.</h4></div>
                                        <div class="col-md-6 text-right "><h3 id="montoPetro">{{muestraFloat(0, 2)}}</h3></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 border-bottom mt-2">
                                    <div class="row">
                                        <div class="col-md-2"><img src="{{url('/')}}/dist/img/euro.png" class="img-fluid" ></div>
                                        <div class="col-md-4 "><h4>Ref.</h4></div>
                                        <div class="col-md-6 text-right "><h3 id="montoEuro">{{muestraFloat(0, 2)}}</h3></div>
                                    </div>
                                </div>
                                <div class="col-sm-12 border-bottom mt-2">
                                    <div class="row">
                                        <div class="col-md-2"><i class="fa fa-dollar-sign fa-2x "></i></div>
                                        <div class="col-md-4 "><h4>Ref.</h4></div>
                                        <div class="col-md-6 text-right "><h3 id="montoDolar">{{muestraFloat(0, 2)}}</h3></div>
                                    </div>
                                </div>








                            </div>
                        </div>
                    </div>
                </div>    

            </div>


        </div>
    </div>
</div>






<script type="text/javascript">

    $(document).ready(function () {



    });

    function goBack() {
        $("#main-content").prepend(LOADING);
        $.get("{{route('facturas')}}", function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });
    }




</script>