<meta name="csrf-token" content="{{ csrf_token() }}">
<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 ol-xs-12">
            <button class="form-control btn btn-info" onclick="$('#Tasa_Avc').modal('show')"><b>Tasa</b></button>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 ol-xs-12">
            <button class="form-control btn btn-info" onclick="$('#Cargas_Avc').modal('show')"><b>Carga</b></button>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 ol-xs-12">
            <button class="form-control btn btn-info" onclick="$('#Dosas_Avc').modal('show')"><b>Dosa</b></button>
        </div>
    </div>


    <!--<div class="row mt-5">
        <div class="col-lg-2 col-md-2 col-xs-12 col-sm-12">
            <span class="input-group-text"><b>{{__('Tipo de Vuelo')}}:</b></span>
        </div>

        <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 form-inline">
            {{Form::hidden('tipo_id', "", ["id"=>"tipo_id"])}}
            <button class="button_slide slide_left" id="tip_vuelo_1" style="width:15%;" onclick="tip_vuelo(this,1,$('#tipo_id').attr('id'),'INTER...','NACIONAL')">NACIONAL</button>
            <button class="button_slide slide_right" id="tip_vuelo_2" style="width:15%;" onclick="tip_vuelo(this,2,$('#tipo_id').attr('id'),'NAC...','INTERNACIONAL')">INTERNACIONAL</button>
        </div>
    </div>-->
</div>
    <style>

        .FirstColor{
            background-color: rgb(173, 173, 210);
        }
        .SecondColor{
            background-color: rgb(242 243 247);
        }
        .button_slide {
            color: #000000;
            border: 2px solid rgb(242,243,247);
            border-radius: 50px;
            padding: 8px 20px;
            display: inline-block;
            font-family: "Lucida Console", Monaco, monospace;
            font-size: 17px;
            letter-spacing: 1px;
            cursor: pointer;
            /*box-shadow: inset 0 0 0 0 #000000;*/
            -webkit-transition: ease-out 0.4s;
            -moz-transition: ease-out 0.4s;
            transition: ease-out 0.9s;
        }

        .slide_right:hover {
            box-shadow: inset 400px 0 0 0 #051958;
        }

        .slide_left:hover {
            box-shadow: inset 400px 0 0 0px#051958;
        }

    </style>
    @include('facturacion_aviacion_comercial.modal.index')
    <script type="text/javascript">
        function tip_vuelo(obj,numb,hide,text1,text2){
            id=$(obj).attr('id');
            lengt=id.length;
                if(numb==1){
                    $('#'+id).css('width','30%');
                    $('#'+id.substr(0,parseInt(lengt)-parseInt(1))+"2").css('width','5%');
                    $('#'+id.substr(0,parseInt(lengt)-parseInt(1))+"2").html(text1);
                    $('#'+id).html(text2);
                    $('#'+hide).val(1);
                    $('#'+id.substr(0,parseInt(lengt)-parseInt(1))+"2").attr( "class", "button_slide slide_left SecondColor" );
                    //.removeClass('FirstColor SecondColor').addClass('SecondColor');
                    $('#'+id).attr('class','button_slide slide_left FirstColor');
                }else{
                    $('#'+id).css('width','30%');
                    $('#'+id.substr(0,parseInt(lengt)-parseInt(1))+"1").html(text1);
                    $('#'+id).html(text2);
                    $('#'+id.substr(0,parseInt(lengt)-parseInt(1))+"1").css('width','5%');
                    $('#'+hide).val(2);
                    $('#'+id.substr(0,parseInt(lengt)-parseInt(1))+"1").attr( "class", "button_slide slide_left SecondColor" );
                    $('#'+id).attr('class','button_slide slide_left FirstColor');
                }
        }

        function Get_Services_Avc(data){//tasa
            $.post("{{route('proforma_services')}}", $("#ProFormAvcTasa").serialize(), function (response) {
                $.each( response.data1, function( i, item ) {
                    let html=
                    `
                        <tr>
                            <td>${$('#cant_pax').val()}</td>
                            <td>${item['servicio']}</td>
                            <td>${item['formula']}</td>
                        </tr>

                    `;
                    $('#ServFactura').html(html);
                });
                $('#base_IT').text(response.data2.BaseImponible);
                $('#exento_T').text(response.data2.Exento);
                $('#subtotal_T').text(response.data2.Subtotal);
                $('#iva_T').text(response.data2.Iva);
                $('#total_T').text(response.data2.Total);
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }

        function Get_Services_Avc_Carga(data){//carga
            $.post("{{route('proforma_carga')}}", data, function (response) {
                $('#CargaServices').empty();
                $.each( response.data1, function( i, item ) {
                    let html=
                    `
                        <tr>
                            <td>${item['codigo']}</td>
                            <td>${item['servicio']}</td>
                            <td>${item['formula']}</td>
                        </tr>
                    `;
                    $('#CargaServices').append(html);
                });
                $('#baseC').text(response.data2.BaseImponible);
                $('#exentoC').text(response.data2.Exento);
                $('#subtotalC').text(response.data2.Subtotal);
                $('#ivaC').text(response.data2.Iva);
                $('#totalC').text(response.data2.Total);
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }

        function Get_Services_Avc_Dosa(data){//carga
            $.post("{{route('proforma_dosa')}}", data, function (response) {
                /*$('#CargaServices').empty();
                $.each( response.data1, function( i, item ) {
                    let html=
                    `
                        <tr>
                            <td>${item['codigo']}</td>
                            <td>${item['servicio']}</td>
                            <td>${item['formula']}</td>
                        </tr>
                    `;
                    $('#CargaServices').append(html);
                });
                $('#baseC').text(response.data2.BaseImponible);
                $('#exentoC').text(response.data2.Exento);
                $('#subtotalC').text(response.data2.Subtotal);
                $('#ivaC').text(response.data2.Iva);
                $('#totalC').text(response.data2.Total);
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });*/
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }

        function ListServices(data){
            $.get("{{route('list_service_avc')}}/"+data, function (response) {
                $('#ListServicesExtras').empty();
                $.each(response,function(i,item){
                    let input=`
                        <input type="checkbox" name="proservicios[${i}][codigo]" id="proservicios${i}codigo" onclick="DismitHiden('proservicios${i}codigo','proservicios${i}cant')" value="${item['crypt_id']}">
                        Cant.<input type="text" name="proservicios[${i}][cant]" id="proservicios${i}cant" disabled="disabled" value="0" style="width:45px;">
                        ${item['codigo']}-${item['descripcion']}<br>`;
                    $('#ListServicesExtras').append(input);
                });/*
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });*/
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }

        function findDato2() {
            if ($("#ProFormAvcTasa #document").valid()) {
                $("#ProFormAvcTasa").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#ProFormAvcTasa #type_document").val() + "/" + $("#ProFormAvcTasa #document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $("#ProFormAvcTasa #cliente_id").val(response.data.crypt_id);
                        $("#ProFormAvcTasa #razon").val(response.data.razon_social);
                        $("#ProFormAvcTasa #phone").val(response.data.telefono);
                        $("#ProFormAvcTasa #direccion").val(response.data.direccion);
                        $("#ProFormAvcTasa #razon, #ProFormAvcTasa #phone, #ProFormAvcTasa #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#ProFormAvcTasa #razon, #ProFormAvcTasa #phone, #ProFormAvcTasa #correo, #ProFormAvcTasa #direccion").prop("readonly", false);
                        $("#ProFormAvcTasa #cliente_id, #ProFormAvcTasa #razon, #ProFormAvcTasa #phone, #ProFormAvcTasa #direccion").val("");
                        $("#ProFormAvcTasa #razon").focus();
                    }
                    $("#ProFormAvcTasa").valid();
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#ProFormAvcTasa #razon, #ProFormAvcTasa #phone, #ProFormAvcTasa #direccion").prop("readonly", true);
                    $("#ProFormAvcTasa #cliente_id, #ProFormAvcTasa #razon, #ProFormAvcTasa #phone, #ProFormAvcTasa #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }
        }

        function AddServices(event,servi,dataform){

            $.post(
                "{{route('service_single_avc')}}",
                servi+'&'+dataform,
                function (response) {
                console.log(response);
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }

        function DismitHiden(check,id){
            if( $('#'+check).prop('checked') == true) {
                $(`#${id}`).removeAttr("disabled");
            }else{
                $(`#${id}`).attr("disabled","disabled").val(0);
            }
        }
    </script>
</div>
