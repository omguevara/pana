<!--///////////////////////////////TASA/////////////////////////////////////-->
<div class="modal"  role="dialog" id="Tasa_Avc">
    <div class="modal-dialog modal-xl" role="Tasa_Avc">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center"><b>PROFORMA TASA</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

            {{Form::open(["route"=>"recaudacion",'class'=>'form-horizontal',   'id'=>'ProFormAvcTasa','autocomplete'=>'Off'])}}

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Aeropuerto')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::hidden('tipo_id', "", ["id"=>"tipo_id_tasa"])}}
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Aeronave')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Fecha Operación')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::date("fecha_operacion", date("d/m/Y"), ["required"=>"required", "class"=>"form-control  required date_time uno", "id"=>"fecha_operacion", "placeholder"=>__('Hora Fecha Operación')])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Num. Vuelo')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("num_vuelo", '', ["required"=>"required", "class"=>"form-control  required", "id"=>"num_vuelo", 'maxlength'=>'4' ,"placeholder"=>__('Num. Vuelo')])}}
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Doc')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1 cliente", "id"=>"type_document" ,"required"=>"required"])}}
                            {{Form::hidden('cliente_id', "", [ "id"=>"cliente_id"])}}
                            {{Form::hidden("temp_alt","0", ["class"=>"temp_alt", "id"=>"temp_alt"])}}
                        </div>
                        {{Form::text("document", "", ["onBlur"=>"findDato2()", "data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}
                        <div class="input-group-append" >
                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i  id="b_s" class="fa fa-search"></i></div>
                        </div>
                    </div>
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Responsable:</b></span>
                        </div>
                        {{Form::text("razon", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"razon", "placeholder"=>__('Responsable')])}}
                    </div>

                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                        </div>
                        {{Form::text("phone", "", ["required"=>"required", "class"=>"form-control required phone cliente", "id"=>"phone", "placeholder"=>__('Phone')])}}
                    </div>

                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                        </div>
                        {{Form::text("direccion", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"direccion", "placeholder"=>__('Dirección')])}}
                    </div>





                </div>

                <div class="row mt-2">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                        <span class="input-group-text"><b>{{__('Tipo de Vuelo')}}:</b></span>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                        {{Form::hidden('tipo_id', "", ["id"=>"tipo_id"])}}
                        <button class="button_slide slide_left" id="tip_vuelo_1" style="width:15%;" onclick="tip_vuelo(this,1,$('#tipo_id').attr('id'),'INTER...','NACIONAL'),Get_Services_Avc($('#ProFormAvcTasa').serialize())">NACIONAL</button>
                        <button class="button_slide slide_right" id="tip_vuelo_2" style="width:15%;" onclick="tip_vuelo(this,2,$('#tipo_id').attr('id'),'NAC...','INTERNACIONAL'),Get_Services_Avc($('#ProFormAvcTasa').serialize())">INTERNACIONAL</button>
                    </div>
                </div>


                <div class="row mt-2">
                    <div class="input-group mt-2 ">

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Cant. Pax.')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("cant_pax", "0", ["onkeyup"=>"Get_Services_Avc($('#ProFormAvcTasa').serialize());","data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number  ", "id"=>"cant_pax", "placeholder"=>__('Cantidad de Pasajeros')])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Hora de Salida')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("hora_salida", date("H:m"), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_salida", "placeholder"=>__('Hora Salida')])}}
                        </div>
                    </div>
                </div>

                <!--
                <div class="row">
                    <button onclick="$('#ServiciosAvc').modal('show')"><i class="fa fa-file-invoice-dollar"></i> Servicios</button>
                </div>-->

                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><h4><b>Detalles de Factura</b></h4></div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <table border="1" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:7%;">CANT.</th>
                                    <th style="width:80%;">SERVICIO</th>
                                    <th>SUBTOTAL</th>
                                </tr>
                            </thead>
                            <tbody id="ServFactura">

                            </tbody>
                        </table>


                        <table border="1"  width="100%">
                            <tr>
                                <th style="width:87%;">BASE IMPONIBLE</th>
                                <th id="base_IT"></th>
                            </tr>

                            <tr>
                                <th>EXENTO</th>
                                <th id="exento_T"></th>
                            </tr>

                            <tr>
                                <th>SUBTOTAL</th>
                                <th id="subtotal_T"></th>
                            </tr>

                            <tr>
                                <th>IVA(16%)</th>
                                <th id="iva_T"></th>
                            </tr>

                            <tr>
                                <th>TOTAL</th>
                                <th id="total_T"></th>
                            </tr>

                        </table>
                    </div>
                </div>

            {{ Form::close() }}

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!--///////////////////////////////CARGAS///////////////////////////////////-->
<div class="modal" tabindex="1" id="Cargas_Avc" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="Cargas_Avc">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center"><b>PROFORMA CARGAS</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {{Form::open(["route"=>"recaudacion",'class'=>'form-horizontal',   'id'=>'ProFormAvcCarga','autocomplete'=>'Off'])}}

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Aeropuerto')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::hidden('tipo_id', "", ["id"=>"tipo_id"])}}
                            {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Aeronave')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control form-select uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                        </div>

                    </div>
                </div>

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Num. Vuelo')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("num_vuelo", '', ["required"=>"required", "class"=>"form-control  required", "id"=>"num_vuelo", 'maxlength'=>'4' ,"placeholder"=>__('Num. Vuelo')])}}
                        </div>
                        &nbsp;

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Documento')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1 cliente", "id"=>"type_document" ,"required"=>"required"])}}
                            {{Form::hidden('cliente_id', "", [ "id"=>"cliente_id"])}}
                            {{Form::hidden("temp_alt","0", ["class"=>"temp_alt", "id"=>"temp_alt"])}}
                        </div>
                        {{Form::text("document", "", ["onBlur"=>"findDato2()", "data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}
                        <div class="input-group-append" >
                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i  id="b_s" class="fa fa-search"></i></div>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="input-group mt-2 ">

                    </div>
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Responsable:</b></span>
                        </div>
                        {{Form::text("razon", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"razon", "placeholder"=>__('Responsable')])}}
                    </div>

                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                        </div>
                        {{Form::text("phone", "", ["required"=>"required", "class"=>"form-control required phone cliente", "id"=>"phone", "placeholder"=>__('Phone')])}}
                    </div>

                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                        </div>
                        {{Form::text("direccion", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"direccion", "placeholder"=>__('Dirección')])}}
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                        <span class="input-group-text"><b>{{__('Tipo de Vuelo')}}:</b></span>
                    </div>
                    <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                        {{Form::hidden('tipo_idC', "", ["id"=>"tipo_idC"])}}
                        <button class="button_slide slide_left" id="tip_vueloC_1" style="width:15%;" onclick="tip_vuelo(this,1,$('#tipo_idC').attr('id'),'INTER...','NACIONAL'),Get_Services_Avc_Carga($('#ProFormAvcCarga').serialize())">NACIONAL</button>
                        <button class="button_slide slide_right" id="tip_vueloC_2" style="width:15%;" onclick="tip_vuelo(this,2,$('#tipo_idC').attr('id'),'NAC...','INTERNACIONAL'),Get_Services_Avc_Carga($('#ProFormAvcCarga').serialize())">INTERNACIONAL</button>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Embarque')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("embarque", '', ["required"=>"required", "class"=>"form-control  required", "id"=>"embarque", 'maxlength'=>'4' ,"placeholder"=>__('Num. Vuelo Embarque')])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Fecha Operación')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::date("fecha_operacion_emb", date("d/m/Y"), ["required"=>"required", "class"=>"form-control  required date_time uno", "id"=>"fecha_operacion", "placeholder"=>__('Fecha Operación Embarque')])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Carga Embarcada Kg.')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("embarque_emb", '0', ["required"=>"required", "class"=>"form-control  required", "id"=>"embarque_emb", 'maxlength'=>'4', "onchange"=>"Get_Services_Avc_Carga($('#ProFormAvcCarga').serialize())" ,"placeholder"=>__('Carga Embarcada Kg.')])}}
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Desembarque')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("desembarque", '', ["required"=>"required", "class"=>"form-control  required", "id"=>"desembarque", 'maxlength'=>'4' ,"placeholder"=>__('Num. Vuelo Desembarque')])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Fecha Operación')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::date("fecha_operacion_desm", date("d/m/Y"), ["required"=>"required", "class"=>"form-control  required date_time uno", "id"=>"fecha_operacion_desm", "placeholder"=>__('Fecha Operación Desembarque')])}}
                        </div>

                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Carga Desembarcada Kg.')}}:</b></span>
                        </div>
                        <div class="input-group-prepend" >
                            {{Form::text("embarque_desm", '0', ["required"=>"required", "class"=>"form-control  required", "id"=>"embarque_desm", 'maxlength'=>'4', "onchange"=>"Get_Services_Avc_Carga($('#ProFormAvcCarga').serialize())" ,"placeholder"=>__('Carga Desembarcada Kg.')])}}
                        </div>
                    </div>
                </div>

                <!--
                <div class="row">
                    <button onclick="$('#ServiciosAvc').modal('show')"><i class="fa fa-file-invoice-dollar"></i> Servicios</button>
                </div>-->

                <div class="row mt-4">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><h4><b>Detalles de Factura</b></h4></div>
                </div>

                <div class="row mt-2">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <table border="1" width="100%">
                            <thead>
                                <tr>
                                    <th style="width:7%;">CODIGO</th>
                                    <th style="width:80%;">SERVICIO</th>
                                    <th>SUBTOTAL</th>
                                </tr>
                            </thead>
                            <tbody id="CargaServices">

                            </tbody>
                        </table>


                        <table border="1"  width="100%">
                            <tr>
                                <th style="width:87%;">BASE IMPONIBLE</th>
                                <th id="baseC"></th>
                            </tr>

                            <tr>
                                <th>EXENTO</th>
                                <th id="exentoC"></th>
                            </tr>

                            <tr>
                                <th>SUBTOTAL</th>
                                <th id="subtotalC"></th>
                            </tr>

                            <tr>
                                <th>IVA(16%)</th>
                                <th id="ivaC"></th>
                            </tr>

                            <tr>
                                <th>TOTAL</th>
                                <th id="totalC"></th>
                            </tr>

                        </table>
                    </div>
                </div>

            {{ Form::close() }}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!--///////////////////////////////DOSAS////////////////////////////////////-->
<div class="modal" role="dialog" id="Dosas_Avc">
    <div class="modal-dialog modal-xl" role="Dosas_Avc">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center"><b>PROFORMA DOSAS</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-7 col-md-7 col-xs-12 col-sm-12">
                    {{Form::open(["route"=>"recaudacion",'class'=>'form-horizontal',   'id'=>'ProFormAvcDosa','autocomplete'=>'Off'])}}
                        <div class="row mt-2">
                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Aeropuerto')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::hidden('tipo_id', "", ["id"=>"tipo_id"])}}
                                    {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                                </div>

                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Aeronave')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Fecha Operación')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::date("fecha_operacion", date("d/m/Y"), ["required"=>"required", "class"=>"form-control  required date_time uno", "id"=>"fecha_operacion", "placeholder"=>__('Hora Fecha Operación')])}}
                                </div>

                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Num. Vuelo')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::text("num_vuelo", '', ["required"=>"required", "class"=>"form-control  required", "id"=>"num_vuelo", 'maxlength'=>'4' ,"placeholder"=>__('Num. Vuelo')])}}
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Doc')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1 cliente", "id"=>"type_document" ,"required"=>"required"])}}
                                    {{Form::hidden('cliente_id', "", [ "id"=>"cliente_id"])}}
                                    {{Form::hidden("temp_alt","0", ["class"=>"temp_alt", "id"=>"temp_alt"])}}
                                </div>
                                {{Form::text("document", "", ["onBlur"=>"findDato2()", "data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}
                                <div class="input-group-append" >
                                    <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i  id="b_s" class="fa fa-search"></i></div>
                                </div>
                            </div>
                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Responsable:</b></span>
                                </div>
                                {{Form::text("razon", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"razon", "placeholder"=>__('Responsable')])}}
                            </div>

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                                </div>
                                {{Form::text("phone", "", ["required"=>"required", "class"=>"form-control required phone cliente", "id"=>"phone", "placeholder"=>__('Phone')])}}
                            </div>

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                                </div>
                                {{Form::text("direccion", "", ["required"=>"required", "class"=>"form-control required cliente", "id"=>"direccion", "placeholder"=>__('Dirección')])}}
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-lg-3 col-md-3 col-xs-12 col-sm-12">
                                <span class="input-group-text"><b>{{__('Tipo de Vuelo')}}:</b></span>
                            </div>
                            <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                                {{Form::hidden('tipo_iddtv', "", ["id"=>"tipo_iddtv"])}}
                                <button class="button_slide slide_left" id="tip_vuelodtv_1" style="width:15%;" onclick="tip_vuelo(this,1,$('#tipo_iddtv').attr('id'),'INTER...','NACIONAL'),Get_Services_Avc_Dosa($('#ProFormAvcDosa').serialize()),ListServices($('#tipo_iddtv').val())">NACIONAL</button>
                                <button class="button_slide slide_right" id="tip_vuelodtv_2" style="width:15%;" onclick="tip_vuelo(this,2,$('#tipo_iddtv').attr('id'),'NAC...','INTERNACIONAL'),Get_Services_Avc_Dosa($('#ProFormAvcDosa').serialize()),ListServices($('#tipo_iddtv').val())">INTERNACIONAL</button>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Hora de Llegada')}}:</b></span>
                                </div>
                                <div class="input-group-prepend">
                                    {{Form::text("hora_llegada", date('d/m/Y H:i'),  [ "class"=>"form-control date_time  ", "id"=>"hora_llegada" ])}}
                                </div>

                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Hora de Salida')}}:</b></span>
                                </div>
                                <div class="input-group-prepend">
                                    {{Form::text("hora_salida", date('d/m/Y H:i'),  [ "class"=>"form-control date_time  ", "id"=>"hora_salida" ])}}
                                </div>


                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <table border="1" width="100%">
                                    <tr>
                                        <th style="width:7%;">CANT.</th>
                                        <th style="width:80%;">SERVICIO</th>
                                        <th>SUBTOTAL</th>
                                    </tr>

                                </table>
                                <table border="1"  width="100%">
                                    <tr>
                                        <th style="width:87%;">BASE IMPONIBLE</th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        <th>EXENTO</th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        <th>SUBTOTAL</th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        <th>IVA(16%)</th>
                                        <th></th>
                                    </tr>

                                    <tr>
                                        <th>TOTAL</th>
                                        <th></th>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>

                <div class="col-lg-5 col-md-5 col-xs-12 col-sm-12">
                    {{Form::open(["route"=>"recaudacion",   'id'=>'ServExtraDosa','autocomplete'=>'Off'])}}
                        <div id="ListServicesExtras">

                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                {{Form::button(__("Agregar"),  ["onclick"=>"AddServices(event,$('#ServExtraDosa').serialize(),$('#ProFormAvcDosa').serialize())", "type"=>"button", "class"=>" form-control btn btn-success", "id"=>"AddServicess"])}}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
<!--///////////////////////////////SERVICIOS////////////////////////////////-->
<div class="modal" role="dialog" id="ServiciosAvc">
    <div class="modal-dialog modal-md" role="ServiciosAvc">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><b><h4>Servicios Extras</h4></b></div>
            </div>


        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
