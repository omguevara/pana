

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Editar Cuenta')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('cuentas_bancarias')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["cuentas_bancarias.edit", $CuentaBancaria->crypt_id], 'id'=>'frmEditCuenta','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">               
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Banco:</b></span>
                    </div>
                    {{Form::select('banco_id', $Bancos, $CuentaBancaria->getBancos->crypt_id, ["class"=>"form-control tab1 select2", "id"=>"banco_id" ,"required"=>"required"])}}

                </div>

                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Número de cuenta:</b></span>
                    </div>

                    {{Form::text("numero_cuenta", $CuentaBancaria->numero_cuenta, ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos ", "maxlength"=>"20", "id"=>"numero_cuenta", "placeholder"=>__('Número de Cuenta')])}}
                </div>


            </div>
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>





<script type="text/javascript">




    $(document).ready(function () {

        $('.select2').select2();

        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $('.number').numeric({negative: false});
        $('#frmEditCuenta').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmEditCuenta').valid()) {
                $("#frmEditCuenta").prepend(LOADING);
                var formData = new FormData(document.getElementById("frmEditCuenta"));


                $.ajax({
                    url: this.action,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('cuentas_bancarias')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmEditCuenta").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmEditCuenta").find(".overlay-wrapper").remove();
                        }
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });




            }
            return false;
        });

        $('#frmEditCuenta').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
