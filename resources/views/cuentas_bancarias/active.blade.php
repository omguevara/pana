

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Activar Cuenta Bancaria')}}</h3>

            <div class="card-tools">
                 <a type="button"  href="{{route('cuentas_bancarias')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["cuentas_bancarias.active", $CuentaBancaria->crypt_id],  'id'=>'frmActivarCuenta','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-sm-12 col-md-12">

                    <div class="form-group">
                        <label for="numero_cuenta">{{__('Número de cuenta')}}</label>
                        {{Form::text("numero_cuenta", $CuentaBancaria->numero_cuenta, ["disabled"=>"disabled", "class"=>"form-control  ", "id"=>"numero_cuenta", "placeholder"=>__('Número de Cuenta')])}}

                    </div>
                </div>
            </div>


            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-check-circle"></i> '.__("Active"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#frmActivarCuenta').on("submit", function (event) {
            event.preventDefault();

            $("#frmActivarCuenta").prepend(LOADING);
            $.post(this.action, $("#frmActivarCuenta").serialize(), function (response) {
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $.get("{{route('cuentas_bancarias')}}", function (response) {
                        $("#main-content").html(response);

                    }).fail(function (xhr) {
                        $("#frmActivarCuenta").find(".overlay-wrapper").remove();
                    });
                } else {
                    $("#frmActivarCuenta").find(".overlay-wrapper").remove();
                }
                //console.log(response);
            }).fail(function () {
                $("#frmActivarCuenta").find(".overlay-wrapper").remove();
            });

            return false;
        });

        $('#frmActivarCuenta').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
