@extends('layouts.default2')

@section('alarms')
    <div class="modal fade" id="equiposRevisionModal" tabindex="-1" role="dialog" aria-labelledby="equiposRevisionModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="equiposRevisionModalLabel">Equipos que necesitan revisión</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                <thead>
                    <tr>
                    <th>Modelo</th>
                    <th>Serial</th>
                    <th>Precio</th>
                    <th>Última revisión</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($equipos as $equipo)
                    <tr>
                    <td>{{ $equipo->modelo }}</td>
                    <td>{{ $equipo->serial }}</td>
                    <td>{{ $equipo->precio }}</td>
                    <td>{{ $equipo->ultima_revision }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>
@endsection