

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Servicio')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('prodservicios')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"prodservicios.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="codigo">{{__('Código')}}</label>
                        {{Form::text("codigo", "", ["maxlength"=>"10", "required"=>"required", "class"=>"form-control required", "id"=>"codigo", "placeholder"=>__('Código')])}}

                    </div>
                </div>


                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="nomenclatura">{{__('NOMENCLATURA')}}</label>
                        {{Form::text("nomenclatura", "", ["maxlength"=>"100",  "class"=>"form-control ", "id"=>"nomenclatura", "placeholder"=>__('NOMENCLATURA')])}}

                    </div>
                </div>



                <div class="col-md-12">
                    <div class="form-group">
                        <label for="descripcion">{{__('Descripción')}}</label>
                        {{Form::text("descripcion", "", ["maxlength"=>"200", "required"=>"required", "class"=>"form-control required", "id"=>"descripcion", "placeholder"=>__('Descripción')])}}

                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="valor_dollar">{{__('Precio en Dolar')}}</label>
                        {{Form::text("valor_dollar", "0,00", [ "required"=>"required", "class"=>"form-control required money text-right", "id"=>"valor_dollar", "placeholder"=>__('Precio en Dolar')])}}

                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="valor_euro">{{__('Precio en Euro')}}</label>
                        {{Form::text("valor_euro", "0,00", ["required"=>"required", "class"=>"form-control required money text-right", "id"=>"valor_euro", "placeholder"=>__('Precio en Euro')])}}

                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="valor_petro">{{__('Precio en Petro')}}</label>
                        {{Form::text("valor_petro", "0,00", ["required"=>"required", "class"=>"form-control required petro text-right", "id"=>"valor_petro", "placeholder"=>__('Precio en Petro')])}}

                    </div>
                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="iva">{{__('Aplica IVA')}}</label>
                        {{Form::select("iva", ["0"=>"No", "16"=>"Si"],  "", ["required"=>"required", "class"=>"form-control required", "id"=>"iva", "placeholder"=>__('Select')])}}

                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label for="tipo_vuelo_id">{{__('Tipo ')}}</label>
                        {{Form::select('tipo_vuelo_id', ["1"=>"COMERCIAL", "2"=>"GENERAL"], "", ["placeholder"=>"Seleccione",   "class"=>"form-control  ", "id"=>"tipo_vuelo_id" ,"required"=>"required"])}}

                    </div>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Nacionalidad</label>
                        {{Form::select('nacionalidad_id', ["1"=>"NACIONAL", "2"=>"INTERNACIONAL"], "", ["placeholder"=>"No Aplica",   "class"=>"form-control  ", "id"=>"nacionalidad_id" ])}}
                    </div>

                </div>

                <div class="col-xs-12 col-md-6">
                    <div class="form-group">
                        <label>Turno</label>
                        {{Form::select('turno_id', ["1"=>"DIURNO", "2"=>"NOCTURNO"], "", ["placeholder"=>"No Aplica",   "class"=>"form-control  ", "id"=>"turno_id" ])}}
                    </div>
                </div>


                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Es un Servicio Extra</label>
                        {{Form::select('prodservicio_extra', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control ", "id"=>"prodservicio_extra" ,"required"=>"required"])}}
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Aplica Categoría</label>
                        {{Form::select('aplica_categoria', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control ", "id"=>"aplica_categoria" ,"required"=>"required"])}}
                    </div>
                </div>

                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Influye en el Estacionamiento</label>
                        {{Form::select('aplica_estacion', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control  ", "id"=>"aplica_estacion" ,"required"=>"required"])}}
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Aplica al Peso</label>
                        {{Form::select('aplica_peso', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control ", "id"=>"aplica_peso" ,"required"=>"required"])}}
                    </div>
                </div>


                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="peso_inicio">{{__('Peso de  Inicio (TON)')}}</label>
                        {{Form::text("peso_inicio", "0,00", ["readonly"=>"readonly",  "class"=>"form-control text-right  money", "id"=>"peso_inicio", "placeholder"=>__('Peso de Inicio')])}}

                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label for="peso_fin">{{__('Peso de Fin (TON)')}}</label>
                        {{Form::text("peso_fin", "0,00", ["readonly"=>"readonly",  "class"=>"form-control text-right  money", "id"=>"peso_fin", "placeholder"=>__('Peso Fin')])}}

                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Servicio Obligatorio Para Tasa</label>
                        {{Form::select('default_tasa', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control required ", "id"=>"default_tasa" ,"required"=>"required"])}}
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Servicio Obligatorio Para Dosa</label>
                        {{Form::select('default_dosa', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control required ", "id"=>"default_dosa" ,"required"=>"required"])}}
                    </div>
                </div>
                <div class="col-xs-12 col-md-4">
                    <div class="form-group">
                        <label>Servicio Obligatorio Para Carga</label>
                        {{Form::select('default_carga', ["0"=>"NO", "1"=>"SI"], "0", ["placeholder"=>"Seleccione",   "class"=>"form-control required ", "id"=>"default_carga" ,"required"=>"required"])}}
                    </div>
                </div>
                <div class="col-xs-12 col-md-12">
                    <label>Fórmula</label>
                    <div class="input-group mb-3">

                        {{Form::textarea('formula', '',   array("style"=>"min-height:50px",'rows'=>'2',  'required'=>"required", "data-msg-required"=>__("Required Field"), "class"=>"form-control", "placeholder"=>__("Formula")))}}
                        <div class="input-group-append">
                            <span  data-toggle="modal" data-target="#modal-info"   style="cursor:pointer" class="input-group-text"><i class="fas fa-info"></i></span>
                        </div>


                    </div>
                </div>






            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>


<div class="modal fade" id="modal-info"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">AYUDA</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div  class="modal-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-info"></i>
                                COMO CONSTRUIR LA FORMULA DEL SERVICIO
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="alert alert-info alert-dismissible">
                                A continuaci&oacute;n se muestran las distintas variables a usar al momento de especificar
                                el c&aacute;lculo del precio del servicio
                            </div>


                            <dl>
                                <dt>[CANT_PASAJEROS]</dt>
                                <dd>Esta variable contempla la cantidad de pasajeros que se registro en el vuelo.</dd>
                                <dt>[PESO_AVION]</dt>
                                <dd>Esta variable contempla el peso de la aeronave.</dd>
                                <dt>[VALOR_DOLLAR]</dt>
                                <dd>Esta variable captura el valor asignado en el campo <strong><a href="#valor_dollar">Precio en Dolar</a></strong> <img class="img-fluid border border-2" src="{{url('/')}}/dist/img/precio_dollar.png"/></dd>
                                <dt>[VALOR_PETRO]</dt>
                                <dd>Esta variable captura el valor asignado en el campo <strong><a href="#valor_petro">Precio en Petros</a></strong> <img class="img-fluid border border-2" src="{{url('/')}}/dist/img/precio_petro.png"/></dd>
                                <dt>[VALOR_EURO]</dt>
                                <dd>Esta variable captura el valor asignado en el campo <strong><a href="#valor_euro">Precio en Euros</a></strong> <img class="img-fluid border border-2" src="{{url('/')}}/dist/img/precio_euro.png"/></dd>


                                <dt>[DOLLAR]</dt>
                                <dd>Esta variable captura el valor de la tasa del Dolar actual ({{muestraFloat($VALOR_DOLLAR)}})</dd>
                                <dt>[PETRO]</dt>
                                <dd>Esta variable captura el valor de la tasa del Petro actual ({{muestraFloat($VALOR_PETRO)}})</dd>
                                <dt>[EURO]</dt>
                                <dd>Esta variable captura el valor de la tasa del Euro actual ({{muestraFloat($VALOR_EURO)}})</dd>

                            </dl>

                            <div class="callout callout-info">
                                <h5>Ejemplo:</h5>

                                <p style="font-style: italic; text-align: justify ">
                                    Derecho de aterrizaje de aeronaves en vuelos nacionales diurno
                                    (por tonelada o fracción de tonelada del peso máximo de despegue de la aeronave),
                                    Tarifa es de 0,05 Petros
                                </p>
                                <p style=" text-align: justify ">
                                    En este caso se caso se debe ingresar en el campo
                                    <strong><a onClick="javascript:void(0)" href="#">Precio en Petros</a></strong>
                                    el valor de la tarifa en petros, y en el campo formula se expresa lo siguiente: se divide el peso de la aeronave
                                    entre mil (1.000 Fracci&oacute;n de tonelada)
                                    y se multiplica por la variable <strong>[VALOR_PETRO]</strong>, la cual contiene el valor asignado en el campo
                                    <strong><a onClick="javascript:void(0)" href="#">Precio en Petros</a></strong>
                                </p>
                                <dl>
                                    <dt>RESULTADO</dt>
                                    <dt>([PESO_AVION]/1000)*[VALOR_PETRO]</dt>
                                </dl>
                            </div>


                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"   class="btn btn-primary" data-dismiss="modal">Cerrar</button>


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">




    $(document).ready(function () {
        $(".money").maskMoney({"decimal": ",", "thousands": ".", "allowZero": true});
        $(".petro").maskMoney({precision:4,"decimal": ",", "thousands": ".", "allowZero": true});

        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('prodservicios')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });
        $("#aplica_peso").on("change", function () {
            if (this.value == 0) {
                $("#peso_inicio, #peso_fin").prop("readonly", true).removeClass("required").removeAttr("required");
            } else {
                $("#peso_inicio, #peso_fin").prop("readonly", false).addClass("required").attr("required", "required");
            }

        });
        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
