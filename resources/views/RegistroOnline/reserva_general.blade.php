@extends('layouts.principal')
@section('content')

<div class="col-sm-12">
    <div class="row mb-2">

        <h1>Registro</h1>
    </div>

</div>



<div class="col-sm-12 offset-md-3  col-md-6  offset-md-4 col-xl-4">




    <div id="card1" class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Reserva de Vuelo</h3>
        </div>
        <div id="carBodyP" class="card-body p-0">
            {{Form::open(array("route"=>"reserva_general", "target"=>"_blank", "enctype"=>"multipart/form-data",  "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"tabFrm1"))}}

            {{Form::hidden("piloto_id", "", ["id"=>"piloto_id"])}}
            {{Form::hidden("aeronave_id", "", ["id"=>"aeronave_id"])}}

            <div class="col-12 ">
                <div class="form-group">
                    <label for="type_document2">{{__('Tipo Documento')}}</label>
                    {{Form::select('type_document2', [ "V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document2" ,"required"=>"required"])}}

                </div>  
            </div>
            <div class="col-sm-12 ">

                <div class="form-group">
                    <label for="document2">{{__('Documento del Piloto')}}</label>
                    <div class="input-group " >
                        {{Form::text("document2", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "onblur"=>'findDato2()',  "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document2", "placeholder"=>__('Document')])}}    
                        <div class="input-group-append" >
                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato2" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                        </div>

                    </div>
                </div>  
            </div>  
            <div class="col-sm-12 ">
                <div class="form-group">
                    <label for="piloto">{{__('Piloto')}}</label>
                    {{Form::text("piloto", "", [ "readonly"=>"readonly", "maxlength"=>"100", "required"=>"required", "class"=>"form-control required tab1", "id"=>"piloto", "placeholder"=>__('Nombre del Piloto')])}}    

                </div>
            </div>  

            <div class="rows">
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <label for="aeropuerto_id" >Aeropuerto</label>
                        {{Form::select('aeropuerto_id', $DESTINO, "", ["placeholder"=>"Seleccione",  "class"=>"form-control select2 required", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                    </div>
                    <!-- /.form-group -->
                </div>
            </div>
            <div class="col-12 col-sm-12">
                <div class="form-group">
                    <label>Tipo de Vuelo</label>
                    {{Form::select('nacionalidad_id', ["1"=>"NACIONAL", "2"=>"INTERNACIONAL"], 1, ["placeholder"=>"Seleccione",   "class"=>"form-control  ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                </div>
            </div>
            <div class="col-12 col-sm-12">
                <div class="form-group">
                    <label>Tipo de Operación</label>
                     {{Form::select('tipo_operacion_id', ["1"=>"LLEGADA", "2"=>"SALIDA"], "", [  "data-msg-required"=>"Campo Requerido",  "placeholder"=>"Seleccione" ,"class"=>"form-control   ", "id"=>"tipo_operacion_id" ,"required"=>"required"])}}
                </div>
            </div>
            <div class="col-12 col-sm-12">
                <div class="form-group">
                    <label >{{__('Fecha de Operación')}}</label>

                    <div class="input-group " >
                        {{Form::date("fecha", date("Y-m-d"), [ "data-msg-required"=>"Campo Requerido", "class"=>"form-control required", "id"=>"fecha" ,"required"=>"required"  ])}}
                        <div class="input-group-append" >
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>

                </div>
                <!-- /.form-group -->
            </div>

            <div class="col-12 col-sm-12">
                <div class="form-group ">
                    <label for="hora_inicio" >Hora de la Operaci&oacute;n Local (Formato 24H)</label>
                    {{Form::text("hora_inicio", date("H:m"), ["required"=>"required", "class"=>"form-control tab2 required time", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                </div>
            </div>

            <div class="col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="cant_pasajeros">{{__('Cant. Pasajeros')}}</label>
                    {{Form::number("cant_pasajeros", "", ["min"=>"1", "max"=>"99", "required"=>"required", "class"=>"form-control required number ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                </div>
            </div>


            <div class="col-sm-12 ">
                <div class="form-group">
                    <label for="placa">Matr&iacute;cula</label>
                    <div class="input-group " >
                        {{Form::text("placa", "", ["required"=>"required", "maxlength"=>"10" ,"onblur"=>"findPlaca()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control required tab2", "id"=>"placa", "placeholder"=>__('Matrícula')])}}    
                        <div class="input-group-append" >
                            <div title="{{__('Buscar la Aeronave')}}" id="btnFinPlaca" onClick="findPlaca()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                        </div>

                        <div class="input-group-append" >
                            <div title="{{__('Crear Nueva Aeronave')}}" onClick="$('#modal-nave').modal('show');" style="cursor:pointer" class="input-group-text"><i class="fa fa-plus"></i></div>
                        </div>



                    </div>
                </div>
            </div>  


            <div class="col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="aeronave">{{__('Modelo de Aeronave')}}</label>
                    {{Form::text("aeronave", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"aeronave", "placeholder"=>__('Aeronave')])}}    

                </div>
            </div>

            <div class="col-sm-12 col-md-12">
                <div class="form-group">
                    <label for="peso_avion">{{__('Peso Max. de Despegue (TON)')}}</label>
                    {{Form::text("peso_avion", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"peso_avion", "placeholder"=>__('Peso Max. TON')])}}    

                </div>
            </div>




            <div class="col-sm-12 mb-2">
                <div class="input-group ">
                    {{Form::text('captcha', '',   array("style"=>"min-height:50px", 'required'=>"required", "data-msg-required"=>__("Required Field"), "class"=>"form-control", "placeholder"=>__("Captcha")))}}

                    <div class="input-group-append">
                        <div style="padding: 0px" class="input-group-text">
                            {!! Captcha::img('flat'); !!}
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 ">

                <button class="btn btn-primary float-right mb-2" type="button" onclick="fin()"><li class="fa fa-save"></li> Guardar</button>


            </div>





            {{Form::close()}}
        </div>
    </div>
</div>
<!-- /.card -->

<div class="modal fade" id="modal-nave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        {{Form::open(["route"=>"aeronaves.create", "onSubmit"=>"return false",  'id'=>'frmPrinc2','autocomplete'=>'on'])}}
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Aeronave</h4>

            </div>
            <div id="modal-nave-body" class="modal-body">
                <div class="row">


                    <div class="col-sm-12 col-md-6" >
                        <div class="form-group">
                            <label for="matricula">Matr&iacute;cula</label>
                            {{Form::text("matricula", "", ["maxlength"=>"10", "required"=>"required", "class"=>"form-control required matricula", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="nombre">{{__('Aeronave')}}</label>
                            {{Form::text("nombre", "", ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="peso_maximo">Peso M&aacute;ximo de Despegue <span class="right badge badge-info">(Tn)</span></label>
                            {{Form::text("peso_maximo", "", ["maxlength"=>"6",  "required"=>"required", "class"=>"form-control required  money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                        </div>
                    </div> 



                </div>  
            </div>
            <div  class="modal-footer justify-content-between">

                <button  id="btn-clse-nave"  type="button" class="btn btn-secundary bg-danger"  data-dismiss="modal"><li class="fa fa-undo"></li> {{__('Cerrar')}}</button>

                <button  class="btn btn-primary" id="btn-save-nave"  type="button"  onclick="saveNave()"><li class="fa fa-save"></li> {{__('Save')}}</button>
            </div>
        </div>

        {{ Form::close() }} 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">





    document.addEventListener('DOMContentLoaded', function () {


        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $(".select2").select2();
        $("#matricula, #nombre, #peso_maximo").val("");
        $('.time').inputmask({alias: "datetime", inputFormat: "HH:MM", "clearIncomplete": true});
        $(".matricula").alphanum({
                allowNumeric: true,
                allowUpper: true,
                allowLower: true,
                allowSpace: false,
                allowOtherCharSets:false
                

            });
        
        $(document).on('select2:open', () => {
            document.querySelector('.select2-search__field').focus();
        });

        $('#tabFrm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $('#frmPrinc2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });


        $("#document2").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {

                findDato2();

            }
        });

        $("#placa").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {

                findPlaca();

            }
        });

        $("#document2").focus();

    });






    function findPlaca() {
        if ($("#placa").valid()) {
            $("#frmPrinc2").prepend(LOADING);
            $.get("{{url('get-placa')}}/" + $("#placa").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#aeronave, #peso_avion").prop("readonly", true);
                    $("#aeronave_id").val(response.data.crypt_id);
                    $("#aeronave").val(response.data.nombre);
                    $("#peso_avion").val(muestraFloat(response.data.peso_maximo / 1000, 2));
                    
                    
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Aeronave no Encontrada')}}"
                    });
                    $("#aeronave, #peso_avion").val("");


                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#aeronave, #peso_avion").prop("readonly", true);
                
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar la Placa')}}"
                });
            });
        }
    }


    function saveNave() {
        if ($("#frmPrinc2").valid()) {
            $("#modal-nave-body").prepend(LOADING);
            $("#btn-save-nave, #btn-clse-nave").prop("disabled", true);


            $.post("{{route('aeronaves.add')}}", $("#frmPrinc2").serialize(), function (response) {
                $("#btn-save-nave, #btn-clse-nave").prop("disabled", false);
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $("#placa").val(response.data.matricula);
                    $("#btn-clse-nave").click();
                    $("#btnFinPlaca").click();
                    
                }

            }).fail(function () {
                $("#btn-save-nave, #btn-clse-nave").prop("disabled", false);
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Crear la Aeronave')}}"
                });
            });
        }
    }


    function fin() {


        if ($("#tabFrm1").valid()) {
            Swal.fire({
                title: 'Esta Seguro que Desea Guardar el Vuelo?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#tabFrm1").prepend(LOADING);
                    $.post($("#tabFrm1").attr("action"), $("#tabFrm1").serialize(), function (response){
                        $(".overlay-wrapper").remove();
                        
                        
                        
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            Swal.fire({
                                title: 'Registrado Correctamente',
                                html: "Confirmaci&oacute;n",
                                icon: 'success',

                                confirmButtonColor: '#3085d6',

                                confirmButtonText: '<i class="fa fa-check"></i> Ok',


                            }).then((result) => {
                                if (result.isConfirmed) {
                                    document.location.reload();
                                }
                            });
                            
                            
                            $("#tabFrm1").trigger("reset");
                            $("#aeropuerto_id").trigger("change");
                        
                            //
                        }
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error al Guardar"
                        });
                        $(".overlay-wrapper").remove();
                        
                    })
                    
                }
            });
        }




    }





    function findDato2() {


        if ($("#type_document2, #document2").valid()) {

            $("#tab2").prepend(LOADING);
            $.get("{{url('get-data-piloto')}}/" + $("#type_document2").val() + "/" + $("#document2").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#piloto_id").val(response.data.crypt_id);
                    $("#piloto").val(response.data.full_name);





                    $("#piloto").prop("readonly", true);

                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#piloto").prop("readonly", false);
                    $("#piloto").focus();
                    $("#piloto_id, #piloto").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#piloto").prop("readonly", true);
                $("#piloto_id, #piloto").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });


        }


    }


</script>  




@endsection