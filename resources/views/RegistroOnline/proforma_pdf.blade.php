<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>PROFORMA</title>
    </head>
    <style>
        .page_break { page-break-before: always; }
    </style>
    <body>
        
        <table width="100%" border="0">
            <tr>
                <td style="width:40%" ><img style="width:100%; height: 80px;" src="{{url("/dist/img/logo4.png")}}" /></td>
                <td style="width:60%" >
                    <div style="text-align: center; font-weight: bold;   ">RIF.: G-20008992-0</div>
                    <div style="text-align: center; font-weight: bold;   " >FORMA LIBRE:</div>
                    <div style="text-align: center; font-weight: bold;   ">N&deg; de CONTROL:</div>
                    <div style="text-align: center; font-weight: bold;   ">{{showCode($RegistroOnline->id)}}</div>
                </td>
            </tr>
        </table>

        <h3 style="width:100%; text-align: center; margin-top: 5px; border-bottom: solid 1px gray; margin-bottom: 5px;">PROFORMA </h3>
        <table width="100%" cellspacing="0" border="1">
            <tr>

                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >FECHA DE EMISION</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{showDate($RegistroOnline->fecha_registro)}}</td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONDICION DE PAGO</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">CONTADO</td>

            </tr>

            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">R.I.F:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$RegistroOnline->piloto->documento}}</td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TEL&Eacute;FONO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">{{$RegistroOnline->piloto->telefono}}</td>

            </tr>
            {{--
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%">TASA PETRO:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" align="right" width="25%">{{muestraFloat($RegistroOnline->tasa_petro)}}</td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >TASA EURO:</td>
                <td style="  font-family: 'Ubuntu', sans-serif; font-size: 12px" align="right" width="25%">{{muestraFloat($RegistroOnline->tasa_euro)}}</td>

            </tr>
            --}}
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >RESPONSABLE:</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >{{$RegistroOnline->piloto->nombres}}</td>

            </tr>
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >DIRECCIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" ></td>

            </tr>
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" width="25%" >INFORMACIÓN:</td>
                <td style=" font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="3" >Fecha de Vuelo {{showDate($RegistroOnline->fecha)}},  Matrícula del Avión {{$RegistroOnline->aeronave->matricula}}, Aeronave {{$RegistroOnline->aeronave->nombre}}, Tipo de Vuelo {{$TIPOS_VUELOS[$RegistroOnline->tipo_id]}}, Aeropuerto de Salida {{$RegistroOnline->origen->nombre}}, Aeropuerto de Llegada {{$RegistroOnline->destino->nombre}}</td>

            </tr>
        </table>
        <table style="margin-top: 20px" width="100%" cellspacing="0" border="1">
            <tr>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px;   font-weight: bold;" colspan="2" ><strong>OBSERVACI&Oacute;N:</strong></td>

            </tr>
            <tr>
                <td style="width:10px;" ></td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px;   " >{{$RegistroOnline->observacion}}</td>

            </tr>
        </table>

        <table width="100%" style="margin-top: 5px;" cellspacing="0" border="1">
            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 12px" colspan="5" ><strong>DETALLES DE LA FACTURA</strong></td>
            </tr>

            <tr>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="10%" ><strong>CANT.</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="10%"><strong>CÓDIGO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="49%"><strong>CONCEPTO</strong></td>
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="11%"><strong>NOMENCLATURA</strong></td>
                {{-- <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="19%"><strong>MONTO BOLIVARES</strong></td> --}}
                <td style="text-align: center; font-weight: bold; font-family: 'Ubuntu', sans-serif; font-size: 10px" width="20%"><strong>MONTO {{$MonedaPago}}</strong></td>
            </tr>
            
            @if(count($tasa)>0)
            <tr>
                <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px;  text-align:left" >TASA</td>
            </tr>
            @endif
            
            @php
            $base_imponible = 0; // TODO LO QUE TENGA IVA
            $excento = 0; // TODO LO QUE NO TENGA IVA
            $sub_total = 0; // SUMAR TODO
            $iva = 0; 
            @endphp
            @foreach($tasa as $value)
            @php
            $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
            $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
            $sub_total += ($value['precio']*$value['categoria_aplicada']);
            $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
            @endphp
            <tr>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >1</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:8%; text-align:center" >{{$value['codigo']}}</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width: 57%"  >{{$value['descripcion'].' '.$value['iva2']}}</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >{{$RegistroOnline->pasajeros}} {{$value['nomenclatura']}}</td>
                {{-- <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right" >{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td> --}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;" align="right" >{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
            </tr>
            @endforeach
            
            @if(count($dosa)>0)
            <tr>
                <td colspan="5" style="font-family: 'Ubuntu', sans-serif; font-size: 10px;  text-align:left" >DOSA</td>
            </tr>
            @endif
            
            @foreach($dosa as $value)
            @php
            $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
            $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
            $sub_total += ($value['precio']*$value['categoria_aplicada']);
            $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
            @endphp
            <tr>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >1</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:8%; text-align:center" >{{$value['codigo']}}</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width: 57%"  >{{$value['descripcion'].' '.$value['iva2']}}</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >1 {{$value['nomenclatura']}}</td>
                {{-- <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right" >{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td> --}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;" align="right" >{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
            </tr>
            @endforeach  
            
            
            
            
            @foreach($extra as $value)
            @php
            $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
            $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
            $sub_total += ($value['precio']*$value['categoria_aplicada']);
            $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
            @endphp
            <tr>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:5%; text-align:center" >1</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:8%; text-align:center" >{{$value['codigo']}}</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width: 67%"  >{{$value['descripcion'].' '.$value['iva2']}}</td>
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%; text-align:center" >1 {{$value['nomenclatura']}}</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right" >{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;" align="right" >{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
            </tr>
            @endforeach
            
            
            
            <tr>    

                <td colspan="4" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">BASE IMPONIBLE</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($base_imponible* $RegistroOnline->{"tasa_".strtolower($MonedaPago)})  }}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($base_imponible)}}</td>
            </tr>

            <tr>
                <td colspan="4" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">EXENTO</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($excento* $RegistroOnline->{"tasa_".strtolower($MonedaPago)})}}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($excento)}}</td>


            </tr>

            <tr>

                <td colspan="4" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">SUBTOTAL</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($sub_total* $RegistroOnline->{"tasa_".strtolower($MonedaPago)})}}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($sub_total)}}</td>


            </tr>

            <tr>
                <td colspan="4" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">DESCUENTO</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat(0)}}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat(0)}}</td>


            </tr>

            <tr>
                <td colspan="4" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">IVA (16,00%)</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($iva* $RegistroOnline->{"tasa_".strtolower($MonedaPago)})}}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($iva)}}</td>



            </tr>

            <tr>

                <td colspan="4" style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:80%;  font-weight: bold;"  align="right" align="right">TOTAL</td>
                {{--<td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat(($sub_total+$iva)* $RegistroOnline->{"tasa_".strtolower($MonedaPago)})}}</td>--}}
                <td style="font-family: 'Ubuntu', sans-serif; font-size: 10px; width:10%;"  align="right">{{muestraFloat($sub_total+$iva)}}</td>



            </tr>
        </table>
        <div style="  clear:both; left:0px; bottom: 0;  position: fixed;  width:100%; text-align: center; font-family: 'Ubuntu', sans-serif; font-size: 10px; ">

            Av. Venezuela, Urb. El Rosal, Edif. Antigua sede FONTUR, Municipio Chacao, Caracas Distrito Capital, Venezuela

        </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    </body>
</html>
