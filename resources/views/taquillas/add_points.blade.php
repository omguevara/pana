<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Administrar Puntos de Ventas a las Taquillas')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('taquillas')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>



            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
             <div class="row">
                
                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label for="codigo">{{__('Código')}}</label>
                        {{Form::text("codigo", $Taquillas->codigo, ["maxlength"=>"10", "readonly"=>"readonly" ,"required"=>"required", "class"=>"form-control required", "id"=>"codigo", "placeholder"=>__('Código')])}}    

                    </div>
                </div>
                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label for="nombre">{{__('Nombre')}}</label>
                        {{Form::text("nombre", $Taquillas->nombre, ["required"=>"required", "readonly"=>"readonly", "class"=>"form-control required", "id"=>"nombre", "placeholder"=>__('Nombre')])}}    

                    </div>
                </div>

                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label for="aeropuerto_id">{{__('Aeropuerto')}}</label>
                        {{Form::select('aeropuerto_id', $aeropuertos, $Taquillas->aeropuerto->crypt_id, ["disabled"=>"disabled" , "class"=>"form-control ", "id"=>"aeropuerto_id", "placeholder"=>__('Seleccione') ,"required"=>"required"])}}

                    </div>
                </div>
            </div>
            {{Form::open(["route"=>["taquillas.add_points", $Taquillas->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="serial">{{__('Serial')}}</label>
                        {{Form::text("serial", "", ["required"=>"required", "class"=>"form-control required", "id"=>"Serial", "placeholder"=>__('Serial')])}}    

                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="banco_id">{{__('Banco')}}</label>
                        {{Form::select('banco_id', $bancos, "", [ "class"=>"form-control select2 ", "id"=>"banco_id", "placeholder"=>__('Seleccione') ,"required"=>"required"])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-12">
                {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
            </div>
            {{ Form::close() }} 
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>{{__('Banco')}}</th>
                                <th>{{__('Serial')}}</th>

                                <th>{{__('Actions')}}</th>

                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>



                    
                        <!-- /.card -->
                    

                </div>

            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    var data1 = @json($Puntos)
            ;
    
    $(function () {
        $(".select2").select2();
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('taquillas.add_points', $Taquillas->crypt_id)}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });





        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            initComplete: function (){
                $('form.frm2').on("submit", function (event) {
                event.preventDefault();
                
                    $("#panel_princ").prepend(LOADING);
                    $.post(this.action, $(this).serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('taquillas.add_points', $Taquillas->crypt_id)}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#panel_princ").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#panel_princ").find(".overlay-wrapper").remove();
                        }
                        //console.log(response);
                    }).fail(function () {
                        $("#panel_princ").find(".overlay-wrapper").remove();
                    });
                
                return false;
            });
            },
            columns: [
                {data: 'banco.nombre'},
                {data: 'serial'},
                
                
                {data: function (data){
                        form = '<form class="frm2" action ="{{url("/taquillas/add-points")}}/'+data.crypt_id+'" method="post">';
                        form += '<input type="hidden" name="_method" value="delete" />';
                        form += '<input type="hidden" name="_token" value="{{ csrf_token() }}">';
                        form += '{{Form::button(__("Borrar"),  ["type"=>"submit", "class"=>"btn btn-warning", "id"=>"delete"])}}';    
                        form += '</form>';
                        return form;
                }}
            ]
            @if (app()-> getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        });
        
        
    });
</script>