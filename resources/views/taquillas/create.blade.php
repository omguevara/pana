

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Taquilla')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('taquillas')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>



            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"taquillas.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="codigo">{{__('Código')}}</label>
                        {{Form::text("codigo", "", ["maxlength"=>"10", "required"=>"required", "class"=>"form-control required", "id"=>"codigo", "placeholder"=>__('Código')])}}    

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="nombre">{{__('Nombre')}}</label>
                        {{Form::text("nombre", "", ["required"=>"required", "class"=>"form-control required", "id"=>"nombre", "placeholder"=>__('Nombre')])}}    

                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="aeropuerto_id">{{__('Aeropuerto')}}</label>
                        {{Form::select('aeropuerto_id', $aeropuertos, "", [ "class"=>"form-control select2", "id"=>"aeropuerto_id", "required"=>"required"])}}

                    </div>
                </div>


                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="seguridad">{{__('Clave de Seguridad')}}</label>
                        {{Form::text("seguridad", $hash, ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required", "id"=>"seguridad", "placeholder"=>__('Clave de Seguridad')])}}    

                    </div>
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="numero_factura">{{__('Número de Factura')}}</label>
                        {{Form::text("numero_factura", "", ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required number", "id"=>"numero_factura", "placeholder"=>__('Numero de Factura')])}}    

                    </div>
                </div>


                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="numero_control">{{__('Número de Control')}}</label>
                        {{Form::text("numero_control", "", ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required number", "id"=>"numero_control", "placeholder"=>__('Numero de Control')])}}    

                    </div>
                </div>




            </div>  
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    

        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
</div>





<script type="text/javascript">




    $(document).ready(function () {
        $(".select2").select2();
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('taquillas')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
