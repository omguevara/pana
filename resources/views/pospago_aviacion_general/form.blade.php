



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturacion Pospago')}}</h3>

            <div id="toolID" class="card-tools">
                
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"reporte_vuelos_tortuga",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">


            <div class="row">
                <div class="col-12 col-sm-4">
                    <div class="row">
                        
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, "", ["placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 ", "id"=>"aeronave_id" ,"required"=>"required"])}}
                            
                        </div>
                        
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Rango de Fechas :</b></span>
                            </div>
                            {{Form::text("rango", "", ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                            <div class="input-group-append">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            
                            <div onClick="setParamsSearch()" style="cursor:pointer" class="input-group-append">
                                <div  class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                            

                        </div>
                        
                        
                        
                        
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <table id="example1" class="table table-sm">
                    <thead>
                        <tr>
                            <th>{{__('Fecha de Vuelo')}}</th>
                            <th>{{__('Piloto')}}</th>
                            <th>{{__('total')}}</th>
                            
                        </tr>
                    </thead>
                    <tbody id="cuerpoTabla">
                      
                   
                   
                        
                     </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
    
  
    <style>
        .dt-buttons .btn-group{
            float:right;
        }
    </style>        
    







    <script type="text/javascript">
        var data1 =[];
        var dataTableExample1;
        $(document).ready(function () {


            

            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

        });
     
    function setParamsSearch(){
        $("#frmPrinc1").prepend(LOADING);
            $.post("{{route('facturacion_pospago')}}", $("#frmPrinc1").serialize() ,function (response){
                $(".overlay-wrapper").remove();
                
                $(".rowFil").remove();
                if (response.status == 1){
                   sum = 0; 
                   for(i in response.data){
                       sum +=response.data[i].total;
                       tr = '<tr class="rowFil">';
                       tr +='<td>'+showDate(response.data[i].fecha_llegada)+'</td>';     
                       tr +='<td>'+response.data[i].piloto_llegada.full_name+'</td>';     
                       tr +='<td>'+muestraFloat(response.data[i].total)+'</td>';     
                       tr += '</tr>';
                       
                       $("#cuerpoTabla").append(tr);
                   }
                   
                    tr = '<tr class="rowFil">';
                    tr +='<td colspan="2">TOTAL</td>';     
                    
                    tr +='<td>'+muestraFloat(sum)+'</td>';     
                    tr += '</tr>';
                     $("#cuerpoTabla").append(tr);

                }


            }, 'json').fail(function (){
                $(".overlay-wrapper").remove();
            });
    }


    </script>


</div>


