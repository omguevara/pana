



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registro de Aviación General Pospago')}}</h3>

            <div class="card-tools">



            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"pospago_aviacion_general",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        {{Form::select('prod_servd[]', [], "", ["style"=>"display:none", "id"=>"prod_servd" ,"multiple"=>"multiple"])}}
        {{Form::hidden('servicios_facturar', "", ["id"=>"servicios_facturar" ])}}
        
        
        {{Form::hidden('type_document', "", [ "id"=>"type_document"])}}
        {{Form::hidden("document", "", ["id"=>"document"])}}    
        {{Form::hidden("razon", "", [ "id"=>"razon"])}}      
        {{Form::hidden("phone", "", ["id"=>"phone"])}}    
        {{Form::hidden("correo", "", ["id"=>"correo"])}}    
        {{Form::hidden("direccion", "", ["id"=>"direccion"])}}    
        
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-6   ">
                    <div class="row">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeropuerto:</b></span>
                            </div>
                            {{Form::select('aeropuerto_id', $Aeropuertos, (count($Aeropuertos)==1 ? key($Aeropuertos):"" ), ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 uno", "id"=>"aeropuerto_id" ,"required"=>"required"], $AeropuertosProp)}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, "", ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"], $AeronavesProp)}}
                            <div class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, "", ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div  title="Agregar Nuevo Piloto" onClick="$('#modal-princ-pilotos').modal('show')" class="input-group-text">



                                    <i class="fa fa-plus"></i>

                                </div>
                            </div>
                        </div>
                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required uno", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>



                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Nacionalidad:</b></span>
                            </div>
                            {{Form::select('nacionalidad_id', ["1"=>"Nacional", "2"=>"Internacional"], 1, ["onChange"=>"$(this).valid()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"nacionalidad_id" ,"required"=>"required"])}}









                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", "", ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number uno ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad de Pasajeros')])}}    
                        </div>




                        <div  class="input-group mt-2 time_estadia">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Tasa?:</b></span>
                            </div>
                            {{Form::select('cobrar_tasa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_tasa" ,"required"=>"required"])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Dosa?:</b></span>
                            </div>
                            {{Form::select('cobrar_dosa', ["1"=>"Si", "0"=>"No"], 1, ["class"=>"form-control  uno ", "id"=>"cobrar_dosa" ,"required"=>"required"])}}
                        </div>


                        <div class="form-group">
                            <label class="">Horas Estacionamiento:</label>
                            <div  class="input-group mt-2 time_estadia">


                                <div class="input-group-prepend">

                                    <span class="input-group-text">


                                        <div class="icheck-success d-inline">
                                            <input style="min-height: unset; margin-top: unset; margin-bottom: unset;" checked type="checkbox" vaue="1" name="applyParking" checked id="applyParking">
                                            <label for="applyParking"></label>
                                        </div>

                                        <b>Hora Llegada  (24H):</b>
                                    </span>
                                </div>

                                {{Form::text("hora_llegada", (isset($VuelosGenerales['hora_llegada'])? $VuelosGenerales['hora_llegada']:""), [ "class"=>"form-control   timer uno", "id"=>"hora_llegada", "placeholder"=>__('Hora Llegada')])}}    
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Hora Salida  (24H):</b></span>
                                </div>
                                {{Form::text("hora_salida", (isset($VuelosGenerales['hora_salida'])? $VuelosGenerales['hora_salida']:""), [ "class"=>"form-control   timer uno", "id"=>"hora_salida", "placeholder"=>__('Hora Salida')])}}    
                            </div>
                        </div>








                    </div>  
                    <div class="rows  mt-2 ">
                        {{Form::button('<li class="fa fa-plus"></li> '.__("Servicios").' <li class="fa fa-caret-up"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"addProds"])}}    
                        {{Form::button('<li class="fa fa-euro-sign"></li> '.__("Ver Proforma").' <li class="fa fa-caret-right"></li>',  ["type"=>"button", "class"=>"btn btn-primary float-right", "id"=>"next1"])}}    
                    </div>

                </div>  
                <div   class="col-sm-12 col-md-6   ">
                    <h2>PROFORMA</h2>
                    <table style="width:100%" id="datFactura">
                        <tr>
                            <td style="width:5%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                            <td style="width:45%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>

                    </table>


                    <div class="rows  mt-2 ">
                        {{Form::button('<li class="fa fa-save"></li> '.__("Facturar").' <li class="fa fa-caret-right"></li>',  ["onClick"=>"Facturar()", "type"=>"button", "class"=>"btn btn-primary "])}}    
                        {{Form::button('<li class="fa fa-save"></li> '.__("Guardar Pospago").' <li class="fa fa-caret-right"></li>',  ["onClick"=>"Pospago()", "type"=>"button", "class"=>"btn btn-primary float-right"])}}    
                    </div>




                </div>


            </div>  

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>


    <div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nuevo Piloto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"pospago_aviacion_general.add_piloto",  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                <div id="modalPrincBodyFact" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="type_document">{{__('Tipo de Documento')}}</label>
                                {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>
                                {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="name_user">{{__('Name')}}</label>
                                {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="surname_user">{{__('Surname')}}</label>
                                {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                            </div>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"pospago_aviacion_general.add_nave",  'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6" >
                            <div class="form-group">
                                <label for="matricula">{{__('Matrícula')}}</label>
                                {{Form::text("matricula", "", ["required"=>"required", "maxlength"=>"10" ,"class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="nombre">{{__('Modelo')}}</label>
                                {{Form::text("nombre", "", ["required"=>"required", "maxlength"=>"20" , "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                                {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-princ-serv-extra"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalExt" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Agregar Servicios Extra</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"pospago_aviacion_general",  'id'=>'frmServ','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-12" >

                            <table style="width:100%" id="tableServExt">
                                <tr>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"></td>
                                    <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                                    <td style="width:60%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                                    <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                                </tr>

                            </table>

                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Aplicar"),  ["type"=>"button", "class"=>"btn btn-primary",  "id"=>"aplicar"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


    <div class="modal fade" id="modal-princ-facturar"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modal-fact-1" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Datos del Responsable de Pago</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"pospago_aviacion_general",  'id'=>'frmCliente','autocomplete'=>'Off'])}}
                <div  class="modal-body">
                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Documento:</b></span>
                        </div>
                        {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", ["style"=>"max-width:80px", "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                        {{Form::text("document", "", ["onBlur"=>"findDato2()", "data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required dos", "id"=>"document", "placeholder"=>__('Document')])}}    
                        <div class="input-group-append" >
                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                        </div>
                    </div>

                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>Responsable:</b></span>
                        </div>

                        {{Form::text("razon", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos alpha", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
                    </div>
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                        </div>

                        {{Form::text("phone", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone dos", "id"=>"phone", "placeholder"=>__('Phone')])}}    
                    </div>
                    
                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Correo')}}:</b></span>
                        </div>

                        {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  dos", "id"=>"correo", "placeholder"=>__('Correo')])}}    
                    </div>

                    <div class="input-group mt-2 ">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                        </div>

                        {{Form::text("direccion", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                    </div>


                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Guardar"),  ["type"=>"button", "class"=>"btn btn-primary", "onClick"=>"saveFactura()" , "id"=>"guardarFact"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>





    <script type="text/javascript">

        var TOTAL_FACTURA;
        var PRODSERVICIOS_AGREGADOS


        $(document).ready(function () {
            $(".select2").select2();

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });

            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });

            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('.timer').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy HH:MM"});
            $('#iconDate').datetimepicker({icons: {time: 'far fa-clock'}});
            $('.number').numeric({negative: false});

            $("#modal-fact").on('shown.bs.modal', function () {
                $("#totalEuro").html(muestraFloat(TOTAL_FACTURA, 2));
                $("#totalBs").html(muestraFloat(TOTAL_FACTURA * VALOR_EURO, 2));
            });

            $("#nro_factura").val($("#aeropuerto_id").children(":selected").data("numero_factura"));
            $("#nro_control").val($("#aeropuerto_id").children(":selected").data("numero_control"));

            $("#aeropuerto_id").on("change", function () {
                $("#nro_factura").val($(this).children(":selected").data("numero_factura"));
                $("#nro_control").val($(this).children(":selected").data("numero_control"));

                if ($(this).val() == $("#aeronave_id").children(":selected").data("base")) {
                    $("#applyParking").prop("checked", false);
                    evaluaChecked(false);
                } else {
                    $("#applyParking").prop("checked", true);
                    evaluaChecked(true);
                }



            });


            $("#applyParking").on("click", function () {

                if ($("#aeropuerto_id").val() == $("#aeronave_id").children(":selected").data("base") && this.checked) {
                    Swal.fire({
                        icon: 'warning',
                        title: 'INFORMACIÓN',
                        html: 'Alerta el Aeropuerto <b>' + $("#aeropuerto_id").children(":selected").text() + '</b> es la Base de la Aeronave <b>' + $("#aeronave_id").children(":selected").text() + '</b>, si Marca Esta Opción se le cobrara el tiempo de estacionamiento'
                    })

                }

                evaluaChecked(this.checked);


            });

            $("#aeronave_id").on("change", function () {
                if ($("#aeropuerto_id").val() == $(this).children(":selected").data("base")) {
                    $("#applyParking").prop("checked", false);
                    evaluaChecked(false);
                } else {
                    $("#applyParking").prop("checked", true);
                    evaluaChecked(true);
                }
            });





            $("#addProds").on("click", function () {
                if ($(".uno").valid()) {

                    $("#modal-princ-serv-extra").modal();
                    $("#modalExt").prepend(LOADING);
                    $("#prod_servd").html("");
                    $.post("{{route('pospago_aviacion_general.get_serv_extra')}}", $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        $(".filaServExt").remove();
                        sum = 0;
                        base_imponible = 0;
                        excento = 0;
                        iva = 0;
                        for (i in response.data) {
                            sum += parseFloat(response.data[i]['precio'], 10);
                            mult = parseFloat(response.data[i]['precio'], 10);
                            cant = response.data[i]['cant'];

                            if (response.data[i]['iva_aplicado'] > 0) {
                                base_imponible += mult;
                                iva += mult * response.data[i]['iva_aplicado'] / 100;
                            } else {
                                excento += parseFloat(mult, 10);

                            }


                            tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaServExt">';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input type="checkbox" class="form-control selectItem" onClick="selectItem(this)" data-crypt_id="' + response.data[i]['crypt_id'] + '" /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><input type="number" id="' + response.data[i]['crypt_id'] + '" onChange="UpItemSelect()" value="0" class="form-control" disabled /></td>';
                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + ' ' + response.data[i]['iva2'] + '</td>';

                            tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                            tabla += '</tr>';
                            $("#tableServExt").append(tabla);
                        }


                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "Error al Consultar los Servicios"
                        });
                    });


                }
            });

            $("#aplicar").on("click", function () {
                $("#modal-princ-serv-extra").modal("hide");
                $('#next1').click();
            });

            $("#next1").on("click", function () {
                if ($(".uno").valid()) {

                    f1 = moment($("#hora_llegada").val(), 'DD/MM/YYYY HH:mm');
                    f2 = moment($("#hora_salida").val(), 'DD/MM/YYYY HH:mm');

                    if (f2.isAfter(f1) || !$("#applyParking").prop("checked")) {



                        $("#panel_princ").prepend(LOADING);
                        $.post("{{route('pospago_aviacion_general.get_serv')}}", $("#frmPrinc1").serialize(), function (response) {

                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            PRODSERVICIOS_AGREGADOS = response.data;
                            construcTable();

                        }, 'json').fail(function () {
                            $(".overlay-wrapper").remove();
                        });
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "La Hora de Llegada no Puede ser Mayor a la Hora de Salida"
                        });
                    }
                } else {

                }
            });

            $("#next2").on("click", function () {
                if ($(".uno").valid() && $(".dos").valid()) {
                    $("#modal-fact").modal();

                } else {

                }
            });



            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


            $('#frmPilotos').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });



            $('#frmPilotos').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPilotos').valid()) {
                    $("#frmPilotos").prepend(LOADING);
                    $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmPilotos").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#piloto_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                            $("#piloto_id").val(response.data.id);
                            $("#piloto_id").trigger("reset");
                            $('#frmPilotos').trigger("reset");
                            $("#modal-princ-pilotos").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    $("#frmNave").prepend(LOADING);
                    $.post(this.action, $("#frmNave").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmNave").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#aeronave_id").append('<option  value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                            $("#aeronave_id").val(response.data.id);
                            $("#aeronave_id").trigger("reset");
                            $('#frmNave').trigger("reset");
                            $("#modal-princ-aeronave").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });


            $('#document').bind('keypress', function (e) {
                if (e.keyCode == 13) {
                    findDato2();
                }
            });

        });



        function findDato2() {


            if ($("#frmCliente #type_document, #frmCliente #document").valid()) {
                $("#frmCliente").prepend(LOADING);
                $.get("{{url('get-data-cliente')}}/" + $("#frmCliente #type_document").val() + "/" + $("#frmCliente #document").val(), function (response) {
                    $(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#frmCliente #cliente_id").val(response.data.crypt_id);
                        $("#frmCliente #razon").val(response.data.razon_social);
                        $("#frmCliente #phone").val(response.data.telefono);
                        $("#frmCliente #correo").val(response.data.correo);
                        $("#frmCliente #direccion").val(response.data.direccion);
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                        Toast.fire({
                            icon: "success",
                            title: "{{__('Datos Encontrados')}}"
                        });
                    } else {
                        Toast.fire({
                            icon: "warning",
                            title: "{{__('Datos no Encontrados, Registrelos')}}"
                        });
                        $("#frmCliente #razon").focus();
                        $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", false);
                        $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                    }


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    $("#frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").prop("readonly", true);
                    $("#frmCliente #cliente_id, #frmCliente #razon, #frmCliente #phone, #frmCliente #correo, #frmCliente #direccion").val("");
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Consultar los Datos')}}"
                    });
                });
            }


        }

        function clsFP() {
            $(".money").val("0,00");
        }

        function clsPOS() {
            $(".pos").val("");
        }

        function getMonto() {
            T = 0;
            $(".money").each(function () {
                T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
            });
            return T;
            // muestra

        }

        function Pospago() {


            //porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
            //if (porcentaje >= 98) {

            if ($("#frmPrinc1").valid()) {
                if ($(".filaFactura").length > 0) {

                    $("#servicios_facturar").val(JSON.stringify(PRODSERVICIOS_AGREGADOS));

                    Swal.fire({
                        title: 'Esta Seguro que Desea Registrar el Vuelo?',
                        html: "Confirmaci&oacute;n",
                        icon: 'question',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Si',
                        cancelButtonText: 'No'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $("#modal-fact").modal('hide');
                            $("#main-content").prepend(LOADING);
                            $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                                $(".overlay-wrapper").remove();
                                // Swal.close();
                                //$("#modal-fact").modal('hide');
                                //$("#main-content").html(response);

                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                                $(".overlay-wrapper").remove();
                                if (response.status == 1) {
                                    $(".filaFactura").remove();
                                    $("#frmPrinc1").trigger("reset");
                                } else {

                                }





                            }).fail(function () {
                                $(".overlay-wrapper").remove();
                                Toast.fire({
                                    icon: "error",
                                    title: "{{__('Error al Guardar')}}"
                                });
                            });


                        }
                    });

                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('No Existen Servicios a Facturar')}}"
                    });
                }







            }
            /*} else {
             Toast.fire({
             icon: "error",
             title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
             });
             $(".money").eq(0).focus();
             }*/
        }
        function evaluaChecked(status) {

            $(".timer").prop("disabled", !status);
            if (status != true) {
                $("#hora_llegada, #hora_salida").removeClass("required");
                $(".timer").val("");
            } else {
                $("#hora_llegada, #hora_salida").addClass("required");
            }
            $("#hora_llegada, #hora_salida").valid();
        }



        function selectItem(obj) {
            id = $(obj).data("crypt_id");
            if (obj.checked == true) {
                $("#" + id).val("1").prop("disabled", false).focus();
            } else {
                $("#" + id).val("0").prop("disabled", true);
            }
            UpItemSelect();
        }

        function UpItemSelect() {
            $("#prod_servd").html("");
            $(".selectItem:checked").each(function () {
                Id = $(this).data('crypt_id');
                $("#prod_servd").append('<option selected value="' + $("#" + Id).val() + ':' + Id + '"></option>');
            });
        }
        function deleteItem(id) {
            delete PRODSERVICIOS_AGREGADOS[id];
            construcTable();
        }

        function construcTable() {

            $(".filaFactura").remove();
            sum = 0;
            base_imponible = 0;
            excento = 0;
            iva = 0;

            for (i in PRODSERVICIOS_AGREGADOS) {
                //console.log(response.data[i]['categoria_aplicada']);
                sum += parseFloat(PRODSERVICIOS_AGREGADOS[i]['precio'], 10);
                mult = parseFloat(PRODSERVICIOS_AGREGADOS[i]['precio'], 10);
                cant = PRODSERVICIOS_AGREGADOS[i]['cant'];

                if (PRODSERVICIOS_AGREGADOS[i]['iva_aplicado'] > 0) {
                    base_imponible += mult;
                    iva += mult * PRODSERVICIOS_AGREGADOS[i]['iva_aplicado'] / 100;
                } else {
                    excento += parseFloat(mult, 10);

                }
                idProdServs = '<input type="hidden" name="prods_extra[]" value="' + PRODSERVICIOS_AGREGADOS[i]['crypt_id'] + '" />';




                //sum += response.data[i]['cant'] * response.data[i]['precio'] * response.data[i]['categoria_aplicada'] ;
                tabla = '<tr title="' + PRODSERVICIOS_AGREGADOS[i]['formula2'] + '" class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; "><span onclick="deleteItem(' + i + ')" style="padding: 5px; cursor:pointer; border:solid 1px #BBD8FB; background-color: #F3F7FD" class="fa fa-times"></span></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + idProdServs + cant + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + PRODSERVICIOS_AGREGADOS[i]['full_descripcion'] + ' ' + PRODSERVICIOS_AGREGADOS[i]['iva2'] + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (cant + ' ' + PRODSERVICIOS_AGREGADOS[i]['nomenclatura'] || '') + '</td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                tabla += '</tr>';
                $("#datFactura").append(tabla);
            }
            TOTAL_FACTURA = sum;
            MontoGlobalBs = sum * VALOR_EURO;
            if (TOTAL_FACTURA > 0) {
                tabla = '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>BASE IMPONIBLE</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>EXCENTO</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                tabla += '</tr>';



                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>SUB TOTAL</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="4"><b>IVA</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                tabla += '</tr>';

                tabla += '<tr class="filaFactura">';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="4"><b>TOTAL</b></td>';
                tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                tabla += '</tr>';

                $("#datFactura").append(tabla);
            }

        }


        function Facturar() {
            if ($("#frmPrinc1").valid()) {
                if ($(".filaFactura").length > 0) {

                    $("#modal-princ-facturar").modal('show');

                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('No Existen Servicios a Facturar')}}"
                    });
                }







            }
        }
        function saveFactura() {
            if ($("#frmCliente").valid()) {
                Swal.fire({
                    title: 'Esta Seguro que Desea Facturar el Vuelo?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.isConfirmed) {
                        
                        
                        $("#frmPrinc1 #type_document").val( $("#frmCliente #type_document").val());
                        $("#frmPrinc1 #document").val( $("#frmCliente #document").val());
                        $("#frmPrinc1 #razon").val($("#frmCliente #razon").val());
                        $("#frmPrinc1 #cliente").val( $("#frmCliente #cliente_id").val());
                        $("#frmPrinc1 #razon").val($("#frmCliente #razon").val());
                        $("#frmPrinc1 #phone").val($("#frmCliente #phone").val());
                        $("#frmPrinc1 #correo").val($("#frmCliente #correo").val());
                        $("#frmPrinc1 #direccion").val($("#frmCliente #direccion").val());
                        
                  
                        
                        
                        $("#servicios_facturar").val(JSON.stringify(PRODSERVICIOS_AGREGADOS));
                        $("#modal-fact").modal('hide');
                        $("#main-content").prepend(LOADING);
                        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize() + "&facturar=true", function (response) {
                            $(".overlay-wrapper").remove();


                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $(".overlay-wrapper").remove();
                             $("#modal-princ-facturar").modal('hide');
                            if (response.status == 1) {
                                $(".filaFactura").remove();
                                $("#frmPrinc1").trigger("reset");
                                if (response.data.id != ""){
                                    window.open("{{url('view-factura-print-tortuga')}}/"+response.data.id);
                                }
                                
                                
                                
                            } else {

                            }





                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });
            }
        }

    </script>

</div>