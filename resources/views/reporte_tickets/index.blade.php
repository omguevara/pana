<div id="panel_princ" class="col-sm-12  mt-1">
    <style>
        .subir {
            width: 100%;
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        .subir .file-upload {
            height: 100px;
            width: 100px;
            border-radius: 100px;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 4px solid #FFFFFF;
            overflow: hidden;
            background-image: linear-gradient(to bottom, #2590EB 50%, #FFFFFF 50%);
            background-size: 100% 200%;
            transition: all 1s;
            color: #FFFFFF;
            font-size: 50px;
        }
        .subir .file-upload input[type=file] {
            height: 100px;
            width: 100px;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            cursor: pointer;
        }
        .subir .file-upload:hover {
            background-position: 0 -100%;
            color: #2590EB;
        }
        .file-name {
            font-size: 16px;
            font-weight: bold;
            color: #333;
            background-color: #f5f5f5;
            padding: 5px 10px;
            border-radius: 5px;
            }
    </style>


    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Reporte de Cupones')}} </h3>
            

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <form action="{{ route('ExcelFactura') }}" id="frmReportesExcel" method="GET">
            <div id="card-body-main" class="card-body ">
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                        <div class="row">      
                            <div class="input-group mt-2 mr-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeropuertos:</b></span>
                                </div>
                                {{Form::select('aeropuerto_id', $Aeropuertos, "", [ "required"=>'required',   "placeholder"=>"Seleccione",     "class"=>"form-control required select2", "id"=>"aeropuerto_id" ])}}
                            </div>                            
                        </div>
                    </div>
                    <div class="btn-group">
                        <div class="input-group mt-2">
                            <div class="input-group-append">
                                <p class="input-group-text"><b>Rango de Fecha</b></p>
                            </div>
                            {!! Form::text('rango_fecha', '', ['class' => 'form-control required', 'id' => 'rango_fecha']) !!}
                        </div>
                    </div>
                    <div class="btn-group">
                        <div class="input-group mt-2 ml-2 ">
                                @csrf

                                <button type="button" id="down_Ticket" class="btn btn-success ml-1"><i class="far fa-file-excel"></i> Descargar Cupones</button>
                        
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>









    <script type="text/javascript">

        $(document).ready(function () {
            $("#down_Ticket").on("click", function(){
                $("#frmReportesExcel").attr('action', '{{ route("ExcelTickets") }}');
                $("#panel_princ").prepend(LOADING);
                $.post('{{ route("ExcelTickets") }}', $("#frmReportesExcel").serialize(), function(response){
                    $("#panel_princ").find(".overlay-wrapper").remove();
                    if (response.status == 0) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                    }else{
                        $("#frmReportesExcel").submit();
                    }
                });
            });
            $('input[name="rango_fecha"]').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });
           
        });
        $("#aeropuerto_id").select2({
            placeholder: "Seleccione",
            allowClear: true
        });

        
    </script>


</div>


