 {{Form::open(["route"=>"aviacion_general",  'id'=>'frmAddProdServ','autocomplete'=>'Off'])}}
<div class="row">

    <div class="col-sm-12 col-md-12">
        <div class="row">
            
            <div class="col-sm-12 col-md-12 ">

                <div class="form-group">
                    <label for="prodservs_id">{{__('Servicios Adicionales')}}</label>
                    {{Form::select("prodservs_id[]", $listPrdServs, "", ["multiple"=>"multiple", "required"=>"required", "class"=>"form-control  prodserv_select2  required", "id"=>"prodservs_id"], $selected)}}    

                </div>
            </div>

            <div class="col-sm-12 col-md-12 text-center">
                {{Form::button("<li class='fas fa-save'></li> Aplicar Servicios", ["class"=>"btn btn-success", "onClick"=>"aplicarServs()"])}}
            </div>
        </div>
    </div>
   


</div>
{{ Form::close() }} 


<script type="text/javascript">
    
    
    $('#frmAddProdServ').on("submit", function (event) {
        event.preventDefault();
        return false;
    });
    

    $(document).ready(function () {
        $("#prodservs_id").select2();
        
        $('.select2-search__field').focus();
        
    });

    function aplicarServs() {
        
            $("#frmAddProdServ").prepend(LOADING);
            $.post("{{url('/aviacion-general/add-prodserv')}}/{{$Operacion->crypt_id}}", $("#frmAddProdServ").serialize(), function (response) {
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#modal-princ-prodserv").modal("hide");
                    //refreshData();
                }
                //console.log(response);
            }).fail(function () {
                Toast.fire({
                    icon: "error",
                    title: "Error al Guardar"
                });
                $(".overlay-wrapper").remove();
            });
        
    }

</script>