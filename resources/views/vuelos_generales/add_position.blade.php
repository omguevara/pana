{{Form::open(["route"=>["aviacion_general.add_position", $Operacion->crypt_id],  'id'=>'frmAddPos','autocomplete'=>'Off'])}}
<div class="row">

    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-3 ">

                <div class="form-group">
                    <label for="matricula_id">{{$Operacion->aeronave->full_nombre}}</label>


                </div>
            </div>


        </div>
    </div>
    <div class="col-12 col-sm-12">
        <div class="card card-primary card-tabs">
            <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                    @foreach($Aeropuerto->ubicaciones as $key=>$value)
                    <li class="nav-item">
                        <a class="nav-link {{($key==0? 'active':'')}}" id="tab-{{$value->crypt_id}}" data-toggle="pill" href="#content-tab-{{$value->crypt_id}}" role="tab" aria-controls="content-tab-{{$value->crypt_id}}" aria-selected="true">
                            {{$value->nombre}}
                        </a>
                    </li>
                    @endforeach

                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                    @foreach($Aeropuerto->ubicaciones as $key=>$value)
                    <div class="tab-pane fade {{($key==0? 'show active':'')}}" id="content-tab-{{$value->crypt_id}}" role="tabpanel" aria-labelledby="tab-{{$value->crypt_id}}">
                        <div class="row">

                            <div class="col-5 col-sm-3">
                                <div class="nav flex-column nav-tabs h-100"  role="tablist" aria-orientation="vertical">
                                    @foreach($value->lugares as $key2=>$value2)
                                    <a class="nav-link {{($key2==0? 'active':'')}}" id="vert-tabs-{{$value2->crypt_id}}" data-toggle="pill" href="#vert-tabs-content-{{$value2->crypt_id}}" role="tab" aria-controls="vert-tabs-content-{{$value2->crypt_id}}" aria-selected="true">
                                        {{$value2->nombre}}
                                    </a>
                                    @endforeach

                                </div>
                            </div>
                            <div class="col-7 col-sm-9">
                                <div class="tab-content" >
                                    @foreach($value->lugares as $key2=>$value2)
                                    <div class="tab-pane text-left fade {{($key2==0? 'show active':'')}}" id="vert-tabs-content-{{$value2->crypt_id}}" role="tabpanel" aria-labelledby="vert-tabs-{{$value2->crypt_id}}">
                                        <div class="row">
                                            @foreach($value2->puestos as $key3=>$value3)
                                            @php
                                            if ($value3->aeronave_id ==null ){
                                            $click = "onClick=\"selectPuesto(this, '".$value3->crypt_id."', '".$Operacion->aeronave->full_nombre."')\" ";
                                            }else{
                                            $click = '';
                                            }
                                            @endphp
                                            <div {!!$click!!} style="{{($value3->aeronave_id ==null ? 'cursor:pointer':'')}}"    class="col-md-4 col-sm-6 col-12  {{$value3->aeronave_id==null ? 'free':''}}">
                                                <div id="bg_{{$value3->crypt_id}}" class="info-box {{ ($value3->aeronave_id ==null ? 'bg-success':'bg-danger') }}">
                                                    <span class="info-box-icon"><i class="fas fa-plane"></i></span>

                                                    <div class="info-box-content">

                                                        <span class="info-box-number">{{$value3->nombre}}</span>

                                                        <div class="progress">
                                                            <div class="progress-bar" style="width: 100%"></div>
                                                        </div>
                                                        <span id="description_{{$value3->crypt_id}}"  class="progress-description">

                                                            {{ ($value3->aeronave_id ==null ? '':$value3->aeronave->full_nombre) }}
                                                        </span>
                                                    </div>
                                                    <!-- /.info-box-content -->
                                                </div>
                                                <!-- /.info-box -->
                                            </div>

                                            @if ($value3->aeronave_id ==null)
                                            {{Form::radio("puesto", $value3->crypt_id, false, ["style"=>"display:none","class"=>"puesto_aplicado", "id"=>"puesto_".$value3->crypt_id])}}  
                                            @endif
                                            @endforeach    
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div>

                        </div>



                    </div>
                    @endforeach




                </div>
            </div>
            <!-- /.card -->
        </div>
    </div>

    <div class="col-sm-12 col-md-12 text-center">
        {{Form::button("<li class='fa fa-save'></li> Aplicar Puesto ", ["class"=>"btn btn-info", "onClick"=>"aplicarPuesto()"])}}
    </div>


</div>
{{ Form::close() }} 


<script type="text/javascript">


    $(document).ready(function () {
        /*
         $("#frmEditOper .edit_select2").select2({dropdownParent: $("#modal-princ-edit"), language: "es"});
         $('#frmEditOper').validate({
         errorElement: 'span',
         errorPlacement: function (error, element) {
         error.addClass('invalid-feedback');
         element.closest('.input-group').append(error);
         },
         highlight: function (element, errorClass, validClass) {
         $(element).addClass('is-invalid');
         },
         unhighlight: function (element, errorClass, validClass) {
         $(element).removeClass('is-invalid').addClass("is-valid");
         }
         });
         
         
         
         
         */

        $('#frmAddPos').on("submit", function (event) {
            event.preventDefault();
            return false;
        });
    });

</script>