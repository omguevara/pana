<table id="example1" class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>{{__('Fecha Vuelo')}}</th>
            <th>{{__('Tipo')}}</th>
            
            <th>{{__('Aeronave')}}</th>
            <th>{{__('Piloto')}}</th>
            <th>{{__('Origen')}}</th>
            <th>{{__('Destino')}}</th>
            <th>{{__('Opción')}}</th>


        </tr>
    </thead>
    <tbody>
    </tbody>
</table>

<script type="text/javascript">
    var data1 = @json($data1)
            ;


    $(document).ready(function () {
        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            columns: [
                
                {data: 'fecha'},
                {data: 'tipo'},
                {data: 'aeronave.matricula'},
                {data: 'aeronave.full_nombre'},
                {data: 'origen.nombre'},
                {data: 'destino.nombre'},
                {data: 'action'}
            ]
            @if (app()-> getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        });

    });



</script>