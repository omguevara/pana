

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Vuelos')}}</h3>

            <div class="card-tools">
                
                <a href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(array( "target"=>"_blank",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frm1"))}}
        <div id="card-body-main" class="card-body ">

            <div class="row">

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label >{{__('Fecha')}}</label>

                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.form-group -->
                </div>
                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Aeropuerto</label>
                        {{Form::select('aeropuerto_id', $Aeropuertos, "", ["placeholder"=>"Seleccione",   "class"=>"form-control select2  ", "id"=>"aeropuerto_id" ])}}
                    </div>

                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Tipo de Vuelo</label>
                        {{Form::select('tipo[]', $Tipos, "", ["multiple"=>"multiple",   "class"=>"form-control select2 tab2 ", "id"=>"tipo" ])}}
                    </div>

                </div>
                
                <div class="col-12 col-sm-4">
                    <button type="submit"   class="btn btn-primary" ><li class="fa fa-search"></li>Buscar</button>
                </div>

                



            </div>
        </div>
         {{Form::close()}}
    </div>
</div>



<script type="text/javascript">


    $(function () {
        $(".select2").select2();
        $('#frm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        
        
        $('#iconDate').datetimepicker({
            format: 'L'
            
            
        });
        
        
        /*
         const options = {
         translation: {
         '0': {pattern: /\d/},
         '1': {pattern: /[1-9]/},
         '9': {pattern: /\d/, optional: true},
         '#': {pattern: /\d/, recursive: true},
         'Y': {pattern: /[Yy]/, fallback: 'Y'},
         'V': {pattern: /[Vv]/, fallback: 'V'}
         }
         };
         
         $("#matricula").mask('YV9999', options);
         
         */

    });





</script>