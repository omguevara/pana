<style>
    .container{
        max-width: 100%;
    }
</style>

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Vuelos')}}</h3>

            <div class="card-tools">
                
                <a href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">

            <div class="row">

                @if (in_array("add", Auth::user()->getActions()[$route_id])) 

                <a  class=" btn btn-primary float-left" onClick="setAjax(this, event)" href="{{route('registros.add')}}"><li class="fa fa-plus"></li> {{__("Create")}}</a>
                @endif



                <div class="col-md-12">
                    <table id="example1" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>
                                <th>{{__('OPERACIÓN')}}</th>
                                <th>{{__('PILOTO')}}</th>
                                <th>{{__('AERONAVE')}}</th>
                                <th>{{__('TIPO LLEGADA')}}</th>
                                <th>{{__('TIPO SALIDA')}}</th>
                                <th>{{__('PAX EMBARC')}}</th>
                                <th>{{__('OPCIONES')}}</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>



                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Facturar</h4>

            </div>
            {{Form::open(array( "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frm2"))}}
            {{Form::hidden("cliente_id", "", ["id"=>"cliente_id"])}}
            <div id="modalPrincBodyFact" class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 border-right">
                        <h4 class="">Datos para la Factura</h4>
                        <div class="row">



                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="document">{{__('Document')}}</label>
                                    <div class="input-group " >
                                        <div class="input-group-prepend" >
                                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                        </div>
                                        {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                        <div class="input-group-append" >
                                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>  









                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="razon">{{__('Responsable')}}</label>
                                    {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Responsable')])}}    

                                </div>
                            </div>  


                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="phone">{{__('Phone')}}</label>
                                    {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                                </div>
                            </div>  



                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="correo">{{__('Correo')}}</label>
                                    {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    

                                </div>  
                            </div> 
                            <div class="col-sm-12 col-md-12">

                                <div class="form-group">
                                    <label for="direccion">{{__('Dirección')}}</label>
                                    {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                                </div>  
                            </div>
                        </div>
                    </div>



                </div>

            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrinc" onClick="$('#frm2').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button type="button"  id="setModalPrinc"   onClick="Facturar()"                            class="btn btn-primary float-right">Facturar</button>


            </div>
            {{Form::close()}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="modal-fact-p"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact1" class="modal-content">
            <div class="modal-header">
                <h4 class="">Facturar</h4>
                <button id="closeModalFact" disabled type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            <div id="modalPrincBodyFact1" class="modal-body">


            </div>
            <div class="modal-footer justify-content-between">



            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">
    var crypt_id = "";
    var data1 = @json($data1)
            ;


    $(function () {

        $('#frm2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        /*
         const options = {
         translation: {
         '0': {pattern: /\d/},
         '1': {pattern: /[1-9]/},
         '9': {pattern: /\d/, optional: true},
         '#': {pattern: /\d/, recursive: true},
         'Y': {pattern: /[Yy]/, fallback: 'Y'},
         'V': {pattern: /[Vv]/, fallback: 'V'}
         }
         };
         
         $("#matricula").mask('YV9999', options);
         
         */


        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            columns: [
                {data: 'numero_operacion'},
                {data: 'piloto_llegada.full_name'},
                {data: 'aeronave.matricula'},
                {data: 'tipo_llegada'},
                {data: 'tipo_salida'},
                {data: 'pax_embarcados'},
                {data: 'action'},
            ]
            @if (app()->getLocale() != "en")
                ,language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        }
        );
    });




    function verFact(id) {
        crypt_id = id;
        $("#modal-fact").modal("show");
    }


    function findDato2() {


        if ($("#type_document, #document").valid()) {
            $("#modal-fact").prepend(LOADING);
            $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#cliente_id").val(response.data.crypt_id);
                    $("#razon").val(response.data.razon_social);
                    $("#phone").val(response.data.telefono);
                    $("#correo").val(response.data.correo);
                    $("#direccion").val(response.data.direccion);
                    $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                    $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


    }

    function setTasa(id) {

        $("#main-content").prepend(LOADING);
        $.get("{{url('/tasas/')}}/" + id, function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });


        /*
         $("#closeModalFact").prop("disabled", true);
         $("#modalPrincBodyFact1").html("");
         $("#modal-fact-p").modal("show");
         $("#modal-fact-p").prepend(LOADING);
         
         $.get("{{url('/tasas/')}}/"+id, function (response){
         
         $("#closeModalFact").prop("disabled", false);
         $(".overlay-wrapper").remove();
         $("#modalPrincBodyFact1").html(response);
         
         }).fail(function () {
         $("#closeModalFact").prop("disabled", false);
         $(".overlay-wrapper").remove();
         
         });
         */
    }


    function setDosa(id) {

        $("#main-content").prepend(LOADING);
        $.get("{{url('/dosas/')}}/" + id, function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });


        /*    
         $("#closeModalFact").prop("disabled", true);
         $("#modalPrincBodyFact1").html("");
         $("#modal-fact-p").modal("show");
         $("#modal-fact-p").prepend(LOADING);
         
         $.get("{{url('/dosas/')}}/"+id, function (response){
         
         $("#closeModalFact").prop("disabled", false);
         $(".overlay-wrapper").remove();
         $("#modalPrincBodyFact1").html(response);
         
         }).fail(function () {
         $("#closeModalFact").prop("disabled", false);
         $(".overlay-wrapper").remove();
         
         });
         
         */



    }

    function Facturar() {




        if ($("#frm2").valid()) {



            Swal.fire({
                title: 'Esta Seguro que Desea Guardar?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#modal-fact").prepend(LOADING);
                    $.post("{{route('facturas')}}", $("#frm2").serialize() + '&crypt=' + crypt_id, function (response) {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            window.open("{{url('view-invoice')}}/" + response.data, '_blank', 'height=' + screen.height + ',width=' + screen.width + ',top=0,left=0');
                            $("#modal-fact").modal("hide");
                            $('#frm1, #frm2').trigger("reset");
                            $("#prodservicios, #bodyListProd").html("");
                            MontoGlobalBs = 0;
                        } else {
                        }


                        /*
                         if (response.status == 1) {
                         for (i = 0; i < response.data.length; i++) {
                         $("#prodservicios_extra").append('<option  value="' + response.data[i].crypt_id + '">' + response.data[i].full_descripcion + ' (IVA: ' + muestraFloat(response.data[i].iva) + ') ( ' + response.data[i].moneda + ': ' + muestraFloat(response.data[i].precio) + ')  (Bs.: ' + muestraFloat(response.data[i].bs) + ' ) ( TASA: ' + muestraFloat(response.data[i].tasa) + ')</option>');
                         
                         }
                         $('#prodservicios_extra').bootstrapDualListbox('refresh');
                         } else {
                         Toast.fire({
                         icon: "error",
                         title: "{{__('No se Encontraron Servicios')}}"
                         });
                         }
                         */
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar')}}"
                        });
                    });


                }
            });
        }else{
            Toast.fire({
                icon: "error",
                title: "{{__('Faltan Datos Requeridos')}}"
            });
        }

    }
    function setTortuga(id){
        $("#main-content").prepend(LOADING);
        $.get("{{url('/tasas/')}}/" + id, function (response) {
            $("#main-content").html(response);
            $(".overlay-wrapper").remove();


        }).fail(function () {

            $(".overlay-wrapper").remove();

        });
    }

</script>