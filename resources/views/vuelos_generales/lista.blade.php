<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Registros')}}</h3>

            <div class="card-tools">
                <a id="refreshMolulo" href="{{route('registros')}}" onClick="setAjax(this, event)"   style="display:none">

                </a>  

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            
            <div class="row">

                 @if (in_array("add", Auth::user()->getActions()[$route_id])) 

                <a  class=" btn btn-primary float-left" onClick="setAjax(this, event)" href="{{route('registros.add')}}"><li class="fa fa-plus"></li> {{__("Create")}}</a>
                @endif



                <div class="col-md-12">
                    <table id="example1" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>

                                <th>{{__('PILOTO')}}</th>
                                <th>{{__('HORA SALIDA')}}</th>
                                <th>{{__('MATRICULA')}}</th>
                                <th>{{__('CANT. PASAJEROS')}}</th>
                                <th>{{__('TIPO REGISTRO')}}</th>
                                <th>{{__('OPCIONES')}}</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>



                </div>

            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    var data1 = @json($data1)
            ;
    

    $(function () {
        
        
        /*
        const options = {
                translation: {
                    '0': {pattern: /\d/},
                    '1': {pattern: /[1-9]/},
                    '9': {pattern: /\d/, optional: true},
                    '#': {pattern: /\d/, recursive: true},
                    'Y': {pattern: /[Yy]/, fallback: 'Y'},
                    'V': {pattern: /[Vv]/, fallback: 'V'}
                }
            };
        
        $("#matricula").mask('YV9999', options);
        
        */
        
        
        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            columns: [

                {data: 'piloto.nombres'},
                {data: 'hora_vuelo'},
                {data: 'aeronave.matricula'},
                {data: 'cantidad_pasajeros'},
                {data: 'tipo_registro'},
                
                {data: 'tipo_registro'}
                
            ]
            @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        }
        );
    });
    
    
    
    
    
    
    
    
    
    
    
</script>