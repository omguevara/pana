{{Form::open(["route"=>["aviacion_general.edit", $Operacion->crypt_id],  'id'=>'frmEditOper','autocomplete'=>'Off'])}}
<div class="row">

    <div class="col-sm-12 col-md-12">
        <div class="row">
            <div class="col-sm-12 col-md-3 ">

                <div class="form-group">
                    <label for="matricula_id">{{__('Aeronave')}}</label>


                </div>
            </div>
            <div class="col-sm-12 col-md-6 ">

                <div class="form-group">

                    {{Form::select("matricula_id", $Aeronaves,  ($Operacion->aeronave != null ? $Operacion->aeronave->crypt_id:""), ["required"=>"required", "class"=>"form-control tasa dosa edit_select2  required", "id"=>"matricula_id", "placeholder"=>__('Select')])}}    

                </div>
            </div>

        </div>
    </div>
    <div style="border: 1px solid black;" class="col-sm-12 col-md-6">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="card card-widget widget-user-2 shadow-sm">

                    <div class="widget-user-header bg-warning">
                        <div class="widget-user-image">
                            <img class="img-circle elevation-2" src="{{url('dist/img/arrive.png')}}" alt="User Avatar">
                        </div>

                        <h3 class="widget-user-username">Dosa</h3>
                        <h5 class="widget-user-desc">Actualizar Datos de la Dosa</h5>
                    </div>
                    <div class="card-footer p-0">
                        <div class="row">
                            <div class="col-sm-12 col-md-3">

                                <div class="form-group">
                                    <label for="piloto_llegada_id">{{__('Piloto de Llegada')}}</label>


                                </div>
                            </div>
                            <div class="col-sm-12 col-md-8">

                                <div class="form-group">

                                    {{Form::select("piloto_llegada_id", $Pilotos,   ($Operacion->piloto_llegada != null ? $Operacion->piloto_llegada->crypt_id:""), ["required"=>"required", "class"=>"form-control dosa edit_select2 required", "id"=>"piloto_llegada_id", "placeholder"=>__('Select')])}}    

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-3">

                                <div class="form-group">
                                    <label for="procedencia_id">{{__('Procedencia')}}</label>


                                </div>
                            </div>
                            <div class="col-sm-12 col-md-8">

                                <div class="form-group">

                                    {{Form::select("procedencia_id", $Aeropuertos,  ($Operacion->procedencia2 != null ? $Operacion->procedencia2->crypt_id:""), ["required"=>"required", "class"=>"form-control dosa edit_select2 required", "id"=>"procedencia_id", "placeholder"=>__('Select')])}}    

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label for="hora_llegada">{{__('Hora Llegada')}}</label>
                                    {{Form::text("hora_llegada", $Operacion->hora_llegada, ["required"=>"required", "class"=>"form-control dosa time  required", "id"=>"hora_llegada", "placeholder"=>__('Surname')])}}    

                                </div>
                            </div> 
                            <div class="col-sm-12 col-md-4">

                                <div class="form-group">
                                    <label for="tipo_llegada_id">{{__('Tipo de Llegada')}}</label>
                                    {{Form::select("tipo_llegada_id", ["1" => "NACIONAL", "2" => "INTERNACIONAL"],  $Operacion->tipo_llegada_id, ["required"=>"required", "class"=>"form-control dosa  required", "id"=>"tipo_llegada_id", "placeholder"=>__('Select')])}}    

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label for="pax_desembarcados">{{__('PAX Desembarcados')}}</label>
                                    {{Form::number("pax_desembarcados", $Operacion->pax_desembarcados, ["required"=>"required", "class"=>" form-control dosa required", "id"=>"pax_desembarcados", "placeholder"=>__('cantidad'), "max"=>"99"])}}    

                                </div>
                            </div> 


                            <div class="col-sm-12 col-md-12 text-center">
                                {{Form::button("<li class='fa fa-save'></li> Guardar Dosa <li class='fa fa-plane-arrival'></li>", ["class"=>"btn btn-success", "onClick"=>"saveDataOper('dosa')"])}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div> 
    </div> 
    <div style="border: 1px solid black;" class="col-sm-12 col-md-6">
        <div class="row">


            <div class="card card-widget widget-user-2 shadow-sm">

                <div class="widget-user-header bg-warning">
                    <div class="widget-user-image">
                        <img class="img-circle elevation-2" src="{{url('dist/img/leave.png')}}" alt="User Avatar">
                    </div>

                    <h3 class="widget-user-username">Tasa</h3>
                    <h5 class="widget-user-desc">Actualizar Datos de la Tasa</h5>
                </div>




                <div class="card-footer p-0">
                    <div class="row">
                        <div class="col-sm-12 col-md-3">

                            <div class="form-group">
                                <label for="piloto_salida_id">{{__('Piloto de Salida')}}</label>


                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8">

                            <div class="form-group">

                                {{Form::select("piloto_salida_id", $Pilotos,  ($Operacion->piloto_salida != null ? $Operacion->piloto_salida->crypt_id:""), ["required"=>"required", "class"=>"form-control tasa edit_select2 required", "id"=>"piloto_salida_id", "placeholder"=>__('Select')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">

                            <div class="form-group">
                                <label for="destino_id">{{__('Destino')}}</label>


                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8">

                            <div class="form-group">

                                {{Form::select("destino_id", $Aeropuertos,  ($Operacion->destino2 != null ? $Operacion->destino2->crypt_id:""), ["required"=>"required", "class"=>"form-control tasa edit_select2 required", "id"=>"destino_id", "placeholder"=>__('Select')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="hora_salida">{{__('Hora Salida')}}</label>
                                {{Form::text("hora_salida", $Operacion->hora_salida, ["required"=>"required", "class"=>"form-control tasa time  required", "id"=>"hora_salida", "placeholder"=>__('Hora de Salida')])}}    

                            </div>
                        </div> 
                        <div class="col-sm-12 col-md-4">

                            <div class="form-group">
                                <label for="tipo_salida_id">{{__('Tipo Salida')}}</label>
                                {{Form::select("tipo_salida_id", ["1" => "NACIONAL", "2" => "INTERNACIONAL"],  $Operacion->tipo_salida_id, ["required"=>"required", "class"=>"form-control tasa  required", "id"=>"tipo_salida_id", "placeholder"=>__('Select')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="pax_embarcados">{{__('PAX Embarcados')}}</label>
                                {{Form::number("pax_embarcados", $Operacion->pax_embarcados, ["required"=>"required", "class"=>" form-control tasa required", "id"=>"pax_embarcados", "placeholder"=>__('cantidad'), "max"=>"99"])}}    

                            </div>
                        </div> 


                        <div class="col-sm-12 col-md-12 text-center">
                            {{Form::button("<li class='fas fa-save'></li> Guardar Tasa <li class='fas fa-plane-departure'></li>", ["class"=>"btn btn-success", "onClick"=>"saveDataOper('tasa')"])}}
                        </div>
                    </div>
                </div>
            </div>






        </div>
    </div>

    <div class="col-sm-12 col-md-12 ">

        <div class="form-group">
            <label for="observacion">{{__('Observaci��n')}}</label>
            {{Form::textarea("observacion", $Operacion->observaciones_operaciones, ["style"=>"max-height:50px",  "class"=>"form-control tasa dosa ", "id"=>"observacion"])}}    

        </div>
    </div>

</div>
{{ Form::close() }} 


<script type="text/javascript">


    $(document).ready(function () {
        $('.time').inputmask({alias: "datetime",inputFormat: "HH:MM"});
        $("#frmEditOper .edit_select2").select2({dropdownParent: $("#modal-princ-edit"), language: "es"});
        $('#frmEditOper').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });



        $('#frmEditOper').on("submit", function (event) {
            event.preventDefault();
            return false;
        });

    });

    function saveDataOper(tipo) {
        if ($('#frmEditOper .' + tipo).valid()) {
            $("#frmEditOper").prepend(LOADING);
            $.post($("#frmEditOper").attr("action"), $("#frmEditOper ." + tipo).serialize() + "&tipo=" + tipo + "&_token=" + $('[name="_token"]').val(), function (response) {
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#modal-princ-edit").modal("hide");
                    refreshData();
                }
                //console.log(response);
            }).fail(function () {
                Toast.fire({
                    icon: "error",
                    title: "Error al Guardar"
                });
                $(".overlay-wrapper").remove();
            });
        } else {
            /*
             validator = $('#frmEditOper .' + tipo).validate();
             
             errors = validator.invalidElements();
             errors.each(function (c, o){
             console.log($(o).attr('class'));
             });
             * 
             */
        }
    }

</script>