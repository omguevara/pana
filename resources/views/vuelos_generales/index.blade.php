<style>
    .container{
        max-width: 100%;
    }
</style>

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Aviación General')}}</h3>
            <div class="card-tools">
                @if (in_array("add_arrive", Auth::user()->getActions()[$route_id])) 
                <button type="button"  onClick="$('#modal-princ-create-reg').modal('show')" class="btn btn-tool" ><i class="fas fa-plane-arrival"></i>Dosa </button>
                @endif
                @if (in_array("add_leave", Auth::user()->getActions()[$route_id])) 
                <button type="button"  onClick="$('#modal-princ-create-reg2').modal('show')" class="btn btn-tool" ><i class="fas fa-plane-departure"></i>Tasa </button>
                @endif
                <button type="button"  onClick="refreshData()" class="btn btn-tool" ><i class="fas fa-sync-alt  "></i> {{__("Actualizar")}}</button>
                {{--<button type="button"  onClick="getFormTurtle()" class="btn btn-tool" ><i class="fas fa-cloud-sun  "></i> Tortuga</button>--}}

                @if (in_array("excel", Auth::user()->getActions()[$route_id])) 
                <button type="button"  onClick="exportExcel()"  class="btn btn-tool" ><i class="fas fa-file-excel  "></i> Descargar Datos</button>
                @endif
                @if (in_array("add_nave", Auth::user()->getActions()[$route_id])) 
                <button type="button"  onClick="$('#modal-princ-aeronave').modal('show')"  class="btn btn-tool" ><i class="fas fa-plane "></i> Agregrar Aeronave</button>
                @endif
                @if (in_array("add_piloto", Auth::user()->getActions()[$route_id])) 
                <button type="button"  onClick="$('#modal-princ-pilotos').modal('show')" class="btn btn-tool" ><i class="fas fa-user-plus"></i> Agregar Piloto</button>
                @endif
                @if (in_array("online_reg", Auth::user()->getActions()[$route_id])) 
                <button id="buttonOnline" onClick="verRegOnline()" type="button"  class="btn btn-tool" ><i class="fas fa-globe  "></i> 
                    

                </button>
                @endif







            </div>

        </div>

        <div style="padding: 0px;" id="" class="card-body ">
            <section class="dhx_sample-container">
                <div class="dhx_sample-container__widget" id="grid"></div>
            </section>
        </div>
        <div class="modal-footer justify-content-between">






        </div>

    </div>
</div>



<div class="modal fade" id="modal-princ-create-reg"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-lg">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Registro de Dosa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general.add_arrive",  'id'=>'frmArrive','autocomplete'=>'Off'])}}

            <div  class="modal-body">



                <div class="row">
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="matricula_id">{{__('Aeronave')}}</label>
                            {{Form::select("matricula_id", $Aeronaves,  "", ["required"=>"required", "class"=>"form-control create_select2  required", "id"=>"matricula_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="piloto_id">{{__('Piloto')}}</label>
                            {{Form::select("piloto_id", $Pilotos,  "", ["required"=>"required", "class"=>"form-control create_select2 required", "id"=>"piloto_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="hora_llegada">{{__('Hora Llegada')}}</label>
                            {{Form::text("hora_llegada", "", ["required"=>"required", "class"=>"form-control time  required", "id"=>"hora_llegada", "placeholder"=>__('Surname')])}}    

                        </div>
                    </div> 
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="tipo_llegada_id">{{__('Tipo')}}</label>
                            {{Form::select("tipo_llegada_id", ["1" => "NACIONAL", "2" => "INTERNACIONAL"],  "", ["required"=>"required", "class"=>"form-control  required", "id"=>"tipo_llegada_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="procedencia_id">{{__('Procedencia')}}</label>
                            {{Form::select("procedencia_id", $Aeropuertos,  "", ["required"=>"required", "class"=>"form-control create_select2 required", "id"=>"procedencia_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="pax_desembarcados">{{__('Pasajeros Desembarcados')}}</label>
                            {{Form::number("pax_desembarcados", "", ["required"=>"required", "class"=>" form-control required", "id"=>"pax_desembarcados", "placeholder"=>__('cantidad'), "max"=>"99"])}}    

                        </div>
                    </div> 
                </div>




            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
            </div>
            {{ Form::close() }} 


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-princ-create-reg2"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-lg">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Registro de Tasa</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general.add_leave",  'id'=>'frmLeave','autocomplete'=>'Off'])}}

            <div  class="modal-body">



                <div class="row">
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="matricula_id">{{__('Aeronave')}}</label>
                            {{Form::select("matricula_id", $Aeronaves,  "", ["required"=>"required", "class"=>"form-control create_select2  required", "id"=>"matricula_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="piloto_id">{{__('Piloto')}}</label>
                            {{Form::select("piloto_id", $Pilotos,  "", ["required"=>"required", "class"=>"form-control create_select2 required", "id"=>"piloto_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="hora_salida">{{__('Hora Salida')}}</label>
                            {{Form::text("hora_salida", "", ["required"=>"required", "class"=>"form-control time  required", "id"=>"hora_salida", "placeholder"=>__('Hora de Salida')])}}    

                        </div>
                    </div> 
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="tipo_salida_id">{{__('Tipo Salida')}}</label>
                            {{Form::select("tipo_salida_id", ["1" => "NACIONAL", "2" => "INTERNACIONAL"],  "", ["required"=>"required", "class"=>"form-control  required", "id"=>"tipo_salida_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="destino_id">{{__('Destino')}}</label>
                            {{Form::select("destino_id", $Aeropuertos,  "", ["required"=>"required", "class"=>"form-control create_select2 required", "id"=>"destino_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="pax_embarcados">{{__('Pasajeros Embarcados')}}</label>
                            {{Form::number("pax_embarcados", "", ["required"=>"required", "class"=>" form-control required number", "id"=>"pax_embarcados", "placeholder"=>__('cantidad'), "max"=>"99"])}}    

                        </div>
                    </div> 
                </div>




            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
            </div>
            {{ Form::close() }} 


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Registrar Nuevo Piloto</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general.add_piloto",  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
            <div id="modalPrincBodyFact" class="modal-body">

                <div class="row">
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="type_document">{{__('Tipo de Documento')}}</label>
                            {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="document">{{__('Document')}}</label>
                            {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="name_user">{{__('Name')}}</label>
                            {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="surname_user">{{__('Surname')}}</label>
                            {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="phone">{{__('Phone')}}</label>
                            {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">
                        <div class="form-group">
                            <label for="email">{{__('Email')}}</label>
                            {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                        </div>
                    </div>  
                </div>
            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
            </div>
            {{ Form::close() }} 

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Registrar Nueva Aeronave</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general.add_nave",  'id'=>'frmNave','autocomplete'=>'Off'])}}
            <div  class="modal-body">

                <div class="row">
                    <div class="col-sm-12 col-md-6" >
                        <div class="form-group">
                            <label for="matricula">{{__('Matrícula')}}</label>
                            {{Form::text("matricula", "", ["required"=>"required", "class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="nombre">{{__('Modelo')}}</label>
                            {{Form::text("nombre", "", ["required"=>"required", "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                            {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
            </div>
            {{ Form::close() }} 

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-princ-edit"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Edición de la Operación</h4>
                <button id="closeMEdit" disabled type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>


            <div id="bodyEditOper"  class="modal-body">


            </div>
            <div class="modal-footer justify-content-between">


            </div>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-princ-prodserv"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalProdServ" class="modal-content">
            <div class="modal-header">
                <h4 class="">Control de Servicios Adicionales</h4>
                <button id="closeProdServEdit" disabled type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>


            <div id="bodyEditProdServOper"  class="modal-body">


            </div>
            <div class="modal-footer justify-content-between">


            </div>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="modal-add-pos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalPos" class="modal-content">
            <div class="modal-header">
                <h4 class="">Ubicar Puesto a la Aeronave</h4>
                <button id="closePos" disabled type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>


            <div id="bodyAddPos"  class="modal-body">


            </div>
            <div class="modal-footer justify-content-between">


            </div>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="modal-reg-online"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalOnline" class="modal-content">
            <div class="modal-header">
                <h4 class="">Registros Online</h4>
                <button id="closeOnline" disabled type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>


            <div id="bodyRegOnline"  class="modal-body">


            </div>
            <div class="modal-footer justify-content-between">


            </div>


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<div class="modal fade" id="modal-reg-turtle"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        <div id="modalOnline" class="modal-content">
            <div class="modal-header">
                <h4 class="">Registros Vuelo a la Tortuga</h4>
                <button   type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general.add_turtle",  'id'=>'frmTurtle','autocomplete'=>'Off'])}}
            <div   class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12">

                        <div class="form-group">
                            <label for="t_origen_id">{{__('Origen')}}</label>
                            {{Form::select("t_origen_id", $Aeropuertos,  "", ["required"=>"required", "class"=>"form-control create_select2 required", "id"=>"t_origen_id", "placeholder"=>__('Select')])}}    

                        </div>
                    </div>

                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <label >{{__('Fecha de Vuelo')}}</label>

                            <div class="input-group date" id="iconDate" data-target-input="nearest">
                                {{Form::text("t_fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required", "id"=>"t_fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                                <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>

                        </div>
                        <!-- /.form-group -->
                    </div>


                    <div class="col-sm-6">
                        <!-- radio -->
                        <div class="form-group mt-5 ">
                            
                            <div class="icheck-primary d-inline">
                                <input type="radio" id="radioPrimary1" name="r1" >
                                <label for="radioPrimary1">
                                    Doble Toque
                                </label>
                            </div>

                            <div class="icheck-primary d-inline">
                                <input type="radio" id="radioPrimary3" name="r1" >
                                <label for="radioPrimary3">
                                    Estadía
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group ">
                            <label for="t_hora_llegada" >Hora Llegada (Formato 24H)</label>



                            {{Form::text("t_hora_llegada", date("H:m"), ["required"=>"required", "class"=>"form-control  required time", "id"=>"t_hora_llegada", "placeholder"=>__('Hora Llegada')])}}    



                        </div>

                        <!-- /.input group -->
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="form-group ">
                            <label for="t_hora_salida" >Hora Salida (Formato 24H)</label>



                            {{Form::text("t_hora_salida", date("H:m"), ["required"=>"required", "class"=>"form-control  required time", "id"=>"t_hora_salida", "placeholder"=>__('Hora Salida')])}}    



                        </div>

                        <!-- /.input group -->
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="form-group">
                            <label for="cant_pasajeros">{{__('Cant. Pasajeros')}}</label>
                            {{Form::number("cant_pasajeros", "", ["max"=>"99", "required"=>"required", "class"=>"form-control required number ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                        </div>
                    </div>



                </div>

            </div>
            <div class="modal-footer justify-content-between text-right">
                {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    

            </div>
            {{ Form::close() }} 

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<script type="text/javascript">
    var grid;
    function add(dat) {
        grid.data.add(dat);

    }


    var dataset = @json($data);
            ;

    var datacollection = new dhx.DataCollection();
    datacollection.parse(dataset);


    $(document).on('select2:open', () => {
        document.querySelector('.select2-search__field').focus();
    });

    $(document).ready(function () {
        setTimeout(checkRegOnline, 10000);
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $('.time').inputmask({alias: "datetime", inputFormat: "HH:MM"});
        $('.number').numeric({negative: false});
        $("#modal-princ-create-reg").on('shown.bs.modal', function () {

            if ($('#modal-princ-create-reg .create_select2').hasClass("select2-hidden-accessible")) {
                $("#modal-princ-create-reg .create_select2").select2('destroy');
            }
            $("#modal-princ-create-reg .create_select2").select2({dropdownParent: $("#modal-princ-create-reg"), language: "es"});


        });

        $("#modal-princ-create-reg2").on('shown.bs.modal', function () {

            if ($('#modal-princ-create-reg2 .create_select2').hasClass("select2-hidden-accessible")) {
                $("#modal-princ-create-reg2 .create_select2").select2('destroy');
            }
            $("#modal-princ-create-reg2 .create_select2").select2({dropdownParent: $("#modal-princ-create-reg2"), language: "es"});


        });

        $("#modal-reg-turtle").on('shown.bs.modal', function () {

            if ($('#modal-reg-turtle .create_select2').hasClass("select2-hidden-accessible")) {
                $("#modal-reg-turtle .create_select2").select2('destroy');
            }
            $("#modal-reg-turtle .create_select2").select2({dropdownParent: $("#modal-reg-turtle"), language: "es"});


        });

        $('#iconDate').datetimepicker({
            format: 'L'
        });


        $('#frmPilotos').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $('#frmNave').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $('#frmArrive').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        
        
        $('#frmTurtle').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                var elem = $(element);
                if (elem.hasClass("select2-hidden-accessible")) {
                    element = $("#select2-" + elem.attr("id") + "-container").parent(); 
                    error.insertAfter(element);
                } else {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                }
                
                
                
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        
        
         $('#frmTurtle').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmTurtle').valid()) {
                $("#frmTurtle").prepend(LOADING);
                $.post(this.action, $("#frmTurtle").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                     $(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        //add(response.data);
                        refreshData();
                        $('#frmTurtle').trigger("reset");

                        $("#modal-reg-turtle").modal('hide'); // Update
                    }
                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });
        
        $('#frmArrive').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmArrive').valid()) {
                $("#frmArrive").prepend(LOADING);
                $.post(this.action, $("#frmArrive").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $("#frmArrive").find(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        //add(response.data);
                        refreshData();
                        $('#frmArrive').trigger("reset");

                        $("#modal-princ-create-reg").modal('hide'); // Update
                    }
                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });


        $('#frmLeave').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmLeave').valid()) {
                $("#frmLeave").prepend(LOADING);
                $.post(this.action, $("#frmLeave").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $("#frmLeave").find(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        //add(response.data);
                        refreshData();
                        $('#frmLeave').trigger("reset");

                        $("#modal-princ-create-reg2").modal('hide'); // Update
                    }
                    //console.log(response);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPilotos').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPilotos').valid()) {
                $("#frmPilotos").prepend(LOADING);
                $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $("#frmPilotos").find(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#frmLeave #piloto_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                        $("#frmArrive #piloto_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                        $('#frmPilotos').trigger("reset");
                        $("#modal-princ-pilotos").modal('hide');
                    }
                    //console.log(response);
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmNave').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmNave').valid()) {
                $("#frmNave").prepend(LOADING);
                $.post(this.action, $("#frmNave").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $("#frmNave").find(".overlay-wrapper").remove();
                    if (response.status == 1) {
                        $("#matricula_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                        $('#frmNave').trigger("reset");
                        $("#modal-princ-aeronave").modal('hide');
                    }
                    //console.log(response);
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        //$("#maxW").click();
        //setTimeout(initData(), 5000);
        initData();


    });


    function getFormTurtle() {
        $("#modal-reg-turtle").modal("show");
    }


    function initData() {


        if (typeof (grid) == "object") {
            grid = null;
        }
        grid = new dhx.Grid("grid", {
            columns: [
                {id: "crypt_id", header: [{text: "ID", rowspan: 2, hideCol: true}]},
                {width: 150, id: "action", header: [{text: "OPC.", rowspan: 2, css: "head-custom", title: "Opciones"}], tooltip: false},
                {width: 120, id: "fecha2", header: [{text: "FECHA", rowspan: 2, align: "center", css: "head-custom"}]},
                {width: 120, id: "matricula", header: [{text: "MATRICULA", rowspan: 2, align: "center", css: "head-custom"}]},

                {width: 120, id: "modelo", header: [{text: "AERONAVE", rowspan: 2, align: "center", css: "head-custom"}]},
                {width: 150, id: "piloto", header: [{text: "PILOTO", rowspan: 2, align: "center", css: "head-custom"}]},

                {width: 60, id: "hora_llegada", header: [{text: "LLEGADAS", colspan: 4, align: "center", css: "head-custom"}, {text: "HORA", align: "center", css: "head-custom"}]},
                {width: 60, id: "tipo_llegada", header: ["", {text: "TIPO", align: "center", css: "head-custom"}]},
                {width: 60, id: "procedencia", header: ["", {text: "PROC", align: "center", css: "head-custom"}]},
                {width: 100, id: "pax_desembarcados", header: ["", {text: "PAX DESEM", align: "center", css: "head-custom"}]},

                {width: 60, id: "hora_salida", header: [{text: "SALIDAS", colspan: 4, align: "center", css: "head-custom"}, {text: "HORA", align: "center", css: "head-custom"}]},
                {width: 60, id: "tipo_salida", header: ["", {text: "TIPO", align: "center", css: "head-custom"}]},
                {width: 60, id: "destino", header: ["", {text: "DESTINO", align: "center", css: "head-custom"}]},
                {width: 100, id: "pax_embarcados", header: ["", {text: "PAX EMBARC", align: "center", css: "head-custom"}]},

                {width: 90, id: "numero_operacion", header: [{text: "NUM.", rowspan: 2, align: "center", css: "head-custom"}]},
                {width: 100, id: "puesto", header: [{text: "PUESTO", rowspan: 2, align: "center", css: "head-custom"}]},
                {id: "observaciones_operaciones", header: [{text: "OBSERVACIÓN", rowspan: 2, align: "center", css: "head-custom"}]}


            ],
            selection: "complex",
            css: "myGridCss",
            autoWidth: true,
            sortable: false,

            htmlEnable: true,
            data: datacollection,
            resizable: true

        });
        grid.hideColumn("crypt_id");






        grid.events.on("cellDblClick", function (row, col, obj) {
            /*    
             console.log(row);
             console.log(col);
             console.log(obj);
             dat = [];
             dat['dato'] = arguments[1].id;
             dat['valor'] = arguments[0][dat['dato']];
             dat['rowId'] = arguments[0]['id'];
             
             //console.log(arguments.length);
             console.log(dat);
             let argsList = "";
             for (let i = 0; i < arguments.length; i++) {
             argsList += JSON.stringify(arguments[i]);
             }
             
             alert(argsList);
             */
        });


    }



    function exportExcel() {
        grid.export.xlsx({
            url: "//export.dhtmlx.com/excel"
        });
    }
    function editOperation(id) {
        $("#closeMEdit").prop("disabled", true);
        $("#bodyEditOper").html("");
        $("#modal-princ-edit").modal("show");
        $("#bodyEditOper").prepend(LOADING);
        $.get("{{url('/aviacion-general/edit/')}}/" + id, function (response) {

            $("#closeMEdit").prop("disabled", false);
            $(".overlay-wrapper").remove();
            $("#bodyEditOper").html(response);

        }).fail(function () {
            $("#closeMEdit").prop("disabled", false);
            $(".overlay-wrapper").remove();

        });

    }
    function refreshData() {
        $("#panel_princ").prepend(LOADING);
        $.get("{{route('refresh_data', $crypt_route_id)}}", function (response) {
            grid.data.removeAll();
            grid.data.add(response.data);
            $(".overlay-wrapper").remove();
        }).fail(function () {
            $(".overlay-wrapper").remove();

        });
    }

    function addProdserv(id) {
        $("#closeProdServEdit").prop("disabled", true);
        $("#bodyEditProdServOper").html("");
        $("#modal-princ-prodserv").modal("show");
        $("#modal-princ-prodserv").prepend(LOADING);
        $.get("{{url('/aviacion-general/add-prodserv/')}}/" + id, function (response) {

            $("#closeProdServEdit").prop("disabled", false);
            $(".overlay-wrapper").remove();
            $("#bodyEditProdServOper").html(response);

        }).fail(function () {
            $("#closeProdServEdit").prop("disabled", false);
            $(".overlay-wrapper").remove();

        });
    }

    function checkRegOnline() {


        $.get("{{route('check_reg_online')}}", function (response) {
            $("#badgeOnine").remove();
            if (response.data > 0) {
                $("#buttonOnline").append('<span id="badgeOnine" class="badge badge-danger navbar-badge">' + response.data + '</span>');
            }
            setTimeout(checkRegOnline, 10000);
        });

    }


    function addPosition(id) {


        $("#closePos").prop("disabled", true);
        $("#bodyAddPos").html("");
        $("#modal-add-pos").modal("show");
        $("#modal-add-pos").prepend(LOADING);
        $.get("{{url('/aviacion-general/add-position/')}}/" + id, function (response) {

            $("#closePos").prop("disabled", false);
            $(".overlay-wrapper").remove();
            $("#bodyAddPos").html(response);

        }).fail(function () {
            $("#closePos").prop("disabled", false);
            $(".overlay-wrapper").remove();

        });

    }

    function selectPuesto(obj, id, nave) {
        $("#puesto_" + id).prop("checked", true);
        $(".free").find(".progress-description").html("");
        $(".free").find(".bg-warning").removeClass("bg-warning").addClass('bg-success');
        $("#description_" + id).html(nave);
        $("#bg_" + id).removeClass("bg-success").addClass('bg-warning');
    }

    function aplicarPuesto() {
        if ($(".puesto_aplicado:checked").length > 0) {
            $("#frmAddPos").prepend(LOADING);
            $.post($("#frmAddPos").attr("action"), $("#frmAddPos").serialize(), function (response) {
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#modal-add-pos").modal("hide");
                    refreshData();
                }
                //console.log(response);
            }).fail(function () {
                Toast.fire({
                    icon: "error",
                    title: "Error al Guardar"
                });
                $(".overlay-wrapper").remove();
            });
        } else {
            Toast.fire({
                icon: "error",
                title: "No Ha seleccionado el Puesto"
            });
        }
    }

    function verRegOnline() {
        //modal-reg-online  closeOnline   bodyRegOnline

        $("#closeOnline").prop("disabled", true);
        $("#bodyRegOnline").html("");
        $("#modal-reg-online").modal("show");
        $("#modal-reg-online").prepend(LOADING);

        $.get("{{route('aviacion_general.online_reg')}}", function (response) {

            $("#closeOnline").prop("disabled", false);
            $(".overlay-wrapper").remove();
            $("#bodyRegOnline").html(response);

        }).fail(function () {
            $("#closeOnline").prop("disabled", false);
            $(".overlay-wrapper").remove();

        });
    }


    function getRegOnline(id) {
        $("#closeOnline").prop("disabled", true);
        //$("#bodyRegOnline").html("");
        //$("#modal-reg-online").modal("show");
        $("#modal-reg-online").prepend(LOADING);

        $.get("{{url('aviacion-general/online-reg')}}/" + id, function (response) {

            $("#closeOnline").prop("disabled", false);
            $(".overlay-wrapper").remove();
            $("#modal-reg-online").modal("hide");
            refreshData();
            //$("#bodyRegOnline").html(response);

        }).fail(function () {
            $("#closeOnline").prop("disabled", false);
            $(".overlay-wrapper").remove();

        });
    }
    
    
</script>