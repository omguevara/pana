

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Aviación General')}}</h3>
            <div class="card-tools">






            </div>

        </div>

        <div style="padding: 0px;" id="" class="card-body ">
            {{Form::open(["route"=>"registro_aviacion_general", "onSubmit"=>"false", 'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
            @method('PUT')
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-12">
                    <div class="form-group">
                        <label >{{__('Fecha')}}</label>

                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.form-group -->
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">

                    <div class="form-group">
                        <label for="piloto_id">{{__('Piloto')}}</label>
                        {{Form::select("piloto_id", $Pilotos,  $piloto, ["class"=>"form-control select2 ", "id"=>"piloto_id", "placeholder"=>__('Select')])}}    

                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12">

                    <div class="form-group">
                        <label for="matricula_id">{{__('Aeronave')}}</label>
                        {{Form::select("matricula_id", $Aeronaves,  $aeronave, [ "class"=>"form-control select2  required", "id"=>"matricula_id", "placeholder"=>__('Select')])}}    

                    </div>
                </div>


                <div class="col-sm-12 col-md-4 col-lg-4">

                    <div class="form-group">
                        <label for="aeropuerto_id">{{__('aeropuerto_id')}}</label>
                        <div class="input-group " >
                            {{Form::select("aeropuerto_id", $Aeropuertos,  $aeropuerto, [ "class"=>"form-control select2 ", "id"=>"aeropuerto_id", "placeholder"=>__('Select')])}}    
                            <div class="input-group-append" >
                                <div title="{{__('Buscar')}}"  onClick="refreshDataVuelos()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>

                        </div>
                    </div>  
                </div>




            </div>

            {{ Form::close() }} 

            <table id="example1" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>{{__('N')}}&deg;</th>
                        <th>{{__('Aerpuertos')}}</th>
                        
                        <th>{{__('Matricula')}}</th>
                        <th>{{__('Aeronave')}}</th>
                        <th>{{__('Aeronave')}}</th>
                        <th>{{__('Actions')}}</th>

                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="modal-footer justify-content-between">






        </div>

    </div>
</div>



<div class="modal fade" id="modal-princ-1"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-lg">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Detalle del registro</h4>
               
            </div>

           

            <div id="modal-princ-body-1"  class="modal-body">






            </div>
            <div  class="modal-footer justify-content-between">

                
            </div>
           


        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script type="text/javascript">


    var data1 = @json($data);
            ;


    $(document).on('select2:open', () => {
        document.querySelector('.select2-search__field').focus();
    });


    $(document).ready(function () {
        $('#iconDate').datetimepicker({
            format: 'L',
            "maxDate": moment()
        });
        $(".select2").select2();
        $("#fecha").val("{{showDate($fecha)}}");



        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            columns: [
                {data: 'numero_operacion'},
                {data: 'aeropuerto.full_nombre'},
                
                {data: 'matricula'},
                {data: 'modelo'},
                {data: 'piloto'},

                {data: 'action'}
            ]
             @if (app()-> getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        });





        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });





    });
    function getDataVuelo(id){
        $("#modal-princ-body-1").html("");
        $('#modal-princ-1').modal('show');
        $("#modal-princ-body-1").prepend(LOADING);
        $.get("{{url('registro-aviacion-general-get')}}/"+id, function (response){
            $("#modal-princ-body-1").html(response);
            $(".overlay-wrapper").remove();
        }).fail(function (xhr) {
            $(".overlay-wrapper").remove();
            
        });
    }
    function refreshDataVuelos() {
       
        $("#frmPrinc1").prepend(LOADING);
        AJAX_ACTIVE = true;
       
        $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
            AJAX_ACTIVE = false;
            $("#main-content").html(response);

        }, 'html').fail(function (xhr) {
            $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
            AJAX_ACTIVE = false;
        });
    }
    
    function editOperationVuelo(id){
        $("#modal-princ-body-1").html("");
        $('#modal-princ-1').modal('show');
        $("#modal-princ-body-1").prepend(LOADING);
        $.get("{{url('registro-aviacion-general-update')}}/"+id, function (response){
            $("#modal-princ-body-1").html(response);
            $(".overlay-wrapper").remove();
        }).fail(function (xhr) {
            $(".overlay-wrapper").remove();
            
        });
    }
    
    function saveVueloAdmin(){
        $("#modal-princ-body-1").prepend(LOADING);
        $.post("{{route('registro_aviacion_general')}}", $("#frmPrinc2").serialize(), function (response) {
            $(".overlay-wrapper").remove();
            $("#modal-princ-1").modal('hide');

        }, 'html').fail(function (xhr) {
            $(".overlay-wrapper").remove();
            
        });
    }

</script>
