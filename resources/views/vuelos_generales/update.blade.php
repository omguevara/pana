{{Form::open(["route"=>"registro_aviacion_general", "onSubmit"=>"false", 'id'=>'frmPrinc2','autocomplete'=>'Off'])}}
<div class="row">
    <div class="col-lg-12 col-md-3 col-sm-12">
        {{$VuelosGenerales->aeropuerto->full_nombre}}
        <br/>


        {{$VuelosGenerales->matricula}}
        <br/>
        {{$VuelosGenerales->modelo}}
        <br/>
        {{$VuelosGenerales->piloto}}
        <br/>
    </div>

    {{Form::hidden("id", $VuelosGenerales->crypt_id)}}
    <div class="col-lg-4 col-md-3 col-sm-12">

        <div class="form-group">
            <label for="exento_tasa">{{__('Excento Tasa')}}</label>
            {{Form::select("exento_tasa", ["0"=>"No", "1"=>"Si"],  $VuelosGenerales->exento_tasa, ["class"=>"form-control select2 ", "id"=>"exento_tasa", "placeholder"=>__('Select')])}}    

        </div>
    </div>

    <div class="col-lg-4 col-md-3 col-sm-12">

        <div class="form-group">
            <label for="exento_dosa">{{__('Excento Dosa')}}</label>
            {{Form::select("exento_dosa", ["0"=>"No", "1"=>"Si"],  $VuelosGenerales->exento_dosa, ["class"=>"form-control select2 ", "id"=>"exento_dosa", "placeholder"=>__('Select')])}}    

        </div>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12">
        <button  class="btn btn-primary mt-5" id="btn-save-nave"  type="button"  onclick="saveVueloAdmin()">{{__('Save')}}</button>
    </div>





    <button type="button"  class="btn btn-primary" data-dismiss="modal">Volver</button>
</div>

{{ Form::close() }} 

