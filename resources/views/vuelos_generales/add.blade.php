<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registrar Plan de Vuelo')}}</h3>


            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(array("route"=>"registros.add",  "enctype"=>"multipart/form-data",  "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"frmPrinc1"))}}
        {{Form::hidden("cliente_id", "", ["id"=>"cliente_id"])}}
        {{Form::hidden("piloto_id", "", ["id"=>"piloto_id"])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">

                <div class="col-12 col-md-2">
                    <div class="form-group">
                        <label for="type_document2">{{__('Tipo Documento')}}</label>
                        {{Form::select('type_document2', [ "V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document2" ,"required"=>"required"])}}

                    </div>  
                </div>

                <div class="col-sm-12 col-md-4">

                    <div class="form-group">
                        <label for="document2">{{__('Documento del Piloto')}}</label>
                        <div class="input-group " >
                            {{Form::text("document2", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "onblur"=>'findDato2()',  "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document2", "placeholder"=>__('Document')])}}    
                            <div class="input-group-append" >
                                <div title="{{__('Buscar la Datos')}}" id="btnFinDato2" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>

                        </div>
                    </div>  
                </div>  
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="piloto">{{__('Piloto')}}</label>
                        {{Form::text("piloto", "", [ "readonly"=>"readonly", "maxlength"=>"100", "required"=>"required", "class"=>"form-control required tab1", "id"=>"piloto", "placeholder"=>__('Nombre del Piloto')])}}    

                    </div>
                </div>  


                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label>Tipo de Vuelo</label>
                        {{Form::select('nacionalidad_id', ["1"=>"NACIONAL", "2"=>"INTERNACIONAL"], "", ["placeholder"=>"Seleccione",   "class"=>"form-control tab2 ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                    </div>

                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <label >{{__('Fecha de Vuelo')}}</label>

                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.form-group -->
                </div>



                <div class="col-12 col-sm-4">
                    <div class="form-group ">
                        <label for="hora_inicio" >Hora Salida (Hora Militar)</label>

                        <div class="input-group date" id="timepicker" data-target-input="nearest">

                            {{Form::text("hora_inicio", date("H:m"), ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                            <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                            </div>
                        </div>

                        <!-- /.input group -->
                    </div>
                </div>





                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label>Origen</label>
                        {{Form::select('origen_id', $ORIGEN, "", ["placeholder"=>"Seleccione",  "onChange"=>"checkOrigen(this)",  "class"=>"form-control tab2 select2 select2-danger", "id"=>"origen_id" ,"required"=>"required"])}}
                        {{Form::hidden('origen_otro')}}

                    </div>
                    <!-- /.form-group -->
                </div>


                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="destino_id" >Destino</label>

                        {{Form::select('destino_id', $DESTINO, "", ["placeholder"=>"Seleccione", "onChange"=>"checkOrigen(this)", "class"=>"form-control tab2 required", "id"=>"destino_id" ,"required"=>"required"])}}
                        {{Form::hidden('destino_otro')}}

                    </div>
                    <!-- /.form-group -->
                </div>

                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="cant_pasajeros">{{__('Cant. Pasajeros')}}</label>
                        {{Form::text("cant_pasajeros", "", ["maxlength"=>"3", "required"=>"required", "class"=>"form-control required number tab2", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                    </div>
                </div>
                {{Form::hidden("numero_vuelo", "1")}}    
                <!--
                <div class="col-sm-12 col-md-4">
                    <div class="form-group">
                        <label for="numero_vuelo">{{__('Número de Vuelo')}}</label>
                        {{Form::text("numero_vuelo", "", ["required"=>"required", "class"=>"form-control required number tab2", "id"=>"numero_vuelo", "placeholder"=>__('Número de Vuelo')])}}    

                    </div>
                </div>
                -->
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="placa">Matr&iacute;cula</label>
                        <div class="input-group " >
                            {{Form::text("placa", "", ["required"=>"required", "onblur"=>"findPlaca()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control required tab2", "id"=>"placa", "placeholder"=>__('Matrícula')])}}    
                            <div class="input-group-append" >
                                <div title="{{__('Buscar la Aeronave')}}" id="btnFinPlaca" onClick="findPlaca()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                            <!--
                            <div class="input-group-append" >
                                <div title="{{__('Crear Nueva Aeronave')}}" onClick="$('#modal-nave').modal('show');" style="cursor:pointer" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                            -->


                        </div>
                    </div>
                </div>  





                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label for="aeronave">{{__('Aeronave')}}</label>
                        {{Form::text("aeronave", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"aeronave", "placeholder"=>__('Aeronave')])}}    

                    </div>
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label for="peso_avion">{{__('Peso Max. Kg')}}</label>
                        {{Form::text("peso_avion", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"peso_avion", "placeholder"=>__('Peso Max. Kg')])}}    

                    </div>
                </div>

                <div class="col-sm-12 col-md-3">
                    <div class="form-group">
                        <label for="maximo_pasajeros">{{__('Max. Pasajeros')}}</label>
                        {{Form::text("maximo_pasajeros", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"maximo_pasajeros", "placeholder"=>__('Max. Pasajeros')])}}    

                    </div>
                </div>

                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label for="estacion_id" >Angar</label>

                        {{Form::select('estacion_id', $DESTINO, "", [ "placeholder"=>"Seleccione", "class"=>"form-control tab2 required", "id"=>"estacion_id" ,"required"=>"required"])}}


                    </div>
                    <!-- /.form-group -->
                </div>

                <div class="col-12 ">
                    <button  class="btn btn-primary" id="btn-save-nave"  type="button"  onclick="fin()">{{__('Save')}}</button>
                </div>


            </div>
            <div id="iframe-load"></div>

        </div>
        {{Form::close()}}
    </div>
</div>



<script type="text/javascript">



    $(function () {
        $("#estacion_id").readonly(true);
        $('#iconDate').datetimepicker({
            format: 'L',
            "minDate": moment(),
            "maxDate": moment().add(7, 'days')
        });

        $("#fecha").val("{{date('d/m/Y')}}");

        $('#timepicker').datetimepicker({
            format: 'LT'
        });
        $("#frmPrinc1").on("submit", function (event){
             event.preventDefault();
             if ($('#frmPrinc1').valid()) {
                 $("#card-body-main").prepend(LOADING);
                 
                 
                 $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('registros')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
                 
             }
            
        });
        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $("#document2").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {

                findDato2();

            }
        });

        $("#placa").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {

                findPlaca();

            }
        });

        $("#document2").focus();




    });



    function checkOrigen(obj) {
        if ($("#origen_id").val() == $("#destino_id").val()) {
            Toast.fire({
                icon: "error",
                title: "{{__('EL Origen No Puede Ser Igual al Destino')}}"
            });
            $(obj).val("");
        } else {
            if ($(obj).children(":selected").text() == "OTRO") {

                otro = null;
                while (otro == null || $.trim(otro) == "") {
                    otro = prompt("Ingrese El Aeropuerto: ");
                }
                $(obj).next().val(otro);

            }
        }

    }

    function findPlaca() {
        if ($("#placa").valid()) {
            $("#card-body-main").prepend(LOADING);
            $.get("{{url('get-placa')}}/" + $("#placa").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                    $("#estacion_id").readonly(true);
                    $("#aeronave").val(response.data.nombre);
                    $("#peso_avion").val(muestraFloat(response.data.peso_maximo, 0));
                    $("#maximo_pasajeros").val(response.data.maximo_pasajeros);
                    $("#estacion_id").val(response.data.estacion_crypt_id);
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Aeronave no Encontrada')}}"
                    });
                    $("#aeronave, #peso_avion, #maximo_pasajeros, #estacion_id").val("");


                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                $("#estacion_id").readonly(true);
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar la Placa')}}"
                });
            });
        }
    }

    function findDato2() {


        if ($("#type_document2, #document2").valid()) {

            $("#card-body-main").prepend(LOADING);
            $.get("{{url('get-data-piloto')}}/" + $("#type_document2").val() + "/" + $("#document2").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#piloto_id").val(response.data.crypt_id);
                    $("#piloto").val(response.data.full_name);





                    $("#piloto").prop("readonly", true);

                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#piloto").prop("readonly", false);
                    $("#piloto").focus();
                    $("#piloto_id, #piloto").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#piloto").prop("readonly", true);
                $("#piloto_id, #piloto").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });


        }


    }


    function fin() {
        //$("#modal-pdf-body").html('<h1>Aplicando Espere</h1>');




        
        $("#frmPrinc1").submit();

    }






</script>