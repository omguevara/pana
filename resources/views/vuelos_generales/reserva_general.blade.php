@extends('layouts.principal')
@section('content')

<div class="col-sm-12">
    <div class="row mb-2">

        <h1>Proforma</h1>
    </div>

</div>



<div class="col-sm-12 offset-md-3 col-md-6 offset-xl-1 col-xl-10">




    <div id="card1" class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Reserva de Vuelo</h3>
        </div>
        <div id="carBodyP" class="card-body p-0">
            {{Form::open(array("route"=>"reserva_general", "target"=>"iframePdf", "enctype"=>"multipart/form-data",  "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"tabFrm1"))}}
            {{Form::hidden("piloto_id", "", ["id"=>"piloto_id"])}}


            <div id="select2"  >
                <div class="bs-stepper">
                    <div class="bs-stepper-header" role="tablist">
                        <!-- your steps here -->


                        
                        <div class="step" data-target="#tab2">
                            <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="title2">
                                <span class="bs-stepper-circle">1</span>
                                <span class="bs-stepper-label">Datos del Vuelo</span>
                            </button>
                        </div>
                        <div class="line"></div>
                        <div class="step" data-target="#tab3">
                            <button type="button" class="step-trigger" role="tab" aria-controls="information-part" id="title3">
                                <span class="bs-stepper-circle">2</span>
                                <span class="bs-stepper-label">Datos del Pago</span>
                            </button>
                        </div>
                    </div>
                    <div class="bs-stepper-content">
                        <!-- your steps content here -->

                        <div id="tab2" class="content" role="tabpanel" aria-labelledby="title2">

                            <div class="card">

                                <div style="background-color:#f4f6f9; " class="card-body">


                                    <div class="row">

                                        <div class="col-12 col-md-2">
                                            <div class="form-group">
                                                <label for="type_document2">{{__('Tipo Documento')}}</label>
                                                {{Form::select('type_document2', [ "V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document2" ,"required"=>"required"])}}

                                            </div>  
                                        </div>

                                        <div class="col-sm-12 col-md-4">

                                            <div class="form-group">
                                                <label for="document2">{{__('Documento del Piloto')}}</label>
                                                <div class="input-group " >
                                                    {{Form::text("document2", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "onblur"=>'findDato2()',  "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document2", "placeholder"=>__('Document')])}}    
                                                    <div class="input-group-append" >
                                                        <div title="{{__('Buscar la Datos')}}" id="btnFinDato2" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                                    </div>

                                                </div>
                                            </div>  
                                        </div>  
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="piloto">{{__('Piloto')}}</label>
                                                {{Form::text("piloto", "", [ "readonly"=>"readonly", "maxlength"=>"100", "required"=>"required", "class"=>"form-control required tab1", "id"=>"piloto", "placeholder"=>__('Nombre del Piloto')])}}    

                                            </div>
                                        </div>  


                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                <label>Tipo de Vuelo</label>
                                                {{Form::select('nacionalidad_id', ["1"=>"NACIONAL", "2"=>"INTERNACIONAL"], "", ["placeholder"=>"Seleccione",   "class"=>"form-control tab2 ", "id"=>"nacionalidad_id" ,"required"=>"required"])}}
                                            </div>

                                        </div>

                                        <div class="col-12 col-sm-4">
                                            <div class="form-group">
                                                <label >{{__('Fecha de Vuelo')}}</label>

                                                <div class="input-group date" id="iconDate" data-target-input="nearest">
                                                    {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                                                    <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- /.form-group -->
                                        </div>



                                        <div class="col-12 col-sm-4">
                                            <div class="form-group ">
                                                <label for="hora_inicio" >Hora Salida (Hora Militar)</label>

                                                <div class="input-group date" id="timepicker" data-target-input="nearest">

                                                    {{Form::text("hora_inicio", date("H:m"), ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                                                    <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                                    </div>
                                                </div>

                                                <!-- /.input group -->
                                            </div>
                                        </div>





                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Origen</label>
                                                {{Form::select('origen_id', $ORIGEN, "", ["placeholder"=>"Seleccione",  "onChange"=>"checkOrigen(this)",  "class"=>"form-control tab2 select2 select2-danger", "id"=>"origen_id" ,"required"=>"required"])}}
                                                {{Form::hidden('origen_otro')}}

                                            </div>
                                            <!-- /.form-group -->
                                        </div>


                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label for="destino_id" >Destino</label>

                                                {{Form::select('destino_id', $DESTINO, "", ["placeholder"=>"Seleccione", "onChange"=>"checkOrigen(this)", "class"=>"form-control tab2 required", "id"=>"destino_id" ,"required"=>"required"])}}
                                                {{Form::hidden('destino_otro')}}

                                            </div>
                                            <!-- /.form-group -->
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="cant_pasajeros">{{__('Cant. Pasajeros')}}</label>
                                                {{Form::text("cant_pasajeros", "", ["maxlength"=>"3", "required"=>"required", "class"=>"form-control required number tab2", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                                            </div>
                                        </div>
                                        {{Form::hidden("numero_vuelo", "1")}}    
                                        <!--
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="numero_vuelo">{{__('Número de Vuelo')}}</label>
                                                {{Form::text("numero_vuelo", "", ["required"=>"required", "class"=>"form-control required number tab2", "id"=>"numero_vuelo", "placeholder"=>__('Número de Vuelo')])}}    

                                            </div>
                                        </div>
                                        -->
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="placa">Matr&iacute;cula</label>
                                                <div class="input-group " >
                                                    {{Form::text("placa", "", ["required"=>"required", "onblur"=>"findPlaca()",  "data-msg-required"=>"Campo Requerido",  "class"=>"form-control required tab2", "id"=>"placa", "placeholder"=>__('Matrícula')])}}    
                                                    <div class="input-group-append" >
                                                        <div title="{{__('Buscar la Aeronave')}}" id="btnFinPlaca" onClick="findPlaca()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                                    </div>

                                                    <div class="input-group-append" >
                                                        <div title="{{__('Crear Nueva Aeronave')}}" onClick="$('#modal-nave').modal('show');" style="cursor:pointer" class="input-group-text"><i class="fa fa-plus"></i></div>
                                                    </div>



                                                </div>
                                            </div>
                                        </div>  





                                        <div class="col-sm-12 col-md-3">
                                            <div class="form-group">
                                                <label for="aeronave">{{__('Aeronave')}}</label>
                                                {{Form::text("aeronave", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"aeronave", "placeholder"=>__('Aeronave')])}}    

                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <div class="form-group">
                                                <label for="peso_avion">{{__('Peso Max. Kg')}}</label>
                                                {{Form::text("peso_avion", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"peso_avion", "placeholder"=>__('Peso Max. Kg')])}}    

                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-3">
                                            <div class="form-group">
                                                <label for="maximo_pasajeros">{{__('Max. Pasajeros')}}</label>
                                                {{Form::text("maximo_pasajeros", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"maximo_pasajeros", "placeholder"=>__('Max. Pasajeros')])}}    

                                            </div>
                                        </div>

                                        <div class="col-12 col-sm-3">
                                            <div class="form-group">
                                                <label for="estacion_id" >Basamento</label>

                                                {{Form::select('estacion_id', $DESTINO, "", [ "placeholder"=>"Seleccione", "class"=>"form-control tab2 required", "id"=>"estacion_id" ,"required"=>"required"])}}


                                            </div>
                                            <!-- /.form-group -->
                                        </div>
                                        <div class="col-12 col-sm-12">
                                            <div class="card card-primary card-outline">
                                                <div class="card-header">
                                                    <h3 class="card-title">
                                                        <i class="fas fa-edit"></i>
                                                        Servicios Extra
                                                    </h3>

                                                    <div class="card-tools">
                                                        <button title="{{__('Actualizar Listado de Servicios')}}" onClick="refreshProdServExt()" type="button" class="btn btn-tool" >
                                                            <i class="fas fa-sync"></i>
                                                        </button>
                                                    </div>

                                                </div>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                {{Form::select('prodservicios[]', [], "", [ "multiple"=>"multiple", "class"=>"form-control tab2 ", "id"=>"prodservicios" ])}}

                                                            </div>
                                                            <!-- /.form-group -->
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                                <!-- /.card -->
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            

                            <button class="btn btn-primary float-right" type="button"  onclick="seePre();">Siguiente</button>
                        </div>
                        <div id="tab3" class="content" role="tabpanel" aria-labelledby="title3">
                            <div class="card">

                                <div id="panelP"  style="background-color:#f4f6f9; " class="card-body">
                                    <div id="MG" class="col-form-label text-center">
                                        0,00
                                    </div>

                                    <div  class="col-form-label text-center">
                                        TASA DEL EURO {{muestraFloat($VALOR_EURO)}}
                                    </div>




                                    {{Form::hidden("precio", "0", ["id"=>"precio"])}}
                                    {{Form::hidden("pagada", "0", ["id"=>"pagada"])}}
                                    <div class="col-form-label text-center">
                                        <label for="value">Pagar en Taquilla</label>
                                        <input type="checkbox" disabled="disabled"  onChange="pagoCheck()" checked="checked" value="1" name="taquilla_id"  id="taquilla_id"  data-bootstrap-switch data-off-color="danger" data-on-color="success" />

                                    </div>
                                    <div class="dropdown-divider"></div>

                                    <div id="pagoLine" class="row">
                                        <div class="col-12 col-sm-3">
                                            <!--
                                            <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                                                <a class="nav-link" id="vert-tabs-home-tab " data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">Banco 1</a>
                                                <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Banco 2</a>
                                                <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-messages" role="tab" aria-controls="vert-tabs-messages" aria-selected="false">Pago M&oacute;vil</a>

                                            </div>
                                            -->

                                            <div class="small-box bg-success">
                                                <div class="inner">
                                                    <h3 id="TotalP">0,00</h3>

                                                    <p>Monto a Pagar en Bs.</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="fas fa-euro-sign"></i>
                                                </div>

                                            </div>


                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <h2 class="text-center border-bottom">Pago M&oacute;vil</h2>
                                            <h4 class="text-center" >Banco Venezuela (0102) <img src="{{asset('dist/img/bancos/0102.png')}}" class="img-fluid" /></h4>
                                            <h4 class="text-center" >RIF G-200089920</h4>
                                            <h4 class="text-center" >0416-5977068</h4>

                                                                                   </div>

                                        <div class="col-12 col-sm-3">
                                            <img id="show1" src="{{url('/')}}/dist/img/getMoney.png"  class="img-fluid "   />
                                        </div>


                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="exampleInputFile">Ingrese el Capture del Pago M&oacute;vil <span class="right badge badge-info">(S&oacute;lo si Realiza Pago M&oacute;vil)</span></label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" id="img" name="img">
                                                    <label class="custom-file-label" for="exampleInputFile">Subir Capture</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="prodservicios" >Observaci&oacute;n</label>
                                            <div class="input-group " >
                                                {{Form::textarea('observacion',  "", [ "class"=>"form-control tab3 ", "rows"=>"3", "id"=>"observacion" ])}}

                                            </div>
                                        </div>
                                        <!-- /.form-group -->
                                    </div>




                                </div>
                            </div>
                            <button class="btn btn-primary" id="backF" type="button" onclick="stepper.previous()">Anterior</button>
                            <button class="btn btn-primary float-right" type="button" onclick="fin()">Finalizar</button>
                        </div>


                    </div>
                </div>
            </div>

            {{Form::close()}}
        </div>
    </div>
</div>
<!-- /.card -->

<div class="modal fade" id="modal-presupuesto"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Presupuesto Proforma</h4>

            </div>
            <div id="modal-presupuesto-body" class="modal-body">

            </div>
            <div class="modal-footer justify-content-between">

                <button type="button" disabled="disabled" id="backTab2" class="btn btn-primary" data-dismiss="modal">Volver</button>

                <button disabled="disabled" class="btn btn-primary" disabled="disabled" type="button" id="goTab3" onclick=" stepper.next(); $('#modal-presupuesto').modal('hide');">Siguiente</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-nave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        {{Form::open(["route"=>"aeronaves.create", "onSubmit"=>"return false",  'id'=>'frmPrinc2','autocomplete'=>'Off'])}}
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Aeronave</h4>

            </div>
            <div id="modal-nave-body" class="modal-body">
                <div class="row">


                    <div class="col-sm-12 col-md-6" >
                        <div class="form-group">
                            <label for="matricula">Matr&iacute;cula</label>
                            {{Form::text("matricula", "", ["maxlength"=>"10", "required"=>"required", "class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}    

                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="nombre">{{__('Modelo')}}</label>
                            {{Form::text("nombre", "", ["maxlength"=>"30", "required"=>"required", "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="peso_maximo">Peso M&aacute;ximo <span class="right badge badge-info">(Kg.)</span></label>
                            {{Form::text("peso_maximo", "", ["maxlength"=>"6",  "required"=>"required", "class"=>"form-control required number ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                        </div>
                    </div> 

                    <div class="col-sm-12 col-md-6">

                        <div class="form-group">
                            <label for="maximo_pasajeros2">M&aacute;ximo Pasajero</label>
                            {{Form::text("maximo_pasajeros2", "", ["maxlength"=>"3", "required"=>"required", "class"=>"form-control required number", "id"=>"maximo_pasajeros2", "placeholder"=>__('Máximo Pasajero')])}}    

                        </div>
                    </div> 


                </div>  
            </div>
            <div  class="modal-footer justify-content-between">

                <button  id="btn-clse-nave"  type="button" class="btn btn-secundary bg-danger"  data-dismiss="modal">{{__('Cerrar')}}</button>

                <button  class="btn btn-primary" id="btn-save-nave"  type="button"  onclick="saveNave()">{{__('Save')}}</button>
            </div>
        </div>

        {{ Form::close() }} 
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-pdf"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Presupuesto Proforma</h4>

            </div>
            <div id="modal-pdf-body" class="modal-body">

            </div>
            <div  class="modal-footer justify-content-between">

                <button id="pdfPrint" onClick="newReg()" disabled="disabled" type="button" class="btn btn-primary"  data-dismiss="modal">Cerrar</button>


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
    DualList = false;

    function pagoCheck() {
        //$("#pagoLine").readonly($("#taquilla_id").is(":checked"));
    }





    function Pagar() {
        $("#carBodyP").prepend(LOADING);
        $("#pagada").val(1);
        setTimeout(function () {
            $(".overlay-wrapper").remove();
            $("#backF").prop("disabled", true);
            $("#panelP").readonly();
            Toast.fire({
                icon: "success",
                title: "Pago Procesado Correctamente"
            });
        }, 3000);

    }

    document.addEventListener('DOMContentLoaded', function () {
        
        
        
        bsCustomFileInput.init();
        window.stepper = new Stepper(document.querySelector('.bs-stepper'));
        pagoCheck();
        $("input[data-bootstrap-switch]").each(function () {
            $(this).bootstrapSwitch('state', $(this).prop('checked'));
            $(".tab-pane").toggleClass("fade");


        });

        $("span:contains(OFF)").text("NO");
        $("span:contains(ON)").text("SI");

        //stepper.next();
        // stepper.next();


   
        $("#estacion_id").readonly(true);
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});

        $(".money").maskMoney({"decimal": ",", "thousands": ".", "allowZero": true});
        $('#iconDate').datetimepicker({
            format: 'L',
            "minDate": moment(),
            "maxDate": moment().add(7, 'days')
        });
        
        $("#fecha").val("{{date('d/m/Y')}}");

        $('#timepicker').datetimepicker({
            format: 'LT'
        });


        $('#tabFrm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });

        $('#frmPrinc2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });


        $("#document2").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {

                findDato2();

            }
        });

        $("#placa").on("keypress", function (event) {
            //console.log(event.which);
            if (parseInt(event.which, 10) == 13) {

                findPlaca();

            }
        });

        $("#document2").focus();

    });





    function nextTab(tab) {
        if ($("." + tab).valid()) {
            stepper.next();
        }
    }
    function checkOrigen(obj) {
        if ($("#origen_id").val() == $("#destino_id").val()) {
            Toast.fire({
                icon: "error",
                title: "{{__('EL Origen No Puede Ser Igual al Destino')}}"
            });
            $(obj).val("");
        } else {
            if ($(obj).children(":selected").text() == "OTRO") {

                otro = null;
                while (otro == null || $.trim(otro) == "") {
                    otro = prompt("Ingrese El Aeropuerto: ");
                }
                $(obj).next().val(otro);

            }
        }

    }

    function findPlaca() {
        if ($("#placa").valid()) {
            $("#carBodyP").prepend(LOADING);
            $.get("{{url('get-placa')}}/" + $("#placa").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                    $("#estacion_id").readonly(true);
                    $("#aeronave").val(response.data.nombre);
                    $("#peso_avion").val(muestraFloat(response.data.peso_maximo, 0));
                    $("#maximo_pasajeros").val(response.data.maximo_pasajeros);
                    $("#estacion_id").val(response.data.estacion_crypt_id);
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Aeronave no Encontrada')}}"
                    });
                    $("#aeronave, #peso_avion, #maximo_pasajeros, #estacion_id").val("");


                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                $("#estacion_id").readonly(true);
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar la Placa')}}"
                });
            });
        }
    }

    function refreshProdServExt() {
        if ($(".tab2").valid()) {
            $("#prodservicios").children().remove();

            $("#carBodyP").prepend(LOADING);
            $.post("{{route('get_prod_serv_ext')}}", $(".tab2, [name=_token]").serialize(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    for (i = 0; i < response.data.length; i++) {
                        $("#prodservicios").append('<option data-formula="' + response.data[i].formula + '" data-iva="' + response.data[i].iva + '" data-valor_petro="' + response.data[i].valor_petro + '" data-valor_euro="' + response.data[i].valor_euro + '" value="' + response.data[i].crypt_id + '">' + response.data[i].full_descripcion + ' (IVA: ' + muestraFloat(response.data[i].iva) + ') ( ' + response.data[i].moneda + ': ' + muestraFloat(response.data[i].precio) + ')  (Bs.: ' + muestraFloat(response.data[i].bs) + ' ) ( TASA: ' + muestraFloat(response.data[i].tasa) + ')</option>');
                    }

                    if (DualList == true) {
                        $('#prodservicios').bootstrapDualListbox('refresh');
                    } else {
                        DualList = true;

                        $('#prodservicios').bootstrapDualListbox({
                            nonSelectedListLabel: 'Servicios Extra Disponibles',
                            selectedListLabel: 'Servicios Extra Asignados',
                            preserveSelectionOnMove: 'moved',
                            moveOnSelect: false,
                            infoText: 'Total {0}',
                            infoTextEmpty: 'Vacio',
                            filterPlaceHolder: 'Filtro',
                            moveSelectedLabel: 'Agregar Selección',
                            moveAllLabel: 'Agregar Todos',
                            removeSelectedLabel: 'Remover Selección',
                            removeAllLabel: 'Remover Todos',
                            filterTextClear: 'Mostrar Todos',
                            infoTextFiltered: "<span class='label label-warning'>Filtrados</span> {0} de {1}"
                                    //nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
                        });


                    }





                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('No se Encontraron Servicios')}}"
                    });
                }
            }).fail(function () {
                $(".overlay-wrapper").remove();

                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Servicios')}}"
                });
            });
        }
    }
    function saveNave() {
        if ($("#frmPrinc2").valid()) {
            $("#modal-nave-body").prepend(LOADING);
            $("#btn-save-nave, #btn-clse-nave").prop("disabled", true);


            $.post("{{route('aeronaves.add')}}", $("#frmPrinc2").serialize(), function (response) {
                $("#btn-save-nave, #btn-clse-nave").prop("disabled", false);
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $("#placa").val(response.data.matricula);
                    $("#btn-clse-nave").click();
                    $("#btnFinPlaca").click();
                    $('#frmPrinc2').trigger("reset");
                }

            }).fail(function () {
                $("#btn-save-nave, #btn-clse-nave").prop("disabled", false);
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Crear la Aeronave')}}"
                });
            });
        }
    }
    function seePre() {
        if ($(".tab2").valid()) {

            $("#modal-presupuesto-body").html('<h1>Calculando Presupuesto, Por Favor Espere</h1>');
            $("#modal-presupuesto").modal('show');
            $("#modal-presupuesto-body").prepend(LOADING);
            $("#backTab2, #goTab3").prop("disabled", true);
            $.post("{{route('get_presupuesto')}}", $(".tab2, [name=_token]").serialize(), function (response) {
                $(".overlay-wrapper").remove();

                $("#modal-presupuesto-body").html(response);
                $("#backTab2").prop("disabled", false);
                $('#MG').html('<label>Monto Total a Pagar Bs.: ' + MontoGlobal + '</label>');
                $('#TotalP').html(MontoGlobal);

                $('#precio').val(MontoGlobal);
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#modal-presupuesto").modal('hide');
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar el Presupuesto')}}"
                });
            });
        }

    }
    function correction() {
        $("#modal-pdf").modal('hide');
    }
    function checkSaveFin(Iframe) {

        $('#pdfPrint').prop('disabled', false);
        $('.overlay-wrapper').remove();

    }
    function fin() {
        //$("#modal-pdf-body").html('<h1>Aplicando Espere</h1>');
        $('#pdfPrint').prop('disabled', true);
        $("#modal-pdf").modal('show');
        $("#modal-pdf-body").html("<iframe onload=\"checkSaveFin(this);\" name=\"iframePdf\" id=\"iframePdf\" style=\"width:100%; height: 600px; border:none\" src=\"\"></iframe>");

        $("#modal-pdf-body").prepend(LOADING);
        $("#tabFrm1").submit();

        /*
         if ($(".tab3").valid()) {
         Swal.fire({
         title: 'Esta Seguro que Desea Guardar?',
         html: "Confirmaci&oacute;n",
         icon: 'question',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Si'
         }).then((result) => {
         if (result.isConfirmed) {
         $('#pdfPrint').prop('disabled', true);
         $("#modal-pdf").modal('show');
         $("#modal-pdf-body").html("<iframe onload=\"$('#pdfPrint').prop('disabled', false); $('.overlay-wrapper').remove();\" name=\"iframePdf\" id=\"iframePdf\" style=\"width:100%; height: 600px; border:none\" src=\"\"></iframe>");
         
         $("#modal-pdf-body").prepend(LOADING);
         $("#tabFrm1").submit();
         
         
         
         }
         });
         }*/




    }

    function newReg() {

        document.location.reload();
        /*
         Swal.fire({
         title: 'Ya Re?',
         html: "Desea Usted Realizar Otro Registro",
         icon: 'question',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         confirmButtonText: 'Si'
         }).then((result) => {
         if (result.isConfirmed) {
         
         $("#prodservicios").children().remove();
         $(".tab2").val("");
         stepper.to(2);
         
         } else {
         document.location.reload();
         }
         });
         * 
         */
    }
    function refreshCatpcha() {
        $('#iLoad').addClass('fa-spin');
        $.ajax({
            type: 'GET',
            url: "{{url('refresh-captcha')}}",
            success: function (data) {
                $('#iLoad').removeClass('fa-spin');
                $("#imgCatpcha").html(data.captcha);
            }
        });

    }

    function findDato1() {


        if ($("#type_document, #document").valid()) {
            $("#tab1").prepend(LOADING);
            $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#cliente_id").val(response.data.crypt_id);
                    $("#name_user").val(response.data.razon_social);
                    $("#phone").val(response.data.telefono);
                    $("#correo").val(response.data.correo);
                    $("#direccion").val(response.data.direccion);




                    $("#name_user, #phone, #correo, #direccion").prop("readonly", true);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#name_user, #phone, #correo, #direccion").prop("readonly", false);
                    $("#cliente_id, #name_user, #phone, #correo, #direccion").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#name_user, #phone, #correo, #direccion").prop("readonly", true);
                $("#cliente_id, #name_user, #phone, #correo, #direccion").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });


        }


    }

    function findDato2() {


        if ($("#type_document2, #document2").valid()) {

            $("#tab2").prepend(LOADING);
            $.get("{{url('get-data-piloto')}}/" + $("#type_document2").val() + "/" + $("#document2").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#piloto_id").val(response.data.crypt_id);
                    $("#piloto").val(response.data.full_name);





                    $("#piloto").prop("readonly", true);

                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#piloto").prop("readonly", false);
                    $("#piloto").focus();
                    $("#piloto_id, #piloto").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#piloto").prop("readonly", true);
                $("#piloto_id, #piloto").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });


        }


    }

    function clearEmail() {
        $("#correo").val($.trim($("#correo").val()));
    }

</script>  




@endsection