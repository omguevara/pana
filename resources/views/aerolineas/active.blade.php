

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Activar Aerolineas')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('aerolineas')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["aerolineas.active", $Aerolineas->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="tipo_documento">{{__('Tipo de Documento')}}</label>
                        {{Form::select('tipo_documento', ["V"=>"V", "E"=>"E", "P"=>"P", "J"=>"J", "G"=>"G"], $Aerolineas->tipo_documento, ["disabled"=>"disabled",  "class"=>"form-control ", "id"=>"tipo_documento" ,"required"=>"required"])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="documento">{{__('Document')}}</label>
                        {{Form::text("documento", $Aerolineas->documento, ["disabled"=>"disabled", "class"=>"form-control ", "id"=>"documento", "placeholder"=>__('Document')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-12" >
                    <div class="form-group">
                        <label for="razon_social">{{__('Razón Social')}}</label>
                        {{Form::text("razon_social", $Aerolineas->razon_social, ["disabled"=>"disabled", "class"=>"form-control ", "id"=>"razon_social", "placeholder"=>__('Razón Social')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="telefono">{{__('Phone')}}</label>
                        {{Form::text("telefono", $Aerolineas->telefono, ["disabled"=>"disabled", "class"=>"form-control  ", "id"=>"telefono", "placeholder"=>__('Phone')])}}

                    </div>
                </div>

            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-check-circle"></i> '.__("Active"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();

            $("#frmPrinc1").prepend(LOADING);
            $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $.get("{{route('aerolineas')}}", function (response) {
                        $("#main-content").html(response);

                    }).fail(function (xhr) {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    });
                } else {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                }
                //console.log(response);
            }).fail(function () {
                $("#frmPrinc1").find(".overlay-wrapper").remove();
            });

            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
