

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Aerolinea')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('aerolineas')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"aerolineas.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="tipo_documento">{{__('Tipo de Documento')}}</label>
                        {{Form::select('tipo_documento', ["V"=>"V", "E"=>"E", "P"=>"P", "J"=>"J", "G"=>"G"], "", [ "class"=>"form-control ", "id"=>"tipo_documento" ,"required"=>"required"])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="documento">{{__('Document')}}</label>
                        {{Form::text("documento", "", ["required"=>"required", "class"=>"form-control required", "id"=>"documento", "placeholder"=>__('Document')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-12" >
                    <div class="form-group">
                        <label for="razon_social">{{__('Razón Social')}}</label>
                        {{Form::text("razon_social", "", ["required"=>"required", "class"=>"form-control required", "id"=>"razon_social", "placeholder"=>__('Razón Social')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="telefono">{{__('Phone')}}</label>
                        {{Form::text("telefono", "", ["required"=>"required", "class"=>"form-control required phone", "id"=>"telefono", "placeholder"=>__('Phone')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="abreviatura">{{__('Siglas')}}</label>
                        {{Form::text("abreviatura", "", ["required"=>"required", "class"=>"form-control required ", "id"=>"abreviatura", "placeholder"=>__('Siglas')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-12">

                    <div class="form-group">
                        <label for="direccion">{{__('Address')}}</label>
                        {{Form::text("direccion", "", ["required"=>"required", "class"=>"form-control required ", "id"=>"direccion", "placeholder"=>__('Address')])}}

                    </div>
                </div>
                
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <label for="exampleInputFile">Imagen</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="imagen" name="imagen" />
                                <label class="custom-file-label" for="iamgen">Subir Imagen</label>
                            </div>

                        </div>
                    </div>
                </div>
                
                
            </div>
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                
                var formData = new FormData(document.getElementById("frmPrinc1"));
                
                $.ajax({
                    url: this.action,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('aerolineas')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });
                
                
                
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
