

<div id="panel_princ" class="col-sm-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Assign Permissions')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('profiles')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["profiles.permissions", $profile->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <h2>{{$profile->code.' '.$profile->name_profile}}</h2>
            <div class="col-md-12">

                <div class="card card-primary card-tabs">
                    <div class="card-header p-0 pt-1">
                        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">

                            @foreach($Menu as $key=>$value)
                            <li class="nav-item">
                                <a class="nav-link {{($key==0? 'active':'')}}" id="custom-tabs-{{$value->id}}" data-toggle="pill" href="#custom-tabs-child-{{$value->id}}" role="tab" aria-controls="custom-tabs-child-{{$value->id}}" aria-selected="true">{{$value->name_menu}}</a>
                            </li>
                            @endforeach


                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="custom-tabs-one-tabContent">
                            @foreach($Menu as $key=>$value)
                            <div class="tab-pane fade show {{($key==0? 'active':'')}}" id="custom-tabs-child-{{$value->id}}" role="tabpanel" aria-labelledby="custom-tabs-{{$value->id}}">
                                <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="100px">{{Form::checkbox('all', 1, 0 ,["id"=>"all", "onClick"=>"clickAll(this, ".$key.")"])}} {{__('All')}}</th>
                                                <th>{{__('Process')}}</th>
                                                <th width="250px">{{__('Actions')}}</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($value->getProcess as $key2=>$value2)
                                            <tr>
                                                <td>@php
                                                    $checked = isset($profile->getMenu()[$value->id][$value2->id]);
                                                   echo  Form::checkbox('process['.$value2->id.'][id]', $value2->id, $checked ,["class"=>"l_procs".$key]);
                                                @endphp</td>
                                                <td>{{$value2->name_process}}</td>
                                                <td>@php
                                                    if (trim($value2->actions)!=''){
                                                        $list  = explode('|', $value2->actions);
                                                        if (count($list)>0){

                                                        echo '<ul class="nav flex-column">';
                                                            foreach($list as $value3){

                                                                if ($checked==true){
                                                                    $checked2 = in_array($value3, $profile->getMenu()[$value->id][$value2->id]);
                                                                }else{
                                                                    $checked2 = false;
                                                                }


                                                                echo '<li class="nav-item"> <a href="javascript:void(0)"   class="nav-link">';
                                                                echo Form::checkbox('process['.$value2->id.'][actions][]', $value3, $checked2);
                                                                echo showActions($value3);
                                                                //echo $value3;
                                                                echo '</a></li>';
                                                            }
                                                        echo '</ul>';
                                                        }
                                                    }

                                                    @endphp</td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                            </div>

                            @endforeach
                        </div>
                    </div>
                    <!-- /.card -->
                </div>


            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        @foreach($Menu as $key=>$value)
        if ($(".l_procs{{$key}}").length == $(".l_procs{{$key}}:checked").length){
            $("#all").prop("checked", true);
        }else{
            if ($(".l_procs{{$key}}:checked").length>0){
                $("#all").prop("indeterminate", true);
            }
        }
        @endforeach


        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('profiles')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });
function clickAll(obj, num){
    if ($(obj).is(":checked")){
        $(".l_procs"+num).prop("checked", true);
    }else{
        $(".l_procs"+num).prop("checked", false);
    }
}
</script>
