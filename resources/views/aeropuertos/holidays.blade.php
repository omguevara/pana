

<div id="panel_princ" class="col-sm-12 col-md-5 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Agregar Feriados a los Aeropuertos')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('aeropuertos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>



             
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["aeropuertos.holidays", $aereopuerto->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="codigo_oaci">{{__('Código OACI')}}</label>
                        {{Form::text("codigo_oaci", $aereopuerto->codigo_oaci, ["disabled"=>"disabled", "required"=>"required", "class"=>"form-control required", "id"=>"codigo_oaci", "placeholder"=>__('C��digo OACI')])}}    

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="form-group">
                        <label for="codigo_oaci">{{__('Código IATA')}}</label>
                        {{Form::text("codigo_iata", $aereopuerto->codigo_oaci, ["disabled"=>"disabled", "required"=>"required", "class"=>"form-control required", "id"=>"codigo_iata", "placeholder"=>__('C��digo IATA')])}}    

                    </div>
                </div>
            </div>
            

            
            <div class="form-group">
                <label for="nombre">{{__('Nombre')}}</label>
                {{Form::text("nombre", $aereopuerto->nombre, ["disabled"=>"disabled",  "required"=>"required", "class"=>"form-control required", "id"=>"nombre", "placeholder"=>__('Nombre')])}}    

            </div>
            <div class="form-group  text-right">
               
                {{Form::button('<la class="fa fa-plus"></li> Aregar Nueva Fecha',["class"=>"form-control btn-info ", "id"=>"addFecha"])}}    

            </div>
            <div id="listDateHolidays" class="row">
                @foreach($aereopuerto->getFeriados as $key => $value)
                
                <div class="input-group mt-2" id="iconDate" data-target-input="nearest">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Fecha:</b></span>
                    </div>
                    <input  class="form-control " name="data[{{$key}}][fecha]" type="text" value="{{showDate($value->fecha)}}">
                    <div class="input-group-append" >
                         <div class="input-group-text"><i class="fa fa-calendar"></i></div>                    
                    </div>
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Nombre:</b></span>
                    </div>
                    <input  class="form-control   " placeholder="Descripción" name="data[{{$key}}][nombre]" type="text" value="{{$value->nombre}}">    
                    <div class="input-group-prepend">
                         <div style="cursor:pointer" onClick="$(this).parent().parent().remove()" class="input-group-text"><i class="fa fa-times"></i></div>
                    </div>
                </div>
                
                @endforeach
            </div>



            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<la class="fa fa-save"></li> '.__("Guardar"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    

        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>






<script type="text/javascript">

    var  cantF = parseInt("{{(isset($key) ? $key:0)}}", 10);


    $(document).ready(function () {
        
        $("#addFecha").on("click", function (){
        
        
            
            f = '';
            f += '<div class="input-group mt-2" id="iconDate" data-target-input="nearest">';
                f += '<div class="input-group-prepend">';
                    f += '<span class="input-group-text"><b>Fecha:</b></span>';
                f += '</div>';
                f += '<input  class="form-control " name="data['+cantF+'][fecha]" type="text" value="">';
                f += '<div class="input-group-append" >';
                    f += ' <div class="input-group-text"><i class="fa fa-calendar"></i></div>';
                f += '</div>';
                f += '<div class="input-group-prepend">';
                    f += '<span class="input-group-text"><b>Nombre:</b></span>';
                f += '</div>';
                f += '<input  class="form-control   " placeholder="Descripción" name="data['+cantF+'][nombre]" type="text" value="">    ';
                f += '<div class="input-group-prepend">';
                    f += ' <div style="cursor:pointer" onClick="$(this).parent().parent().remove()" class="input-group-text"><i class="fa fa-times"></i></div>';
                f += '</div>';
            f += '</div>';
            cantF++;
            //f = $("#listDateHolidays").children().eq(0).clone();
            
            $("#listDateHolidays").append(f)
        
        });
    
    
        $('#frmPrinc1').on("submit", function (event){
            event.preventDefault();
            
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response){
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status==1){
                        $.get("{{route('aeropuertos')}}", function (response){
                            $("#main-content").html(response);

                        }).fail(function (xhr){
                             $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    }else{
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function (){
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            
            return false;
        });
        
        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });
    
    

</script>
</div>