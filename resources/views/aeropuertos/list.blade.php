<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}: Videos</title>

        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">

        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">

    </head>
    <style>
        body{
            background-image: url('{{url("/")}}/dist/img/aeropuertos-2.png')
        }
    </style>
    <body >
        
        <div class="wrapper">

            {{-- <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="margin-left: 1px; ">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    
                    <li class="nav-item d-none d-sm-inline-block">
                        <h1><a href="#" class="nav-link">{{config('app.name')}} (P.A.N.A) :: Aeropuertos</a></h1>
                        
                    </li>

                </ul>
            </nav> --}}
            <nav class="navbar navbar-light">
                <a class="navbar-brand w-100" href="#">
                  <img src="{{url('/')}}/dist/img/banner.jpeg" class="d-inline-block align-top mx-auto" width="100%" height="80px" style="text-align: center;" alt="">
                </a>
            </nav>
            <nav class="navbar navbar-light">
                <ul class="navbar-nav">
                    <li class="nav-item d-none d-sm-inline-block">
                        <h2><a href="#" class="nav-link">{{config('app.name')}} (P.A.N.A) :: Aeropuertos</a></h2>
                    </li>
                </ul>
            </nav>
        </div>  
        
       

        <div class="container mt-5" style="margin: auto; padding: 1rem;">
            <div class="row" >
                @foreach ($Aeropuertos as $Aeropuerto)
                <div class="col-md-2 mt-2">
                    @csrf
                    <a href="{{ route('informacionAeropuerto', $Aeropuerto->codigo_oaci)}}" title="{{ $Aeropuerto->nombre}}"  style="background-color: #01175e; color: #fff;" class="btn btn-block   py-3 px-5"><b>{{ $Aeropuerto->codigo_oaci }}</b></a>
                </div>
                @endforeach
            </div>
        </div>
          
        <!-- jQuery -->
        <script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>

       

    </body>
</html>






