<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <style>
        table.borde, table.borde th, table.borde tr td {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 0;
            margin: 0;
            border-spacing: 0;
        }
        .t1{
            background-color:grey;
            text-align:center;
            font-size: 14px;
        }
        .t2{
            background-color: #dbd9d9;
            text-align:center;
            font-size: 12px;
        }

        table.sn_border tr td{
            border:none;
        }
        tr:hover td{
            background-color: #75bff2;
        }
        input[type=text]{





            min-width: 95%;
            max-width: 95%;

            font-size: 12px;

            margin-left: 2.5%;
            margin-bottom: 5px;
            margin-top: 5px;
        }

        input[type="checkbox"], input[type="radio"]{
            width:20px;
            height:20px;
        }
        table tr td input[type=text]{
            width: 50%;
        }
    </style>
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Información del Aeropuerto')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->


        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12 col-lg-12 ">
                    <div class="row">
                        <div class="col-md-12">

                            @if (Auth::user()->aeropuerto_id != null)
                                <div id="accordion">
                                    <div class="card card-warning">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                                <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                                                    OPERACIONES
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="collapse " data-parent="#accordion">
                                            <div class="card-body">
                                                @include('aeropuertos.caracteristicas.operaciones')
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-primary ">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                            
                                                <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
                                                    SEGURIDAD AEROPUERTUARIA


                                                    
                                                
                                                    
                                                </a>
                                                
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                @include('aeropuertos.caracteristicas.seguridad')
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-danger">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                                <a class="d-block w-100" data-toggle="collapse" href="#collapseThree">
                                                    SSEI
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                @include('aeropuertos.caracteristicas.ssei')
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-success">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                                <a class="d-block w-100" data-toggle="collapse" href="#collapseFour">
                                                    CERIFICACIÓN Y CAPACITACIÓN
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFour" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                @include('aeropuertos.caracteristicas.certificacion')
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card card-info ">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                                <a class="d-block w-100" data-toggle="collapse" href="#collapseFive">
                                                    MISCELANEOS
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapseFive" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                @include('aeropuertos.caracteristicas.miscelaneos')
                                            </div>
                                        </div>
                                    </div>
                                </div>        
                            @else
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="alert alert-info" role="alert">
                                            Este usuario no posee aeropuerto asignado.
                                        </div>                          
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>    
                </div>
            </div>
            <div class="card-footer text-center">
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                      event.preventDefault();
                      return false;
                    }
                 });
                
                
                $('.date').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy", "clearIncomplete": true});
                $('.number').numeric({negative: false});

                $(".date").on("blur", function () {
                    F = moment(this.value, "DD/MM/YYYY");
                    hoy = moment();
                    if (F > hoy) {
                        this.value = '';
                        Toast.fire({
                            icon: "error",
                            title: "fecha Errada: " + this.value
                        });
                        this.value = '';
                    }

                });

                $('#frmOper').on("submit", function (event) {
                    event.preventDefault();

                    $("#frmOper").prepend(LOADING);
                    $.post(this.action, $("#frmOper").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmOper").find(".overlay-wrapper").remove();

                    }).fail(function () {
                        $("#frmOper").find(".overlay-wrapper").remove();
                    });

                    return false;
                });
                
                $('#frmSeg').on("submit", function (event) {
                    event.preventDefault();

                    $("#frmSeg").prepend(LOADING);
                    $.post(this.action, $("#frmSeg").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmSeg").find(".overlay-wrapper").remove();

                    }).fail(function () {
                        $("#frmSeg").find(".overlay-wrapper").remove();
                    });

                    return false;
                });

                $('#frmCer').on("submit", function (event) {
                    event.preventDefault();

                    $("#frmCer").prepend(LOADING);
                    $.post(this.action, $("#frmCer").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmCer").find(".overlay-wrapper").remove();

                    }).fail(function () {
                        $("#frmCer").find(".overlay-wrapper").remove();
                    });

                    return false;
                });
                
                
                $('#frmBom').on("submit", function (event) {
                    event.preventDefault();

                    $("#frmBom").prepend(LOADING);
                    $.post(this.action, $("#frmBom").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmBom").find(".overlay-wrapper").remove();

                    }).fail(function () {
                        $("#frmBom").find(".overlay-wrapper").remove();
                    });

                    return false;
                });


            });


        </script>

    </div>
</div>

