

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Desactivar Aeropuertos')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('aeropuertos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["aeropuertos.disable", $aereopuerto->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">


             <div class="form-group">
                <label for="codigo_oaci">{{__('C��digo OACI')}}</label>
                {{Form::text("codigo_oaci", $aereopuerto->codigo_oaci, ["disabled"=>"disabled", "required"=>"required", "class"=>"form-control required", "id"=>"codigo_oaci", "placeholder"=>__('C��digo OACI')])}}

            </div>

            <div class="form-group">
                <label for="codigo_oaci">{{__('C��digo IATA')}}</label>
                {{Form::text("codigo_iata", $aereopuerto->codigo_oaci, ["disabled"=>"disabled", "required"=>"required", "class"=>"form-control required", "id"=>"codigo_iata", "placeholder"=>__('C��digo IATA')])}}

            </div>
            <div class="form-group">
                <label for="nombre">{{__('Nombre')}}</label>
                {{Form::text("nombre", $aereopuerto->nombre, ["disabled"=>"disabled",  "required"=>"required", "class"=>"form-control required", "id"=>"nombre", "placeholder"=>__('Nombre')])}}

            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-minus-circle"></i> '.__("Disable"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>





<script type="text/javascript">




    $(document).ready(function () {

        $('#frmPrinc1').on("submit", function (event){
            event.preventDefault();

                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response){
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status==1){
                        $.get("{{route('aeropuertos')}}", function (response){
                            $("#main-content").html(response);

                        }).fail(function (xhr){
                             $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    }else{
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function (){
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });

            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
