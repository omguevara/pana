<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Información de Aeropuertos')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->


        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12 col-lg-12 ">
                    <div class="row">
                        <img class="map " width="850" height="665"  usemap="#mapa"   alt="Mapa de Venezuela"  src="{{url('dist/img/mapa.png')}}" />


                        <map name="mapa"> 
                            <area data-bs-custom-class="custom-tooltip"  data-placement="top" title="sdfasdfasdf"   href="javascript:void(0)" onClick="setSede('sede-caracas')" shape="circle" coords="501,127,5" alt="Sede de Caracas" data-maphilight='{"stroke":true,"fillColor":"ff0000","fillOpacity":0.6}' >
                            <area id="aa"  href="javascript:void(0)" onClick="setSede('sede-caracas')" shape="circle" coords="530,75,5" alt="Sede de Caracas" data-maphilight='{"stroke":true,"fillColor":"ff0000","fillOpacity":0.6}' title="Sede de Caracas">
                        </map>



                    </div>
                </div>    
            </div>
        </div>
        <div class="card-footer text-center">
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            //430,55,10
            $('.map').maphilight();
            $('#aa').mouseover();
            
            $('.tips').tooltip({"html": true, "template": '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'});


        });
    </script>

</div>

