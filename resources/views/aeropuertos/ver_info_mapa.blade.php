<div id="panel_princ" class="col-sm-12 col-md-12  mt-1">
    
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Información del Aeropuerto')}}</h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->


        <div id="card-body-main" style="height: 100%; " class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12 col-lg-12 h-auto">
                    <div class="row">
                        <div  class="col-sm-12 col-md-6 col-lg-06 border-right ">
                            <div style="height: 65vh" id="map" class="map-container z-0"></div>


                        </div>
                        <div  class="col-sm-12 col-md-6 col-lg-6 ">

                            @foreach($Aeropuertos as $value3)


                            <div style="display:none" id="ae{{$value3->crypt_id}}" class="row aeropuertos">
                                <div  class="col-sm-12 col-md-12 col-lg-12 ">
                                    <h2>{{$value3->full_nombre}}</h2>
                                </div>

                                <div  class="col-sm-12 col-md-4 col-lg-6 ">

                                    <svg xmlns="http://www.w3.org/2000/svg" 
                                         width="200" 
                                         height="200" 
                                         viewBox="{{(isset($value3->estado) ? ($value3->estado->posicion2!=null ?  $value3->estado->posicion2:$value3->estado->posicion ):'')}}" 
                                         >
                                        <path stroke="black" stroke-width="4" fill="#d4e6f1" title="{{(isset($value3->estado) ? $value3->estado->name_state:'')}}" d="{{(isset($value3->estado) ? ($value3->estado->mapa2!=null ?  $value3->estado->mapa2:$value3->estado->mapa):'')}}"/>
                                    </svg>

                                </div>
                                <div  class="col-sm-12 col-md-4 col-lg-6 ">
                                    @if (file_exists(public_path('dist/img/aeropuertos/'.$value3->id.'.png')))
                                    <img src="{{url('dist/img/aeropuertos/'.$value3->id.'.png')}}" class="img-fluid" />
                                    @else
                                    <h3>Sin Fachada</h3>
                                    @endif

                                </div>
                                <div  class="col-sm-12 col-md-6 col-lg-6  mt-2 ">
                                    <a onClick="verInformacion('OPERACIONES', 'operaciones')" class="btn btn-block btn-warning" >{{$value3->check_info_operaciones==true ? '<i class="far fa-check-circle"></i> ':''}}OPERACIONES</a>
                                </div>


                                <div  class="col-sm-12 col-md-6 col-lg-6  mt-2 ">
                                    <a onClick="verInformacion('SEGURIDAD AEROPORTUARIA', 'seguridad')" class="btn  btn-block btn-primary" >{{$value3->check_info_seguridad==true ? '<i class="far fa-check-circle"></i> ':''}}SEGURIDAD AEROPORTUARIA</a>
                                </div>

                                <div  class="col-sm-12 col-md-6 col-lg-6  mt-2 ">
                                    <a onClick="verInformacion('SSEI', 'ssei')" class="btn  btn-block  btn-danger">{{$value3->check_info_bomberos==true ? '<i class="far fa-check-circle"></i> ':''}}SSEI</a>
                                </div>

                                <div  class="col-sm-12 col-md-6 col-lg-6  mt-2 ">
                                    <a onClick="verInformacion('CERTIFICACIÓN Y CAPACITACIÓN', 'certificacion')" class="btn  btn-block  btn-success">{{$value3->check_info_certificacion==true ? '<i class="far fa-check-circle"></i> ':''}}CERTIFICACIÓN Y CAPACITACIÓN</a>
                                </div>


                                <div  class="col-sm-12 col-md-6 col-lg-6 offset-md-3 offset-xl-3 mt-2 ">
                                    <a onClick="verInformacion('MISCELANEOS', 'miscelaneos')" class="btn  btn-block  btn-info" >MISCELANEOS</a>
                                </div>






                            </div>
                            @endforeach
                        </div>
                    </div>    
                </div>
            </div>
            <div class="row">
                <canvas id="grafica1"   style="min-width: 100%; height: 250px;" ></canvas>
            </div>
            <div class="card-footer text-center">
            </div>
        </div>
        
        {{Form::open(["route"=>"ver_info_mapa", "id"=>"formSeePdf", "target"=>"see_info" ,'autocomplete'=>'Off'])}}
        {{Form::hidden("form", '', ["id"=>"form"])}}
        {{Form::hidden("aeropuerto", '', ["id"=>"aeropuerto"])}}
        {{ method_field('PUT') }}
        {{ Form::close() }}
        <script type="text/javascript">
            var AEROPUESTO_SELECT = 0;
            var GRAFICA = [];
            $(document).ready(function () {
                var map = L.map('map').setView([7.621133322974677, -66.358886718750014], 5.5);
                
                
                let principal = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {maxZoom: 20}).addTo(map);
                let secondary = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');
                
                let airplaneIcon = L.icon({
                    iconUrl: "{{url('dist/img/marker.png')}}",
                    iconSize: [30, 30],
                    iconAnchor: [19, 19],
                    popupAnchor: [0, -10]
                });
                
                let airplaneIcon2 = L.icon({
                    iconUrl: "{{url('dist/img/marker2.png')}}",
                    iconSize: [30, 30],
                    iconAnchor: [19, 19],
                    popupAnchor: [0, -10]
                });
                
                let capas = {
                    'Mapa estándar': principal,
                    'Vista satélite': secondary
                }
                // secondary.setZIndex(0);
                // Remove original control
                map.zoomControl.remove();
                L.control.layers(capas).addTo(map);
                // Add custom control
                L.Control.zoomHome = L.Control.extend({
                    options: {
                        position: 'topleft',
                        zoomInText: '<i class="fa fa-plus" style="line-height:1.65;"></i>',
                        zoomInTitle: 'Zoom in',
                        zoomOutText: '<i class="fa fa-minus" style="line-height:1.65;"></i>',
                        zoomOutTitle: 'Zoom out',
                        zoomHomeText: '<i class="fa fa-home" style="line-height:1.65;"></i>',
                        zoomHomeTitle: 'Zoom home'
                    },

                    onAdd: function (map) {
                        var controlName = 'gin-control-zoom',
                                container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
                                options = this.options;

                        this._zoomInButton = this._createButton(options.zoomInText, options.zoomInTitle,
                                controlName + '-in', container, this._zoomIn);
                        this._zoomHomeButton = this._createButton(options.zoomHomeText, options.zoomHomeTitle,
                                controlName + '-home', container, this._zoomHome);
                        this._zoomOutButton = this._createButton(options.zoomOutText, options.zoomOutTitle,
                                controlName + '-out', container, this._zoomOut);

                        this._updateDisabled();
                        map.on('zoomend zoomlevelschange', this._updateDisabled, this);

                        return container;
                    },

                    onRemove: function (map) {
                        map.off('zoomend zoomlevelschange', this._updateDisabled, this);
                    },

                    _zoomIn: function (e) {
                        this._map.zoomIn(e.shiftKey ? 3 : 1);
                    },

                    _zoomOut: function (e) {
                        this._map.zoomOut(e.shiftKey ? 3 : 1);
                    },

                    _zoomHome: function (e) {
                        map.setView([7.621133322974677, -66.358886718750014], 5.5);
                    },

                    _createButton: function (html, title, className, container, fn) {
                        var link = L.DomUtil.create('a', className, container);
                        link.innerHTML = html;
                        link.href = '#';
                        link.title = title;

                        L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
                                .on(link, 'click', L.DomEvent.stop)
                                .on(link, 'click', fn, this)
                                .on(link, 'click', this._refocusOnMap, this);

                        return link;
                    },

                    _updateDisabled: function () {
                        var map = this._map,
                                className = 'leaflet-disabled';

                        L.DomUtil.removeClass(this._zoomInButton, className);
                        L.DomUtil.removeClass(this._zoomOutButton, className);

                        if (map._zoom === map.getMinZoom()) {
                            L.DomUtil.addClass(this._zoomOutButton, className);
                        }
                        if (map._zoom === map.getMaxZoom()) {
                            L.DomUtil.addClass(this._zoomInButton, className);
                        }
                    }
                });
                var zoomHome = new L.Control.zoomHome();
                // add the new control to the map
                zoomHome.addTo(map);

                // L.control.layers(capas, null, {collapsed: true}).addTo(map);

                principal.on('click', function (e) {
                    principal.addTo(map);
                    secondary.removeFrom(map);
                });
                secondary.on('click', function (e) {
                    secondary.addTo(map);
                    principal.removeFrom(map);
                });
                
                
                GRAFICA['A']=[];
                GRAFICA['R']=[];
                GRAFICA['C']=[];
                // get map locations
                @foreach($Aeropuertos  as $key=>$value)
                    
                    
                    GRAFICA['A'].push('{{$value->codigo_oaci}}');
                    GRAFICA['C'].push('blue');
                    
                    GRAFICA['R'].push({{ $value->check_info['cant'] }});
                    
                    
                    
                    @if ($value->latitude!="")
                        @if ($value->check_info['new_info']==false)
                    let marker{{$key}} = L.marker([{{$value->latitude}}, {{$value->longitude}}], {icon: airplaneIcon}).addTo(map);
                        @else
                        
                            let marker{{$key}} = L.marker([{{$value->latitude}}, {{$value->longitude}}], {icon: airplaneIcon2}).addTo(map);
                        @endif
                    // Agregar el contenido del botón "Ver más" a cada objeto de marcador
                    marker{{$key}}.bindPopup(createPopupContent('{{$value->codigo_oaci}}', '{{$value->full_nombre}}', '{{$value->crypt_id}}'));
                    marker{{$key}}.on('click', function (e) {
                        this.openPopup();
                        map.flyTo(e.latlng, 15, {
                            animate: true,
                            duration: 1.5
                        });
                        // map.setView(e.latlng, 15);
                    });
                    @endif
                @endforeach
                createGrafica(GRAFICA);

                $('#map').on('click', '#btn-ver-mas', function (event) {
                    /*let popupContent = $(this).parent();
                    popupContent.find('p').show();
                    $('#more-info').removeClass('d-none');
                    $('#more-info').fadeIn(1000);
          
                        */
                    $(".aeropuertos").hide();
                    $("#ae" + $(this).attr("data-id")).fadeIn("fast");
                    AEROPUESTO_SELECT = $(this).attr("data-id");
                });

                // Evento para cambiar el ancho del mapa de vuelta a su valor original al cerrar la descripción del marcador
                map.on('popupclose', function (e) {
                    $('#close-info').click(function () {
                        $('#more-info').addClass('d-none');
                        $('#more-info').fadeOut(1000);
                    });
                });

                $('#close-info').on('click', function () {
                    $('#more-info').addClass('d-none');
                    $('#more-info').fadeOut(1000);
                });

            });
            /*
            function displayInfo(id) {
            
                $(".aeropuertos").hide();
                $("#ae" + id).fadeIn("fast");
                AEROPUESTO_SELECT = id;
            }
            */
            function verInformacion(title, form) {
                $("#t-info").html(title);
                $("#modal-info-aeropuertos").modal("show");
                $("#form").val(form);
                $("#aeropuerto").val(AEROPUESTO_SELECT);
                $("#formSeePdf").submit();
            }
            function createPopupContent(title, content, mapa_id) {
                return '<div class="popup-content">' +
                    '<h3>' + title + '</h3>' +
                    '<p>' + content + '</p>' +
                    '<button data-id="'+mapa_id+'"  id="btn-ver-mas">Ver más</button>' +
                  '</div>';
            }
            
            function createGrafica(DATOS){
                G_1 = new Chart("grafica1", {
                    type: "bar",
                    data: {
                        labels: DATOS['A'],
                        datasets: [{
                                label: "Aeropuertos: " ,
                                backgroundColor: DATOS['C'],
                                data:DATOS['R']
                            }]
                    },

                    options: {
                        responsive: false,
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                        },
                        title: {
                            display: true,
                            text: "Cantidades de Vuelos Registrados en Aviación General"
                        },
                        plugins: {
                            tooltip: {
                                callbacks: {
                                    label: function (context) {
                                        let label = (context.dataset.label+context.parsed.y) || '';
                                        return label;
                                    }
                                }
                            }
                        }
                    }
                });
            }
            
            
        </script>

    </div>


    <div class="modal fade" id="modal-info-aeropuertos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 id="t-info"></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>


                <div  class="modal-body">

                    <div class="row">


                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <iframe src="" id="see_info" name="see_info" style="width:100%; height: 100vh; border: none">

                    </iframe>

                </div>


            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

</div>

