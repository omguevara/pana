

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Aeropuertos')}}</h3>

            <div class="card-tools">

                <a type="button"  href="{{route('aeropuertos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["aeropuertos.edit", $aereopuerto->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="form-group">
                <label for="codigo_oaci">{{__('Código OACI')}}</label>
                {{Form::text("codigo_oaci", $aereopuerto->codigo_oaci, ["maxlength"=>"6", "required"=>"required", "class"=>"form-control required", "id"=>"codigo_oaci", "placeholder"=>__('Código OACI')])}}

            </div>

            <div class="form-group">
                <label for="codigo_oaci">{{__('Código IATA')}}</label>
                {{Form::text("codigo_iata", $aereopuerto->codigo_oaci, ["maxlength"=>"6", "required"=>"required", "class"=>"form-control required", "id"=>"codigo_iata", "placeholder"=>__('Código IATA')])}}

            </div>
            <div class="form-group">
                <label for="nombre">{{__('Nombre')}}</label>
                {{Form::text("nombre", $aereopuerto->nombre, ["required"=>"required", "class"=>"form-control required", "id"=>"nombre", "placeholder"=>__('Nombre')])}}

            </div>

            <div class="form-group">
                <label for="categoria_id">{{__('Categoría')}}</label>
                {{Form::select("categoria_id", $clasificaciones,  $aereopuerto->categoria_id, ["required"=>"required", "class"=>"form-control required", "id"=>"categoria_id", "placeholder"=>__('Select')])}}

            </div>
            <div class="form-group">
                <label for="pais_id">{{__('País')}}</label>
                {{Form::select("pais_id", $paises,  $aereopuerto->pais_id, ["required"=>"required", "class"=>"form-control required", "id"=>"pais_id", "placeholder"=>__('Select'), "onchange" => "checkPais()"])}}

            </div>
            <div class="form-group">
                <label for="estado_id">{{__('Estado')}}</label>
                {{Form::select("estado_id", $estados,  $aereopuerto->estado_id, ["required"=>"required", "class"=>"form-control required", "id"=>"estado_id", "placeholder"=>__('Select')])}}

            </div>

            <div class="form-group">
                <label for="pertenece_baer">{{__('Pertenece a Baer')}}</label>
                {{Form::select("pertenece_baer", ["0"=>"No", "1"=>"Si"],  $aereopuerto->pertenece_baer, ["required"=>"required", "class"=>"form-control required", "id"=>"pertenece_baer", "placeholder"=>__('Select')])}}

            </div>

            <div class="form-group">
                <label for="iva">{{__('Aplica IVA')}}</label>
                {{Form::select("iva", ["0"=>"No","16.00"=>"Si"],  $aereopuerto->iva, ["required"=>"required", "class"=>"form-control required", "id"=>"iva", "placeholder"=>__('Select')])}}

            </div>

            <div class="form-group">
                <label for="hora_ocaso">{{__('Puesta del Sol')}}</label>
                {{Form::text("hora_ocaso", $aereopuerto->hora_ocaso, [ "required"=>"required", "class"=>"form-control required timer", "id"=>"hora_ocaso", "placeholder"=>__('Hora Ocaso')])}}

            </div>

            <div class="form-group">
                <label for="hora_amanecer">{{__('Salida del Sol')}}</label>
                {{Form::text("hora_amanecer", $aereopuerto->hora_amanecer, [ "required"=>"required", "class"=>"form-control required timer", "id"=>"hora_amanecer", "placeholder"=>__('Hora Amanecer')])}}

            </div>

            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>

<script type="text/javascript">

    $(document).ready(function () {
        $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM"});
        $('#frmPrinc1').on("submit", function (event){
            event.preventDefault();
            if ($('#frmPrinc1').valid()){
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response){
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status==1){
                        $.get("{{route('aeropuertos')}}", function (response){
                            $("#main-content").html(response);

                        }).fail(function (xhr){
                             $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    }else{
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function (){
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });
    function checkPais(){
    if ($('#pais_id').val() == 1){
        $('#estado_id').children().prop("disabled", false);
    }else{
        $('#estado_id').children().not('[value=25]').prop("disabled", true);
        $('#estado_id').val(25);

    }
}

</script>
</div>
