@if (isset($pdf))

<style>
    table.borde, table.borde th, table.borde tr td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 0;
        margin: 0;
        border-spacing: 0;
    }
    .t1{
        background-color:grey;
        text-align:center;
        font-size: 14px;
    }
    .t2{
        background-color: #dbd9d9;
        text-align:center;
        font-size: 12px;
    }

    table.sn_border tr td{
        border:none;
    }
    tr:hover td{
        background-color: #75bff2;
    }
    input[type=text]{





        min-width: 95%;
        max-width: 95%;

        font-size: 12px;

        margin-left: 2.5%;
        margin-bottom: 5px;
        margin-top: 5px;
        border: none;
    }

    input[type="checkbox"], input[type="radio"]{
        width:20px;
        height:20px;
    }




    .no_pdf {
        display: none;


    }

    .pagenum:before {
        content: counter(page);
    }

 
</style>
<table class="borde" style="width: 100%">
    <tr>
        <td class="t1" colspan="2"><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>
    </tr>
    <tr>
        <td class="t1" colspan="2">SEGURIDAD AEROPUERTUARIA</td>
    </tr>
    <tr>
        <td class="t2" width="200px">Nombre del Aeropuerto</td>
        <td>{{$Aeropuerto1->nombre}}</td>
    </tr>
    <tr>
        <td class="t2">Designador OACI</td>
        <td>{{$Aeropuerto1->codigo_oaci}}</td>
    </tr>
    <tr>
        <td class="t2">Ubicación</td>
        <td></td>
    </tr>
    <tr>
        <td class="t2">Horario Operacional</td>
        <td></td>
    </tr>
</table>


<br/>
@endif
{{Form::open(["route"=>"ver_mapa",  "target"=>"_blanck" ,'autocomplete'=>'Off'])}}
{{Form::hidden("form", 'seguridad')}}
{{ method_field('PUT') }}
<button type="submit" class="btn btn-md no_pdf">
    <i class="fas fa-file-pdf"></i> Descargar
</button>
{{ Form::close() }}




{{Form::open(["route"=>"ver_mapa",  'id'=>'frmSeg','autocomplete'=>'Off'])}}
{{Form::hidden("form", 'seguridad')}}
<table class="borde " style="width: 100%">
    <tr>
        <th colspan="4" class="t1">1. PERSONAL Y EQUIPOS DE SEGURIDAD</th>
    </tr>
    <tr>
        <th colspan="4">Si no cuenta con los equipos mencionados dejar los campos de Operativos e inoperativos vacíos</th>
    </tr>
    <tr>
        <td>1</td>
        <td>Número de Supervisores de Seguridad</td>
        <td colspan="2"><input type="text" class="number" name="s_ns_avsec" value="{{($InfoSeguridad!= null ? $InfoSeguridad->s_ns_avsec:'')}}"/></td>
    </tr>
    <tr>
        <td>2</td>
        <td>Número de Agentes de Seguridad</td>
        <td colspan="2"><input type="text" class="number" name="s_na_avsec" value="{{($InfoSeguridad!= null ? $InfoSeguridad->s_na_avsec:'')}}"/></td>
    </tr>

    <tr>
        <td>3</td>
        <td>Supervisores con curso Inicial</td>
        <td colspan="2"><input type="text" class="number" name="s_nsavseci" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_nsavseci:'')}}"/></td>
    </tr>
    <tr>
        <td>4</td>
        <td>Supervisores con curso Recurrente</td>
        <td colspan="2"><input type="text" class="number" name="s_nsavsecr" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_nsavsecr:'')}}"/></td>
    </tr>
    <tr>
        <td>5</td>
        <td>Agentes con curso Inicial</td>
        <td colspan="2"><input type="text" class="number" name="s_naavseci" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_naavseci:'')}}"/></td>
    </tr>
    <tr>
        <td>6</td>
        <td>Agentes con curso Recurrente</td>
        <td colspan="2"><input type="text" class="number" name="s_naavsecr" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_naavsecr:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">7</td>
        <td rowspan="2">Detector de Metales de Portico</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_edmp_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_edmp_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_edmp_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_edmp_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">8</td>
        <td rowspan="2">Detector de Metales Manual</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_edmm_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_edmm_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_edmm_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_edmm_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">9</td>
        <td rowspan="2">Máquina de Rayos X de mano</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_mrpc_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_mrpc_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_mrpc_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_mrpc_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">10</td>
        <td rowspan="2">Máquina X de Bodega</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_mb_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_mb_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_mb_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_mb_i:'')}}"/></td>
    </tr>    
    <tr>
        <td rowspan="2">11</td>
        <td rowspan="2">Radios Portátiles</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_rp_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_rp_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_rp_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_rp_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">12</td>
        <td rowspan="2">Radios Base</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_rb_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_rb_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_rb_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_rb_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">13</td>
        <td rowspan="2">Cámaras</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_c_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_c_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_c_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_c_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">14</td>
        <td rowspan="2">Grabadoras</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_g_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_g_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_g_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_g_i:'')}}"/></td>
    </tr>
    <tr>
        <td rowspan="2">15</td>
        <td rowspan="2">BodyScanners</td>
        <td>Operativos</td>
        <td><input type="text" class="number" name="s_mech_o" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_mech_o:'')}}"/></td>
    </tr>
    <tr>
        <td>Inoperativos</td>
        <td><input type="text" class="number" name="s_mech_i" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_mech_i:'')}}"/></td>
    </tr>
    
    
    <tr>
        <th colspan="4" class="t1">2. PROGRAMAS Y PLANES</th>
    </tr>
    <tr>
        <td rowspan="3">1</td>
        <td rowspan="3">Plan de Contingencia (PC)</td>
        <td>Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_cls ===true ? 'checked':'') :'') }}  name="s_cls" value="1" /></td>
    </tr>
    <tr>
        <td>No Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_cls ===false ? 'checked':'') :'') }} name="s_cls" value="0" /></td>
    </tr>
    <tr>
        <td>No Aplica</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_cls ===null ? 'checked':'') :'') }} name="s_cls" value="null" /></td>
    </tr>
    <tr>
        <td rowspan="3">2</td>
        <td rowspan="3">Programa Local de Seguridad (PLS)</td>
        <td>Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pls ===true ? 'checked':'') :'') }}  name="s_pls" value="1" /></td>
    </tr>
    <tr>
        <td>No Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pls ===false ? 'checked':'') :'') }}  name="s_pls" value="0" /></td>
    </tr>
    <tr>
        <td>No Aplica</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pls ===null ? 'checked':'') :'') }}  name="s_pls" value="null" /></td>
    </tr>
    <tr>
        <td rowspan="3">3</td>
        <td rowspan="3">Programa Intrucción en Materia de Seguridad de la Aviación Civil  para AD (PISA)</td>
        <td>Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pimsacad ===true ? 'checked':'') :'') }}  name="s_pimsacad" value="1" /></td>
    </tr>
    <tr>
        <td>No Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pimsacad ===false ? 'checked':'') :'') }}  name="s_pimsacad" value="0" /></td>
    </tr>
    <tr>
        <td>No Aplica</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pimsacad ===null ? 'checked':'') :'') }}  name="s_pimsacad" value="null" /></td>
    </tr>
    <tr>
        <td rowspan="3">4</td>
        <td rowspan="3">Programa de Control de la Calidad en Materia de Seguridad de la Aviación Civil para AD (PCC)</td>
        <td>Certificado</td>
        <td align="center"><input type="radio" {{ ($InfoSeguridad != null ? ($InfoSeguridad->s_pccmsacad ===true ? 'checked':'') :'') }}  name="s_pccmsacad" value="1" /></td>
    </tr>
    <tr>
        <td>No Certificado</td>
        <td align="center"><input type="radio" {{ ( $InfoSeguridad != null ? ($InfoSeguridad->s_pccmsacad ===false ? 'checked':'') :'') }}  name="s_pccmsacad" value="0" /></td>
    </tr>
    <tr>
        <td>No Aplica</td>
        <td align="center"><input type="radio" {{ ( $InfoSeguridad != null ? ($InfoSeguridad->s_pccmsacad ===null ? 'checked':'') :'') }}  name="s_pccmsacad" value="null" /></td>
    </tr>
</table>
<table class="borde" style="width: 100%">
    <tr>
        <th colspan="4" class="t1">3. ORGANISMOS DE SEGURIDAD DE ESTADO</th>
    </tr>
    <tr style="display: none">
        <td>1</td>
        <td>Administración de AD</td>
        <td colspan="2"><input type="text" name="s_aad" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_aad:' ')}}"/></td>
    </tr>
    <tr style="display: none">
        <td>2</td>
        <td>Ofinina de Notificación ARO/AIS</td>
        <td colspan="2"><input type="text" name="s_ona" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_ona:' ')}}"/></td>
    </tr>
    <tr style="display: none">
        <td>3</td>
        <td>Ofinina de Notificación MET</td>
        <td colspan="2"><input type="text" name="s_onm" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_onm:' ')}}"/></td>
    </tr>
    <tr style="display: none">
        <td>4</td>
        <td>ATS</td>
        <td colspan="2"><input type="text" name="s_ats" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_ats:' ')}}"/></td>
    </tr>
    <tr style="display: none">
        <td>5</td>
        <td>Abastecimiento de Combustible</td>
        <td colspan="2"><input type="text" name="s_ac" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_ac:' ')}}"/></td>
    </tr>
    <tr style="display: none">
        <td>6</td>
        <td>Servicio de Escala / Handling</td>
        <td colspan="2"><input type="text" name="s_seh" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_seh:' ')}}"/></td>
    </tr>
    <tr style="display: none">
        <td>7</td>
        <td>Seguridad AVSEC</td>
        <td colspan="2"><input type="text" name="s_avsec" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_avsec:' ')}}"/></td>
    </tr>
    <tr>
        <td>1</td>
        <td>GNB Antidrogas</td>
        <td colspan="2"><input type="text" name="s_gnb_a" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_gnb_a:'')}}"/></td>
    </tr>
    <tr>
        <td>2</td>
        <td>SENIAT</td>
        <td colspan="2"><input type="text" name="s_seniat" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_seniat:'')}}"/></td>
    </tr>
    <tr>
        <td>3</td>
        <td>SAIME</td>
        <td colspan="2"><input type="text" name="s_saime" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_saime:'')}}"/></td>
    </tr>
    <tr>
        <td>4</td>
        <td>CICPC</td>
        <td colspan="2"><input type="text" name="s_ds" maxlength="80" value="{{($InfoSeguridad != null ? $InfoSeguridad->s_ds:'')}}"/></td>
    </tr>
    <tr>
        <td>5</td>
        <td>GNB  Resguardo Nacional</td>
        <td colspan="2"><input type="text" name="s_gnb_rn" maxlength="80" value="{{($InfoSeguridad!= null ? $InfoSeguridad->s_gnb_rn:'')}}"/></td>
    </tr>
    <tr>
        <td>6</td>
        <td>PNB</td>
        <td colspan="2"><input type="text" class="number" maxlength="80" name="s_pnb" value="{{($InfoSeguridad!= null ? $InfoSeguridad->s_pnb:'')}}"/></td>
    </tr>
    <tr>
        <td>7</td>
        <td>SEBIN</td>
        <td colspan="2"><input type="text" class="number" name="s_sebin" value="{{($InfoSeguridad!= null ? $InfoSeguridad->s_sebin:'')}}"/></td>
    </tr>
    <tr>

        <td colspan="4" align="center">{{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary no_pdf", "id"=>"save"])}}</td>
    </tr>

</table>


{{ Form::close() }}