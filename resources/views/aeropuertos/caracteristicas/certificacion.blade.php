@if (isset($pdf))

<style>
    table.borde, table.borde th, table.borde tr td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 0;
        margin: 0;
        border-spacing: 0;
    }
    .t1{
        background-color:grey;
        text-align:center;
        font-size: 14px;
    }
    .t2{
        background-color: #dbd9d9;
        text-align:center;
        font-size: 12px;
    }

    table.sn_border tr td{
        border:none;
    }
    tr:hover td{
        background-color: #75bff2;
    }
    input[type=text]{



        

        width: auto !important;

        font-size: 12px;

        margin-left: 2.5%;
        margin-bottom: 5px;
        margin-top: 5px;
        border: solid 1px;
    }

    input[type="checkbox"], input[type="radio"]{
        width:20px;
        height:20px;
    }




    .no_pdf {
        display: none;


    }
</style>
<table class="borde" style="width: 100%">
    <tr>
        <td class="t1" colspan="2"><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>
    </tr>
    <tr>
        <td class="t1" colspan="2">CERTIFICACIÓN Y CAPACITACIÓN</td>
    </tr>
    <tr>
        <td class="t2" width="200px">Nombre del Aeropuerto</td>
        <td>{{$Aeropuerto1->nombre}}</td>
    </tr>
    <tr>
        <td class="t2">Designador OACI</td>
        <td>{{$Aeropuerto1->codigo_oaci}}</td>
    </tr>
    <tr>
        <td class="t2">Ubicación</td>
        <td></td>
    </tr>
    <tr>
        <td class="t2">Horario Operacional</td>
        <td></td>
    </tr>
</table>
<br/>
@endif
{{Form::open(["route"=>"ver_mapa",  "target"=>"_blanck" ,'autocomplete'=>'Off'])}}
{{Form::hidden("form", 'certificacion')}}
{{ method_field('PUT') }}
<button type="submit" class="btn btn-md no_pdf">
    <i class="fas fa-file-pdf"></i> Descargar
</button>
{{ Form::close() }}




{{Form::open(["route"=>"ver_mapa",  'id'=>'frmCer','autocomplete'=>'Off'])}}
{{Form::hidden("form", 'certificacion')}}

<table class="borde" style="width: 100%">

    <tr>
        <th colspan="9" class="t1"> 1. CERTIFICACIÓN</th>
    </tr>
    <tr>
        <td class="t2" colspan="3">FASE</td>
        <td class="t2" colspan="3"> <input onClick="if (this.checked == false) {
                    $('.cert').prop('disabled', false);
                } else {
                    $('.cert').prop('disabled', true);
                }" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->fase_all==true ? 'checked':''):'' ) }} name="fase_all" type="checkbox" />  CERTIFICACIÓN CULMINADA</td>
        <td class="t2">SI</td>
        <td class="t2">NO</td>
        <td class="t2">FECHA</td>
    </tr>
    <tr>
        <td colspan="6">FASE 1</td>
        <td><input onClick="$('#ff1').prop('disabled', false).focus()" value="1" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==true ? 'checked':''):'' ) }} name="f1" /></td>
        <td><input onClick="$('#ff1').val('').prop('disabled', true)" value="0" class="cert" type="radio"  {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==false ? 'checked':''):'' ) }} name="f1" /></td>
        <td><input type="text"  name="ff1" class="date cert" id="ff1" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==false ? 'disabled':''):'' ) }} value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ff1:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">FASE 2</td>
        <td><input onClick="$('#ff2').prop('disabled', false).focus()" value="1" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f2==true ? 'checked':''):'' ) }} name="f2" /></td>
        <td><input onClick="$('#ff2').val('').prop('disabled', true)" value="0" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f2==false ? 'checked':''):'' ) }} name="f2" /></td>
        <td><input type="text"  name="ff2" class="date cert" id="ff2" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==false ? 'disabled':''):'' ) }} value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ff2:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">FASE 3</td>
        <td><input onClick="$('#ff3').prop('disabled', false).focus()" value="1" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f3==true ? 'checked':''):'' ) }} name="f3" /></td>
        <td><input onClick="$('#ff3').val('').prop('disabled', true)" value="0" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f3==false ? 'checked':''):'' ) }} name="f3" /></td>
        <td><input type="text"  name="ff3" class="date cert" id="ff3" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==false ? 'disabled':''):'' ) }} value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ff3:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">FASE 4</td>
        <td><input onClick="$('#ff4').prop('disabled', false).focus()" value="1" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f4==true ? 'checked':''):'' ) }} name="f4" /></td>
        <td><input onClick="$('#ff4').val('').prop('disabled', true)" value="0" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f4==false? 'checked':''):'' ) }} name="f4" /></td>
        <td><input type="text"  name="ff4" class="date cert" id="ff4" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==false ? 'disabled':''):'' ) }} value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ff4:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">FASE 5</td>
        <td><input onClick="$('#ff5').prop('disabled', false).focus()" value="1" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f5==true ? 'checked':''):'' ) }} name="f5" /></td>
        <td><input onClick="$('#ff5').val('').prop('disabled', true)" value="0" class="cert" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f5==false ? 'checked':''):'' ) }} name="f5" /></td>
        <td><input type="text"  name="ff5" class="date cert" id="ff5" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->f1==false ? 'disabled':''):'' ) }} value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ff5:'' ) }}" /></td>
    </tr>

    <tr>
        <th colspan="9" class="t1"> 2. CAPACITACIÓN</th>
    </tr>
    <tr>
        <td class="t2" colspan="6">CURSO</td>
        <td class="t2">SI</td>
        <td class="t2">NO</td>
        <td class="t2">FECHA</td>
    </tr>
    <tr>
        <td colspan="6">SMS</td>
        <td><input onClick="$('#f_c_sms').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_sms==true ? 'checked':''):'' ) }} value="1" name="c_sms" /></td>
        <td><input onClick="$('#f_c_sms').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_sms==false ? 'checked':''):'' ) }} value="0" name="c_sms" /></td>
        <td><input type="text"  name="f_c_sms" class="date" id="f_c_sms" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_c_sms:'' ) }}" /></td>
    </tr>
    {{-- <tr>
        <td colspan="6">AV SEC</td>
        <td><input onClick="$('#f_c_avsev').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_avsev==true ? 'checked':''):'' ) }} value="1" name="c_avsev" /></td>
        <td><input onClick="$('#f_c_avsev').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_avsev==false ? 'checked':''):'' ) }} value="0" name="c_avsev" /></td>
        <td><input type="text"  name="f_c_avsev" class="date" id="f_c_avsev" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_c_avsev:'' ) }}" /></td>
    </tr> --}}<input type="hidden"  name="f_c_avsev" class="date" id="f_c_avsev" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_c_avsev:'' ) }}" />
    <tr>
        <td colspan="6">MERCANCÍA PELIGROSAS</td>
        <td><input onClick="$('#f_c_mp').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_mp==true ? 'checked':''):'' ) }} value="1" name="c_mp" /></td>
        <td><input onClick="$('#f_c_mp').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_mp==false ? 'checked':''):'' ) }} value="0" name="c_mp" /></td>
        <td><input type="text"  name="f_c_mp" class="date" id="f_c_mp" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_c_mp:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">TRATO DE PERSONAS CON  DISCAPACIDAD</td>
        <td><input onClick="$('#f_c_tpd').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_tpd==true ? 'checked':''):'' ) }} value="1" name="c_tpd" /></td>
        <td><input onClick="$('#f_c_tpd').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->c_tpd==false ? 'checked':''):'' ) }} value="0" name="c_tpd" /></td>
        <td><input type="text"  name="f_c_tpd" class="date" id="f_c_tpd" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_c_tpd:'' ) }}" /></td>
    </tr>


    <tr>
        <th colspan="9" class="t1"> 3. PLANES</th>
    </tr>
    <tr>
        <td class="t2" rowspan="2" colspan="6">Tipo de Plan</td>
        <td class="t2" colspan="3" >¿Elaborado?</td>
    </tr>
    <tr>

        <td class="t2" >SI</td>
        <td class="t2" >NO</td>
        <td class="t2" >FECHA</td>

    </tr>
    <tr>
        <td colspan="6">Plan Maestro</td>
        <td><input onClick="$('#f_p_pm').prop('disabled', false).focus()" type="radio"  {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_pm==true ? 'checked':''):'' ) }} value="1" name="p_pm" /></td>
        <td><input onClick="$('#f_p_pm').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_pm==false ? 'checked':''):'' ) }} value="0" name="p_pm" /></td>
        <td><input type="text"  name="f_p_pm" class="date" id="f_p_pm" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_pm:'' ) }}" /></td>

    </tr>
    {{-- <tr>
        <td colspan="6">Plan de Contingencia</td>
        <td><input onClick="$('#f_p_pc').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_pc==true ? 'checked':''):'' ) }} value="1" name="p_pc" /></td>
        <td><input onClick="$('#f_p_pc').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_pc==false ? 'checked':''):'' ) }} value="0" name="p_pc" /></td>
        <td><input type="text"  name="f_p_pc" class="date" id="f_p_pc" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_pc:'' ) }}" /></td>
    </tr> --}}<input type="hidden"  name="f_p_pc" class="date" id="f_p_pc" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_pc:'' ) }}" />

    <tr>
        <td colspan="6">Plan de Emergencia</td>
        <td><input onClick="$('#f_p_pe').prop('disabled', false).focus()" type="radio"  {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_pe==true ? 'checked':''):'' ) }} value="1" name="p_pe" /></td>
        <td><input onClick="$('#f_p_pe').val('').prop('disabled', true)" type="radio"  {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_pe==false ? 'checked':''):'' ) }} value="0"  name="p_pe" /></td>
        <td><input type="text"  name="f_p_pe" class="date" id="f_p_pe" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_pe:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">Plan de Traslado de Aeronaves Inutilizadas</td>
        <td><input onClick="$('#f_p_tai').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_tai==true ? 'checked':''):'' ) }} value="1" name="p_tai" /></td>
        <td><input onClick="$('#f_p_tai').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_tai==false ? 'checked':''):'' ) }} value="0" name="p_tai" /></td>
        <td><input type="text"  name="f_p_tai" class="date" id="f_p_tai" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_tai:'' ) }}" /></td>

    </tr>
    <tr>
        <td colspan="6">Plan de control de Peligro Aviario y Fauna</td>
        <td><input onClick="$('#f_p_cpaf').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_cpaf==true ? 'checked':''):'' ) }} value="1" name="p_cpaf" /></td>
        <td><input onClick="$('#f_p_cpaf').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_cpaf==false ? 'checked':''):'' ) }} value="0" name="p_cpaf" /></td>
        <td><input type="text"  name="f_p_cpaf" class="date" id="f_p_cpaf" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_cpaf:'' ) }}" /></td>

    </tr>

    <tr>
        <td colspan="6">Plan de Capacitación del Aeropuerto</td>

        <td><input onClick="$('#f_p_ca').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_ca==true ? 'checked':''):'' ) }} value="1" name="p_ca" /></td>
        <td><input onClick="$('#f_p_ca').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_ca==false ? 'checked':''):'' ) }} value="0" name="p_ca" /></td>
        <td><input type="text"  name="f_p_ca" class="date" id="f_p_ca" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_ca:'' ) }}" /></td>
    </tr>




    <tr>
        <th colspan="9" class="t1"> 4.PROGRAMAS</th>
    </tr>
    <tr>
        <td class="t2" rowspan="2" colspan="6">Tipo de Programa</td>
        <td class="t2" colspan="3" >Elaborado?</td>
    </tr>
    <tr>

        <td class="t2" >SI</td>
        <td class="t2" >NO</td>
        <td class="t2" >Fecha</td>
    </tr>
    <tr>
        <td colspan="6">Programa de Gestión del Medio Ambiente</td>
        <td><input onClick="$('#f_p_gma').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_gma==true ? 'checked':''):'' ) }} value="1" name="p_gma" /></td>
        <td><input onClick="$('#f_p_gma').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_gma==false ? 'checked':''):'' ) }} value="0" name="p_gma" /></td>
        <td><input type="text"  name="f_p_gma" class="date" id="f_p_gma" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_gma:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">Programa de Control de Calidad</td>
        <td><input onClick="$('#f_p_cc').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_cc==true ? 'checked':''):'' ) }} value="1" name="p_cc" /></td>
        <td><input onClick="$('#f_p_cc').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_cc==false ? 'checked':''):'' ) }} value="0" name="p_cc" /></td>
        <td><input type="text"  name="f_p_cc" class="date" id="f_p_cc"  value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_cc:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">Programa local de Facilitación</td>
        <td><input onClick="$('#f_p_lf').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_lf==true ? 'checked':''):'' ) }} value="1" name="p_lf" /></td>
        <td><input onClick="$('#f_p_lf').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_lf==false ? 'checked':''):'' ) }} value="0" name="p_lf" /></td>
        <td><input type="text"  name="f_p_lf" class="date" id="f_p_lf" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_lf:'' ) }}" /></td>
    </tr>
    {{-- <tr>
        <td colspan="6">Programa de Seguridad Local</td>
        <td><input onClick="$('#f_p_sl').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_sl==true ? 'checked':''):'' ) }} value="1" name="p_sl" /></td>
        <td><input onClick="$('#f_p_sl').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->p_sl==false ? 'checked':''):'' ) }} value="0" name="p_sl" /></td>
        <td><input type="text"  name="f_p_sl" class="date" id="f_p_sl" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_sl:'' ) }}" /></td>
    </tr> --}}<input type="hidden"  name="f_p_sl" class="date" id="f_p_sl" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_p_sl:'' ) }}" />
    <tr>
        <th colspan="9" class="t1"> 5.ESTUDIOS</th>
    </tr>
    <tr>
        <td class="t2" rowspan="2" colspan="6">Tipo de Estudio</td>
        <td class="t2" colspan="3" >¿Elaborado?</td>
    </tr>
    <tr>

        <td class="t2" >SI</td>
        <td class="t2" >NO</td>
        <td class="t2" >Fecha</td>
    </tr>
    <tr>
        <td colspan="6">Estudio Económico Financiero</td>
        <td><input onClick="$('#f_e_ef').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->e_ef==true ? 'checked':''):'' ) }} value="1" name="e_ef" /></td>
        <td><input onClick="$('#f_e_ef').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->e_ef==false ? 'checked':''):'' ) }} value="0" name="e_ef" /></td>
        <td><input type="text"  name="f_e_ef" class="date" id="f_e_ef" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_e_ef:'' ) }}" /></td>
    </tr>
    <tr>
        <td colspan="6">Estudio Ecológico</td>
        <td><input onClick="$('#f_e_e').prop('disabled', false).focus()" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->e_e==true ? 'checked':''):'' ) }} value="1" name="e_e" /></td>
        <td><input onClick="$('#f_e_e').val('').prop('disabled', true)" type="radio" {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->e_e==false ? 'checked':''):'' ) }} value="0" name="e_e" /></td>
        <td><input type="text"  name="f_e_e" class="date" id="f_e_e" value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->f_e_e:'' ) }}" /></td>
    </tr>
    

    <tr>
        <th colspan=" 9" class="t1"><center>6.CAPACITACIÓN PERSONAL OPERACIONES AÉREAS</center></th>
</tr>
<tr>
    <th class="t2" colspan="2">CURSOS</th>
    <th class="t2">PERSONAL CAPACITADO</th>
    <th class="t2">PERSONAL SIN CAPACITACIÓN</th>
    <th class="t2" colspan="5">OBSERVACIONES</th>
</tr>

<tr>
    <th colspan="2" class="t2">MMPP</th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->mmpp_pc:'' ) }}" type ="text" name="mmpp_pc" id="mmpp_pc"></th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->mmpp_psn:'' ) }}" type="text" name="mmpp_psn" id="mmpp_psn"></th>
    <th colspan="5"><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->mmpp_obs:'' ) }}" type="text" name="mmpp_obs" id="mmpp_obs"></th>
</tr>

<tr>
    <th colspan="2" class="t2">SMS</th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->sms_pc:'' ) }}" type="text" name="sms_pc" id="sms_pc"></th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->sms_psn:'' ) }}" type="text" name="sms_psn" id="sms_psn"></th>
    <th colspan="5"><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->sms_obs:'' ) }}" type="text" name="sms_obs" id="sms_obs"></th>
</tr>

<tr>
    <th colspan="2" class="t2">FFHH</th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ffhh_pc:'' ) }}" type="text" name="ffhh_pc" id="ffhh_pc"></th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ffhh_psn:'' ) }}" type="text" name="ffhh_psn" id="ffhh_psn"></th>
    <th colspan="5"><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->ffhh_obs:'' ) }}" type="text" name="ffhh_obs" id="ffhh_obs"></th>
</tr>

<tr>
    <th colspan="2" class="t2">FACILITACIÓN</th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fac_pc:'' ) }}" type="text" name="fac_pc" id="fac_pc"></th>
    <th><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fac_psn:'' ) }}" type="text" name="fac_psn" id="fac_psn"></th>
    <th colspan="5"><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fac_obs:'' ) }}" type="text" name="fac_obs" id="fac_obs"></th>
</tr>

<tr>
    <th colspan="2" class="t2">CONAPDIS</th>
    <th colspan="2" >
        <select name="conapdis" id="conapdis">
            <option value=""></option>
            <option {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->conapdis=='1' ? 'selected="selected"':''):'' ) }} value="1">SI</option>
            <option {{ ( $InfoCertificacion  != null ? ($InfoCertificacion->conapdis=='0' ? 'selected="selected"':''):'' ) }} value="0">NO</option>
        </select>
    </th>
    <th colspan="5"><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->conapdis_obs:'' ) }}" type="text" name="conapdis_obs" id="conapdis_obs"></th>
</tr>
</table>

<table class="borde" style="width: 100%">
    {{-- <tr>
        <th style="font-size:10px !important"  class="t2" rowspan="2">FECHA<br>ÚLTIMA<br>CAPACITACIÓN</th>
        <th style="font-size:10px !important"  class="t2"  rowspan="2">FECHA<br>ESTIMADA<br>CAPACITACIÓN</th>
        <th  style="font-size:10px !important"  class="t2" rowspan="2">NRO.<br>PERSONAL<br>AVSEC</th>


        <th  style="font-size:10px !important"  class="t2" colspan="2">
            FUNCIONARIOS CON<br>CAPACITACIÓN<br>(RECURRENTE)
        </th>



        <th  style="font-size:10px !important"  class="t2" colspan="2">
            FUNCIONARIOS SIN<br>CAPACITACIÓN
        </th>

        
    </tr>

    <tr>


        <th  style="font-size:10px !important; text-align: center !important">
            AGENT E
        </th>

        <th  style="font-size:10px !important; text-align: center !important">
            SUPER V
        </th>

        <th  style="font-size:10px !important; text-align: center !important">
            AGENT E
        </th>

        <th  style="font-size:10px !important; text-align: center !important">
            SUPER V
        </th>


    </tr> --}}


    {{-- <tr>
        <td><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fuc:'' ) }}" type="text" name="fuc" class="date" id="fuc"></td>
        <td><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fec:'' ) }}" type="text" name="fec" class="date" id="fec"></td>
        <td><input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->npavsec:'' ) }}" type="text" name="npavsec" id="npavsec"></td>


        <td>
            <input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fccrae:'' ) }}" type="text" name="fccrae" id="fccrae">
        </td>

        <td>
            <input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fccrsv:'' ) }}" type="text" name="fccrsv" id="fccrsv">
        </td>







        <td>
            <input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fsciae:'' ) }}" type="text" name="fsciae" id="fsciae">
        </td>

        <td>
            <input value="{{ ( $InfoCertificacion  != null ? $InfoCertificacion->fscisv:'' ) }}" type="text" name="fscisv" id="fscisv">
        </td>



    </tr> --}}
    <tr>

        <td colspan="13" align="center">{{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary no_pdf", "id"=>"save"])}}</td>
    </tr>
</table>

{{Form::hidden("npadsa", "")}}    
        {{Form::hidden("npadss", "")}}    
        {{Form::hidden("fcciae", "")}}    
        {{Form::hidden("fccisv", "")}} 
         {{Form::hidden("fscrae", "")}}    
        {{Form::hidden("fscrsv", "")}} 
        {{Form::hidden("fuc", "")}} 
        {{Form::hidden("fec", "")}} 
        {{Form::hidden("npavsec", "")}} 
        {{Form::hidden("fccrae", "")}} 
        {{Form::hidden("fccrsv", "")}} 
        {{Form::hidden("fsciae", "")}} 
        {{Form::hidden("fscisv", "")}} 

{{ Form::close() }}