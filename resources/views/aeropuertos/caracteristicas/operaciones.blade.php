@if (isset($pdf))

<style>

    @page {
        margin: 20px 20px;
        font-family: Arial;
    }

    body {
        margin: 20px 20px 20px 20px;
    }

    table.borde, table.borde th, table.borde tr td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 0;
        margin: 0;
        border-spacing: 0;
    }
    .t1{
        background-color:grey;
        text-align:center;
        font-size: 14px;
    }
    .t2{
        background-color: #dbd9d9;
        text-align:center;
        font-size: 12px;
    }

    table.sn_border tr td{
        border:none;
    }
    tr:hover td{
        background-color: #75bff2;
    }
    select{
         width: auto !important;
         border: none;
         display: block;
    }
    input[type=text]{



        width: auto !important;


        font-size: 12px;


        margin-bottom: 5px;
        margin-top: 5px;
        border: none;
    }

    input[type="checkbox"], input[type="radio"]{
        width:20px;
        height:20px;
    }




    .no_pdf {
        display: none;


    }

    .pagenum:before {
        content: counter(page);
    }


</style>



<table class="borde" style="width: 100%">
    <tr>
        <td class="t1" colspan="2"><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>
    </tr>
    <tr>
        <td class="t1" colspan="2">OPERACIONES</td>
    </tr>
    <tr>
        <td class="t2" width="200px">Nombre del Aeropuerto</td>
        <td>{{$Aeropuerto1->nombre}}</td>
    </tr>
    <tr>
        <td class="t2">Designador OACI</td>
        <td>{{$Aeropuerto1->codigo_oaci}}</td>
    </tr>
    <tr>
        <td class="t2">Ubicación</td>
        <td></td>
    </tr>
    <tr>
        <td class="t2">Horario Operacional</td>
        <td></td>
    </tr>
</table>


<br/>
@endif
{{Form::open(["route"=>"ver_mapa",  "target"=>"_blanck" ,'autocomplete'=>'Off'])}}
{{Form::hidden("form", 'operaciones')}}
{{ method_field('PUT') }}
<button type="submit" class="btn btn-md no_pdf">
    <i class="fas fa-file-pdf"></i> Descargar
</button>
{{ Form::close() }}

{{Form::open(["route"=>"ver_mapa", "id"=>"frmOper" ,  'id'=>'frmOper','autocomplete'=>'Off'])}}
{{Form::hidden("form", 'operaciones')}}

<table class="borde" style="width: 100%">
    <tr>
        <th colspan="9" class="t1">1. PISTAS</th>
    </tr>
  
    <tr>
        <td class="t2" style="width:10%" >Orientación</td>
        <td class="t2" style="width:10%">Largo (m)</td>
        <td class="t2" style="width:10%">Ancho (m)</td>
        <td class="t2" style="width:10%">PCN 1</td>
        <td  style="width:10%"><input value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->pc1_pstas:'' ) }}" type="text" maxlength="100" name="pc1_pstas" id="pc1_pstas"/></td>
        <td style="width:50%"  class="t2" colspan="4">1.1 Distancias Declaradas</td>
    </tr>

    <tr>
        <td rowspan="2" style="width:10%"><input type="text" maxlength="100" name="orientacion" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->orientacion:'' ) }}" id="orientacion"/> </td>
        <td rowspan="2" style="width:10%"><input type="text" maxlength="100" name="largo" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->largo:'' ) }}" id="largo"/></td>
        <td rowspan="2" style="width:10%"><input type="text" maxlength="100" name="ancho" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->ancho:'' ) }}" id="ancho"/></td>
        <td class="t2" style="width:10%">PCN 2</td>
        <td style="width:10%"><input type="text" maxlength="100" name="pc2_pstas" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->pc2_pstas:'' ) }}" id="pc2_pstas"/></td>
        <td  style="width:12%" class="t2">TORA (m)</td>
        <td style="width:12%"  class="t2">TODA (m)</td>
        <td style="width:12%"  class="t2">ASDA (m)</td>
        <td  style="width:14%" class="t2">LDA (m)</td>
    </tr>
    <tr>
        <td style="width:10%" class="t2">PCN 3</td>
        <td style="width:10%"><input type="text" maxlength="100" name="pc3_pstas" id="pc3_pstas" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->pc3_pstas:'' ) }}"/></td>
        <td style="width:12%"><input type="text" maxlength="100" name="tora" id="tora" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tora:'' ) }}"/></td>
        <td style="width:12%"><input type="text" maxlength="100" name="toda" id="toda" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->toda:'' ) }}" /></td>
        <td style="width:12%"><input type="text" maxlength="100" name="asda" id="asda" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->asda:'' ) }}" /></td>
        <td style="width:12%"><input type="text" maxlength="100" name="lda" id="lda" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->lda:'' ) }}" /></td>
    </tr>
   
    <tr>
        <td style="width:30%" class="t2" colspan="3">1.2 Tipo de Pavimento (marque con X)</td>
        <td  class="t2" colspan="2">Demarcación (marque con x)</td>
        <td class="t2" colspan="5">Condiciones</td>
    </tr>
    <tr>
        <td style="width:10%" class="t2">Flexible</td>
        <td style="width:10%" class="t2">Rígido</td>
        <td style="width:10%" class="t2">Otro</td>
        <td style="width:10%" class="t2" >Buena: </td>
        <td style="width:10%" class="t2" ><input type="radio" name="demarcacion" id="buena_demarcacion" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->demarcacion==1 ? 'checked':''):'' ) }} value="1"/></td>
        <td style="width:25%" class="t2" colspan="2">Buena: </td>
        <td style="width:25%" class="t2" colspan="2"><input type="radio" name="condicion" id="buena_condiciones" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->condicion==1 ? 'checked':''):'' ) }} value="1"/></td>
    </tr>
    <tr>
        <td align="center" rowspan="2"><input type="checkbox"  name="flexible" id="flexible"  {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->flexible==true ? 'checked':''):'' ) }} value="1" /></td>
        <td align="center" rowspan="2"><input type="checkbox"  name="rigido" id="rigido" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->rigido==true ? 'checked':''):'' ) }} value="1" /></td>
        <td align="center" rowspan="2"><input type="checkbox"  name="otro" id="otro" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->otro==true ? 'checked':''):'' ) }} value="1" /></td>
        <td class="t2" >Regular: </td>
        <td class="t2" ><input type="radio" name="demarcacion"  id="regular_demarcacion" value="2" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->demarcacion==2 ? 'checked':''):'' ) }}/></td>
        <td class="t2" colspan="2">Regular: </td>
        <td class="t2" colspan="2"><input type="radio" name="condicion" id="regular_condiciones" value="2" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->condicion==2 ? 'checked':''):'' ) }}/></td>
    </tr>
    <tr>
        <td class="t2" >Deficiente: </td>
        <td class="t2" ><input  type="radio" name="demarcacion"  id="deficiente_demarcacion" value="3" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->demarcacion==3 ? 'checked':''):'' ) }}/></td>
        <td class="t2" colspan="2">Deficiente: </td>
        <td class="t2" colspan="2"><input type="radio" name="condicion"  id="deficiente__condiciones" value="3" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->condicion==3 ? 'checked':''):'' ) }}/></td>
    </tr>
</table>
<br/>
<table class="borde" style="width: 100%">    
    <tr>
        <th class="t1" colspan="5">2. PLATAFORMAS</th>
        <th  class="t1" colspan="4">No Conformidades</th>
    </tr>
    <tr>
        <td class="t2" colspan="5">2.1 Tipos de Plataforma</td>
        <td   colspan="3">Agrietamientos</td>
        <td align="center" ><input type="checkbox" name="agrietamiento" id="agrietamiento" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->agrietamiento==true ? 'checked':''):'' ) }} value="1"  /></td>
    </tr>
    <tr>
        <td colspan="5">Pavimento: Rígido (R)/ Flexible (f) / Otro (O)</td>
        <td colspan="3">Ahuellamiento</td>
        <td align="center" ><input type="checkbox" name="ahuellamiento"   id="ahuellamiento" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ahuellamiento==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td colspan="5">Condición: Buena(B) / Regular (R) / Deficiente (D)</td>
        <td colspan="3">Disgregación</td>
        <td align="center" ><input type="checkbox" name="disgregacion" id="disgregacion" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->disgregacion==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td  class="t2">Comercial</td>
        <td colspan="2" class="t2">General</td>
        <td colspan="2" class="t2">Carga</td>
        <td colspan="3">Contaminación Neumática o pos otros entres</td>
        <td align="center" ><input type="checkbox" name="cont_neumatica" id="cont_neumatica" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->cont_neumatica==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td  >Área (m2)1: <input type="text" maxlength="100"  name="comercial_area" id="comercial_area" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->comercial_area:'' ) }}" /> </td>
        <td  >Área (m2)2</td>
        <td  ><input type="text" maxlength="100"  name="general_area" id="general_area" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->general_area:'' ) }}" /></td>
        <td  >Área (m2)3</td>
        <td  ><input type="text" maxlength="100"  name="carga_area" id="carga_area" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->carga_area:'' ) }}" /></td>
        <td colspan="3">Grieta Piel de Cocodrilo</td>
        <td align="center" ><input type="checkbox" name="piel_cocodrilo" id="piel_cocodrilo" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->piel_cocodrilo==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td  >Puestos1: <input type="text" maxlength="100" name="comercial_puesto" id="comercial_puesto" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->comercial_puesto:'' ) }}"/></td>
        <td  >Puestos2:</td>
        <td  ><input type="text" maxlength="100" name="general_puesto" id="general_puesto" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->general_puesto:'' ) }}"/></td>
        <td  >Puestos3:</td>
        <td  ><input type="text" maxlength="100" name="carga_puesto" id="carga_puesto" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->carga_puesto:'' ) }}"/></td>
        <td colspan="3">Presencia de Vegetación</td>
        <td align="center" ><input type="checkbox" name="pres_vegetacion" id="pres_vegetacion" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->pres_vegetacion==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td  >PCN1: <input type="text" maxlength="100" name="comercial_pcn1" id="comercial_pcn1" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->comercial_pcn1:'' ) }}"/></td>
        <td  >PCN2:</td>
        <td  ><input type="text" maxlength="100" name="general_pcn2" id="general_pcn2" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->general_pcn2:'' ) }}"/></td>
        <td  >PCN3:</td>
        <td  ><input type="text" maxlength="100" name="carga_pcn3" id="carga_pcn3" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->carga_pcn3:'' ) }}" /></td>
        <td colspan="3">Detritos</td>
        <td align="center" ><input type="checkbox" name="detritos" id="detritos" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->detritos==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td  >Pavimento1: 
            <select name="comercial_pavimento" id="comercial_pavimento">
                <option value=""></option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->comercial_pavimento=='R' ? 'selected="selected"':''):'' ) }} value="R">Rígido (R)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->comercial_pavimento=='F' ? 'selected="selected"':''):'' ) }} value="F">Flexible (F)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->comercial_pavimento=='O' ? 'selected="selected"':''):'' ) }} value="O">Otro (O)</option>
            </select>
        </td>
        <td  >Pavimento2:</td>
        <td  >
            <select name="general_pavimento" id="general_pavimento">
                <option value=""></option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->general_pavimento=='R' ? 'selected="selected"':''):'' ) }} value="R">Rígido (R)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->general_pavimento=='F' ? 'selected="selected"':''):'' ) }} value="F">Flexible (F)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->general_pavimento=='O' ? 'selected="selected"':''):'' ) }} value="O">Otro (O)</option>
            </select>
        </td>
        <td  >Pavimento3:</td>
        <td >
            <select name="carga_pavimento" id="carga_pavimento">
                <option value=""></option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->carga_pavimento=='R' ? 'selected="selected"':''):'' ) }} value="R">Rígido (R)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->carga_pavimento=='F' ? 'selected="selected"':''):'' ) }} value="F">Flexible (F)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->carga_pavimento=='O' ? 'selected="selected"':''):'' ) }} value="O">Otro (O)</option>
            </select>
        </td>
        <td colspan="3">Drenaje en Mal Estado</td>
        <td align="center" ><input type="checkbox" name="drenaje" id="drenaje" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->drenaje==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <td  >Condición1:
            <select name="comercial_condicion" id="comercial_condicion">
                <option value=""></option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->comercial_condicion=='B' ? 'selected="selected"':''):'' ) }} value="B">Buena (B)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->comercial_condicion=='R' ? 'selected="selected"':''):'' ) }} value="R">Regular (R)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->comercial_condicion=='D' ? 'selected="selected"':''):'' ) }} value="D">Deficiente (D)</option>
            </select>
        </td>
        <td  >Condición2:</td>
        <td  >
            <select name="general_condicion" id="general_condicion">
                <option value=""></option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->general_condicion=='B' ? 'selected="selected"':''):'' ) }} value="B">Buena (B)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->general_condicion=='R' ? 'selected="selected"':''):'' ) }} value="R">Regular (R)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->general_condicion=='D' ? 'selected="selected"':''):'' ) }} value="D">Deficiente (D)</option>
            </select>
        </td>
        <td  >Condición3:</td>
        <td  >
            <select name="carga_condicion" id="carga_condicion">
                <option value=""></option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->carga_condicion=='B' ? 'selected="selected"':''):'' ) }} value="B">Buena (B)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->carga_condicion=='R' ? 'selected="selected"':''):'' ) }} value="R">Regular (R)</option>
                <option {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->carga_condicion=='D' ? 'selected="selected"':''):'' ) }} value="D">Deficiente (D)</option>
            </select>
        </td>
        <td colspan="3">Otras (especifique) <input value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->otras_text:'' ) }}" type="text" maxlength="100" name="otras_text" id="otras_text"/></td>
        <td align="center" ><input type="checkbox"  name="otras" id="otras" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->otras==true ? 'checked':''):'' ) }} value="1" /></td>
    </tr>
    <tr>
        <th colspan="9" class="t1"> 3. CALLES DE RODAJE</th>
    </tr>
    <tr>
        <td class="t2">A:</td>
        <td colspan="8"><input type="text" maxlength="100" name="calle_a" id="calle_a" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_a:'' ) }}" /></td>
    </tr>
    <tr>
        <td class="t2">B:</td>
        <td colspan="8"><input type="text" maxlength="100" name="calle_b" id="calle_b" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_b:'' ) }}" /></td>
    </tr>
    <tr>
        <td class="t2">C:</td>
        <td colspan="8"><input type="text" maxlength="100" name="calle_c" id="calle_c" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_c:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">D:</td>
        <td colspan="8"><input type="text" maxlength="100" name="calle_d" id="calle_d" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_d:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">E:</td>
        <td colspan="8"><input type="text" maxlength="100" name="calle_e" id="calle_e" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_e:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">F:</td>
        <td colspan="8"><input type="text" maxlength="100" name="calle_f" id="calle_f" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_f:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">G:</td>
        <td colspan="8"><input type="text" name="calle_g" id="calle_g" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_g:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">H:</td>
        <td colspan="8"><input type="text" name="calle_h" id="calle_h" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_h:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">I:</td>
        <td colspan="8"><input type="text" name="calle_i" id="calle_i" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_i:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">J:</td>
        <td colspan="8"><input type="text" name="calle_j" id="calle_j" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_j:'' ) }}"/></td>
    </tr>
    <tr>
        <td class="t2">K:</td>
        <td colspan="8"><input type="text" name="calle_k" id="calle_k" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->calle_k:'' ) }}"/></td>
    </tr>
    <tr>
        <th colspan="9" class="t1"> 4. SISTEMA DE BALIZAJE</th>
    </tr>
    <tr>
        <td class="t2" colspan="2">Tipo:</td>
        <td class="t2" >Operativa</td>
        <td class="t2" >Inoperativa</td>
        <td class="t2" >No Aplica</td>
        <td class="t2" colspan="1">Tipo:</td>
        <td class="t2" >Operativa</td>
        <td class="t2" >Inoperativa</td>
        <td class="t2" >No Aplica</td>
    </tr>
    <tr>
        <td colspan="2">Luces de borde de pista:</td>
        <td align="center"><input type="radio" name="lbp" id="lbp_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lbp===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="lbp" id="lbp_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lbp===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="lbp" id="lbp_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lbp===null ? 'checked':''):'' ) }} value="null"/></td>
        <td colspan="1">Luces de Calle de Rodaje:</td>
        <td align="center"><input type="radio" name="lcr" id="lcr_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lcr===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="lcr" id="lcr_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lcr===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="lcr" id="lcr_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lcr===null ? 'checked':''):'' ) }} value="null"/></td>
        
    </tr>
    <tr>
        <td colspan="2">Luces de eje de pista:</td>
        <td align="center"><input type="radio" name="lep" id="lep_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lep==true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="lep" id="lep_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lep===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="lep" id="lep_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lep===null ? 'checked':''):'' ) }} value="null"/></td>

        <td colspan="1">Luces de Plataforma:</td>
        <td align="center"><input type="radio" name="lp" id="lp_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lp===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="lp" id="lp_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lp===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="lp" id="lp_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lp===null ? 'checked':''):'' ) }} value="null"/></td>

    </tr>
    <tr>
        <td colspan="2">PAPI/APAPI:</td>
        <td align="center"><input type="radio" name="papi" id="papi_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->papi===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="papi" id="papi_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->papi===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="papi" id="papi_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->papi===null ? 'checked':''):'' ) }} value="null"/></td>

        <td colspan="1">Luces de Parada:</td>
        <td align="center"><input type="radio" name="lp2" id="lp2_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lp2===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="lp2" id="lp2_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lp2===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="lp2" id="lp2_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->lp2===null ? 'checked':''):'' ) }} value="null"/></td>

    </tr>
    <tr>
        <td colspan="3">Observaciones del Sistema de Balizaje</td>
        <td colspan="7"><input type="text" maxlength="100" name="obs_balizaje" id="obs_balizaje" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->obs_balizaje:'' ) }}" /></td>
    </tr>
    <tr>
        <th colspan="7" class="t1"> 5. INSTALACIONES (INFRAESTRUCTURA) DEL SERVICIO  A LA NAVEGACIÓN AÉREA</th>
        <th colspan="2" class="t1"> Condiciones</th>
    </tr>
    <tr>
        <td class="t2"  colspan="6">
            Radioayudas
        </td>
        <td class="t2">Operativas</td>
        <td class="t2">Inoperativas</td>
        <td class="t2">No Aplica</td>
    </tr>    
    <tr>
        <td  colspan="6">
            VOR
        </td>
        <td align="center"><input type="radio" name="vor"  id="na_vor_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->vor===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="vor"  id="na_vor_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->vor===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="vor"  id="na_vor_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->vor===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>    
    <tr>
        <td  colspan="6">
            DME
        </td>
        <td align="center"><input type="radio" name="dme" id="na_dme_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->dme===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="dme" id="na_dme_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->dme===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="dme" id="na_dme_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->dme===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>    
    <tr>
        <td  colspan="6">
            ILS
        </td>
        <td align="center"><input type="radio" name="ils" id="na_ils_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ils===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="ils" id="na_ils_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ils===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="ils" id="na_ils_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ils===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>    
    <tr>
        <td  colspan="6">
            NDB
        </td>
        <td align="center"><input type="radio" name="ndb" id="na_ndb_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ndb===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="ndb" id="na_ndb_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ndb===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="ndb" id="na_ndb_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ndb===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>
        <td  colspan="6">
            OTRAS (Especifique)
            <input type="text" maxlength="100" name="na_otras_esp" id="na_otras_esp" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->na_otras_esp:'' ) }}" />
        </td>
        <td align="center"><input type="radio" name="radio_o" id="na_radio_o_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->radio_o===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center"><input type="radio" name="radio_o" id="na_radio_o_ino" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->radio_o===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center"><input type="radio" name="radio_o" id="na_radio_o_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->radio_o===null ? 'checked':''):'' ) }} value="null"/></td>
        
    </tr>
    <tr>
        <th colspan="9" class="t1"> 6. SERVICIO DE COMBUSTIBLE DE AVIACIÓN</th>
    </tr>
    <tr>
        <td class="t2" colspan="7">TIPO</td>
        <td class="t2">Capacidad (Lts)</td>
        <td class="t2">Existencia (Lts)</td>
    </tr>
    <tr>
        <td  colspan="7">AVGAS</td>
        <td><input type="text" maxlength="100" name="ser_comb_avgas_cap" id="ser_comb_avgas_cap" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->ser_comb_avgas_cap:'' ) }}" /></td>
        <td><input type="text" maxlength="100" name="ser_comb_avgas_exis" id="ser_comb_avgas_exis" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->ser_comb_avgas_exis:'' ) }}" /></td>
    </tr>
    <tr>
        <td  colspan="7">JETA 1</td>
        <td><input type="text" maxlength="100" name="ser_comb_jeta_cap" id="ser_comb_jeta_cap" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->ser_comb_jeta_cap:'' ) }}" /></td>
        <td><input type="text" maxlength="100" name="ser_comb_jeta_exis" id="ser_comb_jeta_exis" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->ser_comb_jeta_exis:'' ) }}"   /></td>
    </tr>
    <tr>
        <td  colspan="7">Otro (Especifique): <input type="text" maxlength="100" name="sev_comb_otro_text" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->sev_comb_otro_text:'' ) }}" /></td>
        <td><input type="text" maxlength="100" name="serv_comb_otro_cap" id="serv_comb_otro_cap" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_otro_cap:'' ) }}" /></td>
        <td><input type="text" maxlength="100" name="serv_comb_otro_exis" id="serv_comb_otro_exis" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_otro_exis:'' ) }}" /></td>
    </tr>
    <tr>
        <td class="t2" colspan="7">&nbsp;</td>
        <td class="t2">Operativos</td>
        <td class="t2">Inoperativos</td>
    </tr>
    <tr>
        <td colspan="7" >Camión Cisterna</td>
        <td><input type="text" maxlength="100" name="serv_comb_cs_op" id="serv_comb_cs_op" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_cs_op:'' ) }}"/></td>
        <td><input type="text" maxlength="100" name="serv_comb_cs_ino" id="serv_comb_cs_ino" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_cs_ino:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="7" >Hidrantes</td>
        <td><input type="text" maxlength="100" name="serv_comb_hdtt_op" id="serv_comb_hdtt_op" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_hdtt_op:'' ) }}"/></td>
        <td><input type="text" maxlength="100" name="serv_comb_hdtt_ino" id="serv_comb_hdtt_ino" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_hdtt_ino:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Observaciones</td>
        <td colspan="7"><input type="text" maxlength="100" name="serv_comb_obs" id="serv_comb_obs" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->serv_comb_obs:'' ) }}"/></td>
    </tr>
    <tr>
        <th colspan="9" class="t1"> 7. EMPRESAS DE SERVICIOS DE APOYO EN TIERRA</th>
    </tr>
    <tr>
        <td>Empresa</td>
        <td colspan="9"><input type="text" maxlength="100" name="emp_serv_empre" id="emp_serv_empre" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->emp_serv_empre:'' ) }}"/></td>

    </tr>

    <tr>
        <th colspan="9" class="t1"> 8. CERCA PERIMETRAL</th>
    </tr>
    <tr>
        <td colspan="2">Tipo de Material</td>
        <td colspan="7"><input type="text" maxlength="100" name="cerc_per_tip_mtl" id="cerc_per_tip_mtl" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->cerc_per_tip_mtl:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Longitud Total (m)</td>
        <td colspan="7"><input type="text" maxlength="100" name="cerc_per_long_ttl" id="cerc_per_long_ttl" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->cerc_per_long_ttl:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Longitud Actual (m)</td>
        <td colspan="7"><input type="text" maxlength="100" name="cerc_per_long_atl" id="cerc_per_long_atl" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->cerc_per_long_atl:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Observaciones</td>
        <td colspan="7"><input type="text" maxlength="100" name="cerc_per_obs" id="cerc_per_obs" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->cerc_per_obs:'' ) }}"/></td>
    </tr>
    <tr>
        <th colspan="9" class="t1"> 9. TERMINAL DE PASAJEROS</th>
    </tr>
    <tr>
        <td colspan="2">Sala Lobby</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_lbby" id="tml_pax_lbby" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_lbby:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Sala de Chequeo</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_cheq" id="tml_pax_cheq" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_cheq:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Sala de Embarque</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_emb" id="tml_pax_emb" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_emb:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Sala de Desembarque</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_desm" id="tml_pax_desm" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_desm:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Sistema de Iluminación</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_illum" name="tml_pax_illum" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_illum:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Sala Sanitarias</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_sani" id="tml_pax_sani" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_sani:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Estacionamiento</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_estaci" id="tml_pax_estaci" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_estaci:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Tanques de Agua Potable</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_pot" id="tml_pax_pot" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_pot:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Planta de Tratamiento</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_trat" id="tml_pax_trat" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_trat:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Sistema de Climatización</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_clima" id="tml_pax_clima" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_clima:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Áreas verdes</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_area_verde" id="tml_pax_area_verde" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_area_verde:'' ) }}"/></td>
    </tr>
    <tr>
        <td colspan="2">Servicio de Transporte</td>
        <td colspan="7"><input type="text" maxlength="100" name="tml_pax_serv_trans" id="tml_pax_serv_trans" value="{{ ( isset($InfoOperaciones)  ? $InfoOperaciones->tml_pax_serv_trans:'' ) }}"/></td>
    </tr>
    <tr>
        <th colspan="9" class="t1"> 10. ORGANISMOS INVOLUCRADOS EN LAS OPERACIONES AÉREAS</th>
    </tr>
    <tr>
        <td class="t2" colspan="6">Organismo / Área</td>
        <td  class="t2" >Posee</td>
        <td class="t2" >No Posee</td>
        <td class="t2" >No Aplica</td>
    </tr>
    <tr>

        <td colspan="6">Servicio de Seguridad Aeropuertuaria</td>
        <td align="center" ><input type="radio" name="ssa" id="rrhh_ssa_po" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ssa===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio" name="ssa" id="rrhh_ssa_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ssa===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio" name="ssa" id="rrhh_ssa_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ssa===null ? 'checked':''):'' ) }} value="null"/></td>

    </tr>
    <tr>
        <td colspan="6">SAIME</td>
        <td align="center" ><input type="radio"  name="saime" id="rrhh_saime_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->saime===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="saime"  id="rrhh_saime_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->saime===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="saime"  id="rrhh_saime_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->saime===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>
        <td colspan="6">Vigilancia Epidemiológica</td>
        <td align="center" ><input type="radio"  name="ve" id="rrhh_ve_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ve===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="ve" id="rrhh_ve_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ve===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="ve" id="rrhh_ve_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->ve===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>

        <td colspan="6">Servicio de Tránsito Aereo</td>
        <td align="center" ><input type="radio"  name="sta" id="rrhh_sta_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sta===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="sta" id="rrhh_sta_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sta===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="sta" id="rrhh_sta_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sta===null ? 'checked':''):'' ) }} value="null"/></td>

    </tr>
    <tr>
        <td colspan="6">Servicio de Aduana</td>
        <td align="center" ><input type="radio"  name="sa" id="rrhh_sa_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sa===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="sa" id="rrhh_sa_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sa===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="sa" id="rrhh_sa_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sa===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>

        <td colspan="6">Vigilancia de Metereología</td>
        <td align="center" ><input type="radio"  name="vm" id="rrhh_vm_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->vm===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="vm" id="rrhh_vm_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->vm===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="vm" id="rrhh_vm_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->vm===null ? 'checked':''):'' ) }} value="null"/></td>

    </tr>
    <tr>
        <td colspan="6">Oficina AIS</td>
        <td align="center" ><input type="radio"  name="oais" id="rrhh_oais_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->oais===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="oais" id="rrhh_oais_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->oais===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="oais" id="rrhh_oais_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->oais===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>
        <td colspan="6">INSAI</td>
        <td align="center" ><input type="radio"  name="insai" id="rrhh_insai_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->insai===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="insai" id="rrhh_insai_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->insai===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="insai" id="rrhh_insai_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->insai===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>
        <td colspan="6">Servicio Policial Estadal</td>
        <td align="center" ><input type="radio"  name="spe" id="rrhh_spe_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->spe===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="spe" id="rrhh_spe_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->spe===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="spe" id="rrhh_spe_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->spe===null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>
        <td colspan="6">Servicio de Combustible</td>
        <td align="center" ><input type="radio"  name="sc" id="rrhh_sc_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sc===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="sc" id="rrhh_sc_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sc===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="sc" id="rrhh_sc_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sc==null ? 'checked':''):'' ) }} value="null"/></td>
    </tr>
    <tr>

        <td colspan="6">Servicio de Desechos Solidos</td>
        <td align="center" ><input type="radio"  name="sds" id="rrhh_sds_op" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sds===true ? 'checked':''):'' ) }} value="1"/></td>
        <td align="center" ><input type="radio"  name="sds" id="rrhh_sds_np" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sds===false ? 'checked':''):'' ) }} value="0"/></td>
        <td align="center" ><input type="radio"  name="sds" id="rrhh_sds_noa" {{ ( $InfoOperaciones  != null ? ($InfoOperaciones->sds===null ? 'checked':''):'' ) }} value="null"/></td>

    </tr>

    <tr>

        <td colspan="9" align="center">{{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary no_pdf", "id"=>"save"])}}</td>
    </tr>

</table>



{{ Form::close() }}