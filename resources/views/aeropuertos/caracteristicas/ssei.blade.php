@if (isset($pdf))

<style>
    table.borde, table.borde th, table.borde tr td {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 0;
        margin: 0;
        border-spacing: 0;
    }
    .t1{
        background-color:grey;
        text-align:center;
        font-size: 14px;
    }
    .t2{
        background-color: #dbd9d9;
        text-align:center;
        font-size: 12px;
    }

    table.sn_border tr td{
        border:none;
    }
    tr:hover td{
        background-color: #75bff2;
    }
    input[type=text]{





        width: auto !important;


        font-size: 12px;

        margin-left: 2.5%;
        margin-bottom: 5px;
        margin-top: 5px;
        border: none;
    }

    input[type="checkbox"], input[type="radio"]{
        width:20px;
        height:20px;
    }




    .no_pdf {
        display: none;


    }
</style>
<table class="borde" style="width: 100%">
    <tr>
        <td class="t1" colspan="2"><img style="width:100%" src="{{url('dist/img/logo.jpeg')}}"/></td>
    </tr>
    <tr>
        <td class="t1" colspan="2">CERTIFICACIÓN</td>
    </tr>
    <tr>
        <td class="t2" width="200px">Nombre del Aeropuerto</td>
        <td>{{$Aeropuerto1->nombre}}</td>
    </tr>
    <tr>
        <td class="t2">Designador OACI</td>
        <td>{{$Aeropuerto1->codigo_oaci}}</td>
    </tr>
    <tr>
        <td class="t2">Ubicación</td>
        <td></td>
    </tr>
    <tr>
        <td class="t2">Horario Operacional</td>
        <td></td>
    </tr>
</table>
<br/>
@endif

{{Form::open(["route"=>"ver_mapa",  "target"=>"_blanck" ,'autocomplete'=>'Off'])}}
{{Form::hidden("form", 'bomberos')}}
{{ method_field('PUT') }}
<button type="submit" class="btn btn-md no_pdf">
    <i class="fas fa-file-pdf"></i> Descargar
</button>




   
{{ Form::close() }}


{{Form::open(["route"=>"ver_mapa",  'id'=>'frmBom','autocomplete'=>'Off'])}}
{{Form::hidden("form", 'bomberos')}}

{{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary no_pdf float-right ", "id"=>"save"])}}

<table class="borde" style="width: 100%">
    <tr>
        <th class="t1" colspan="2">BOMBEROS AERONAUTICOS</th>
        <th  class="t2" colspan="7">VEHICULOS DE EMERGENCIAS</th>
    </tr>
    <tr>
        <td class="t1" >ENTE</td>
        <td class="t1">CANT</td>
        <td class="t2" colspan="2">VEHICULOS CONTRA INCENDIOS</td>
        <td class="t2" colspan="5">AMBULANCIA</td>
    </tr>
    <tr>
        <td class="t2">BAER</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->baer_cant:'' ) }}" name="baer_cant" id="baer_cant" /></td>


        <td class="t2">MARCA AÑO Y MODELO</td>
        <td class="t2">OBSERVACIONES</td>
        <td class="t2" colspan="2">MARCA AÑO Y MODELO</td>
        <td class="t2" colspan="3">OBSERVACIONES</td>
    </tr>
    <tr>

        <td class="t2">OTROS ENTES</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->otros_cant:'' ) }}" name="otros_cant" id="otros_cant" /></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam1_vci:'' ) }}" name="mam1_vci" id="mam1_vci"/></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs1_vci:'' ) }}" name="mam_obs1_vci" id="mam_obs1_vci"/></td>
        <td colspan="2"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam1_ambu:'' ) }}" name="mam1_ambu" id="mam1_ambu"/></td>
        <td colspan="3"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs1_ambu:'' ) }}" name="mam_obs1_ambu" id="mam_obs1_ambu"/></td>
    </tr>
    <tr>

        <td class="t2">REQUIERIDOS</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->requi_cant:'' ) }}" name="requi_cant" id="requi_cant" /></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam2_vci:'' ) }}" name="mam2_vci" id="mam2_vci"/></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs2_vci:'' ) }}" name="mam_obs2_vci" id="mam_obs2_vci"/></td>
        <td colspan="2"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam2_ambu:'' ) }}" name="mam2_ambu" id="mam2_ambu"/></td>
        <td colspan="3"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs2_ambu:'' ) }}" name="mam_obs2_ambu" id="mam_obs2_ambu"/></td>
    </tr>
    <tr>
        <td class="t2" colspan="2">APROVISIONAMIENTO DE AGUA</td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam3_vci:'' ) }}" name="mam3_vci" id="mam3_vci"/></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs3_vci:'' ) }}" name="mam_obs3_vci" id="mam_obs3_vci"/></td>
        <td colspan="2"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam3_ambu:'' ) }}" name="mam3_ambu" id="mam3_ambu"/></td>
        <td colspan="3"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs3_ambu:'' ) }}" name="mam_obs3_ambu" id="mam_obs3_ambu"/></td>
    </tr>
    <tr>

        <td class="t2">BOMBA</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->calle_a:'' ) }}" name="bomba_a" id="bomba_a" /></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam4_vci:'' ) }}" name="mam4_vci" id="mam4_vci"/></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs4_vci:'' ) }}" name="mam_obs4_vci" id="mam_obs4_vci"/></td>
        <td colspan="2"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam4_ambu:'' ) }}" name="mam4_ambu" id="mam4_ambu"/></td>
        <td colspan="3"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs4_ambu:'' ) }}" name="mam_obs4_ambu" id="mam_obs4_ambu"/></td>
    </tr>
    <tr>

        <td class="t2">HIDRONEUMATICO</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->calle_a:'' ) }}" name="hidro_a" id="hidro_a" /></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam5_vci:'' ) }}" name="mam5_vci" id="mam5_vci"/></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs5_vci:'' ) }}" name="mam_obs5_vci" id="mam_obs5_vci"/></td>
        <td colspan="2"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam5_ambu:'' ) }}" name="mam5_ambu" id="mam5_ambu"/></td>
        <td colspan="3"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs5_ambu:'' ) }}" name="mam_obs5_ambu" id="mam_obs5_ambu"/></td>
    </tr>
    <tr>

        <td class="t2">TANQUE</td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->calle_a:'' ) }}" name="tanqu_a" id="tanqu_a" /></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam6_vci:'' ) }}" name="mam6_vci" id="mam6_vci"/></td>
        <td><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs6_vci:'' ) }}" name="mam_obs6_vci" id="mam_obs6_vci"/></td>
        <td colspan="2"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam6_ambu:'' ) }}" name="mam6_ambu" id="mam6_ambu"/></td>
        <td colspan="3"><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->mam_obs6_ambu:'' ) }}" name="mam_obs6_ambu" id="mam_obs6_ambu"/></td>
    </tr>
    <tr>
        <th class="t2" colspan="5">AGENTE EXTINTOR</th>
        <th  class="t2" colspan="2">EQUIPO DE PROTECCION PERSONAL</th>
        <th  class="t2" >DISPONIBLE</th>
        <th  class="t2" >REQUERIDO</th>
    </tr>
    <tr>
        <th class="t2" colspan="3">CONCENTRADO ESPUMANTE</th>
        <th class="t2" colspan="2">POLVO QUIMICO SECO</th>
        <td class="t2" colspan="2"><b>TRAJES ALUMINIZADOS</b> <br>(CASCO, PROTECTOR FACIAL, GUANTES, CHAQUETA Y OANTALON)</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->traalu_dis:'' ) }}" name="traalu_dis" id="traalu_dis"/></td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->traalu_req:'' ) }}" name="traalu_req" id="traalu_req"/></td>
    </tr>
    <tr>
        <td  align="center" >A: <input type="checkbox" {{ ( $InfoBomberos  != null ? ($InfoBomberos->con_a==1? 'checked':''):'' ) }} name="con_a" id="con_a" /></td>
        <td  align="center" >B: <input type="checkbox" {{ ( $InfoBomberos  != null ? ($InfoBomberos->con_b==1? 'checked':''):'' ) }} name="con_b" id="con_b" /></td>
        <td  align="center" >C: <input type="checkbox" {{ ( $InfoBomberos  != null ? ($InfoBomberos->con_c==1? 'checked':''):'' ) }} name="con_c" id="con_c" /></td>
        <td class="t2">KG</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->pqseco:'' ) }}" name="pqseco" id="pqseco" /></td>
        <td class="t2" colspan="2"><b>TRAJES ESTRUCTURAL</b> <br>(CASCO, PROTECTOR FACIAL, GUANTES, CHAQUETA Y OANTALON)</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->traest_dis:'' ) }}" name="traest_dis" id="traest_dis"/></td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->traest_req:'' ) }}" name="traest_req" id="traest_req"/></td>
    </tr>
    <tr>
        <td class="t2" colspan="5"></td>
        <td class="t2" colspan="2"><b>EQUIPO DE PROTECCION RESPIRATORIA</b><br>(CILINDRO, ARNES, MASCARA Y ALARMA AUDITIVA)</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->eqprocres_dis:'' ) }}" name="eqprocres_dis" id="eqprocres_dis"/></td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->eqprocres_req:'' ) }}" name="eqprocres_req" id="eqprocres_req"/></td>
    </tr>
    <tr>
        <th class="t2" colspan="2">EQUIPO DE PROTECCION PERSONAL</th>
        <td class="t2" >SISTEMA DE LLENADO DE CILINDRO </td>
        <td class="t2">OPERATIVO</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->sisci_op:'' ) }}" name="sisci_op" id="sisci_op" /></td>
        <td class="t2">INOPERATIVO</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->sisci_inop:'' ) }}" name="sisci_inop" id="sisci_inop" /></td>
        <td class="t2">NOPOSEE</td>
        <td ><input type="text" value="{{ ( $InfoBomberos  != null ? $InfoBomberos->sisci_nopose:'' ) }}" name="sisci_nopose" id="sisci_nopose" /></td>
    </tr>
    <tr>
        <th class="t2" colspan="11">HERRAMIENTAS DE RESCATE</th>
    </tr>
    <tr>
        <th class="t2" colspan="3">DESCRIPCION</th>
        <th class="t2" colspan="2">CONDICION</th>
        <th class="t2" colspan="2">DESCRIPCION</th>
        <th class="t2" colspan="2">CONDICION</th>
    </tr>
    <tr>
        <td colspan="3"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc1:'' ) }}" type="text" name="herrdesc1" id="herrdesc1"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond1:'' ) }}" type="text" name="herrcond1" id="herrcond1"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc6:'' ) }}" type="text" name="herrdesc6" id="herrdesc6"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond6:'' ) }}" type="text" name="herrcond6" id="herrcond6"/></td>
    </tr>
    <tr>
        <td colspan="3"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc2:'' ) }}" type="text" name="herrdesc2" id="herrdesc2"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond2:'' ) }}" type="text" name="herrcond2" id="herrcond2"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc7:'' ) }}" type="text" name="herrdesc7" id="herrdesc7"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond7:'' ) }}" type="text" name="herrcond7" id="herrcond7"/></td>
    </tr>
    <tr>
        <td colspan="3"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc3:'' ) }}" type="text" name="herrdesc3" id="herrdesc3"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond3:'' ) }}" type="text" name="herrcond3" id="herrcond3"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc8:'' ) }}" type="text" name="herrdesc8" id="herrdesc8"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond8:'' ) }}" type="text" name="herrcond8" id="herrcond8"/></td>
    </tr>
    <tr>
        <td colspan="3"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc4:'' ) }}" type="text" name="herrdesc4" id="herrdesc4"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond4:'' ) }}" type="text" name="herrcond4" id="herrcond4"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc9:'' ) }}" type="text" name="herrdesc9" id="herrdesc9"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond9:'' ) }}" type="text" name="herrcond9" id="herrcond9"/></td>
    </tr>
    <tr>
        <td colspan="3"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc5:'' ) }}" type="text" name="herrdesc5" id="herrdesc5"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond5:'' ) }}" type="text" name="herrcond5" id="herrcond5"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrdesc10:'' ) }}" type="text" name="herrdesc10" id="herrdesc10"/></td>
        <td colspan="2"><input value="{{ ( $InfoBomberos  != null ? $InfoBomberos->herrcond10:'' ) }}" type="text" name="herrcond10" id="herrcond10"/></td>
    </tr>
    <tr>

        <td colspan="9" align="center">{{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary no_pdf", "id"=>"save"])}}</td>
    </tr>

</table>
{{ Form::close() }}

