

<div id="panel_princ" class="col-sm-12 col-md-5 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Asignar aerolineas al aeropuerto')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('aeropuertos')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        {{Form::open(["route"=>["aeropuertos.airlines", $aereopuerto->crypt_id],  'id'=>'frmAddAirlines','autocomplete'=>'Off'])}}
            <!-- /.card-header -->
            <div id="card-body-main" class="card-body ">
                <div class="col-md-12">
                    <div class="input-group mt-2"  >
                        <div class="input-group-prepend">
                            <span id="labelAerolinea" class="input-group-text"><b>{{__('Aeropuerto')}}</b></span>
                        </div>
                        {{Form::text("aeropuerto", $aereopuerto->full_nombre, ["disabled"=>"disabled",  "required"=>"required", "class"=>"form-control required", "id"=>"aeropuerto", "placeholder"=>__('Nombre')])}}
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="input-group mt-2"  >
                        <div class="input-group-prepend">
                            <span id="labelAerolinea" class="input-group-text"><b>{{__('Aerolineas')}}</b></span>
                        </div>
                        {{Form::select('aerolineas[]', $Aerolineas, $AerolineaSelected, ["data-msg-required"=>"Campo Requerido", "style"=>"heigth: 200px;", "class"=>"form-control required  select2 ", "id"=>"aerolineas" ,"required"=>"required", 'multiple' => true])}}
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="card-footer text-center">
                {{Form::button('<la class="fa fa-save"></li> '.__("Guardar"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    

            </div>
            <!-- /.card -->
        {{ Form::close() }} 
    </div>
<script type="text/javascript">

    $(document).ready(function () {
        $("#frmAddAirlines").validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            },
            submitHandler: function (form) {
                $("#panel_princ").prepend(LOADING);
                $.ajax({
                    url: form.action,
                    type: "post",
                    dataType: "json",
                    data: $('#frmAddAirlines').serialize(),
                    success: function (response) {
                        $(".overlay-wrapper").remove();                        
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });
                return false;
            }
        });

    });
    
    

</script>
</div>