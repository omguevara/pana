
<img style="width:100%; " src="{{url('/')}}/dist/img/cintillo_pdf.png" />
<br/>
<br/>
<table width="100%" border="0">
    <tr>
        <th colspan="2" scope="col"><h2>APROBACIÓN DE SOLICITUD DE VUELO</h2></th>
    </tr>
    <tr>
        <th colspan="2"  scope="col">(VE-{{showDate($Registros->fecha, 'flat')}}-{{showCode($Registros->id, 4)}})</th>
    </tr>

    <tr>
        <td style="width:80%; text-align: center"  scope="col">
            Nos es grato confirmar que su solicitud de vuelo 
            N° (VE-{{showDate($Registros->fecha, 'flat')}}-{{showCode($Registros->id, 4)}}) a sido aprobada como fue solicitada
            para el día (AAAA-MM-DD) con hora de salida (00:00,am) en la aeronave tipo (AVIONETA)  
            siglas (yv000t), con ({{$Registros->cantidad_pasajeros}}) pasajeros  y carga de ({{$Registros->carga}} kg)...
            Código de aprobación.</td>
        <td scope="col">
            
            <img style="margin:auto; width:100px" src="data:image/png;base64, {{ base64_encode(QrCode::size(100)->generate("BAER")) }} ">
            
        </td>
    </tr>

    <tr>
        <th colspan="2" scope="col">(VE-{{showDate($Registros->fecha, 'flat')}}-{{showCode($Registros->id, 4)}})</th>
    </tr>
</table>

<div style="width: 100%; text-align: center;">
    <strong>Construyendo el Socialismo Aeroportuario.</strong>
</div>
