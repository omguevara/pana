<h1>Registro en el Sistema PANA</h1>
Estimado ciudadano <b>{{$full_nombre}}</b>, el siguiente correo es para darle entrega de sus credenciales en la 
Plataforma Administrativa Nacional Aeroportuaria (PANA), a continuación le mostramos el usuario, la contraseña
y el vinculo para acceder:
<br/>
<br/>
Perfil: {{$profile['name_profile']}}<br/>
Usuario: {{$username}}<br/>
Clave: {{$pwd}}<br/>
Enlace: <a href="{{route('login')}}" target="_blank">Plataforma Administrativa Nacional Aeroportuaria (P.A.N.A.)</a><br/>




