

Bolivariana de Aeropuertos BAER.<br><br>

Estimado(a) cliente: {{ $cliente['razon_social'] }}.<br><br>

El siguiente correo es para hacer entrega del resumen de proformas correspondiente a los vuelos efectuados entre las siguientes fechas: {{ $rango_fecha }}.<br><br><br>


{{Auth::user()->full_nombre}}<br>
Analista de Recaudación y Facturación<br>




Visite nuestra página web <a href="http://www.baer.gob.ve">http://www.baer.gob.ve</a> <br><br><br><br>


Gracias por utilizar Nuestros Servicios.<br><br><br>


<b>Esta es una cuenta de correo electrónico no monitoreada, no responda o reenvíe mensajes a esta cuenta.</b>