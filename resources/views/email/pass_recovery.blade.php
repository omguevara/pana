<h1>Recuperación de Contraseña (Sistema PANA)</h1>
Estimado ciudadano <b>{{$full_nombre}}</b>, Usted ha solicitado cambiar de contraseña, por favor haga click en el siguiente enlace para proceder a recuperar su clave:
<br/>
<br/>

Enlace: <a href="{{route('change_password', $crypt_id)}}" target="_blank">Recuperación de Contraseña en la  Plataforma Administrativa Nacional Aeroportuaria (P.A.N.A.)</a><br/>

Si usted no ha solicitado cambio de clave por favor ignore este correo




