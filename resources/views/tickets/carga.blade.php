<div id="panel_princ" class="col-sm-12  mt-1">



    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Cargar Cupones')}}  </h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->




        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-md-5 border-right">
                    {{Form::open(["route"=>"carga_tickets",'class'=>'form-horizontal',   'files'=>'true', 'id'=>'frmPrinc1','autocomplete'=>'Off'])}}


                    <div class="row">
                        <div  class="col-md-12 ">
                            <div class="input-group mt-2"  >
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeropuerto:</b></span>
                                </div>
                                {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=> 'Seleccione',   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 " >
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span  class="input-group-text"><b>Tiempo :</b></span>
                                </div>
                                {{Form::select('tiempo_id', $Tiempos, "" , ["placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",  "class"=>"form-control required   ", "id"=>"tiempo_id" ,"required"=>"required"], $properties)}}
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Precio')}} <span title="" class="badge bg-primary">&#36;</span>:</b></span>
                                </div>
                                {{Form::text("precio", "", ["required"=>"required", "readonly" => true, "maxlength"=>"8", "class"=>"form-control required money text-right ", "id"=>"precio", "placeholder"=>__('Precio')])}}    
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 mt-2">
                            <div class="form-group">

                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" accept="text/plain" onchange="loadFile(event)" class="custom-file-input required" id="archivo" name="archivo" />
                                        <label class="custom-file-label" for="exampleInputFile">Subir Documento</label>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 mt-2">
                            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"button", "class"=>"btn btn-block btn-primary", "id"=>"save"])}}
                        </div>
                    </div>



                    {{ Form::close() }} 
                </div>
                <div class="col-md-7 border-right">
                    <textarea style="border:none; resize: none; width: 50%; height: 25vh" disabled="" name="txt" id="txt"></textarea>
                </div>
            </div>



            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->

    </div>





    <script type="text/javascript">


        $(document).ready(function () {
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $(".select2").select2();

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $("#save").on("click", function () {
                if ($("#frmPrinc1").valid()) {
                    $("#frmPrinc1").prepend(LOADING);
                    var formData = new FormData(document.getElementById("frmPrinc1"));


                    $.ajax({
                        url: $("#frmPrinc1").attr("action"),
                        type: "post",
                        dataType: "json",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                            if (response.status == 1) {

                                $("#frmPrinc1").trigger("reset");
                                $("#aeropuerto_id").trigger("change");
                                $("#txt").val("");
                            }
                        },
                        error: function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar la Información')}}"
                            });

                        }
                    });
                }
            });

            $("#tiempo_id").on("change", function(){
                $("#precio").val(muestraFloat($(this).find(":selected").data("valor")));
            });
        });
        function loadFile(event) {
            var fr = new FileReader();
            fr.onload = function () {
                txt.value = fr.result;

            };
            fr.readAsText(event.target.files[0]);
        }

    </script>

</div>
