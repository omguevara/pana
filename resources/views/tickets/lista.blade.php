<div id="panel_princ" class="col-sm-12  mt-1">



    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Cupones')}}  </h3>

            <div class="card-tools">

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->




        <div id="card-body-main" class="card-body ">
            <div class="row">

                <table id="example1" class="display compact table-hover">
                    <thead>
                        <tr>
                            <th  text-align: center">{{__('EMISIÓN')}}</th>
                            <th >{{__('AEROPUERTO')}}</th>
                            <th >{{__('CÓDIGO')}}</th>
                            <th >{{__('TIEMPO')}}</th>
                            <th >{{__('PRECIO')}}</th>
                            <th >{{__('ESTADO')}}</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>



            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->

    </div>





    <script type="text/javascript">


        $(document).ready(function () {

            table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                },

                serverSide: true,
                processing: true,

                ajax: {
                    url: "{{route('lista_tickets')}}",
                    type: 'POST',
                    data: function (d) {
                        d._token = '{{csrf_token()}}'
                        
                    }

                },

                columns: [
                    {data: 'fecha_registro2'},
                    {data: 'oaci'},
                    {data: 'codigo'},
                    {data: 'tiempo'},
                    {data: 'precio2'},
                    {data: 'estado'},
                ],

            });


        });
        function loadFile(event) {
            var fr = new FileReader();
            fr.onload = function () {
                txt.value = fr.result;

            };
            fr.readAsText(event.target.files[0]);
        }

    </script>

</div>
