<div id="panel_princ" class="col-sm-12  mt-1">
<style type="text/css">
.info{
    font-family:Arial, Helvetica, sans-serif; 
    font-size:13px;
    border: 1px solid;
    margin: 10px 0px;
    padding:15px 10px 15px 50px;
    background-repeat: no-repeat;
    background-position: 10px center;

    color: #00529B;
    background-color: #BDE5F8;
    
    background-image: url('dist/img/info.png');
    
}
</style>


@if (isset($error) && $error['type'] == 'error')
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Venta Tickets')}}  </h3>

            <div class="card-tools">
                <button type="button" id="full-screen" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i>
                </button>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <!-- /.card -->
    </div>
    <div class="alert alert-danger" role="alert">
        {{ $error['message'] }}
    </div>
@else
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Venta De Cupones')}}  </h3>



            <div class="card-tools">
                <button type="button" id="full-screen" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i>
                </button>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-md-2 border-right">
                    <h2 class="border-bottom">AYUDA</h2>

                    <div id="htmlHelp" class="row">
                        
                    </div>

                </div>
                <div class="col-md-3 border-right" style="min-height: calc(100vh - 250px)">
                    <h2 class="border-bottom">Datos del Cupón</h2>


                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span  class="input-group-text"><b>Indique el Tiempo :</b></span>
                        </div>
                        {{Form::text("tiempo_id", "", ['onFocus'=>'showHelp(1)', "required"=>"required", "maxlength"=>"3", "class"=>"form-control required text-right ", "id"=>"tiempo_id", "placeholder"=>__('0')])}}    

                    </div>

                    <div class="input-group mt-2">
                        <div class="input-group-prepend">
                            <span  class="input-group-text"><b>Indique la Cantidad:</b></span>
                        </div>
                        {{Form::text("cantidad", "1", ['onFocus'=>'showHelp(2)',"required"=>"required", "maxlength"=>"3", "class"=>"form-control required text-right ", "id"=>"cantidad", "placeholder"=>__('1')])}}    
                    </div>

                    <div class="small-box bg-gray-light mt-2 ">
                        <div style="margin-left: unset; margin-right: unset; " class="row border-bottom">
                            <div class="col-md-6"><h3>Monto Bs.</h3></div>
                            <div class="col-md-6 text-right"><h3 id="monto_bs">{{muestraFloat(0, 2)}}</h3></div>
                        </div>
                    </div>
                    <div class="small-box bg-gray-light ">
                        <div style="margin-left: unset; margin-right: unset; " class="row border-bottom">
                            <div class="col-md-6"><h3>Monto $.</h3></div>
                            <div class="col-md-6 text-right"><h3 id="monto_div">{{muestraFloat(0, 2)}}</h3></div>
                        </div>
                    </div>


                </div>
                <div class="col-md-4 border-right">
                    <h2 class="border-bottom">Información</h2>

                    <div class="card card-widget ">
                        <div class="card-footer p-0">
                            <ul id="listTicketsExists" class="nav flex-column">

                                @foreach($Tiempos as $key => $value)


                                <li class="nav-item">
                                    <a onClick="setTicket({{$value->id}})" href="javascript:void(0)" class="nav-link">
                                        <span class=" badge bg-primary">{{$value->id}}</span> <img src="{{url('/')}}/dist/img/{{$value->icono}}.png" style="max-width:25px" class="img-fluid" > {{$value->nombre}} (Existencia: {{$value->cant}}), (Valor $: {{muestraFloat($value->precio)}})
                                    </a>
                                </li>

                                @endforeach
                            </ul>
                        </div>
                    </div>



                </div>
                <div class="col-md-3 border-right">
                    <h2 class="border-bottom">Cupones Comprado</h2>

                    <div id="facturaFiscal" class="row" style=" overflow-x: hidden; overflow-y: auto; height: 100%; width: 97%; font-size: 14px;margin-left: 10px;" >
                        <ul class="" style=" border-radius: 0.2em; list-style: none; padding: 3px;   width:100%">
                            <li  style="text-align:center;"><strong>BAER</strong></li>
                            <li  style="text-align:center; font-size:12px; width: 100%">BOLIVARIANA DE AEROPUERTOS C.A.</li>
                            <li>
                                <ul class=" list-unstyled" style="list-style: none; font-size: 12px;  font-family: ROBOTO;   ">
                                    <li>CAJERO: {{Auth::user()->name_user.' '.Auth::user()->surname_user}} </li>
                                    <li>FECHA: <?php echo date("d/m/Y"); ?><span style="float: right; margin-right: 25px;">HORA: {{date('H:i')}} PM</span></li>
                                    <li style="width:100%; border-top:1px dashed " ></li>
                                    <div  id="detalleFact">
                                        <li class="row" ><span class="col-md-2"><b>CANT</b></span><span class="col-md-7"><b>Cupón</b></span><span class="col-md-3"><b>MONTO $</b></span></li> 
                                    </div>
                                    <li style="width:100%; border-top:1px dashed " ></li>


                                    <li style="font-size: 14px; margin-left: 0px" class="row">
                                        <span class="col-md-3" style="padding-left: 0px;"><strong>TOTAL $</strong></span>
                                        <span class="col-md-9 " style="padding-right: 25px; text-align: right;" id="totalFactura" ><strong>0,00</strong></span>
                                    </li>



                                </ul>
                            </li>



                        </ul>

                    </div>


                </div>
                

            </div>



            <!-- /.card-body -->
        </div>
        <div class="card-footer ">
            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        
                    
                        <div class="col-md-8"><h3><b>Tasa del Dolar:</b></h3></div>
                        <div class="col-md-4 text-right"><h3 id="monto_bs"><b>{{muestraFloat($VALOR_DOLLAR, 2)}}</b></h3></div>
                    </div>
                </div>
                <div class="col-md-6 text-center">

                    <button  type="button" onClick="setPDF()" class="btn btn-app bg-warning mt-2">
                        <i class="far fa-file-pdf" ></i> <b>F3</b>
                    </button>

                    {{--
                    <a href="javascript:void(0)" class="btn btn-app bg-warning mt-2">

                        <i class="fas fa-edit"></i> <b>F2</b>
                    </a>
                    ---}}
                    <button  type="button" onClick="setMoney()" class="btn btn-app bg-primary mt-2">
                        <i class="fas fa-save"></i> <b>F6</b>
                    </button>

                    <button  type="button" onClick="clearAll()" class="btn btn-app bg-danger mt-2">
                        <i class="fas fa-trash"></i> <b>ESC</b>
                    </button>
                   
                    
                </div>
            </div>

        </div>
        <!-- /.card -->

    </div>


    <div class="modal fade" id="modal-pay"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Detalle del Pago</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                <div id="modal-body-princ-vuelo"  class="modal-body">
                    {{Form::open(["route"=>"venta_tickets",'class'=>'form-horizontal',   'id'=>'frmPrinc2','autocomplete'=>'Off'])}}
                    
                    
                    
                    <button  type="button" style="display: none" onclick="document.getElementById('see_codes').contentWindow.print();" id="btnPrint"  class="btn btn-primary  btn-block">
                        <i class="fas fa-save"></i> IMPRIMIR
                    </button>  
                    
                    
                    <div id="divForm" class="row">
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span  class="input-group-text"><b>Efectivo Bs:</b></span>
                            </div>
                            {{Form::text("fp[1]", "", ["data-formula"=>"[VALOR]/[VALOR_DOLLAR]", "class"=>"form-control required fp text-right money", "id"=>"fp_1" ])}}    

                        </div>
                        
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span  class="input-group-text"><b>Divisas $:</b></span>
                            </div>
                            {{Form::text("fp[2]", "", ["data-formula"=>"[VALOR]", "class"=>"form-control required fp text-right money", "id"=>"fp_2", ])}}    

                        </div>
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span  class="input-group-text"><b>Punto Venta Bs:</b></span>
                            </div>
                            {{Form::text("fp[3]", "", ["data-formula"=>"[VALOR]/[VALOR_DOLLAR]", "class"=>"form-control fp required text-right money", "id"=>"fp_3" ])}}    
                            <div class="input-group-prepend">
                                <span  class="input-group-text"><b>Ref :</b></span>
                            </div>
                            {{Form::text("ref1", "", [ "class"=>"form-control  text-right ", "id"=>"ref1" ])}}    

                        </div>
                        
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span  class="input-group-text"><b>Pago Movil Bs:</b></span>
                            </div>
                            {{Form::text("fp[4]", "", ["data-formula"=>"[VALOR_DOLLAR]*[VALOR]", "class"=>"form-control fp required text-right money", "id"=>"fp_4" ])}}    
                            <div class="input-group-prepend">
                                <span  class="input-group-text"><b>Ref :</b></span>
                            </div>
                            {{Form::text("ref2", "", [ "class"=>"form-control  text-right ", "id"=>"ref2" ])}}    

                        </div>
                        
                        
                    </div> 
                    <div id="divIframe" style="display:none" class="row">
                        <iframe src="" id="see_codes" name="see_codes" style="width:100%; height: 100vh; border: none">

                        </iframe>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button  type="button" onclick="crearTickets()" id="guardaFinal" class="btn btn-primary  btn-block">
                            <i class="fas fa-save"></i> GUARDAR
                        </button>  
                    </div>
                    {{ Form::close() }} 
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        var TICKETS_TEMP = [];
        var TICKETS = [];
        
        var winQR;
        
        var tiempo;
        
        @foreach($Tiempos as $key => $value)
            TICKETS[{{$value->id}}] = [];
            TICKETS[{{$value->id}}]['cant']= {{$value->cant}};
            TICKETS[{{$value->id}}]['precio']= {{$value->precio}};
            TICKETS[{{$value->id}}]['nombre']= '{{$value->nombre}}';
            TICKETS[{{$value->id}}]['icono']= '{{$value->icono}}';
            
            TICKETS_TEMP[{{$value->id}}]= [];
            TICKETS_TEMP[{{$value->id}}]['cant']= 0;
            
        @endforeach
        
        $(document).ready(function () {
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $(".select2").select2();
            $("#full-screen").click();
            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            
            //winQR = PopupCenter("{{route('venta_tickets', 'QR')}}", "winQR", 800,600);
            
            
            $('#frmPrinc2').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $("#tiempo_id").on("keypress", function (event) {
                
                if (parseInt(event.which, 10) == 13) {
                    tiempo = parseInt(this.value, 10);
                    if (!isNaN(tiempo)){
                        setTicket(tiempo);
                    }
                    
                }
            });
            
            $("#tiempo_id").on('keydown', function (event) {
                if (event.keyCode == 27) {  //ESCAPE
                    event.preventDefault();
                    
                    clearAll();
                    
                    return false;
                }
                
            });
            
            $("#modal-pay").on('shown.bs.modal', function (e) {
                //ref = e.target.href.split("#")[1];
                $("#ref1, #ref2").val("");
                for(i=1; i<=4; i++){
                    $("#fp_"+i).val(muestraFloat(0));
                }
                $("#fp_1").val( $("#monto_bs").html()  ).focus();
                
            });
            
            $("#cantidad").on("keypress", function (event) {
                //console.log(event.which);
                if (parseInt(event.which, 10) == 13) {
                    cantidad = parseInt(this.value, 10);
                    
                    if (!isNaN(cantidad)) {
                        if (TICKETS[tiempo]['cant'] >= cantidad) {
                            
                            
                            //$("#detalleFact").append('<li class="row filaFactTick" ><span class="col-md-2">'+cantidad+'</span><span class="col-md-7">'+TICKETS[tiempo]['nombre']+'</span><span class="col-md-3">'+muestraFloat(TICKETS[tiempo]['precio'])+'</span></li> ');
                            
                            
                            TICKETS_TEMP[tiempo]['cant'] += parseInt(cantidad, 10);
                            TICKETS[tiempo]['cant'] -= cantidad;
                            Toast.fire({
                                icon: "success",
                                title: "Agregado Correctamente"
                            });
                            showTime();
                            this.value="";
                            
                            $(".filaFactTick").remove();
                            monto_div = 0;
                            for(i in TICKETS_TEMP ){
                                monto_div += (TICKETS_TEMP[i]['cant']*TICKETS[i]['precio']);
                                if (TICKETS_TEMP[i]['cant']>0){
                                    $("#detalleFact").append('<li class="row filaFactTick" ><span class="col-md-2">'+TICKETS_TEMP[i]['cant']+'</span><span class="col-md-7">'+TICKETS[i]['nombre']+'</span><span class="col-md-3">'+muestraFloat(TICKETS_TEMP[i]['cant']*TICKETS[i]['precio'])+'</span></li> ');
                                }
                            }
                            monto_bs = monto_div* VALOR_DOLLAR;
                            $("#monto_div").html(muestraFloat(monto_div));
                            $("#monto_bs").html(muestraFloat(monto_bs));
                            
                            
                            $("#totalFactura").html("<b>"+muestraFloat(monto_div)+"</b>");
                            
                            
                            
                            $("#tiempo_id").val("").focus();
                        } else {
                            Toast.fire({
                                icon: "error",
                                title: "El Cupón sin Stock"
                            });
                            $(this).focus();
                            $(this).select();

                        }
                        


                    } else {
                        $(this).focus();
                        $(this).select();
                    }


                }
            });


            $(window).on('keydown', function (event) {

                if (event.keyCode == 27) {  //ESCAPE
                    event.preventDefault();
                    
                    clearAll();
                    
                    return false;
                }

                if (event.keyCode == 113) {  //F2 
                    event.preventDefault();
                    alert("EDITAR");
                    return false;
                }
                if (event.keyCode == 117) {  //F6 
                    event.preventDefault();
                    setMoney();
                    return false;
                }

                if (event.keyCode == 116) {  //F5
                    event.preventDefault();
                    return false;
                }
                if (event.keyCode == 114) { 
                    event.preventDefault(); //F3
                    setPDF();
                    return false;
                }

            });

            $("#tiempo_id").focus();


        });

        function setPDF(){
            window.open('{{ route("generatePdf") }}', '_blank');
        }
        
        function clearAll(){
            $(".filaFactTick").remove();
            $("#divForm").show();
            $("#divIframe").hide();
            
            //$("#guardaFinal").show();
            //$("#btnPrint").hide();
              
            for(i in TICKETS_TEMP){
                TICKETS[i]['cant'] += parseInt(TICKETS_TEMP[i]['cant'], 10);
                TICKETS_TEMP[i]['cant'] = 0;
            }
            
            
            showTime();
            
            $("#monto_div").html(muestraFloat(0));
            $("#monto_bs").html(muestraFloat(0));
            $("#totalFactura").html("<b>"+muestraFloat(0)+"</b>");
            
            $("#tiempo").focus();
        }

        function showTime(){
            $("#listTicketsExists").html("");
            for(i in TICKETS){
                tick  = '<li class="nav-item">'+
                        '<a onClick="setTicket('+i+')" href="javascript:void(0)"  class="nav-link">';

                tick += '<span class=" badge bg-primary">'+i+'</span> ';
                tick += '<img src="'+ROOT+'/dist/img/'+TICKETS[i]['icono']+'.png" style="max-width:25px" class="img-fluid" > ';
                tick += TICKETS[i]['nombre']+' (Existencia: '+TICKETS[i]['cant']+'), (Valor $: '+muestraFloat(TICKETS[i]['precio'])+')';

                tick += '</a>';
                tick += '</li>';

                $("#listTicketsExists").append(tick);                        

            }
            
            
        }
        
        function setTicket(tick){
            tiempo = tick;
            $("#tiempo_id").val(tick);
            setTiempo(tick);
        }
        
        function setTiempo(tiempo){
            if (!isNaN(tiempo)) {
                if (tiempo in TICKETS) {

                    if (TICKETS[tiempo]['cant'] > 0) {

                        $("#cantidad").val(1).select();
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "El Ticket no Posee Existencia"
                        });
                        $("#tiempo_id").focus();
                        $("#tiempo_id").select();
                    }
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "Tipo de Ticket no Existe"
                    });
                    $("#tiempo_id").focus();
                    $("#tiempo_id").select();
                }
            } else {
                $("#tiempo_id").focus();
                $("#tiempo_id").select();
            }
        }
        
        function showHelp(op){
            switch (op) {
                case 1:
                    msg = "Ingresa el Tipo de Cupones a Comprar y Presione Enter";
                  break;
                case 2:
                    msg = "Ingresa la Cantidad de Cupones a Comprar y Presione Enter";
                  break;
                
                
                default:
                    msg = "";
                  break;
            }
            caja = '<div class="col-md-12 info">'+msg+'</div>';
            $("#htmlHelp").html(caja);
        }
        
        
        function setMoney(){
            if (   $(".filaFactTick").length >0 ){
                $("#divForm").show();
                $("#divIframe").hide();
                
                $("#guardaFinal").show();
                $("#btnPrint").hide();
                
                $("#modal-pay").modal("show");
            }else{
                Toast.fire({
                    icon: "error",
                    title: "Debe Registrar los Cupones a Vender"
                });
                $("#tiempo_id").focus();
                
            }
        }
        
        function crearTickets(){
            if (  $("#frmPrinc2").valid()  ){
                datos = $("#frmPrinc2").serialize();
                for(i in TICKETS_TEMP){
                    datos +="&tickets["+i+"]="+TICKETS_TEMP[i]['cant'];
                    if (TICKETS_TEMP[i]['cant']> 0){
                        TICKETS[i]['cant'] -= TICKETS_TEMP[i]['cant'];
                    }
                }
                $("#frmPrinc2").prepend(LOADING);
                $.post($("#frmPrinc2").attr("action") ,datos, function (response){
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    
                    //$("#modal-pay").modal("hide");
                    $("#guardaFinal").hide();
                    $("#btnPrint").show();
                    
                    $("#divForm").slideUp('fast', function (){
                        $("#divIframe").show('fast', function (){
                            $("#see_codes").attr("src", "{{url('tickets/venta-tickets')}}/CODES:"+response.data);
                        });
                    });
                    
                    
                    clearAll();
                    refreshTiempos();
                    winQR = PopupCenter("{{url('tickets/venta-tickets')}}/QR:"+response.data, "winQR", 800,600);
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Guardar"
                    });
                    $(".overlay-wrapper").remove();
                });
                    
                
                
            }
        }
        
        function PopupCenter(pageURL, title,w,h) {
            let left = (screen.width/2)-(w/2);
            let top = (screen.height/2)-(h/2);
            let winPOPUP = window.open (pageURL, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
            return winPOPUP;
        } 
        
        function refreshTiempos(){
             
            $.post("{{route('venta_tickets')}}" ,'_method=put&_token={{csrf_token()}}', function (response){
                $(".overlay-wrapper").remove();
                
                $("#listTicketsExists").html("");
                for(i in response.data){
                    tick  = '<li class="nav-item">'+
                            '<a onClick="setTicket('+response.data[i]['id']+')" href="javascript:void(0)"  class="nav-link">';

                    tick += '<span class=" badge bg-primary">'+i+'</span> ';
                    tick += '<img src="'+ROOT+'/dist/img/'+response.data[i]['icono']+'.png" style="max-width:25px" class="img-fluid" > ';
                    tick += response.data[i]['nombre']+' (Existencia: '+response.data[i]['cant']+'), (Valor $: '+muestraFloat(response.data[i]['precio'])+')';

                    tick += '</a>';
                    tick += '</li>';

                    $("#listTicketsExists").append(tick);                        

                }
                
            }).fail(function () {

                $(".overlay-wrapper").remove();
            });
        }
        function setTime(){
            $("#fechaHora").html('FECHA: '+moment().format('D/MM/YYYY')+'<span style="float: right; margin-right: 25px;">HORA: '+moment().format('HH:mm a')+' </span>');
            setTimeout( setTime, 60000 );
        }
    </script>

@endif
</div>
