<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <script type="text/javascript">
            function imprimir_tickets() {
                window.print();
            }
        </script>
    </head>
    <body class="container">

        <div style="border: 1px solid black; border-radius: 0.2em; padding: 3px;  background-color: white">
            <div style="padding: 2px; text-align:center ">
                <img src="{{url('dist/img/logo5.png')}}" style="max-height: 90px" /> 

            </div>
            <table style="width:100%; border-collapse: collapse;" >
                @foreach($QRS->detalle as $value)
                <tr>
                    <td style="text-align:center; padding: 15px; font-family: ROBOTO; font-size: 26px; border:solid 1px; ">Ticket {{$value->ticket->lote->tiempo->nombre}}<br> <b>{{$value->codigo}}</b></td>
                    <td style="text-align:center; padding: 15px; font-family: ROBOTO; font-size: 12px; border:solid 1px; ">{!! QrCode::size(100)->generate($value->codigo); !!}</td>
                </tr>
                @endforeach
            </table>

        </div>
    </body>
</html>