<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

</head>

{{-- <style>
  table {
   width: 100%;
   border: 1px solid #000;
}
th, td {
   width: 25%;
   text-align: center;
   vertical-align: top;
   border: 1px solid #000;
   border-collapse: collapse;
   padding: 0.3em;
   caption-side: bottom;
}
caption {
   padding: 0.3em;
   color: #fff;
    background: #000;
}
th {
   background: #eee;
}
</style> --}}
<body>
    <header class="imagen-titulo">
        <img src="{{url('dist/img/banner.jpeg')}}" width="100%" alt="" srcset="">
    </header>
    <h5 class="mt-5">VENTAS DE HOY</h5>
    <table id="movimientos-table" class="table table-bordered table-hover" style="width: 100%; font-size: 10pt;">
        <thead style="background-color: #001042; color: white;">
            <tr>
                <th class="text-center" colspan="6">{{ $nombre_aeropuerto}}  |  {{ showDate($fecha_actual) }}</th>
            </tr>
            <tr>
            </thead>    

                <th><b>USUARIO</b></th>
                <th><b>HORA</b></th>
                <th><b>CODIGO BOLETO</b></th>
                <th><b>TIEMPO</b></th>
                <th><b>VALOR $</b></th>
                <th><b>MONTO Bs</b></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($Tickes as $Ticket)
                @foreach ($Ticket->detalle as $Boleto)
                <tr>
                    <td>{{ upper($Ticket->usuarios->name_user) }} {{ upper($Ticket->usuarios->surname_user) }}</td>
                    <td>{{ showDate($Ticket->fecha, 'hour') }}</td>
                    <td>{{ $Boleto->codigo}}</td>
                    <td>{{ $Boleto->ticket->lote->tiempo->nombre }}</td>
                    <td>{{ muestraFloat($Boleto->precio) }}</td>
                    <td>{{ muestraFloat($Ticket->tasa * $Boleto->precio) }}</td>

                    {{-- <td>{{ $Ticket->venta_id }}</td>
                    <td>{{ $Ticket->getMetodoPago->nombre }}</td>
                    <td>{{ $Ticket->monto }}</td> --}}
                </tr>
                @endforeach
            @endforeach


    </table>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>

</body>
</html>