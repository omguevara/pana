

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Hangar')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('hangares')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["hangares.edit", $Hangar->crypt_id],  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}

        <div class="card-body ">

            <div class="form-group">
                <div class="input-group mt-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Aeropuerto:</b></span>
                    </div>
                    {{Form::select('aeropuerto_id', $Aeropuertos, $Hangar->aeropuerto->crypt_id , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                </div>
            </div>
            <div class="form-group">
                <div class="input-group mt-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Número de Hangar:</b></span>
                    </div>
                    {{Form::text("nombre", $Hangar->nombre, ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"nombre", "placeholder"=>__('Número de Hangar')])}} 
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Dimensión:</b> <span title="" class="badge bg-primary">Mts2</span></span>
                    </div>
                    {{Form::text("dimension", $Hangar->dimension, ["required"=>"required",  "class"=>"form-control required money text-right  ", "id"=>"dimension", "placeholder"=>__('Dimensión')])}} 

                </div>
            </div>


            <div class="form-group">
                <div class="input-group mt-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Razón Social:</b></span>
                    </div>
                    {{Form::text("razon_social", $Hangar->razon_social, ["required"=>"required",  "class"=>"form-control required  ", "id"=>"razon_social", "placeholder"=>__('razon social')])}} 


                </div>
            </div>
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('hangares')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
