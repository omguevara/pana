

<div id="panel_princ" class="col-sm-12 col-md-12   mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Nuevo Hangar')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('hangares')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"hangares.create",  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">
                <div class="col-md-6">
                    <div class="card ">
                        <div class="card-header bg-info">
                            <h3 class="card-title">Datos Principales</h3>
                        </div>
                        <div class="card-body ">


                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Aeropuerto:</b></span>
                                    </div>
                                    {{Form::select('aeropuerto_id', $Aeropuertos, "" , ["placeholder"=>(count($Aeropuertos)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Hangar:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Dimensión:</b> <span title="" class="badge bg-primary">Mts2</span></span>
                                    </div>
                                    {{Form::text("dimension", "", ["required"=>"required",  "class"=>"form-control required money text-right  ", "id"=>"dimension", "placeholder"=>__('Dimensión')])}} 

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Ubicación:</b></span>
                                    </div>
                                    {{Form::select('ubicacion_id', $Ubicaciones, "" , ["placeholder"=>(count($Ubicaciones)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"ubicacion_id" ,"required"=>"required"])}}
                                    <div role="button" class="input-group-append">
                                        <div  title="Agregar Nueva Ubicación" onClick="$('#modal-add-ubicacion').modal('show')" class="input-group-text">
                                            <i class="fa fa-plus"></i>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     

                <div class="col-md-6">
                    <div class="card ">
                        <div class="card-header bg-info">
                            <h3 class="card-title">Datos Arrendatario</h3>
                        </div>
                        <div class="card-body ">

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Document')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                </div>
                                {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "onBlur"=>"findDato2();" ,"minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Razón Social:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 


                                </div>
                            </div>

                            <div class="input-group mt-2 ">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>{{__('Representante Local')}}:</b></span>
                                </div>
                                <div class="input-group-prepend" >
                                    {{Form::select('type_document', ["V"=>"V",  "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                </div>
                                {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "onBlur"=>"findDato2();" ,"minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required cliente", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Nombres y Apellidos:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 


                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>N&uacute;mero de Contacto:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 


                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>N&uacute;mero de Contrato:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 


                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Objeto:</b></span>
                                    </div>
                                    {{Form::select('ubicacion_id', [], "" , ["placeholder"=>(count($Ubicaciones)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"ubicacion_id" ,"required"=>"required"])}}
                                    <div role="button" class="input-group-append">
                                        <div  title="Agregar Nueva Ubicación" onClick="$('#modal-add-ubicacion').modal('show')" class="input-group-text">
                                            <i class="fa fa-plus"></i>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Uso:</b></span>
                                    </div>
                                    {{Form::select('ubicacion_id', [], "" , ["placeholder"=>(count($Ubicaciones)>1 ? 'Seleccione':null),   "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 ", "id"=>"ubicacion_id" ,"required"=>"required"])}}
                                    <div role="button" class="input-group-append">
                                        <div  title="Agregar Nueva Ubicación" onClick="$('#modal-add-ubicacion').modal('show')" class="input-group-text">
                                            <i class="fa fa-plus"></i>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Fecha de Suscripci&oacute;n:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 


                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Fecha de Vencimiento:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 


                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mt-2">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Canon:</b></span>
                                    </div>
                                    {{Form::text("hangar", "", ["required"=>"required",  "class"=>"form-control required matricula", "id"=>"hangar", "placeholder"=>__('Hangar')])}} 
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><b>Dimensión:</b> <span title="" class="badge bg-primary">Mts2</span></span>
                                    </div>
                                    {{Form::text("dimension", "", ["required"=>"required",  "class"=>"form-control required money text-right  ", "id"=>"dimension", "placeholder"=>__('Dimensión')])}} 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>     


            </div>





            <!-- /.card-body -->

            <div class="card-footer text-center">
                {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

            </div>
            <!-- /.card -->
            {{ Form::close() }}
        </div>
    </div>


    <div class="modal fade" id="modal-add-ubicacion"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Ubicacion</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>
                {{Form::open([ 'id'=>'frmUbicacion','autocomplete'=>'Off'])}}
                <div  class="modal-body">
                    <div class="form-group">
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Ubicación:</b></span>
                            </div>
                            {{Form::text("ubicacion", "", [ "required"=>"required", "class"=>"form-control required ", "id"=>"ubicacion", "placeholder"=>__('Ubicación')])}}    
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button('<li class="fa fa-save"></li> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $(".select2").select2();
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $(".matricula").alphanum({
                allowNumeric: true,
                allowUpper: true,
                allowLower: true,
                allowSpace: false,
                allowOtherCharSets: false


            });
            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    $("#frmPrinc1").prepend(LOADING);
                    $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('hangares')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                        //console.log(response);
                    }).fail(function () {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    });
                } else {
                    $(".select2-container").each(function () {
                        padre = $(this).parent().width();
                        $(this).siblings().not(".error").each(function () {
                            padre -= $(this).width();
                        });
                        $(this).css("width", padre);
                    });
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
            $('#frmUbicacion').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });
        });

    </script>
</div>