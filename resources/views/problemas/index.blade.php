<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registro de Errores del Sistema ')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row text-center">
                <a  class="actions_users btn btn-primary " onClick="$('#modal-err-sv').modal('show')" href="javascript:void(0)"><li class="fa fa-plus"></li> {{__("Crear Nuevo Registro")}}</a>
            </div>
            <div class="col-md-12">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>{{__('Aeropuerto')}}</th>
                            <th>{{__('Modulo')}}</th>
                            <th>{{__('Usuario')}}</th>
                            <th>{{__('Estado')}}</th>
                            <th>{{__('Opciones')}}</th>

                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <div class="modal fade" id="modal-err-sv"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-lg">
            {{Form::open(["route"=>"problemas", "onSubmit"=>"return false",  'id'=>'frmPrinc1','autocomplete'=>'on'])}}
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Agregar Error</h4>

                </div>
                <div id="modal-nave-body" class="modal-body">
                    <div class="row">


                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Módulo con Error:</b></span>
                            </div>
                            {{Form::text("modulo", "", ["maxlength"=>"50", "required"=>"required", "class"=>"form-control required ", "id"=>"modulo", "placeholder"=>__('Módulo')])}}    
                        </div>
                        <div class="col-sm-12 col-md-12">


                            <label for="descripcion">Descripción </label>
                            {{Form::textarea("descripcion", "", ["maxlength"=>"6",  "required"=>"required", "class"=>"form-control required  ", "id"=>"descripcion", "placeholder"=>__('Descripción')])}}    


                        </div> 



                    </div>  
                </div>
                <div  class="modal-footer justify-content-between">

                    <button  id="btn-clse-nave"  type="button" class="btn btn-secundary bg-danger"  data-dismiss="modal"><li class="fa fa-undo"></li> {{__('Cerrar')}}</button>

                    <button  class="btn btn-primary" id="btn-save-nave"  type="button"  onclick="saveErr()"><li class="fa fa-save"></li> {{__('Save')}}</button>
                </div>
            </div>

            {{ Form::close() }} 
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-err-see"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-xl">
            {{Form::open(["route"=>"problemas", "onSubmit"=>"return false",  'id'=>'frmPrinc2','autocomplete'=>'on'])}}
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detalle del Error</h4>

                </div>
                <div id="modal-nave-body" class="modal-body">
                    <div class="row">


                        <div id="div_modulo" class="col-sm-12 col-md-12">
                            <label >Módulo: </label>
                        </div>
                        <div id="div_desc" class="col-sm-12 col-md-6">
                            <label >Descripción: </label>



                        </div> 

                        <div id="div_resp" class="col-sm-12 col-md-6">
                            <label >Respuestas: </label>



                        </div> 



                    </div>  
                </div>
                <div  class="modal-footer justify-content-between">

                    <button  id="btn-clse-nave"  type="button" class="btn btn-secundary bg-danger"  data-dismiss="modal"><li class="fa fa-undo"></li> {{__('Cerrar')}}</button>


                </div>
            </div>

            {{ Form::close() }} 
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <script type="text/javascript">
        var data1 = @json($data)
                ;
        $(document).ready(function () {

            var table1 = $('#example1').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                data: data1,
                columns: [
                    {data: 'aeropuerto.full_nombre'},
                    {data: 'modulo'},
                    {data: 'usuario.full_nombre'},
                    {data: 'estatus'},
                    {data: 'action'},
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()-> getLocale()}}.json"
                }


            });


            $('#descripcion').summernote({
                toolbar: [
                    ['style', ['style','highlight', 'bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['table', ['table']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view', ['fullscreen']]
                ]
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $("#modal-err-sv").on('shown.bs.modal', function () {
                $("#modulo").focus();

            });

        });
        function saveErr() {
            if ($("#frmPrinc1").valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    $('#modulo').val('');
                    $('#descripcion').summernote('code', '');


                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: 'error',
                        title: 'Error al Guardar'
                    });
                });
            }
        }
        function getDetailError(obj, event) {

            event.preventDefault();
            $("#panel_princ").prepend(LOADING);
            $.get(obj.href, function (response) {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });

                $("#div_modulo").html('<label >Módulo: </label> ' + response.data.modulo);
                $("#div_desc").html('<label >Descripción: </label> ' + response.data.descripcion);
                $("#div_resp").html();


                $("#modal-err-see").modal('show');
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: 'error',
                    title: 'Error al Buscar'
                });
            });
        }


    </script>
</div>
