
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">




    </head>
    <body style="text-align:center" >
        <div class="wrapper">
            <div id="main-content" class="content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <img style="max-width:100%" class="img-fluid" src="{{url('/')}}/dist/img/logo.png" />
                    </div>
                </div>
                <div class="row" >
                    <div class="col-md-12">
                        <img style="max-width:80%" class="img-fluid" src="{{url('/')}}/dist/img/404.jpg" />
                    </div>

                </div>
            </div>
        </div>
        
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function () {
                document.location="{{request()->getSchemeAndHttpHost()}}";
            });
        </script>
    </body>
</html>
