<div class="col-sm-12 offset-md-3  col-md-6">

    <div class="card card-danger ">
        <div class="card-header">
            <h3 class="card-title text-center">
                

                    <i class="fas fa-exclamation-triangle"></i>
                    Alerta 

                
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <h2>{{__("PRECAUCIÓN")}}</h2>
                <h3>{{__("Esta Taquilla ya Fue Cerrada por el día de Hoy: ").date("d/m/Y")}}</h3>
            </div>
        </div>
        <!-- /.card-body -->
    </div><!-- comment -->
</div>