<div class="col-sm-12 offset-md-3  col-md-6">

    <div class="card card-danger ">
        <div class="card-header">
            <h3 class="card-title text-center">


                <i class="fas fa-exclamation-triangle"></i>
                Error 403




            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <h2>{{__("FORBIDDEN")}}</h2>
                <h3>{{__("Access to this resource on the server is denied!")}}</h3>
            </div>
        </div>
        <!-- /.card-body -->
    </div><!-- comment -->
</div>
<script type="text/javascript">
    $(document).ready(function  (){
        document.location="{{request()->getSchemeAndHttpHost()}}";
    });
</script>