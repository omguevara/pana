@php
    $base_imponible = 0; // TODO LO QUE TENGA IVA
    $excento = 0; // TODO LO QUE NO TENGA IVA
    $sub_total = 0; // SUMAR TODO
    $iva = 0; 
    $sub_total_g = 0;

@endphp
@if (count($tasa)>0)
<table class="table table-sm table-bordered table-hover">
    <thead>
        <tr>
            <th style="text-align: center" colspan="4" >Tasa</th>
        </tr>
        <tr>
            <th  style="width: 10px">#</th>
            
            <th >Descripci&oacute;n</th>
            <th  style="width: 120px">Precio Bs</th>
            <th  style="width: 120px">Precio {{$MonedaPago}}</th>
        </tr>
        
    </thead>
    <tbody>
        @php
            $base_imponible = 0; // TODO LO QUE TENGA IVA
            $excento = 0; // TODO LO QUE NO TENGA IVA
            $sub_total = 0; // SUMAR TODO
            $iva = 0; 
            $sub_total_g = 0;
            
        @endphp
        @foreach($tasa as $key => $value)
            @php
                $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                $sub_total += ($value['precio']*$value['categoria_aplicada']);
                $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
                
            @endphp
        <tr>
            <td>{{$key+1}}</td>
            
            <td>{{$value['descripcion'].' '.$value['iva2']}}</td>
            <td align="right">{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td>
            
            <td align="right">{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
        </tr>
        @endforeach
       
        <tr>
            <td colspan="2" align="right">BASE IMPONIBLE</td>
            <td align="right">{{muestraFloat($base_imponible*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($base_imponible)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">EXCENTO</td>
            <td align="right">{{muestraFloat($excento*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($excento)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">SUB TOTAL</td>
            <td align="right">{{muestraFloat($sub_total*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($sub_total)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">IVA</td>
            <td align="right">{{muestraFloat($iva*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($iva)}}</td>
            
        </tr>
         <tr>
            <td colspan="2" align="right">TOTAL</td>
            <td align="right">{{muestraFloat(($sub_total+$iva)*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($sub_total+$iva)}}</td>
            
        </tr>
    </tbody>
</table>
@php
    $sub_total_g += (($sub_total+$iva)*${$MonedaPago});
@endphp
@endif
@if (count($dosa)>0)
<table class="table table-sm table-bordered table-hover">
    <thead>
        <tr>
            <th style="text-align: center" colspan="4" >Dosa</th>
        </tr>
        <tr>
            <th  style="width: 10px">#</th>
            
            <th >Descripci&oacute;n</th>
            <th  style="width: 120px">Precio Bs</th>
            <th  style="width: 120px">Precio {{$MonedaPago}}</th>
        </tr>
    </thead>
    <tbody>
        @php
            $base_imponible = 0; // TODO LO QUE TENGA IVA
            $excento = 0; // TODO LO QUE NO TENGA IVA
            $sub_total = 0; // SUMAR TODO
            $iva = 0; 
        @endphp
        @foreach($dosa as $key => $value)
            @php
                $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                $sub_total += ($value['precio']*$value['categoria_aplicada']);
                $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
            @endphp
        <tr>
            <td>{{$key+1}}</td>
            
            <td>{{$value['descripcion'].' '.$value['iva2']}}</td>
            <td align="right">{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td>
            
            <td align="right">{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="2" align="right">BASE IMPONIBLE</td>
            <td align="right">{{muestraFloat($base_imponible*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($base_imponible)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">EXCENTO</td>
            <td align="right">{{muestraFloat($excento*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($excento)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">SUB TOTAL</td>
            <td align="right">{{muestraFloat($sub_total*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($sub_total)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">IVA</td>
            <td align="right">{{muestraFloat($iva*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($iva)}}</td>
            
        </tr>
         <tr>
            <td colspan="2" align="right">TOTAL</td>
            <td align="right">{{muestraFloat(($sub_total+$iva)*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($sub_total+$iva)}}</td>
            
        </tr>
    </tbody>
</table>
@php
    $sub_total_g += (($sub_total+$iva)*${$MonedaPago});
@endphp
@endif

@if (count($extra)>0)
<table class="table table-sm table-bordered table-hover">
    <thead>
        <tr>
            <th style="text-align: center" colspan="4" >Servicios Extra</th>
        </tr>
        <tr>
            <th  style="width: 10px">#</th>
            <th >Descripci&oacute;n</th>
            <th  style="width: 120px">Precio Bs</th>
            <th  style="width: 120px">Precio {{$MonedaPago}}</th>
        </tr>
    </thead>
    <tbody>
        @php
            $base_imponible = 0; // TODO LO QUE TENGA IVA
            $excento = 0; // TODO LO QUE NO TENGA IVA
            $sub_total = 0; // SUMAR TODO
            $iva = 0; 
        @endphp
        @foreach($extra as $key => $value)
            @php
                $base_imponible += ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                $excento += ($value['iva_aplicado'] == 0 ? ($value['precio']*$value['categoria_aplicada']):0); 
                $sub_total += ($value['precio']*$value['categoria_aplicada']);
                $iva +=  ($value['iva_aplicado'] > 0 ? ($value['precio']*$value['categoria_aplicada']*$value['iva_aplicado']/100):0); ; 
            @endphp
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$value['descripcion'].' '.$value['iva2']}}</td>
            <td align="right">{{muestraFloat($value['bs']*$value['categoria_aplicada'])}}</td>
            <td align="right">{{muestraFloat($value['precio']*$value['categoria_aplicada'])}}</td>
        </tr>
        @endforeach
        
        <tr>
            <td colspan="2" align="right">BASE IMPONIBLE</td>
            <td align="right">{{muestraFloat($base_imponible*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($base_imponible)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">EXCENTO</td>
            <td align="right">{{muestraFloat($excento*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($excento)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">SUB TOTAL</td>
            <td align="right">{{muestraFloat($sub_total*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($sub_total)}}</td>
            
        </tr>
        <tr>
            <td colspan="2" align="right">IVA</td>
            <td align="right">{{muestraFloat($iva*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($iva)}}</td>
            
        </tr>
         <tr>
            <td colspan="2" align="right">TOTAL</td>
            <td align="right">{{muestraFloat(($sub_total+$iva)*${$MonedaPago})}}</td>
            <td align="right">{{muestraFloat($sub_total+$iva)}}</td>
            
        </tr>
    </tbody>
</table>
@php
    $sub_total_g += (($sub_total+$iva)*${$MonedaPago});
@endphp
@endif
<script type="text/javascript">
    MontoGlobal = "{{muestraFloat($sub_total_g)}}";
    MontoGlobalBs = "{{$sub_total_g}}";
    $("#goTab3").prop("disabled", false);
</script>