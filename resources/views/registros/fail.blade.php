
<style>
    .alert {
        padding: 20px;
        background-color: #f44336; /* Red */
        color: white;
        margin-bottom: 15px;
    }

    .button {
        background-color: #008CBA; /* Green */
        border: none;
        color: white;
        padding: 15px 32px;
        text-align: center;
        text-decoration: none;
        display: inline-block;
        font-size: 16px;
        cursor:pointer;
    }

</style>
<input type="button" class="button" value="Cerrar" onClick="parent.correction()" />
@if (is_array($msg))

@foreach($msg as $value)
<div class="alert">

    {{$value}}
</div>
@endforeach
@else
<div class="alert">

    {{$msg}}
</div>
@endif


