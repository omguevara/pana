@extends('layouts.principal')
@section('content')

<div class="col-sm-12">
    <div class="row mb-2">

        <h1>Registro de Solicitud</h1>
    </div>

</div>



<div class="col-sm-12 offset-md-3 col-md-6 offset-xl-4 col-xl-4">




    <div id="card1" class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Reserva</h3>
        </div>
        <div class="card-body p-0">
            {{Form::open(array("route"=>"reserva_general", "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"tabFrm1"))}}
            <div class="col-sm-12 ">

                <div class="form-group row">
                    <label class="col-sm-4 col-form-label">{{__('Fecha de Vuelo')}}</label>
                    <div class="col-sm-8">
                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                            {{Form::text("fecha", date("d/m/Y"), ["class"=>"form-control datetimepicker-input required", "readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="hora_inicio" class="col-sm-4 col-form-label"  >Hora Salida</label>
                    <div class="col-sm-8">
                        <div class="input-group date" id="timepicker" data-target-input="nearest">

                            {{Form::text("hora_inicio", "", ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control datetimepicker-input required", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                            <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="far fa-clock"></i></div>
                            </div>
                        </div>
                    </div>
                    <!-- /.input group -->
                </div>





                <div class="form-group row">
                    <label for="origen_id" class="col-sm-4 col-form-label">Origen</label>
                    <div class="col-sm-8">
                        {{Form::select('origen_id', $ORIGEN, "", [ "class"=>"form-control ", "id"=>"origen_id" ,"required"=>"required"])}}
                    </div>
                </div>





                <div class="form-group row">
                    <label for="destino_id" class="col-sm-4 col-form-label">Destino</label>
                    <div class="col-sm-8">
                        {{Form::select('destino_id', $DESTINO, "", [ "class"=>"form-control ", "id"=>"destino_id" ,"required"=>"required"])}}
                    </div>
                </div>


                <div class="form-group row">
                    <label for="cantidad_pasajeros" class="col-sm-4 col-form-label">Cantidad de Pasajeros</label>
                    <div class="col-sm-8">
                        {{Form::number("cantidad_pasajeros", "", ["onBlur"=>"calcular()", "required"=>"required", "class"=>"form-control required", "id"=>"cantidad_pasajeros", "placeholder"=>__('Cantidad de Pasajeros')])}}    
                    </div>
                </div>

                <div class="form-group row">
                    <label for="carga" class="col-sm-4 col-form-label">Carga</label>
                    <div class="col-sm-8">
                        {{Form::number("carga", "0", ["required"=>"required", "class"=>"form-control required", "id"=>"carga", "placeholder"=>__('Carga')])}}    
                    </div>
                </div>

                <div class="form-group row">
                    <label for="monto" class="col-sm-4 col-form-label">Total a Pagar</label>
                    <div class="col-sm-8">
                        {{Form::text("monto", "", ["required"=>"required", "readonly"=>"readonly", "class"=>"form-control required", "id"=>"monto", "placeholder"=>__('Total a Pagar')])}}    
                    </div>
                </div>


                <div class="form-group row">
                    <label for="referencia" class="col-sm-4 col-form-label">Referencia Bancaria</label>
                    <div class="col-sm-8">
                        {{Form::text("referencia", "", ["required"=>"required", "class"=>"form-control required", "id"=>"referencia", "placeholder"=>__('Referencia')])}}    
                    </div>
                </div>

                <div class="form-group row">
                    <label for="contacto" class="col-sm-4 col-form-label">Responsable</label>
                    <div class="col-sm-8">
                        {{Form::text("contacto", "", ["required"=>"required", "class"=>"form-control required", "id"=>"contacto", "placeholder"=>__('Responsable')])}}    
                    </div>
                </div>

                <div class="form-group row">
                    <label for="correo" class="col-sm-4 col-form-label">Correo</label>
                    <div class="col-sm-8">
                        {{Form::text("correo", "", ["required"=>"required", "class"=>"form-control email required", "id"=>"correo", "placeholder"=>__('Correo')])}}    
                    </div>
                </div>

                <div class="form-group row">
                    <label for="telefono" class="col-sm-4 col-form-label">Tel&eacute;fono</label>
                    <div class="col-sm-8">
                        {{Form::text("telefono", "", ["required"=>"required", "class"=>"form-control required", "id"=>"telefono", "placeholder"=>__('Teléfono')])}}    
                    </div>
                </div>



                <div class="card-footer text-center">
                    {{Form::button(__("Guardar"),  ["type"=>"button", "class"=>"btn float-right btn-primary", "id"=>"sig1"])}}    

                </div>
            </div>
            {{Form::close()}}
        </div>


    </div>
    <!-- /.card-body -->
    <div class="card-footer">

    </div>
</div>
<!-- /.card -->




<script type="text/javascript">
    TASA = "{{$TASA}}";
    document.addEventListener('DOMContentLoaded', function () {

        $('#iconDate').datetimepicker({
            format: 'L',
            "minDate": moment()
        });
        $('#timepicker').datetimepicker({
            format: 'LT'
        });

        $("#sig1").on("click", function () {
            if ($("#tabFrm1").valid()) {
                $("#card1").prepend(LOADING);

                $("#tabFrm1").submit();
            }


        });

        $('#tabFrm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        
        
        @if ($msg != "")
            Toast.fire({
            icon: 'success',
            title: '{{$msg}}'
        });
        @endif
    });
    
     function calcular(){
         $("#monto").val("Bs. "+ muestraFloat(4*TASA*$("#cantidad_pasajeros").val(), 2)+" / Euro: "+muestraFloat(4*$("#cantidad_pasajeros").val(), 2));
     }
</script>  




@endsection