<div id="panel_princ" class="col-sm-12 offset-md-3 col-md-6  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Detalle del Registros')}}</h3>

            <div class="card-tools">
                 <a href="{{route('registros')}}" onClick="setAjax(this, event)"   class="form-control bg-success">
                    <li class="fa fa-arrow-left "></li> Regresar 
                </a>  

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            <div class="row">

                <div class="col-md-12">
                    
                    <table id="example1" class="table table-bordered table-hover table-sm">
                        <tbody>
                            <tr>
                                <th>Fecha</th>
                                <td>{{$data->fecha2}}</td>
                            </tr>
                            <tr>
                                <th>Hora</th>
                                <td>{{$data->hora}}</td>
                            </tr>
                            <tr>
                                <th>Origen</th>
                                <td>{{$data->origen->codigo_oaci}}</td>
                            </tr>
                            <tr>
                                <th>Destino</th>
                                <td>{{$data->destino->codigo_oaci}}</td>
                            </tr>
                            <tr>
                                <th>Cantidad de Pasajeros</th>
                                <td>{{$data->cantidad_pasajeros}}</td>
                            </tr>
                            <tr>
                                <th>Carga</th>
                                <td>{{$data->carga}}</td>
                            </tr>
                            <tr>
                                <th>Referencia</th>
                                <td>{{$data->referencia}}</td>
                            </tr>
                            <tr>
                                <th>Responsable</th>
                                <td>{{$data->contacto}}</td>
                            </tr><!-- comment -->
                            <tr>
                                <th>Correo</th>
                                <td>{{$data->correo}}</td>
                            </tr>
                            <tr>
                                <th>Tel&eacute;fono</th>
                                <td>{{$data->telefono}}</td>
                            </tr>
                        </tbody>
                    </table>



                </div>

            </div>
        </div>
    </div>
</div>




<script type="text/javascript">
    
            ;
    
    $(function () {

        

        });






    });
</script>