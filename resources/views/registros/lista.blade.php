<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Listado de Registros')}}</h3>

            <div class="card-tools">
                <a id="refreshMolulo" href="{{route('registros')}}" onClick="setAjax(this, event)"   style="display:none">

                </a>  

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        <div id="card-body-main" class="card-body ">
            {{Form::open(array("route"=>"registros", "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frm1"))}}
            <div class="row">
                

                <div class="col-sm-12 col-md-3" >
                    <div class="form-group">
                        <label for="matricula">{{__('Matricula')}}</label>
                        {{Form::text("matricula", "", ["maxlength"=>"10", "required"=>"required", "class"=>"form-control ", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                    </div>
                </div>


                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label >{{__('Fecha de Vuelo')}}</label>

                        <div class="input-group date" id="iconDate" data-target-input="nearest">
                            {{Form::text("fecha_v", "", ["readonly"=>"readonly",  "class"=>"form-control datetimepicker-input ", "readonly"=>"readonly"   ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.form-group -->
                </div>


                <div class="col-12 col-sm-3">
                    <div class="form-group">
                        <label >{{__('Fecha de Registro')}}</label>

                        <div class="input-group date" id="iconDate2" data-target-input="nearest">
                            {{Form::text("fecha_r", '', ["readonly"=>"readonly", "class"=>"form-control datetimepicker-input ", "readonly"=>"readonly"   ,"data-target"=>"#iconDate2"])}}
                            <div class="input-group-append" data-target="#iconDate2" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>

                    </div>
                    <!-- /.form-group -->
                </div>
                
                <div class="col-12 col-sm-3">
                    <label >Opciones</label>
                    <div class="input-group " >
                        <button class="btn btn-primary" onClick="$('#frm1').trigger('reset');"  type="button" title="Borrar Filtros" ><i class="fa fa-trash"></i> </button>
                        <button class="btn btn-primary ml-1" onclick="buscarVuelos()" title="Buscar"  ><i class="fa fa-search"></i> </button>
                        <button class="btn btn-primary ml-1" onclick="addVuelos()" title="Agregar Vuelo"  ><i class="fa fa-plus"></i> </button>
                    </div>
                    
                </div>
                
            </div>
            {{Form::close()}}
            <div class="row">





                <div class="col-md-12">
                    <table id="example1" class="table table-bordered table-hover table-sm">
                        <thead>
                            <tr>

                                <th>{{__('FECHA VUELO')}}</th>
                                <th>{{__('TIPO')}}</th>
                                <th>{{__('MATRICULA')}}</th>
                                <th>{{__('NOMBRE PILOTO')}}</th>
                                <th>{{__('TOTAL')}}</th>
                                <th>{{__('OPCIONES')}}</th>


                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>



                </div>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-add"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalPrincAdd" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registro de Vuelos Aereos</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            {{Form::open(array("route"=>"registros", "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frm2"))}}
            {{Form::hidden("piloto_id", "", ["id"=>"piloto_id"])}}
            <div id="modalPrincBodyAdd" class="modal-body">
                <div class="row">
                    <div class="col-12 col-sm-6 border-right-2">
                        <div class="row">
                            <div class="col-12 col-sm-5">
                                <div class="form-group ">
                                    <label for="hora_inicio" >Hora Salida <span class="right badge badge-info">(Hora Militar)</span></label>

                                    <div class="input-group date" id="timepicker" data-target-input="nearest">

                                        {{Form::text("hora_inicio", date("H:m"), ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                                        <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="far fa-clock"></i></div>
                                        </div>
                                    </div>

                                    <!-- /.input group -->
                                </div>
                            </div>

                            <div class="col-12 col-sm-7">
                                <div class="form-group">
                                    <label for="destino_id" >Destino</label>

                                    {{Form::select('destino_id', $DESTINO, "", ["placeholder"=>"Seleccione",  "class"=>"form-control required", "id"=>"destino_id" ,"required"=>"required"])}}
                                    {{Form::hidden('destino_otro')}}

                                </div>
                                <!-- /.form-group -->
                            </div>
                            
                            
                            <div class="col-sm-12 col-md-5">

                                <div class="form-group">
                                    <label for="document2">{{__('Documento del Piloto')}}</label>
                                    <div class="input-group " >
                                        <div class="input-group-prepend" >
                                            {{Form::select('type_document2', [ "V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document2" ,"required"=>"required"])}}
                                        </div>
                                        {{Form::text("document2", "", ["data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document2", "placeholder"=>__('Document')])}}    
                                        <div class="input-group-append" >
                                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato2" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>

                                    </div>
                                </div>  
                            </div>  
                            <div class="col-sm-12 col-md-7">
                                <div class="form-group">
                                    <label for="piloto">{{__('Piloto')}}</label>
                                    {{Form::text("piloto", "", [ "readonly"=>"readonly", "maxlength"=>"100", "required"=>"required", "class"=>"form-control required tab1", "id"=>"piloto", "placeholder"=>__('Nombre del Piloto')])}}    

                                </div>
                            </div>
                            
                            
                            
                            
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="cant_pasajeros">{{__('Cant. Pasajeros')}}</label>
                                    {{Form::text("cant_pasajeros", "", ["onblur"=>"getProdserv()", "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number tab2", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                                </div>
                            </div>
                            
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="placa">{{__('Matricula')}}</label>
                                    <div class="input-group " >
                                        {{Form::text("placa", "YV2966", ["required"=>"required", "class"=>"form-control required tab2", "id"=>"placa", "placeholder"=>__('Matricula')])}}    
                                        <div class="input-group-append" >
                                            <div title="{{__('Buscar la Aeronave')}}" id="btnFinPlaca" onClick="findPlaca()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>
                                        <!--
                                        <div class="input-group-append" >
                                            <div title="{{__('Crear Nueva Aeronave')}}" onClick="$('#modal-nave').modal('show');" style="cursor:pointer" class="input-group-text"><i class="fa fa-plus"></i></div>
                                        </div>
                                        -->


                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label for="aeronave">{{__('Aeronave')}}</label>
                                    {{Form::text("aeronave", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"aeronave", "placeholder"=>__('Aeronave')])}}    

                                </div>
                            </div>

                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label for="peso_avion">{{__('Peso Max. Kg')}}</label>
                                    {{Form::text("peso_avion", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"peso_avion", "placeholder"=>__('Peso Max. Kg')])}}    

                                </div>
                            </div>

                            <div class="col-sm-12 col-md-4">
                                <div class="form-group">
                                    <label for="maximo_pasajeros">{{__('Max. Pasajeros')}}</label>
                                    {{Form::text("maximo_pasajeros", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"maximo_pasajeros", "placeholder"=>__('Max. Pasajeros')])}}    

                                </div>
                            </div>

                            
                            
                        </div>    
                    </div>    
                    
                    <div class="col-12 col-sm-6">
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Tarifas y Servicios Aplicados</h3>
                                    </div>
              
                                    <div id="bodyListProd" class="card-body">
                
                                        
             
                                    </div>
              
                                </div>
                            </div>
                        </div>
                    </div>    

                      
                    
                    
                    
                    

                      
                    
                    <div class="col-12 col-sm-12">
                        <div class="card card-primary card-outline">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="fas fa-edit"></i>
                                    Servicios Extra
                                </h3>

                                <div class="card-tools">
                                    <button title="{{__('Actualizar Listado de Servicios')}}" onClick="refreshProdServExt()" type="button" class="btn btn-tool" >
                                        <i class="fas fa-sync"></i>
                                    </button>
                                </div>

                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            {{Form::select('prodservicios[]', [], "", [ "multiple"=>"multiple", "class"=>"form-control tab2 ", "id"=>"prodservicios" ])}}

                                        </div>
                                        <!-- /.form-group -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    
                    
                    
                </div>    

            </div>
            
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrincAdd"  class="btn btn-primary" data-dismiss="modal"> <i class="fa fa-undo"></i>  Cerrar</button>
                
                
                <button type="button" onClick="saveDat()"    class="btn btn-primary float-right" ><i class="fa fa-save"></i> Guardar </button>


            </div>
            {{Form::close()}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="modal fade" id="modal-info"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalPrinc" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div id="modalPrincBody" class="modal-body">
                <div class="col-md-12">
                    
                </div>

            </div>
            <div class="modal-footer justify-content-between">

                <button type="button" disabled="disabled" id="closeModalPrinc"  class="btn btn-primary" data-dismiss="modal">Cerrar</button>


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<script type="text/javascript">
    var data1 = @json($data1)
            ;
    var DualList = false;
    function aplicar(obj) {
        if (obj.value != "") {
            status = obj.value.split(":");
            msg = 'Segugo que Desea ' + (status == 0 ? 'Negar' : 'Aprobar') + ' La Solicitud';
            Swal.fire({
                title: 'Esta Seguro?',
                html: "",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#panel_princ").prepend(LOADING);
                    $.get("{{url('/registros/aprobado/')}}/" + obj.value, function (response) {
                        $(".overlay-wrapper").remove();
                        $("#refreshMolulo").click();
                    });
                }
            });
        }
    }

    $(function () {
        
        $('#iconDate').datetimepicker({
            format: 'L',
            ignoreReadonly: true
            
        });
        
        $('#iconDate2').datetimepicker({
            format: 'L',
            ignoreReadonly: true
        });
        
        
        $('#timepicker').datetimepicker({
            format: 'LT'
        });
        
        $("#prodservicios").on("change", function (){
            getProdserv();
        });
        
        const options = {
                translation: {
                    '0': {pattern: /\d/},
                    '1': {pattern: /[1-9]/},
                    '9': {pattern: /\d/, optional: true},
                    '#': {pattern: /\d/, recursive: true},
                    'Y': {pattern: /[Yy]/, fallback: 'Y'},
                    'V': {pattern: /[Vv]/, fallback: 'V'}
                }
            };
        
        $("#matricula").mask('YV9999', options);
        
        
        $('#frm2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        
        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            columns: [

                {data: 'fecha2'},
                {data: 'tipo'},
                {data: 'aeronave.matricula'},
                {data: 'piloto.nombres'},
                {data: function (dat) {
                        return muestraFloat(dat.monto.bs)
                    }},
                {data: 'action'}
                
            ]
            @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        }
        );
    });
    function viewReport(id) {
        $("#modal-info").modal("show");
        $("#modalPrinc").prepend(LOADING);
        $("#modalPrincBody").html("<iframe onload=\"$('#closeModalPrinc').prop('disabled', false); $('.overlay-wrapper').remove();\" name=\"iframePrint\" id=\"iframePrint\" style=\"width:100%; height: 600px; border:none\" src=\"{{url('/registros/ver/')}}/" + id + "\"   ></iframe>");
    }
    function viewImg(id) {
        $("#modal-info").modal("show");
        $("#modalPrinc").prepend(LOADING);
        $("#modalPrincBody").html("<iframe onload=\"$('#closeModalPrinc').prop('disabled', false); $('.overlay-wrapper').remove();\" name=\"iframePrint\" id=\"iframePrint\" style=\"width:100%; height: 600px; border:none\" src=\"{{url('/registros/seeMoney/')}}/" + id + "\"   ></iframe>");
    }

    function addProds(id) {
        $("#modal-info").modal("show");
        $("#modalPrinc").prepend(LOADING);
        $("#modalPrincBody").html("<iframe onload=\"$('#closeModalPrinc').prop('disabled', false); $('.overlay-wrapper').remove();\" name=\"iframePrint\" id=\"iframePrint\" style=\"width:100%; height: 600px; border:none\" src=\"{{url('/registros/addProds/')}}/" + id + "\"   ></iframe>");
    }
    function quitW() {
        $(".overlay-wrapper").remove();
        $("#modal-info").modal("hide");
        Toast.fire({
            icon: "success",
            title: "Servicios Agregados Correctamente"
        });
    }

    function anularVuelo(id) {
        Swal.fire({
            title: 'Esta Seguro que Desea Anular Este Registro?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                $("#panel_princ").prepend(LOADING);
                $.get("{{url('/registros/anular-vuelo')}}/" + id, function (response) {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "success",
                        title: "Vuelo Anulado Correctamente"
                    });
                    $("a.active").click();
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Anular el Vuelo"
                    });
                    $(".overlay-wrapper").remove();
                });
            }
        });
    }


    function correction() {
        $("#modal-info").modal('hide');
    }
    
    
    
    function buscarVuelos(){
        $("#panel_princ").prepend(LOADING);
        $.post("{{route('search_vuelos')}}", $("#frm1").serialize(), function (response){
            
            $(".overlay-wrapper").remove();
            
            $('#example1').DataTable().clear().rows.add(response).draw();
        }, 'json').fail(function (){
            Toast.fire({
                icon: "error",
                title: "Error al Buscar Vuelos Registrados"
            });
            $(".overlay-wrapper").remove();
        });
    }
    
    
    function addVuelos(){
        $("#modal-add").modal("show");
    }
    
    function findPlaca() {
        if ($("#placa").valid()) {
            $("#modalPrincBodyAdd").prepend(LOADING);
            $.get("{{url('get-placa')}}/" + $("#placa").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                    
                    $("#aeronave").val(response.data.nombre);
                    $("#peso_avion").val(response.data.peso_maximo);
                    $("#maximo_pasajeros").val(response.data.maximo_pasajeros);
                    getProdserv();
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Aeronave no Encontrada')}}"
                    });
                    $("#aeronave, #peso_avion, #maximo_pasajeros").val("");


                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar la Placa')}}"
                });
            });
        }
    }
    
    
    function findDato2() {


        if ($("#type_document2, #document2").valid()) {

            $("#modalPrincBodyAdd").prepend(LOADING);
            $.get("{{url('get-data-piloto')}}/" + $("#type_document2").val() + "/" + $("#document2").val(), function (response) {
                $(".overlay-wrapper").remove();
                
                if (response.status == 1) {
                    $("#piloto_id").val(response.data.crypt_id);
                    $("#piloto").val(response.data.full_name);

                    $("#piloto").prop("readonly", true);
                    
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#piloto").prop("readonly", false);
                    $("#piloto_id, #piloto").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#piloto").prop("readonly", true);
                $("#piloto_id, #piloto").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });


        }


    }
    
    
    function refreshProdServExt() {
        if ($("#frm2").valid()) {
            $("#prodservicios").children().remove();

            $("#modalPrincBodyAdd").prepend(LOADING);
            $.post("{{route('get_prod_serv_ext')}}", $("#frm2").serialize(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    for (i = 0; i < response.data.length; i++) {
                        $("#prodservicios").append('<option data-formula="' + response.data[i].formula + '" data-iva="' + response.data[i].iva + '" data-valor_petro="' + response.data[i].valor_petro + '" data-valor_euro="' + response.data[i].valor_euro + '" value="' + response.data[i].crypt_id + '">' + response.data[i].full_descripcion + ' (IVA: ' + muestraFloat(response.data[i].iva) + ') ( ' + response.data[i].moneda + ': ' + muestraFloat(response.data[i].precio) + ')  (Bs.: ' + muestraFloat(response.data[i].bs) + ' ) ( TASA: ' + muestraFloat(response.data[i].tasa) + ')</option>');
                    }

                    if (DualList == true) {
                        $('#prodservicios').bootstrapDualListbox('refresh');
                    } else {
                        DualList = true;

                        $('#prodservicios').bootstrapDualListbox({
                            helperSelectNamePostfix: null,
                            nonSelectedListLabel: 'Servicios Extra Disponibles',
                            selectedListLabel: 'Servicios Extra Asignados',
                            preserveSelectionOnMove: 'moved',
                            moveOnSelect: false,
                            infoText: 'Total {0}',
                            infoTextEmpty: 'Vacio',
                            filterPlaceHolder: 'Filtro',
                            moveSelectedLabel: 'Agregar Selección',
                            moveAllLabel: 'Agregar Todos',
                            removeSelectedLabel: 'Remover Selección',
                            removeAllLabel: 'Remover Todos',
                            filterTextClear: 'Mostrar Todos',
                            infoTextFiltered: "<span class='label label-warning'>Filtrados</span> {0} de {1}"
                                    //nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
                        });


                    }





                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('No se Encontraron Servicios')}}"
                    });
                }
            }).fail(function () {
                $(".overlay-wrapper").remove();

                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Servicios')}}"
                });
            });
        }
    }
    
    function getProdserv(){
        
        if (   $("#frm2").valid() ){
            $.post("{{route('get_presupuesto_tasa')}}", $("#frm2").serialize(), function (response) {
                $("#bodyListProd").html(response);
                
            }).fail(function () {
                
                $("#bodyListProd").html('');
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar el Presupuesto')}}"
                });
            });
        }
    }
    
    function saveDat(){
        if (   $("#frm2").valid() ){
            $.post("{{route('registros.create')}}", $("#frm2").serialize(), function (response) {
                //$("#bodyListProd").html(response);
                //console.log(response);
                
            }).fail(function () {
                
                $("#bodyListProd").html('');
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar el Presupuesto')}}"
                });
            });
        }
    }
    
    
</script>