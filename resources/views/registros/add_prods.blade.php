@extends('layouts.css')
@section('content')
<div class="row">
    {{Form::open(array("route"=>"registros.add_prods_save", "target"=>"iframePdf", "enctype"=>"multipart/form-data",  "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"tabFrm100"))}}
    {{Form::hidden("id", $Reservaciones->crypt_id)}}
    <div class="col-12 col-sm-12">
        <div class="form-group">
            <label for="prodservicios" >Servicios Extra</label>
            <div class="input-group " >
                <select name="prodservicios[]" id="prodservicios" style="min-height: 350px" multiple="multiple" class="form-control " >
                    @foreach($data as $value)
                    <option {{ (in_array($value['crypt_id'], $prods) ? 'selected="selected"':"" ) }} value="{{$value['crypt_id']}}" >
                        {{$value['full_descripcion'] . ' (IVA: ' . muestraFloat($value['iva'],2) . ') ( ' . $value['moneda'] . ': ' . muestraFloat($value['precio']) . ')  (Bs.: ' . muestraFloat($value['bs']) . ' ) ( TASA: ' . muestraFloat($value['tasa']).')' }}
                    </option>
                    @endforeach
                </select>
                <div class="input-group-append" >
                    <div onClick="$('#prodservicios option:selected').removeAttr('selected');" style="cursor:pointer" class="input-group-text"><i class="fa fa-eraser"></i></div>
                </div>

            </div>
            <button class="btn btn-primary" type="button" onclick="addProds()">Guardar</button>
        </div>
        <!-- /.form-group -->
    </div>
    {{Form::close()}}
</div>

<script type="text/javascript">
    function addProds() {

        Swal.fire({
            title: 'Esta Seguro que Desea Agregar Estos Servicios?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                $("#modal-info").modal("hide");
                $.post($("#tabFrm100").attr('action'), $("#tabFrm100").serialize(), function (response) {
                    parent.quitW();
                }).fail(function () {
                    $(".overlay-wrapper").remove();
                });
            }
        });



    }
</script>
@endsection
