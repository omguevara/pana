@extends('layouts.css')
@section('content')
<div class="row">

    <div class="col-md-12">
        <table  id="example1" class="table table-bordered table-hover table-sm">
            <thead>

                <tr>

                    <th>{{__('Origen')}}</th>
                    <td>{{$Reservaciones->origen->nombre}}</td>
                    <th>{{__('Destino')}}</th>
                    <td>{{$Reservaciones->destino->nombre}}</td>
                </tr>
                <tr>
                    <th>{{__('Aeronave')}}</th>
                    <td>{{$Reservaciones->aeronave->nombre}}</td>
                    <th>{{__('Matricula')}}</th>
                    <td>{{$Reservaciones->aeronave->matricula}}</td>
                </tr>
                <tr>    
                    <th>{{__('Piloto')}}</th>
                    <td>{{$Reservaciones->piloto->nombres}}</td>
                    <th>{{__('Monto a Pagar Bs.')}}</th>
                    <td>{{muestraFloat($T)}}</td>


                </tr>
            </thead>
            <tbody>
                @if($Reservaciones->pagada == true)
                <tr>
                    <td align="center" colspan="6"><button class="btn btn-primary" type="button" onclick="emitirFact('{{$Reservaciones->crypt_id }}')">Emitir Factura</button></td>
                </tr>
                @endif
            </tbody>
        </table>
        @if($Reservaciones->pagada == false)
        <div class="row">
            <div class="col-md-4 col-xl-4">
                {{Form::open(array("route"=>"registros.facturar", "target"=>"iframePdfact", "enctype"=>"multipart/form-data",  "class"=>"form-horizontal" ,"method"=>"post", "autocomplete"=>"off", "id"=>"fact1"))}}
                {{Form::hidden('id', $Reservaciones->crypt_id)}}
                <table  id="example1" class="table table-bordered table-hover table-sm">
                    <tbody>

                        @foreach($forma_pagos as $key =>$value)
                        <tr>
                            <td>{{$value->nombre}}</td>
                            <td>{{Form::text("money[".$value->crypt_id."]", "0,00", [ "class"=>"text-right form-control money"])}}</td>

                        </tr>
                        @endforeach
                        <tr>
                            <td style="text-align: center" colspan="2"><button class="btn btn-primary" id="savePay" type="button" onclick="emitirFact('{{$Reservaciones->crypt_id }}')">Guardar y Facturar</button></td>

                        </tr>
                    </tbody>
                </table>
                {{Form::close()}}
            </div>
            <div class="col-md-8 col-xl-8">
                <iframe onload="$('.overlay-wrapper').remove();" name="iframePdfact" id="iframePdfact" style="width:100%; height: 600px; border:none" src=""></iframe>
            </div>
        </div>
        
        @endif


    </div>

</div>

<script type="text/javascript">
    @if ($Reservaciones->pagada == true)
    function emitirFact(id){
        Swal.fire({
            title: 'Esta Seguro que Desea Emitir la Factura?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                $("#body").prepend(LOADING);
                $("body").html("<iframe onload=\"$('.overlay-wrapper').remove();\" name=\"iframePdfact\" id=\"iframePdfact\" style=\"width:100%; height: 600px; border:none\" src=\"{{route('registros.emitir_factura', $Reservaciones->crypt_id )}}\"></iframe>");
            }
        });
    }
    @endif
    
    
    @if ($Reservaciones->pagada == false)
     document.addEventListener('DOMContentLoaded', function () {
    $(".money").maskMoney({"decimal": ",", "thousands": ".", "allowZero": true});
     });
    function emitirFact(id){
        Swal.fire({
            title: 'Esta Seguro que Desea Guardar ?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                $("body").prepend(LOADING);
                $(".money").prop("readonly", true);
                $("#savePay").prop("disabled", true);
                $("#fact1").submit();
            }
        });
    }
    @endif
</script>

@endsection
