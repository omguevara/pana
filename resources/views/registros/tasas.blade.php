

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturar TASAS ')}}</h3>


        </div>
        {{Form::open(array("route"=>"tasas", "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frm1"))}}
        {{Form::hidden("piloto_id", "", ["id"=>"piloto_id"])}}
        {{Form::select('prodservicios[]', [], "", [ "multiple"=>"multiple",  "style"=>"display:none" ,"id"=>"prodservicios" ])}}

        <div id="bodyTasas" class="card-body ">
            <div class="row">
                <div class="col-12 col-sm-6 border-right-2">
                    <div class="row">
                        <div class="col-12 col-sm-5">
                            <div class="form-group ">
                                <label for="hora_inicio" >Hora Salida <span class="right badge badge-info">(Hora Militar)</span></label>

                                <div class="input-group date" id="timepicker" data-target-input="nearest">

                                    {{Form::text("hora_inicio", date("H:m"), ["onkeypress"=>"return false", "required"=>"required", "data-target"=>"#timepicker", "class"=>"form-control tab2 datetimepicker-input required", "id"=>"hora_inicio", "placeholder"=>__('Hora Salida')])}}    
                                    <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                    </div>
                                </div>

                                <!-- /.input group -->
                            </div>
                        </div>

                        <div class="col-12 col-sm-7">
                            <div class="form-group">
                                <label for="destino_id" >Destino</label>

                                {{Form::select('destino_id', $DESTINO, "", ["placeholder"=>"Seleccione",  "class"=>"form-control required", "id"=>"destino_id" ,"required"=>"required"])}}
                                {{Form::hidden('destino_otro')}}

                            </div>
                            <!-- /.form-group -->
                        </div>


                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document2">{{__('Documento del Piloto')}}</label>
                                <div class="input-group " >
                                    <div class="input-group-prepend" >
                                        {{Form::select('type_document2', [ "V"=>"V", "E"=>"E", "P"=>"P"], "", [  "class"=>"form-control tab1", "id"=>"type_document2" ,"required"=>"required"])}}
                                    </div>
                                    {{Form::text("document2", "", ["data-msg-minlength"=>"Mínimo 4 Números", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document2", "placeholder"=>__('Document')])}}    
                                    <div class="input-group-append" >
                                        <div title="{{__('Buscar la Datos')}}" id="btnFinDato2" onClick="findDato1()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                    </div>

                                </div>
                            </div>  
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="piloto">{{__('Piloto')}}</label>
                                {{Form::text("piloto", "", [ "readonly"=>"readonly", "maxlength"=>"100", "required"=>"required", "class"=>"form-control required tab1", "id"=>"piloto", "placeholder"=>__('Nombre del Piloto')])}}    

                            </div>
                        </div>




                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="cant_pasajeros">{{__('Cant. Pasajeros')}}</label>
                                {{Form::text("cant_pasajeros", "", ["minlength"=>"1","data-msg-minlength"=>"Mínimo un Números", "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number tab2", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="placa">{{__('Matricula')}}</label>
                                <div class="input-group " >
                                    {{Form::text("placa", "YV2966", ["required"=>"required", "class"=>"form-control required tab2", "id"=>"placa", "placeholder"=>__('Matricula')])}}    
                                    <div class="input-group-append" >
                                        <div title="{{__('Buscar la Aeronave')}}" id="btnFinPlaca" onClick="findPlaca()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                    </div>
                                    <!--
                                    <div class="input-group-append" >
                                        <div title="{{__('Crear Nueva Aeronave')}}" onClick="$('#modal-nave').modal('show');" style="cursor:pointer" class="input-group-text"><i class="fa fa-plus"></i></div>
                                    </div>
                                    -->


                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="aeronave">{{__('Aeronave')}}</label>
                                {{Form::text("aeronave", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"aeronave", "placeholder"=>__('Aeronave')])}}    

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="peso_avion">{{__('Peso Max. Kg')}}</label>
                                {{Form::text("peso_avion", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"peso_avion", "placeholder"=>__('Peso Max. Kg')])}}    

                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <div class="form-group">
                                <label for="maximo_pasajeros">{{__('Max. Pasajeros')}}</label>
                                {{Form::text("maximo_pasajeros", "", ["required"=>"required", "class"=>"form-control required  tab2", 'readonly'=>'readonly' ,"id"=>"maximo_pasajeros", "placeholder"=>__('Max. Pasajeros')])}}    

                            </div>
                        </div>



                    </div>  

                    <button type="button" onClick="getProdServ()"    class="btn btn-primary " ><i class="fa fa-list"></i> Buscar </button>
                    <button type="button" onClick="Fact()"    class="btn btn-primary float-right " ><i class="fa fa-fax"></i> Facturar </button>

                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{muestraFLoat($DOLAR)}}</h5>
                                <span class="description-text">DOLAR</span>
                            </div>

                        </div>

                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{muestraFLoat($EURO)}}</h5>
                                <span class="description-text">EURO</span>
                            </div>

                        </div>

                        <div class="col-sm-4">
                            <div class="description-block">
                                <h5 class="description-header">{{muestraFLoat($PETRO)}}</h5>
                                <span class="description-text">PETRO</span>
                            </div>

                        </div>

                    </div>

                </div>    

                <div class="col-12 col-sm-6">
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Servicios Aplicados</h3>

                                    <div class="card-tools">

                                        <button onclick="addProdServ()" type="button" class="btn btn-primary btn-sm" >
                                            <i class="fas fa-plus"></i>
                                        </button>
                                    </div>



                                </div>

                                <div id="bodyListProd" class="card-body">



                                </div>
                                <div class="modal-footer justify-content-between">







                                </div>

                            </div>
                        </div>
                    </div>
                </div>    










            </div>    
        </div>
        <div class="modal-footer justify-content-between">






        </div>
        {{Form::close()}}
    </div>
</div>

<div class="modal fade" id="modal-extra"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalPrinc" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Listado de Servicios Adicionales</h4>

            </div>
            <div id="modalPrincBody" class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            {{Form::select('prodservicios_extra[]', [], "", [ "multiple"=>"multiple", "class"=>"form-control  ", "id"=>"prodservicios_extra" ])}}

                        </div>
                        <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                </div>

            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrinc" onClick="$('#prodservicios').children().remove();"  class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button type="button"  id="setModalPrinc"   onClick="aplicServExt()"                            class="btn btn-primary float-right">Aplicar</button>


            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>



<div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Facturar</h4>

            </div>
            {{Form::open(array("route"=>"tasas", "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frm2"))}}
            {{Form::hidden("cliente_id", "", ["id"=>"cliente_id"])}}
            <div id="modalPrincBodyFact" class="modal-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6 border-right">
                        <h4 class="">Datos para la Factura</h4>
                        <div class="row">



                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="document">{{__('Document')}}</label>
                                    <div class="input-group " >
                                        <div class="input-group-prepend" >
                                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                                        </div>
                                        {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required tab1", "id"=>"document", "placeholder"=>__('Document')])}}    
                                        <div class="input-group-append" >
                                            <div title="{{__('Buscar la Datos')}}" id="btnFinDato1" onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>  









                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="razon">{{__('Responsable')}}</label>
                                    {{Form::text("razon", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"razon", "placeholder"=>__('Responsable')])}}    

                                </div>
                            </div>  


                            <div class="col-sm-12 col-md-6">

                                <div class="form-group">
                                    <label for="phone">{{__('Phone')}}</label>
                                    {{Form::text("phone", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone tab1", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                                </div>
                            </div>  



                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label for="correo">{{__('Correo')}}</label>
                                    {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  tab1", "id"=>"correo", "placeholder"=>__('Correo')])}}    

                                </div>  
                            </div> 
                            <div class="col-sm-12 col-md-12">

                                <div class="form-group">
                                    <label for="direccion">{{__('Dirección')}}</label>
                                    {{Form::text("direccion", "", ["readonly"=>"readonly", "required"=>"required", "class"=>"form-control required tab1", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                                </div>  
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12 col-md-6">

                        <div class="row">

                            <div class="col-sm-12 col-md-6 border-right">
                                <h4 class="">Forma de Pago 

                                    <button type="button" onClick="clsFP()" title="{{__('Borrar Formas de Pago')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash"></i></button>
                                </h4>    
                                <div class="row">
                                    @foreach($forma_pagos as $key =>$value)


                                    <div class="col-sm-12 col-md-6">
                                        <div class="form-group">
                                            <label for="">{{$value->nombre}}</label>
                                            {{Form::text("money[".$value->crypt_id."]", "0,00", ["data-reverso"=>$value->reverso_formula, "data-formula"=>$value->calculo_formula, "class"=>"text-right form-control money"])}}

                                        </div>
                                    </div> 


                                    @endforeach
                                </div> 
                            </div> 
                            <div class="col-sm-12 col-md-6 ">
                                <h4 class="">Monto Total</h4>
                                <div class="row">

                                    <div class="col-sm-12 border-bottom">
                                        <div class="row">
                                            <div class="col-md-2"><img src="{{url('/')}}/dist/img/bs.png" class="img-fluid" ></div>
                                            <div class="col-md-4"><h3>Monto.</h3></div>
                                            <div class="col-md-6 text-right"><h3 id="montoTot">{{muestraFloat(0, 2)}}</h3></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 border-bottom mt-2">
                                        <div class="row">
                                            <div class="col-md-2"><img src="{{url('/')}}/dist/img/petro.png" class="img-fluid" ></div>
                                            <div class="col-md-4 "><h4>Ref.</h4></div>
                                            <div class="col-md-6 text-right "><h3 id="montoPetro">{{muestraFloat(0, 2)}}</h3></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 border-bottom mt-2">
                                        <div class="row">
                                            <div class="col-md-2"><img src="{{url('/')}}/dist/img/euro.png" class="img-fluid" ></div>
                                            <div class="col-md-4 "><h4>Ref.</h4></div>
                                            <div class="col-md-6 text-right "><h3 id="montoEuro">{{muestraFloat(0, 2)}}</h3></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 border-bottom mt-2">
                                        <div class="row">
                                            <div class="col-md-2"><i class="fa fa-dollar-sign fa-2x "></i></div>
                                            <div class="col-md-4 "><h4>Ref.</h4></div>
                                            <div class="col-md-6 text-right "><h3 id="montoDolar">{{muestraFloat(0, 2)}}</h3></div>
                                        </div>
                                    </div>








                                </div>
                            </div>
                        </div>
                    </div>    

                </div>

            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrinc" onClick="$('#frm2').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button type="button"  id="setModalPrinc"   onClick="Facturar()"                            class="btn btn-primary float-right">Facturar</button>


            </div>
            {{Form::close()}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">

    var MontoGlobal = 0;
    var MontoGlobalBs = 0;
    /*
     let DOLAR = parseFloat("{{$DOLAR}}", 10);
     let EURO = parseFloat("{{$EURO}}", 10);
     let PETRO = parseFloat("{{$PETRO}}", 10);
     */

    $(document).ready(function () {
        $('#timepicker').datetimepicker({
            format: 'LT'
        });
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $('#frm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        $('#frm2').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
        $(".money").on("focus", function () {
            //MontoGlobalBs



            if (usaFloat(this.value) == 0) {
                T = 0;
                $(".money").each(function () {
                    T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
                });
                // muestra
                v = MontoGlobalBs - T;

                this.value = muestraFloat(eval(getValor(this.dataset.reverso, v)));





            }





        });
        refreshServ();
        $("#prodservicios_extra").on("change", function () {
            $("#prodservicios").children().remove();
            $("#prodservicios_extra >:selected").each(function () {
                $("#prodservicios").append('<option selected="selected"  value="' + this.value + '"></option>');
            });
        });
    });
    function findPlaca() {
        if ($("#placa").valid()) {
            $("#bodyTasas").prepend(LOADING);
            $.get("{{url('get-placa')}}/" + $("#placa").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                    $("#aeronave").val(response.data.nombre);
                    $("#peso_avion").val(response.data.peso_maximo);
                    $("#maximo_pasajeros").val(response.data.maximo_pasajeros);
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Aeronave no Encontrada')}}"
                    });
                    $("#aeronave, #peso_avion, #maximo_pasajeros").val("");
                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#aeronave, #peso_avion, #maximo_pasajeros").prop("readonly", true);
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar la Placa')}}"
                });
            });
        }
    }


    function findDato1() {


        if ($("#type_document2, #document2").valid()) {

            $("#bodyTasas").prepend(LOADING);
            $.get("{{url('get-data-piloto')}}/" + $("#type_document2").val() + "/" + $("#document2").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#piloto_id").val(response.data.crypt_id);
                    $("#piloto").val(response.data.full_name);
                    $("#piloto").prop("readonly", true);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#piloto").prop("readonly", false);
                    $("#piloto_id, #piloto").val("");
                    $("#piloto").focus();
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#piloto").prop("readonly", true);
                $("#piloto_id, #piloto").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


    }

    function getProdServ() {

        if ($("#frm1").valid()) {
            MontoGlobalBs = 0;
            $("#bodyTasas").prepend(LOADING);
            $.post("{{route('get_presupuesto_tasa')}}", $("#frm1").serialize(), function (response) {
                $("#bodyListProd").html(response);
                $(".overlay-wrapper").remove();
            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#bodyListProd").html('');
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar el Presupuesto')}}"
                });
            });
        }
    }

    function addProdServ() {
        if ($("#frm1").valid()) {
            $("#modal-extra").modal("show");
            $("#modalPrinc").prepend(LOADING);
            $("#closeModalPrinc, #setModalPrinc").prop("disabled", true);
            $.post("{{route('get_prod_serv_ext')}}", $("#frm1").serialize(), function (response) {
                $(".overlay-wrapper").remove();
                $("#closeModalPrinc, #setModalPrinc").prop("disabled", false);
                if (response.status == 1) {
                    for (i = 0; i < response.data.length; i++) {
                        $("#prodservicios_extra").append('<option  value="' + response.data[i].crypt_id + '">' + response.data[i].full_descripcion + ' (IVA: ' + muestraFloat(response.data[i].iva) + ') ( ' + response.data[i].moneda + ': ' + muestraFloat(response.data[i].precio) + ')  (Bs.: ' + muestraFloat(response.data[i].bs) + ' ) ( TASA: ' + muestraFloat(response.data[i].tasa) + ')</option>');
                    }
                    $('#prodservicios_extra').bootstrapDualListbox('refresh');
                } else {
                    Toast.fire({
                        icon: "error",
                        title: "{{__('No se Encontraron Servicios')}}"
                    });
                }
            }).fail(function () {
                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Servicios')}}"
                });
            });
        }
    }
    function Fact() {
        if ($("#frm1").valid()) {
            if (MontoGlobalBs > 0) {
                $("#montoTot").html(muestraFloat(MontoGlobalBs));
                $("#montoEuro").html(muestraFloat(MontoGlobalBs / parseFloat("{{$EURO}}", 2)));
                $("#montoPetro").html(muestraFloat(MontoGlobalBs / parseFloat("{{$PETRO}}", 2)));
                $("#montoDolar").html(muestraFloat(MontoGlobalBs / parseFloat("{{$DOLAR}}", 2)));


                $("#modal-fact").modal("show");
            } else {
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Facturar')}}"
                });
            }


        }
    }
    function refreshServ() {
        $('#prodservicios_extra').bootstrapDualListbox({
            helperSelectNamePostfix: null,
            nonSelectedListLabel: 'Servicios Extra Disponibles',
            selectedListLabel: 'Servicios Extra Asignados',
            preserveSelectionOnMove: 'moved',
            moveOnSelect: false,
            infoText: 'Total {0}',
            infoTextEmpty: 'Vacio',
            filterPlaceHolder: 'Filtro',
            moveSelectedLabel: 'Agregar Selección',
            moveAllLabel: 'Agregar Todos',
            removeSelectedLabel: 'Remover Selección',
            removeAllLabel: 'Remover Todos',
            filterTextClear: 'Mostrar Todos',
            infoTextFiltered: "<span class='label label-warning'>Filtrados</span> {0} de {1}"
                    //nonSelectedFilter: 'ion ([7-9]|[1][0-2])'
        });
    }

    function aplicServExt() {

        $("#modal-extra").modal("hide");
        getProdServ();
    }

    function findDato2() {


        if ($("#type_document, #document").valid()) {
            $("#modal-fact").prepend(LOADING);
            $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#cliente_id").val(response.data.crypt_id);
                    $("#razon").val(response.data.razon_social);
                    $("#phone").val(response.data.telefono);
                    $("#correo").val(response.data.correo);
                    $("#direccion").val(response.data.direccion);
                    $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                    $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


    }
    function Facturar() {


        porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
        if (porcentaje >= 98) {

            if ($("#frm2").valid()) {



                Swal.fire({
                    title: 'Esta Seguro que Desea Guardar?',
                    html: "Confirmaci&oacute;n",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#modal-fact").prepend(LOADING);
                        $.post("{{route('tasas')}}", $("#frm1").serialize() + '&' + $("#frm2").serialize(), function (response) {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            if (response.status == 1) {
                                window.open("{{url('view-invoice')}}/" + response.data, '_blank', 'height='+screen.height+',width='+screen.width+',top=0,left=0');
                                $("#modal-fact").modal("hide");
                                $('#frm1, #frm2').trigger("reset");
                                $("#prodservicios, #bodyListProd").html("");
                                MontoGlobalBs = 0;
                            } else {
                            }


                            /*
                             if (response.status == 1) {
                             for (i = 0; i < response.data.length; i++) {
                             $("#prodservicios_extra").append('<option  value="' + response.data[i].crypt_id + '">' + response.data[i].full_descripcion + ' (IVA: ' + muestraFloat(response.data[i].iva) + ') ( ' + response.data[i].moneda + ': ' + muestraFloat(response.data[i].precio) + ')  (Bs.: ' + muestraFloat(response.data[i].bs) + ' ) ( TASA: ' + muestraFloat(response.data[i].tasa) + ')</option>');
                             
                             }
                             $('#prodservicios_extra').bootstrapDualListbox('refresh');
                             } else {
                             Toast.fire({
                             icon: "error",
                             title: "{{__('No se Encontraron Servicios')}}"
                             });
                             }
                             */
                        }).fail(function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar')}}"
                            });
                        });


                    }
                });









            }
        } else {
            Toast.fire({
                icon: "error",
                title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
            });
            $(".money").eq(0).focus();
        }
    }
    function clsFP() {
        $(".money").val("0,00");
    }
    function getMonto() {
        T = 0;
        $(".money").each(function () {
            T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
        });
        return T;
        // muestra

    }

</script>