

<div id="panel_princ" class="col-sm-12 col-md-4 offset-md-4  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Aviación General Tortuga')}}</h3>
            <div class="card-tools">

                <a type="button"  href="{{route('aviacion_general_tortuga')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>

                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>






            </div>

        </div>
        {{Form::open(["route"=>["aviacion_general_tortuga.operacion", $id], "onSubmit"=>"return false", 'id'=>'frm1','autocomplete'=>'Off'])}}
        <div  id="" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12   ">
                    <div class="row">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, $Vuelo->piloto_llegada->crypt_id, ["placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required", "disabled"=>'disabled'])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, $Vuelo->aeronave->crypt_id, [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required", "disabled"=>'disabled'])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Peso Aeronave TON:</b></span>
                            </div>
                            {{Form::text("tn", muestraFloat($Vuelo->aeronave->peso_maximo/1000), ["class"=>"form-control tab2  required uno", "id"=>"tn" ,"readonly"=>"readonly" ,"required"=>"required"  ,"disabled"=>"disabled"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Origen:</b></span>
                            </div>
                            {{Form::select('origen_id', $Aeropuertos, $Vuelo->procedencia2->crypt_id, [ "disabled"=>"disabled", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 uno", "id"=>"origen_id" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Plan de Vuelo:</b></span>
                            </div>
                            {{Form::select('plan_vuelo', ["1"=>"Doble Toque", "2"=>"Estadía"], $Vuelo->plan_vuelo_id, ["disabled"=>"disabled",    "class"=>"form-control  uno ", "id"=>"plan_vuelo" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>
                            {{Form::hidden("fecha_vuelo", $Vuelo->fecha_llegada, ["class"=>"uno", "id"=>"fecha_vuelo"])}}
                            {{Form::text("fecha", showDate($Vuelo->fecha_llegada), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2  required uno", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"disabled"=>"disabled"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", $Vuelo->pax_desembarcados, ["disabled"=>"disabled",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number uno ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                        </div>
                        <div class="col-12 col-sm-12 text-center mt-2">
                            <button type="button" onClick="setGO()"  class="btn btn-primary" ><li  class="fa fa-save"></li>Aplicarr</button>
                        </div>
                    </div>
                </div>
            </div>





        </div>
        {{ Form::close() }} 
        <div class="modal-footer justify-content-between">






        </div>

    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {


    });
    function setGO() {
        Swal.fire({
            title: 'Esta Seguro que Desea Marcar el Vuelo como Realizado?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                $("#modal-fact").modal('hide');
                $("#main-content").prepend(LOADING);
                $.post($("#frm1").attr("action"), $("#frm1").serialize() , function (response) {
                    $(".overlay-wrapper").remove();
                    // Swal.close();



                    $("#modal-fact").modal('hide');
                    $("#main-content").html(response);



                }).fail(function () {
                    $(".overlay-wrapper").remove();
                    Toast.fire({
                        icon: "error",
                        title: "{{__('Error al Guardar')}}"
                    });
                });


            }
        });
    }


</script>


