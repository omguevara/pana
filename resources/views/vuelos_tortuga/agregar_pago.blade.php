



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Pagar Factura')}}</h3>

            <div class="card-tools">
                <button type="button"  onClick="backPrinc()" class="btn btn-tool" ><i class="fas fa-undo"></i>regresar</button>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["facturacion_general_tortuga.agregar_pago_factura", $id],'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-3   ">
                    <div class="row">


                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, $Vuelo->piloto_llegada->crypt_id, ["placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required", "disabled"=>'disabled'])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, $Vuelo->aeronave->crypt_id, [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required", "disabled"=>'disabled'])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Peso Aeronave TON:</b></span>
                            </div>
                            {{Form::text("tn", muestraFloat($Vuelo->aeronave->peso_maximo/1000), ["class"=>"form-control tab2  required uno", "id"=>"tn" ,"readonly"=>"readonly" ,"required"=>"required"  ,"disabled"=>"disabled"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Origen:</b></span>
                            </div>
                            {{Form::select('origen_id', $Aeropuertos, $Vuelo->procedencia2->crypt_id, [ "disabled"=>"disabled", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 uno", "id"=>"origen_id" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Plan de Vuelo:</b></span>
                            </div>
                            {{Form::select('plan_vuelo', ["1"=>"Doble Toque", "2"=>"Estadía"], $Vuelo->plan_vuelo_id, ["disabled"=>"disabled",    "class"=>"form-control  uno ", "id"=>"plan_vuelo" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>

                            {{Form::text("fecha", showDate($Vuelo->fecha_llegada), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2  required uno", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"disabled"=>"disabled"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", $Vuelo->pax_desembarcados, ["disabled"=>"disabled",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number uno ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                        </div>





                    </div>  

                </div>  
                <div   class="col-sm-12 col-md-9   ">

                    <div class="row">

                        <div class="col-sm-12 col-md-12  ">

                            <div class="row">
                                <div class="col-sm-12 border ">
                                    <div class="row">
                                        <div class="col-md-2"><label>Tasa del Euro:</label></div>
                                        <div class="col-md-2 text-right "><label id="totalEuro" >{{muestraFloat($Vuelo->factura_tortuga->monto_moneda_aplicada, 2)}}</label></div>


                                        <div class="col-md-2"><label>Total Euro:</label></div>
                                        <div class="col-md-2 text-right "><label id="totalEuro" >{{muestraFloat($Vuelo->factura_tortuga->getTotal()/$Vuelo->factura_tortuga->monto_moneda_aplicada, 2)}}</label></div>

                                        <div class="col-md-2"><label>Total Bs:</label></div>
                                        <div class="col-md-2 text-right "><label id="totalBs" >{{muestraFloat($Vuelo->factura_tortuga->getTotal(), 2)}}</label></div>
                                    </div>
                                </div>



                            </div>

                            <div class="row">
                                <div class="col-sm-6 border ">
                                    <div class="row">

                                        <div class="col-md-7"><label>Número de Factura:</label></div>
                                        <div class="col-md-5 text-right "><label>{{Form::text("nro_factura", $Vuelo->factura_tortuga->nro_factura, ["disabled"=>"disabled", "class"=>"form-control "])}}</label></div>
                                    </div>
                                </div>
                                <div class="col-sm-6 border ">
                                    <div class="row">

                                        <div class="col-md-7 "><label>Número de Control:</label></div>
                                        <div class="col-md-5 text-right "><label >{{Form::text("nro_control", $Vuelo->factura_tortuga->nro_documento, ["disabled"=>"disabled", "class"=>"form-control "])}}</label></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 border ">
                                    <div class="row">

                                        <div class="col-md-3"><label>Responsable de Pago:</label></div>
                                        <div class="col-md-9">{{$Vuelo->factura_tortuga->cliente->rif.' '.$Vuelo->factura_tortuga->cliente->razon_social }}</div>
                                    </div>
                                </div>

                            </div>
                        </div>



                        <div class="col-sm-12 col-md-12 border bg-info">
                            <h4 class="text-center">Forma de Pago

                                <button type="button" onClick="clsFP()" title="{{__('Borrar Formas de Pago')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                            </h4>    
                            <div class="row">
                                @foreach($forma_pagos as $key =>$value)


                                <div class="col-sm-12 col-md-2 ">
                                    <div class="form-group">
                                        <label for="">{{$value->nombre}}</label>
                                        {{Form::text("money[".$value->crypt_id."]", "0,00", ["data-reverso"=>$value->reverso_formula, "data-formula"=>$value->calculo_formula, "class"=>"text-right form-control money"])}}

                                    </div>
                                </div> 


                                @endforeach
                            </div> 
                        </div> 

                        <div class="col-sm-6 col-md-6 border bg-success">
                            <h4 class="text-center">Referencias

                                <button type="button" onClick="clsPOS()" title="{{__('Borrar Monto')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                            </h4>    
                            <div class="row">
                                @foreach($TAQUILLA->puntos as $key =>$value)
                                @if ($value->activo ==true)

                                <div class="col-sm-12 col-md-4 ">
                                    <div class="form-group">
                                        <label for="">{{$value->banco->nombre.' ('.$value->serial.')'}}</label>
                                        {{Form::text("pos[".$value->crypt_id."]", "", [ "class"=>"text-right form-control pos"])}}

                                    </div>
                                </div> 
                                @endif

                                @endforeach
                            </div> 
                        </div>
                        <div class="col-sm-6 col-md-6 border bg-success">

                            <div class="row">

                                <div class="col-sm-12 col-md-12 ">
                                    <div class="form-group">
                                        <label for="">Observación</label>
                                        {{Form::textarea("observacion", "", [ 'rows' =>4,  "class"=>" form-control "])}}

                                    </div>
                                </div> 



                            </div> 
                        </div>



                    </div>






                </div>

            </div>  
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">

            <button type="button"  id="setPay"   onClick="pagar()"     class="btn btn-primary float-right">Guardar</button>
        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
</div>








<script type="text/javascript">

    var MontoGlobalBs = parseFloat("{{$Vuelo->factura_tortuga->getTotal()}}", 10).toFixed(2);



    $(document).ready(function () {
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $('.number').numeric({negative: false});

        $(".money").on("focus", function () {
            if (usaFloat(this.value) == 0) {
                T = 0;
                $(".money").each(function () {
                    T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
                });
                // muestra
                v = MontoGlobalBs - T;
                this.value = muestraFloat(eval(getValor(this.dataset.reverso, v)));
            }
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });










    });

    function clsFP() {
        $(".money").val("0,00");
    }
    function clsPOS() {
        $(".pos").val("");
    }
    function getMonto() {
        T = 0;
        $(".money").each(function () {
            T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
        });
        return T;
        // muestra

    }

    function pagar() {
        porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
        if (porcentaje >= 98) {





            Swal.fire({
                title: 'Esta Seguro que Desea Procesar el Pago?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {

                    $("#panel_princ").prepend(LOADING);
                    $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize(), function (response) {
                        $(".overlay-wrapper").remove();

                        backPrinc();




                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar')}}"
                        });
                    });


                }
            });

        } else {
            Toast.fire({
                icon: "error",
                title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
            });
            $(".money").eq(0).focus();
        }

    }

    function backPrinc() {
        $("#main-content").html(LOADING);
        AJAX_ACTIVE = true;
        $.get("{{route('facturacion_general_tortuga')}}", function (response) {
            AJAX_ACTIVE = false;
            $("#main-content").html(response);

        }, 'html').fail(function (xhr) {
            $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
            AJAX_ACTIVE = false;
        });
    }

</script>
