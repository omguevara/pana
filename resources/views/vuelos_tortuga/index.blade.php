<style>
    .container{
        max-width: 100%;
    }
</style>

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Aviación General Tortuga')}}</h3>
            <div class="card-tools">
                <button type="button"  onClick="registerTortoise()" class="btn btn-tool" ><i class="fas fa-plus"></i>Registrar Vuelo </button>
                <a href="{{route('aviacion_general_tortuga')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-sync"></i> Actualizar</a>
                <a href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
            </div>

        </div>

        <div style="padding: 0px;" id="" class="card-body ">
            {{Form::open(["route"=>"aviacion_general_tortuga", "onSubmit"=>"return false", 'id'=>'frm1','autocomplete'=>'Off'])}}
            {{Form::hidden("search", 1 )}}
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            
                            <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Fecha Registro:</b></span>
                                </div>
                                {{Form::text("fecha_registro", request('fecha_registro'), ["class"=>"form-control tab2 datetimepicker-input ", "id"=>"fecha_registro" ,"readonly"=>"readonly" , "data-target"=>"#iconDate"])}}
                                <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                    <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>

                            </div>
                            
                            
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            
                            <div class="input-group mt-2"  id="iconDate2" data-target-input="nearest">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Fecha Vuelo:</b></span>
                                </div>
                                {{Form::text("fecha_vuelo", request('fecha_vuelo'), ["class"=>"form-control tab2 datetimepicker-input ", "id"=>"fecha_vuelo" ,"readonly"=>"readonly" ,"data-target"=>"#iconDate2"])}}
                                <div class="input-group-append" data-target="#iconDate2" data-toggle="datetimepicker">
                                    <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>

                            </div>
                           
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Piloto:</b></span>
                                </div>
                                {{Form::select("piloto_id", $Pilotos,  request('piloto_id'), ["class"=>"form-control select2 ", "id"=>"piloto_id", "placeholder"=>__('Select')])}}    
                                
                            </div>
                            
                            
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeronave:</b></span>
                                </div>
                                {{Form::select("matricula_id", $Aeronaves, request('matricula_id'), [ "class"=>"form-control select2  required", "id"=>"matricula_id", "placeholder"=>__('Select')])}}    
                                
                            </div>
                            
                            
                            
                        </div>
                         <div class="col-12 col-sm-12 text-center mt-2">
                            <button type="button" onClick="aplicaFiltros()"  class="btn btn-primary" ><li id="spanLoad" class="fa fa-search"></li>Buscar</button>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <table>
                        <tr>
                            <td style="background-color: #f8d7da; border: solid 1px #f5c6cb; width: 40px"></td>
                            <td>ANULADAS</td>
                        </tr>
                        
                        <tr>
                            <td style="background-color: #daeea8; border: solid 1px #c3e6cb; width: 40px"></td>
                            <td>EN EDICI&Oacute;N</td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffe69b; border: solid 1px #bee5eb; width: 40px"></td>
                            <td>ENVIADAS A RECAUDACIÓN</td>
                        </tr>
                        <tr>
                            <td style="background-color: #d2ecf1; border: solid 1px #bee5eb; width: 40px"></td>
                            <td>FACTURADAS</td>
                        </tr>
                        
                        
                    </table>
                </div>
            </div>
            

            {{ Form::close() }} 

            <table id="example1" class="display compact table-hover">
                <thead>
                    <tr>
                        <th>{{__('Actions')}}</th>
                        <th>{{__('Fecha de Vuelo')}}</th>

                        <th>{{__('Hora LLegada')}}</th>
                        <th>{{__('Hora Salida')}}</th>
                        
                        <th>{{__('Documento Piloto')}}</th>
                        <th>{{__('Matrícula')}}</th>
                        <th>{{__('Plan de Vuelo')}}</th>
                        <th>{{__('Pasajeros')}}</th>



                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="modal-footer justify-content-between">






        </div>

    </div>

<script type="text/javascript">
    var data1 = @json($data)
            ;
    $(document).ready(function () {
        $('#iconDate').datetimepicker({
            format: 'L',
            "maxDate": moment()
        });
        $('#iconDate2').datetimepicker({
            format: 'L'

        });

        $("#fecha_vuelo").val("{{request('fecha_vuelo')}}");
        $("#fecha_registro").val("{{request('fecha_registro')}}");


        $(".select2").select2();

        $('#frm1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });



        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            rowCallback: function (row, data) {
                $(row).addClass(data.class);
                },
            columns: [
                {data: 'action'},
                {data: 'fecha2'},
                {data: function (row) {
                        if (row.plan_vuelo_id == 1) {
                            return row.hora_llegada + " - " + row.tortuga.hora_llegada
                        } else {
                            return row.hora_llegada
                        }
                    }},
                {data: function (row) {
                        if (row.plan_vuelo_id == 1) {
                            return row.hora_salida + " - " + row.tortuga.hora_salida
                        } else {
                            return row.hora_salida
                        }
                    }},
                //{data: 'get_procedencia2.codigo_oaci'},
                {data: 'piloto_llegada.full_name'},
                {data: 'aeronave.matricula'},
                {data: 'plan_vuelo'},
                {data: 'pax_desembarcados'},
            ]
            @if (app()->getLocale() != "en")
                , language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        });

    });


    function registerTortoise(id) {
        $("#main-content").html(LOADING);
        $.get("{{url('aviacion-general-tortuga-store')}}" + (id == null ? "" : "/" + id), function (response) {
            $("#main-content").html(response);
        }).fail(function () {
            Toast.fire({
                icon: "error",
                title: "Error"
            });
        });
    }

    function factTortoise() {
        $("#main-content").html(LOADING);
        $.get("{{route('aviacion_general_tortuga_create')}}", function (response) {
            $("#main-content").html(response);
        }).fail(function () {
            Toast.fire({
                icon: "error",
                title: "Error"
            });

        });
    }
    function listoFacturar(id) {
        Swal.fire({
            title: 'Esta Seguro que Desea Confirmar el Vuelo para su Facturación?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {

                $.get("{{url('lista-facturar')}}/" + id, function (response) {
                    $("#main-content").html(LOADING);
                    $.get("{{route('aviacion_general_tortuga')}}", function (response) {
                        $("#main-content").html(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error"
                        });

                    });
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Aplicar"
                    });
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });

            }
        });
    }
    function anularRegistroVueloTortuga(id) {
        Swal.fire({
            title: 'Esta Seguro que Desea Eliminar El Registro?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {

                $.get("{{url('anular-registro-vuelo')}}/" + id, function (response) {
                    $("#main-content").html(LOADING);
                    $.get("{{route('aviacion_general_tortuga')}}", function (response) {
                        $("#main-content").html(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error"
                        });

                    });
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Anular"
                    });
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });

            }
        });

    }
    function aplicaFiltros() {
        //console.log($('#frm1').attr("action"));
        ///event.preventDefault();
        //$("#main-content").html(LOADING);
        $("#spanLoad").removeClass('fa-search');
        $("#spanLoad").addClass('fa-spinner fa-spin');
        $.get($('#frm1').attr("action"), $('#frm1').serialize(), function (response) {
            $("#main-content").html(response);
        });
        return false;
    }
    
    function verFacturar(id){
        window.open("{{url('view-factura-print')}}/"+id+"/1");
        
    }
    function registroOperacion(id){
        alert("sss");
    }
</script>


</div>