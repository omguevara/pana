



<div id="panel_princ" class="col-sm-12 col-md-8  offset-md-2  mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Registrar Vuelo a la Tortuga')}}</h3>

            <div class="card-tools">

                <button type="button"  onClick="backPrinc()" class="btn btn-tool" ><i class="fas fa-undo"></i>regresar</button>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->

        @if ($id!=null)
        {{Form::open(["route"=>["aviacion_general_tortuga.store", $id],'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @else
        {{Form::open(["route"=>"aviacion_general_tortuga.store",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        @endif

        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-12   ">
                    <div class="row">


                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, (isset($VuelosGenerales['piloto_salida'])? $VuelosGenerales['piloto_salida']['crypt_id']:""), ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div  title="Agregar Nuevo Piloto" onClick="$('#modal-princ-pilotos').modal('show')" class="input-group-text">



                                    <i class="fa fa-plus"></i>

                                </div>
                            </div>
                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, (isset($VuelosGenerales['aeronave'])? $VuelosGenerales['aeronave']['crypt_id']:""), ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required"])}}
                            <div class="input-group-append">
                                <div title="Agregar Nueva Aeronave" onClick="$('#modal-princ-aeronave').modal('show')" class="input-group-text"><i class="fa fa-plus"></i></div>
                            </div>
                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Origen:</b></span>
                            </div>



                            {{Form::select('origen_id', $Aeropuertos, (isset($VuelosGenerales['procedencia2'])? $VuelosGenerales['procedencia2']['crypt_id']:""), ["onChange"=>"$(this).valid()", "placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 uno", "id"=>"origen_id" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Plan de Vuelo:</b></span>
                            </div>
                            {{Form::select('plan_vuelo', ["1"=>"Doble Toque", "2"=>"Estadía"], (isset($VuelosGenerales['plan_vuelo_id'])? $VuelosGenerales['plan_vuelo_id']:"2"), ["placeholder"=>"Seleccione",    "class"=>"form-control  uno ", "id"=>"plan_vuelo" ,"required"=>"required"], [1=>["disabled"=>"disabled"]])}}

                        </div>


                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>
                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2 datetimepicker-input required uno", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", (isset($VuelosGenerales['pax_desembarcados'])? $VuelosGenerales['pax_desembarcados']:""), ["data-msg-required"=>"Campo Requerido",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number uno ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                        </div>

                        <div  class="input-group mt-2 time_estadia">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Tasa?:</b></span>
                            </div>
                            {{Form::select('cobrar_tasa', ["1"=>"Si", "0"=>"No"], (isset($VuelosGenerales['exento_tasa'])? !$VuelosGenerales['exento_tasa']:""), ["class"=>"form-control  uno ", "id"=>"cobrar_tasa" ,"required"=>"required"])}}
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cobrar Dosa?:</b></span>
                            </div>
                            {{Form::select('cobrar_dosa', ["1"=>"Si", "0"=>"No"], (isset($VuelosGenerales['exento_dosa'])? !$VuelosGenerales['exento_dosa']:""), ["class"=>"form-control  uno ", "id"=>"cobrar_dosa" ,"required"=>"required"])}}
                        </div>





                        @if (isset($VuelosGenerales['plan_vuelo_id']))
                        @if ($VuelosGenerales['plan_vuelo_id']==1)
                        @php
                        $time_estadia = "display:none";
                        $time_toque='';
                        @endphp
                        @else
                        @php
                        $time_estadia = '';
                        $time_toque='display:none';
                        @endphp
                        @endif
                        @else
                        @php
                        $time_estadia = '';
                        $time_toque='display:none';

                        @endphp

                        @endif
                        <div style="{{$time_estadia}}" class="input-group mt-2 time_estadia">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Llegada  (Formato 24H):</b></span>
                            </div>

                            {{Form::text("hora_llegada_estadia", (isset($VuelosGenerales['hora_llegada'])? $VuelosGenerales['hora_llegada']:"08:00"), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_llegada_estadia", "placeholder"=>__('Hora Llegada')])}}    
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Salida  (Formato 24H):</b></span>
                            </div>
                            {{Form::text("hora_salida_estadia", (isset($VuelosGenerales['hora_salida'])? $VuelosGenerales['hora_salida']:"16:00"), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_salida_estadia", "placeholder"=>__('Hora Salida')])}}    
                        </div>

                        <div style="{{$time_toque}}" class="input-group mt-2 time_toque">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Llegada  (1er Toque)  (Formato 24H):</b></span>
                            </div>

                            {{Form::text("hora_llegada_toque_1", (isset($VuelosGenerales['hora_llegada'])? $VuelosGenerales['hora_llegada']:date("H:m")), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_llegada_toque_1", "placeholder"=>__('Hora Llegada')])}}    
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Salida  (1er Toque)  (Formato 24H):</b></span>
                            </div>
                            {{Form::text("hora_salida_toque_1", (isset($VuelosGenerales['hora_salida'])? $VuelosGenerales['hora_salida']:date("H:m")), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_salida_toque_1", "placeholder"=>__('Hora Salida')])}}    
                        </div>


                        <div style="{{$time_toque}}" class="input-group mt-2  time_toque">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Llegada (2do Toque) (Formato 24H):</b></span>
                            </div>

                            {{Form::text("hora_llegada_toque_2", (isset($VuelosGenerales['get_tortuga'])? $VuelosGenerales['get_tortuga']['hora_llegada']:date("H:m")), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_llegada_toque_2", "placeholder"=>__('Hora Llegada')])}}    
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Hora Salida (2do Toque) (Formato 24H):</b></span>
                            </div>
                            {{Form::text("hora_salida_toque_2", (isset($VuelosGenerales['get_tortuga'])? $VuelosGenerales['get_tortuga']['hora_salida']:date("H:m")), ["required"=>"required", "class"=>"form-control  required time uno", "id"=>"hora_salida_toque_2", "placeholder"=>__('Hora Salida')])}}    
                        </div>





                    </div>  
                    <div class="rows text-right mt-2 ">
                        {{Form::button(__("Guardar").' <li class="fa fa-caret-right"></li>',  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"next1"])}}    
                    </div>
                </div>  

            </div>  
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>


    <div class="modal fade" id="modal-princ-pilotos"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nuevo Piloto</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"aviacion_general_tortuga.add_piloto",  'id'=>'frmPilotos','autocomplete'=>'Off'])}}
                <div id="modalPrincBodyFact" class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="type_document">{{__('Tipo de Documento')}}</label>
                                {{Form::select('type_document', ["V"=>"V", "E"=>"E", "P"=>"P"], "", [ "class"=>"form-control ", "id"=>"type_document" ,"required"=>"required"])}}

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="document">{{__('Document')}}</label>
                                {{Form::text("document", "", ["required"=>"required", "class"=>"form-control required number", "id"=>"document", "placeholder"=>__('Document')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="name_user">{{__('Name')}}</label>
                                {{Form::text("name_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"name_user", "placeholder"=>__('Name')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="surname_user">{{__('Surname')}}</label>
                                {{Form::text("surname_user", "", ["required"=>"required", "class"=>"form-control required alpha", "id"=>"surname_user", "placeholder"=>__('Surname')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="phone">{{__('Phone')}}</label>
                                {{Form::text("phone", "", [ "required"=>"required",  "class"=>"form-control required  phone", "id"=>"phone", "placeholder"=>__('Phone')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="email">{{__('Email')}}</label>
                                {{Form::text("email", "", [ "class"=>"form-control  email", "id"=>"email", "placeholder"=>__('Email')])}}    

                            </div>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <div class="modal fade" id="modal-princ-aeronave"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

        <div class="modal-dialog modal-md">
            <div id="modalFact" class="modal-content">
                <div class="modal-header">
                    <h4 class="">Registrar Nueva Aeronave</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="fa fa-times"></span>
                    </button>
                </div>

                {{Form::open(["route"=>"aviacion_general_tortuga.add_nave",  'id'=>'frmNave','autocomplete'=>'Off'])}}
                <div  class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-md-6" >
                            <div class="form-group">
                                <label for="matricula">{{__('Matrícula')}}</label>
                                {{Form::text("matricula", "", ["required"=>"required", "class"=>"form-control required", "id"=>"matricula", "placeholder"=>__('Matricula')])}}    

                            </div>
                        </div>  
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="nombre">{{__('Modelo')}}</label>
                                {{Form::text("nombre", "", ["required"=>"required", "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Modelo')])}}    

                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">

                            <div class="form-group">
                                <label for="peso_maximo">{{__('Peso Máximo')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                                {{Form::text("peso_maximo", "", ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}    

                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer justify-content-between">

                    {{Form::button(__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}    
                </div>
                {{ Form::close() }} 

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>



    <script type="text/javascript">

        var TOTAL_FACTURA;



        $(document).ready(function () {
            
            @if (isset($VuelosGenerales['fecha_llegada']))
                $("#fecha").val("{{showDate($VuelosGenerales['fecha_llegada'])}}");
            @endif    
            
            $(".select2").select2();

            $("input[data-bootstrap-switch]").each(function () {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            });

            $(document).on('select2:open', () => {
                document.querySelector('.select2-search__field').focus();
            });
            $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
            $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
            $('.time').inputmask({alias: "datetime", inputFormat: "HH:MM"});
            $('#iconDate').datetimepicker({
                format: 'L'
            });
            $('.number').numeric({negative: false});
            $(".alpha").alphanum({
                allowNumeric: false,
                allowUpper: true,
                allowLower: true

            });
            $("#plan_vuelo").on("change", function () {
                if (this.value == "") {
                    $(".time_toque, .time_estadia").hide();
                } else {
                    if (this.value == 1) {
                        $(".time_toque").show();
                        $(".time_estadia").hide();
                    } else {
                        $(".time_toque").hide();
                        $(".time_estadia").show();
                    }
                }
            });



            $('#frmPrinc1').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPrinc1').valid()) {
                    
                    h1 = parseInt($('#hora_llegada_estadia').val().replace(":", ""));
                    h2 = parseInt($('#hora_salida_estadia').val().replace(":", ""));
                    if (h2>h1){

                        $("#frmPrinc1").prepend(LOADING);
                        $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            if (response.status == 1) {
                                backPrinc();
                                /*
                                 $.get("{{route('aerolineas')}}", function (response) {
                                 $("#main-content").html(response);

                                 }).fail(function (xhr) {
                                 $("#frmPrinc1").find(".overlay-wrapper").remove();
                                 });
                                 */

                            } else {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            }
                            //console.log(response);
                        }).fail(function () {
                            Toast.fire({
                                icon: "error",
                                title: "Error al Guardar"
                            });
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    }else{
                        Toast.fire({
                            icon: "error",
                            title: "La Hora de Salida no Puede ser Menor a la Hora de LLegada"
                        });
                    }
                }
                return false;
            });

            $('#frmPrinc1').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });


            $('#frmPilotos').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });

            $('#frmNave').validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid').addClass("is-valid");
                }
            });




            $('#frmPilotos').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmPilotos').valid()) {
                    $("#frmPilotos").prepend(LOADING);
                    $.post(this.action, $("#frmPilotos").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmPilotos").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#piloto_id").append('<option value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                            $("#piloto_id").val(response.data.id);
                            $("#piloto_id").trigger("reset");
                            $('#frmPilotos').trigger("reset");
                            $("#modal-princ-pilotos").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });

            $('#frmNave').on("submit", function (event) {
                event.preventDefault();
                if ($('#frmNave').valid()) {
                    $("#frmNave").prepend(LOADING);
                    $.post(this.action, $("#frmNave").serialize(), function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        $("#frmNave").find(".overlay-wrapper").remove();
                        if (response.status == 1) {
                            $("#aeronave_id").append('<option  value="' + response.data.id + '">' + response.data.full_nombre + '</option>');
                            $("#aeronave_id").val(response.data.id);
                            $("#aeronave_id").trigger("reset");
                            $('#frmNave').trigger("reset");
                            $("#modal-princ-aeronave").modal('hide');
                        }
                        //console.log(response);
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                    });
                }
                return false;
            });




        });


        function backPrinc() {
            $("#main-content").html(LOADING);
            AJAX_ACTIVE = true;
            $.get("{{route('aviacion_general_tortuga')}}", function (response) {
                AJAX_ACTIVE = false;
                $("#main-content").html(response);

            }, 'html').fail(function (xhr) {
                $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
                AJAX_ACTIVE = false;
            });
        }



    </script>
</div>