



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Relación de Vuelos a la Tortuga')}}</h3>

            <div id="toolID" class="card-tools">
                
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"reporte_vuelos_tortuga",'class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">


            <div class="row">
                <div class="col-12 col-sm-4">
                    <div class="row">

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Rango de Fechas :</b></span>
                            </div>
                            {{Form::text("rango", "", ["data-msg-required"=>"Campo Requerido",   "required"=>"required", "class"=>"form-control required   ", "id"=>"rango", "placeholder"=>__('Fecha Desde - Hasta')])}}    
                            <div class="input-group-append">
                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            
                            <div onClick="setParamsSearch()" style="cursor:pointer" class="input-group-append">
                                <div  class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <table id="example1" class="display compact table-hover">
                    <thead>
                        <tr>
                            <th>{{__('Fecha de Viaje')}}</th>
                            <th>{{__('Matricula')}}</th>

                            <th>{{__('Modelo')}}</th>
                            <th>{{__('Total Aterrizaje')}}</th>
                            <th>{{__('Total Despegue')}}</th>
                            <th>{{__('Pasajeros Nacionales Desembarcados')}}</th>
                            <th>{{__('Pasajeros Nacionales Embarcados')}}</th>
                            <th>{{__('Pasajeros Nacionales Desembarcados')}}</th>
                            <th>{{__('Pasajeros Nacionales Embarcados')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $pax = 0;
                        @endphp
                        @foreach($data as $value)
                            @php
                                $pax += $value->pax_desembarcados;
                            @endphp
                        <tr>
                            <td>{{showDate($value->fecha_llegada)}}</td>
                            <td>{{$value->aeronave->matricula}}</td>
                            <td>{{$value->aeronave->nombre}}</td>
                            <td>1</td>
                            <td>1</td>
                            <td>{{$value->pax_desembarcados}}</td>
                            <td>{{$value->pax_desembarcados}}</td>
                            <td>{{$value->pax_desembarcados}}</td>
                            <td>{{$value->pax_desembarcados}}</td>
                        </tr>
                        @endforeach
                   
                   
                        <tr>
                            <td></td>
                            <td></td>
                            <td>TOTALES</td>
                            <td>{{$data->count()}}</td>
                            <td>{{$data->count()}}</td>
                            <td>{{$pax}}</td>
                            <td>{{$pax}}</td>
                            <td>{{$pax}}</td>
                            <td>{{$pax}}</td>
                        </tr>
                     </tbody>
                </table>
            </div>
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
    
  
    <style>
        .dt-buttons .btn-group{
            float:right;
        }
    </style>        
    







    <script type="text/javascript">
        var data1 =[];
        var dataTableExample1;
        $(document).ready(function () {


            dataTableExample1 =  $("#example1").DataTable({
                "responsive": true, "lengthChange": false, "autoWidth": false,"paging": false, "searching": false,  "ordering": false,
                dom: 'Bfrtip',
                "buttons": [{extend: 'copy', 'text':'Copiar'}, {extend: 'excel', 'text':'EXCEL'}, {extend: 'pdf', 'text':'PDF'}, {extend: 'print', 'text':'Imprimir'}]

                @if (app()->getLocale() != "en")
                    , language: {
                    url: "{{url('/')}}/plugins/datatables/es.json"
                    }
                @endif

            });
            dataTableExample1.buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

            /*
            dataTableExample1 = $('#example1').DataTable({
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
                "paging": false,
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                "autoWidth": false,
                "responsive": true
                @if (app()->getLocale() != "en")
                    , language: {
                    url: "{{url('/')}}/plugins/datatables/es.json"
                    }
                @endif}).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');;
            */
            $('#rango').daterangepicker({
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Guardar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Personalizar",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agosto",
                        "Setiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                },

                "opens": "center"
            });

        });
        function setParamsSearch(){
            dataTableExample1.clear().draw();

            $("#panel_princ").prepend(LOADING);
            $.post("{{route('reporte_vuelos_tortuga')}}", $("#frmPrinc1").serialize() ,function (response){
                $(".overlay-wrapper").remove();
                if (response.status == 1){
                    pax = 0;
                    for(i in response.data){
                        pax += response.data[i].pax_desembarcados;
                        dataTableExample1
                            .row.add( [ showDate(response.data[i].fecha_llegada), response.data[i].aeronave.matricula, response.data[i].aeronave.nombre, 1, 1, response.data[i].pax_desembarcados, response.data[i].pax_desembarcados, response.data[i].pax_desembarcados, response.data[i].pax_desembarcados ] )
                            .draw();
                    }
                    dataTableExample1
                        .row.add( [ '', '', 'TOTALES', response.data.length, response.data.length, pax, pax, pax, pax ] )
                        .draw()

                }


            }, 'json').fail(function (){
                $(".overlay-wrapper").remove();
            });
        }



    </script>


</div>