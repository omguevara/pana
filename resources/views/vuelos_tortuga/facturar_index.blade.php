<style>
    .container{
        max-width: 100%;
    }
</style>

<div id="panel_princ" class="col-sm-12 col-md-12  mt-3">
    
    
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Aviación General Tortuga Facturación')}}</h3>
            <div class="card-tools">

              
                  





            </div>

        </div>

        <div style="padding: 0px;" id="" class="card-body ">
            
            {{Form::open(["route"=>"facturacion_general_tortuga", "onSubmit"=>"return false", 'id'=>'frm1','autocomplete'=>'Off'])}}
            {{Form::hidden("search", 1 )}}
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            
                            <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Fecha Registro:</b></span>
                                </div>
                                {{Form::text("fecha_registro", request('fecha_registro'), ["class"=>"form-control tab2 datetimepicker-input ", "id"=>"fecha_registro" ,"readonly"=>"readonly" , "data-target"=>"#iconDate"])}}
                                <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                    <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>

                            </div>
                            
                            
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            
                            <div class="input-group mt-2"  id="iconDate2" data-target-input="nearest">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Fecha Vuelo:</b></span>
                                </div>
                                {{Form::text("fecha_vuelo", request('fecha_vuelo'), ["class"=>"form-control tab2 datetimepicker-input ", "id"=>"fecha_vuelo" ,"readonly"=>"readonly" ,"data-target"=>"#iconDate2"])}}
                                <div class="input-group-append" data-target="#iconDate2" data-toggle="datetimepicker">
                                    <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>

                            </div>
                           
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Piloto:</b></span>
                                </div>
                                {{Form::select("piloto_id", $Pilotos,  request('piloto_id'), ["class"=>"form-control select2 ", "id"=>"piloto_id", "placeholder"=>__('Select')])}}    
                                
                            </div>
                            
                            
                            
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="input-group mt-2">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><b>Aeronave:</b></span>
                                </div>
                                {{Form::select("matricula_id", $Aeronaves, request('matricula_id'), [ "class"=>"form-control select2  required", "id"=>"matricula_id", "placeholder"=>__('Select')])}}    
                                
                            </div>
                            
                            
                            
                        </div>
                         <div class="col-12 col-sm-12 text-center mt-2">
                            <button type="button" onClick="aplicaFiltros()"  class="btn btn-primary" ><li id="spanLoad" class="fa fa-search"></li>Buscar</button>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <table>
                        
                        <tr>
                            <td style="background-color: #ffe69b; border: solid 1px #bee5eb; width: 40px"></td>
                            <td>LISTA PARA FACTURAR</td>
                        </tr>
                        <tr>
                            <td style="background-color: #d2ecf1; border: solid 1px #bee5eb; width: 40px"></td>
                            <td>FACTURADAS</td>
                        </tr>
                        
                        
                    </table>
                </div>
            </div>
            

            {{ Form::close() }} 
            
            <table id="example1" class="display compact table-hover">
                <thead>
                    <tr>
                        <th>{{__('Actions')}}</th>
                        <th>{{__('Fecha de Vuelo')}}</th>
                        
                        
                        <th>{{__('Aeropuerto (OACI)')}}</th>
                        <th>{{__('Documento Piloto')}}</th>
                        <th>{{__('Matrícula')}}</th>
                        <th>{{__('Serv. Cobrado')}}</th>
                        <th>{{__('Plan de Vuelo')}}</th>
                        <th>{{__('Pasajeros')}}</th>



                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="modal-footer justify-content-between">






        </div>

    </div>
</div>
<script type="text/javascript">
       var data1 = @json($data)
            ;
    $(document).ready(function (){
       
       
        $('#iconDate').datetimepicker({
            format: 'L',
            "maxDate": moment()
        });
        $('#iconDate2').datetimepicker({
            format: 'L'

        });

        $("#fecha_vuelo").val("{{request('fecha_vuelo')}}");
        $("#fecha_registro").val("{{request('fecha_registro')}}");


        $(".select2").select2();
        
        var table1 = $('#example1').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            data: data1,
            rowCallback: function (row, data) {
                $(row).addClass(data.class);
            },
            columns: [
                {data: 'action'},
                {data: 'fecha2'},
                
                {data: 'get_procedencia2.codigo_oaci'},
                {data: 'piloto_llegada.full_name'},
                {data: 'aeronave.matricula'},
                {data: 'cobrado'},
                {data: 'hours'},
                {data: 'pax_desembarcados'},
            ]
            @if (app()->getLocale() != "en")
                ,language: {
                url: "{{url('/')}}/plugins/datatables/es.json"
                }
            @endif

        }
        );
        
    });
    
    
    
    
    
    function listoFacturar(id){
        Swal.fire({
            title: 'Esta Seguro que Desea Confirmar el Vuelo para su Facturación?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                
                $.get("{{url('lista-facturar')}}/"+id, function (response){
                    $("#main-content").html(LOADING);
                    $.get("{{route('aviacion_general_tortuga')}}", function (response){
                         $("#main-content").html(response);
                    }).fail(function (){
                        Toast.fire({
                            icon: "error",
                            title: "Error"
                        });

                    });
                }).fail(function (){
                    Toast.fire({
                        icon: "error",
                        title: "Error al Aplicar"
                    });
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });

            }
        });
    }
    function anularFacturaTortuga(id){
        Swal.fire({
            title: 'Esta Seguro que Desea Eliminar El Registro?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {
                
                $.get("{{url('anular-factura-vuelo')}}/"+id, function (response){
                    $("#main-content").html(LOADING);
                    $.get("{{route('facturacion_general_tortuga')}}", function (response){
                         $("#main-content").html(response);
                    }).fail(function (){
                        Toast.fire({
                            icon: "error",
                            title: "Error"
                        });

                    });
                }).fail(function (){
                    Toast.fire({
                        icon: "error",
                        title: "Error al Anular"
                    });
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });

            }
        });

    }
    function factVueloTort(id){
        
        $("#panel_princ").prepend(LOADING);
        $.post("{{url('/facturacion-general-tortuga')}}/"+id, "_method=PUT&_token={{csrf_token()}}" ,function (response){
        
            $("#main-content").html(response);
          
        }).fail(function (){
            
        })
    }
    function aplicaFiltros() {
        //console.log($('#frm1').attr("action"));
        ///event.preventDefault();
        //$("#main-content").html(LOADING);
        $("#spanLoad").removeClass('fa-search');
        $("#spanLoad").addClass('fa-spinner fa-spin');
        $.get($('#frm1').attr("action"), $('#frm1').serialize(), function (response) {
            $("#main-content").html(response);
        });
        return false;
    }
    function agregarPago(id){
        $("#panel_princ").prepend(LOADING);
        $.get("{{url('agregar-pago-factura')}}/"+id, function (response) {
            $("#main-content").html(response);
        });
        return false;
    }
    
   function backOper(id) {
        Swal.fire({
            title: 'Esta Seguro que Desea Retornar el Registro a Operaciones?',
            html: "Confirmaci&oacute;n",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si'
        }).then((result) => {
            if (result.isConfirmed) {

                $.get("{{url('regresar-operacion')}}/" + id, function (response) {
                    $("#main-content").html(LOADING);
                    $.get("{{route('facturacion_general_tortuga')}}", function (response) {
                        $("#main-content").html(response);
                    }).fail(function () {
                        Toast.fire({
                            icon: "error",
                            title: "Error"
                        });

                    });
                }).fail(function () {
                    Toast.fire({
                        icon: "error",
                        title: "Error al Aplicar"
                    });
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });

            }
        });
    }
</script>


