



<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Facturar Vuelo a la Tortuga')}}</h3>

            <div class="card-tools">

                <button type="button"  onClick="backPrinc()" class="btn btn-tool" ><i class="fas fa-undo"></i>regresar</button>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(['class'=>'form-horizontal',   'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">
                <div  class="col-sm-12 col-md-3   ">
                    <div class="row">


                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Piloto:</b></span>
                            </div>
                            {{Form::select('piloto_id', $Pilotos, $Vuelo->piloto_llegada->crypt_id, ["placeholder"=>"Seleccione", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control  select2 uno", "id"=>"piloto_id" ,"required"=>"required", "disabled"=>'disabled'])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Aeronave:</b></span>
                            </div>
                            {{Form::select('aeronave_id', $Aeronaves, $Vuelo->aeronave->crypt_id, [ "placeholder"=>"Seleccione",   "data-msg-required"=>"Campo Requerido",  "class"=>"form-control  select2 uno", "id"=>"aeronave_id" ,"required"=>"required", "disabled"=>'disabled'])}}

                        </div>
                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Peso Aeronave TON:</b></span>
                            </div>
                            {{Form::text("tn", muestraFloat($Vuelo->aeronave->peso_maximo/1000), ["class"=>"form-control tab2  required uno", "id"=>"tn" ,"readonly"=>"readonly" ,"required"=>"required"  ,"disabled"=>"disabled"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Origen:</b></span>
                            </div>
                            {{Form::select('origen_id', $Aeropuertos, "", [ "disabled"=>"disabled", "data-msg-required"=>"Campo Requerido",    "class"=>"form-control required  select2 uno", "id"=>"origen_id" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Plan de Vuelo:</b></span>
                            </div>
                            {{Form::select('plan_vuelo', ["1"=>"Doble Toque", "2"=>"Estadía"], $Vuelo->plan_vuelo_id, ["disabled"=>"disabled",    "class"=>"form-control  uno ", "id"=>"plan_vuelo" ,"required"=>"required"])}}

                        </div>


                        <div class="input-group mt-2"  >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Fecha de Vuelo:</b></span>
                            </div>
                            {{Form::hidden("fecha_vuelo", $Vuelo->fecha_llegada, ["class"=>"uno", "id"=>"fecha_vuelo"])}}
                            {{Form::text("fecha", showDate($Vuelo->fecha_llegada), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control tab2  required uno", "id"=>"fecha" ,"readonly"=>"readonly" ,"required"=>"required"  ,"disabled"=>"disabled"])}}

                        </div>

                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Cant. Pasajeros:</b></span>
                            </div>
                            {{Form::text("cant_pasajeros", $Vuelo->pax_desembarcados, ["disabled"=>"disabled",  "maxlength"=>"3", "required"=>"required", "class"=>"form-control required number uno ", "id"=>"cant_pasajeros", "placeholder"=>__('Cantidad')])}}    

                        </div>





                    </div>  

                </div>  
                <div   class="col-sm-12 col-md-5   ">

                    <h2>PROFORMA</h2>
                    <table style="width:100%" id="datFactura">
                        <tr>
                            <td><b>Categoría</b></td>
                            <td>{{$AeropuertosAux->categoria->nombre.' '.muestraFloat($AeropuertosAux->categoria->porcentaje*100)}}%</td>
                            <td>Tasa del Euro</td>
                            <td>{{muestraFloat($VALOR_EURO)}}</td>
                        </tr>
                        <tr>
                            <td style="width:10%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>CANT.</b></td>
                            <td style="width:50%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>DESCRIPCI&Oacute;N</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMENC</b></td>
                            <td style="width:20%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TOTAL EURO</b></td>
                        </tr>


                    </table>





                </div>
                <div  class="col-sm-12 col-md-4   ">
                    <h4 class="">Responsable de Pago</h4>
                    <div class="row">




                        <div  class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>N&deg; de Factura:</b></span>
                            </div>

                            {{Form::text("nro_factura", showCode($TAQUILLA->numero_factura+1), ["required"=>"required", "class"=>"form-control  required ", "id"=>"nro_control", "placeholder"=>__('Número de Factura')])}}    
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>N&deg; de Control:</b></span>
                            </div>
                            {{Form::text("nro_control", showCode($TAQUILLA->numero_control+1), ["required"=>"required", "class"=>"form-control  required ", "id"=>"nro_control", "placeholder"=>__('Número de Control')])}}    
                        </div>


                        <div class="input-group mt-2">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Documento:</b></span>
                            </div>
                            {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", ["style"=>"max-width:80px", "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                            {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "onBlur"=>"findDato2()" ,"minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required dos", "id"=>"document", "placeholder"=>__('Document')])}}    
                            <div class="input-group-append" >
                                <div title="{{__('Buscar la Datos')}}" id="btnFinDato1"  onClick="findDato2()" style="cursor:pointer" class="input-group-text"><i class="fa fa-search"></i></div>
                            </div>
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>Responsable:</b></span>
                            </div>

                            {{Form::text("razon", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos alpha", "id"=>"razon", "placeholder"=>__('Responsable')])}}    
                        </div>
                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                            </div>

                            {{Form::text("phone", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required phone dos", "id"=>"phone", "placeholder"=>__('Phone')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Correo')}}:</b></span>
                            </div>

                            {{Form::text("correo", "", ["readonly"=>"readonly",  "class"=>"form-control email  dos", "id"=>"correo", "placeholder"=>__('Correo')])}}    
                        </div>

                        <div class="input-group mt-2 ">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                            </div>

                            {{Form::text("direccion", "", ["data-msg-required"=>"Campo Requerido", "readonly"=>"readonly", "required"=>"required", "class"=>"form-control required dos", "id"=>"direccion", "placeholder"=>__('Dirección')])}}    
                        </div>






                    </div>

                    <div class="row ">
                        <div class="col-md-4 text-left mt-2 ">
                            {{Form::button(__("Proforma").' <li class="fa fa-print"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "id"=>"proF"])}}    
                        </div>
                        <div class="col-md-4 text-center mt-2 ">
                            {{Form::button(__("Enviar @").' <li class="fa fa-mail-bulk"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "onClick"=>"SendEmail()"])}}    
                        </div>
                        <div class="col-md-4 text-right mt-2 ">
                            {{Form::button(__("Facturar").' <li class="fa fa-file-invoice-dollar"></li>',  ["type"=>"button", "class"=>"btn btn-primary", "onClick"=>"Facturar()"])}}    
                        </div>
                    </div>    
                </div>

            </div>  
            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">


        </div>
        <!-- /.card -->
        {{ Form::close() }} 
    </div>
</div>



<div class="modal fade" id="modal-princ-emails"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        <div  class="modal-content">
            <div class="modal-header">
                <h4 class="">Enviar Proforma por Correo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>
            {{Form::open([ 'id'=>'frmSendMail', "route"=>["facturacion_general_tortuga.send_prof_email", $id],'autocomplete'=>'Off'])}}
            {{Form::hidden('type_document', "", [ "id"=>"type_document" ])}}
            {{Form::hidden("document", "", ["id"=>"document"])}}    
            {{Form::hidden("razon", "", ["id"=>"razon"])}}                        
            {{Form::hidden("phone", "", ["id"=>"phone"])}}    
            {{Form::hidden("correo", "", ["id"=>"correo"])}}    
            {{Form::hidden("direccion", "", [ "id"=>"direccion"])}}    
            <div  class="modal-body">

                <div class="row">
                    <input type="text" id="emails_to_send" name="emails_to_send" class="form-control" value='["{{Auth::user()->email}}"]'>
                </div>
            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Enviar"),  ["onClick"=>"sendProfEmail()", "type"=>"button", "class"=>"btn btn-primary", ])}}    
            </div>
            {{ Form::close() }} 

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<div class="modal fade" id="modal-princ-prodservs"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-md">
        <div  class="modal-content">
            <div class="modal-header">
                <h4 class="">Agregar Servicios Extras</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="fa fa-times"></span>
                </button>
            </div>

            {{Form::open(["route"=>"aviacion_general_tortuga",  'id'=>'frmServ','autocomplete'=>'Off'])}}
            <div  class="modal-body">

                <div class="row">

                </div>
            </div>
            <div class="modal-footer justify-content-between">

                {{Form::button(__("Aceptar"),  ["type"=>"button", "class"=>"btn btn-primary", ])}}    
            </div>
            {{ Form::close() }} 

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-fact"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

    <div class="modal-dialog modal-xl">
        <div id="modalFact" class="modal-content">
            <div class="modal-header">
                <h4 class="">Facturar</h4>

            </div>
            {{Form::open(array( "enctype"=>"multipart/form-data", "onsubmit"=>"return false",   "class"=>"form-horizontal",  "autocomplete"=>"off", "id"=>"frmAddFact2"))}}

            <div id="modalPrincBodyFact" class="modal-body">






                <div class="row">

                    <div class="col-sm-12 col-md-12  ">

                        <div class="row">
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-3"><label>Total Euro:</label></div>
                                    <div class="col-md-3 text-right "><label id="totalEuro" >0,00</label></div>

                                    <div class="col-md-3"><label>Total Bs:</label></div>
                                    <div class="col-md-3 text-right "><label id="totalBs" >0,00</label></div>
                                </div>
                            </div>
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-7"><label>Número de Factura:</label></div>
                                    <div class="col-md-5 text-right "><label>{{Form::text("nro_factura", showCode($TAQUILLA->numero_factura+1), ["required"=>"required", "class"=>"form-control "])}}</label></div>
                                </div>
                            </div>
                            <div class="col-sm-4 border ">
                                <div class="row">

                                    <div class="col-md-7 "><label>Número de Control:</label></div>
                                    <div class="col-md-5 text-right "><label >{{Form::text("nro_control", showCode($TAQUILLA->numero_control+1), ["required"=>"required", "class"=>"form-control "])}}</label></div>
                                </div>
                            </div>


                        </div>
                    </div>



                    <div class="col-sm-12 col-md-12 border bg-info">
                        <h4 class="text-center">Forma de Pago

                            <button type="button" onClick="clsFP()" title="{{__('Borrar Formas de Pago')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                        </h4>    
                        <div class="row">
                            @foreach($forma_pagos as $key =>$value)


                            <div class="col-sm-12 col-md-2 ">
                                <div class="form-group">
                                    <label for="">{{$value->nombre}}</label>
                                    {{Form::text("money[".$value->crypt_id."]", "0,00", ["data-reverso"=>$value->reverso_formula, "data-formula"=>$value->calculo_formula, "class"=>"text-right form-control money"])}}

                                </div>
                            </div> 


                            @endforeach
                        </div> 
                    </div> 

                    <div class="col-sm-6 col-md-6 border bg-success">
                        <h4 class="text-center">Referencias

                            <button type="button" onClick="clsPOS()" title="{{__('Borrar Monto')}}" class="btn btn-outline-danger  btn-sm float-right"><i class="fa fa-trash fa-2x"></i></button>
                        </h4>    
                        <div class="row">
                            @foreach($TAQUILLA->puntos as $key =>$value)
                            @if ($value->activo ==true)

                            <div class="col-sm-12 col-md-4 ">
                                <div class="form-group">
                                    <label for="">{{$value->banco->nombre.' ('.$value->serial.')'}}</label>
                                    {{Form::text("pos[".$value->crypt_id."]", "", [ "class"=>"text-right form-control pos"])}}

                                </div>
                            </div> 
                            @endif

                            @endforeach
                        </div> 
                    </div>
                    <div class="col-sm-6 col-md-6 border bg-success">

                        <div class="row">

                            <div class="col-sm-12 col-md-12 ">
                                <div class="form-group">
                                    <label for="">Observación</label>
                                    {{Form::textarea("observacion", "", [ 'rows' =>4,  "class"=>" form-control "])}}

                                </div>
                            </div> 



                        </div> 
                    </div>



                </div>




            </div>
            <div class="modal-footer justify-content-between">

                <button type="button"  id="closeModalPrinc" onClick="$('#frm2').trigger('reset');" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
                <button type="button"  id="setModalPrinc"   onClick="Facturar()"                            class="btn btn-primary float-right">Facturar</button>


            </div>
            {{Form::close()}}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

{{Form::open(["route"=>"proforma", "method"=>"post" ,"target"=>"_blank" ,'id'=>'frmAux','autocomplete'=>'Off'])}}
{{Form::hidden("prof_id", $Vuelo->crypt_id, ["id"=>"prof_id"])}}

{{Form::hidden("prof_type_document", '', ["id"=>"prof_type_document"])}}
{{Form::hidden("prof_document", '', ["id"=>"prof_document"])}}
{{Form::hidden("prof_razon", '', ["id"=>"prof_razon"])}}
{{Form::hidden("prof_phone", '', ["id"=>"prof_phone"])}}
{{Form::hidden("prof_direccion", '', ["id"=>"prof_direccion"])}}
{{Form::close()}}






<script type="text/javascript">

    var TOTAL_FACTURA;



    $(document).ready(function () {
        $(".select2").select2();

        $(document).on('select2:open', () => {
            document.querySelector('.select2-search__field').focus();
        });
        $('#emails_to_send').multiple_emails({position: "bottom"});
        $(".alpha").alphanum({
            allowNumeric: false,
            allowUpper: true,
            allowLower: true

        });

        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});
        $('.time').inputmask({alias: "datetime", inputFormat: "HH:MM"});

        $('.number').numeric({negative: false});

        $("#modal-fact").on('shown.bs.modal', function () {
            $("#totalEuro").html(muestraFloat(TOTAL_FACTURA, 2));
            $("#totalBs").html(muestraFloat(TOTAL_FACTURA * VALOR_EURO, 2));
        });
        $(".money").on("focus", function () {
            if (usaFloat(this.value) == 0) {
                T = 0;
                $(".money").each(function () {
                    T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
                });
                // muestra
                v = MontoGlobalBs - T;
                this.value = muestraFloat(eval(getValor(this.dataset.reverso, v)));
            }
        });


        /*
         $("#next2").on("click", function () {
         if ($(".uno").valid() && $(".dos").valid()) {
         $("#modal-fact").modal();
         
         } else {
         
         }
         });
         */

        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                $.post(this.action, $("#frmPrinc1").serialize(), function (response) {
                    Toast.fire({
                        icon: response.type,
                        title: response.message
                    });
                    if (response.status == 1) {
                        $.get("{{route('aerolineas')}}", function (response) {
                            $("#main-content").html(response);

                        }).fail(function (xhr) {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        });
                    } else {
                        $("#frmPrinc1").find(".overlay-wrapper").remove();
                    }
                    //console.log(response);
                }).fail(function () {
                    $("#frmPrinc1").find(".overlay-wrapper").remove();
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });


        $("#proF").on("click", function () {
            if ($(".uno").valid() && $(".dos").valid()) {
                $("#prof_type_document").val($("#type_document").val());
                $("#prof_document").val($("#document").val());
                $("#prof_razon").val($("#razon").val());
                $("#prof_phone").val($("#phone").val());
                $("#prof_direccion").val($("#direccion").val());


                $("#frmAux").submit();
            }

        });










        $('#document').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                findDato2();
            }
        });

        getCalculos();

    });


    function backPrinc() {
        $("#main-content").html(LOADING);
        AJAX_ACTIVE = true;
        $.get("{{route('facturacion_general_tortuga')}}", function (response) {
            AJAX_ACTIVE = false;
            $("#main-content").html(response);

        }, 'html').fail(function (xhr) {
            $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
            AJAX_ACTIVE = false;
        });
    }

    function findDato2() {


        if ($("#type_document, #document").valid()) {
            $("#frmPrinc1").prepend(LOADING);
            $.get("{{url('get-data-cliente')}}/" + $("#type_document").val() + "/" + $("#document").val(), function (response) {
                $(".overlay-wrapper").remove();
                if (response.status == 1) {
                    $("#cliente_id").val(response.data.crypt_id);
                    $("#razon").val(response.data.razon_social);
                    $("#phone").val(response.data.telefono);
                    $("#correo").val(response.data.correo);
                    $("#direccion").val(response.data.direccion);
                    $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                    Toast.fire({
                        icon: "success",
                        title: "{{__('Datos Encontrados')}}"
                    });
                } else {
                    Toast.fire({
                        icon: "warning",
                        title: "{{__('Datos no Encontrados, Registrelos')}}"
                    });
                    $("#razon").focus();
                    $("#razon, #phone, #correo, #direccion").prop("readonly", false);
                    $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                }


            }).fail(function () {
                $(".overlay-wrapper").remove();
                $("#razon, #phone, #correo, #direccion").prop("readonly", true);
                $("#cliente_id, #razon, #phone, #correo, #direccion").val("");
                Toast.fire({
                    icon: "error",
                    title: "{{__('Error al Consultar los Datos')}}"
                });
            });
        }


    }

    function clsFP() {
        $(".money").val("0,00");
    }

    function clsPOS() {
        $(".pos").val("");
    }

    function getMonto() {
        T = 0;
        $(".money").each(function () {
            T += eval(getValor(this.dataset.formula, usaFloat(this.value)));
        });
        return T;
        // muestra

    }



    /*   
     function Facturar() {
     
     
     porcentaje = (getMonto().toFixed(2) * 100 / MontoGlobalBs).toFixed(2);
     if (porcentaje >= 98) {
     
     if ($("#frmAddFact2").valid()) {
     
     
     
     Swal.fire({
     title: 'Esta Seguro que Desea Procesar la Factura?',
     html: "Confirmaci&oacute;n",
     icon: 'question',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: 'Si'
     }).then((result) => {
     if (result.isConfirmed) {
     $("#modal-fact").modal('hide');
     $("#main-content").prepend(LOADING);
     $.post($("#frmAddFact2").attr("action"), $("#frmPrinc1").serialize() + '&' + $("#frmAddFact2").serialize(), function (response) {
     $(".overlay-wrapper").remove();
     // Swal.close();
     
     
     
     $("#modal-fact").modal('hide');
     $("#main-content").html(response);
     
     
     
     }).fail(function () {
     $(".overlay-wrapper").remove();
     Toast.fire({
     icon: "error",
     title: "{{__('Error al Guardar')}}"
     });
     });
     
     
     }
     });
     }
     } else {
     Toast.fire({
     icon: "error",
     title: "{{__('El Monto Abonado es Inferior al Monto a Pagar')}}"
     });
     $(".money").eq(0).focus();
     }
     }
     */

    function Facturar() {




        if ($(".uno").valid() && $(".dos").valid()) {

            Swal.fire({
                title: 'Esta Seguro que Desea Procesar la Factura?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {
                    $("#modal-fact").modal('hide');
                    $("#main-content").prepend(LOADING);
                    $.post($("#frmPrinc1").attr("action"), $("#frmPrinc1").serialize() + '&' + $("#frmAddFact2").serialize(), function (response) {
                        $(".overlay-wrapper").remove();
                        // Swal.close();
                        $("#modal-fact").modal('hide');
                        $("#main-content").html(response);



                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar')}}"
                        });
                    });


                }
            });
        }
    }

    function getCalculos() {
        if ($(".uno").valid()) {
            $("#panel_princ").prepend(LOADING);
            /*
             let params = "";
             $(".uno").each(function () {
             params += this.id + "=" + this.value + "&";
             
             });
             params += $("[name=_token]:first").serialize();
             */
            //$.post("{{route('facturacion_general_tortuga.get_serv_fact')}}", params, function (response) {
            $.get("{{route('facturacion_general_tortuga.get_serv_fact_2', $id)}}", function (response) {

                $(".overlay-wrapper").remove();
                Toast.fire({
                    icon: response.type,
                    title: response.message
                });
                if (response.status == 1) {
                    $(".filaFactura").remove();
                    sum = 0;
                    base_imponible = 0;
                    excento = 0;
                    iva = 0;
                    for (i in response.data) {
                        //console.log(response.data[i]['categoria_aplicada']);

                        if (response.data[i]['codigo'] == '2.5.1.1') {//SI ES TASA
                            sum += parseFloat(response.data[i]['precio'], 10);
                            mult = response.data[i]['precio'];
                            cant = "{{$Vuelo->pax_desembarcados}}";
                        } else {

                            sum += parseFloat(response.data[i]['cant'] * response.data[i]['precio']);
                            mult = response.data[i]['cant'] * response.data[i]['precio'];
                            cant = response.data[i]['cant'];


                        }
                        if (response.data[i]['iva_aplicado'] > 0) {
                            base_imponible += mult;
                            iva += mult * response.data[i]['iva_aplicado'] / 100;
                        } else {
                            excento += parseFloat(mult, 10);

                        }

                        //sum += response.data[i]['cant'] * response.data[i]['precio'] * response.data[i]['categoria_aplicada'] ;
                        tabla = '<tr title="' + response.data[i]['formula2'] + '" class="filaFactura">';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + cant + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + response.data[i]['full_descripcion'] + ' ' + response.data[i]['iva2'] + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; ">' + (cant + ' ' + response.data[i]['nomenclatura'] || '') + '</td>';
                        tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right ">' + muestraFloat(mult) + '</td>';
                        tabla += '</tr>';
                        $("#datFactura").append(tabla);
                    }
                    TOTAL_FACTURA = sum;
                    MontoGlobalBs = sum * VALOR_EURO;
                    tabla = '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>BASE IMPONIBLE</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(base_imponible) + '</b></td>';
                    tabla += '</tr>';

                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>EXCENTO</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(excento) + '</b></td>';
                    tabla += '</tr>';



                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>SUB TOTAL</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(sum) + '</b></td>';
                    tabla += '</tr>';

                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000;  text-align:right" colspan="3"><b>IVA</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px dotted #000; text-align:right "><b>' + muestraFloat(iva) + '</b></td>';
                    tabla += '</tr>';

                    tabla += '<tr class="filaFactura">';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000;  text-align:right" colspan="3"><b>TOTAL</b></td>';
                    tabla += '<td  style="font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000; text-align:right "><b>' + muestraFloat(sum + iva) + '</b></td>';
                    tabla += '</tr>';

                    $("#datFactura").append(tabla);
                }


            }, 'json').fail(function () {
                $(".overlay-wrapper").remove();
            });

        } else {

        }
    }
    ;

    function SendEmail() {
        if ($('#frmPrinc1').valid()) {
            $("#modal-princ-emails").modal("show");
        }
    }

    function sendProfEmail() {

        cant = JSON.parse($("#emails_to_send").val());
        if (cant.length > 0) {

            Swal.fire({
                title: 'Esta Seguro que Desea Enviar el Correo?',
                html: "Confirmaci&oacute;n",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si'
            }).then((result) => {
                if (result.isConfirmed) {

                    $('#frmSendMail #type_document').val($('#frmPrinc1 #type_document').val());
                    $("#frmSendMail #document").val($("#frmPrinc1 #document").val());
                    $("#frmSendMail #razon").val($("#frmPrinc1 #razon").val());
                    $("#frmSendMail #phone").val($("#frmPrinc1 #phone").val());
                    $("#frmSendMail #correo").val($("#frmPrinc1 #correo").val());
                    $("#frmSendMail #direccion").val($("#frmPrinc1 #direccion").val());


                    $("#modal-princ-emails").prepend(LOADING);
                    $.post($("#frmSendMail").attr("action"), $("#frmSendMail").serialize(), function () {
                        $(".overlay-wrapper").remove();
                        $("#modal-princ-emails").modal("hide");
                        Toast.fire({
                            icon: "success",
                            title: "{{__('proforma Enviada Correctamente')}}"
                        });





                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar')}}"
                        });
                    });


                }
            });
        } else {
            Toast.fire({
                icon: "error",
                title: "{{__('Debe Agregar los Correos')}}"
            });
        }

    }

</script>
