<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}: Videos</title>

        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">

        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">

    </head>

<style>
    /* Estilos de etiqueta*/
    body {
        background-color: #141625;
    }

    table {
        background: rgb(26, 40, 233);
        width: 50%;
        margin-top: 0%;
        border-collapse: collapse;
        text-align: center;

        tr:nth-child(odd) {
            background-color: rgb(26, 40, 233);
        }

        tr td:nth-child(odd) {
            background-color: rgb(1, 0, 87);
        }

        tr td:nth-child(even) {
            background-color: rgb(25, 0, 255);
        }

        th:nth-child(odd) {
            background-color: rgb(95, 86, 0);
        }

        th:nth-child(even) {
            background-color: rgb(119, 117, 20);
        }
    }

    th {
        background-color: rgba(28, 0, 151, 0.99);
        height: 35px;
        border-bottom: 1px solid rgb(210, 220, 250);
        color: rgb(251, 255, 0);
    }

    td {
        color: white;
        height: 30px;
        border: 0.5px solid rgba(240, 240, 240, 10);
    }

    tfoot {
        font-weight: bold;
    }

    th:hover {
        background-color: rgba(20, 20, 20, 90);
    }

    tr:hover {
        background-color: rgb(177, 177, 177);
    }

    /* Estililos de clases*/
    .PrecioTotal:hover,
    .CantidadTotal:hover {
        color: rgb(230, 50, 50);
    }

    .Cabecera {
        background-color: rgba(28, 0, 151, 0.99);
        margin-top: 0%;
        height: 50px;
        width: 100%;
        text-align: center;
        color: white;
    }

    a {
        text-decoration: none;
        color: white;
        font-size: 14pt;
    }

    footer {
        margin-top: 40px;
        text-align: center;
    }

    .imagen-titulo {
        display: flex;
        align-items: center;
    }

    .imagen-titulo img {
        margin-right: 10px;
    }

    .imagen-titulo img {
        border-radius: 50%;
        width: 30px;
        height: 30px;
    }
    .videos{
        position: absolute;
width: 100vw;
background-color: #FFFFFF;
height: 100vh;
z-index: 1000;
    }
</style>
<div id="videos" style="display:none" class="videos">
    <video width="100%"  height="100%" id="video" autoplay >
       <source src="" type="video/mp4">
    </video>
</div>
<div class="header">
    <header class="Cabecera">
        <h1>{{ $Aeropuerto->full_nombre }}</h1>
    </header>
</div>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        @php
            $active = false;
        @endphp
        @if (count($movimientosNacL)>0)
            @php
                $active = true;
            @endphp
        <div class="carousel-item active">
            <table id="movimientos-table" class="table table-striped table-hover" style="width: 100%; font-size: 14pt;">
              <div class="table-responsive">
                <header class="imagen-titulo">
                    <h3 class="text-white"><i class="fas fa-flag"></i>
                        NACIONAL / LLEGADA
                    </h3>
                </header>
                <thead>
                    <tr>

                        <th><b>HORA</b></th>
                        <th><b>ORIGEN</b></th>
                        <th><b>AEROLÍNEA</b></th>
                        <th><b>N° VUELO</b></th>
                        <th><b>PUERTA</b></th>
                        <th><b>OBSERVACIONES</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($movimientosNacL as $nacionalL)
                        <tr>

                            <td><b>{{ $nacionalL->hora }}</b></td>
                            <td><b>{{ $nacionalL->getOrigenDestino->nombre }}</b></td>
                            <td><b><img width="200px" height="50px" src="{{ url('dist/img/aerolineas/aerolinea_' . $nacionalL->getAerolinea->id . '.' . $nacionalL->getAerolinea->imagen) }}"
                                    alt="{{ $nacionalL->getAerolinea->razon_social }}" /></td>
                            <td><b>{{ $nacionalL->numero_vuelo }}</b></td>
                            <td><b>{{ $nacionalL->puerta }}</td>
                            <td><b>{{ $nacionalL->observaciones }}</b></td>
                        </tr>
                    @endforeach
                      </div>           

            </table>
        </div>
        @endif
        @if (count($movimientosNacS)>0)
            
        <div class="carousel-item {{($active == true ? '':'active')}}">
            <table id="movimientos-table" class="table table-striped table-hover" style="width: 100%; font-size: 14pt;">
                <header class="imagen-titulo">
                    <h3 class="text-white"><i class="fas fa-flag"></i>
                        NACIONAL / SALIDA
                    </h3>
                </header>
                <thead>
                    <tr>

                        <th><b>HORA</b></th>
                        <th><b>DESTINO</b></th>
                        <th><b>AEROLÍNEA</b></th>
                        <th><b>N° VUELO</b></th>
                        <th><b>PUERTA</b></th>
                        <th><b>OBSERVACIONES</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($movimientosNacS as $nacionalS)
                        <tr>

                            <td><b>{{ $nacionalS->hora }}</b></td>
                            <td><b>{{ $nacionalS->getOrigenDestino->nombre }}</b></td>
                            <td><img width="200px" height="50px" src="{{ url('dist/img/aerolineas/aerolinea_' . $nacionalS->getAerolinea->id . '.' . $nacionalS->getAerolinea->imagen) }}"
                                    alt="{{ $nacionalS->getAerolinea->razon_social }}" /></td>
                            <td><b>{{ $nacionalS->numero_vuelo }}</b></td>
                            <td><b>{{ $nacionalS->puerta }}</b></td>
                            <td><b>{{ $nacionalS->observaciones }}</b></td>
                        </tr>
                    @endforeach


            </table>
        </div>
        @php
            $active = true;
        @endphp
        @endif
        @if (count($movimientosInterL)>0)
        <div class="carousel-item {{($active == true ? '':'active')}}">
            <table id="movimientos-table" class="table table-striped table-hover" style="width: 100%; font-size: 14pt;">
                <header class="imagen-titulo">
                    <h3 class="text-white"><i class="fas fa-globe"></i>
                        INTERNACIONAL / LLEGADA
                    </h3>
                </header>
                <thead>
                    <tr>

                        <th><b>HORA</b></th>
                        <th><b>ORIGEN</b></th>
                        <th><b>AEROLÍNEA</b></th>
                        <th><b>N° VUELO</b></th>
                        <th><b>PUERTA</b></th>
                        <th><b>OBSERVACIONES</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($movimientosInterL as $internacionalL)
                        <tr>

                            <td><b>{{ $internacionalL->hora }}</b></td>
                            <td><b>{{ $internacionalL->getOrigenDestino->nombre }}<b></td>
                            <td><b><img width="200px" height="50px" src="{{ url('dist/img/aerolineas/aerolinea_' . $internacionalL->getAerolinea->id . '.' . $internacionalL->getAerolinea->imagen) }}"
                                    alt="{{ $internacionalL->getAerolinea->razon_social }}" /></td>
                            <td><b>{{ $internacionalL->numero_vuelo }}</b></td>
                            <td><b>{{ $internacionalL->puerta }}</b></td>
                            <td><b>{{ $internacionalL->observaciones }}</b></td>
                        </tr>
                    @endforeach


            </table>
        </div>
        @php
            $active = true;
        @endphp
        @endif
        @if (count($movimientosInterS)>0)
        <div class="carousel-item {{($active == true ? '':'active')}}">
            <table id="movimientos-table" class="table table-striped table-hover" style="width: 100%; font-size: 14pt;">
                <header class="imagen-titulo">
                    <h3 class="text-white"><i class="fas fa-globe"></i>
                        INTERNACIONAL / SALIDA
                    </h3>
                </header>
                <thead>
                    <tr>

                        <th><b>HORA</b></th>
                        <th><b>DESTINO</b></th>
                        <th><b>AEROLÍNEA</b></th>
                        <th><b>N° VUELO</b></th>
                        <th><b>PUERTA</b></th>
                        <th><b>OBSERVACIONES</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($movimientosInterS as $internacionalS)
                        <tr>

                            <td><b>{{ $internacionalS->hora }}</b></td>
                            <td><b>{{ $internacionalS->getOrigenDestino->nombre }}</b></td>
                            <td><img width="200px" height="50px" src="{{ url('dist/img/aerolineas/aerolinea_' . $internacionalS->getAerolinea->id . '.' . $internacionalS->getAerolinea->imagen) }}"
                                    alt="{{ $internacionalS->getAerolinea->razon_social }}" /></td>
                            <td><b>{{ $internacionalS->numero_vuelo }}</b></td>
                            <td><b>{{ $internacionalS->puerta }}</b></td>
                            <td><b>{{ $internacionalS->observaciones }}</b></td>
                        </tr>
                    @endforeach


            </table>
        </div>
         @endif
    </div>

</div>



<!-- jQuery -->
<script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
<script src="{{url('/')}}/plugins/jquery/popper.js"></script>
<script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript">
    var VIDEO=document.getElementById('video');
    var I = 0;
    var INTERVAL_VIDEO=null;
    
    var LISTA_VIDEOS = @json($Videos)
            ;
    $(document).ready(function() {
        
        VIDEO.addEventListener('ended',closeVideo,false);
        
        
        
         
        
        
  movimientosTable = null;

  // Seleccionar el carrusel
  var carousel = $('#carouselExampleControls');

  // Función para mover el carrusel al siguiente slide
  function moveNext() {
    carousel.carousel('next');
  }

        // Ejecutar la función moveNext() cada 5 segundos
        setInterval(moveNext, 30000);
        
        INTERVAL_VIDEO = setInterval(nextVideo, 15000);

    });
    function closeVideo(){
        $("#videos").fadeOut("fast", function (){
            INTERVAL_VIDEO = setInterval(nextVideo, 15000);
        });
    }
    
    function nextVideo(){
        I++;
        if (typeof(LISTA_VIDEOS[I])=='undefined'){
            I=0;
        }
        VIDEO.src= '{{url("dist/videos/")}}/'+LISTA_VIDEOS[I];
        $("#videos").slideDown("fast", function (){
            clearInterval(INTERVAL_VIDEO);
            VIDEO.play();
        });
        
        
    }
    
    
</script>
</html>
