<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Llegadas y Salidas')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#modal-info').modal('show')" class="btn btn-tool" ><i class="fas fa-info"></i></a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
            <div id="card-body-main" class="card-body ">
                <div class="row">
                    <div class="col-sm-12 col-md-12 mt-4" >

                        <table id="movimientos-table" class="display compact table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>ACCIONES</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>FECHA</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>HORA</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>TIPO DE OPERACION</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AEROPUERTO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>RECORRIDO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AEROLÍNEA</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AEROLÍNEA ORIGEN / DESTINO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>N° VUELO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>PUERTA</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>OBSERVACIONES</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center"></div>
            <!-- /.card -->
    </div>
    <!-- Modal Registrar Movimientos -->
    <div class="modal fade" id="movimientoModal" tabindex="-1" data-backdrop="static" aria-labelledby="movimientoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar movimientos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{Form::open(["route"=>"pantallas",'class'=>'form-horizontal', 'id'=>'frmMovimiento','autocomplete'=>'Off'])}}
                            {{Form::hidden("_method", "POST", [ "id"=>"_method"])}}
                            {{Form::hidden("movimiento_id", "", [ "id"=>"movimiento_id"])}}
                            <div class="col-md-12">
                                <div class="row">
                                    @if (Auth::user()->aeropuerto_id == null)
                                        <div class="col-md-12">
                                            <div class="input-group mt-2"  >
                                                <div class="input-group-prepend">
                                                    <span id="labelAerolinea" class="input-group-text"><b>Aeropuerto:</b></span>
                                                </div>
                                                {{Form::select('aeropuerto_id', $AeropuertosUser, "" , ["placeholder"=>(count($AeropuertosUser)>1 ? 'Seleccione':null),"data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-12 ">
                                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                            <div class="input-group-prepend">
                                                <span  class="input-group-text"><b>Fecha:</b></span>
                                            </div>
                                            {{Form::text("fecha", date("d/m/Y"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input required ", "id"=>"fecha" ,"required"=>"required"  ,"data-target"=>"#iconDate"])}}
                                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div  class="col-md-12 ">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span  class="input-group-text"><b>Hora: <small class="badge badge-success"><i class="far fa-clock"></i> 24H</small>:</b></span>
                                            </div>
                                            {{Form::text("hora", date("H:i"), ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  timer required ", "id"=>"hora" ,"required"=>"required"])}}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div  class="col-md-12">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelTipoOperacion" class="input-group-text"><b>Tipo Operación:</b></span>
                                            </div>
                                            {{Form::select('tipo_operacion', ['1' =>"Llegada / Arrival", '2' => 'Salida / Departure' ], "" , ["data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"tipo_operacion" ,"required"=>"required"])}}
                                        </div>
                                    </div>
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelTipoRecorrido" class="input-group-text"><b>Tipo de Recorrido:</b></span>
                                            </div>
                                            {{Form::select('tipo_recorrido', ["" => "Seleccione", "1" => "Nacional / National", "2" => "Internacional / International"], "" , ["data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"tipo_recorrido" ,"required"=>"required"])}}
                                            
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelAerolinea" class="input-group-text"><b>Aerolínea:</b></span>
                                            </div>
                                            {{Form::select('aerolinea', $Aerolineas, "" , ["data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"aerolinea" ,"required"=>"required"])}}
                                        </div>
                                    </div>
                                    
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelNroVuelo" class="input-group-text"><b>N° Vuelo:</b></span>
                                            </div>
                                            {{Form::text("nro_vuelo", "", ["data-msg-required"=>"Campo Requerido",  "required"=>"required", "class"=>"form-control required ", "id"=>"nro_vuelo", "placeholder"=>__('N° Vuelo')])}}                                                
                                        </div>
                                    </div>
                                    
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelOrigDest" class="input-group-text"><b>Aeropuerto Origen:</b></span>
                                            </div>
                                            {{Form::select('aeropuerto_origen_destino', $OrigenDestino, "" , ["placeholder"=>(count($OrigenDestino)>1 ? 'Seleccione':null), "data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"aeropuerto_origen_destino" ,"required"=>"required"])}}
                                            
                                        </div>
                                    </div>
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelPuerta" class="input-group-text"><b>Puerta:</b></span>
                                            </div>
                                            {{Form::text("puerta", "", ["data-msg-required"=>"Campo Requerido",  "required"=>"required", "class"=>"form-control required ", "id"=>"puerta", "placeholder"=>__('Puerta')])}}    
                                        </div>
                                    </div>
                                    
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelObservaciones" class="input-group-text"><b>Observaciones:</b></span>
                                            </div>
                                            {{Form::select('observaciones', $observaciones, "" , ["class"=>"form-control  select2 ", "id"=>"observaciones" ])}}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-2">
                                        <a class="btn btn-success mx-auto" id="send">Guardar</a>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }} 
                    </div>
                </div>               
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            movimientosTable = null;
            const options = [];
            // $(".select2").select2();
            $('#iconDate').datetimepicker({
                format: 'L',
                maxDate: moment(),
                minDate: moment().add(-9, 'days')
            });
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy", "clearIncomplete": true});
            $('.timer').inputmask({alias: "datetime", inputFormat: "HH:MM", "clearIncomplete": true});
            
            // $(document).on('select2:open', () => {
            //     document.querySelector('.select2-search__field').focus();
            // });

            @if (isset($error))
                Toast.fire({
                    icon: "{{ $error['type'] }}",
                    title: "{{ $error['message'] }}"
                });          
            @endif

            $('.close').on("click", function (){
                $("#frmMovimiento")[0].reset();
                $("#movimiento_id").val('');
            })
            $("#send").on("click", function(e){
                e.preventDefault();
                $("#frmMovimiento").submit();
            });
            movimientosTable = $('#movimientos-table').DataTable({
                "paging": true,
                pageLength: 10,
                "deferRender": true,
                "lengthChange": false,
                searching: true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,          
                serverSide: true,
                processing: true,
                dom: 'Bfrtip',
                buttons: {
                    buttons: [
                        {
                            text: '<i class="far fa-chart-bar"></i> REGISTRAR MOVIMIENTO',
                            action: function (e, dt, node, config) {
                                // clearFrom();
                                $("#movimientoModal").modal('show');
                                $(".modal-title").text("Registrar movimiento");
                                $('#send').html("<i class='fas fa-save'></i> Guardar");
                                $("#_method").val('POST');
                            }
                        }
                    ],
                    dom: {
                        button: {className: "btn btn-info"},

                    }
                },
                ajax: {
                    'url': "{{route('pantallas.obtener_lista')}}",
                    'dataType': 'json',
                    'type': 'GET',
                    /* "data":
                        function (d) {
                            d.cliente_id = $('#cliente_id').val(),
                            d.estatus_id = $('#estatus_id').val(),
                            d.rango_fecha = $('#rango_fecha').val()
                        }, */
                },
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'boton'},
                    {data: 'fecha2'},
                    {data: 'hora' },
                    {data: 'tipo_operacion' },
                    {data: 'get_aeropuerto.nombre' },
                    {data: 'tipo_recorrido' },
                    {data: 'get_aerolinea.razon_social'},
                    {data: 'get_origen_destino.nombre'},
                    {data: 'numero_vuelo'},
                    {data: 'puerta'},
                    {data: 'observaciones'},
                  
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });

            $("#frmMovimiento").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    event.preventDefault();

                    $("#movimientoModal").prepend(LOADING);
                    $.post("{{ route('pantallas') }}", $('#frmMovimiento').serialize(), function (response) {
                        $(".overlay-wrapper").remove();                        
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        movimientosTable.draw();
                    }).fail(function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error en la conexión')}}"
                        });
                    });
                }
            });
            $("#tipo_operacion").on("change", function(){
                if ($(this).val() == "1") {
                    $("#labelOrigDest").html("<b>Aeropuerto Origen</b>");
                } else {
                    $("#labelOrigDest").html("<b>Aeropuerto Destino</b>");
                }
            })
            // Combobox aeropuerto
            $('#aeropuerto_id').change(function(){
                $('#aeropuerto_id option:selected').each(function() {
                    var aeropuerto_id = $(this).val();
                    $('#aerolinea').empty();
                    $('#aerolinea').append('<option value="" selected>Seleccione</option>');
                    if($(this).val() != 0){
                        $.get("{{ route('aeropuertos.search_airlines') }}", { aeropuerto_id:aeropuerto_id }, function(response){
                            if (response.type === 'success') {
                                let options = [];
                                $.each(response.data, function(index, value){
                                    let option = '<option value="' +value.crypt_id +'">'+ value.full_razon_social +'</option>';
                                    options.push(option);
                                });
                                const html = options.join('');
                                $('#aerolinea').html(html);
                            }else if(response.type === 'info'){
                                Toast.fire({
                                    icon: response.type,
                                    title: response.message
                                });
                            }                            
                        });
                    }
                });
            });
        });
        function editMovimiento(id, fecha, hora, tipo_operacion, aerolinea, nro_vuelo, origen_destino, puerta, obs, aeropuerto, recorrido){
            $("#frmMovimiento").attr({
                action: '{{ route("pantallas") }}',
                method: 'PUT'
            });
            $(".modal-title").text("Editar movimiento");
            $("#_method").val('PUT');
            $("#movimiento_id").val(id);
            $('#aeropuerto_id').val(aeropuerto).prop('selected', true);
            $('#fecha').val(showDate(fecha));
            $('#hora').val(hora);
            $('#tipo_operacion').val(tipo_operacion).prop('selected', true);
            $('#aerolinea').val(aerolinea).prop('selected', true);
            $('#tipo_recorrido').val(recorrido).prop('selected', true);
            $('#nro_vuelo').val(nro_vuelo);
            $('#aeropuerto_origen_destino').val(origen_destino).prop('selected', true);
            $('#puerta').val(puerta);
            $('#observaciones').val(obs).prop('selected', true);
            $('#send').html("<i class='fas fa-save'></i> Actualizar");
        }

       
    </script>

</div>