<div id="panel_princ" class="col-sm-12 col-md-12   mt-1">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Control de Videos')}}</h3>

            <div class="card-tools">
                <a type="button"  href="javascript:void(0)" onClick="$('#modal-info').modal('show')" class="btn btn-tool" ><i class="fas fa-info"></i></a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
            <div id="card-body-main" class="card-body ">
                <div class="row">
                    <div class="col-sm-12 col-md-12 mt-4" >

                        <table id="movimientos-table" class="display compact table-hover" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>ACCIONES</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>AEROPUERTO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>COMERCIO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>FECHA DE EXPIRACIÓN</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>NOMBRE DEL VIDEO</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>¿ES PUBLICIDAD?</b></th>
                                    <th style="width:14%;  font-family: ROBOTO; font-size: 12px; border-bottom: 1px solid #000"><b>ESTATUS</b></th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center"></div>
            <!-- /.card -->
    </div>
    <!-- Modal Registrar movimientos -->
    <div class="modal fade" id="videoModal" tabindex="-1" data-backdrop="static" aria-labelledby="videoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cargar video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        {{Form::open(["route"=>"pantallas.lista_videos",'class'=>'form-horizontal', 'id'=>'frmVideos','autocomplete'=>'Off', 'files' => true])}}
                            {{Form::hidden("_method", "POST", [ "id"=>"_method"])}}
                            {{Form::hidden("video_id", "", [ "id"=>"video_id"])}}
                            <div class="col-md-12">
                                <div class="row">
                                    @if (Auth::user()->aeropuerto_id == null)
                                        <div class="col-md-12">
                                            <div class="input-group mt-2"  >
                                                <div class="input-group-prepend">
                                                    <span id="labelAerolinea" class="input-group-text"><b>Aeropuerto:</b></span>
                                                </div>
                                                {{Form::select('aeropuerto_id', $AeropuertosUser, "" , ["placeholder"=>(count($AeropuertosUser)>1 ? 'Seleccione':null),"data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"aeropuerto_id" ,"required"=>"required"])}}
                                            </div>
                                        </div>
                                    @endif
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelTipoAnuncio" class="input-group-text"><b>Es publicidad?</b></span>
                                            </div>
                                            {{Form::select('tipo_anuncio', ["1"=>"SÍ", "0"=>"NO"], "" , [ "data-msg-required"=>"Campo Requerido", "class"=>"form-control required ", "required" => "required", "id"=>"tipo_anuncio" ])}}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-12 ">
                                        <div class="input-group mt-2"  id="iconDate" data-target-input="nearest">
                                            <div class="input-group-prepend">
                                                <span  class="input-group-text"><b>Fecha de Expiración:</b></span>
                                            </div>
                                            {{Form::text("fecha_expiracion", "dd/mm/yyyy", ["data-msg-required"=>"Campo Requerido", "class"=>"form-control  datetimepicker-input", "id"=>"fecha_expiracion"  ,"data-target"=>"#iconDate"])}}
                                            <div class="input-group-append" data-target="#iconDate" data-toggle="datetimepicker">
                                                <div  class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="col-md-12 mt-2">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelComercio" class="input-group-text"><b>Comercio:</b></span>
                                            </div>
                                            {{Form::text("comercio", "BAER", ["data-msg-required"=>"Campo Requerido",  "class"=>"form-control ", "id"=>"comercio","readonly" => "readonly", "placeholder"=>__('Comercio')])}}                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div  class="col-md-12">
                                        <div class="input-group mt-2"  >
                                            <div class="input-group-prepend">
                                                <span id="labelTipoOperacion" class="input-group-text"><b>Video:</b></span>
                                            </div>
                                            {{Form::file('video', ["data-msg-required"=>"Campo Requerido",  "class"=>"form-control required  select2 ", "id"=>"video" ,"required"=>"required", "accept"=>"video/*"])}}
                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-2">
                                        <a class="btn btn-success mx-auto" id="send">Guardar</a>
                                    </div>
                                </div>
                            </div>
                        {{ Form::close() }} 
                    </div>
                </div>               
            </div>
        </div>
    </div>
    <!-- Modal Reproducir video -->
    <div class="modal fade" id="playVideoModal" tabindex="-1" data-backdrop="static" aria-labelledby="playVideoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="videomodal-title">Video: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-12" id="video-content">
                        <video id="video_play" width="100%" height="100%" autoplay="" loop="" muted="">
                            <source src="" type="video/mp4">
                            Tu navegador no soporta la etiqueta de video.
                        </video>
                    </div>
                </div>               
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            videosTable = null;
            // $(".select2").select2();
            $('#iconDate').datetimepicker({
                format: 'L',
                minDate: moment().endOf('year')
            });
            $('.date_time').inputmask({alias: "datetime", inputFormat: "dd/mm/yyyy", "clearIncomplete": true});
            
            // $(document).on('select2:open', () => {
            //     document.querySelector('.select2-search__field').focus();
            // });

            $('.close').on("click", function (){
                $("#frmVideos")[0].reset();
                $("#video_id").val('');
            })
            $("#send").on("click", function(e){
                e.preventDefault();
                $("#frmVideos").submit();
            });
            videosTable = $('#movimientos-table').DataTable({
                "paging": true,
                pageLength: 10,
                "deferRender": true,
                "lengthChange": false,
                searching: true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,          
                serverSide: true,
                processing: true,
                dom: 'Bfrtip',
                buttons: {
                    buttons: [
                        {
                            text: '<i class="fas fa-cloud-upload-alt"></i> CARGAR VIDEO',
                            action: function (e, dt, node, config) {
                                // clearFrom();
                                $("#videoModal").modal('show');
                                $(".modal-title").text("Cargar video");
                                $('#send').html("<i class='fas fa-save'></i> Guardar");
                                $("#_method").val('POST');
                                $('#fecha_expiracion').val(moment().endOf('year').format('DD/MM/YYYY'));
                                $("#video").prop("required", true).addClass("required").attr("required", true).attr("disabled", false).removeClass("d-none");
                                $('#labelTipoOperacion').removeClass("d-none");
                            }
                        }
                    ],
                    dom: {
                        button: {className: "btn btn-info"},

                    }
                },
                ajax: {
                    'url': "{{route('pantallas.obtener_videos')}}",
                    'dataType': 'json',
                    'type': 'GET',
                },
                rowCallback: function (row, data) {
                    $(row).addClass(data.class);
                },
                columns: [
                    {data: 'boton'},
                    {data: 'get_aeropuerto.nombre' },
                    {data: 'comercio' },
                    {data: 'fecha' },
                    {data: 'video'},
                    {data: 'publicidad'},
                    {data: 'estatus'},
                  
                ],
                language: {
                    url: "{{url('/')}}/plugins/datatables/{{app()->getLocale()}}.json"
                }
            });

            $("#frmVideos").validate({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                },
                submitHandler: function (form) {
                    event.preventDefault();

                    $("#videoModal").prepend(LOADING);
                    var formData = new FormData(document.getElementById("frmVideos"));

                    $.ajax({
                        url: "{{ route('pantallas.lista_videos') }}",
                        type: "post",
                        dataType: "json",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            $(".overlay-wrapper").remove();                        
                            Toast.fire({
                                icon: response.type,
                                title: response.message
                            });
                            videosTable.draw();
                        },
                        error: function () {
                            $(".overlay-wrapper").remove();
                            Toast.fire({
                                icon: "error",
                                title: "{{__('Error al Guardar la Información')}}"
                            });

                        }
                    });
                }
            });
            $("#tipo_anuncio").on("change", function(){
                if($(this).val() == 1){
                    $("#comercio").val('BAER').attr('readonly', true);
                }else{
                    $("#comercio").val('').attr('readonly', false);
                }
            });
        });
        function editVideo(id, aeropuerto, comercio, fecha_expiracion, nombre_video, publicidad){
            console.log(publicidad)
            $("#frmVideos").attr({
                action: '{{ route("pantallas.lista_videos") }}',
                method: 'PUT'
            });
            $(".modal-title").text("Editar video");
            $("#_method").val('PUT');
            $("#video_id").val(id);
            $('#aeropuerto_id').val(aeropuerto).prop('selected', true);
            $('#comercio').val(comercio).prop('selected', true);
            $('#fecha_expiracion').val(showDate(fecha_expiracion));
            $('#video').prop("required", false).removeAttr("required").attr("disabled", true).addClass("d-none");
            $('#labelTipoOperacion').addClass("d-none");
            $('#tipo_anuncio').val(publicidad).prop('selected', true);

            $('#send').html("<i class='fas fa-save'></i> Actualizar");
        }
        function playVideo(video){
            document.getElementById('video_play').src="{{ url('dist/videos') }}/"+video;
            document.getElementById('video_play').play();
            $(".videomodal-title").text('Video: '+video);
        }
    </script>

</div>