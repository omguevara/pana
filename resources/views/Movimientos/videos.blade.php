<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}: Videos</title>

        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">

    </head>
    <body >
        <script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript">
        </script>

    </body>
</html>
