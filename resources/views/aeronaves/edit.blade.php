

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Actualizar Aeronave')}}</h3>

            <div class="card-tools">


                <div class="card-tools">
                    <a type="button"  href="{{route('aeronaves')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                    <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>
    
    
    
                </div>

            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>["aeronaves.edit", $Aeronaves->crypt_id], 'files'=>'true',  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">

            <div class="row">


                <div class="col-sm-12 col-md-4" >
                    <div class="form-group">
                        <label for="matricula">{{__('Matrícula')}}</label>
                        {{Form::text("matricula", $Aeronaves->matricula, ["maxlength"=>"10", "minlength"=>"5", "required"=>"required", "class"=>"form-control matricula required", "id"=>"matricula", "placeholder"=>__('Matrícula')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-4">

                    <div class="form-group">
                        <label for="nombre">{{__('Aeronave')}}</label>
                        {{Form::text("nombre", $Aeronaves->nombre, ["maxlength"=>"20", "required"=>"required", "class"=>"form-control required ", "id"=>"nombre", "placeholder"=>__('Nombre')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-4">

                    <div class="form-group">
                        <label for="maximo_pasajeros">{{__('Máximo Pasajero')}}</label>
                        {{Form::text("maximo_pasajeros", $Aeronaves->maximo_pasajeros, ["required"=>"required", "class"=>"form-control number required ", "id"=>"maximo_pasajeros", "placeholder"=>__('Máximo Pasajero')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="peso_maximo">{{__('Peso Máximo de Despegue')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                        {{Form::text("peso_maximo", muestraFloat($Aeronaves->peso_maximo/1000), ["required"=>"required", "class"=>"form-control money text-right required ", "id"=>"peso_maximo", "placeholder"=>__('Peso Máximo')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-6">

                    <div class="form-group">
                        <label for="peso_maximo_certificado">{{__('Peso Máximo Certificado')}} <span title="" class="badge bg-primary">Peso en Tn</span></label>
                        {{Form::text("peso_maximo_certificado", muestraFloat($Aeronaves->peso_maximo_certificado/1000), ["required"=>"required", "class"=>"form-control required money text-right ", "id"=>"peso_maximo_certificado", "placeholder"=>__('Peso Máximo Certificado')])}}

                    </div>
                </div>

                <div class="col-sm-12 col-md-12">

                    <div class="form-group">
                        <label for="estacion_id">{{__('Basamento')}}</label>
                        {{Form::select("estacion_id", $Aeropuertos,  $Aeronaves->estacion_id, ["required"=>"required", "class"=>"form-control select2 required", "id"=>"estacion_id", "placeholder"=>__('Select')])}}

                    </div>
                </div>
                <div class="col-sm-12 col-md-12 ">
                    <div class="form-group">
                        <label for="">Observación</label>
                        {{Form::textarea("observaciones", $Aeronaves->observaciones, [ 'rows' =>4,  "class"=>" form-control "])}}

                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <div class="form-group">
                        <label for="exampleInputFile">Certificado de Homologación Acústica</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="homologacion" name="homologacion">
                                <label class="custom-file-label" for="exampleInputFile">Subir Documento</label>
                            </div>

                        </div>
                    </div>
                </div>

            </div>





            <!-- /.card-body -->
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>





<script type="text/javascript">




    $(document).ready(function () {

        $(".money").maskMoney({"selectAllOnFocus": true, "decimal": ",", "thousands": ".", "allowZero": true});

        $(".select2").select2();
        
        $(".matricula").alphanum({
                allowNumeric: true,
                allowUpper: true,
                allowLower: true,
                allowSpace: false,
                allowOtherCharSets:false
                

            });
        
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                var formData = new FormData(document.getElementById("frmPrinc1"));


                $.ajax({
                    url: this.action,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('aeronaves')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });
            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
