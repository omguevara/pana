

<div id="panel_princ" class="col-sm-12 col-md-6 offset-md-3  mt-3">
    <div class="card card-success">
        <div class="card-header">
            <h3 class="card-title">{{__('Crear Cliente')}}</h3>

            <div class="card-tools">
                <a type="button"  href="{{route('clientes')}}" onClick="setAjax(this, event)" class="btn btn-tool" ><i class="fas fa-undo"></i> Regresar</a>
                <a type="button"  href="javascript:void(0)" onClick="$('#panel_princ').remove()" class="btn btn-tool" ><i class="fas fa-times"></i> Salir</a>


            </div>
            <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        {{Form::open(["route"=>"clientes.create", 'files'=>'true',  'id'=>'frmPrinc1','autocomplete'=>'Off'])}}
        <div id="card-body-main" class="card-body ">
            <div class="row">

                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Documento:</b></span>
                    </div>
                    {{Form::select('type_document', ["V"=>"V", "J"=>"J", "G"=>"G", "E"=>"E", "P"=>"P"], "", ["style"=>"max-width:80px", "class"=>"form-control tab1", "id"=>"type_document" ,"required"=>"required"])}}
                    {{Form::text("document", "", ["data-msg-minlength"=>"Mínimo 4 Dígitos", "minlength"=>"4", "data-msg-number"=>"Sólo Números", "data-msg-required"=>"Campo Requerido", "maxlength"=>"12", "required"=>"required", "class"=>"form-control number required dos", "id"=>"document", "placeholder"=>__('Document')])}}

                </div>

                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>Razón Social:</b></span>
                    </div>

                    {{Form::text("razon", "", ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos ", "id"=>"razon", "placeholder"=>__('Responsable')])}}
                </div>
                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Phone')}}:</b></span>
                    </div>

                    {{Form::text("phone", "", ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required phone dos", "id"=>"phone", "placeholder"=>__('Phone')])}}
                </div>

                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Correo')}}:</b></span>
                    </div>

                    {{Form::text("correo", "", ["class"=>"form-control email  dos", "id"=>"correo", "placeholder"=>__('Correo')])}}
                </div>

                <div class="input-group mt-2 ">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><b>{{__('Dirección')}}:</b></span>
                    </div>

                    {{Form::text("direccion", "", ["data-msg-required"=>"Campo Requerido", "required"=>"required", "class"=>"form-control required dos", "id"=>"direccion", "placeholder"=>__('Dirección')])}}
                </div>


            </div>
        </div>
        <div class="card-footer text-center">
            {{Form::button('<i class="fa fa-save"></i> '.__("Save"),  ["type"=>"submit", "class"=>"btn btn-primary", "id"=>"save"])}}

        </div>
        <!-- /.card -->
        {{ Form::close() }}
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {


        $(".select2").select2();
        $(".phone").inputmask({"mask": "(0999)-999.99.99", "clearIncomplete": true});
        $('.number').numeric({negative: false});
        $('#frmPrinc1').on("submit", function (event) {
            event.preventDefault();
            if ($('#frmPrinc1').valid()) {
                $("#frmPrinc1").prepend(LOADING);
                var formData = new FormData(document.getElementById("frmPrinc1"));


                $.ajax({
                    url: this.action,
                    type: "post",
                    dataType: "json",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        Toast.fire({
                            icon: response.type,
                            title: response.message
                        });
                        if (response.status == 1) {
                            $.get("{{route('clientes')}}", function (response) {
                                $("#main-content").html(response);

                            }).fail(function (xhr) {
                                $("#frmPrinc1").find(".overlay-wrapper").remove();
                            });
                        } else {
                            $("#frmPrinc1").find(".overlay-wrapper").remove();
                        }
                    },
                    error: function () {
                        $(".overlay-wrapper").remove();
                        Toast.fire({
                            icon: "error",
                            title: "{{__('Error al Guardar la Información')}}"
                        });

                    }
                });


            }
            return false;
        });

        $('#frmPrinc1').validate({
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.input-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid').addClass("is-valid");
            }
        });
    });

</script>
