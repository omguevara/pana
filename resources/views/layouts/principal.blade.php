<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>

        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">


        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/jqvmap/jqvmap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/daterangepicker/daterangepicker.css">
        <!-- summernote -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/summernote/summernote-bs4.min.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">


        <link rel="stylesheet" href="{{url('/')}}/plugins/select2/css/select2.min.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/jqueryReadonly/jquery.readonly.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">

        
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/codebase/grid.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/common/index.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/common/grid.css">
        
        
        

        <link rel="stylesheet" href="{{url('/')}}/dist/css/main.css">



        <!-- BS Stepper -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/bs-stepper/css/bs-stepper.min.css">
    </head>
    <body class="hold-transition layout-top-nav">
        <a target="_blank" href="https://api.whatsapp.com/send?phone=584123776330" class="btn-flotante"><img  src="{{url('dist/img/icon-w.png')}}" /></a>

        <div class="wrapper">

            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="{{url('/')}}/dist/img/inicio.png" alt="Logo PANA" height="60" width="60">
            </div>

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">

                <div class="container">
                    <a href="{{route('home')}}" class="navbar-brand">
                        <img src="{{url('/')}}/dist/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                        <span class="brand-text font-weight-light"></span>
                    </a>


                    <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                        <!-- Left navbar links -->



                    </div>


                </div>
            </nav>  

            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <div class="container">
                        <div class="row">
                            @yield('content')
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>









            <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">
                        {{ENV("APP_NAME")}} ({{ENV('APP_CLIENTE')}})
                </div>
                <!-- Default to the left -->
                <strong> Versión {{ENV("APP_VERSION")}}</strong> 
            </footer>






            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{url('/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
$.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- ChartJS -->

        <script src="{{url('/')}}/plugins/chart.js/Chart.min.js"></script>
        <script src="{{url('/')}}/plugins/chart.js/chartjs-plugin-datalabels.min.js"></script>

        <script src="{{url('/')}}/plugins/select2/js/select2.full.min.js"></script>

        <!-- Sparkline -->
        <script src="{{url('/')}}/plugins/sparklines/sparkline.js"></script>
        <!-- JQVMap -->
        <script src="{{url('/')}}/plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="{{url('/')}}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{url('/')}}/plugins/jquery-knob/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="{{url('/')}}/plugins/moment/moment.min.js"></script>
        <script src="{{url('/')}}/plugins/moment/locale/es.js"></script>
        <script src="{{url('/')}}/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{url('/')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- Summernote -->
        <script src="{{url('/')}}/plugins/summernote/summernote-bs4.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="{{url('/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="{{url('/')}}/dist/js/adminlte.js"></script>

        <!-- Bootstrap Switch -->
        <script src="{{url('/')}}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

        <!-- BS-Stepper -->
        <script src="{{url('/')}}/plugins/bs-stepper/js/bs-stepper.min.js"></script>


        <!-- DataTables  & Plugins -->
        <script src="{{url('/')}}/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <!-- jquery-validation -->
        <script src="{{url('/')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="{{url('/')}}/plugins/jquery-validation/additional-methods.min.js"></script>

        <!-- InputMask -->
        <script src="{{url('/')}}/plugins/inputmask/jquery.inputmask.min.js"></script>


        <!-- MaskMoney -->
        <script src="{{url('/')}}/plugins/jqueryMaskMoney/jquery.maskMoney.min.js"></script>


        <script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>

        <script src="{{url('/')}}/plugins/jqueryReadonly/jquery.readonly.min.js"></script>

        <script src="{{url('/')}}/plugins/jquery-marquee/jquery.marquee.min.js"></script>

        <script src="{{url('/')}}/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

        <script src="{{url('/')}}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

        
        <script src="{{url('/')}}/plugins/dhx/codebase/grid.min.js"></script>

        
        <script src="{{url('/')}}/plugins/alphaNum/jquery.alphanum.js"></script>
        

        <!-- AdminLTE for demo purposes -->
        <script src="{{url('/')}}/dist/js/demo.js"></script>
        <script type="text/javascript">
var ROOT = "{{url('/')}}";
var LOADING = '<div class="overlay-wrapper"><div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">{{__("Loading...")}}</div></div></div>';

var VALOR_PETRO = parseFloat({{$VALOR_PETRO}}, 10);
var VALOR_EURO = parseFloat({{$VALOR_EURO}}, 10);
var VALOR_DOLLAR = parseFloat({{$VALOR_DOLLAR}}, 10);

var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

        </script>



    </body>
</html>
