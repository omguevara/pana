<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
            <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

        <!-- SweetAlert2 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">
        
        {!! RecaptchaV3::initJs() !!}
        
        
    </head>
    <body class="hold-transition login-page content-wrapper">
        <style>
            .content-wrapper {
                background: url("{{url('/')}}/dist/img/b1.webp") no-repeat   fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                
                
            }
            .login-box, .register-box {
                width: 400px !important;
            }
        </style>
        @yield('content')
    </body>
</html>
