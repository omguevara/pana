<html>
    <head>
        <style>
            body{
                font-family: sans-serif;
            }
            @page {
                margin: 200px 25px;
            }
            header {
                position: fixed;
                left: 0px;
                top: -200px;
                right: 0px;
                height: 200px;
                
                text-align: center;
            }
            
            footer {
                position: fixed;
                left: 0px;
                bottom: -50px;
                right: 0px;
                height: 40px;

            }
            footer .page:after {
                content: counter(page);

            }
            footer table {
                width: 100%;
            }
            footer p {
                text-align: right;
            }
            footer .izq {
                text-align: center;
            }
            
            #content{
                /*margin-top: 200px;*/
            }
        </style>
    <body>
        <header>
            <table style="width:100%; ">
                <tr>
                    <td ><img style="width:100%; " src="{{url('/')}}/dist/img/cintillo.png" /></td>
                </tr>
                <tr>
                    <td ><img style  src="{{url('/')}}/dist/img/logo3.png" /></td>

                </tr>
            </table>
            <table style="width:100%; ">
                <thead>
                    <tr>
                        <th style="border:  solid 1px; padding:4px; "  >
                            <div style="width: 100%; background-color: gray; font-weight: bold">
                                {{__($title)}}
                            </div>

                        </th>

                    </tr>
                    @if (isset($subTitle))
                    <tr>
                        <th style="border-bottom:  solid 1px; margin-top: 20px"  >{{__($subTitle)}}</th>

                    </tr>
                    @endif
                </thead>
            </table>

        </header>
        <footer>
            <table>
                <tr>
                    <td>
                        <p class="izq">
                            <img  src="{{url('/')}}/dist/img/foot.png" />
                        </p>
                    </td>

                </tr>
                <tr>

                    <td>
                        <p class="page">
                            Página
                        </p>
                    </td>
                </tr>
            </table>
        </footer>
        <div id="content">
            <p>
                @yield('content')
            </p>
        </div>
    </body>
</html>