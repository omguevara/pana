<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>

        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">


        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/jqvmap/jqvmap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/daterangepicker/daterangepicker.css">
        <!-- summernote -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/summernote/summernote-bs4.min.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">


        <link rel="stylesheet" href="{{url('/')}}/dist/css/main.css">



        <!-- BS Stepper -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/bs-stepper/css/bs-stepper.min.css">
    </head>
    <body >



        @yield('content')

        <script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
        <script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>
        <script src="{{url('/')}}/plugins/jqueryMaskMoney/jquery.maskMoney.min.js"></script>
        <script type="text/javascript">
var ROOT = "{{url('/')}}";
var LOADING = '<div class="overlay-wrapper"><div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">{{__("Loading...")}}</div></div></div>';
        </script>
    </body>
</html>
