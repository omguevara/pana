<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>
        
        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">
        
        
        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/jqvmap/jqvmap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/daterangepicker/daterangepicker.css">
        <!-- summernote -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/summernote/summernote-bs4.min.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        
        <link rel="stylesheet" href="{{url('/')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
        
        
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/codebase/grid.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/common/index.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/common/grid.css">

        <link rel="stylesheet" href="{{url('/')}}/dist/css/main.css">



        <!-- BS Stepper -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/bs-stepper/css/bs-stepper.min.css">
    </head>
    <body class="hold-transition sidebar-mini layout-fixed {{(Auth::user()->theme==2 ? "dark-mode":"")}} ">

        <style>
            .nav-flat.nav-child-indent .nav-treeview .nav-icon {
                margin-left: 2rem !important;
            }

            .content-wrapper {
                background: url("{{url('/')}}/dist/img/b1.webp.png") no-repeat   ;
                background-size: auto;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
            
            
            .loading_input{
                background-image: url("dist/img/waiting.gif") !important;
                background-repeat: no-repeat;
                background-position: right calc(.375em + .1875rem) center;
                background-size: calc(.75em + .375rem) calc(.75em + .375rem);
            }
            
        </style>
        <div class="wrapper">

            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="{{url('/')}}/dist/img/inicio.png" alt="Logo PANA" height="60" width="60">
            </div>

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">{{config('app.name')}}</a>
                    </li>

                </ul>

                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">




                    <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" id="chgTheme" href="#">
                            <i class="fas {{(Auth::user()->theme==2 ? "fa-sun":"fa-moon")}}"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right p-0">
                            <a href="#" onClick="modeTheme(this, 1)" class="selectTheme dropdown-item {{(Auth::user()->theme==1 ? "active":"")}} ">
                                <i class="fas fa-sun mr-2"></i> {{__("Light")}}
                            </a>
                            <a href="#" onClick="modeTheme(this, 2)" class="selectTheme dropdown-item {{(Auth::user()->theme==2 ? "active":"")}}">
                                <i class="fas fa-moon mr-2"></i> {{__("Dark")}}
                            </a>

                        </div>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                            <i class="fas fa-expand-arrows-alt"></i>
                        </a>
                    </li>
                    -->

                </ul>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->

            <aside class="main-sidebar bg-primary elevation-4">
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                    
                    <img src="{{url('/')}}/dist/img/logo.png" alt="Ventel Logo" class="img-fluid " >
                    
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{url('/')}}/dist/img/avatar/{{Auth::user()->avatar}}.png" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info text-white">
                            <a href="#" class="d-block">{{ Auth::user()->name_user.' '.Auth::user()->surname_user }}</a>
                        </div>
                    </div>



                    <!-- Sidebar Menu -->
                    <nav class="mt-2"> 
                        <ul class="nav nav-pills nav-sidebar flex-column nav-compact nav-child-indent text-sm nav-flat" data-widget="treeview" role="menu" data-accordion="false">
                            <!-- Add icons to the links using the .nav-icon class
                                 with font-awesome or any other icon font library -->
                            @foreach($Menu as $key=>$menu)
                            <li class="nav-item ">
                                <a href="#" class="nav-link check-click">
                                    <i class="nav-icon fas fa-{{$menu['icon']}}"></i>
                                    <p>
                                        {{$menu['name_menu']}}
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    @foreach($menu['process'] as $link)
                                    <li class="nav-item">
                                        <a data-fiscal="{{$link['requiere_fiscal']}}" href="{{route($link['route'])}}" class="nav-link princ-menu">
                                            <i class="fas fa-{{$link['icon']}} nav-icon"></i>
                                            <p>{{$link['name_process']}}</p>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                              
                            <li class="nav-item ">
                                <a href="{{url('logout')}}" class="nav-link ">
                                    <i class="nav-icon fas fa-undo"></i>
                                    <p>
                                        {{__("Logout")}}

                                    </p>
                                </a>
                            </li>
                            
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div id="main-content" class="content-wrapper">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2022 </strong>

                <div class="float-right d-none d-sm-inline-block">
                    <b>Version</b> 1.2.0
                </div>
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{url('/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
                $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- ChartJS -->
        
        <script src="{{url('/')}}/plugins/chart.js/Chart.min.js"></script>
        <script src="{{url('/')}}/plugins/chart.js/chartjs-plugin-datalabels.min.js"></script>
        
        
        
        <!-- Sparkline -->
        <script src="{{url('/')}}/plugins/sparklines/sparkline.js"></script>
        <!-- JQVMap -->
        <script src="{{url('/')}}/plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="{{url('/')}}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{url('/')}}/plugins/jquery-knob/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="{{url('/')}}/plugins/moment/moment.min.js"></script>
        <script src="{{url('/')}}/plugins/moment/locale/es.js"></script>
        <script src="{{url('/')}}/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{url('/')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- Summernote -->
        <script src="{{url('/')}}/plugins/summernote/summernote-bs4.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="{{url('/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="{{url('/')}}/dist/js/adminlte.js"></script>
        
        <!-- Bootstrap Switch -->
        <script src="{{url('/')}}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
        
        <!-- BS-Stepper -->
        <script src="{{url('/')}}/plugins/bs-stepper/js/bs-stepper.min.js"></script>


        <!-- DataTables  & Plugins -->
        <script src="{{url('/')}}/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

        <!-- jquery-validation -->
        <script src="{{url('/')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="{{url('/')}}/plugins/jquery-validation/additional-methods.min.js"></script>

        <!-- InputMask -->
        <script src="{{url('/')}}/plugins/inputmask/jquery.inputmask.min.js"></script>


        <!-- MaskMoney -->
        <script src="{{url('/')}}/plugins/jqueryMaskMoney/jquery.maskMoney.min.js"></script>


        <script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>
        
        
        
        <script src="{{url('/')}}/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
        
        <!-- AdminLTE for demo purposes -->
        <script src="{{url('/')}}/dist/js/demo.js"></script>
        <script type="text/javascript">
            var ROOT = "{{url('/')}}";
            var LOADING = '<div class="overlay-wrapper"><div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">{{__("Loading...")}}</div></div></div>';
           
            $(document).ready(function () {
                $(".check-click").on("click", function () {

                    $(".check-click").removeClass("active");
                    //if ( $(this).parent().hasClass("menu-open")  ){
                    this.classList.add("active");
                    //}


                });
                
           
            });

            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
        </script>



    </body>
</html>
