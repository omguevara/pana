<!DOCTYPE html>
<html lang="en">
    <head>
        <title>{{config('app.name')}}</title>


    </head>
    <style>
        
    </style>
    <body style="margin-bottom: 55px; padding:0px" >

        <table style="width:100%; ">
            <tr>
                <td ><img style="width:100%; " src="{{url('/')}}/dist/img/cintillo.png" /></td>
            </tr>
            <tr>
                <td ><img  src="{{url('/')}}/dist/img/logo3.png" /></td>

            </tr>
        </table>
        <table style="width:100%; ">
            <thead>
                <tr>
                    <th style="border:  solid 1px; padding:4px; "  >
                        <div style="width: 100%; background-color: gray; font-weight: bold">
                            {{__($title)}}
                        </div>

                    </th>

                </tr>
                @if (isset($subTitle))
                <tr>
                    <th style="border-bottom:  solid 1px; margin-top: 20px"  >{{__($subTitle)}}</th>

                </tr>
                @endif
            </thead>
        </table>


        @yield('content')
        <br/>
        <div style="  clear:both; left:0px; bottom: 0;  position: fixed;  width:100%; text-align: center; ">

            <img  src="{{url('/')}}/dist/img/foot.png" />
            
        </div>
    </body>
</html>
