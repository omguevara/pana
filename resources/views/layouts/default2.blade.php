<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{config('app.name')}}</title>

        <link rel="icon" href="{{url('/')}}/dist/img/favicon.png">


        <!-- Google Font: Source Sans Pro -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Tempusdominus Bootstrap 4 -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
        <!-- JQVMap -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/jqvmap/jqvmap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('/')}}/dist/css/adminlte.min.css">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/daterangepicker/daterangepicker.css">
        <!-- summernote -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/summernote/summernote-bs4.min.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/select2/css/select2.min.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">

        <!-- DataTables -->
         
        
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
        
        

       
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables/jquery.dataTables.min.css">
       
        <link rel="stylesheet" href="{{url('/')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">

        
        
        
        
        <link rel="stylesheet" href="{{url('/')}}/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">    

        <link rel="stylesheet" href="{{url('/')}}/plugins/jqueryReadonly/jquery.readonly.css">

        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/codebase/grid.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/common/index.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/dhx/common/grid.css">
        <link rel="stylesheet" href="{{url('/')}}/plugins/summernote/summernote-bs5.min.css">


        <link rel="stylesheet" href="{{url('/')}}/plugins/selectize/css/selectize.css">
        
        
        <link rel="stylesheet" href="{{url('/')}}/plugins/leaflet/leaflet.css">

        <link href="{{url('/')}}/plugins/jQuery-Plugin-Email/multiple-emails.css" rel="stylesheet" type="text/css"/>


        <link rel="stylesheet" href="{{url('/')}}/dist/css/main.css">



        <!-- BS Stepper -->
        <link rel="stylesheet" href="{{url('/')}}/plugins/bs-stepper/css/bs-stepper.min.css">
    </head>
    <body class="hold-transition layout-top-nav">


        <div class="wrapper">

            <!-- Preloader -->
            <div class="preloader flex-column justify-content-center align-items-center">
                <img class="animation__shake" src="{{url('/')}}/dist/img/inicio.png" alt="Logo PANA" height="60" width="60">
            </div>

            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand-md navbar-primary navbar-dark">

                <div class="container">
                    <a href="javascript:void(0)" class="navbar-brand">
                        <img src="{{url('/')}}/dist/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                        <span class="brand-text font-weight-light">MENU</span>
                    </a>

                    <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse order-3" id="navbarCollapse">
                        <!-- Left navbar links -->
                        <ul class="navbar-nav">

                            @foreach($Menu as $key=>$menu)
                            <li class="nav-item dropdown">
                                <a id="dropdownSubMenu{{$key}}" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">
                                    {{$menu['name_menu']}}
                                </a>
                                <ul aria-labelledby="dropdownSubMenu{{$key}}" class="dropdown-menu border-0 shadow">
                                    @foreach($menu['process'] as $link)
                                    <li >
                                        <a  href="{{route($link['route'])}}" class="dropdown-item princ-menu">

                                            {{$link['name_process']}}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                            
                            



                        </ul>
                       

                        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
                            <!-- Messages Dropdown Menu -->
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="modal" data-target="#see_information"  title="Información" href="javascript:void(0)">
                                    <small class="badge badge-info"><i class="fas fa-info"></i></small>
                                </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link"  title="salir" href="{{url('logout')}}">
                                    <small class="badge badge-danger"><i class="fas fa-door-open"></i></small>
                                </a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" data-toggle="dropdown" href="#">
                                    <small class="badge badge-warning"><i class="fas fa-user"></i></small>
                                </a>
                                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                    <a href="#" class="dropdown-item">
                                        <div class="media">
                                            <div class="media-body">
                                                <h3 class="dropdown-item-title">
                                                    <small class="badge badge-success">{{Auth::user()->full_nombre}}</small>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item">
                                        <div class="media">
                                            <div class="media-body">
                                                <h3 class="dropdown-item-title">
                                                    <small class="badge badge-success">Perfil: {{Auth::user()->profile->name_profile}}</small>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    @if (Auth::user()->aeropuerto_id!=null)
                                    <div class="dropdown-divider"></div>
                                    <a href="#" class="dropdown-item">
                                        <div class="media">
                                            <div class="media-body">
                                                <h3 class="dropdown-item-title">
                                                    <small class="badge badge-success">Aeropuerto: {{Auth::user()->aeropuerto->codigo_oaci}}</small>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    @endif
                                    
                                    <div class="dropdown-divider"></div>
                                    <a href="{{route('change_pwd')}}#" class="dropdown-item princ-menu">
                                        <div class="media">
                                            <div class="media-body">
                                                <h3 class="dropdown-item-title">
                                                    <small class="badge badge-success">{{__('Change Password')}}</small>
                                                </h3>
                                            </div>
                                        </div>
                                    </a>
                                    
                                </div>
                            </li>
                        </ul>

                    </div>






                </div>
            </nav>  

            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <div class="container">
                        <div id="main-content" class="row">
                            @yield('content')
                        </div>
                        <!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>









            <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">

                    {{ENV("APP_NAME")}} ({{ENV('APP_CLIENTE')}})
                </div>
                <!-- Default to the left -->
                <strong> Versión {{ENV("APP_VERSION")}}</strong> 
            </footer>






            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        
        
        <div class="modal fade" id="see_information"  data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel">

            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modalTitle">Información</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span class="fa fa-times"></span>
                        </button>
                    </div>
                    <div id="body_information"  class="modal-body">
                        
                    </div>
                    <div class="modal-footer justify-content-between">
                    </div>
                </div>
            </div>
        </div>
        
        
        <!-- ./wrapper -->
        <!-- jQuery -->
        <script src="{{url('/')}}/plugins/jquery/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{url('/')}}/plugins/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
$.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 4 -->
        <script src="{{url('/')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- ChartJS -->

        <script src="{{url('/')}}/plugins/chart.js/chartNew.js"></script>
        
        <script src="{{url('/')}}/plugins/chart.js/chartjs-plugin-datalabels.min.js"></script>



        <!-- Sparkline -->
        <script src="{{url('/')}}/plugins/sparklines/sparkline.js"></script>
        <!-- JQVMap -->
        <script src="{{url('/')}}/plugins/jqvmap/jquery.vmap.min.js"></script>
        <script src="{{url('/')}}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{url('/')}}/plugins/jquery-knob/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="{{url('/')}}/plugins/moment/moment.min.js"></script>
        <script src="{{url('/')}}/plugins/moment/locale/es.js"></script>
        <script src="{{url('/')}}/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{url('/')}}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
        <!-- Summernote -->
        <script src="{{url('/')}}/plugins/summernote/summernote-bs4.min.js"></script>
        <!-- overlayScrollbars -->
        <script src="{{url('/')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
        <!-- AdminLTE App -->
        <script src="{{url('/')}}/dist/js/adminlte.js"></script>


        <!-- Bootstrap Switch -->
        <script src="{{url('/')}}/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>

        <!-- BS-Stepper -->
        <script src="{{url('/')}}/plugins/bs-stepper/js/bs-stepper.min.js"></script>


        <!-- DataTables  & Plugins -->
        <script src="{{url('/')}}/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


        <script src="{{url('/')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>







        <script src="{{url('/')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
        <script src="{{url('/')}}/plugins/jszip/jszip.min.js"></script>
        <script src="{{url('/')}}/plugins/pdfmake/pdfmake.min.js"></script>
        <script src="{{url('/')}}/plugins/pdfmake/vfs_fonts.js"></script>
        <script src="{{url('/')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
        <script src="{{url('/')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>





        <!-- jquery-validation -->
        <script src="{{url('/')}}/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="{{url('/')}}/plugins/jquery-validation/additional-methods.min.js"></script>

        <script src="{{url('/')}}/plugins/jquery-validation/localization/messages_es.js"></script>

        <!-- InputMask -->
        <script src="{{url('/')}}/plugins/inputmask/jquery.inputmask.min.js"></script>


        <script src="{{url('/')}}/plugins/select2/js/select2.full.min.js"></script>
        <script src="{{url('/')}}/plugins/select2/js/i18n/es.js"></script>

        <!-- InputMask2 -->
        <script src="{{url('/')}}/plugins/jquery-mask/jquery.mask.min.js"></script>


        <!-- MaskMoney -->
        <script src="{{url('/')}}/plugins/jqueryMaskMoney/jquery.maskMoney.min.js"></script>


        <script src="{{url('/')}}/plugins/sweetalert2/sweetalert2.min.js"></script>


        <script src="{{url('/')}}/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>

        <script src="{{url('/')}}/plugins/jqueryReadonly/jquery.readonly.min.js"></script>

        <script src="{{url('/')}}/plugins/dhx/codebase/grid.min.js"></script>
        <script src="{{url('/')}}/plugins/summernote/summernote-bs5.min.js"></script>


        <script src="{{url('/')}}/plugins/selectize/js/selectize.min.js"></script>

        <script src="{{url('/')}}/plugins/input-plugin-jQuery-numeric-js/jquery.numeric-min.js"></script>

        <script src="{{url('/')}}/plugins/alphaNum/jquery.alphanum.js"></script>

        <script src="{{url('/')}}/plugins/jQuery-Plugin-Email/multiple-emails.js" type="text/javascript"></script>
        <script src="{{url('/')}}/plugins/Map-Maphilight/jquery.maphilight.min.js" type="text/javascript"></script>
        
        
        <script src="{{url('/')}}/plugins/leaflet/leaflet.js" type="text/javascript"></script>
        

        
   
        <!-- AdminLTE for demo purposes -->
        <script src="{{url('/')}}/dist/js/demo.js"></script>
        <script type="text/javascript">
            
var LIFETIME = parseInt("{{$lifetime}}", 10) * 60;
var LIFETIME2 = 0;
var refreshIntervalId;
var REVIVE = false;
var ROOT = "{{url('/')}}";
var LOADING = '<div class="overlay-wrapper"><div class="overlay"><i class="fas fa-3x fa-sync-alt fa-spin"></i><div class="text-bold pt-2">{{__("Loading...")}}</div></div></div>';
var VALOR_PETRO = parseFloat("{{$VALOR_PETRO}}", 10);
var VALOR_EURO = parseFloat("{{$VALOR_EURO}}", 10);
var VALOR_DOLLAR = parseFloat("{{$VALOR_DOLLAR}}", 10);
var SET_INTERVAL = null;
$(document).ready(function () {
    
    @if(Auth::user()->aeropuerto_id!=null)
        
        setInterval(function () {
            $.get("{{route('flights')}}", function (response){
                console.log(response.data.length);
                if (response.data.length>0){
                    html = '<table class="table table-bordered table-hover"  ><tr><th>ORIGEN</th><th>DESTINO</th><th>AERONAVE</th></tr>';
                    for(i in response.data){
                        html += '<tr><td>'+response.data[i].aeropuerto.full_nombre+'</td><td>'+response.data[i].origen_destino.full_nombre+'</td><td>'+response.data[i].aeronave.full_nombre_tn+'</td></tr>';
                    }
                    html += "</table>";
                    $("#body_information").html(html);
                    
                    
                }else{
                
                }
            });
        }, 60000);
        
    @endif
    
    $(".check-click").on("click", function () {

        $(".check-click").removeClass("active");
//if ( $(this).parent().hasClass("menu-open")  ){
        this.classList.add("active");
//}


    });

    //refreshIntervalId = setInterval(checkSession, 1000);
});
function checkSession() {
    LIFETIME2++;
    por = (LIFETIME2 * 100) / LIFETIME;
    if (por >= 90) {
        if (REVIVE == false) {
            REVIVE = true;
            Swal.fire({
                icon: 'question',
                title: 'Su sessión esta por vencer, ¿desea extenderla?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                denyButtonText: 'No'
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.get("{{route('regenerate')}}", function () {
                        REVIVE = false;
                        LIFETIME2 = 0;
                    }).fail(function () {
                        document.location = "{{route('logout')}}";
                    });
                } else if (result.isDenied) {
                    clearInterval(refreshIntervalId);
                    document.location = "{{route('logout')}}";
                }
            });
        }
    }
    if (por >= 105) {
        clearInterval(refreshIntervalId);
        document.location = "{{route('logout')}}";
    }

    //console.log(LIFETIME2);
}
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 8000
});
        </script>



    </body>
</html>
