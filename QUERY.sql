ALTER TABLE maest_membresias ADD COLUMN activo boolean DEFAULT true;
php artisan migrate --path=/database/migrations/2023_10_30_160112_create_proc_alarmas_inventario_detalle_table.php
/******************************************************************************************/
php artisan migrate --path=/database/migrations/2023_10_27_170510_create_proc_costos_reparacion_table.php

/*****************************************************************************************/
INSERT INTO seg_processes (name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id) VALUES('Inventario de Equipos', '', '', 'inventario_equipos', '*', false, 6, 3);

UPDATE seg_processes SET "order"=5 WHERE id=29;
UPDATE seg_processes SET "order"=4 WHERE id=28;
UPDATE seg_processes SET "order"=3 WHERE id=23;
UPDATE seg_processes SET "order"=2 WHERE id=18;

php artisan migrate --path=/database/migrations/2023_10_25_152541_create_maest_concesion_equipos_table.php
php artisan migrate --path=/database/migrations/2023_10_25_152910_create_maest_inventario_equipos_table.php
/**************************************************************************************/
ALTER TABLE IF EXISTS public.proc_proforma ADD COLUMN tipo_vuelo_id bigint;
-- ***********************************************************************************

INSERT INTO seg_processes (name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id) VALUES('Reporte de Cupones', '', '', 'reporte_cupones', '*', false, 6, 5);
/**********************************************************************************/
update seg_processes  set name_process='Proforma Personalizada AVG' where id=33;

insert into seg_processes (name_process, description, icon, route, actions, "order", menu_id)
	values
('Proforma Personalizada AVC', '', 'plane', 'facturar_pospago.proforma_personalizada_avc', '*',4,4);

ALTER TABLE public.proc_info_operaciones ADD COLUMN calle_g character varying(100);
ALTER TABLE public.proc_info_operaciones ADD COLUMN calle_h character varying(100);
ALTER TABLE public.proc_info_operaciones ADD COLUMN calle_i character varying(100);
ALTER TABLE public.proc_info_operaciones ADD COLUMN calle_j character varying(100);
ALTER TABLE public.proc_info_operaciones ADD COLUMN calle_k character varying(100);

php artisan migrate --path=/database/migrations/2023_10_10_133820_create_aeropuertos_aerolineas_table.php
UPDATE seg_processes SET actions='create|edit|disable|ubicaciones|lugares|puestos|active|holidays|aerolineas' WHERE id=5;
UPDATE maest_bancos SET codigo = TRIM(codigo);

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id) VALUES('Cuentas Bancarias', '', '', 'cuentas_bancarias', 'create|edit|disable|active', false, 12, 2);
php artisan migrate --path=/database/migrations/2023_10_06_134820_create_maest_cuenta_bancarias_table.php

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id) VALUES('Bancos', '', '', 'bancos', 'create|edit|disable|active', false, 11, 2);
UPDATE maest_bancos SET user_id=1, ip= '127.0.0.1';
ALTER TABLE maest_bancos ADD COLUMN fecha_creacion TIMESTAMP DEFAULT NOW();
ALTER TABLE maest_bancos ADD COLUMN ip VARCHAR(20);
ALTER TABLE maest_bancos ADD COLUMN user_id BIGINT;
ALTER TABLE maest_bancos ADD COLUMN activo boolean DEFAULT true;

ALTER TABLE proc_movimientos ADD COLUMN tipo_recorrido_id BIGINT;
/*********************************************/
UPDATE public.seg_processes SET route='pantallas' WHERE id = 39;
UPDATE public.seg_processes SET route='pantallas.lista_videos' WHERE id = 40;
php artisan migrate --path=/database/migrations/2023_10_04_081448_create_proc_movimiento_logs_table.php
php artisan migrate --path=/database/migrations/2023_10_04_160316_create_trigger_update_movimientos_log.php
INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id) VALUES('Control de Videos', '', '', 'pantallas.lista_videos', '*', false, 2, 10);
php artisan migrate --path=/database/migrations/2023_10_05_100143_create_proc_videos_table.php


/********************************************/

php artisan make:migration create_trigger_update_movimientos_log

php artisan make:migration create_trigger_insert_movimientos_log
/******************************************/
ALTER TABLE maest_clientes ADD COLUMN imagen VARCHAR(20) DEFAULT '';
/*****************************************/

php artisan migrate --path=/database/migrations/2023_10_04_081410_create_proc_movimientos_table.php
php artisan migrate --path=/database/migrations/2023_10_04_081448_create_proc_movimiento_logs_table.php

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id) VALUES('LLegadas y Salidas', '', '', 'pantallas', '*', false, 1, 10);

INSERT INTO seg_menus(name_menu, description, icon, "order") values('Pantallas', '', '', '10');

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Reporte de Proforma', '', '', 'reporte_proforma', '*', false, 5, 5);

/****************************************/
ALTER TABLE public.proc_vuelos ADD COLUMN carga decimal(10,2);

ALTER TABLE proc_info_bomberos ADD COLUMN new_info BOOLEAN DEFAULT FALSE;
ALTER TABLE proc_info_cerfiticaciones ADD COLUMN new_info BOOLEAN DEFAULT FALSE;
ALTER TABLE proc_info_operaciones ADD COLUMN new_info BOOLEAN DEFAULT FALSE;
ALTER TABLE proc_info_seguridad ADD COLUMN new_info BOOLEAN DEFAULT FALSE;


/**********************/
ALTER TABLE proc_proforma ADD COLUMN tipo_proforma BIGINT DEFAULT 1;

ALTER TABLE IF EXISTS public.proc_hangares ALTER COLUMN ubicacion_id DROP NOT NULL;
/****************************/

ALTER TABLE proc_vuelos_detalle ALTER COLUMN aplicable DROP NOT NULL;

/************************************/
ALTER TABLE proc_venta_tickets ADD COLUMN aeropuerto_id bigint, ADD CONSTRAINT fk_aeropuerto_id FOREIGN KEY (aeropuerto_id) REFERENCES maest_aeropuertos(id);
php artisan migrate --path=/database/migrations/2023_09_18_153456_create_proc_proforma_pagos_table.php
ALTER TABLE proc_proforma ADD COLUMN pagado BOOLEAN DEFAULT FALSE;
php artisan migrate --path=/database/migrations/2023_09_18_135505_create_proc_venta_tickets_cierre_table.php
/*************************************/

INSERT INTO seg_menus(name_menu, description, icon, "order") values('Recaudación', '', '', '8');
INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Efectuar Pagos', '', '', 'recaudacion', '*', false, 1, 9);



//********************************/
INSERT INTO seg_menus(name_menu, description, icon, "order") values('Tickets', '', '', '7');
INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Cargar', '', 'ticket', 'carga_tickets', '*', false, 1, 8);

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Listar', '', 'ticket', 'lista_tickets', '*', false, 2, 8);


INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Vender', '', 'ticket', 'venta_tickets', '*', false, 2, 8);


php artisan migrate --path=/database/migrations/2023_09_06_143959_create_maest_tiempo_tickets_table.php
php artisan migrate --path=/database/migrations/2023_09_06_144221_create_proc_lotes_tickets_table.php
php artisan migrate --path=/database/migrations/2023_09_06_144434_create_proc_lotes_detalle_table.php

php artisan migrate --path=/database/migrations/2023_09_08_131850_create_proc_venta_tickets_table.php
php artisan migrate --path=/database/migrations/2023_09_08_131914_create_proc_venta_tickets_detalle_table.php
php artisan migrate --path=/database/migrations/2023_09_08_131924_create_proc_venta_tickets_pago_table.php



update maest_aeropuertos set latitude='6.834888036447006' , longitude='-63.34795013029583' where codigo_oaci = 'SVPU';
update maest_aeropuertos set latitude='5.792485925099387' , longitude='-61.436676012677346' where codigo_oaci = 'SVPP';
update maest_aeropuertos set latitude='8.946014266098098' , longitude='-64.14432165923046' where codigo_oaci = 'SVST';
update maest_aeropuertos set latitude='8.81906536509283' , longitude='-64.7395141110768' where codigo_oaci = 'SVPF';
update maest_aeropuertos set latitude='9.430726520132339' , longitude='-64.47138795975764' where codigo_oaci = 'SVAN';
update maest_aeropuertos set latitude='10.288297556175767' , longitude='-66.8171042375537' where codigo_oaci = 'SVCS';
update maest_aeropuertos set latitude='10.46247' , longitude='-66.09278' where codigo_oaci = 'SVHG';


/*************************************/
alter table maest_aeropuertos add column latitude varchar(20);
alter table maest_aeropuertos add column longitude  varchar(20);

UPDATE maest_aeropuertos set latitude='5.6276437228476315' , longitude='-67.60382898396799' where codigo_oaci = 'SVPA';
UPDATE maest_aeropuertos set latitude='10.104082476420468' , longitude='-64.6826254209222' where codigo_oaci = 'SVBC';
UPDATE maest_aeropuertos set latitude='9.180101216187065' , longitude='-64.6981468178339' where codigo_oaci = 'SVPF';
UPDATE maest_aeropuertos set latitude='8.950095989020726' , longitude='-64.14400043176859' where codigo_oaci = 'SVST';
UPDATE maest_aeropuertos set latitude='10.076804406289849' , longitude='-64.52838179317133' where codigo_oaci = 'SVAN';
UPDATE maest_aeropuertos set latitude='7.205376875052697' , longitude='-70.76315636513165' where codigo_oaci = 'SVGD';
UPDATE maest_aeropuertos set latitude='8.622112982476091' , longitude='-70.22114079210363' where codigo_oaci = 'SVBI';
UPDATE maest_aeropuertos set latitude='4.55498952003674' , longitude='-61.13971181079008' where codigo_oaci = 'SVSE';
UPDATE maest_aeropuertos set latitude='6.848221036508514' , longitude='-63.34586338393698' where codigo_oaci = 'SVPU';
UPDATE maest_aeropuertos set latitude='7.253058838517529' , longitude='-61.520843447937175' where codigo_oaci = 'SVTM';
UPDATE maest_aeropuertos set latitude='6.732064704714809' , longitude='-61.63555621554433' where codigo_oaci = 'SVPP';
UPDATE maest_aeropuertos set latitude='7.4786101815980235' , longitude='-61.906978420950296' where codigo_oaci = 'SVGT';
UPDATE maest_aeropuertos set latitude='7.627506777539447' , longitude='-66.16500827862069' where codigo_oaci = 'SVCD';
UPDATE maest_aeropuertos set latitude='9.654725974263965' , longitude='-68.57821714791284' where codigo_oaci = 'SVCJ';
UPDATE maest_aeropuertos set latitude='10.95069045823232' , longitude='-65.22493664379726' where codigo_oaci = 'SVIT';
UPDATE maest_aeropuertos set latitude='8.925452468628462' , longitude='-67.41421485264205' where codigo_oaci = 'SVCL';
UPDATE maest_aeropuertos set latitude='10.284183498426275' , longitude='-66.82245096625228' where codigo_oaci = 'SVCS';
UPDATE maest_aeropuertos set latitude='10.134143248455715' , longitude='-66.78943143154295' where codigo_oaci = 'SVMP';
UPDATE maest_aeropuertos set latitude='10.464033836508955' , longitude='-66.09233248073969' where codigo_oaci = 'SVHG';
UPDATE maest_aeropuertos set latitude='8.62484240848917' , longitude='-71.65956191133112' where codigo_oaci = 'SVVG';
UPDATE maest_aeropuertos set latitude='8.582403149113569' , longitude='-71.16149283015626' where codigo_oaci = 'SVMD';
UPDATE maest_aeropuertos set latitude='10.915412523397832' , longitude='-63.96767362183372' where codigo_oaci = 'SVMG';
UPDATE maest_aeropuertos set latitude='10.794314224847781' , longitude='-63.981378170212786' where codigo_oaci = 'SVIE';
UPDATE maest_aeropuertos set latitude='8.237610473711975' , longitude='-72.26859445392016' where codigo_oaci = 'SVLF';
UPDATE maest_aeropuertos set latitude='7.840272326274855' , longitude='-72.43953179391913' where codigo_oaci = 'SVSA';
UPDATE maest_aeropuertos set latitude='7.801890204945266' , longitude='-72.2027847278464' where codigo_oaci = 'SVPM';
UPDATE maest_aeropuertos set latitude='7.571510707857776' , longitude='-72.04392905277884' where codigo_oaci = 'SVSO';
UPDATE maest_aeropuertos set latitude='10.279915285273603' , longitude='-68.75546044915657' where codigo_oaci = 'SVSP';
UPDATE maest_aeropuertos set latitude='10.557874340042888' , longitude='-71.72804114132724' where codigo_oaci = 'SVMC';
UPDATE maest_aeropuertos set latitude='10.332362899086004' , longitude='-71.31932174223992' where codigo_oaci = 'SVON';
UPDATE maest_aeropuertos set latitude='8.976913846465909' , longitude='-71.94291895709702' where codigo_oaci = 'SVSZ';


INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Proforma Personalizada', '', 'plane', 'facturar_pospago.proforma_personalizada', '*', false, 3, 4);


/*************************************/
INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Emisión de Proforma X Operación', '', 'plane', 'facturar_pospago.proforma_operacion', '*', false, 2, 4);


/*************************************/


INSERT INTO seg_menus(name_menu, description, icon, "order") values('Comercialización', '', '', '6');
INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Concesiones', '', 'plane', 'concesiones', '*', false, 1, 7);



php artisan migrate --path=/database/migrations/2023_08_03_195945_create_maest_locales_usos_table.php
php artisan migrate --path=/database/migrations/2023_08_03_200027_create_maest_actividades_table.php
php artisan migrate --path=/database/migrations/2023_08_03_200130_create_maest_locales_table.php
php artisan migrate --path=/database/migrations/2023_08_03_200722_create_proc_concesiones_table.php


--php artisan migrate --path=/database/migrations/2023_08_03_201031_create_proc_concesiones_log_table.php


/**********************************************/

create table a as
select id, aeronave_id, observaciones_facturacion
from proc_vuelos
where observaciones_facturacion is not null and observaciones_facturacion != ''
order by id desc;


create index index_a on a(aeronave_id);

create  table  b as select distinct on (aeronave_id) * from a;



update proc_vuelos set observaciones_facturacion = (select observaciones_facturacion from b where b.aeronave_id = proc_vuelos.aeronave_id)
where observaciones_facturacion is null or trim(observaciones_facturacion) = '';




php artisan migrate --path=database/migrations/2023_07_10_122233_create_proc_info_cerfiticaciones_table.php



/************************/



ALTER TABLE conf_estados ADD COLUMN mapa2 text default null;
ALTER TABLE conf_estados ADD COLUMN posicion2 text default null;


php artisan migrate --path=database/migrations/2023_07_13_174735_create_proc_info_bomberos.php


ALTER TABLE conf_estados ADD COLUMN posicion varchar(20);

UPDATE conf_estados SET posicion ='50 150 280 280' WHERE id = 3;
UPDATE conf_estados SET posicion ='230 80 100 100' WHERE id = 4;
UPDATE conf_estados SET posicion ='70 150 200 200' WHERE id = 5;
UPDATE conf_estados SET posicion ='263 140 320 320' WHERE id = 6;
UPDATE conf_estados SET posicion ='205 80 70 70' WHERE id = 7;
UPDATE conf_estados SET posicion ='390 50 50 50' WHERE id = 18;
UPDATE conf_estados SET posicion ='250 50 100 100' WHERE id = 13;
UPDATE conf_estados SET posicion ='473 100 130 130' WHERE id = 9;
UPDATE conf_estados SET posicion ='270 80 30 30' WHERE id = 10;
UPDATE conf_estados SET posicion ='220 80 200 200' WHERE id = 12;
UPDATE conf_estados SET posicion ='105 70 100 100' WHERE id = 14;
UPDATE conf_estados SET posicion ='270 60 100 100' WHERE id = 16;
UPDATE conf_estados SET posicion ='135 120 90 90' WHERE id = 19;
UPDATE conf_estados SET posicion ='20 160 100 100' WHERE id = 21;
UPDATE conf_estados SET posicion ='90 100 70 70' WHERE id = 22;
UPDATE conf_estados SET posicion ='300 60 200 200' WHERE id = 2;
UPDATE conf_estados SET posicion ='-30 10 200 200' WHERE id = 24;
UPDATE conf_estados SET posicion ='240 -20 160 160' WHERE id = 26;
UPDATE conf_estados SET posicion ='200 280 260 260' WHERE id = 1;
UPDATE conf_estados SET posicion ='180 100 100 100' WHERE id = 8;
UPDATE conf_estados SET posicion ='90 0 150 150' WHERE id = 11;
UPDATE conf_estados SET posicion ='40 140 100 100' WHERE id = 15;
UPDATE conf_estados SET posicion ='390 70 200 200' WHERE id = 17;
UPDATE conf_estados SET posicion ='380 50 150 150' WHERE id = 20;
UPDATE conf_estados SET posicion ='180 70 100 100' WHERE id = 23;


UPDATE maest_aeropuertos SET cx = '385.7702', cy = '110.9362' WHERE id = 2;
UPDATE maest_aeropuertos SET cx = '385.7702', cy = '167.9362' WHERE id = 25;
UPDATE maest_aeropuertos SET cx = '290.7902', cy = '110.0862' WHERE id = 16;
UPDATE maest_aeropuertos SET cx = '270.0302', cy = '160.0762' WHERE id = 22;



UPDATE maest_aeropuertos SET cx = '130.3402', cy = '240.7162' WHERE id = 23;











/****************************/

php artisan migrate --path=database/migrations/2023_07_07_131736_create_proc_info_seguridad_table.php
php artisan migrate --path=database/migrations/2023_07_10_114747_create_proc_info_seguridad_log_table.php

php artisan migrate --path=database/migrations/2023_07_10_122233_create_proc_info_cerfiticaciones_table.php
php artisan migrate --path=database/migrations/2023_07_10_122234_create_proc_info_cerfiticaciones_log_table.php


php artisan migrate --path=database/migrations/2023_07_10_192650_create_proc_info_operaciones_table.php



INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Revisión de Proformas', '', 'plane', 'revision', '*', false, 1, 4);

ALTER TABLE proc_vuelos ADD COLUMN ok BOOLEAN default null;
ALTER TABLE proc_vuelos ADD COLUMN obs_rev varchar(250) ;



INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Registro Información de Aeropuertos', '', 'plane', 'ver_mapa', '*', false, 1, 3);
INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Información de Aeropuertos', '', 'plane', 'ver_info_mapa', '*', false, 1, 3);

ALTER TABLE conf_estados ADD COLUMN mapa text;
ALTER TABLE conf_estados ADD COLUMN nombre varchar(10);







UPDATE conf_estados SET mapa='m 290.0502,90.456198 -0.25,2.69 -1.24,0.99 -5.43,2.84 -1.97,0.24 -2.6,-0.24 -0.98,-0.62 -1.06,-1.3 0,0 0.56,-0.68 0,0 0.74,-0.61 1.24,-2.23 1.11,-0.12 0.62,-0.62 0.24,-1.11 1.36,-0.24 0.87,1.11 1.23,-0.13 5.31,0 0,0 z', nombre='VE-A'   WHERE upper(name_state) = 'DISTRITO CAPITAL';
UPDATE conf_estados SET mapa='m 472.7702,184.9362 -0.84,0.32 -0.86,0 -1.36,0.37 -0.62,1.61 -0.99,0.98 -1.11,0.87 -1.85,0.99 -1.85,-0.13 -1.48,-0.74 -1.98,-0.12 -4.81,-1.85 -1.86,0.12 -1.35,0.74 -1.73,-0.86 -1.36,1.11 -0.99,1.23 -1.73,0.37 -1.23,0 -1.36,2.35 -1.6,0.99 -1.61,0.61 -0.86,0.74 -3.21,1.73 -2.23,0.25 -1.73,-0.12 -1.23,-0.75 -1.85,0.25 -2.72,1.73 -1.48,0 -1.73,1.23 -2.47,3.83 -0.86,1.11 -1.61,1.12 -2.1,1.23 -2.47,-0.12 -2.96,-0.5 -1.6,-1.97 -2.47,-1.48 -1.61,-1.73 -2.1,-0.13 -4.44,2.1 -1.73,0.99 -1.85,-0.74 -2.6,-0.12 -0.86,0.74 -1.73,0.98 -1.48,1.73 0.25,2.47 0.37,1.11 1.11,0.99 1.36,0.74 0.12,1.24 -0.87,1.23 -4.19,0.25 -3.83,2.1 -1.73,1.6 -3.33,0.99 -1.98,0 -1.6,-0.25 -1.98,-1.48 -2.1,-1.36 -1.48,-1.97 -2.1,-0.87 -2.84,-0.61 -2.1,-0.62 -4.94,-1.85 -2.1,-0.5 -2.22,-0.12 0,0 -1.48,-1.11 0,0 -1.48,-0.99 -0.74,-1.23 0.99,-1.73 0.12,-2.72 1.23,-1.36 1.24,-2.47 1.23,-1.85 3.21,-4.07 1.36,-0.99 1.61,-1.73 1.23,-1.48 1.24,-1.85 0.12,-2.23 0.86,-1.97 1.11,-3.21 0.13,-1.98 -1.11,-1.36 0.24,-1.97 0.74,-1.48 1.36,-1.24 3.71,-0.24 3.95,0.12 0.86,0.86 0.99,1.36 2.47,0 0.74,-1.23 0.74,-2.23 1.11,-1.85 0.5,-1.97 -0.87,-1.98 -2.84,-1.36 -1.85,-1.23 -1.11,-0.99 -1.61,-0.74 0,-2.47 -2.22,-2.22 -2.34,-1.24 -2.47,-0.86 -2.1,-0.37 -1.61,-1.48 -1.73,-1.86 -1.11,-2.22 -1.36,-1.73 0.62,-1.73 1.98,-0.24 1.11,-0.87 0.99,-1.11 -0.25,-1.48 -1.24,-0.99 -1.85,-0.24 -2.1,-1.11 -5.06,-4.57 -1.11,0.24 -0.74,1.36 -2.96,0.5 -3.22,-0.25 -1.6,-0.25 0,-2.71 -1.61,-0.75 -0.12,-2.71 0.62,-1.98 0,-2.1 -0.87,-2.1 -0.91,-1.6 -0.12,-1.85 0,0 0.37,-1.36 0,0 2.34,0.12 1.61,-0.86 1.85,-1.24 1.36,-1.23 1.36,-1.61 0.98,-0.61 1.36,-1.24 1.24,-0.86 0.12,-0.8 0,0 6.99,1.2 8.61,2.45 4.01,-1 7.31,0.03 1.63,-0.32 1.05,-0.34 1.4,-0.73 0.79,-2.95 3.68,-2.29 2.29,-0.38 2.16,0.38 2.13,-0.31 0.19,-0.32 0,0 0.51,0.78 0.49,0.37 1.24,1.36 0.49,1.85 1.36,0.87 1.23,0.37 1.36,0.62 1.36,0.37 1.11,0.74 4.45,0 1.23,-0.62 3.58,-1.36 3.21,0 0.87,0.37 1.11,0.74 0,0 0.33,0 0,0 0.02,0.74 -1.97,1.12 -2.72,0.74 -0.49,1.6 0.25,1.98 1.11,0.86 1.36,0.5 0.86,0.74 0.37,1.48 1.23,0.86 0.87,1.85 -0.87,0.87 -0.49,1.73 0.49,1.97 1.61,1.24 1.23,1.6 1.36,4.45 0,1.48 -0.99,0.49 -1.11,1.11 -1.97,1.61 -1.36,1.73 0.12,2.1 0.99,2.1 0.99,1.72 0.74,2.1 0.74,0.99 2.35,-0.62 1.23,-0.49 1.48,-0.25 1.61,0.87 2.71,1.11 4.7,3.58 2.34,2.59 1.98,1.11 1.48,1.36 3.83,2.84 0.98,1.73 0.62,1.61 0.25,1.72 1.11,1.98 1.11,1.48 7.78,-0.37 2.59,-0.86 2.47,0 0.5,1.23 -1.86,1.73 -2.59,-0.12 0.25,0.86 -0.37,2.47 0.86,1.24 1.85,1.35 2.6,0.99 5.18,1.48 1.98,0.74 2.34,1.24 1.73,0.49 0,0 z', nombre='VE-B'   WHERE upper(name_state) = 'ANZOÁTEGUI';
UPDATE conf_estados SET mapa='m 312.3402,218.7162 -0.95,1.07 -1.32,0.82 -2.3,0.5 -0.99,2.14 -1.15,1.97 -3.13,4.45 -1.15,0.99 -1.32,2.47 -3.95,1.81 -2.8,0 -2.3,1.15 -2.8,2.8 -2.47,0.98 -2.96,-0.16 -2.47,0.82 -2.14,1.16 -1.49,2.63 -0.65,2.96 0.98,1.65 1.48,-0.33 1.32,1.65 0.17,1.48 -0.66,6.58 -2.31,2.8 -0.82,3.62 -1.48,2.97 0.16,2.8 -0.33,1.81 -1.31,2.3 -2.14,0.66 -0.83,0.82 -0.82,2.31 -1.97,0.99 -1.16,1.48 -1.48,0.49 -0.82,0.99 -1.37,0.93 0,0 -1.41,-0.29 -4.89,-2.48 -2.47,-0.62 -5.68,-0.13 -1.35,-0.86 -4.45,0.99 -0.62,1.36 -0.86,1.23 -1.98,0.62 -5.57,-1.31 -1.27,-0.09 -1.43,0.9 -1.11,1.49 -1.76,-1.3 -1.63,0.43 -0.62,0.87 -1.72,0.06 -3.84,-1.3 -2.1,0.31 -1.73,0.8 -4.44,0 -1.98,1.63 -2.11,-0.27 -1.14,-0.68 -1.84,-0.55 -3.28,-0.75 -8.75,-1.25 -1.35,-0.06 -1.65,-1.03 -2.84,0.62 -1.85,2.59 -1.12,1.98 -0.72,1.67 -2.36,-0.19 -0.74,-1.11 -0.19,-1.55 -2.43,0.12 -2.58,1.2 -1.72,-1.26 0.13,-2.09 -4.08,-3.09 -4.32,-4.2 -0.49,-3.33 -2.56,-2.28 -5.59,-6.37 -0.37,-1.97 -4.32,-3.71 -5.01,-6.7 -0.6,-2.09 -1.61,-1.73 -1.85,0 -3.33,0.37 -1.48,0.74 -1.11,0.99 -2.09,-0.11 -0.74,-0.5 -0.88,-1.86 -2.84,-0.86 -1.6,-0.74 -2.23,-2.96 -15.31,0 -0.74,0.98 -0.98,0.99 -1.73,1.11 -1.36,1.11 -0.8,0.95 -2.78,-0.08 -1.61,-1.98 -19.810001,-0.25 -1.42,-1.85 -2.23,-0.12 -1.73,1.11 -3.82,0.74 -2.47,1.11 -7.29,0.25 -2.34,-2.1 -0.99,-1.36 -3.33,-3.46 -0.55,-1.11 -0.85,-2.83 0.02,-1.19 0.42,-1.59 1.03,-1.3 -1.85,-1.61 -1.73,-0.86 -4.69,0 -1.09,-0.97 0,0 0.42,-0.94 3.13,-0.16 2.31,0.16 1.31,-0.66 1.32,-0.16 0.99,1.15 1.15,0.99 1.65,0.99 2.47,0.32 2.96,0 1.48,-0.65 1.32,-0.33 0.49,-0.99 0,-0.82 0.99,-1.49 0,0 0.09,-0.29 0,0 3.02,0 1.32,1.32 2.14,1.31 2.63,0.33 1.32,-0.99 0.82,0 0.99,0.99 2.47,0 1.48,-0.82 5.44,-0.17 1.81,0.83 4.11,0.33 1.81,1.31 1.48,1.32 2.800001,0 4.94,2.14 6.16,-0.16 2.3,0.82 3.46,0.49 2.31,-0.82 0.33,-1.32 3.12,0.17 1.98,-1.48 1.97,-2.31 2.64,-0.49 1.81,-1.32 2.14,-6.59 3.13,-0.32 3.45,-0.66 2.14,-2.31 3.13,-2.14 3.95,-0.16 4.94,-3.79 4.45,0.33 2.47,-1.48 5.43,-4.45 4.61,-3.45 3.79,-0.5 7.57,0.17 3.29,0.49 2.31,1.48 4.77,0.33 3.13,2.47 2.47,1.32 3.78,-0.17 4.78,-0.49 3.46,-0.66 4.6,-0.16 2.64,0.33 3.46,0.98 3.78,-1.15 1.48,-0.66 1.65,0 0.66,0.66 5.43,0.99 7.25,0.66 5.76,-0.66 2.3,-0.66 1.98,-1.15 2.3,-0.17 1.44,-0.78 0,0 1.79,2.07 0,0 4.11,1.48 3.63,1.48 2.14,1.65 4.61,1.64 5.43,0.17 4.94,0.49 3.78,1.32 4.45,1.97 4.12,0.83 1.31,-0.99 0.66,-2.8 2.47,-0.16 1.15,-0.83 2.47,-0.49 2.8,1.81 1.15,1.15 2.97,0.33 0.82,2.14 0,0 z', nombre='VE-C'   WHERE upper(name_state) = 'APURE';
UPDATE conf_estados SET mapa='m 260.4302,116.0462 -0.12,-0.18 -2.22,-1.36 -3.83,-0.12 -1.23,-0.37 -0.37,-1.73 0.25,-1.73 0.61,-2.84 0.37,-2.72 0.75,-1.6 0,-1.85 -1.12,-1.24 -2.1,-0.25 -2.84,0.13 -1.85,-0.370002 -1.73,-0.87 -1.11,-1.23 0.13,-1.73 2.22,-2.1 1.11,-0.37 0.17,-0.6 0,0 8.8,-1.14 5.58,-0.38 2.79,-0.51 1.31,-0.08 0,0 0.22,1.85 0.74,0.74 -0.25,1.23 0.99,1.36 0.12,1.11 1.11,0.12 0.74,-0.49 0.5,-0.99 1.11,-0.74 0.99,0.25 0.86,0.99 1.11,0 1.11,-0.62 0,0 0.05,-0.02 0,0 0.18,1.38 0,2.84 1.23,1.730002 0.49,1.11 1.24,1.36 -0.12,1.72 -0.99,0.99 0,1.73 0.86,1.24 4.08,1.48 1.23,0.74 1.36,1.23 1.11,1.24 2.35,0 6.05,0.37 1.97,0.74 2.84,-0.13 0,0 0.18,0 0,0 0,3.34 0.49,1.48 0.74,0.74 0,1.85 -1.11,0.62 -0.37,1.24 -1.36,1.48 0.74,0.86 1.24,1.11 1.48,1.73 0.37,2.1 -1.24,1.23 0.25,3.22 0.86,1.11 0,0.98 -1.11,2.84 -0.98,0.99 -0.5,1.24 -0.74,1.11 -1.48,0.74 -3.58,-1.73 -1.85,-1.11 -3.71,-1.11 -1.36,-0.87 -1.48,-0.49 -1.6,-1.11 -0.87,-1.73 0.25,-1.36 1.48,-0.49 1.61,0 1.23,-0.74 0.62,-1.11 0.99,-1.36 0,-2.35 -1.12,-1.48 -2.22,-0.74 0,-1.11 -1.48,-0.37 -1.6,-0.99 -4.08,-3.21 -1.36,-1.36 -0.86,-1.48 -1.11,-0.74 -3.34,0 -3.95,-1.36 -0.86,-1.23 -1.11,-0.25 -2.72,-0.13 -2.22,0.62 0,0 z', nombre='VE-D'   WHERE upper(name_state) = 'ARAGUA';
UPDATE conf_estados SET mapa='m 256.6902,204.5162 -1.44,0.78 -2.3,0.17 -1.98,1.15 -2.3,0.66 -5.76,0.66 -7.25,-0.66 -5.43,-0.99 -0.66,-0.66 -1.65,0 -1.48,0.66 -3.78,1.15 -3.46,-0.98 -2.64,-0.33 -4.6,0.16 -3.46,0.66 -4.78,0.49 -3.78,0.17 -2.47,-1.32 -3.13,-2.47 -4.77,-0.33 -2.31,-1.48 -3.29,-0.49 -7.57,-0.17 -3.79,0.5 -4.61,3.45 -5.43,4.45 -2.47,1.48 -4.45,-0.33 -4.94,3.79 -3.95,0.16 -3.13,2.14 -2.14,2.31 -3.45,0.66 -3.13,0.32 -2.14,6.59 -1.81,1.32 -2.64,0.49 -1.97,2.31 -1.98,1.48 -3.12,-0.17 -0.33,1.32 -2.31,0.82 -3.46,-0.49 -2.3,-0.82 -6.16,0.16 -4.94,-2.14 -2.800001,0 -1.48,-1.32 -1.81,-1.31 -4.11,-0.33 -1.81,-0.83 -5.44,0.17 -1.48,0.82 -2.47,0 -0.99,-0.99 -0.82,0 -1.32,0.99 -2.63,-0.33 -2.14,-1.31 -1.32,-1.32 -3.02,0 0,0 0.24,-0.86 0,0 0.99,-0.82 1.97,-0.33 1.48,-0.82 2.31,0.82 3.46,0.49 1.97,0.66 2.31,0 1.64,0.66 1.32,0.33 3.62,-0.17 -0.16,-1.15 -0.83,-0.99 -1.31,-0.98 -1.15,-1.16 -0.99,-0.49 -0.99,-1.15 0,-1.65 0.16,-1.64 0.99,-0.33 -0.49,-2.64 0.07,-0.26 0,0 0.97,-0.68 0,0 1.6,-0.87 3.83,-2.59 2.22,-1.11 1.86,-2.1 2.71,-2.1 2.470001,-2.22 1.61,-1.73 1.48,-1.85 0.62,-1.36 0.24,-1.11 0.87,-0.99 0,-3.09 -0.99,-0.99 0,-2.09 -0.99,-0.62 -0.24,-1.11 1.23,0 0.49,-0.99 1.36,-1.36 0,-1.85 0.87,-1.48 0,-1.98 1.23,-2.22 1.24,-0.74 1.48,-1.85 0.99,-0.87 1.23,-1.6 1.11,-1.11 1.61,-0.99 1.72,-0.25 0.99,-0.37 0.99,-1.36 0,-0.24 0.49,-0.75 1.12,-0.12 0.98,-0.86 1.24,-0.25 1.11,-0.86 -0.37,-1.24 1.85,-2.59 0.41,-0.83 0,0 1.81,-0.21 0,0 2.47,-0.86 7.41,-0.13 1.85,0.99 1.36,0 0,0 1.56,0.24 0,0 1.52,0.76 1.31,1.64 1.65,1.16 2.3,1.97 2.31,1.15 2.14,1.81 2.8,0.83 2.96,1.15 1.15,0.99 7.08,0 2.14,1.97 5.27,1.98 2.8,-0.66 2.8,0.99 2.14,1.97 0.99,1.49 1.81,2.3 0.33,1.98 1.97,0.82 0.66,1.32 -1.32,1.15 -0.33,1.48 1.32,0.66 1.65,1.48 1.64,1.81 2.14,0.82 1.49,1.49 0.49,1.81 1.32,0.82 1.31,1.98 2.14,0.98 1.98,-0.33 0.16,-6.75 0.33,-1.81 1.48,-0.49 1.32,-2.31 1.98,-0.98 2.14,-0.66 1.31,-2.14 0.5,-1.98 0.82,-1.31 0.5,-2.14 0,0 -0.14,-2.24 0,0 10.24,0.42 2.31,0.5 1.15,0 0.66,-0.66 3.46,-0.17 1.64,0.5 2.97,2.47 0,0 0.58,0.51 0,0 0.07,0.31 1.82,1.15 -0.17,3.3 1.65,0 1.81,1.15 2.96,1.32 2.96,3.45 2.64,2.8 1.97,0.33 2.47,1.48 -0.16,3.46 1.48,1.65 0.66,3.62 0,0 z', nombre='VE-E'   WHERE upper(name_state) = 'BARINAS';
UPDATE conf_estados SET mapa='m 387.6102,370.6262 0.41,-6.72 0,-1.98 -1.31,-1.65 0,-2.96 -0.5,-2.14 -1.64,-0.49 -1.81,-0.83 -1.48,-0.16 -0.99,0.82 -0.83,0.17 -1.81,-1.16 -2.8,-0.65 -2.79,-2.14 -5.77,-8.24 -2.79,-5.1 -1.65,-2.63 -0.82,-2.47 -0.66,-3.79 -1.32,-1.65 -0.33,-2.79 1.32,-0.83 4.77,-0.49 0.99,-0.99 0,-2.14 -1.81,-1.48 -1.15,-0.66 -0.17,-2.14 1.65,-0.66 0.33,-1.15 0,-1.15 -1.65,-1.32 -1.97,-1.98 -2.14,-2.79 0.16,-0.99 1.48,-0.82 0.5,-1.16 -0.5,-1.31 -0.82,0 -2.14,-1.16 -5.76,0 -1.98,1.48 -2.14,2.64 -2.47,0 -2.3,-2.96 -0.17,-4.78 1.65,-4.28 0.33,-4.44 -0.99,-1.98 -3.13,-0.82 -2.96,0.33 -1.48,0.49 -1.98,0.99 -2.63,6.09 -2.14,2.3 -1.48,2.14 -1.32,1.65 -3.13,1.48 -3.29,-0.16 -1.15,0.99 -2.14,0.98 -1.65,0.17 -0.33,-1.98 -1.15,-0.82 0.16,-3.79 -0.66,-1.31 -2.63,0.33 -3.29,3.12 -2.31,1.81 -0.99,1.32 -4.77,2.47 -2.63,-0.33 -2.64,0.82 -6.25,0.17 -3.79,0.82 -1.32,0.99 -2.3,-0.33 -1.65,-1.81 -4.28,0.33 -0.66,-1.98 0,-2.3 0.83,-1.98 0.16,-1.81 -0.49,-1.48 1.31,-0.66 0,-2.96 -0.98,-1.65 0,-1.97 -1.65,-0.83 -3.29,-0.82 -2.14,0.17 -1.65,-0.5 -0.82,-0.66 -2.14,-0.82 -0.45,0 0,0 0.15,-1.44 0,0 1.37,-0.93 0.82,-0.99 1.48,-0.49 1.16,-1.48 1.97,-0.99 0.82,-2.31 0.83,-0.82 2.14,-0.66 1.31,-2.3 0.33,-1.81 -0.16,-2.8 1.48,-2.97 0.82,-3.62 2.31,-2.8 0.66,-6.58 -0.17,-1.48 -1.32,-1.65 -1.48,0.33 -0.98,-1.65 0.65,-2.96 1.49,-2.63 2.14,-1.16 2.47,-0.82 2.96,0.16 2.47,-0.98 2.8,-2.8 2.3,-1.15 2.8,0 3.95,-1.81 1.32,-2.47 1.15,-0.99 3.13,-4.45 1.15,-1.97 0.99,-2.14 2.3,-0.5 1.32,-0.82 0.95,-1.07 0,0 0.8,0.87 0,0 3.12,0.33 2.97,-2.14 1.31,-1.64 0.66,0.98 1.32,0.17 2.63,0 2.14,0.82 2.64,-0.82 2.14,-0.5 1.81,-2.3 1.81,-1.81 1.32,-2.14 0.98,-0.82 1.82,-0.33 1.31,-0.83 2.47,-0.33 0.99,-0.82 1.98,0.17 0.16,0.82 1.48,0 1.15,0.49 0.83,0.99 1.64,0 0.21,-0.8 0,0 0.01,0.01 0,0 2.22,0.12 2.1,0.5 4.94,1.85 2.1,0.62 2.84,0.61 2.1,0.87 1.48,1.97 2.1,1.36 1.98,1.48 1.6,0.25 1.98,0 3.33,-0.99 1.73,-1.6 3.83,-2.1 4.19,-0.25 0.87,-1.23 -0.12,-1.24 -1.36,-0.74 -1.11,-0.99 -0.37,-1.11 -0.25,-2.47 1.48,-1.73 1.73,-0.98 0.86,-0.74 2.6,0.12 1.85,0.74 1.73,-0.99 4.44,-2.1 2.1,0.13 1.61,1.73 2.47,1.48 1.6,1.97 2.96,0.5 2.47,0.12 2.1,-1.23 1.61,-1.12 0.86,-1.11 2.47,-3.83 1.73,-1.23 1.48,0 2.72,-1.73 1.85,-0.25 1.23,0.75 1.73,0.12 2.23,-0.25 3.21,-1.73 0.86,-0.74 1.61,-0.61 1.6,-0.99 1.36,-2.35 1.23,0 1.73,-0.37 0.99,-1.23 1.36,-1.11 1.73,0.86 1.35,-0.74 1.86,-0.12 4.81,1.85 1.98,0.12 1.48,0.74 1.85,0.13 1.85,-0.99 1.11,-0.87 0.99,-0.98 0.62,-1.61 1.36,-0.37 0.86,0 0.84,-0.32 0,0 -0.01,0.29 0,0 1.48,0 1.48,-1.6 1.48,-1.36 0,0 0.87,-0.31 0.32,2.19 0.12,1.97 0,0 1.24,1.12 0.74,0.98 1.6,1.48 0,0 1.11,1.24 0.5,1.73 0,0 4.32,-0.13 3.09,0.13 2.1,0.74 0,0 4.32,2.96 1.23,1.48 0.99,1.49 0.62,1.23 0.24,1.48 0,0 1.24,0.74 3.09,0.99 1.35,-1.11 1.24,-0.37 0.74,-0.87 1.48,0 1.85,-0.24 1.11,-0.5 2.35,0.62 1.11,2.1 0.74,1.73 1.24,1.97 0.74,1.73 1.36,1.24 0,0 2.34,1.85 2.6,0.37 0,0 1.35,0.74 1.98,0.12 1.36,0.99 1.85,0 0.74,-1.36 1.11,-0.74 1.36,-0.74 2.1,0 1.36,-0.99 1.6,-0.37 0.37,-0.74 2.84,-1.6 1.73,-1.36 1.98,-0.12 1.11,0.61 0.61,0.99 0.75,2.84 1.11,0.87 1.48,0.86 2.1,-1.11 1.36,0.12 0.86,-0.99 1.48,-0.37 0.74,-0.74 0.5,-0.86 1.23,0.12 0.49,0.74 0.75,0 0.37,-0.61 1.85,-0.37 1.11,-0.87 13.62,-0.25 0,0 -1.23,1.45 -1.96,1.19 -6.64,0.29 -1.64,1.64 -1.02,4.07 -0.63,3.36 -2.92,1.85 -2.43,0.57 -0.3,1.98 0.12,1.73 1.14,2.1 1.21,0.28 2.24,2.88 0.83,2.02 -0.18,1.34 -1.77,1.69 1.28,1.29 2.22,1.78 1.35,0.15 1.81,-0.74 1.53,-0.28 2.98,0 1.75,0.59 1.45,1.21 1.39,0.91 1.04,1.94 -0.9,1.88 -2.02,2.01 -2.85,2.36 -3.19,1.53 -1.6,2.08 -2.08,1.12 -3.73,-0.12 -1.36,0.71 -1.02,2.05 -1.39,1.25 -2.57,-0.07 -1.81,-1.05 -2.5,-0.2 -1.32,-0.28 -1.6,2.22 -2.01,0.49 -1.67,0.55 -2.22,-0.07 -1.74,-0.9 -2.63,0.07 -1.36,-1.3 -0.93,0.14 0.2,2.13 1.18,1.39 0.97,1.98 0.22,1.57 -0.36,1.78 1.74,3.1 0.14,10.28 1.18,-0.07 0,2.92 -1.11,0.14 -0.14,1.87 -1.87,1.53 -2.23,1.18 -1.59,1.04 -0.91,2.5 -1.87,1.05 -1.55,0.05 -1.78,1.36 0.64,1.35 12.35,12.71 10.78,13.13 3.78,3.79 1.57,1.71 1.02,1.36 1.84,1.5 2.78,3.6 1.06,4.34 -0.06,1.08 -1.07,0.86 -1.85,2.42 -2.15,2.43 -6.25,3.91 -2.05,1.58 -0.98,1.79 0,2.15 -2.04,2.4 -4.09,0.07 -0.84,0.9 -5,0 -1.18,-1.04 -3.68,0.07 -0.42,4.79 -5.55,0 -2.02,0.83 -1.11,1.18 0.14,2.29 -0.07,1.46 -1.67,1.81 -2.29,0.14 -4.65,-0.14 -0.28,1.18 -2.15,0 -3.13,3.61 -2.15,0.28 -2.08,-0.36 -1.84,0.36 -2.43,-0.21 -2.03,1.07 -1.16,1.11 -2.75,0.38 -3.17,-1.28 -2.56,-0.45 -1.66,-0.91 -2.28,-0.71 -1.2,-0.25 -1.52,0.07 -1.05,1.53 -2.15,1.18 -0.79,1.25 0.03,1.8 -2.02,1.18 -5.83,-0.27 -1.72,0.28 -1.3,0.8 -0.87,1.92 0.36,2.93 1.07,1.07 0.71,3 1.19,2.8 -0.48,1.53 -0.93,1.87 -0.92,1.37 -1.5,0.85 -1.65,1.47 -2.63,-0.04 -1.43,-0.57 -9.85,-9.35 -4.5,-5.36 -1.25,-1.92 -1.5,-0.86 -3.92,0.18 -0.29,1.32 -1.07,0.14 0.07,1.64 -0.3,1.34 -1.88,0.35 -1.53,-0.62 -1.5,-1.35 -0.93,-1.14 -1.14,-1 -1.64,0.15 -1.86,-0.15 -1.47,1.26 -3.61,-0.07 -1.04,-0.97 -1.67,-0.14 -0.56,1.18 -1.59,-0.28 -0.21,1.19 -1.88,0.69 -0.75,1.25 -1.17,-0.12 -1.29,-0.37 -0.74,-1.01 -1.21,-5.47 -1.57,-1.94 -2.13,-1.3 -3.61,-0.55 -1.21,-0.54 -3.83,-0.27 -1.95,0.03 -2.23,0.6 -1.64,0.27 -3.22,0.09 -1.39,-0.83 -1.57,-2.32 z', nombre='VE-F'   WHERE upper(name_state) = 'BOLÍVAR';
UPDATE conf_estados SET mapa='m 247.5202,92.916198 -0.17,0.6 -1.11,0.37 -2.22,2.1 -0.13,1.73 1.11,1.23 1.73,0.87 1.85,0.370002 2.84,-0.13 2.1,0.25 1.12,1.24 0,1.85 -0.75,1.6 -0.37,2.72 -0.61,2.84 -0.25,1.73 0.37,1.73 1.23,0.37 3.83,0.12 2.22,1.36 0.12,0.18 0,0 -0.36,2.16 0,0 -0.13,0.99 -2.09,-0.12 -0.99,1.11 -1.61,0.25 -0.99,0.61 -0.74,1.12 -1.72,-0.13 -0.13,-0.86 -0.86,-0.99 -2.6,-1.98 -1.48,0.13 -0.74,0.86 -0.24,1.36 0,0 0,1.36 0,0 -4.57,0.25 -2.47,-1.73 -2.47,-0.37 -2.72,-0.74 -2.59,-1.61 -1.11,-1.36 -1.48,-0.24 -0.62,-1.86 0.37,-1.35 0.99,-0.75 0.98,-1.48 -0.12,-0.98 -1.23,-0.99 -0.87,0.61 -0.99,1.12 -2.71,1.85 -0.87,1.36 0,0 -1.11,-0.25 0,0 -0.37,-1.73 -0.12,-1.36 -0.74,-0.86 0,-2.59 0.74,-1.12 0.62,-1.35 0.98,-1.24 0,-0.99 -0.86,-1.48 -1.36,-0.860002 -0.25,-0.99 0.25,-3.7 0.99,-2.23 -0.25,-1.97 1.11,-0.37 1.36,0.86 1.36,-0.12 0.62,-0.5 1.6,-0.37 1.41,0 0,0 1.36,1.58 1.59,1.42 8.13,0 1.52,0.89 1.75,-0.8 z', nombre='VE-G'   WHERE upper(name_state) = 'CARABOBO';
UPDATE conf_estados SET mapa='m 245.7502,120.8062 0.38,0.17 1.48,1.98 1.32,1.31 0.49,1.16 0.5,1.97 -0.66,1.15 -1.32,1.65 -1.97,1.15 -2.47,0.66 -1.65,1.15 -1.32,0.66 0,2.64 0.33,2.79 1.16,0.66 0.33,6.26 1.15,1.48 1.15,2.47 0,8.73 -1.32,1.81 -1.31,0.98 -2.31,2.47 -0.49,1.48 -2.47,3.79 0.33,1.32 0.99,0.82 0.16,7.08 -1.48,0.49 0.16,1.65 0,0 -1.31,-1.15 0,0 -2.97,-2.47 -1.64,-0.5 -3.46,0.17 -0.66,0.66 -1.15,0 -2.31,-0.5 -10.24,-0.42 0,0 -0.03,-0.56 0,0 -1.65,-1.32 -1.81,-0.82 -0.49,-1.98 -1.32,-1.65 -1.48,-0.33 -1.81,-1.15 0.99,-0.99 1.64,-0.82 0.99,-0.66 2.64,-0.33 0.82,-1.15 -0.82,-2.14 -0.5,-2.14 -0.99,-1.97 0.5,-1.65 1.81,-1.98 -3.13,-2.79 0,-1.98 -0.49,-1.65 -1.81,-0.49 -3.13,-3.29 -2.47,-1.32 -0.33,-1.81 0.16,-1.48 -3.12,-5.76 0,-1.82 -0.17,-1.48 -1.32,-0.82 0,-1.65 -1.68,-4.18 0,0 0.42,-0.48 0,0 1.36,-0.98 0.25,-3.21 0.86,-0.13 0.25,-1.97 -0.13,-2.23 0.08,-1.04 0,0 1.32,-0.19 0,0 0.99,-0.99 1.73,0.13 2.47,2.34 1.72,1.11 1.24,0.62 3.08,-0.99 0.38,-0.99 1.6,-0.98 2.59,-0.13 -0.12,1.36 0.37,1.98 0.86,0.86 1.49,-0.12 2.71,-1.48 1.11,-1.24 1.24,-0.49 0.73,-1.59 0,0 0.5,0.11 0,0 0.87,-1.36 2.71,-1.85 0.99,-1.12 0.87,-0.61 1.23,0.99 0.12,0.98 -0.98,1.48 -0.99,0.75 -0.37,1.35 0.62,1.86 1.48,0.24 1.11,1.36 2.59,1.61 2.72,0.74 2.47,0.37 2.47,1.73 4.57,-0.25 0,0 z', nombre='VE-H'   WHERE upper(name_state) = 'COJEDES';
UPDATE conf_estados SET mapa='m 200.6802,86.656198 -0.56,-1.12 -0.86,-0.98 -1.24,-0.62 -1.85,-0.12 -2.22,0.74 -1.11,1.11 -2.1,0.24 -1.85,-1.11 -2.35,-0.37 -3.83,-0.86 -4.07,0 -3.09,0.25 -1.85,0.74 -2.35,-0.99 -1.6,-1.24 -1.61,-0.61 -1.48,-0.13 -1.48,-0.86 -1.36,0.86 -0.86,0.99 -2.1,1.24 -1.98,-0.13 -1.23,-0.24 0.49,-1.36 -1.73,0.61 -0.98,0.99 -1.73,0.37 -2.72,0.87 -1.85,0.74 -1.85,0.99 -2.72,0 -1.6,0.61 -1.49,1.48 -1.72,0.62 -2.47,0.13 -0.75,0.74 -1.97,0.98 -6.79,0.37 -1.73,0.62 -3.7,1.73 -4.08,0.49 0,0 -2.84,-0.24 0,0 -3.09,0.12 -2.22,-0.25 -1.85,-1.35 -2.1,-0.75 -3.21,-3.95 -0.62,-1.97 -1.23,-1.73 0.25,-1.61 0.74,-0.98 0.12,-1.24 -0.74,-1.23 -1.24,-1.48 -1.480001,-0.87 -0.99,-1.73 -1.35,-1.11 -1.86,-2.22 -1.75,-1.3 0,0 0.59,-0.61 4.11,-1.75 4.000001,-2.42 1.86,-1.52 2.49,-0.58 4.38,-2.36 2.85,-1.13 3.91,-0.84 3.62,-0.38 1.49,0.1 3.29,-0.25 1.69,-0.28 0.73,-0.66 2,-1.21 1.12,-0.1 4.33,-1.77 3.72,-0.95 3.05,-1.26 1.22,-1.67 1.99,-1.38 3.54,-0.33 -0.29,-1.56 0.48,-0.82 3.21,0 2.28,1.52 0.99,1.61 1.48,0.12 1.36,-0.12 3.2,-0.59 0.68,-3.89 -1.04,-2.69 -1.84,-3.57 -1.64,-0.51 -1.8,0.06 -1.13,1.12 -5.38,0.64 -7.74,2.09 -0.97,-1.18 -0.37,-2.47 1.24,-0.87 0,-3.08 -2.96,-3.21 -0.4,-1.56 -0.17,-2.2 1.55,-2.67 1.15,-2.91 1.7,-2.71 4.17,-2.7 2.38,-1.08 4.62,0.23 2.53,2.2 1.36,2.53 -0.17,2.03 1.01,2.2 1.36,2.03 0.34,2.88 0.17,3.05 1.86,5.41 2.46,3.64 0.99,2.1 0.44,2.05 1.35,0.84 2.03,1.02 2.06,-1.24 3.7,-0.62 1.01,0.84 1.77,0.39 1.44,-0.86 2.33,-0.96 4.11,0.92 3.35,1.21 3.08,1.33 8.51,0.44 8.1,4.35 2.81,1.77 2.1,0.62 4.94,3.7 1.71,2.49 2.96,3.72 2.62,2.93 0,2.71 -1.43,1.64 1.58,1.18 0.11,0.51 -1.01,1.02 -0.68,1.86 -0.19,1.86 0.63,3.17 1.23,1.73 2.22,4.15 0,0 -4.44,0.66 -6.42,-0.12 -2.1,-0.37 -1.6,-0.74 -1.24,-0.87 -1.11,-1.23 -0.37,-3.46 -2.59,0 -3.95,0.87 -2.72,2.59 0,0 z', nombre='VE-I'   WHERE upper(name_state) = 'FALCÓN';
UPDATE conf_estados SET mapa='m 352.0302,209.0762 -0.21,0.8 -1.64,0 -0.83,-0.99 -1.15,-0.49 -1.48,0 -0.16,-0.82 -1.98,-0.17 -0.99,0.82 -2.47,0.33 -1.31,0.83 -1.82,0.33 -0.98,0.82 -1.32,2.14 -1.81,1.81 -1.81,2.3 -2.14,0.5 -2.64,0.82 -2.14,-0.82 -2.63,0 -1.32,-0.17 -0.66,-0.98 -1.31,1.64 -2.97,2.14 -3.12,-0.33 0,0 -1.65,-1.81 0,0 -0.82,-2.14 -2.97,-0.33 -1.15,-1.15 -2.8,-1.81 -2.47,0.49 -1.15,0.83 -2.47,0.16 -0.66,2.8 -1.31,0.99 -4.12,-0.83 -4.45,-1.97 -3.78,-1.32 -4.94,-0.49 -5.43,-0.17 -4.61,-1.64 -2.14,-1.65 -3.63,-1.48 -4.11,-1.48 0,0 -2.14,-2.47 0,0 -0.66,-3.62 -1.48,-1.65 0.16,-3.46 -2.47,-1.48 -1.97,-0.33 -2.64,-2.8 -2.96,-3.45 -2.96,-1.32 -1.81,-1.15 -1.65,0 0.17,-3.3 -1.82,-1.15 -0.07,-0.31 0,0 0.73,0.64 0,0 -0.16,-1.65 1.48,-0.49 -0.16,-7.08 -0.99,-0.82 -0.33,-1.32 2.47,-3.79 0.49,-1.48 2.31,-2.47 1.31,-0.98 1.32,-1.81 0,-8.73 -1.15,-2.47 -1.15,-1.48 -0.33,-6.26 -1.16,-0.66 -0.33,-2.79 0,-2.64 1.32,-0.66 1.65,-1.15 2.47,-0.66 1.97,-1.15 1.32,-1.65 0.66,-1.15 -0.5,-1.97 -0.49,-1.16 -1.32,-1.31 -1.48,-1.98 -0.38,-0.17 0,0 0,-0.25 0,0 0.24,-1.36 0.74,-0.86 1.48,-0.13 2.6,1.98 0.86,0.99 0.13,0.86 1.72,0.13 0.74,-1.12 0.99,-0.61 1.61,-0.25 0.99,-1.11 2.09,0.12 0.13,-0.99 0,0 0.37,-2.22 0,0 2.22,-0.62 2.72,0.13 1.11,0.25 0.86,1.23 3.95,1.36 3.34,0 1.11,0.74 0.86,1.48 1.36,1.36 4.08,3.21 1.6,0.99 1.48,0.37 0,1.11 2.22,0.74 1.12,1.48 0,2.35 -0.99,1.36 -0.62,1.11 -1.23,0.74 -1.61,0 -1.48,0.49 -0.25,1.36 0.87,1.73 1.6,1.11 1.48,0.49 1.36,0.87 3.71,1.11 1.85,1.11 3.58,1.73 1.48,-0.74 0.74,-1.11 0.5,-1.24 0.98,-0.99 1.11,-2.84 0,-0.98 -0.86,-1.11 -0.25,-3.22 1.24,-1.23 -0.37,-2.1 -1.48,-1.73 -1.24,-1.11 -0.74,-0.86 1.36,-1.48 0.37,-1.24 1.11,-0.62 0,-1.85 -0.74,-0.74 -0.49,-1.48 0,-3.34 0,0 3.53,-0.12 0,0 1.48,-0.37 1.48,-0.99 1.48,-0.49 1.61,-0.74 1.48,-0.37 1.36,0.86 1.85,1.61 1.23,0.74 2.35,-0.37 1.6,0.86 1.73,-0.74 1.73,-0.12 2.22,0.99 1.86,0.12 3.58,-0.12 1.73,-0.5 1.11,-0.49 1.6,0.12 1.24,1.11 1.35,0 0.99,0.62 0.74,0.11 0,0 -0.13,0.51 0,0 0.12,1.85 0.91,1.6 0.87,2.1 0,2.1 -0.62,1.98 0.12,2.71 1.61,0.75 0,2.71 1.6,0.25 3.22,0.25 2.96,-0.5 0.74,-1.36 1.11,-0.24 5.06,4.57 2.1,1.11 1.85,0.24 1.24,0.99 0.25,1.48 -0.99,1.11 -1.11,0.87 -1.98,0.24 -0.62,1.73 1.36,1.73 1.11,2.22 1.73,1.86 1.61,1.48 2.1,0.37 2.47,0.86 2.34,1.24 2.22,2.22 0,2.47 1.61,0.74 1.11,0.99 1.85,1.23 2.84,1.36 0.87,1.98 -0.5,1.97 -1.11,1.85 -0.74,2.23 -0.74,1.23 -2.47,0 -0.99,-1.36 -0.86,-0.86 -3.95,-0.12 -3.71,0.24 -1.36,1.24 -0.74,1.48 -0.24,1.97 1.11,1.36 -0.13,1.98 -1.11,3.21 -0.86,1.97 -0.12,2.23 -1.24,1.85 -1.23,1.48 -1.61,1.73 -1.36,0.99 -3.21,4.07 -1.23,1.85 -1.24,2.47 -1.23,1.36 -0.12,2.72 -0.99,1.73 0.74,1.23 1.48,0.99 0,0 z', nombre='VE-J'   WHERE upper(name_state) = 'GUÁRICO';
UPDATE conf_estados SET mapa='m 197.2602,114.3262 -0.08,1.04 0.13,2.23 -0.25,1.97 -0.86,0.13 -0.25,3.21 -1.36,0.98 0,0 -0.99,1.12 0,0 -1.35,0.98 -1.12,1.11 -1.35,0.75 -1.61,0 -1.73,0.24 -0.49,0.62 -1.11,-0.86 -1.36,-0.37 0.12,-5.31 -0.86,-1.12 -0.62,-0.24 -1.23,1.11 -0.25,1.23 -1.11,1.12 -0.62,0.98 -0.25,1.36 -0.98,0.74 0,1.11 -0.87,0.37 0.12,0.87 1.36,1.85 -1.6,0.37 -1.48,-2.35 -1.12,0.37 0,2.1 1.36,0.62 -0.24,1.11 -1.12,-0.37 -2.46,-1.97 -1.36,0.37 0,0.86 1.23,0.99 -0.12,0.99 -0.99,-0.13 -0.86,-1.23 -2.35,0.49 -0.86,0.87 -0.13,5.55 -0.98,0.87 -1.12,0.24 -1.23,-0.12 -0.62,-0.86 -1.6,0 -1.11,0.98 -1.36,-0.37 -1.73,-1.85 1.23,-1.23 0.87,-1.11 0.49,-1.12 0.5,-1.35 -0.38,-1.36 -1.48,0.24 -0.74,0.5 -0.62,0.86 -3.08,-0.12 -2.22,2.35 -0.75,0.37 0,0 -0.74,2.1 0,0 -1.23,-0.87 -0.49,-1.48 -2.6,-2.96 0,-1.98 1.11,-1.73 0.5,-1.11 3.7,-0.37 0.87,-0.37 -0.74,-1.48 -2.1,-0.5 -1.61,-1.35 -1.85,-0.99 -1.73,-0.25 -1.23,-1.48 -1.49,0 -1.11,0.62 -2.1,-0.87 -1.6,-0.86 -0.99,-1.36 -1.23,-0.74 -1.36,0.49 -1.98,0.5 -1.48,-0.87 -1.73,-0.24 0,-2.84 -1.23,-0.99 -1.48,-0.62 0.12,-1.23 -1.61,-0.87 -1.6,0.74 -0.74,1.24 -0.25,1.97 0.25,0.99 -1.13,0.3 0,0 0.03,-0.2 0,0 -2.59,-0.25 -2.1,-1.11 -0.87,-0.74 -1.48,-0.49 -1.85,-1.24 -0.86,-1.85 0.74,-1.61 2.34,-1.35 3.95,-1.98 2.1,-1.85 0,-1.98 0.74,-1.11 0.13,-1.11 -0.87,-0.860002 -0.99,0.240002 -0.98,-0.610002 -0.25,-0.87 0.12,-1.23 -0.37,-1.61 -0.74,-0.74 -0.05,-0.9 0,0 2.73,0.23 0,0 4.08,-0.49 3.7,-1.73 1.73,-0.62 6.79,-0.37 1.97,-0.98 0.75,-0.74 2.47,-0.13 1.72,-0.62 1.49,-1.48 1.6,-0.61 2.72,0 1.85,-0.99 1.85,-0.74 2.72,-0.87 1.73,-0.37 0.98,-0.99 1.73,-0.61 -0.49,1.36 1.23,0.24 1.98,0.13 2.1,-1.24 0.86,-0.99 1.36,-0.86 1.48,0.86 1.48,0.13 1.61,0.61 1.6,1.24 2.35,0.99 1.85,-0.74 3.09,-0.25 4.07,0 3.83,0.86 2.35,0.37 1.85,1.11 2.1,-0.24 1.11,-1.11 2.22,-0.74 1.85,0.12 1.24,0.62 0.86,0.98 0.56,1.12 0,0 -0.25,0.07 0,0 -0.49,0.62 -1.11,0.86 -0.99,1.36 -1.61,0.62 -2.34,0.24 -1.73,0.99 -0.87,1.36 -0.37,2.35 2.1,3.33 0.13,3.330002 0.98,1.11 0,1.36 -4.93,0.13 -0.38,1.6 -0.98,1.24 -1.48,0.37 -1.36,0.98 -0.37,0.87 0.74,1.23 1.11,0.74 1.97,2.35 -0.86,1.23 -0.25,1.24 0.5,1.97 1.11,0.99 1.36,0.74 1.11,0.37 0.86,-1.35 0.25,-2.35 1.11,-1.36 1.11,-0.99 2.1,0.13 0,0 z', nombre='VE-K'   WHERE upper(name_state) = 'LARA';
UPDATE conf_estados SET mapa='m 126.3502,159.5362 -0.41,0.83 -1.85,2.59 0.37,1.24 -1.11,0.86 -1.24,0.25 -0.98,0.86 -1.12,0.12 -0.49,0.75 0,0.24 -0.99,1.36 -0.99,0.37 -1.72,0.25 -1.61,0.99 -1.11,1.11 -1.23,1.6 -0.99,0.87 -1.48,1.85 -1.24,0.74 -1.23,2.22 0,1.98 -0.87,1.48 0,1.85 -1.36,1.36 -0.49,0.99 -1.23,0 0.24,1.11 0.99,0.62 0,2.09 0.99,0.99 0,3.09 -0.87,0.99 -0.24,1.11 -0.62,1.36 -1.48,1.85 -1.61,1.73 -2.470001,2.22 -2.71,2.1 -1.86,2.1 -2.22,1.11 -3.83,2.59 -1.6,0.87 0,0 -1.24,0.86 0,0 -0.98,-0.49 -0.74,-0.74 -0.87,-1.24 0.13,-3.58 -0.38,-3.33 -0.61,-1.24 -1.73,-0.86 -3.71,0 -1.35,-0.25 -1.36,-1.73 -3.46,-2.84 -3.21,-0.49 -1.6,-0.62 0.12,-1.85 1.48,-1.48 0.99,-1.48 0.74,-1.61 1.48,-0.62 0,-4.44 0.25,-1.85 1.11,-1.24 -0.37,-1.23 -0.87,-0.99 -0.37,-2.22 -0.86,-0.25 -0.33,-1.49 0,0 1.01,-0.34 0,0 1.48,0.13 1.85,0.74 1.73,0.24 0.87,-3.58 1.48,-0.24 2.1,-1.24 3.58,-3.83 3.33,-1.85 1.73,-2.1 2.59,-1.23 2.59,-2.47 1.12,-0.87 1.6,-0.86 -0.12,-1.36 -0.62,-0.25 -0.62,-0.86 0.13,-1.98 -0.08,-0.44 0,0 1.01,-0.69 0.72,-0.83 0.33,-1.07 2.03,0.07 0,0 0.49,0.74 1.24,0.13 1.110001,0.98 1.6,0.99 0.87,1.36 1.48,0.49 0.86,-0.86 2.47,-0.12 0.99,-0.87 0,0 1.37,0.11 0,0 0.36,0.68 1.36,0 3.09,1.48 3.7,3.95 1.73,1.61 1.73,-2.59 2.22,0.24 1.6,1.24 0,0 z', nombre='VE-L'   WHERE upper(name_state) = 'MÉRIDA';
UPDATE conf_estados SET mapa='m 338.7902,117.0862 -0.74,-0.11 -0.99,-0.62 -1.35,0 -1.24,-1.11 -1.6,-0.12 -1.11,0.49 -1.73,0.5 -3.58,0.12 -1.86,-0.12 -2.22,-0.99 -1.73,0.12 -1.73,0.74 -1.6,-0.86 -2.35,0.37 -1.23,-0.74 -1.85,-1.61 -1.36,-0.86 -1.48,0.37 -1.61,0.74 -1.48,0.49 -1.48,0.99 -1.48,0.37 0,0 -3.71,0.12 0,0 -2.84,0.13 -1.97,-0.74 -6.05,-0.37 -2.35,0 -1.11,-1.24 -1.36,-1.23 -1.23,-0.74 -4.08,-1.48 -0.86,-1.24 0,-1.73 0.99,-0.99 0.12,-1.72 -1.24,-1.36 -0.49,-1.11 -1.23,-1.730002 0,-2.84 -0.18,-1.38 0,0 1.12,-0.54 0,0 1.06,1.3 0.98,0.62 2.6,0.24 1.97,-0.24 5.43,-2.84 1.24,-0.99 0.25,-2.69 0,0 2.1,0.22 0,0 1.72,-0.86 1.61,-0.37 10.25,0.12 1.36,0.99 1.11,0.12 1.85,-0.25 1.36,-0.37 0.61,-1.23 -0.11,-1.91 0,0 1.85,-1.42 2.35,0.18 0.89,0.36 1.31,1.93 3.3,0.13 1.04,0.44 -0.02,1.21 -1.6,1.01 -0.06,1.2 0.95,1.23 1.32,1.3 4.85,3.86 2.28,1.3 3.05,3.040002 2.27,1.74 0.94,0.16 1.77,-0.1 3.78,1.38 4.05,0.45 5.02,1.59 0,0 -0.12,0.8 -1.24,0.86 -1.36,1.24 -0.98,0.61 -1.36,1.61 -1.36,1.23 -1.85,1.24 -1.61,0.86 -2.34,-0.12 0,0 z', nombre='VE-M'   WHERE upper(name_state) = 'MIRANDA';
UPDATE conf_estados SET mapa='m 486.8702,126.9362 -0.87,2.47 -1.65,1.98 0,0 -1.98,1.97 0.66,2.31 0,0 1.32,3.78 -0.16,3.29 0,3.3 1.31,1.31 1.98,1.16 0.16,2.96 0,0 0.5,1.65 -0.17,1.81 0,0 0.99,0.82 2.14,0.49 4.11,0.5 1.98,-0.5 1.15,-0.98 0,0 1.15,0.16 0.66,2.31 0,0 0.17,6.25 -0.66,4.12 -1.15,1.97 -2.31,1.32 -3.95,4.44 -0.82,1.32 -2.31,0.66 -2.47,2.14 -2.96,0.66 -3.29,0.16 0,0 -2.8,1.16 -0.07,0.22 -0.33,0.12 0,0 -1.48,1.36 -1.48,1.6 -1.48,0 0,0 0.12,-2.59 0,0 -1.73,-0.49 -2.34,-1.24 -1.98,-0.74 -5.18,-1.48 -2.6,-0.99 -1.85,-1.35 -0.86,-1.24 0.37,-2.47 -0.25,-0.86 2.59,0.12 1.86,-1.73 -0.5,-1.23 -2.47,0 -2.59,0.86 -7.78,0.37 -1.11,-1.48 -1.11,-1.98 -0.25,-1.72 -0.62,-1.61 -0.98,-1.73 -3.83,-2.84 -1.48,-1.36 -1.98,-1.11 -2.34,-2.59 -4.7,-3.58 -2.71,-1.11 -1.61,-0.87 -1.48,0.25 -1.23,0.49 -2.35,0.62 -0.74,-0.99 -0.74,-2.1 -0.99,-1.72 -0.99,-2.1 -0.12,-2.1 1.36,-1.73 1.97,-1.61 1.11,-1.11 0.99,-0.49 0,-1.48 -1.36,-4.45 -1.23,-1.6 -1.61,-1.24 -0.49,-1.97 0.49,-1.73 0.87,-0.87 -0.87,-1.85 -1.23,-0.86 -0.37,-1.48 -0.86,-0.74 -1.36,-0.5 -1.11,-0.86 -0.25,-1.98 0.49,-1.6 2.72,-0.74 1.97,-1.12 -0.02,-0.74 0,0 0.9,0 0,0 2.22,-1.11 1.48,-0.12 2.23,-1.11 1.11,-1.61 1.36,-0.99 5.8,-0.12 0.86,-0.62 1.61,-0.74 0.62,-0.74 0.61,-0.37 0.87,-0.86 0.86,-0.5 1.61,0.37 0.86,0.5 1.36,0.99 0.62,0.98 1.97,0.25 1.24,1.24 1.23,0.74 0.99,-0.25 1.48,0.74 4.08,0.37 2.47,0.99 1.6,1.36 3.21,2.34 1.98,-0.12 1.23,0.86 2.47,0.25 2.22,-0.12 1.86,-0.74 4.07,0.12 0.49,-1.61 0.99,-1.23 -0.29,-3.48 0,0 0.37,-0.57 1.49,-0.11 1.78,0.47 1.5,1.25 0.55,0.95 0.82,2.76 1.91,4.99 0.19,3.16 1.77,1.76 1.01,2.31 0.06,2.41 z', nombre='VE-N'   WHERE upper(name_state) = 'MONAGAS';
UPDATE conf_estados SET mapa='m 419.0302,81.806198 -1.45,0.1 -1.45,-0.39 0.48,-0.19 -1.06,-0.29 -0.19,-0.68 1.16,-1.45 2.32,0.19 0.39,0.58 0.19,1.06 -0.39,1.07 z m -10.04,-1.94 -1.83,0.1 -1.54,-0.1 -0.77,-1.16 0.39,-0.68 3.57,-0.19 0.68,0.48 0.1,0.77 -0.6,0.78 z m 15.26,-10.04 0,1.16 -0.97,0.48 -0.48,0.77 -1.45,0.1 -0.58,0.77 -1.06,0.77 -2.41,0.39 -0.97,0.77 -4.63,0.1 -0.68,-0.29 -0.68,-0.97 -1.64,-1.25 -1.74,-0.48 -0.67,1.16 -3.77,-0.1 -0.87,-0.87 -2.03,-0.19 -1.54,-1.06 0.1,-2.03 1.45,-1.35 2.51,-0.48 3.67,0.19 1.83,1.93 1.74,0.97 2.7,0.29 1.64,-0.87 1.35,-0.29 0.96,-2.9 0.68,-0.29 1.35,-1.16 1.25,-1.35 0.48,-0.1 1.74,0.19 0.48,0.58 0.39,1.54 1.35,0.97 0.39,1.35 0.11,1.55 z', nombre='VE-O'   WHERE upper(name_state) = 'NUEVA ESPARTA';
UPDATE conf_estados SET mapa='m 194.1702,124.3662 1.68,4.18 0,1.65 1.32,0.82 0.17,1.48 0,1.82 3.12,5.76 -0.16,1.48 0.33,1.81 2.47,1.32 3.13,3.29 1.81,0.49 0.49,1.65 0,1.98 3.13,2.79 -1.81,1.98 -0.5,1.65 0.99,1.97 0.5,2.14 0.82,2.14 -0.82,1.15 -2.64,0.33 -0.99,0.66 -1.64,0.82 -0.99,0.99 1.81,1.15 1.48,0.33 1.32,1.65 0.49,1.98 1.81,0.82 1.65,1.32 0,0 0.17,2.8 0,0 -0.5,2.14 -0.82,1.31 -0.5,1.98 -1.31,2.14 -2.14,0.66 -1.98,0.98 -1.32,2.31 -1.48,0.49 -0.33,1.81 -0.16,6.75 -1.98,0.33 -2.14,-0.98 -1.31,-1.98 -1.32,-0.82 -0.49,-1.81 -1.49,-1.49 -2.14,-0.82 -1.64,-1.81 -1.65,-1.48 -1.32,-0.66 0.33,-1.48 1.32,-1.15 -0.66,-1.32 -1.97,-0.82 -0.33,-1.98 -1.81,-2.3 -0.99,-1.49 -2.14,-1.97 -2.8,-0.99 -2.8,0.66 -5.27,-1.98 -2.14,-1.97 -7.08,0 -1.15,-0.99 -2.96,-1.15 -2.8,-0.83 -2.14,-1.81 -2.31,-1.15 -2.3,-1.97 -1.65,-1.16 -1.31,-1.64 -1.52,-0.76 0,0 0.05,0.01 0,0 1.48,-1.98 2.22,-2.59 0.49,-1.24 0.25,-1.23 0,-3.09 -2.72,-0.98 -0.49,-2.1 2.22,-2.6 2.35,-1.85 0.62,-1.48 0.35,-1.54 0,0 0.63,-1.79 0,0 0.75,-0.37 2.22,-2.35 3.08,0.12 0.62,-0.86 0.74,-0.5 1.48,-0.24 0.38,1.36 -0.5,1.35 -0.49,1.12 -0.87,1.11 -1.23,1.23 1.73,1.85 1.36,0.37 1.11,-0.98 1.6,0 0.62,0.86 1.23,0.12 1.12,-0.24 0.98,-0.87 0.13,-5.55 0.86,-0.87 2.35,-0.49 0.86,1.23 0.99,0.13 0.12,-0.99 -1.23,-0.99 0,-0.86 1.36,-0.37 2.46,1.97 1.12,0.37 0.24,-1.11 -1.36,-0.62 0,-2.1 1.12,-0.37 1.48,2.35 1.6,-0.37 -1.36,-1.85 -0.12,-0.87 0.87,-0.37 0,-1.11 0.98,-0.74 0.25,-1.36 0.62,-0.98 1.11,-1.12 0.25,-1.23 1.23,-1.11 0.62,0.24 0.86,1.12 -0.12,5.31 1.36,0.37 1.11,0.86 0.49,-0.62 1.73,-0.24 1.61,0 1.35,-0.75 1.12,-1.11 1.35,-0.98 0,0 z', nombre='VE-P'   WHERE upper(name_state) = 'PORTUGUESA';
UPDATE conf_estados SET mapa='m 475.6902,105.4462 0.29,3.48 -0.99,1.23 -0.49,1.61 -4.07,-0.12 -1.86,0.74 -2.22,0.12 -2.47,-0.25 -1.23,-0.86 -1.98,0.12 -3.21,-2.34 -1.6,-1.36 -2.47,-0.99 -4.08,-0.37 -1.48,-0.74 -0.99,0.25 -1.23,-0.74 -1.24,-1.24 -1.97,-0.25 -0.62,-0.98 -1.36,-0.99 -0.86,-0.5 -1.61,-0.37 -0.86,0.5 -0.87,0.86 -0.61,0.37 -0.62,0.74 -1.61,0.74 -0.86,0.62 -5.8,0.12 -1.36,0.99 -1.11,1.61 -2.23,1.11 -1.48,0.12 -2.22,1.11 0,0 -1.23,0 0,0 -1.11,-0.74 -0.87,-0.37 -3.21,0 -3.58,1.36 -1.23,0.62 -4.45,0 -1.11,-0.74 -1.36,-0.37 -1.36,-0.62 -1.23,-0.37 -1.36,-0.87 -0.49,-1.85 -1.24,-1.36 -0.49,-0.37 -0.51,-0.78 0,0 1.36,-2.29 2.28,-1.820002 1.02,-0.97 1.4,-1.05 2.97,-0.37 3.75,-3.91 2.03,0.26 1.78,1.39 2.28,0.64 7.49,0.12 3.04,-0.23 0.86,-0.76 3.44,-1.13 0.79,-0.61 -4.07,-0.95 -2.03,-1.14 -2.79,0 -1.27,-1.78 -2.03,0 -0.51,1.02 -7.23,-0.25 -3.84,1.82 -1.03,-0.91 0.45,-0.87 -1.55,-2.46 0.89,-0.76 1.19,-0.56 1.47,1.07 5.97,-0.25 0.38,-1.02 3.3,-0.12 0.76,1.26 4.44,0 1.08,-1.25 6.67,0.01 2.03,0.86 4.93,-0.04 6.62,-1.48 5.58,0.13 1.23,-1.38 1.56,-0.53 9.82,-0.23 1.35,1 3.17,0 1.53,-0.89 2.41,-0.51 1.77,-1.02 2.54,0 2.54,0.64 3.68,0.13 3.22,-0.32 4.95,0.85 5.92,0.73 4.07,-0.43 4.05,-0.2 1.78,-0.51 5.32,-0.5 -0.17,1.5 -1.98,0.78 -1.65,1.01 -2.54,0.64 -2.53,0.25 -1.52,0.3 -0.92,-0.3 -1.19,-0.08 -3.83,0.43 -1.19,1.19 -1.12,2.01 -1.31,1.05 -5.24,-0.07 -3.43,-0.32 -2.33,-0.66 -5.21,-0.12 -5.99,1.1 -1.13,0.38 -0.53,0.91 1.28,0.43 0.59,0.96 -0.09,2.01 -0.33,0.94 -1.4,0.32 -0.85,-0.17 -0.86,0.22 -1.12,-0.11 -0.91,0.16 -0.75,0.54 -0.86,0.91 -0.32,4.180002 0.58,0.8 1.39,0 0.54,-0.97 -0.05,-3.210002 0.86,-0.85 0.91,-0.16 0.96,0.48 1.93,-0.06 0.65,-1.12 1.43,-0.7 -0.05,-1.39 0.82,-0.52 1.65,1.77 1.39,0.13 1.27,2.03 0.39,3.370002 0,4.46 1.02,0.33 0.88,-0.69 z', nombre='VE-R'   WHERE upper(name_state) = 'SUCRE';
UPDATE conf_estados SET mapa='m 84.720199,212.9062 -0.07,0.26 0.49,2.64 -0.99,0.33 -0.16,1.64 0,1.65 0.99,1.15 0.99,0.49 1.15,1.16 1.31,0.98 0.83,0.99 0.16,1.15 -3.62,0.17 -1.32,-0.33 -1.64,-0.66 -2.31,0 -1.97,-0.66 -3.46,-0.49 -2.31,-0.82 -1.48,0.82 -1.97,0.33 -0.99,0.82 0,0 -0.33,1.15 0,0 -0.99,1.49 0,0.82 -0.49,0.99 -1.32,0.33 -1.48,0.65 -2.96,0 -2.47,-0.32 -1.65,-0.99 -1.15,-0.99 -0.99,-1.15 -1.32,0.16 -1.31,0.66 -2.31,-0.16 -3.13,0.16 -0.42,0.94 0,0 -2.99,-0.14 -0.49,-2.22 -1.48,-0.5 -0.37,-7.28 0.18,-11.48 1.3,-4.52 1.81,-1.1 1.92,-0.01 0.76,-0.63 0.16,-1.2 -2.06,-2.25 -0.2,-8.75 -0.53,-1.44 -0.6,-0.65 -0.94,-0.25 -0.35,-0.4 0,0 1.99,-0.49 1.36,-1.48 0.99,-0.25 0.24,-1.85 5.68,-2.84 2.47,0 3.71,-1.36 2.34,-2.22 0.87,-0.24 1.23,0.61 0.25,1.24 1.6,0 0.62,-0.99 1.98,-0.49 0,0 0.47,-0.16 0,0 0.33,1.49 0.86,0.25 0.37,2.22 0.87,0.99 0.37,1.23 -1.11,1.24 -0.25,1.85 0,4.44 -1.48,0.62 -0.74,1.61 -0.99,1.48 -1.48,1.48 -0.12,1.85 1.6,0.62 3.21,0.49 3.46,2.84 1.36,1.73 1.35,0.25 3.71,0 1.73,0.86 0.61,1.24 0.38,3.33 -0.13,3.58 0.87,1.24 0.74,0.74 0.98,0.49 0,0 z', nombre='VE-S'   WHERE upper(name_state) = 'TÁCHIRA';
UPDATE conf_estados SET mapa='m 149.6302,138.8962 -0.35,1.54 -0.62,1.48 -2.35,1.85 -2.22,2.6 0.49,2.1 2.72,0.98 0,3.09 -0.25,1.23 -0.49,1.24 -2.22,2.59 -1.48,1.98 0,0 -1.61,-0.25 0,0 -1.36,0 -1.85,-0.99 -7.41,0.13 -2.47,0.86 0,0 -2.1,0.25 0,0 -1.6,-1.24 -2.22,-0.24 -1.73,2.59 -1.73,-1.61 -3.7,-3.95 -3.09,-1.48 -1.36,0 -0.36,-0.68 0,0 0.24,0.01 0,0 0.37,-0.98 0.12,-1.36 -0.86,-1.11 -0.37,-0.87 -0.25,-1.48 -0.99,-1.85 -3.95,-1.48 -1.8,0.02 0,0 0.12,-0.17 0.32,-2.37 -0.03,-5.85 0.34,-1.86 0.17,-3.21 0,0 3.12,1.24 2.34,0.49 3.09,-0.24 1.97,-1.36 1.61,-1.48 0,-1.61 0.49,-0.74 0.74,-1.85 1.36,0.12 1.48,-0.49 0.5,-0.99 -0.25,-3.33 -0.99,-1.24 0,0 0.22,-1.28 0,0 1.13,-0.3 -0.25,-0.99 0.25,-1.97 0.74,-1.24 1.6,-0.74 1.61,0.87 -0.12,1.23 1.48,0.62 1.23,0.99 0,2.84 1.73,0.24 1.48,0.87 1.98,-0.5 1.36,-0.49 1.23,0.74 0.99,1.36 1.6,0.86 2.1,0.87 1.11,-0.62 1.49,0 1.23,1.48 1.73,0.25 1.85,0.99 1.61,1.35 2.1,0.5 0.74,1.48 -0.87,0.37 -3.7,0.37 -0.5,1.11 -1.11,1.73 0,1.98 2.6,2.96 0.49,1.48 1.23,0.87 0,0 z', nombre='VE-T'   WHERE upper(name_state) = 'TRUJILLO';
UPDATE conf_estados SET mapa='m 228.3302,89.026198 0.5,0.91 0.42,0.49 0,0 -1.41,0 -1.6,0.37 -0.62,0.5 -1.36,0.12 -1.36,-0.86 -1.11,0.37 0.25,1.97 -0.99,2.23 -0.25,3.7 0.25,0.99 1.36,0.860002 0.86,1.48 0,0.99 -0.98,1.24 -0.62,1.35 -0.74,1.12 0,2.59 0.74,0.86 0.12,1.36 0.37,1.73 0,0 0.61,0.14 0,0 -0.73,1.59 -1.24,0.49 -1.11,1.24 -2.71,1.48 -1.49,0.12 -0.86,-0.86 -0.37,-1.98 0.12,-1.36 -2.59,0.13 -1.6,0.98 -0.38,0.99 -3.08,0.99 -1.24,-0.62 -1.72,-1.11 -2.47,-2.34 -1.73,-0.13 -0.99,0.99 0,0 -1.73,0.25 0,0 -2.1,-0.13 -1.11,0.99 -1.11,1.36 -0.25,2.35 -0.86,1.35 -1.11,-0.37 -1.36,-0.74 -1.11,-0.99 -0.5,-1.97 0.25,-1.24 0.86,-1.23 -1.97,-2.35 -1.11,-0.74 -0.74,-1.23 0.37,-0.87 1.36,-0.98 1.48,-0.37 0.98,-1.24 0.38,-1.6 4.93,-0.13 0,-1.36 -0.98,-1.11 -0.13,-3.330002 -2.1,-3.33 0.37,-2.35 0.87,-1.36 1.73,-0.99 2.34,-0.24 1.61,-0.62 0.99,-1.36 1.11,-0.86 0.49,-0.62 0,0 1.36,-0.37 0,0 2.72,-2.59 3.95,-0.87 2.59,0 0.37,3.46 1.11,1.23 1.24,0.87 1.6,0.74 2.1,0.37 6.42,0.12 z', nombre='VE-U'   WHERE upper(name_state) = 'YARACUY';
UPDATE conf_estados SET mapa='m 102.7802,143.8662 1.81,-0.02 3.95,1.48 0.99,1.85 0.25,1.48 0.37,0.86 0.86,1.11 -0.12,1.36 -0.37,0.99 0,0 -1.61,-0.12 0,0 -0.99,0.86 -2.47,0.12 -0.86,0.86 -1.48,-0.49 -0.86,-1.36 -1.6,-0.99 -1.110001,-0.99 -1.23,-0.12 -0.49,-0.74 0,0 -0.22,-0.01 0.57,-3.48 0.72,-0.52 1.300001,-0.29 1.9,-0.85 0.69,-0.99 z m -9.980001,-73.330002 1.75,1.3 1.85,2.22 1.36,1.11 0.99,1.73 1.480001,0.86 1.23,1.48 0.74,1.23 -0.12,1.23 -0.74,0.99 -0.25,1.6 1.23,1.73 0.62,1.98 3.21,3.95 2.1,0.74 1.85,1.36 2.22,0.25 3.09,-0.12 0,0 0.11,0.01 0,0 0.05,0.9 0.74,0.74 0.37,1.6 -0.12,1.23 0.25,0.86 0.99,0.620002 0.99,-0.250002 0.86,0.860002 -0.12,1.11 -0.74,1.11 0,1.98 -2.1,1.85 -3.95,1.98 -2.35,1.36 -0.74,1.6 0.86,1.85 1.85,1.23 1.48,0.49 0.86,0.74 2.1,1.11 2.59,0.25 0,0 -0.25,1.48 0,0 0.99,1.23 0.25,3.33 -0.49,0.99 -1.48,0.49 -1.36,-0.12 -0.74,1.85 -0.49,0.74 0,1.6 -1.6,1.48 -1.98,1.36 -3.09,0.25 -2.35,-0.49 -3.12,-1.24 0,0 0,-2.37 0.85,-0.17 -0.34,-1.69 -1.69,-5.75 -1.49,-1.47 -2.180001,-2.5 -2.09,-0.38 -1.01,-2.92 0.02,-3.5 -2.3,-2.08 -1.02,-1.13 -1.07,-0.77 -3.03,-4.01 -2.26,-3.870002 -0.82,-3.08 1.18,-1.02 -0.17,-2.03 -2.37,-0.68 -1.52,-1.52 0.18,-2.3 1.01,-0.74 -0.52,-1.98 -0.16,-4.12 -1.07,-1.07 0.12,-1.36 2.96,-0.25 1.73,0.49 -0.02,-2.05 1.52,-1.69 -0.17,-1.02 -1.09,-0.68 -0.12,-1.73 3.09,0.12 0.49,1.1 0.34,2.54 1.86,-0.51 -0.51,-2.88 0.79,-0.75 z m -14.82,-30.83 -3.55,0.34 -2.89,-0.07 -2.23,0.67 -3.34,1.95 -1.69,2.37 -0.51,2.37 1.01,4.39 -0.12,1.85 1.48,0.62 1.73,4.94 1.36,1.98 1.98,2.1 1.71,2.74 5.58,1.69 0.34,1.18 -1.02,0.34 -2.54,0 -2.03,-0.85 -0.56,1.19 2.72,1.98 0.25,3.21 1.66,4.45 1.3,0.98 0.86,0.99 1.48,0.99 0.08,1.95 -0.82,4.84 -1.07,0.4 -1.6,3.74 -0.83,1.51 -4.72,3.25 -1.71,2.14 -1.36,2.970002 0.06,2.04 -1.11,2.59 -5.11,2.24 -0.68,3.38 -6.42,9.22 2.1,3.68 1.39,1.24 1.7,1.97 1.87,2.71 0.1,4.7 2.33,0.15 7.85,5.25 0.42,1.37 -0.37,2.18 -2.5,0.54 -0.51,1.86 2.53,1.01 0,3.7 0.38,1.58 0.85,1.01 1.14,0.65 2.88,0.9 1.13,0.14 2.01,-0.04 2.45,-0.43 8.11,-2.67 1.02,-0.49 1.17,-0.81 0,0 0.07,0.44 -0.12,1.98 0.62,0.86 0.62,0.25 0.12,1.36 -1.6,0.86 -1.11,0.86 -2.59,2.47 -2.59,1.23 -1.73,2.1 -3.33,1.85 -3.58,3.83 -2.1,1.23 -1.48,0.25 -0.86,3.58 -1.73,-0.25 -1.85,-0.74 -1.48,-0.12 0,0 -1.48,0.49 0,0 -1.98,0.49 -0.62,0.99 -1.6,0 -0.25,-1.23 -1.23,-0.62 -0.86,0.25 -2.35,2.22 -3.7,1.36 -2.52,0 -5.68,2.84 -0.25,1.85 -0.99,0.25 -1.36,1.48 -1.99,0.49 0,0 -9.23,-10.61 -5.8,-22.47 -3.33,-0.62 -1.11,1.11 -2.84,-0.25 -1.16,-1.77 0.18,-1.02 -1.01,-2.66 -1.28,-1.83 -5.9,3.47 -0.9800001,1.25 -2.49,1.1 -3.8,-0.39 -1.66,0.6 -1.41999997,-0.25 0.63,-2.42 3.33999997,-2.77 0.12,-3.83 1.79,-1.52 1.55,-3.17 4.0900001,-3.63 1.58,-2.14 3.96,-8.3 1.23,-1.11 0.25,-2.72 -1.23,-1.03 0.4,-4.32 1.33,-2.92 1.36,-2.35 -0.12,-7.48 1.23,-0.99 0,-4.330002 1.11,-2.22 1.73,-3.95 2.96,-4.32 2.47,-2.35 -0.02,-3.4 3.14,-5.62 4.41,-5.06 0.77,-2.92 1.69,-1.69 3.89,-0.68 4.06,-0.34 3.22,-5.92 5.92,-11.51 3.38,-4.57 3.22,-1.52 6.99,-2.33 2.47,-0.99 12.59,-2.47 2.1,-1.85 3.11,-0.98 1.51,1.35 -0.68,2.37 -2.71,1.35 -2.71,1.18 -8.46,2.04 z', nombre='VE-V'   WHERE upper(name_state) = 'ZULIA';
UPDATE conf_estados SET mapa='m 357.8502,69.576198 1.03,1.03 1.67,-0.39 0.77,0.77 0.26,1.16 1.03,1.29 -0.26,0.9 -0.9,0.9 -0.9,0.77 -1.03,0.13 -3.48,-0.13 -1.16,-0.77 -0.77,-0.13 -0.9,0.13 -1.03,-0.39 -0.9,-1.8 0.13,-1.03 0.51,-0.9 1.29,-1.16 3.6,-1.03 1.04,0.65 z m 99.39,-15.93 -1.54,1.89 -1.2,-1.2 -0.51,-1.54 1.03,-1.72 1.72,-0.17 1.2,0.34 0.69,0.86 -0.17,1.03 -1.22,0.51 z m -58.04,-17.23 -0.32,2.12 -0.86,0.52 -0.86,-1.2 0.17,-1.89 1.72,0 0.15,0.45 z m -79.01,-0.45 -1.29,0 -1.29,-0.64 -0.64,-0.77 -1.42,-1.16 0.9,-0.64 0.52,-0.9 2.96,0 0.64,-0.39 0.51,-1.03 1.03,-1.29 1.54,0.77 0.13,1.42 0.64,1.03 0,1.67 -1.16,1.29 -1.16,0.51 -1.91,0.13 z m 69.39,-1.16 -1.93,0 -1.29,-0.64 -0.64,-1.16 0,-1.55 0.77,-1.29 1.42,-0.9 2.58,-0.13 0.9,1.16 0.26,1.16 0,1.8 -2.07,1.55 z m -87.8,-6.82 0.64,1.67 0,3.73 -0.9,1.93 -1.29,1.03 -1.93,0.52 -2.45,0 -1.8,-0.77 -1.54,-0.13 -2.58,0 -1.03,-1.03 0.13,-0.77 8.75,-0.52 1.29,-1.93 0.39,-0.9 -0.9,-1.42 -1.03,-1.29 0,1.67 -0.77,1.42 -1.03,0.9 -3.86,-0.13 -2.83,-2.45 0,-1.29 0.77,-1.03 1.67,0 1.16,-0.77 1.67,-0.39 2.32,-0.77 2.45,-0.13 1.8,1.16 0.9,1.69 z m -36.44,-0.9 -1.42,0.13 -1.8,-0.39 -0.13,-0.64 0.13,-0.77 1.54,-0.26 -0.13,-0.9 -1.8,-1.16 0.64,-1.03 0.9,-0.64 1.54,0.52 1.42,2.7 -0.39,1.54 -0.5,0.9 z m -8.62,-5.41 0.39,1.8 -0.64,2.06 -1.67,0.64 -2.19,0 -1.3,-1.02 0,-1.55 0.9,-1.16 1.29,0.13 0.13,-1.03 -2.83,-1.16 -1.54,-1.03 1.42,-0.9 1.54,0.13 3.09,1.67 1.41,1.42 z', nombre='VE-W'   WHERE upper(name_state) = 'DEPENDENCIAS FEDERALES';
UPDATE conf_estados SET mapa='m 311.9102,86.916198 0.11,1.91 -0.61,1.23 -1.36,0.37 -1.85,0.25 -1.11,-0.12 -1.36,-0.99 -10.25,-0.12 -1.61,0.37 -1.72,0.86 0,0 -2.35,-0.25 0,0 -5.31,0 -1.23,0.13 -0.87,-1.11 -1.36,0.24 -0.24,1.11 -0.62,0.62 -1.11,0.12 -1.24,2.23 -0.74,0.61 0,0 -0.62,0.75 -1.11,0.49 0,0 -1.11,0.62 -1.11,0 -0.86,-0.99 -0.99,-0.25 -1.11,0.74 -0.5,0.99 -0.74,0.49 -1.11,-0.12 -0.12,-1.11 -0.99,-1.36 0.25,-1.23 -0.74,-0.74 -0.22,-1.85 0,0 6.31,-0.43 5,-1.96 4.68,-1.39 11.2,-1.21 8.88,0 5.71,0.53 1.47,0.42 z', nombre='VE-X'   WHERE upper(name_state) = 'LA GUAIRA';
UPDATE conf_estados SET mapa='m 545.4202,178.5662 -0.92,0.65 -1.7,-1.05 0,-0.92 0.39,-1.05 1.18,-0.52 1.05,0.39 0.39,1.31 -0.39,1.19 z m -4.19,-1.05 -0.92,0.39 -0.79,-0.26 -0.52,-1.05 0.26,-0.65 1.18,-0.79 1.44,0.13 0,1.05 -0.65,1.18 z m 20.02,-5.24 0.26,1.18 -0.92,1.05 -1.96,0.52 -0.79,-0.92 0,-1.31 1.7,0.13 1.71,-0.65 z m -2.09,-0.65 -0.39,0.13 -0.79,-0.92 1.05,-0.92 1.05,1.05 -0.92,0.66 z m -8.9,-2.62 -1.83,1.44 -0.65,1.18 -0.79,1.05 -0.26,0.79 -1.83,0.79 -1.96,0.52 -1.83,0 0.13,-1.96 1.44,-0.26 1.57,0 0.39,-1.18 -0.13,-1.31 -1.05,-0.26 -0.92,0.65 -1.7,1.7 -0.92,-0.26 0,-1.05 1.57,-1.05 0.13,-0.92 0.65,-0.13 1.44,-0.79 1.31,-0.13 1.44,-0.92 3.14,-0.13 1.18,0.39 -0.52,1.84 z m 4.32,-2.09 -0.65,1.31 -1.18,-0.13 -0.39,-0.79 0,-0.92 1.18,-0.52 1.04,1.05 z m 2.88,1.96 -1.96,1.05 -0.26,1.31 -0.92,0.65 -0.92,1.18 -1.44,0.79 -1.05,-0.26 0.39,-1.05 0,-0.52 0.92,-0.79 0.79,-0.92 0.65,-1.18 1.18,-0.26 1.7,0.13 0.92,-0.65 1.31,-2.09 1.05,-0.39 0.39,1.05 -0.65,0.39 -0.79,1.05 -1.31,0.51 z m -7.86,-3.66 -1.44,0.13 -0.52,-0.39 -0.13,-1.05 -0.92,-1.44 4.45,-4.19 0.79,0 1.31,0.39 0.26,1.18 -0.52,1.57 -1.44,1.83 -1.84,1.97 z m 6.29,-6.81 -1.57,0.13 -1.7,-1.44 1.05,-1.05 3.27,1.7 -1.05,0.66 z m -11.78,4.98 -1.7,0 0.65,-1.57 1.05,-1.7 1.96,-1.18 1.44,-1.05 1.57,-1.96 1.31,-0.65 0.52,1.18 -0.52,1.05 -1.44,0.79 -1.57,2.09 -1.31,0.92 -0.65,1.31 -1.31,0.77 z m 8.5,-9.56 -0.65,0.52 -1.44,-0.13 -0.39,-1.05 0.39,-1.44 0.65,-0.79 0.79,-0.39 1.18,0.13 0.65,1.44 -0.52,1.44 -0.66,0.27 z m 6.02,-1.18 0.65,1.05 0.92,-0.13 0.13,1.05 -0.65,1.05 -0.79,0 -0.79,-1.18 -1.44,-0.92 -1.57,-0.79 -0.13,-1.05 0.26,-0.79 1.18,-1.83 0.92,-0.26 0.79,0.79 -0.39,0.65 -0.65,0.39 0.39,1.31 1.17,0.66 z m -8.34,-3.68 -1.66,0.96 -0.75,-0.59 -0.05,-0.6 0.6,-0.54 1.05,-0.65 1.18,-2.62 -0.52,-3.8 -0.46,-1.54 0.41,-0.32 0.11,-1.18 0.96,-0.38 0.78,0.1 1.69,1.17 1.54,1.78 1.23,3.21 -0.21,1.02 -1.61,0.16 -0.75,1.02 -2.03,1.87 -1.51,0.93 z m -13.25,-18.3 -0.52,1.31 -1.05,-0.26 -0.92,-0.92 -0.65,-1.57 0.52,-0.65 2.22,1.44 0.4,0.65 z m -26.18,-4.72 0.8,0.11 0.21,-0.8 -0.75,-1.02 -0.48,-0.96 0.27,-0.64 1.22,-0.85 0.92,0.04 0.7,1.24 1.93,-0.05 2.52,1.02 2.41,0.7 1.13,-0.05 -1.5,-2.25 -1.02,-0.96 -0.32,-1.34 0.43,-0.91 0.86,-0.21 5.09,3.64 3.08,3.48 0.5,0.91 1.38,1.48 1.83,2.53 3.79,1.53 2.58,0.47 4.93,0.14 1.74,-0.06 2.6,1.1 0.98,2.54 0,2.52 0.53,0.66 0.78,4.6 -0.79,1.96 -1.68,1.2 -0.18,-0.34 -1.19,2.19 -0.24,1.58 -1.57,0.5 -1.5,-0.07 -0.36,1.21 -1.79,0.07 -1.5,0.29 -1.71,0.64 -0.71,1.43 1.14,0.71 1.36,-1.07 1.07,-0.64 1.36,0 0.34,1.15 1.23,-0.36 0.93,-0.93 2.21,-1.64 0.93,-0.86 0.43,0.43 -0.07,2.14 -1.93,3.2 -2.02,1.65 -0.9,1.41 -1.49,0.92 -0.51,2.14 -0.7,1.29 0.71,1.14 1,0.57 -1.79,2.5 -2,3.57 -0.14,2.86 -0.93,0.79 -0.29,1.07 0.71,1.07 1.21,0.14 1.64,1.14 1.28,0.43 0.29,0.86 3.14,0.57 1.39,0.89 0.97,-0.24 0.64,-3.98 0.78,-0.87 1.49,-0.12 0.65,1.05 1,-0.36 0.14,-0.64 2.49,-0.41 5.16,-0.43 1.49,0.41 2.43,1.96 7.13,1.02 0.86,-1.33 4.64,0 0.79,-0.93 -2.5,-0.71 -1.28,-0.14 0.02,-1.02 1.17,-0.48 6.45,0.22 1.34,0.25 7.15,2.04 3.5,1.93 3,2.64 1.36,2.93 2.14,1.71 0.93,1.43 0,2 -1.5,1.78 -2.36,-0.14 -1.93,1.21 -1.28,0.64 -1.71,1.28 -0.07,2.33 -1,1.64 0.38,1.53 -1.18,0.99 -2.47,-0.19 -1.38,0.57 -4.64,2.52 -1.71,1.5 -1.03,1.21 0,0 -13.62,0.25 -1.11,0.86 -1.85,0.37 -0.37,0.62 -0.74,0 -0.49,-0.74 -1.24,-0.12 -0.49,0.86 -0.74,0.74 -1.48,0.37 -0.86,0.99 -1.36,-0.12 -2.1,1.11 -1.48,-0.86 -1.11,-0.86 -0.74,-2.84 -0.62,-0.99 -1.11,-0.62 -1.98,0.12 -1.73,1.36 -2.84,1.61 -0.37,0.74 -1.6,0.37 -1.36,0.99 -2.1,0 -1.36,0.74 -1.11,0.74 -0.74,1.36 -1.85,0 -1.36,-0.99 -1.98,-0.12 -1.36,-0.74 0,0 -2.59,-0.37 -2.35,-1.85 0,0 -1.36,-1.23 -0.74,-1.73 -1.24,-1.98 -0.74,-1.73 -1.11,-2.1 -2.35,-0.62 -1.11,0.49 -1.85,0.25 -1.48,0 -0.74,0.86 -1.23,0.37 -1.36,1.11 -3.09,-0.99 -1.23,-0.74 0,0 -0.25,-1.48 -0.62,-1.23 -0.99,-1.48 -1.23,-1.48 -4.32,-2.96 0,0 -2.1,-0.74 -3.09,-0.12 -4.32,0.12 0,0 -0.49,-1.73 -1.11,-1.23 0,0 -1.61,-1.48 -0.74,-0.99 -1.23,-1.11 0,0 -0.1,-1.54 -0.16,-0.03 -0.16,-2.61 -0.57,0.21 0.07,-0.23 2.8,-1.15 0,0 3.29,-0.16 2.96,-0.66 2.47,-2.14 2.3,-0.66 0.82,-1.32 3.95,-4.44 2.3,-1.32 1.15,-1.98 0.66,-4.12 -0.16,-6.26 0,0 -0.66,-2.3 -1.15,-0.16 0,0 -1.15,0.99 -1.98,0.49 -4.12,-0.49 -2.14,-0.49 -0.99,-0.82 0,0 0.17,-1.81 -0.49,-1.65 0,0 -0.17,-2.96 -1.98,-1.15 -1.32,-1.32 0,-3.29 0.16,-3.29 -1.32,-3.79 0,0 -0.66,-2.31 1.98,-1.98 0,0 1.65,-1.98 0.87,-2.47 0,0 -0.02,0.13 1.48,0.86 0.25,-0.31 0.27,-2.46 -0.16,-1.71 0.54,-2.14 0.16,-1.82 0.54,-1.39 0.75,-1.29 1.24,-1.06 0.37,-0.76 1.39,-0.64 1.66,-0.32 3.05,0.38 1.12,0.75 3.43,3.21 6.91,6.96 1.04,0.56 z', nombre='VE-Y'   WHERE upper(name_state) = 'DELTA AMACURO';
UPDATE conf_estados SET mapa='m 387.6102,370.6262 -1.36,-0.8 -0.43,-0.68 -1.01,-0.8 -2.18,-1.05 -0.98,-0.26 -1.56,0.04 -0.81,0.93 -0.09,2.39 1.65,2.46 2.27,0.48 0.83,2.73 1.48,2.87 0.83,1.43 0.68,0.82 4.8,4.89 3.17,2.96 3.56,1.61 2.55,1.41 4.37,3.49 1.85,3.24 0.19,1.76 -0.46,1.76 -1.3,1.2 0.28,2.23 -0.28,3.24 -0.11,6.16 0.39,3.56 2.22,3.2 2.65,2.18 3.09,5.18 2.35,5.07 0.12,6.29 -0.62,1.98 -1.23,1.48 0,1.36 1.11,1.85 3.95,0.86 4.57,1.61 8.95,-1.28 9.55,0.64 0.84,0.61 1.28,3.36 0.74,2.6 -2.51,3.05 -0.08,1.52 -5.19,0.74 -3.03,0.86 -2.28,2.1 -3.7,2.96 -2.35,0.87 -5.68,0.12 -2.84,0.99 -1.73,0.99 -1.73,2.71 -0.61,1.98 -0.25,8.02 -0.86,2.35 -2.6,2.59 -5.18,4.08 -2.84,0 -0.37,-1.73 -2.97,0 -2.1,1.6 -2.96,1.49 -2.84,2.19 -1.73,2.1 -4.32,3.46 -1.97,-0.25 -2.1,-1.23 -2.26,-0.05 -2.31,2.39 -2.38,1.03 -2.68,2.43 -4.57,0.86 -1.24,0.5 -1.28,0.77 -0.33,1.85 -0.31,2.92 -0.57,1.71 -0.22,1.52 -3.28,0 -1.91,0.37 -1.36,0.61 -3,3.27 -0.23,2.83 -1.21,3.78 -2.35,0.86 -2.72,-0.15 -0.72,-0.63 0.48,-1.31 0.62,-0.99 1.48,-1.48 0.74,-1.49 0.12,-5.3 -1.11,-1.73 -0.74,-0.74 -3.58,-0.62 -5.43,0.37 -6.18,3.09 -1.11,1.72 -1.23,1.36 -1.36,1.11 -0.87,1.12 -2.59,0.24 -3.21,1.98 -2.72,0.12 -1.97,-0.99 -1.48,0.87 -3.09,-0.37 -1.2,-1.34 -7.66,-6.5 -1.63,-0.44 -10.56,-9.89 -3.88,-3.36 0.24,-1.32 -0.13,-3.58 -1.6,-0.61 -0.77,-4.75 -0.1,-5.38 -1.97,-1.97 -0.82,-3.02 -2.08,-4.74 -0.37,-3.21 -2.06,-3.3 -0.54,-5.83 -1.6,-0.99 -0.13,-1.48 -1.72,-2.97 -0.5,-2.71 0.99,-1.11 0.74,-1.73 -1.6,-2.52 -1.19,-0.91 -2.74,-1.13 -1.75,-3.1 -7.27,-6.32 -3.08,-0.1 -0.37,-2.96 -1.15,-1.81 -1.32,-1.03 -2.72,0.12 -1.73,-0.74 -1.6,0.12 -3.09,0 -1.73,-0.98 2.1,-1.98 16.05,-13.95 3.21,-0.5 -0.37,-2.96 2.6,-1.97 3.21,-2.84 -0.25,-2.97 -3.39,-1.18 -1.06,-1.29 -2.09,-1.36 0.12,-3.82 -0.62,-2.6 -1.73,-1.85 -1.35,-0.12 -1.73,0.25 -2.96,-1.27 -0.01,-1.33 -0.74,-1.73 -1.23,-3.82 -1.24,-0.67 0.13,-2.35 2.1,-1.72 -0.99,-2.23 -3.46,-1.85 -2.81,-3.03 -0.87,-2.87 0.04,-1.81 0.19,-1.2 0.72,-1.52 0.14,-2.53 -0.74,-2.84 -2.1,-1.36 -0.57,-3.24 0.07,-2.94 1.61,-2.34 0.32,-6.61 -0.2,-7.1 1.61,-2.47 0.24,-1.85 -1.95,-0.13 -0.41,-1.67 0.39,-8.08 1.73,-1.97 1.97,-0.99 2.1,-1.6 1.73,-0.99 1.61,-2.47 0.37,-11.29 4.19,-6.17 2.72,-2.59 0.74,-1.11 2.22,-0.37 -0.24,-3.22 -1.61,-0.24 -0.62,-1.24 -0.37,-1.85 0.1,-0.97 0,0 0.45,0 2.14,0.82 0.82,0.66 1.65,0.5 2.14,-0.17 3.29,0.82 1.65,0.83 0,1.97 0.98,1.65 0,2.96 -1.31,0.66 0.49,1.48 -0.16,1.81 -0.83,1.98 0,2.3 0.66,1.98 4.28,-0.33 1.65,1.81 2.3,0.33 1.32,-0.99 3.79,-0.82 6.25,-0.17 2.64,-0.82 2.63,0.33 4.77,-2.47 0.99,-1.32 2.31,-1.81 3.29,-3.12 2.63,-0.33 0.66,1.31 -0.16,3.79 1.15,0.82 0.33,1.98 1.65,-0.17 2.14,-0.98 1.15,-0.99 3.29,0.16 3.13,-1.48 1.32,-1.65 1.48,-2.14 2.14,-2.3 2.63,-6.09 1.98,-0.99 1.48,-0.49 2.96,-0.33 3.13,0.82 0.99,1.98 -0.33,4.44 -1.65,4.28 0.17,4.78 2.3,2.96 2.47,0 2.14,-2.64 1.98,-1.48 5.76,0 2.14,1.16 0.82,0 0.5,1.31 -0.5,1.16 -1.48,0.82 -0.16,0.99 2.14,2.79 1.97,1.98 1.65,1.32 0,1.15 -0.33,1.15 -1.65,0.66 0.17,2.14 1.15,0.66 1.81,1.48 0,2.14 -0.99,0.99 -4.77,0.49 -1.32,0.83 0.33,2.79 1.32,1.65 0.66,3.79 0.82,2.47 1.65,2.63 2.79,5.1 5.77,8.24 2.79,2.14 2.8,0.65 1.81,1.16 0.83,-0.17 0.99,-0.82 1.48,0.16 1.81,0.83 1.64,0.49 0.5,2.14 0,2.96 1.31,1.65 0,1.98 z', nombre='VE-Z'   WHERE upper(name_state) = 'AMAZONAS';


UPDATE   maest_aeropuertos SET cy='158.9362', cx = '405.7702' WHERE codigo_oaci =  'SVST' ;
UPDATE   maest_aeropuertos SET cy='167.9362', cx = '385.7702' WHERE codigo_oaci =  'SVPF ' ;
UPDATE   maest_aeropuertos SET cy='141.9362', cx = '395.7702' WHERE codigo_oaci =  'SVAN' ;
UPDATE   maest_aeropuertos SET cy='240.7162', cx = '130.3402' WHERE codigo_oaci =  'SVGD' ;
UPDATE   maest_aeropuertos SET cy='175.5162', cx = '135.6902' WHERE codigo_oaci =  'SVBI' ;
UPDATE   maest_aeropuertos SET cy='230.6262', cx = '485.6102' WHERE codigo_oaci =  'SVGT' ;
UPDATE   maest_aeropuertos SET cy='260.6262', cx = '430.6102' WHERE codigo_oaci =  'SVPU' ;
UPDATE   maest_aeropuertos SET cy='335.6262', cx = '555.6102' WHERE codigo_oaci =  'SVSE' ;
UPDATE   maest_aeropuertos SET cy='105.916198', cx = '240.5202' WHERE codigo_oaci =  'SVVA' ;
UPDATE   maest_aeropuertos SET cy='130.8062', cx = '210.7502' WHERE codigo_oaci =  'SVCJ' ;
UPDATE   maest_aeropuertos SET cy='160.0762', cx = '270.0302' WHERE codigo_oaci =  'SVBM' ;
UPDATE   maest_aeropuertos SET cy='105.3262', cx = '176.2602' WHERE codigo_oaci =  'SVBM' ;
UPDATE   maest_aeropuertos SET cy='178.5362', cx = '80.3502' WHERE codigo_oaci =  'SVVG' ;
UPDATE   maest_aeropuertos SET cy='174.0762', cx = '99.3502' WHERE codigo_oaci =  'SVMD' ;
UPDATE   maest_aeropuertos SET cy='93.0862', cx = '315.7902' WHERE codigo_oaci =  'SVHG' ;
UPDATE   maest_aeropuertos SET cy='100.0862', cx = '290.7902' WHERE codigo_oaci =  'SVCS' ;
UPDATE   maest_aeropuertos SET cy='73.806198', cx = '415.0302' WHERE codigo_oaci =  'SVMG' ;
UPDATE   maest_aeropuertos SET cy='80.806198', cx = '414.0302' WHERE codigo_oaci =  'SVIE' ;
UPDATE   maest_aeropuertos SET cy='138.3662', cx = '185.1702' WHERE codigo_oaci =  'SVAC' ;
UPDATE   maest_aeropuertos SET cy='190.9062', cx = '50.720199' WHERE codigo_oaci =  'SVLF' ;
UPDATE   maest_aeropuertos SET cy='205.9062', cx = '45.720199' WHERE codigo_oaci =  'SVSA' ;
UPDATE   maest_aeropuertos SET cy='205.9062', cx = '55.720199' WHERE codigo_oaci =  'SVPM' ;
UPDATE   maest_aeropuertos SET cy='220.9062', cx = '60.720199' WHERE codigo_oaci =  'SVSO' ;
UPDATE   maest_aeropuertos SET cy='96.026198', cx = '205.3302' WHERE codigo_oaci =  'SVSP' ;
UPDATE   maest_aeropuertos SET cy='153.8662', cx = '65.7802' WHERE codigo_oaci =  'SVSZ' ;
UPDATE   maest_aeropuertos SET cy='95.8662', cx = '90.7802' WHERE codigo_oaci =  'SVON' ;
UPDATE   maest_aeropuertos SET cy='85.8662', cx = '75.7802' WHERE codigo_oaci =  'SVMC' ;
UPDATE   maest_aeropuertos SET cy='69.576198', cx = '357.8502' WHERE codigo_oaci =  'SVIT' ;
UPDATE   maest_aeropuertos SET cy='86.916198', cx = '280.9102' WHERE codigo_oaci =  'SVMI' ;
UPDATE   maest_aeropuertos SET cy='305.6262', cx = '260.6102' WHERE codigo_oaci =  'SVPA' ;
UPDATE   maest_aeropuertos SET cy='235.6262', cx = '520.6102' WHERE codigo_oaci =  'SVTM' ;
UPDATE   maest_aeropuertos SET cy='294.6262', cx = '522.6102' WHERE codigo_oaci =  'SVPP' ;




 alter table maest_aeropuertos add column cx varchar(20);
 alter table maest_aeropuertos add column cy varchar(20);

/******************************/

ALTER TABLE proc_vuelos ADD COLUMN informado BOOLEAN DEFAULT false;
ALTER TABLE proc_vuelos ADD COLUMN user_edit_id bigint default null;
ALTER TABLE proc_vuelos ADD COLUMN motivo_anulacion text default '';
ALTER TABLE seg_users ADD COLUMN chg_pwd boolean default false;



UPDATE proc_vuelos SET user_edit_id = user_id;



composer require biscolab/laravel-recaptcha
php artisan vendor:publish --provider="Biscolab\ReCaptcha\ReCaptchaServiceProvider"

/****************************/
DROP TABLE proc_aeronaves_puestos;
DELETE FROM migrations WHERE migration  ='2023_06_20_071219_create_proc_aeronaves_puestos_table';
php artisan migrate --path=database/migrations/2023_06_20_071219_create_proc_aeronaves_puestos_table.php



/******************************************/
php artisan migrate --path=database/migrations/2023_06_20_071219_create_proc_aeronaves_puestos_table.php

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Hitórico de Puestos', 'Hitórico de Puestos', 'plane', 'aeronaves_puestos', '*', false, 4, 5);




/*************************************/


alter table maest_aeropuertos add column estado_id bigint;

/*************************************/
ALTER TABLE public.proc_vuelos ADD COLUMN tipo_aviacion_id bigint DEFAULT 1; -- 1 GENERAL 2: OFICIAL  3: MILITAR
ALTER TABLE public.proc_vuelos ADD COLUMN tipo_operacion_id bigint ;
ALTER TABLE public.proc_vuelos ADD COLUMN orig_dest_id bigint ;

ALTER TABLE public.proc_vuelos ADD COLUMN hora_operacion varchar(10);

UPDATE public.proc_vuelos SET hora_operacion = (CASE WHEN hora_llegada IS NULL THEN  substr(hora_salida::text, 12, 5) WHEN hora_salida IS NULL THEN  substr(hora_llegada::text, 12, 5) else substr(hora_salida::text, 12, 5) END) WHERE hora_operacion IS NULL


COLOCAR EL ESTACIONAMIENTO OBLIGATORIO PARA TASA
QUITARLE LA OPCION DE COORDINADO OPERACIONES DE AEROPUERTO DE PERMITIR EDITAR



php artisan migrate --path=database/migrations/2023_06_02_131802_create_proc_usuarios_aeropuertos_table.php
php artisan migrate --path=database/migrations/2023_06_09_143242_create_proc_vuelos_pasajeros_table.php
php artisan make:model UsuariosAeropuertos
php artisan make:model VuelosPasajeros
/*******************************************/
DROP TABLE  IF EXISTS proc_hangares;
DELETE FROM migrations WHERE migration like '%proc_hangares%';

UPDATE seg_processes  SET actions='*'  WHERE id = 16;
UPDATE seg_profile_processes SET actions='*'  WHERE process_id = 16;

ALTER TABLE maest_membresias ADD COLUMN tipo_membresia_id bigint;
ALTER TABLE maest_membresias ADD COLUMN empresa varchar(200);
ALTER TABLE maest_membresias ADD COLUMN exonerar varchar(200);

UPDATE maest_membresias SET empresa = trim(substr(observacion, 0, POSITION('-' IN observacion ) )) WHERE user_id !=1 ;

UPDATE maest_membresias SET exonerar  =

replace(
replace(
replace(
replace(
replace(
trim(substr(observacion, POSITION('EXONERAR' IN observacion )+8 , length(observacion)))
, 'DOSA', '2')
, 'DOSSA', '2')
, 'TASA', '|1')
, ' ', '')
, 'Y', '')
 WHERE user_id !=1 ;

INSERT INTO seg_processes(name_process, description, icon, route, "actions", requiere_fiscal, "order", menu_id)
VALUES('Vuelos Registrados', 'Vuelos Registrados', 'plane', 'flys', '*', false, 3, 5);




php artisan migrate --path=database/migrations/2023_05_15_135644_create_maest_ubicaciones_hangares_table.php
php artisan migrate --path=database/migrations/2023_05_15_135645_create_proc_hangares_table.php
php artisan migrate --path=database/migrations/2023_05_16_191012_create_maest_objeto_hangares_table.php
php artisan migrate --path=database/migrations/2023_05_16_191035_create_maest_usos_hangares_table.php
php artisan migrate --path=database/migrations/2023_05_16_191105_create_maest_estatus_hangares_table.php


--query art. 28 AVG
alter table proc_proforma add column temp_alt boolean default false;
