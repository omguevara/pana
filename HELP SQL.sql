﻿/****** EXTRAC DATA  PERFILES PROCESOS ****/
SELECT '
    [
        "date_register"=>now(),
        "process_id"=>'||process_id||',
        "profile_id"=>'||profile_id||',
        "actions"=>"'||actions||'"
    ],
' AS migracion
FROM seg_profile_processes
ORDER BY profile_id;


﻿/****** EXTRAC DATA  PERFILES ****/
SELECT '
    [
        "code"=>"'||code||'",
        "name_profile"=>"'||name_profile||'",
        "description"=>"'||description||'",
        "status"=>true
    ],
' AS migracion
FROM seg_profiles
ORDER BY id;

﻿/****** EXTRAC DATA  PROSERV ****/
SELECT '
[
	"codigo"=>"'||COALESCE(codigo, '')||'",
	"nomenclatura"=>"'||COALESCE(nomenclatura, '')||'",
	"descripcion"=>"'||COALESCE(descripcion, '')||'",
	"iva"=>"'||COALESCE(iva,0)||'",
	"valor_dollar"=>"'||COALESCE(valor_dollar,0)||'",
	"valor_petro"=>"'||COALESCE(valor_petro,0)||'",
	"valor_euro"=>"'||COALESCE(valor_euro,0)||'",
	"tipo_vuelo_id"=>"'||COALESCE(tipo_vuelo_id, 1)||'",
	"nacionalidad_id"=>"'||COALESCE(nacionalidad_id, 1)||'",
	"turno_id"=>"'||COALESCE(turno_id, 1)||'",
	"formula"=>"'||COALESCE(formula, '')||'",
	"prodservicio_extra"=>"'||COALESCE(prodservicio_extra, false)||'",
	"aplicable"=>"'||COALESCE(aplicable, '')||'",
	"aplica_estacion"=>"'||COALESCE(aplica_estacion, false)||'",
	"aplica_peso"=>"'||COALESCE(aplica_peso, false)||'",
	"peso_inicio"=>"'||COALESCE(peso_inicio, 0)||'",
	"peso_fin"=>"'||COALESCE(peso_fin, 0)||'",
	"default_tasa"=>"'||COALESCE(default_tasa, false)||'",
	"default_dosa"=>"'||COALESCE(default_dosa, false)||'",
	"default_carga"=>"'||COALESCE(default_carga, false)||'",
	"activo"=>"'||COALESCE(activo, true)||'",
	"user_id"=>1,
	"fecha_registro"=>now(),
	"ip"=>"127.0.0.1"
],
'
FROM maest_prod_servicios
ORDER BY id

