CREATE TABLE proc_vuelos_detalle2 AS 
SELECT aplica_categoria, aplica_estacion, aplica_peso, aplicable, bs, cant, categoria_aplicada, codigo, prodservicio_id, default_carga, default_dosa, default_tasa, descripcion, formula, formula2, full_descripcion, iva, iva2, iva_aplicado, moneda, nacionalidad_id, nomenclatura, peso_inicio, peso_fin, precio, prodservicio_extra, tasa, tipo_vuelo_id, turno_id, valor_dollar, valor_euro, valor_petro, vuelo_id, orden,
    ROW_NUMBER() OVER(PARTITION BY codigo, vuelo_id ORDER BY id) AS repetidos
FROM proc_vuelos_detalle;

DELETE FROM proc_vuelos_detalle;

INSERT INTO proc_vuelos_detalle (aplica_categoria, aplica_estacion, aplica_peso, aplicable, bs, cant, categoria_aplicada, codigo, prodservicio_id, default_carga, default_dosa, default_tasa, descripcion, formula, formula2, full_descripcion, iva, iva2, iva_aplicado, moneda, nacionalidad_id, nomenclatura, peso_inicio, peso_fin, precio, prodservicio_extra, tasa, tipo_vuelo_id, turno_id, valor_dollar, valor_euro, valor_petro, vuelo_id, orden)
SELECT aplica_categoria, aplica_estacion, aplica_peso, aplicable, bs, cant, categoria_aplicada, codigo, prodservicio_id, default_carga, default_dosa, default_tasa, descripcion, formula, formula2, full_descripcion, iva, iva2, iva_aplicado, moneda, nacionalidad_id, nomenclatura, peso_inicio, peso_fin, precio, prodservicio_extra, tasa, tipo_vuelo_id, turno_id, valor_dollar, valor_euro, valor_petro, vuelo_id, orden
FROM proc_vuelos_detalle2
WHERE repetidos = 1;








/*
DROP TABLE IF EXISTS vuelos_detalle_aux;
CREATE TABLE vuelos_detalle_aux AS SELECT * FROM proc_vuelos_detalle ;
ALTER TABLE vuelos_detalle DROP COLUMN id;
DELETE FROM proc_vuelos_detalle WHERE vuelo_id= 694;
CREATE INDEX vuelos_detalle_aux_index ON vuelos_detalle_aux(vuelo_id);


DROP FUNCTION IF EXISTS depurar_repetidos;
CREATE FUNCTION depurar_repetidos() RETURNS void AS $$
DECLARE
     
    vuelos RECORD;
    vuelos_detalle RECORD;


BEGIN
   
    FOR vuelos IN SELECT * FROM proc_vuelos WHERE id = 694 LOOP
        FOR vuelos_detalle IN SELECT * FROM vuelos_detalle_aux WHERE vuelo_id = vuelos.id LOOP
            INSERT INTO vuelos_detalle SELECT
            RAISE NOTICE '%',vuelos_detalle.codigo ;
        END LOOP ;
    END LOOP ;
    

    --    RAISE NOTICE '555';
END;
$$ LANGUAGE plpgsql;

*/


