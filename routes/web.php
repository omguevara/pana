<?php

use Illuminate\Support\Facades\Route;

//use PDF;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::post('/aviacion-general/add-turtle', [App\Http\Controllers\VuelosGeneralesController::class, 'add_turtle'])->name('aviacion_general.add_turtle');

Route::match(['get', 'post'], 'login', [App\Http\Controllers\UserController::class, 'login'])->name('login');
Route::get('logout', [App\Http\Controllers\UserController::class, 'logout'])->name('logout');

Route::get('unauthenticated', function () {
    return view('errors.403');
})->name('errors.403');

Route::get('permission-denied', function () {
    return view('errors.permission_denied');
})->name('errors.permission_denied');

Route::get('calculator', function () {
    return view('tools.calculator');
})->name('tools.calculator');

Route::get('qrcode', function () {
    return QrCode::size(250)
            ->backgroundColor(255, 255, 204)
            ->generate('MyNotePaper');
});

Route::get('refresh-captcha', [App\Http\Controllers\RegistrosController::class, 'refreshCaptcha']);

//Route::match(['get', 'put' ,'post'],'reserva-general', [App\Http\Controllers\RegistrosController::class, 'reserva_general'])->name('reserva_general');
Route::match(['get', 'put', 'post'], 'reserva-general', [App\Http\Controllers\RegistroOnlineController::class, 'reserva_general'])->name('reserva_general');

Route::get('get-placa/{placa}', [App\Http\Controllers\AeronavesController::class, 'get_placa'])->name('get_placa');
Route::post('get-prod-serv-ext', [App\Http\Controllers\ProdserviciosController::class, 'get_prod_serv_ext'])->name('get_prod_serv_ext');

Route::post('get-presupuesto', [App\Http\Controllers\RegistrosController::class, 'get_presupuesto'])->name('get_presupuesto');

Route::post('get-presupuesto-tasa', [App\Http\Controllers\RegistrosController::class, 'get_presupuesto_tasa'])->name('get_presupuesto_tasa');
Route::post('get-presupuesto-dosa', [App\Http\Controllers\RegistrosController::class, 'get_presupuesto_dosa'])->name('get_presupuesto_dosa');

Route::get('get-data-cliente/{tipo_doc}/{doc}', [App\Http\Controllers\ClientesController::class, 'get_data_cliente'])->name('get_data_cliente');

Route::get('get-data-piloto/{tipo_doc}/{doc}', [App\Http\Controllers\PilotosController::class, 'get_data_piloto'])->name('get_data_piloto');

Route::get('a', [App\Http\Controllers\RegistrosController::class, 'a'])->name('a');
//Route::get('/', [App\Http\Controllers\UserController::class, 'home'])->name('home');
Route::get('/', [App\Http\Controllers\UserController::class, 'select'])->name('home');



Route::post('/aeronaves/add', [App\Http\Controllers\AeronavesController::class, 'add'])->name('aeronaves.add');

Route::get('/piloto/get/{ce?}', [App\Http\Controllers\PilotosController::class, 'get'])->name('api.piloto');
Route::get('get_vuelo/{id?}', [App\Http\Controllers\VuelosController::class, 'get'])->name('api.get_vuelo');

Route::match(['get', 'post'], 'forgot-password', [App\Http\Controllers\UserController::class, 'forgot_password'])->name('user.forgot_password');
Route::match(['get', 'post'], 'change-password/{id}', [App\Http\Controllers\UserController::class, 'change_password2'])->name('change_password');

Route::get('/informacion-aeropuerto/{codigo_oaci?}', [App\Http\Controllers\MovimientoController::class, 'informacionAeropuerto'])->name('informacionAeropuerto');
// Route::get('/listadoAeropuertos', [App\Http\Controllers\MovimientoController::class, 'list'])->name('list');
Route::get('videos/{codigo_oaci}', [App\Http\Controllers\MovimientoController::class, 'showVideos'])->name('showVideos');



Route::post('get-aeronaves', [App\Http\Controllers\AeronavesController::class, 'get_aeronaves'])->name('get_aeronaves');
Route::post('get-pilotos', [App\Http\Controllers\AeronavesController::class, 'get_pilotos'])->name('get_pilotos');

Route::group(array('middleware' => array('auth', 'CheckPermision')), function () {
//Route::group(array('middleware' => array('auth')), function () {

    Route::get('set-theme/{theme}', [App\Http\Controllers\UserController::class, 'set_theme'])->name('set_theme');

    Route::post('create-img', [App\Http\Controllers\ReportesController::class, 'create_img'])->name('create_img');

    Route::post('search-vuelos', [App\Http\Controllers\RegistrosController::class, 'search_vuelos'])->name('search_vuelos');

    Route::get('view-invoice/{id}', [App\Http\Controllers\FacturasController::class, 'view_invoice'])->name('view_invoice');

    Route::match(['get', 'post'], 'change-pwd', [App\Http\Controllers\UserController::class, 'change_pwd'])->name('change_pwd');

    Route::get('/aviacion-general/refresh-data/{route_id}', [App\Http\Controllers\VuelosGeneralesController::class, 'refresh_data'])->name('refresh_data');

    Route::post('get-prod-serv-ext2', [App\Http\Controllers\ProdserviciosController::class, 'get_prod_serv_ext2'])->name('get_prod_serv_ext2');

    Route::get('check-reg-online', [App\Http\Controllers\RegistroOnlineController::class, 'checkRegOnline'])->name('check_reg_online');

    Route::get('regenerate-session', [App\Http\Controllers\UserController::class, 'regenerate_session'])->name('regenerate');

    Route::get('getHomologacion/{id}', [App\Http\Controllers\AeronavesController::class, 'get_homologacion'])->name('get_homologacion');

    Route::post('show-proforma', [App\Http\Controllers\VuelosGeneralesController::class, 'proforma'])->name('proforma');

    Route::post('get-mensajes', [App\Http\Controllers\MensajesController::class, 'get_mensajes'])->name('get_mensajes');

    Route::post('/aeronaves/plus', [App\Http\Controllers\AeronavesController::class, 'plus'])->name('aeronaves_plus');
    Route::post('/pilotos/plus', [App\Http\Controllers\PilotosController::class, 'plus'])->name('pilotos_plus');
    Route::post('/get-servs-ext', [App\Http\Controllers\ProdserviciosController::class, 'get_servs_ext'])->name('get_servs_ext');

    Route::post('/get-total-serv', [App\Http\Controllers\ProdserviciosController::class, 'get_total_serv'])->name('get_total_serv');

    Route::post('/get-total-serv2', [App\Http\Controllers\ProdserviciosController::class, 'get_total_serv2'])->name('get_total_serv2');

    Route::post('/get-prodservicios', [App\Http\Controllers\ProdserviciosController::class, 'get_prodservicios'])->name('get_prodservicios');

    Route::post('/get-servicios-operacion', [App\Http\Controllers\ProdserviciosController::class, 'get_servicios_operacion'])->name('get_servicios_operacion');

    Route::get('get-vuelos-fact/{id}/{id2?}', [App\Http\Controllers\VuelosController::class, 'get_vuelos_fact'])->name('get_vuelos_fact');

    Route::get('/view-factura/{id}', [App\Http\Controllers\FacturasController::class, 'view_factura'])->name('view_factura');

    Route::get('/view-factura/{id}', [App\Http\Controllers\FacturasController::class, 'view_factura'])->name('view_factura');
    Route::get('/view-detail-fly/{id}', [App\Http\Controllers\VuelosController::class, 'view_detail_fly'])->name('view_detail_fly');
    Route::get('/view-proforma/{id}', [App\Http\Controllers\RecaudacionController::class, 'view_proforma'])->name('view_proforma');
    Route::get('/reporte-proforma', [App\Http\Controllers\RecaudacionController::class, 'reporte_proforma'])->name('reporte_proforma');
    Route::get('/reporte-cupones', [App\Http\Controllers\RecaudacionController::class, 'reporte_cupones'])->name('reporte_cupones');
    Route::match(['get', 'post'],'/ExcelProforma', [App\Http\Controllers\RecaudacionController::class, 'ExcelProforma'])->name('ExcelProforma');
    Route::match(['get', 'post'],'/ExcelFactura', [App\Http\Controllers\RecaudacionController::class, 'ExcelFactura'])->name('ExcelFactura');
    Route::match(['get', 'post'],'/ExcelTickets', [App\Http\Controllers\LotesTicketsController::class, 'ExcelTickets'])->name('ExcelTickets');

    Route::get('/view-fact/{id}/{tasa?}/{numero?}', [App\Http\Controllers\RecaudacionController::class, 'view_fact'])->name('view_fact');

    Route::get('/get-aeronave/{id}', [App\Http\Controllers\AeronavesController::class, 'get_aeronave'])->name('get_aeronave');

    Route::get('/pdf-proforma/{id}', function ($id) {
        $Proforma = \App\Models\Proforma::find(\App\Helpers\Encryptor::decrypt($id));
        $hideBotonPrint = true;
        $proforma = PDF::loadView("reportes.proforma.first", compact('Proforma', 'hideBotonPrint'));
        return $proforma->download("PROFORMA_" . showCode(trim($Proforma->nro_proforma)) . ".pdf");
    })->name('down_proforma');



    Route::get('/pdf-factura/{id}', function ($id) {
        $Proforma = \App\Models\Proforma::find(\App\Helpers\Encryptor::decrypt($id));
        $hideBotonPrint = true;
        $proformaPDF = PDF::loadView("reportes.proforma.second", compact('Proforma', 'hideBotonPrint'));

        return $proformaPDF->download("FACTURA_" . showCode(trim($Proforma->nro_factura)) . ".pdf");
    })->name('down_factura');

    Route::get('/ventas-pdf', [App\Http\Controllers\PDFTickesController::class, 'generatePdf'])->name('generatePdf');

    Route::get('manual-pdf', [App\Http\Controllers\UserController::class, 'manual_pdf'])->name('manual_pdf');

    /*     * ************************************************ */

    Route::get('manual', [App\Http\Controllers\UserController::class, 'manual'])->name('manual');

    /*
      Route::match(['get', 'post'],'/parametros/iva', [App\Http\Controllers\ParametrosController::class, 'iva'])->name('iva');
      Route::match(['get', 'post'],'/parametros/tasa_dolar', [App\Http\Controllers\ParametrosController::class, 'tasa_dolar'])->name('tasa_dolar');
      Route::match(['get', 'post'],'/parametros/tasa_euro', [App\Http\Controllers\ParametrosController::class, 'tasa_euro'])->name('tasa_euro');
     */
    Route::match(['get', 'post'], '/parametros/params', [App\Http\Controllers\ParametrosController::class, 'params'])->name('params');

    Route::match(['get', 'put', 'post'], '/flys', [App\Http\Controllers\VuelosController::class, 'flys'])->name('flys');
    Route::get('flights', [App\Http\Controllers\VuelosController::class, 'flights'])->name('flights');

    Route::get('/pilotos', [App\Http\Controllers\PilotosController::class, 'index'])->name('pilotos');

    Route::match(['get', 'post'], '/pilotos/create', [App\Http\Controllers\PilotosController::class, 'create'])->name('pilotos.create');
    Route::match(['get', 'post'], '/pilotos/edit/{id}', [App\Http\Controllers\PilotosController::class, 'edit'])->name('pilotos.edit');
    Route::match(['get', 'post'], '/pilotos/disable/{id}', [App\Http\Controllers\PilotosController::class, 'disable'])->name('pilotos.disable');
    Route::match(['get', 'post'], '/pilotos/active/{id}', [App\Http\Controllers\PilotosController::class, 'active'])->name('pilotos.active');

    Route::get('/clientes', [App\Http\Controllers\ClientesController::class, 'index'])->name('clientes');

    Route::match(['get', 'post'], '/clientes/create', [App\Http\Controllers\ClientesController::class, 'create'])->name('clientes.create');
    Route::match(['get', 'post'], '/clientes/edit/{id}', [App\Http\Controllers\ClientesController::class, 'edit'])->name('clientes.edit');
    Route::match(['get', 'post'], '/clientes/disable/{id}', [App\Http\Controllers\ClientesController::class, 'disable'])->name('clientes.disable');
    Route::match(['get', 'post'], '/clientes/active/{id}', [App\Http\Controllers\ClientesController::class, 'active'])->name('clientes.active');

    Route::get('/profiles', [App\Http\Controllers\ProfilesController::class, 'index'])->name('profiles');

    Route::match(['get', 'post'], '/profiles/create', [App\Http\Controllers\ProfilesController::class, 'create'])->name('profiles.create');
    Route::match(['get', 'post'], '/profiles/edit/{id}', [App\Http\Controllers\ProfilesController::class, 'edit'])->name('profiles.edit');
    Route::match(['get', 'post'], '/profiles/disable/{id}', [App\Http\Controllers\ProfilesController::class, 'disable'])->name('profiles.disable');
    Route::match(['get', 'post'], '/profiles/active/{id}', [App\Http\Controllers\ProfilesController::class, 'active'])->name('profiles.active');
    Route::match(['get', 'post'], '/profiles/permissions/{id}', [App\Http\Controllers\ProfilesController::class, 'permissions'])->name('profiles.permissions');

    Route::get('/dashboard', [App\Http\Controllers\UserController::class, 'dashboard'])->name('dashboard');

    Route::get('/users', [App\Http\Controllers\UserController::class, 'index'])->name('users');
    Route::match(['get', 'post'], '/users/create', [App\Http\Controllers\UserController::class, 'create'])->name('users.create');
    Route::match(['get', 'post'], '/users/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('users.edit');
    Route::match(['get', 'post'], '/users/disable/{id}', [App\Http\Controllers\UserController::class, 'disable'])->name('users.disable');
    Route::match(['get', 'post'], '/users/active/{id}', [App\Http\Controllers\UserController::class, 'active'])->name('users.active');
    Route::match(['get', 'post'], '/users/change_password/{id}', [App\Http\Controllers\UserController::class, 'change_password'])->name('users.change_password');

    Route::get('/logged_users', [App\Http\Controllers\UserController::class, 'logged_users'])->name('logged_users');
    Route::match(['get', 'post'], '/logged_users/kill/{id}', [App\Http\Controllers\UserController::class, 'kill'])->name('logged_users.kill');

    Route::get('/restricted_access', [App\Http\Controllers\UserController::class, 'restricted_access'])->name('restricted_access');
    Route::match(['get', 'post'], '/restricted_access/detail/{id}', [App\Http\Controllers\UserController::class, 'see_restricted_access'])->name('restricted_access.detail');

    Route::match(['get', 'post'], 'list-backups', [App\Http\Controllers\UserController::class, 'list_backups'])->name('list_backups');

    Route::get('/access_history', [App\Http\Controllers\UserController::class, 'access_history'])->name('access_history');

    Route::get('/categorias', [App\Http\Controllers\CategoriaController::class, 'index'])->name('categorias');

    Route::match(['get', 'post'], '/categorias/create', [App\Http\Controllers\CategoriaController::class, 'create'])->name('categorias.create');
    Route::match(['get', 'post'], '/categorias/edit/{id}', [App\Http\Controllers\CategoriaController::class, 'edit'])->name('categorias.edit');
    Route::match(['get', 'post'], '/categorias/disable/{id}', [App\Http\Controllers\CategoriaController::class, 'disable'])->name('categorias.disable');
    Route::match(['get', 'post'], '/categorias/active/{id}', [App\Http\Controllers\CategoriaController::class, 'active'])->name('categorias.active');

    Route::get('/aeropuertos', [App\Http\Controllers\AeropuertosController::class, 'index'])->name('aeropuertos');

    Route::match(['get', 'post'], '/aeropuertos/create', [App\Http\Controllers\AeropuertosController::class, 'create'])->name('aeropuertos.create');
    Route::match(['get', 'post'], '/aeropuertos/edit/{id}', [App\Http\Controllers\AeropuertosController::class, 'edit'])->name('aeropuertos.edit');
    Route::match(['get', 'post'], '/aeropuertos/disable/{id}', [App\Http\Controllers\AeropuertosController::class, 'disable'])->name('aeropuertos.disable');
    Route::match(['get', 'post'], '/aeropuertos/active/{id}', [App\Http\Controllers\AeropuertosController::class, 'active'])->name('aeropuertos.active');
    Route::match(['get', 'post'], '/aeropuertos/holidays/{id}', [App\Http\Controllers\AeropuertosController::class, 'holidays'])->name('aeropuertos.holidays');
    Route::match(['get', 'post'], '/aeropuertos/airlines/{id}', [App\Http\Controllers\AeropuertosController::class, 'airlines'])->name('aeropuertos.airlines');
    Route::get('/aeropuertos/search-airlines', [App\Http\Controllers\AeropuertosController::class, 'search_airlines'])->name('aeropuertos.search_airlines');

    Route::get('/feriados', [App\Http\Controllers\FeriadosController::class, 'index'])->name('feriados');

    Route::match(['get', 'post'], '/feriados/create', [App\Http\Controllers\FeriadosController::class, 'create'])->name('feriados.create');
    Route::get('/feriados/delete/{id}', [App\Http\Controllers\FeriadosController::class, 'delete'])->name('feriados.delete');

    Route::get('/membresias', [App\Http\Controllers\MembresiaController::class, 'index'])->name('membresias');

    Route::get('/verificar-membresias', [\App\Http\Controllers\MembresiaController::class, 'verify'])->name('membresias.verify');

    Route::match(['get', 'post'], '/membresias/create', [App\Http\Controllers\MembresiaController::class, 'create'])->name('membresias.create');
    Route::match(['get', 'post'], '/membresias/edit/{id}', [App\Http\Controllers\MembresiaController::class, 'edit'])->name('membresias.edit');
    Route::get('/membresias/datatable', [App\Http\Controllers\MembresiaController::class, 'datatable'])->name('membresias.datatable');
    Route::get('/membresias/datatable2', [App\Http\Controllers\MembresiaController::class, 'datatable2'])->name('membresias.datatable2');
    Route::get('/membresias/reporte', [App\Http\Controllers\MembresiaController::class, 'reporte'])->name('membresias.reporte');

    Route::match(['get', 'post', 'delete'], '/aeropuertos/ubicaciones/{id}', [App\Http\Controllers\UbicacionesController::class, 'ubicaciones'])->name('aeropuertos.ubicaciones');
    Route::match(['get', 'post', 'delete'], '/aeropuertos/lugares/{id}', [App\Http\Controllers\LugaresController::class, 'lugares'])->name('aeropuertos.lugares');
    Route::match(['get', 'post', 'delete'], '/aeropuertos/puestos/{id}', [App\Http\Controllers\PuestosController::class, 'puestos'])->name('aeropuertos.puestos');

    Route::get('/aerolineas', [App\Http\Controllers\AerolieasController::class, 'index'])->name('aerolineas');

    Route::match(['get', 'post'], '/aerolineas/create', [App\Http\Controllers\AerolieasController::class, 'create'])->name('aerolineas.create');
    Route::match(['get', 'post'], '/aerolineas/edit/{id}', [App\Http\Controllers\AerolieasController::class, 'edit'])->name('aerolineas.edit');
    Route::match(['get', 'post'], '/aerolineas/disable/{id}', [App\Http\Controllers\AerolieasController::class, 'disable'])->name('aerolineas.disable');
    Route::match(['get', 'post'], '/aerolineas/active/{id}', [App\Http\Controllers\AerolieasController::class, 'active'])->name('aerolineas.active');

    Route::get('/aeronaves', [App\Http\Controllers\AeronavesController::class, 'index'])->name('aeronaves');

    Route::match(['get', 'post'], '/aeronaves/create', [App\Http\Controllers\AeronavesController::class, 'create'])->name('aeronaves.create');
    Route::match(['get', 'post'], '/aeronaves/edit/{id}', [App\Http\Controllers\AeronavesController::class, 'edit'])->name('aeronaves.edit');
    Route::match(['get', 'post'], '/aeronaves/disable/{id}', [App\Http\Controllers\AeronavesController::class, 'disable'])->name('aeronaves.disable');
    Route::match(['get', 'post'], '/aeronaves/active/{id}', [App\Http\Controllers\AeronavesController::class, 'active'])->name('aeronaves.active');
    Route::get('/aeronaves/active-list', [App\Http\Controllers\AeronavesController::class, 'get_aeronaves_active'])->name('aeronaves.aeronaves_active');
    Route::get('/aeronaves/inactive-list', [App\Http\Controllers\AeronavesController::class, 'get_aeronaves_inactive'])->name('aeronaves.aeronaves_inactive');

    Route::get('/prodservicios', [App\Http\Controllers\ProdserviciosController::class, 'index'])->name('prodservicios');

    Route::match(['get', 'post'], '/prodservicios/create', [App\Http\Controllers\ProdserviciosController::class, 'create'])->name('prodservicios.create');
    Route::match(['get', 'post'], '/prodservicios/edit/{id}', [App\Http\Controllers\ProdserviciosController::class, 'edit'])->name('prodservicios.edit');
    Route::match(['get', 'post'], '/prodservicios/disable/{id}', [App\Http\Controllers\ProdserviciosController::class, 'disable'])->name('prodservicios.disable');
    Route::match(['get', 'post'], '/prodservicios/active/{id}', [App\Http\Controllers\ProdserviciosController::class, 'active'])->name('prodservicios.active');

    Route::get('/taquillas', [App\Http\Controllers\TaquillasController::class, 'index'])->name('taquillas');
    Route::match(['get', 'post'], '/taquillas/create', [App\Http\Controllers\TaquillasController::class, 'create'])->name('taquillas.create');
    Route::match(['get', 'post'], '/taquillas/edit/{id}', [App\Http\Controllers\TaquillasController::class, 'edit'])->name('taquillas.edit');
    Route::match(['get', 'post'], '/taquillas/disable/{id}', [App\Http\Controllers\TaquillasController::class, 'disable'])->name('taquillas.disable');
    Route::match(['get', 'post'], '/taquillas/active/{id}', [App\Http\Controllers\TaquillasController::class, 'active'])->name('taquillas.active');
    Route::match(['get', 'post', 'delete'], '/taquillas/add-points/{id}', [App\Http\Controllers\TaquillasPuntosController::class, 'add_points'])->name('taquillas.add_points');

    Route::get('/hangares', [App\Http\Controllers\HangaresController::class, 'index'])->name('hangares');
    Route::match(['get', 'post'], '/hangares/create', [App\Http\Controllers\HangaresController::class, 'create'])->name('hangares.create');
    Route::match(['get', 'post'], '/hangares/edit/{id}', [App\Http\Controllers\HangaresController::class, 'edit'])->name('hangares.edit');
    Route::match(['get', 'post'], '/hangares/disable/{id}', [App\Http\Controllers\HangaresController::class, 'disable'])->name('hangares.disable');
    Route::match(['get', 'post'], '/hangares/active/{id}', [App\Http\Controllers\HangaresController::class, 'active'])->name('hangares.active');

    //Route::match(['get'], '/vuelos/{f?}/{aeropuerto?}', [App\Http\Controllers\VuelosController::class, 'index'])->name('vuelos');
    Route::match(['get', 'post'], '/vuelos-create', [App\Http\Controllers\VuelosController::class, 'create'])->name('vuelos');
    Route::get('/vuelos-datatable', [App\Http\Controllers\VuelosController::class, 'get_lista_vuelos'])->name('vuelos.datatable');
    Route::get('/vuelos-datatable-hoy', [App\Http\Controllers\VuelosController::class, 'get_vuelos_hoy'])->name('vuelos.get_vuelos_hoy');
    Route::get('/edit-vuelos-new/{id}', [App\Http\Controllers\VuelosController::class, 'edit_vuelo_new'])->name('get_vuelos.edit_new');
    Route::post('/save-complementos', [App\Http\Controllers\VuelosController::class, 'save_complementos'])->name('vuelos.save_complementos');
    Route::get('/get-plataformas/{id}', [App\Http\Controllers\VuelosController::class, 'get_plataformas'])->name('vuelos.get_plataformas');
    Route::get('/see-fly/{id}', [App\Http\Controllers\VuelosController::class, 'see_fly'])->name('vuelos.see_fly');

    Route::match(['get', 'post', 'put'], '/get-vuelos', [App\Http\Controllers\VuelosController::class, 'get_vuelos'])->name('get_vuelos');
    Route::get('/vuelos-datatable2', [App\Http\Controllers\VuelosController::class, 'get_lista_vuelos2'])->name('get_vuelos.datatable');

    //Route::post('/get-vuelos-excel', [App\Http\Controllers\VuelosController::class, 'get_vuelos_excel'])->name('get_vuelos.excel');
    Route::get('/see-vuelos/{id}/{back?}', [App\Http\Controllers\VuelosController::class, 'see_vuelo'])->name('get_vuelos.see');
    Route::match(['get', 'post'], '/edit-vuelos/{id}', [App\Http\Controllers\VuelosController::class, 'edit_vuelo'])->name('get_vuelos.edit');
    Route::match(['get', 'post'], '/disable-vuelos/{id}/{back?}', [App\Http\Controllers\VuelosController::class, 'disable_vuelo'])->name('get_vuelos.disable');
    Route::match(['get', 'post'], '/position-vuelos/{id}', [App\Http\Controllers\VuelosController::class, 'position_vuelo'])->name('get_vuelos.position');
    Route::get('exonerar-vuelo/{id}', [App\Http\Controllers\VuelosController::class, 'exonera_vuelo'])->name('get_vuelos.exonera_vuelo');
    Route::get('aprobar-vuelo/{id}', [App\Http\Controllers\VuelosController::class, 'aprobar'])->name('get_vuelos.aprobar');
    Route::get('delete-item-vuelo/{id}', [App\Http\Controllers\VuelosController::class, 'delete_item'])->name('get_vuelos.delete_item');

    Route::match(['get', 'post'], '/edit-status-vuelo', [App\Http\Controllers\VuelosController::class, 'edit_status_vuelo'])->name('edit_status_vuelo');

    Route::match(['get', 'post'], 'problemas/{id?}', [App\Http\Controllers\ProblemasController::class, 'index'])->name('problemas');

    Route::match(['get', 'post', 'put'], '/vuelos-tortuga', [App\Http\Controllers\VuelosController::class, 'vuelos_tortuga'])->name('vuelos_tortuga');

    Route::match(['get', 'post', 'put'], '/recaudacion', [App\Http\Controllers\RecaudacionController::class, 'create'])->name('recaudacion');

    Route::match(['get', 'post', 'put'], '/facturar-pospago', [App\Http\Controllers\RecaudacionController::class, 'facturar_pospago'])->name('facturar_pospago');
    Route::get('delete-item/{id}', [App\Http\Controllers\RecaudacionController::class, 'delete_item'])->name('facturar_pospago.delete_item');
    Route::post('add-item', [App\Http\Controllers\RecaudacionController::class, 'add_item'])->name('facturar_pospago.add_item');
    Route::match(['get', 'post', 'put'], 'edit-vuelo/{id}', [App\Http\Controllers\RecaudacionController::class, 'edit_vuelo'])->name('facturar_pospago.edit_vuelo');
    Route::get('exonera-vuelo/{id}', [App\Http\Controllers\RecaudacionController::class, 'exonera_vuelo'])->name('facturar_pospago.exonera_vuelo');
    Route::get('anular-fact/{id}', [App\Http\Controllers\RecaudacionController::class, 'anular_fact'])->name('facturar_pospago.anular_fact');
    Route::get('anular-prof/{id}', [App\Http\Controllers\RecaudacionController::class, 'anular_prof'])->name('facturar_pospago.anular_prof');
    Route::get('pagar-factura/{id}', [App\Http\Controllers\RecaudacionController::class, 'pagar_factura'])->name('facturar_pospago.pagar_factura');
    Route::match(['get', 'post'], 'libro-ventas/{fecha?}', [App\Http\Controllers\RecaudacionController::class, 'libro_ventas'])->name('facturar_pospago.libro_ventas');


    Route::match(['get', 'post', 'put'], '/proforma-operacion', [App\Http\Controllers\RecaudacionController::class, 'proforma_operacion'])->name('facturar_pospago.proforma_operacion');
    Route::get('/proforma-operacion-lista', [App\Http\Controllers\RecaudacionController::class, 'proforma_operacion_lista'])->name('facturar_pospago.proforma_operacion_lista');
    Route::get('/proforma-operacion-detalle/{llegada}/{salida}', [App\Http\Controllers\RecaudacionController::class, 'proforma_operacion_detalle'])->name('facturar_pospago.detalle');

    Route::match(['get', 'post', 'put'], '/proforma-personalizada', [App\Http\Controllers\RecaudacionController::class, 'proforma_personalizada'])->name('facturar_pospago.proforma_personalizada');
    Route::match(['get', 'post', 'put'], '/proforma_personalizada_avc', [App\Http\Controllers\RecaudacionController::class, 'proforma_personalizada_avc'])->name('facturar_pospago.proforma_personalizada_avc');
    Route::match(['get', 'post', 'put'], '/proforma-services', [App\Http\Controllers\ProdserviciosController::class, 'get_total_serv_Avc'])->name('proforma_services');
    Route::match(['get', 'post', 'put'], '/proforma-carga', [App\Http\Controllers\ProdserviciosController::class, 'Get_Services_Carga_Avc'])->name('proforma_carga');
    Route::match(['get', 'post', 'put'], '/proforma-dosa', [App\Http\Controllers\ProdserviciosController::class, 'Get_Services_Dosa_Avc'])->name('proforma_dosa');
    Route::match(['get', 'post', 'put'], 'list-service-avc/{id?}', [App\Http\Controllers\ProdserviciosController::class, 'Services_List_Avc'])->name('list_service_avc');
    Route::match(['get', 'post', 'put'], 'service-single-avc', [App\Http\Controllers\ProdserviciosController::class, 'SingleServiceAVC'])->name('service_single_avc');

    Route::match(['get', 'post', 'put'], '/inventario-equipos', [App\Http\Controllers\InventarioEquipoController::class, 'inventario_equipos'])->name('inventario_equipos');
    Route::match(['get', 'post'],'/ExcelInventarioEquipo', [App\Http\Controllers\InventarioEquipoController::class, 'ExcelInventarioEquipo'])->name('ExcelInventarioEquipo');
    Route::match(['get', 'post', 'put'], '/get-inventario', [App\Http\Controllers\InventarioEquipoController::class, 'get_inventario'])->name('inventario_equipos.get_inventario');
    Route::match(['get', 'post', 'put', 'delete'], '/set-alarm-inventario-equipo', [App\Http\Controllers\InventarioEquipoController::class, 'set_alarm'])->name('inventario_equipos.set_alarm');
    Route::get('/search-alarm-inventario-equipo/{id?}', [App\Http\Controllers\InventarioEquipoController::class, 'search_alarm'])->name('inventario_equipos.search_alarm');
    
    Route::get('/search-concesion-equipos', [App\Http\Controllers\InventarioEquipoController::class, 'search_concesion'])->name('inventario_equipos.search_concesion');
    

    Route::post('/send-prof-email/{id}', [App\Http\Controllers\RecaudacionController::class, 'send_prof_email'])->name('facturar_pospago.send_prof_email');


    Route::match(['get', 'post', 'put'], '/emitir-factura/', [App\Http\Controllers\RecaudacionController::class, 'emitir_factura'])->name('emitir_factura');

    Route::match(['get', 'post', 'put'], '/search-plane/', [App\Http\Controllers\VuelosController::class, 'search_plane'])->name('search_plane');

    Route::match(['get', 'post', 'put'], 'aeronaves-puestos', [App\Http\Controllers\AeronavesPuestosController::class, 'puestos'])->name('aeronaves_puestos');

    //Route::match(['get', 'post', 'put'], 'concesiones', [App\Http\Controllers\ConcesionesController::class, 'index'])->name('concesiones');

    /*     * **************************** */



    Route::match(['get', 'post', 'put'], '/revision/{id?}', [App\Http\Controllers\VuelosController::class, 'revision'])->name('revision');

    Route::match(['get', 'post', 'put'], '/ver-mapa/', [App\Http\Controllers\AeropuertosController::class, 'ver_mapa'])->name('ver_mapa');
    Route::match(['get', 'put'], '/ver-info-mapa/', [App\Http\Controllers\AeropuertosController::class, 'ver_info_mapa'])->name('ver_info_mapa');


    Route::prefix('tickets')->group(function () {

        Route::match(['get', 'post'], 'carga-lotes', [App\Http\Controllers\LotesTicketsController::class, 'carga'])->name('carga_tickets');
        Route::match(['get', 'post'], 'lista-tickets', [App\Http\Controllers\LotesTicketsController::class, 'lista'])->name('lista_tickets');
        Route::match(['get', 'post', 'put'], 'venta-tickets/{type?}', [App\Http\Controllers\LotesTicketsController::class, 'venta'])->name('venta_tickets');


    });
    Route::prefix('recaudacion')->group(function () {
        Route::get('/', [App\Http\Controllers\RecaudacionController::class, 'pagos'])->name('recaudacion');
        Route::get('/obtener_proforma', [App\Http\Controllers\RecaudacionController::class, 'obtenerProforma'])->name('recaudacion.obtener_proforma');
        Route::post('/pagar', [App\Http\Controllers\RecaudacionController::class, 'pagos'])->name('recaudacion.pagos');
    });

    Route::post('/pdf-pagos', [App\Http\Controllers\RecaudacionController::class, 'down_pagos'])->name('recaudacion.down_pagos');
     Route::post('/send-resumen-email', [App\Http\Controllers\RecaudacionController::class, 'send_resumen_email'])->name('recaudacion.send_resumen_email');
    Route::prefix('concesiones')->group(function () {
        Route::get('/', [App\Http\Controllers\ConcesionController::class, 'index'])->name('concesiones');
        Route::get('mostrar/', [App\Http\Controllers\ConcesionController::class, 'mostrarHistorial'])->name('concesiones.mostrar');

        Route::get('/registrar', [App\Http\Controllers\ConcesionController::class, 'create'])->name('concesiones.create');
        Route::post('/registrar', [App\Http\Controllers\ConcesionController::class, 'storeConcesion'])->name('concesiones.store');

        Route::get('/buscarConcesion/{id}', [App\Http\Controllers\ConcesionController::class, 'searchConcesiones'])->name('concesiones.data_buscar');

        Route::get('/listConcesiones', [App\Http\Controllers\ConcesionController::class, 'listConcesiones'])->name('concesiones.data_concesiones');



        Route::get('/buscarCliente/{tipo}/{rif}', [App\Http\Controllers\ConcesionController::class, 'searchCliente'])->name('concesiones.data');
        Route::get('/buscarLocal/{id}', [App\Http\Controllers\ConcesionController::class, 'searchLocal'])->name('concesiones.data_cliente');
        Route::post('/actualizarLocal', [App\Http\Controllers\ConcesionController::class, 'updateLocal'])->name('concesiones.updateLocal');
        Route::get('/ExcelHistorial/{id}', [App\Http\Controllers\ConcesionController::class, 'ExcelHistorial'])->name('concesiones.ExcelHistorial');
        Route::get('/listHistorialConcesiones/{id}', [\App\Http\Controllers\ConcesionController::class, 'listHistorialConcesiones'])->name('concesiones.data_historial');

        Route::post('/actualizarConcesion/{id}', [App\Http\Controllers\ConcesionController::class, 'updateConcesion'])->name('concesiones.updateConcesion');


        Route::post('/subir-archivo', [App\Http\Controllers\ConcesionController::class, 'uploadFile'])->name('concesiones.uploadFile');
        Route::get('/lista-archivos/{id}', [App\Http\Controllers\ConcesionController::class, 'showFiles'])->name('concesiones.showFiles');
        Route::get('/lista-archivos/{id}/{nombre}', [App\Http\Controllers\ConcesionController::class, 'seeFile'])->name('concesiones.seeFile');
        Route::get('/borrar-archivos/{id}/{nombre}', [App\Http\Controllers\ConcesionController::class, 'eraseFile'])->name('concesiones.araseFile');

    });
    Route::prefix('pantallas')->group(function () {
        Route::match(['get', 'post', 'put'], '/', [App\Http\Controllers\MovimientoController::class, 'index'])->name('pantallas');
        Route::get('/obtener_lista', [App\Http\Controllers\MovimientoController::class, 'obtener_lista'])->name('pantallas.obtener_lista');
        Route::prefix('control-videos')->group(function () {
            Route::match(['get', 'post', 'put'], '/', [App\Http\Controllers\MovimientoController::class, 'lista_videos'])->name('pantallas.lista_videos');
            Route::get('/obtener-videos', [App\Http\Controllers\MovimientoController::class, 'obtener_videos'])->name('pantallas.obtener_videos');
            // Route::match(['get', 'post', 'put'], '/{codigo_oaci}', [App\Http\Controllers\MovimientoController::class, 'showVideos'])->name('pantallas.control_videos');
        });
    });

    Route::prefix('bancos')->group(function () {
        Route::get('/', [App\Http\Controllers\BancosController::class, 'index'])->name('bancos');

        Route::match(['get', 'post'], 'create', [App\Http\Controllers\BancosController::class, 'create'])->name('bancos.create');
        Route::match(['get', 'post'], 'edit/{id}', [App\Http\Controllers\BancosController::class, 'edit'])->name('bancos.edit');
        Route::match(['get', 'post'], 'disable/{id}', [App\Http\Controllers\BancosController::class, 'disable'])->name('bancos.disable');
        Route::match(['get', 'post'], 'active/{id}', [App\Http\Controllers\BancosController::class, 'active'])->name('bancos.active');
    });

    Route::prefix('cuentas-bancarias')->group(function () {
        Route::get('/', [App\Http\Controllers\CuentaBancariaController::class, 'index'])->name('cuentas_bancarias');

        Route::match(['get', 'post'], 'create', [App\Http\Controllers\CuentaBancariaController::class, 'create'])->name('cuentas_bancarias.create');
        Route::match(['get', 'post'], 'edit/{id}', [App\Http\Controllers\CuentaBancariaController::class, 'edit'])->name('cuentas_bancarias.edit');
        Route::match(['get', 'post'], 'disable/{id}', [App\Http\Controllers\CuentaBancariaController::class, 'disable'])->name('cuentas_bancarias.disable');
        Route::match(['get', 'post'], 'active/{id}', [App\Http\Controllers\CuentaBancariaController::class, 'active'])->name('cuentas_bancarias.active');
    });
    /*
      Route::get('/aviacion-general', [App\Http\Controllers\VuelosGeneralesController::class, 'index'])->name('aviacion_general');
      Route::post('/aviacion-general/create-pilotos', [App\Http\Controllers\VuelosGeneralesController::class, 'add_piloto'])->name('aviacion_general.add_piloto');
      Route::post('/aviacion-general/create-aeronaves', [App\Http\Controllers\VuelosGeneralesController::class, 'add_nave'])->name('aviacion_general.add_nave');
      Route::post('/aviacion-general/add-arrive', [App\Http\Controllers\VuelosGeneralesController::class, 'add_arrive'])->name('aviacion_general.add_arrive');
      Route::post('/aviacion-general/add-leave', [App\Http\Controllers\VuelosGeneralesController::class, 'add_leave'])->name('aviacion_general.add_leave');

      Route::match(['get', 'post'], '/aviacion-general-tortuga', [App\Http\Controllers\VuelosTortugaController::class, 'index'])->name('aviacion_general_tortuga');
      Route::match(['get', 'post'], '/aviacion-general-tortuga-add', [App\Http\Controllers\VuelosTortugaController::class, 'create'])->name('aviacion_general_tortuga_create');
      Route::match(['get', 'post'], '/aviacion-general-tortuga-store/{id?}', [App\Http\Controllers\VuelosTortugaController::class, 'store'])->name('aviacion_general_tortuga.store');
      Route::post('/aviacion-general-tortuga/create-pilotos', [App\Http\Controllers\VuelosGeneralesController::class, 'add_piloto'])->name('aviacion_general_tortuga.add_piloto');
      Route::post('/aviacion-general-tortuga/create-aeronaves', [App\Http\Controllers\VuelosGeneralesController::class, 'add_nave'])->name('aviacion_general_tortuga.add_nave');
      Route::post('/aviacion-general-tortuga-get-serv', [App\Http\Controllers\VuelosTortugaController::class, 'get_serv'])->name('aviacion_general_tortuga.get_serv');
      Route::get('/view-factura-print/{id}/{copia}', [App\Http\Controllers\VuelosTortugaController::class, 'view_factura'])->name('aviacion_general_tortuga.view_factura');
      Route::get('/anular-registro-vuelo/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'anular_vuelo'])->name('aviacion_general_tortuga.anular_vuelo');
      Route::get('/lista-facturar/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'lista_facturar'])->name('aviacion_general_tortuga.lista_facturar');
      Route::match(['get', 'post'], '/aviacion-general-tortuga-operacion/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'operacion'])->name('aviacion_general_tortuga.operacion');
      //Route::get('/view-factura-print-tortuga-operador/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'view_factura'])->name('aviacion_general_tortuga.view_factura');



      Route::match(['get', 'post'], '/facturacion-aviacion-general', [App\Http\Controllers\FacturacionVuelosGeneralesController::class, 'facturacion'])->name('facturacion_aviacion_general');
      Route::post('/aviacion-general-tortuga/fact-create-pilotos', [App\Http\Controllers\FacturacionVuelosGeneralesController::class, 'add_piloto'])->name('facturacion_aviacion_general.add_piloto');
      Route::post('/aviacion-general-tortuga/fact-create-aeronaves', [App\Http\Controllers\FacturacionVuelosGeneralesController::class, 'add_nave'])->name('facturacion_aviacion_general.add_nave');
      Route::post('/aviacion-general-get-serv', [App\Http\Controllers\FacturacionVuelosGeneralesController::class, 'get_serv'])->name('facturacion_aviacion_general.get_serv');
      Route::post('/aviacion-general-get-serv-extra', [App\Http\Controllers\FacturacionVuelosGeneralesController::class, 'get_serv_extra'])->name('facturacion_aviacion_general.get_serv_extra');


      Route::match(['get', 'post'], '/facturar-pospago', [App\Http\Controllers\PospagoVuelosGeneralesController::class, 'facturar_pospago'])->name('facturacion_pospago');


      Route::match(['get', 'post'], '/pospago-aviacion-general', [App\Http\Controllers\PospagoVuelosGeneralesController::class, 'facturacion'])->name('pospago_aviacion_general');
      Route::post('/aviacion-general-tortuga/post-create-pilotos', [App\Http\Controllers\PospagoVuelosGeneralesController::class, 'add_piloto'])->name('pospago_aviacion_general.add_piloto');
      Route::post('/aviacion-general-tortuga/post-create-aeronaves', [App\Http\Controllers\PospagoVuelosGeneralesController::class, 'add_nave'])->name('pospago_aviacion_general.add_nave');
      Route::post('/aviacion-general-get-serv-pospago', [App\Http\Controllers\PospagoVuelosGeneralesController::class, 'get_serv'])->name('pospago_aviacion_general.get_serv');
      Route::post('/aviacion-general-get-serv-pospago-extra', [App\Http\Controllers\PospagoVuelosGeneralesController::class, 'get_serv_extra'])->name('pospago_aviacion_general.get_serv_extra');

      //Route::match(['get', 'post'], '/aviacion-general-tortuga-add', [App\Http\Controllers\VuelosTortugaController::class, 'create'])->name('aviacion_general_tortuga_create');
      //Route::match(['get', 'post'], '/aviacion-general-tortuga-store/{id?}', [App\Http\Controllers\VuelosTortugaController::class, 'store'])->name('aviacion_general_tortuga.store');

      //Route::post('/aviacion-general-tortuga-get-serv', [App\Http\Controllers\VuelosTortugaController::class, 'get_serv'])->name('aviacion_general_tortuga.get_serv');
      //Route::get('/view-factura-print/{id}/{copia}', [App\Http\Controllers\VuelosTortugaController::class, 'view_factura'])->name('aviacion_general_tortuga.view_factura');
      //Route::get('/anular-registro-vuelo/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'anular_vuelo'])->name('aviacion_general_tortuga.anular_vuelo');
      // Route::get('/lista-facturar/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'lista_facturar'])->name('aviacion_general_tortuga.lista_facturar');
      //Route::match(['get', 'post'], '/aviacion-general-tortuga-operacion/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'operacion'])->name('aviacion_general_tortuga.operacion');

      //Route::get('/view-factura-print-tortuga-operador/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'view_factura'])->name('aviacion_general_tortuga.view_factura');


      Route::match(['get', 'post', 'put'], '/facturacion-general-tortuga/{id?}', [App\Http\Controllers\VuelosTortugaController::class, 'facturacion_general_tortuga'])->name('facturacion_general_tortuga');
      Route::post('/aviacion-general-tortuga-get-serv-fact', [App\Http\Controllers\VuelosTortugaController::class, 'get_serv'])->name('facturacion_general_tortuga.get_serv_fact');
      Route::get('/aviacion-general-tortuga-get-serv-fact-2/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'get_serv_2'])->name('facturacion_general_tortuga.get_serv_fact_2');
      Route::get('/view-factura-print-tortuga/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'view_factura'])->name('facturacion_general_tortuga.view_factura');
      Route::get('/anular-factura-vuelo/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'anular_factura_vuelo'])->name('facturacion_general_tortuga.anular_factura_vuelo');
      Route::match(['get', 'post'] ,'/agregar-pago-factura/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'agregar_pago_factura'])->name('facturacion_general_tortuga.agregar_pago_factura');
      Route::get('/regresar-operacion/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'regresar_operacion'])->name('facturacion_general_tortuga.regresar_operacion');
      Route::post('/send-prof-email/{id}', [App\Http\Controllers\VuelosTortugaController::class, 'send_prof_email'])->name('facturacion_general_tortuga.send_prof_email');


      Route::match(['get', 'post'], '/reporte_vuelos_tortuga', [App\Http\Controllers\VuelosTortugaController::class, 'reporte_vuelos_tortuga'])->name('reporte_vuelos_tortuga');



      Route::match(['get', 'post'], '/aviacion-general/edit/{id}', [App\Http\Controllers\VuelosGeneralesController::class, 'edit'])->name('aviacion_general.edit');
      Route::match(['get', 'post'], '/aviacion-general/add-prodserv/{id}', [App\Http\Controllers\VuelosGeneralesController::class, 'add_prodserv'])->name('aviacion_general.add_prodserv');
      Route::match(['get', 'post'], '/aviacion-general/add-position/{id}', [App\Http\Controllers\VuelosGeneralesController::class, 'add_position'])->name('aviacion_general.add_position');
      Route::match(['get', 'post'], '/aviacion-general/online-reg/{id?}', [App\Http\Controllers\VuelosGeneralesController::class, 'online_reg'])->name('aviacion_general.online_reg');

      Route::match(['get', 'post'], '/facturas', [App\Http\Controllers\VuelosGeneralesController::class, 'facturas'])->name('facturas');
      Route::match(['get', 'post'], '/tortuga/{id}', [App\Http\Controllers\FacturasController::class, 'tortuga'])->name('facturas.tortuga');
      Route::match(['get', 'post'], '/tasas/{id}', [App\Http\Controllers\FacturasController::class, 'tasas'])->name('facturas.add_tasa');
      Route::match(['get', 'post'], '/dosas/{id}', [App\Http\Controllers\FacturasController::class, 'dosas'])->name('facturas.add_dosa');
      Route::match(['get', 'post'], '/add-prodservs/{id}', [App\Http\Controllers\FacturasController::class, 'add_prodservs'])->name('facturas.add_prodservs');
      Route::get('/del-prodservs/{id}/{serv}/{tipo}', [App\Http\Controllers\FacturasController::class, 'del_prodservs'])->name('facturas.del_prodservs');
      Route::match(['get', 'post'], '/add-invoice/{id}', [App\Http\Controllers\FacturasController::class, 'invoice'])->name('facturas.invoice');
      Route::post('/facturas-add/{id}', [App\Http\Controllers\FacturasController::class, 'create'])->name('facturas.create');
      Route::get('/view-factura/{id}', [App\Http\Controllers\FacturasController::class, 'view_factura'])->name('facturas.view_factura');

      Route::match(['get', 'post'], '/facturas2', [App\Http\Controllers\VuelosGeneralesController::class, 'facturas'])->name('operaciones_facturas');

      Route::match(['get', 'put', 'post'], '/registro-aviacion-general', [App\Http\Controllers\VuelosGeneralesController::class, 'registro_aviacion_general'])->name('registro_aviacion_general');
      Route::get('/registro-aviacion-general-get/{id}', [App\Http\Controllers\VuelosGeneralesController::class, 'registro_aviacion_general_get'])->name('registro_aviacion_general.get');
      Route::get('/registro-aviacion-general-update/{id}', [App\Http\Controllers\VuelosGeneralesController::class, 'registro_aviacion_general_update'])->name('registro_aviacion_general.update');

      Route::match(['get', 'post'], '/listado-facturas', [App\Http\Controllers\VuelosGeneralesController::class, 'listado_facturas'])->name('listado_facturas');
     */



    /*     * *********************REPORTES**************************************** */

    Route::get('/reportes/vuelos', [App\Http\Controllers\ReportesController::class, 'vuelos'])->name('reporte_vuelos');
});

/* * **APIS**** */

Route::post('/apis/aeropuertos', [App\Http\Controllers\AeropuertosController::class, 'api_aeropuertos'])->name('apis.aeropuertos');

/************************/

Route::post('apis/pilotos/login', [App\Http\Controllers\PilotosController::class, 'login'])->name('pilotos.login');


/*********************/
