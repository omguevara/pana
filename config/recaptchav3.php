<?php
return [
    'origin' => env('RECAPTCHAV3_ORIGIN', 'https://www.google.com/recaptcha'),
    'sitekey' => env('RECAPTCHAV3_SITEKEY', '6LebyM8mAAAAAPOZL7tNyFYUDyK3zQ3WH0eiDr4q'),
    'secret' => env('RECAPTCHAV3_SECRET', '6LebyM8mAAAAANVq6iEQqLGyg7XqjancerQgw_T3'),
    'locale' => env('RECAPTCHAV3_LOCALE', '')
];
