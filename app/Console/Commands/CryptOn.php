<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Encryptor;
use App\Helpers\Obfuscator;

class CryptOn extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Crypt:On';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Only Administrator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        /*
          $this->info(Encryptor::encrypt("Hola"));
          $this->info(public_path());
          $this->info(base_path());
          $this->info(storage_path());
         */
        $DS = DIRECTORY_SEPARATOR;
        $projectsListIgnore = array('.', '..');


        /*
        $getRoutesCrypt[0]['route'] = base_path('app' . $DS . 'Models'); 
        $getRoutesCrypt[0]['msg'] = "Crypt Models, Please Wait,...";
        */
        $getRoutesCrypt[1]['route'] = base_path('app' . $DS . 'Http' . $DS . 'Controllers' ); 
        $getRoutesCrypt[1]['msg'] = "Crypt Controller, Please Wait,...";
        
        
        




        foreach($getRoutesCrypt as $value){
            $this->info($value['msg']);
            $handle = opendir($value['route']);
            while ($file = readdir($handle)) {
                if (!is_dir($file) && !in_array($file, $projectsListIgnore)) {
                    $ext = explode(".", $file);
                    $ext = $ext[count($ext) - 1];
                    if ($ext == 'php') {
                        $this->info("****".$file);
                        $sData = file_get_contents($value['route']. $DS . $file);
                        $sData = str_replace(array('<?php', '<?', '?>'), '', $sData); // We strip the open/close PHP tags

                        $sObfusationData = new Obfuscator( $sData, 'Class/Code NAME');
                        file_put_contents($value['route']. $DS . $file, '<?php ' . "\r\n" . $sObfusationData);
                    }
                }
            }
        }
        


        return 1;
    }

}
