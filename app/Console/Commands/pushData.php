<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Controller;

class pushData extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push:Data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {
        $this->info("Procesando Datos...");
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '304404816', 'razon' => 'QUALCOM TELESITEMAS C.A', 'phone' => '(0414)-288.13.74', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3188', 'modelo' => 'C-550', 'peso' => '6,85', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-01', 'pax_desembarcados' => '3', 'pax_embarcados' => '0', 'hora_llegada' => '11:55', 'hora_salida' => 'null', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '810743339', 'razon' => 'MATADERO AVICOLA LOS LLANOS C.A', 'phone' => '(0424)-574.04.11', 'correo' => '', 'direccion' => 'BARINAS', 'aeropuerto_id' => '18', 'matricula' => 'YV107X', 'modelo' => 'RV-14', 'peso' => '0,93', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-01', 'pax_desembarcados' => '0', 'pax_embarcados' => '0', 'hora_llegada' => '12:44', 'hora_salida' => '14:30', 'observacion' => 'BASADO'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '002980265', 'razon' => 'ATRIO SEGUROS C.A', 'phone' => '(0412)-342.80.87', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3090', 'modelo' => 'AC-90', 'peso' => '4,68', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-01', 'pax_desembarcados' => '3', 'pax_embarcados' => '0', 'hora_llegada' => '15:25', 'hora_salida' => 'null', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '090286233', 'razon' => 'SEGUROS CONTITUCIÓN C.A', 'phone' => '(0412)-717.20.40', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3224', 'modelo' => 'BE-20', 'peso' => '5,67', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-01', 'pax_desembarcados' => '5', 'pax_embarcados' => '0', 'hora_llegada' => '18:00', 'hora_salida' => 'null', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YVO115', 'modelo' => 'AC-90', 'peso' => '4,68', 'nacionalidad_id' => '1', 'fecha_operacion' => '2022-03-02', 'pax_desembarcados' => '3', 'pax_embarcados' => '3', 'hora_llegada' => '10:05', 'hora_salida' => '12:22', 'observacion' => 'OFICIAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV3380', 'modelo' => 'BE-40', 'peso' => '7,3', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-02', 'pax_desembarcados' => '3', 'pax_embarcados' => '3', 'hora_llegada' => '08:45', 'hora_salida' => '12:40', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '304404816', 'razon' => 'QUALCOM TELESITEMAS C.A', 'phone' => '(0414)-288.13.74', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3188', 'modelo' => 'C-550', 'peso' => '6,85', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-02', 'pax_desembarcados' => '0', 'pax_embarcados' => '2', 'hora_llegada' => 'null', 'hora_salida' => '10:30', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '002980265', 'razon' => 'ATRIO SEGUROS C.A', 'phone' => '(0412)-342.80.87', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3090', 'modelo' => 'AC-90', 'peso' => '4,68', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-02', 'pax_desembarcados' => '0', 'pax_embarcados' => '3', 'hora_llegada' => 'null', 'hora_salida' => '10:47', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '090286233', 'razon' => 'SEGUROS CONTITUCIÓN C.A', 'phone' => '(0412)-717.20.40', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3224', 'modelo' => 'BE-20', 'peso' => '5,67', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-02', 'pax_desembarcados' => '0', 'pax_embarcados' => '5', 'hora_llegada' => 'null', 'hora_salida' => '15:50', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'PDVSA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV2869', 'modelo' => 'B-190', 'peso' => '7,67', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-03', 'pax_desembarcados' => '6', 'pax_embarcados' => '9', 'hora_llegada' => '11:33', 'hora_salida' => '12:31', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '810743339', 'razon' => 'MATADERO LOS LLANOS C.A', 'phone' => '(0424)-574.04.11', 'correo' => '', 'direccion' => 'BARINAS', 'aeropuerto_id' => '18', 'matricula' => 'YV107X', 'modelo' => 'RV-14', 'peso' => '0,93', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-03', 'pax_desembarcados' => '0', 'pax_embarcados' => '0', 'hora_llegada' => '15:10', 'hora_salida' => '15:50', 'observacion' => 'BASADO'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV3381', 'modelo' => 'BE-40', 'peso' => '7,3', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-04', 'pax_desembarcados' => '2', 'pax_embarcados' => '3', 'hora_llegada' => '07:45', 'hora_salida' => '08:12', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'PDVSA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV1118', 'modelo' => 'LJ-45', 'peso' => '9,75', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-05', 'pax_desembarcados' => '0', 'pax_embarcados' => '3', 'hora_llegada' => '07:50', 'hora_salida' => '09:08', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV3381', 'modelo' => 'BE-40', 'peso' => '7,3', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-05', 'pax_desembarcados' => '1', 'pax_embarcados' => '6', 'hora_llegada' => '07:55', 'hora_salida' => '09:10', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV3381', 'modelo' => 'BE-40', 'peso' => '7,3', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-05', 'pax_desembarcados' => '6', 'pax_embarcados' => '0', 'hora_llegada' => '18:20', 'hora_salida' => '18:45', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => 'V', 'document' => '18739191', 'razon' => 'CARLOS MÉNDEZ', 'phone' => '(0412)-172.31.97', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV2892', 'modelo' => 'C-500', 'peso' => '5,38', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-06', 'pax_desembarcados' => '0', 'pax_embarcados' => '0', 'hora_llegada' => '13:30', 'hora_salida' => '14:10', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '301314522', 'razon' => 'INVERSIONES S.N.C. 88 CA.', 'phone' => '(0414)-481.76.23', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV2249', 'modelo' => 'AC-90', 'peso' => '4,68', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-07', 'pax_desembarcados' => '5', 'pax_embarcados' => '7', 'hora_llegada' => '09:55', 'hora_salida' => '16:08', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '810743339', 'razon' => 'MATADERO LOS LLANOS C.A', 'phone' => '(0424)-574.04.11', 'correo' => '', 'direccion' => 'BARINAS', 'aeropuerto_id' => '18', 'matricula' => 'YV102X', 'modelo' => 'EVOT', 'peso' => '2', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-07', 'pax_desembarcados' => '1', 'pax_embarcados' => '0', 'hora_llegada' => '11:10', 'hora_salida' => 'null', 'observacion' => 'BASADO'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '503209844', 'razon' => 'PRIVATE EXPERIENCE GROUP C.A', 'phone' => '(0424)-127.88.10', 'correo' => '', 'direccion' => 'BARQUISIMETO', 'aeropuerto_id' => '18', 'matricula' => 'YV1271', 'modelo' => 'AC-90', 'peso' => '4,68', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-08', 'pax_desembarcados' => '7', 'pax_embarcados' => '7', 'hora_llegada' => '14:19', 'hora_salida' => '15:45', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '312825928', 'razon' => 'TRANSCOMANDER C.A', 'phone' => '(0424)-180.79.91', 'correo' => '', 'direccion' => 'BARQUISIMETO', 'aeropuerto_id' => '18', 'matricula' => 'YV190T', 'modelo' => 'EC-30', 'peso' => '2,43', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-08', 'pax_desembarcados' => '2', 'pax_embarcados' => '2', 'hora_llegada' => '14:40', 'hora_salida' => '17:30', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'V', 'document' => '18243458', 'razon' => 'WILANDER GOMEZ', 'phone' => '(0424)-348.21.17', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3209', 'modelo' => 'C-500', 'peso' => '5,38', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-09', 'pax_desembarcados' => '7', 'pax_embarcados' => '7', 'hora_llegada' => '08:44', 'hora_salida' => '15:16', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '501319740', 'razon' => 'SAE SERVICIOS AEREOS EJECUTIVOS C.A', 'phone' => '(0424)-364.79.83', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3399', 'modelo' => 'C-550', 'peso' => '6,85', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-09', 'pax_desembarcados' => '7', 'pax_embarcados' => '7', 'hora_llegada' => '10:30', 'hora_salida' => '15:22', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '501294836', 'razon' => 'LS SERVICES & LOGISTES C.A', 'phone' => '(0424)-144.62.11', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV2808', 'modelo' => 'PAY-2', 'peso' => '4,08', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-09', 'pax_desembarcados' => '0', 'pax_embarcados' => '1', 'hora_llegada' => '11:20', 'hora_salida' => '17:30', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'PDVSA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV1118', 'modelo' => 'LJ-45', 'peso' => '9,75', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-09', 'pax_desembarcados' => '5', 'pax_embarcados' => '0', 'hora_llegada' => '14:52', 'hora_salida' => '15:15', 'observacion' => 'MEMBRESIA'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '810743339', 'razon' => 'MATADERO AVICOLA LOS LLANOS C.A', 'phone' => '(0424)-574.04.11', 'correo' => '', 'direccion' => 'BARINAS', 'aeropuerto_id' => '18', 'matricula' => 'YV102X', 'modelo' => 'EVOT', 'peso' => '2', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-10', 'pax_desembarcados' => '0', 'pax_embarcados' => '1', 'hora_llegada' => 'null', 'hora_salida' => '14:26', 'observacion' => 'BASADO'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'CONVIASA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV1009', 'modelo' => 'ATR-42', 'peso' => '18,6', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-10', 'pax_desembarcados' => '6', 'pax_embarcados' => '6', 'hora_llegada' => '14:41', 'hora_salida' => '15:19', 'observacion' => 'CONVIASA'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '409032574', 'razon' => 'INVERSIONES OCIANCA C.A', 'phone' => '(0414)-300.77.15', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3348', 'modelo' => 'LJ25', 'peso' => '6,8', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-10', 'pax_desembarcados' => '4', 'pax_embarcados' => '0', 'hora_llegada' => '18:20', 'hora_salida' => 'null', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '409032574', 'razon' => 'INVERSIONES OCIANCA C.A', 'phone' => '(0414)-300.77.15', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3348', 'modelo' => 'LJ25', 'peso' => '6,8', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-11', 'pax_desembarcados' => '0', 'pax_embarcados' => '0', 'hora_llegada' => 'null', 'hora_salida' => '09:41', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV3381', 'modelo' => 'BE-40', 'peso' => '7,3', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-11', 'pax_desembarcados' => '6', 'pax_embarcados' => '0', 'hora_llegada' => '10:22', 'hora_salida' => 'null', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '405141735', 'razon' => 'INVERSIONES MOTA 2014 C.A', 'phone' => '(0414)-444.12.11', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV651T', 'modelo' => 'LJ-55', 'peso' => '8,85', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-11', 'pax_desembarcados' => '5', 'pax_embarcados' => '0', 'hora_llegada' => '12:02', 'hora_salida' => '13:45', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '000684111', 'razon' => 'CONSTRUCCIONES YAMARO C.A', 'phone' => '(0414)-279.21.44', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3117', 'modelo' => 'BE-9L', 'peso' => '4,58', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-11', 'pax_desembarcados' => '3', 'pax_embarcados' => '0', 'hora_llegada' => '15:45', 'hora_salida' => 'null', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'FANB', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'GNB97116', 'modelo' => 'BELL-412', 'peso' => '5,4', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-11', 'pax_desembarcados' => '0', 'pax_embarcados' => '0', 'hora_llegada' => '17:30', 'hora_salida' => 'null', 'observacion' => 'FANB'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '000684111', 'razon' => 'CONSTRUCCIONES YAMARO C.A', 'phone' => '(0414)-279.21.44', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV3117', 'modelo' => 'BE-9L', 'peso' => '4,58', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-12', 'pax_desembarcados' => '0', 'pax_embarcados' => '4', 'hora_llegada' => 'null', 'hora_salida' => '09:02', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'SATA', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'YV3381', 'modelo' => 'BE-40', 'peso' => '7,3', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-12', 'pax_desembarcados' => '0', 'pax_embarcados' => '0', 'hora_llegada' => 'null', 'hora_salida' => '09:10', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => '', 'document' => '', 'razon' => 'FANB', 'phone' => '(0000)-000.00.00', 'correo' => '', 'direccion' => '', 'aeropuerto_id' => '18', 'matricula' => 'GNB97116', 'modelo' => 'BELL-412', 'peso' => '5,4', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-12', 'pax_desembarcados' => '0', 'pax_embarcados' => '6', 'hora_llegada' => 'null', 'hora_salida' => '09:56', 'observacion' => 'FANB'];
        $data[] = ['aprobado'=>false, 'type_document' => 'V', 'document' => '15751291', 'razon' => 'HECTOR VALECILLO', 'phone' => '(0414)-590.63.74', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV2770', 'modelo' => 'LJ-55', 'peso' => '8,85', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-12', 'pax_desembarcados' => '3', 'pax_embarcados' => '0', 'hora_llegada' => '10:40', 'hora_salida' => '11:22', 'observacion' => 'GENERAL'];
        $data[] = ['aprobado'=>false, 'type_document' => 'J', 'document' => '314454234', 'razon' => 'ARRENDADORA1721 C.A', 'phone' => '(0414)-789.43.61', 'correo' => '', 'direccion' => 'CARACAS', 'aeropuerto_id' => '18', 'matricula' => 'YV1018', 'modelo' => 'C-560', 'peso' => '7,39', 'nacionalidad_id' => '1', 'fecha_operacion' => '2023-03-12', 'pax_desembarcados' => '7', 'pax_embarcados' => '2', 'hora_llegada' => '13:24', 'hora_salida' => '14:05', 'observacion' => 'GENERAL'];

        foreach ($data as $value) {
            /*
            if ($value['document'] == "") {
                
                $Cliente = \App\Models\Clientes::where('tipo_documento', $value['type_document'])->where('documento', $value['document'])->first();
                if ($Cliente == null) {
                    $Cliente = new \App\Models\Clientes();
                    $Cliente->tipo_documento = $value['type_document'];
                    $Cliente->documento = $value['document'];
                }
                $Cliente->razon_social = $value['razon'];
                $Cliente->telefono = $value['phone'];
                $Cliente->correo = $value['correo'];
                $Cliente->direccion = $value['direccion'];

                $Cliente->tipo_id = 4; // general

                $Cliente->user_id = 1;
                $Cliente->ip = '1270.0.1';
                $Cliente->fecha_registro = now();
                $Cliente->save();
            } else {
                $Cliente = \App\Models\Clientes::find(1);
            }
            */
            $observaciones_facturacion = "";
            if ($value['document'] != "") {
                $observaciones_facturacion .= $value['type_document'].'|'.$value['document'].'|'.$value['razon'].'|'.$value['phone'].'|'.$value['direccion'];
            
                
            }
            
            //try {
            $Nave = \App\Models\Aeronaves::where('matricula', $value['matricula'])->first();
            if ($Nave == null) {
                $Nave = new \App\Models\Aeronaves();
                $Nave->matricula = $value['matricula'];
            }
            $Nave->nombre = Upper($value['modelo']);
            $Nave->peso_maximo = saveFloat($value['peso']) * 1000;
            $Nave->peso_maximo_certificado = 0;
            $Nave->observaciones = '';
            $Nave->maximo_pasajeros = 0;
            $Nave->estacion_id = 15;

            $Nave->user_id = 1;
            $Nave->ip = '127.0.0.1';
            $Nave->fecha_registro = now();
            $Nave->save();
            //} catch (\Exception $exc) {
            //echo $exc->getTraceAsString();
            //echo $e->getMessage();   /
            //  dd($value['matricula']);
            //}

            $last_operation = \App\Models\Vuelos::where("aeropuerto_id", $value['aeropuerto_id'])
                    ->where("activo", true)
                    ->orderBy("id", "desc")
                    ->first();
            $num_operation = $last_operation == null ? 100 : $last_operation->numero_operacion + 1;

            $Vuelos = new \App\Models\Vuelos();
            $Vuelos->aeropuerto_id = $value['aeropuerto_id'];
            $Vuelos->aeronave_id = $Nave->id;
            $Vuelos->piloto_id = 1;
            $Vuelos->numero_operacion = $num_operation;
            $Vuelos->tipo_vuelo_id = $value['nacionalidad_id'];
            $Vuelos->fecha_operacion = $value['fecha_operacion'];

            $Vuelos->pasajeros_desembarcados = $value['pax_desembarcados'];

            $Vuelos->pasajeros_embarcados = $value['pax_embarcados'];
            
            $Vuelos->hora_llegada = $value['hora_llegada'] == 'null' ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i', $value['fecha_operacion'] . ' ' . $value['hora_llegada'])->format('Y-m-d H:i');

            $Vuelos->hora_salida = $value['hora_salida'] == 'null' ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i', $value['fecha_operacion'] . ' ' . $value['hora_salida'])->format('Y-m-d H:i');

            $Vuelos->observaciones = $value['observacion'];
            $Vuelos->observaciones_facturacion = $observaciones_facturacion;
            $Vuelos->prodservicios_extra = '[]';

            $Vuelos->user_id = 1;
            $Vuelos->fecha_registro = now();
            $Vuelos->ip = '127.0.0.1';
            ;

            $Vuelos->save();

            $D['tipo_vuelo_id'] = 2; //Vuelos General
            $D['nacionalidad_id'] = $value['nacionalidad_id'];
            ; // NAcional
            $D['aeropuerto_id'] = $value['aeropuerto_id'];

            $D['hora_vuelo'] = ($value['hora_salida'] == 'null' ? $value['hora_llegada'] : $value['hora_salida']);
            $D['aeronave_id'] = $Nave->id;
            $D['plan_vuelo'] = 2; // Doble Toque
            $D['cant_pasajeros'] = $value['pax_embarcados'];
            $D['fecha_vuelo'] = $value['fecha_operacion'] . " 00:00:00";
            $D['serv_extra'] = [];
            $D['exento_dosa'] = false;
            $D['exento_tasa'] = false;

            ;

            $D['hora_llegada'] = $value['hora_llegada'] == 'null' ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i', $value['fecha_operacion'] . ' ' . $value['hora_llegada'])->format('Y-m-d H:i');
            $D['hora_salida'] = $value['hora_salida'] == 'null' ? null : \Carbon\Carbon::createFromFormat('Y-m-d H:i', $value['fecha_operacion'] . ' ' . $value['hora_salida'])->format('Y-m-d H:i');

            //getProdServAplicados
            /*
              $servicios_facturar = new \App\Http\Controllers\ProdserviciosController();
              $servicios_facturar->get_prodservicios($D);
             */

            $servicios_facturar = app(\App\Http\Controllers\Controller::class)->{'getProdServAplicados'}($D);
            //dd($servicios_facturar);

            foreach ($servicios_facturar['data'] as $value2) {
                //dd($value);
                $Detalle = new \App\Models\VuelosDetalle();

                $Detalle->aplica_categoria = $value2['aplica_categoria'];
                $Detalle->aplica_estacion = $value2['aplica_estacion'];
                $Detalle->aplica_peso = $value2['aplica_peso'];
                $Detalle->aplicable = $value2['aplicable'];
                $Detalle->bs = $value2['bs'];
                $Detalle->cant = $value2['cant'];
                $Detalle->categoria_aplicada = $value2['categoria_aplicada'];
                $Detalle->codigo = $value2['codigo'];
                $Detalle->prodservicio_id = \App\Helpers\Encryptor::decrypt($value2['crypt_id']);
                $Detalle->default_carga = $value2['default_carga'];
                $Detalle->default_dosa = $value2['default_dosa'];
                $Detalle->default_tasa = $value2['default_tasa'];
                $Detalle->descripcion = $value2['descripcion'];
                $Detalle->formula = $value2['formula'];
                $Detalle->formula2 = $value2['formula2'];
                $Detalle->full_descripcion = $value2['full_descripcion'];
                $Detalle->iva = $value2['iva'];
                $Detalle->iva2 = $value2['iva2'];
                $Detalle->iva_aplicado = $value2['iva_aplicado'];
                $Detalle->moneda = $value2['moneda'];
                $Detalle->nacionalidad_id = $value2['nacionalidad_id'];
                $Detalle->nomenclatura = $value2['nomenclatura'];
                $Detalle->peso_inicio = $value2['peso_inicio'];
                $Detalle->peso_fin = $value2['peso_fin'];
                $Detalle->precio = $value2['precio'];
                $Detalle->prodservicio_extra = $value2['prodservicio_extra'];
                $Detalle->tasa = $value2['tasa'];
                $Detalle->tipo_vuelo_id = $value2['tipo_vuelo_id'];
                $Detalle->turno_id = $value2['turno_id'];
                $Detalle->valor_dollar = $value2['valor_dollar'];
                $Detalle->valor_euro = $value2['valor_euro'];
                $Detalle->valor_petro = $value2['valor_petro'];
                $Detalle->orden = $value2['orden'];
                $Detalle->vuelo_id = $Vuelos->id;

                $Detalle->save();
            }
        }
        $this->info("Procesado");
        return true;
    }

}
