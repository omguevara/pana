<?php

namespace App\Exports;

use App\User;
use App\Models\Locales;
use App\Models\Concesiones;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class ConcesionesExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithEvents, WithColumnFormatting {

    private $ID = null;
    private $CONTADOR = 1;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function headings(): array {
        return [
            'N°',
            'CONCESION',
            'USO',
            'ACTIVIDAD',
            'Nº. LOCAL',
            'INGRESOS BRUTOS %',
            'M2',
            'CANON FIJO EN PETRO MENSUAL SEGÚN GACETA',
            'CONVENIO INCENTIVO EURO',
            'FECHA INCIO',
            'FECHA FIN',
        ];
    }

    public function map($row): array {
        // echo "<pre>"; var_dump($row->toArray());
        if ($row->empresas == null) {
            $concesion = $row->nombre_empresa;
        } else {
            $concesion = $row->empresas->razon_social;
        }
        $actividad = $row->actividades->actividad;
        
        $ingresos_brutos = saveFloat( $row->ingresos_brutos);
        $metros = saveFloat(  $row->locales->metros);
        $canon_fijo = saveFloat( $row->canon_fijo);
        $convenio_euro_integrado = saveFloat( $row->convenio_euro_integrado);
        
        $fecha_inicio = Date::convertIsoDate($row->fecha_inicio);
        $fecha_fin = Date::convertIsoDate($row->fecha_fin);
        

        // dd($row->toArray());

        return [
            $this->CONTADOR++,
            $concesion,
            $row->locales->usos_local->uso,
            $actividad,
            $row->locales->numero,
            
            $ingresos_brutos,
            $metros,
            $canon_fijo,
            $convenio_euro_integrado,
            
            $fecha_inicio,
            $fecha_fin,
        ];
    }

    public function collection() {
        //  $users = DB::table('proc_concesiones')->select('local_id','empresa_id','actividad_id','ingresos_brutos','canon_fijo','fecha_inicio','fecha_fin','user_id','fecha_registro_aud','ip','cedula','nombre','telefono','correo')->get();
        //  $locales = Locales::with('usos_local','concesion.empresas', 'concesion.actividades')->get();
        $concesiones = Concesiones::where('local_id', $this->ID)->with('locales.usos_local', 'actividades', 'empresas')->orderBy('id', 'asc')->get();
        //  dd($concesiones->toArray());
        return $concesiones;
    }

    public function __construct($cons) {
        //    dd($cons);
        $this->ID = \App\Helpers\Encryptor::decrypt($cons);
    }
    
    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'K' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            //'K' => NumberFormat::FORMAT_PERCENTAGE_00,
            
        ];
    }
    
    
    public function registerEvents(): array {

        // Borde simple
        $borderThin = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => 'thin',
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];
        $fontNegrita = [
            'font' => [
                'bold' => true,
                'size' => 10
            ]
        ];

        // Alinear al centro
        $colCenter = [
            'alignment' => [
                'horizontal' => 'center'
            ]
        ];

        return [
            AfterSheet::class => function (AfterSheet $event) use ($borderThin, $fontNegrita, $colCenter) {

                // $num = 1;
                // return [
                //     AfterSheet::class => function(AfterSheet $event) use ($num) {
                //         $event->sheet->getDelegate()->setCellValue('A1', 'Número');
                //         foreach ($event->sheet->getRowIterator() as $row) {
                //             $cell = $event->sheet->getCell('A' . $row->getRowIndex());
                //             if ($cell->getValue() === '') {
                //                 $cell->setValue($num++);
                //             }
                //         }
                //     }
                // ];


                $event->sheet->getDelegate()->getStyle('A1:k1')
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setARGB('8EB4E2');
                $event->sheet->getStyle('A1:k1')->ApplyFromArray($borderThin);
                $event->sheet->getStyle('A1:k1')->ApplyFromArray($fontNegrita);
                $event->sheet->getStyle('A1:k1')->ApplyFromArray($colCenter);

                $event->sheet->getDelegate()->getRowDimension('1')->setRowHeight(40);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(50);

                
            },
        ];
    }

}
