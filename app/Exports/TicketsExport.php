<?php

namespace App\Exports;

use App\User;
use App\Models\Locales;
use App\Models\Concesiones;
use DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;



use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class TicketsExport implements FromCollection, ShouldAutoSize, WithEvents
{
    private $aeropuerto_id;
    private $fechaInicio;
    private $fechaFin;
    private $spreadsheet;

    public function __construct($fechaInicio, $fechaFin, $aeropuerto_id)
    {
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
        $this->aeropuerto_id = $aeropuerto_id;
    }

    public function collection()
    {
        $where [] =[DB::raw("1"), "1"];
        $titulo = 'Ventas  De Cupones En Todos Los Aeropuertos';
        if ($this->aeropuerto_id != 0){
            $where [] =["aeropuerto_id", $this->aeropuerto_id];
            $titulo = 'Ventas De Cupones';
        }
        $tickets = \App\Models\VentaTickets::with('detalle.ticket.lote.aeropuerto', 'usuarios')
        ->where($where)
        ->whereBetween('fecha', [$this->fechaInicio, $this->fechaFin])
        ->whereHas('detalle.ticket', function ($query) {
            $query->where('vendido', true);
        })
        ->get();

        $this->spreadsheet = new Spreadsheet();
        $sheet = $this->spreadsheet->getActiveSheet();

           // Agregar un banner en la hoja de cálculo
           $drawing = new Drawing();
           $drawing->setPath(public_path('dist/img/banner.jpeg'));
           $drawing->setCoordinates('A1');
           $drawing->setHeight(50); // Establecer un alto específico para la imagen   
           $drawing->setWorksheet($sheet);

        if ($this->aeropuerto_id != 0){
            $sheet->mergeCells('A4:E4');
    
            $sheet->getStyle('A4:E4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A4:E4')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
                   $sheet->getStyle('A4:E4')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

                   // Aplicar un color de fondo a los títulos
                   $sheet->getStyle('A6:E6')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
                   $sheet->getStyle('A6:E6')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }else{
            $sheet->setCellValue('F6', 'Aeropuerto');
            $sheet->getColumnDimension('F')->setAutoSize(true);
            $sheet->mergeCells('A4:F4');
    
            $sheet->getStyle('A4:F4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A4:F4')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
                   $sheet->getStyle('A4:F4')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
                   // Aplicar un color de fondo a los títulos
                   $sheet->getStyle('A6:F6')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
                   $sheet->getStyle('A6:F6')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        }
        // Escribir los datos en la hoja de cálculo
        $sheet->setCellValue('A4', $titulo);

        $sheet->setCellValue('A6', 'Usuario');
        $sheet->setCellValue('B6', 'Fecha');
        $sheet->setCellValue('C6', 'Código');
        $sheet->setCellValue('D6', 'Tasa');
        $sheet->setCellValue('E6', 'Precio');



        // Aplicar el ajuste automático de ancho de columna
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);

      

        $row = 7;
        foreach ($tickets as $ticket) {
            // dd($ticket->detalle[0]->ticket->lote->aeropuerto->full_nombre);

            $sheet->setCellValue('A' . $row, $ticket->usuarios->name_user.' '.$ticket->usuarios->surname_user);
            $sheet->setCellValue('B' . $row, showDate($ticket->fecha));
            $sheet->setCellValue('C' . $row, $ticket->detalle[0]['codigo']);
            
            $sheet->setCellValueExplicit('D' . $row, $ticket->tasa, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC);
            $sheet->getStyle('D' . $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
            $sheet->getStyle('D' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            $sheet->setCellValueExplicit('E' . $row, $ticket->detalle[0]->precio, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC);
            $sheet->getStyle('E' . $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
            $sheet->getStyle('E' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
            
            if ($this->aeropuerto_id == 0){
                $sheet->setCellValue('F' . $row, $ticket->detalle[0]->ticket->lote->aeropuerto->full_nombre);
                $sheet->getStyle('F' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            }
            $sheet->getStyle('A:E')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $row++;
        }

        // Formatear los datos
        if ($this->aeropuerto_id != 0){
            $sheet->getStyle('A6:E6')->getFont()->setBold(true);
            $sheet->getStyle('A6:E6')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A6:E6' . ($row - 1))->getBorders()->getAllBorders()->setBorderStyle('thin');
        }else{
            $sheet->getStyle('A6:F6')->getFont()->setBold(true);
            $sheet->getStyle('A6:F6')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A6:F6' . ($row - 1))->getBorders()->getAllBorders()->setBorderStyle('thin');

        }
        $sheet->getStyle('E6:E6' . ($row - 1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        return $tickets;
    }

   

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setTitle('tickets');

                // Descargar el archivo en formato XLSX
                $writer = new Xlsx($this->spreadsheet);
                $filename = 'tickets ' . $this->fechaInicio->format('d-m-Y') . ' - ' . $this->fechaFin->format('d-m-Y') . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit;
            },
        ];
    }
}
