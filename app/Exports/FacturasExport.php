<?php

namespace App\Exports;

use App\User;
use App\Models\Locales;
use App\Models\Concesiones;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;



use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class FacturasExport implements FromCollection, ShouldAutoSize, WithEvents
{
    private $fechaInicio;
    private $fechaFin;
    private $spreadsheet;

    public function __construct($fechaInicio, $fechaFin)
    {
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
    }

    public function collection()
    {
        $facturas = \App\Models\Proforma::with('getDetalle')
            ->whereNotNull('factura_id')
            ->whereBetween('fecha_proforma', [$this->fechaInicio, $this->fechaFin])
            ->where('anulada', false)
            ->get();
        // dd($facturas);

        $this->spreadsheet = new Spreadsheet();
        $sheet = $this->spreadsheet->getActiveSheet();

         

        // Escribir los datos en la hoja de cálculo
        $sheet->setCellValue('A4', 'FECHA');
        $sheet->setCellValue('B4', 'RIF');
        $sheet->setCellValue('C4', 'Cliente');
        $sheet->setCellValue('D4', 'Teléfono');
        $sheet->setCellValue('E4', 'Base Imponible');
        $sheet->setCellValue('F4', 'Exento');
        $sheet->setCellValue('G4', 'IVA');
        $sheet->setCellValue('H4', 'Monto Total €');
          // Aplicar el ajuste automático de ancho de columna
          $sheet->getColumnDimension('A')->setAutoSize(true);
          $sheet->getColumnDimension('B')->setAutoSize(true);
          $sheet->getColumnDimension('C')->setAutoSize(true);
          $sheet->getColumnDimension('D')->setAutoSize(true);
          $sheet->getColumnDimension('E')->setAutoSize(true);
          $sheet->getColumnDimension('F')->setAutoSize(true);
          $sheet->getColumnDimension('G')->setAutoSize(true);
          $sheet->getColumnDimension('H')->setAutoSize(true);



             // Aplicar un color de fondo a los títulos
        $sheet->getStyle('A4:H4')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
        $sheet->getStyle('A4:H4')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);


        $row = 5;
        foreach ($facturas as $factura) {
            $sheet->setCellValue('A' . $row, $factura->fecha_proforma2);
            $sheet->setCellValue('B' . $row, $factura->cliente->rif);
            $sheet->setCellValue('C' . $row, $factura->cliente->razon_social);
            $sheet->setCellValue('D' . $row, $factura->cliente->telefono);
            $sheet->setCellValue('E' . $row, muestraFloat($factura->getBaseImponible()));
            $sheet->setCellValue('F' . $row, muestraFloat($factura->getExento()));
            $sheet->setCellValue('G' . $row, muestraFloat($factura->getIva()));
            $sheet->setCellValue('H' . $row, muestraFloat($factura->total));
            $row++;

           // Agregar el título "Detalle" debajo de la fila actual

            $sheet->mergeCells('A' . $row  . ':H' . $row);
            $sheet->setCellValue('A' . $row, 'Detalle');
            $sheet->getStyle('A' . $row)->getFont()->setBold(true);
            $sheet->getStyle('A' . $row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A' . $row)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
            $sheet->getStyle('A' . $row  . ':H' . $row)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
            $sheet->getRowDimension($row)->setRowHeight(20);
            $row++;
             // Agregar los títulos de las columnas de los detalles
 
            $sheet->setCellValue('B' . $row, 'Código');
            $sheet->setCellValue('C' . $row, 'Descripción');
            $sheet->setCellValue('D' . $row, 'Precio');
            $sheet->getStyle('D' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            // Escribir los detalles debajo del título "Detalle"
            $detalles = $factura->getDetalle;
            foreach ($detalles as $detalle) {
                $row++;
                
                $sheet->setCellValue('B' . $row , $detalle->codigo);
                $sheet->setCellValue('C' . $row , $detalle->descripcion);
                $sheet->setCellValueExplicit('D' . $row, $detalle->precio * $factura->tasa_euro, \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC);
                $sheet->getStyle('D' . $row)->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_00);
                $sheet->getStyle('D' . $row)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);


                
            } 


            $row++;

            $sheet->mergeCells('A' . $row  . ':H' . $row);
            $sheet->setCellValue('A' . $row, '***');
            $sheet->getStyle('A' . $row)->getFont()->setBold(true);
            $sheet->getStyle('A' . $row)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A' . $row  . ':H' . $row)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('e9e9fa');
            $sheet->getRowDimension($row)->setRowHeight(20);
            

            $row++;
        }

        

        // Formatear los datos
        $sheet->getStyle('A4:H4')->getFont()->setBold(true);
        $sheet->getStyle('A4:H4')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A4:H' . ($row - 1))->getBorders()->getAllBorders()->setBorderStyle('thin');
        $sheet->getStyle('E4:H' . ($row - 1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

          // Agregar un banner en la hoja de cálculo
          $drawing = new Drawing();
          $drawing->setPath(public_path('dist/img/banner.jpeg'));
          $drawing->setCoordinates('A1');
          $drawing->setHeight(50); // Establecer un alto específico para la imagen   
          $drawing->setWorksheet($sheet);

        return $facturas;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setTitle('Facturas');

                // Descargar el archivo en formato XLSX
                $writer = new Xlsx($this->spreadsheet);
                $filename = 'Facturas ' . showDate($this->fechaInicio) . ' - ' . showDate($this->fechaFin) . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit;
            },
        ];
    }
}
