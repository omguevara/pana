<?php

namespace App\Exports;

use App\Models\Vuelos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use Illuminate\Support\Facades\Auth;

class VuelosExportExcel {

    public function fabrica() {
        $request = \Request::all();
        $where [] = [DB::raw("1"), "1"];
        if (isset($request['aeropuerto_id'])) {
            if ($request['aeropuerto_id'] != null) {
                $where [] = ['aeropuerto_id', \App\Helpers\Encryptor::decrypt($request['aeropuerto_id'])];
            }
        }
        if (isset($request['aeronave_id'])) {
            if ($request['aeronave_id'] != null) {
                $where [] = ['aeronave_id', \App\Helpers\Encryptor::decrypt($request['aeronave_id'])];
            }
        }
        if (isset($request['piloto_id'])) {
            if ($request['piloto_id'] != null) {
                $where [] = ['piloto_id', \App\Helpers\Encryptor::decrypt($request['piloto_id'])];
            }
        }
        if (isset($request['rango'])) {
            if ($request['rango'] != null) {

                $fechas = explode(" - ", $request['rango']);
                $fecha_desde = $this->saveDate($fechas[0]);
                $fecha_hasta = $this->saveDate($fechas[1]);
                $where[] = [
                    function ($q) use ($fecha_desde, $fecha_hasta) {
                        $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$fecha_desde, $fecha_hasta]);
                    }
                ];
            }
        }
        if (isset($request['fecha_operacion'])) {
            if ($request['fecha_operacion'] != null) {

                $fechas = explode(" - ", $request['fecha_operacion']);
                $f1[0] = saveDate($fechas[0]);
                $f1[1] = saveDate($fechas[1]);

                $where[] = [
                    function ($q) use ($f1) {
                        $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                    }
                ];

                //$where [] = ['fecha_operacion', saveDate($request['fecha_operacion'])];
            }
        }
        if (Auth::user()->aeropuerto_id != null) {
            $datos = Vuelos::where('aeropuerto_id', Auth::user()->aeropuerto_id)
                    ->where($where)
                    ->where("activo", true)
                    ->where("aprobado", true)
                    ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||COALESCE(hora_operacion, '00:00')||':00') AS timestamp) "), "asc")
                    ->get();
        } else {
            $datos = Vuelos::where($where)
                    ->where("activo", true)
                    ->where("aprobado", true)
                    //->orderBy("fecha_operacion", "asc")
                    ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||COALESCE(hora_operacion, '00:00')||':00') AS timestamp) "), "asc")
                    ->get();
        }


        $ds = DIRECTORY_SEPARATOR;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Paid');
        $drawing->setDescription('Paid');
        $drawing->setPath('dist' . $ds . 'img' . $ds . 'banner.jpeg'); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(0);
        $drawing->setRotation(0);
        $drawing->getShadow()->setVisible(true);
        $drawing->getShadow()->setDirection(0);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        // $drawing = new Drawing();
        // $drawing->setName('Logo');
        // $drawing->setDescription('Logo');
        // $drawing->setPath('banner.jpeg');
        // $drawing->setCoordinates('A1');
        // $drawing->setOffsetX(5);
        // $drawing->setOffsetY(5);
        // $drawing->setWidth(100);
        // $drawing->setHeight(100);

        $sheet->getRowDimension('1')->setRowHeight(70);
        $sheet->getRowDimension('8')->setRowHeight(100);
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->getStyle('A8:AK8')->getAlignment()->setVertical('center');
        $sheet->getStyle('A8:AK8')->getAlignment()->setWrapText(true);

        $sheet
                ->getStyle('A7:AK7')
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_MEDIUM)
                ->setColor(new Color('000000'));

        $styleArray3 = ['borders' =>
            ['allBorders' =>
                ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,],
            ]
        ];
        $sheet->getStyle("A8:AK8")->applyFromArray($styleArray3);

        $styleArray = array('font' => array('bold' => true, 'color' => array('rgb' => '000000'), 'size' => 8, 'name' => 'Verdana'));
        $sheet->getStyle('A8:AK8')->applyFromArray($styleArray);

        $sheet
                ->getStyle('A7:D7')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('00b0f0');

        $sheet
                ->getStyle('A8:D8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('00b0f0');

        $sheet
                ->getStyle('E8:H8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('I8:J8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff0000');

        $sheet
                ->getStyle('K8:L8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff33cc');

        $sheet
                ->getStyle('M8:N8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('00b050');

        $sheet
                ->getStyle('O8:P8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('Q8:R8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff0000');

        $sheet
                ->getStyle('S8:U8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('V8:X8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff0000');

        $sheet
                ->getStyle('Y8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('Z8:AK8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ffd966');

        $sheet
                ->getStyle('Z7:AK7')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ffd966');

        // $spreadsheet->getActiveSheet()->getProtection()->setSheet(true); 
        $spreadsheet->getActiveSheet()->getStyle('A8:AK8')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        // $spreadsheet->getActiveSheet()->getProtection()->setPassword('123465');
        // $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
        // $spreadsheet->getActiveSheet()->getProtection()->setSort(false);
        // $spreadsheet->getActiveSheet()->getProtection()->setInsertRows(false);
        // $spreadsheet->getActiveSheet()->getProtection()->setFormatCells(false);
        $sheet->getColumnDimension('A')->setWidth(40);
        $sheet->getColumnDimension('B')->setWidth(40);
        $sheet->getColumnDimension('C')->setWidth(30);
        $sheet->getColumnDimension('D')->setWidth(15);

        $sheet->getStyle('Z8:AK8')->getAlignment()->setTextRotation(90); //rotar celdas

        $sheet->mergeCells('A7:D7')->setCellValue('A7', 'DATOS PROPIETARIO / RESPONSABLE DE LA AERONAVE');
        $sheet->mergeCells('Z7:AK7')->setCellValue('Z7', 'SERVICIOS  AEROPORTUARIOS PRESTADOS');

        $sheet->setCellValue('A8', 'NOMBRE O RAZÓN SOCIAL');
        $sheet->setCellValue('B8', 'DIRECCIÓN FISCAL');
        $sheet->setCellValue('C8', 'N° DOCUMENTO
            ( RIF / CÉDULA DE IDENTIDAD)');
        $sheet->setCellValue('D8', 'TELÉFONO');
        $sheet->setCellValue('E8', 'MATRÍCULA AERONAVE');
        $sheet->setCellValue('F8', 'TIPO AERONAVE');
        $sheet->setCellValue('G8', 'PESO MÁXIMO DE DESPEGUE (MTOW)');
        $sheet->setCellValue('H8', 'TIPO OPERACIÓN (NACIONAL O INTERNACIONAL)');
        $sheet->setCellValue('I8', 'RUTA LL');
        $sheet->setCellValue('J8', 'RUTA SA');
        $sheet->setCellValue('K8', 'OPS LL NAC');
        $sheet->setCellValue('L8', 'OPS SA NAC');
        $sheet->setCellValue('M8', 'OPS LL INT');
        $sheet->setCellValue('N8', 'OPS SA INT');
        $sheet->setCellValue('O8', 'FECHA LLEGADA');
        $sheet->setCellValue('P8', 'HORA DE LLEGADA (HLV)');
        $sheet->setCellValue('Q8', 'PASAJEROS DESEMBARCADOS LL NAC');
        $sheet->setCellValue('R8', 'PASAJEROS DESEMBARCADOS LL INT');
        $sheet->setCellValue('S8', 'FECHA SALIDA');
        $sheet->setCellValue('T8', 'HORA DE SALIDA (HLV)');
        $sheet->setCellValue('U8', 'PASAJEROS EMBARCADOS');
        $sheet->setCellValue('V8', 'PASAJEROS EMBARCADOS SA INT');
        $sheet->setCellValue('W8', 'PASAJEROS EMBARCADOS SA NAC');
        $sheet->setCellValue('X8', 'PLATAFORMA');
        $sheet->setCellValue('Y8', 'OBSERVACIONES');

        $sheet->setCellValue('Z8', 'DISPOSICIÓN FINAL DE DESECHOS SÓLIDOS'); //2.3
        $sheet->setCellValue('AA8', 'DESINFECCIÓN DE AERONAVES'); //2.4
        $sheet->setCellValue('AB8', 'GUÍA DE AERONAVES (FOLLOW ME)'); //2.6
        $sheet->setCellValue('AC8', 'TRASLADOS DE PASAJEROS Y EQUIPAJES DESDE Y HACIA AERONAVES '); //2.7
        $sheet->setCellValue('AD8', 'ASISTENCIA DE VUELO'); //2.8
        $sheet->setCellValue('AE8', 'ELABORACIÓN DE PLAN DE VUELO'); //2.9
        $sheet->setCellValue('AF8', 'SEÑALEROS, PLATAFORMA Y CALAS DE AERONAVES'); //2.10
        $sheet->setCellValue('AG8', 'ASPIRADO DE AERONAVES '); //2.11
        $sheet->setCellValue('AH8', 'REMOLQUE DE AERONAVES '); //2.12
        $sheet->setCellValue('AI8', 'USO DEL SALÓN VIP'); //2.13.1
        $sheet->setCellValue('AJ8', 'SERVICIO DE LIMPIEZA DE BAÑOS '); //2.13.2
        $sheet->setCellValue('AK8', ' SERVICIO DE UNIDAD DE POTENCIA ELÉCTRICA '); //2.13.3




        $data_add = [];
        /*
          $invoice->piloto->full_name,
          $invoice->piloto->telefono,
          $invoice->piloto->correo,
          $invoice->aeronave->matricula,
          $invoice->aeronave->nombre,
          muestraFloat($invoice->aeronave->peso_maximo/1000, 2),
          $invoice->tipo_vuelo,
          $invoice->fecha_operacion2,
          $invoice->pasajeros_desembarcados,
          $invoice->pasajeros_embarcados,
          $invoice->hora_llegada2,

          $invoice->hora_salida2,
          $invoice->observaciones
         */
        foreach ($datos as $vuelo) {
            if (!in_array($vuelo->id, $data_add)) {

                $servExtra = [];
                $servExtra['2.3'] = 0; //DISPOSICIÓN FINAL DE 
                $servExtra['2.4'] = 0; //DESINFECCIÓN DE AERONAVES
                $servExtra['2.6'] = 0; //GUIA DE AERONAVES 
                $servExtra['2.7'] = 0; //TRASLADOS DE PASAJEROS 
                $servExtra['2.8'] = 0; //ASISTENCIA DE VUELO
                $servExtra['2.9'] = 0; //ELABORACIÓN DE PLAN 
                $servExtra['2.10'] = 0; //SEÑALEROS, PLATAFORMA 
                $servExtra['2.11'] = 0; //ASPIRADO DE AERONAVES 
                $servExtra['2.12'] = 0; //REMOLQUE DE AERONAVES 
                $servExtra['2.13.1'] = 0; //USO DEL SALON 
                $servExtra['2.13.2'] = 0; //SERVICIO DE LIMPIEZA 
                $servExtra['2.13.3'] = 0; //SERVICIO DE UNIDAD 


                foreach ($servExtra as $cod => $cantidad) {
                    foreach ($vuelo->servicios_extra as $servs) {
                        $codigo = substr($servs['codigo'], 0, strlen($cod));
                        if ($codigo == $cod) {
                            $servExtra[$cod] += $servs['cant'];
                        }
                    }
                }


                $data_add[] = $vuelo->id;
                //dd($vuelo->toArray());
                $pax_llegada_inter = '0';
                $pax_llegada_nac = '0';
                $pax_salida_inter = '0';
                $pax_salida_nac = '0';
                $pax_llegada = '0';
                $pax_salida = '0';

                $fecha_llegada = '';
                $hora_llegada = '';
                $fecha_salida = '';
                $hora_salida = '';

                $orig = "";
                $dest = "";

                if ($vuelo->tipo_operacion_id == 1) { //si es llegada
                    $orig = $vuelo->getOrigenDestino != null ? $vuelo->getOrigenDestino->codigo_oaci : "";

                    $fecha_llegada = $vuelo->fecha_operacion2;
                    $hora_llegada = $vuelo->hora_operacion;
                    $pax_llegada = ($vuelo->pasajeros_desembarcados == 0 ? '0' : $vuelo->pasajeros_desembarcados);
                    if ($vuelo->tipo_vuelo_id == 1) {// si es nacional
                        $pax_llegada_nac = ($vuelo->pasajeros_desembarcados == 0 ? '0' : $vuelo->pasajeros_desembarcados);
                    } else {
                        $pax_llegada_inter = ($vuelo->pasajeros_desembarcados == 0 ? '0' : $vuelo->pasajeros_desembarcados);
                    }


                    $where = [];
                    $where [] = ['aeropuerto_id', $vuelo->aeropuerto_id];
                    $where [] = ['aeronave_id', $vuelo->aeronave_id];

                    if (isset($request['rango'])) {
                        if ($request['rango'] != null) {
                            $fecha_desde = $vuelo->fecha_operacion . ' ' . $vuelo->hora_operacion . ':00';
                            $ff = $fecha_hasta . ' 23:59:59';
                            $where[] = [
                                function ($q) use ($fecha_desde, $ff) {
                                    //$q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$fecha_desde, $fecha_hasta]);
                                    $q->where(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), '>=', $fecha_desde);
                                    $q->where(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), '<=', $ff);

                                    //$q->where('fecha_operacion', '<=', $fecha_hasta);
                                }
                            ];
                        }
                    }
                    if (isset($request['fecha_operacion'])) {
                        if ($request['fecha_operacion'] != null) {
                            $f1[0] = $vuelo->fecha_operacion . ' ' . $vuelo->hora_operacion . ':00';
                            $f2 = $f1[1] . ' 23:59:59';

                            $where[] = [
                                function ($q) use ($f1, $f2) {
                                    //$q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                                    //$q->whereDate('fecha_operacion', '>=', $f1[0]);
                                    //$q->whereDate('fecha_operacion', '<=', $f1[1]);

                                    $q->where(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), '>=', $f1[0]);
                                    $q->where(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), '<=', $f2);
                                }
                            ];
                        }
                    }

                    $data_salida = Vuelos::where($where)
                            ->where("activo", true)
                            ->where("aprobado", true)
                            ->where("tipo_operacion_id", 2)
                            ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), "asc")
                            //  ->take(1)
                            // ->toSql();
                            ->first();

                    if ($data_salida != null) {

                        $dest = $data_salida->getOrigenDestino != null ? $data_salida->getOrigenDestino->codigo_oaci : "";

                        $data_add[] = $data_salida->id;
                        $fecha_salida = $data_salida->fecha_operacion2;
                        $hora_salida = $data_salida->hora_operacion;

                        $pax_salida = ($data_salida->pasajeros_desembarcados == 0 ? '0' : $data_salida->pasajeros_desembarcados);
                        if ($vuelo->tipo_vuelo_id == 1) {// si es nacional
                            $pax_salida_nac = ($data_salida->pasajeros_desembarcados == 0 ? '0' : $data_salida->pasajeros_desembarcados);
                        } else {
                            $pax_salida_inter = ($data_salida->pasajeros_desembarcados == 0 ? '0' : $data_salida->pasajeros_desembarcados);
                        }

                        foreach ($servExtra as $cod => $cantidad) {
                            foreach ($data_salida->servicios_extra as $servs) {
                                $codigo = substr($servs['codigo'], 0, strlen($cod));
                                if ($codigo == $cod) {
                                    $servExtra[$cod] += $servs['cant'];
                                }
                            }
                        }
                    }
                } else {
                    $dest = $vuelo->getOrigenDestino != null ? $vuelo->getOrigenDestino->codigo_oaci : "";
                    $fecha_salida = $vuelo->fecha_operacion2;
                    $hora_salida = $vuelo->hora_operacion;
                    $pax_salida = ($vuelo->pasajeros_desembarcados == 0 ? '0' : $vuelo->pasajeros_desembarcados);
                    if ($vuelo->tipo_vuelo_id == 1) {// si es nacional
                        $pax_salida_nac = ($vuelo->pasajeros_desembarcados == 0 ? '0' : $vuelo->pasajeros_desembarcados);
                    } else {
                        $pax_salida_inter = ($vuelo->pasajeros_desembarcados == 0 ? '0' : $vuelo->pasajeros_desembarcados);
                    }
                }

                $data[] = [
                    Upper($vuelo->observaciones_facturacion2[2]),
                    Upper($vuelo->observaciones_facturacion2[4]),
                    Upper($vuelo->observaciones_facturacion2[0] . $vuelo->observaciones_facturacion2[1]),
                    Upper($vuelo->observaciones_facturacion2[3]),
                    Upper($vuelo->aeronave->matricula),
                    Upper($vuelo->aeronave->nombre),
                    muestraFloat($vuelo->aeronave->peso_maximo / 1000, 2),
                    $vuelo->tipo_vuelo,
                    $orig,
                    $dest,
                    "",
                    "",
                    "",
                    "",
                    $fecha_llegada,
                    $hora_llegada,
                    $pax_llegada_nac,
                    $pax_llegada_inter,
                    $fecha_salida,
                    $hora_salida,
                    $pax_salida,
                    $pax_salida_inter,
                    $pax_salida_nac,
                    '',
                    Upper($vuelo->observaciones),
                    ($servExtra['2.3'] > 0 ? $servExtra['2.3'] . "X" : ""),
                    ($servExtra['2.4'] > 0 ? $servExtra['2.4'] . "X" : ""),
                    ($servExtra['2.6'] > 0 ? $servExtra['2.6'] . "X" : ""),
                    ($servExtra['2.7'] > 0 ? $servExtra['2.7'] . "X" : ""),
                    ($servExtra['2.8'] > 0 ? $servExtra['2.8'] . "X" : ""),
                    ($servExtra['2.9'] > 0 ? $servExtra['2.9'] . "X" : ""),
                    ($servExtra['2.10'] > 0 ? $servExtra['2.10'] . "X" : ""),
                    ($servExtra['2.11'] > 0 ? $servExtra['2.11'] . "X" : ""),
                    ($servExtra['2.12'] > 0 ? $servExtra['2.12'] . "X" : ""),
                    ($servExtra['2.13.1'] > 0 ? $servExtra['2.13.1'] . "X" : ""),
                    ($servExtra['2.13.2'] > 0 ? $servExtra['2.13.2'] . "X" : ""),
                    ($servExtra['2.13.3'] > 0 ? $servExtra['2.13.3'] . "X" : ""),
                ];
            }
        }
        //dd($data);
        $sheet->fromArray($data, null, 'A9');
        
        
        //$styleArray = array('font' => array('bold' => true, 'color' => array('rgb' => '000000'), 'size' => 8, 'name' => 'Verdana'));
        $styleArray = array('font' => array('size' => 11, 'name' => 'Arial'));
        
        $sheet->getStyle('A9:AK'.(count($data)+8))->applyFromArray($styleArray);
        $sheet->getStyle('Z9:AK'.(count($data)+8))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G9:G'.(count($data)+8))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        
        $sheet->getStyle('Y9:Y'.(count($data)+8))->getAlignment()->setWrapText(true);
        
       
        
        // $writer = new Xlsx($spreadsheet);
        // $writer->save('user_list.xlsx');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="LISTADO_VUELOS.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

}
