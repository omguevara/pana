<?php

namespace App\Exports;

use App\User;
use App\Models\Locales;
use App\Models\Concesiones;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;



use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class InventarioEquipoExport implements FromCollection, ShouldAutoSize, WithEvents
{
    // private $fechaInicio;
    // private $fechaFin;
    private $filtro;
    private $spreadsheet;

    public function __construct($filtro)
    {
        // $this->fechaInicio = $fechaInicio;
        // $this->fechaFin = $fechaFin;
         $this->filtro = $filtro;
    }

    public function collection()
    {
 
        $where = [];
        $where[] = [DB::raw("1"), "1"];


        if (isset($this->filtro)) {
            foreach ($this->filtro as $value){
                if ($value === '1') {
                    /*
                    $where[] = [function ($q){ 
                        $q->where('irreparable', true);
                    }];
                    */
                    $where[] = ['irreparable', true];
                }
            }
        }

        $inventarios = \App\Models\InventarioEquipo::with('concesiones')->where($where);
        // Si el valor seleccionado es "perdida total"
        $inventarios = $inventarios->get();

        // dd($inventarios);
        $titulo = 'INVENTARIO DE EQUIPOS';

        $this->spreadsheet = new Spreadsheet();
        $sheet = $this->spreadsheet->getActiveSheet();

         

        // Escribir los datos en la hoja de cálculo
        $sheet->mergeCells('A4:F4');
        $sheet->setCellValue('A4', $titulo);

        $sheet->setCellValue('A6', 'SERIAL');
        $sheet->setCellValue('B6', 'MODELO');
        $sheet->setCellValue('C6', 'DESCRIPCIÓN');
        $sheet->setCellValue('D6', 'CANTIDAD');
        $sheet->setCellValue('E6', 'CONSESIÓN');
        $sheet->setCellValue('F6', 'UBICACIÓN');

          // Aplicar el ajuste automático de ancho de columna
          $sheet->getColumnDimension('A')->setAutoSize(true);
          $sheet->getColumnDimension('B')->setAutoSize(true);
          $sheet->getColumnDimension('C')->setAutoSize(true);
          $sheet->getColumnDimension('D')->setAutoSize(true);
          $sheet->getColumnDimension('E')->setAutoSize(true);
          $sheet->getColumnDimension('F')->setAutoSize(true);




             // Aplicar un color de fondo a los títulos
        $sheet->getStyle('A6:F6')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
        $sheet->getStyle('A6:F6')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);

        $sheet->getStyle('A4:F4')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('13028e');
        $sheet->getStyle('A4:F4')->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A4:F4')->getFont()->setBold(true);


        $row = 7;
        foreach ($inventarios as $inventario) {
            $sheet->setCellValue('A' . $row, $inventario->serial);
            $sheet->setCellValue('B' . $row, $inventario->modelo);
            $sheet->setCellValue('C' . $row, $inventario->descripcion);
            $sheet->setCellValue('D' . $row, muestraFloat($inventario->cant, 0));
            $sheet->setCellValue('E' . $row, Upper($inventario->concesiones->nombre));
            $sheet->setCellValue('F' . $row, $inventario->ubicacion_actual);
            // $sheet->setCellValue('A' . $row, $inventario->id);
            // $sheet->setCellValue('H' . $row, $inventario->estado);
            $sheet->getStyle('A:F')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $row++;
        }

        

        // Formatear los datos
        $sheet->getStyle('A6:F6')->getFont()->setBold(true);
        $sheet->getStyle('A6:F6')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A6:F' . ($row - 1))->getBorders()->getAllBorders()->setBorderStyle('thin');
        $sheet->getStyle('D' . ($row - 1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);

          // Agregar un banner en la hoja de cálculo
          $drawing = new Drawing();
          $drawing->setPath(public_path('dist/img/banner.jpeg'));
          $drawing->setCoordinates('A1');
          $drawing->setHeight(50); // Establecer un alto específico para la imagen   
          $drawing->setWorksheet($sheet);

        return $inventarios;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setTitle('Facturas');

                // Descargar el archivo en formato XLSX
                $writer = new Xlsx($this->spreadsheet);
                $filename = 'Inventario ' . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit;
            },
        ];
    }
}
