<?php

namespace App\Exports;

use App\Models\Membresia;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\Auth;

class Membresias implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize {

    public function saveDate($date, $separador = "/") {
        $date = explode($separador, $date);
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection() {

        $datos = Membresia::join('maest_aeronaves', 'maest_membresias.aeronave_id', '=', 'maest_aeronaves.id')
                ->where('fecha_hasta', '>=', date('Y-m-d'))
                ->get();

        //dd($datos[0]->piloto);
        return $datos;
        //return Vuelos::all();
    }

    public function map($invoice): array {
        
        return [
            $invoice->matricula,
            $invoice->nombre,
            $invoice->fecha_desde2,
            $invoice->fecha_hasta2,
            $invoice->tipo,
            
            
            $invoice->empresa,
            $invoice->exonerar2,
           
        ];
    }

    public function headings(): array {
        return ["MATRICULA", "AERONAVE", "FECHA DESDE", "FECHA HASTA", "TIPO", "EMPRESA", "EXONERAR"];
    }

}
