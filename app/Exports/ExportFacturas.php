<?php

namespace App\Exports;

use App\Models\Facturas;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportFacturas implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize {

    public function map($invoice): array {
        return [
            showDate($invoice->fecha_factura),
            Upper($invoice->codigo_oaci),
            Upper($invoice->codigo_iata),
            Upper( $invoice->aeropuerto),
            showCode($invoice->nro_factura),
            $invoice->tipo,
            $invoice->piloto,
            Upper($invoice->matricula.' '.$invoice->nombre),
            $invoice->cliente->razon_social,
            $invoice->monto
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection() {

        $request = \Request::all();
        //return Facturas::all();
        //dd($request);
        $where [] =[DB::raw("1"), "1"];
        if ($request['aeropuerto_id']){
            $where [] =["maest_aeropuertos.id", \App\Helpers\Encryptor::decrypt( $request['aeropuerto_id'])];
        }
        
        
    
        
        return Facturas::select([
                            "proc_facturas.id",
                            "proc_facturas.tipo_pago_id",
                            "proc_facturas.fecha_factura",
                            "proc_facturas.cliente_id",
                            "proc_facturas.nro_factura",
                            "maest_aeropuertos.codigo_oaci",
                            "maest_aeropuertos.codigo_iata",
                            "maest_aeropuertos.nombre as aeropuerto",
                            DB::raw("CASE  WHEN proc_vuelos_generales.factura_tasa IS NULL THEN 'DOSA' WHEN proc_vuelos_generales.factura_dosa IS NULL THEN 'TASA' ELSE ''  END  AS tipo"),
                            DB::raw("CASE  WHEN piloto_tasa.id IS NOT NULL THEN piloto_tasa.nombres    WHEN piloto_dosa.id IS NOT NULL THEN piloto_dosa.nombres  ELSE '' END  AS piloto"),
                            "maest_aeronaves.matricula",
                            "maest_aeronaves.nombre",
                            DB::raw("sum(proc_facturas_detalle.precio) as monto"),
                        ])
                        ->leftJoin("proc_facturas_detalle", "proc_facturas_detalle.factura_id", "=", "proc_facturas.id")
                        ->leftJoin("proc_vuelos_generales", function ($join)  {
                            $join->where("proc_vuelos_generales.factura_tasa", "=", DB::raw("proc_facturas.id"));
                            $join->orWhere("proc_vuelos_generales.factura_dosa", "=", DB::raw("proc_facturas.id"));
                        })
                        ->leftJoin("maest_aeropuertos", "maest_aeropuertos.id", "=", "proc_vuelos_generales.aeropuerto_id")
                        ->leftJoin("maest_aeronaves", "maest_aeronaves.id", "=", "proc_vuelos_generales.aeronave_id")
                                
                                
                        ->leftJoin("maest_pilotos as piloto_tasa", function ($join) {
                            $join->where("piloto_tasa.id", "=", DB::raw("proc_vuelos_generales.piloto_salida_id"));
                            //$join->Where("proc_vuelos_generales.factura_tasa", "!=", DB::raw("NULL"));
                        })
                        
                        ->leftJoin("maest_pilotos as piloto_dosa", function ($join) {
                            $join->where("piloto_dosa.id", "=", DB::raw("proc_vuelos_generales.piloto_llegada_id"));
                            //$join->Where("proc_vuelos_generales.factura_dosa", "!=", DB::raw("NULL"));
                        })
                                
                        ->whereDate("proc_facturas.fecha_factura", saveDate($request['fecha']))
                        ->where($where)        
                        ->groupBy("proc_facturas.id",
                                "proc_facturas.tipo_pago_id",
                                "proc_facturas.fecha_factura",
                                "proc_facturas.cliente_id",
                                "proc_facturas.nro_factura",
                                "maest_aeropuertos.codigo_oaci",
                                "maest_aeropuertos.codigo_iata",
                                "maest_aeropuertos.nombre",
                                "maest_aeronaves.matricula",
                                "maest_aeronaves.nombre",
                                "proc_vuelos_generales.factura_tasa",
                                "proc_vuelos_generales.factura_dosa",
                                "piloto_tasa.id",
                                "piloto_dosa.id",
                                "piloto_dosa.nombres",
                                "piloto_tasa.nombres")
                        ->get();
    }

    public function headings(): array {
        return ["FECHA", "OACI", "IATA", "AEROPUERTO", "NUMERO FACTURA", "TIPO", "PILOTO", "AERONAVE", "RESPONSABLE DE PAGO", "MONTO"];
    }

}
