<?php

namespace App\Exports;

use App\Models\Vuelos;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\Auth;

class VuelosExportBaer implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize {

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection() {
        $request = \Request::all();
        $where [] = [DB::raw("1"), "1"];
        if (isset($request['aeropuerto_id'])) {
            if ($request['aeropuerto_id'] != null) {
                $where [] = ['aeropuerto_id', \App\Helpers\Encryptor::decrypt($request['aeropuerto_id'])];
            }
        }
        if (isset($request['aeronave_id'])) {
            if ($request['aeronave_id'] != null) {
                $where [] = ['aeronave_id', \App\Helpers\Encryptor::decrypt($request['aeronave_id'])];
            }
        }
        if (isset($request['piloto_id'])) {
            if ($request['piloto_id'] != null) {
                $where [] = ['piloto_id', \App\Helpers\Encryptor::decrypt($request['piloto_id'])];
            }
        }
        if (isset($request['fecha_operacion'])) {
            if ($request['fecha_operacion'] != null) {
                
                $fechas = explode(" - ", $request['fecha_operacion']);
                $f1[0] = saveDate($fechas[0]);
                $f1[1] = saveDate($fechas[1]);
                
                $where[] = [
                    function ($q) use ($f1) {
                        $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                    }
                ];
                
                //$where [] = ['fecha_operacion', saveDate($request['fecha_operacion'])];
            }
        }

        if (Auth::user()->aeropuerto_id != null) {
            $datos = Vuelos::where('aeropuerto_id', Auth::user()->aeropuerto_id)
                    ->where($where)
                    ->get();
        } else {
            $datos= Vuelos::where($where)
                    
                    ->orderBy("id", "desc")
                    ->get();
        }
        //dd($datos[0]->piloto);
        return $datos;
        //return Vuelos::all();
    }
    public function map($invoice): array {
        return [
            
            $invoice->piloto->full_name,
            $invoice->piloto->telefono,
            $invoice->piloto->correo,
            $invoice->aeronave->matricula,
            $invoice->aeronave->nombre,
            muestraFloat($invoice->aeronave->peso_maximo/1000, 2),
            $invoice->tipo_vuelo,
            $invoice->fecha_operacion2,
            $invoice->pasajeros_desembarcados,
            $invoice->pasajeros_embarcados,
            $invoice->hora_llegada2,
            
            $invoice->hora_salida2,
            $invoice->observaciones
        ];
    }
    public function headings(): array {
        return [ "NOMBRE PILOTO", "TELÉFONO PILOTO", "CORREO PILOTO", "MATRÍCULA", "TIPO AERONAVE ", "PESO AERONAVE", "TIPO OPERACIÓN", "FECHA OPERACIÓN", "PAX DESEMBARCADOS", "PAX EMBARCADOS", "HORA LLEGADA", "HORA SALIDA", "OBSERVACIONES"];
    }

}
