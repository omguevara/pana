<?php

namespace App\Exports;

use App\User;
use App\Models\Locales;
use App\Models\Concesiones;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;



use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class ProformasExport implements FromCollection, ShouldAutoSize, WithEvents 
{
    private $fechaInicio;
    private $fechaFin;
    private $spreadsheet;

    public function __construct($fechaInicio, $fechaFin, $aeropuerto_id)
    {
        $this->fechaInicio = $fechaInicio;
        $this->fechaFin = $fechaFin;
        $this->aeropuerto_id = $aeropuerto_id;
    }

    public function collection()
    {
        
        $where [] =[DB::raw("1"), "1"];
        if ($this->aeropuerto_id != 0){
            $where [] =["aeropuerto_id", $this->aeropuerto_id];
        }
        $proformas = \App\Models\Proforma::with('usuarios', 'getDetalle')->where($where)->whereDate('fecha_proforma', '>=',$this->fechaInicio)->whereDate('fecha_proforma', '<=', $this->fechaFin)->where('anulada', false)->orderBy('id', 'asc')->get();
        
        $this->spreadsheet = new Spreadsheet();
        $sheet = $this->spreadsheet->getActiveSheet();

        // Escribir los datos en la hoja de cálculo
        $sheet->setCellValue('A1', 'Proforma #');
        $sheet->setCellValue('B1', 'Aeropuerto');
        $sheet->setCellValue('C1', 'Fecha de Vuelo');
        $sheet->setCellValue('D1', 'Tipo de Vuelo');
        $sheet->setCellValue('E1', 'Documento');
        $sheet->setCellValue('F1', 'Telefono');
        $sheet->setCellValue('G1', 'Razón Social');
        $sheet->setCellValue('H1', 'Matricula');
        $sheet->setCellValue('I1', 'Estatus del Pago');
        $sheet->setCellValue('J1', 'Base imponible');
        $sheet->setCellValue('K1', 'Exento');
        $sheet->setCellValue('L1', 'IVA');
        $sheet->setCellValue('M1', 'Monto total');
        $sheet->setCellValue('N1', 'Usuario factura emitida');

        // Aplicar el ajuste automático de ancho de columna
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getColumnDimension('D')->setAutoSize(true);
        $sheet->getColumnDimension('E')->setAutoSize(true);
        $sheet->getColumnDimension('F')->setAutoSize(true);
        $sheet->getColumnDimension('G')->setAutoSize(true);
        $sheet->getColumnDimension('H')->setAutoSize(true);
        $sheet->getColumnDimension('I')->setAutoSize(true);
        $sheet->getColumnDimension('J')->setAutoSize(true);
        $sheet->getColumnDimension('K')->setAutoSize(true);
        $sheet->getColumnDimension('L')->setAutoSize(true);
        $sheet->getColumnDimension('M')->setAutoSize(true);
        $sheet->getColumnDimension('N')->setAutoSize(true);

        $row = 2;
        foreach ($proformas as $proforma) {
            $sheet->setCellValue('A' . $row, $proforma->nro_proforma);
            $sheet->setCellValue('B' . $row, $proforma->aeropuerto->full_nombre);
            $sheet->setCellValue('C' . $row, $proforma->fecha_proforma2);
            if ($proforma->tipo_vuelo_id == 1) {
                $sheet->setCellValue('D' . $row, 'Nacional');
            } elseif ($proforma->tipo_vuelo_id == 2) {
                $sheet->setCellValue('D' . $row, 'Internacional');
            } else {
                $sheet->setCellValue('D' . $row, 'No registrado');
            }
            $sheet->setCellValue('E' . $row, $proforma->cliente->rif);
            $sheet->setCellValue('F' . $row, $proforma->cliente->telefono);
            $sheet->setCellValue('G' . $row, $proforma->cliente->razon_social);
            $sheet->setCellValue('H' . $row, $proforma->aeronave->matricula);

            if($proforma->deuda_actual == 0){
                if ($proforma->pagado == true) {
                    $text = "PAGADO";
                }
            }else{
                if ($proforma->total_monto_abonado == 0) {
                    $text = "POR PAGAR";
                }else {
                    $cantidad = muestraFloat($proforma->total_monto_abonado);
                    $text = "MONTO ABONADO: $cantidad";
                }
            }
            $sheet->setCellValue('I' . $row, $text);
            $sheet->setCellValue('J' . $row, muestraFloat($proforma->getBaseImponible()));
            $sheet->setCellValue('K' . $row, muestraFloat($proforma->getExento()));
            $sheet->setCellValue('L' . $row, muestraFloat($proforma->getIva()));
            $sheet->setCellValue('M' . $row, muestraFloat($proforma->total));
            $sheet->setCellValue('N' . $row, $proforma->usuarios->name_user.' '.$proforma->usuarios->surname_user);
            $row++;
        }

        // Formatear los datos
        $sheet->getStyle('A1:N1')->getFont()->setBold(true);
        $sheet->getStyle('A1:N1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A1:N' . ($row - 1))->getBorders()->getAllBorders()->setBorderStyle('thin');
        $sheet->getStyle('J2:M' . ($row - 1))->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        return $proformas;
    }



    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setTitle('Proformas');

                // Descargar el archivo en formato XLSX
                $writer = new Xlsx($this->spreadsheet);
                $filename = 'Proformas ' . showDate($this->fechaInicio) . ' - ' . showDate($this->fechaFin) . '.xlsx';
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="' . $filename . '"');
                header('Cache-Control: max-age=0');
                $writer->save('php://output');
                exit;
            },
        ];
    }
}