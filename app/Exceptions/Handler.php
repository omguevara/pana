<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Throwable;

class Handler extends ExceptionHandler {

    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
            //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    /*
      public function register() {
      $this->reportable(function (Throwable $e) {
      //
      });
      }
     */
    public function render($request, Throwable $exception) {

        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {

            if ($request->expectsJson()) {
                $json = [
                    'status' => 0,
                    'type' => 'error',
                    //'message' => $exception->getMessage()
                    'message' => __("Sessión Perdida")
                ];
                return response()
                                ->json($json, 200);
            }
            if ($request->ajax()) {
                return redirect()->route('errors.403');
            } else {
                return redirect()->to('login');
            }

            //return redirect('login')->with('msg', ["type" => "warning", "msg" => __("Page Expired")]);
        }

        return parent::render($request, $exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception) {
        if ($request->expectsJson()) {


            $json = [
                'status' => 0,
                'type' => 'error',
                'message' => __("Sessión Perdida") //$exception->getMessage()
            ];
            return response()
                            ->json($json, 200);
        }
        if ($request->ajax()) {
            return redirect()->route('errors.403');
        } else {
            return redirect()->to('login');
        }
    }

}
