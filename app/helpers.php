<?php

use \App\Http\Controllers\Controller;

if (!function_exists('decodeTypeCierre')) {

    function decodeTypeCierre($tipo = null) {
        $Controller = new Controller();

        return $Controller->TIPOS_CIERRE_NOMBRE[$tipo];
    }

}


if (!function_exists('decodeSentidos')) {

    function decodeSentidos($numero = null) {
        $Controller = new Controller();
        if ($numero == null) {
            return $Controller->SENTIDOS;
        } else {
            return $Controller->SENTIDOS[$numero];
        }
    }

}


if (!function_exists('showCode')) {

    function showCode($numero = null, $zero = 8) {
        return str_pad($numero, $zero, "0", STR_PAD_LEFT);
    }

}

if (!function_exists('Upper')) {

    function Upper($text) {
        return mb_strtoupper($text, 'UTF8');
    }

}

if (!function_exists('Lower')) {

    function Lower($text) {
        return mb_strtolower($text, 'UTF8');
    }

}

if (!function_exists('muestraFloat')) {

    function muestraFloat($monto, $dec = 2) {//se7ho
        return number_format($monto, $dec, ',', '.');
    }

}

if (!function_exists('showActions')) {

    function showActions($text) {//se7ho
        if ($text == '*') {
            return "";
        }
        return __(ucwords(str_replace("_", " ", $text)));
    }

}

if (!function_exists('showTime')) {

    function showTime($hour, $min, $seg = null) {
        $turno = $hour < 12 ? "AM" : "PM";
        $hour = (int) $hour;
        $min = (int) $min;
        $hour = ($hour < 10 ? "0" . $hour : ($hour > 9 && $hour < 13 ? $hour : ($hour - 12 < 10 ? "0" . ($hour - 12) : ($hour - 12))) );

        return $hour . ':' . ($min < 10 ? "0" : "") . $min . ":" . ($seg == null ? "00" : $seg) . " " . ($turno);
    }

}


if (!function_exists('showDate')) {

    function showDate($date, $op = "date", $format24H = false) {
        if ($date != null) {
            if ($op == 'date') {
                $date = explode(" ", $date);
                $date = $date[0];
                $date = explode("-", $date);

                $return = $date[2] . '/' . $date[1] . '/' . $date[0];
            }
            if ($op == 'hour') {
                $date = explode(" ", $date);
                $date = $date[1];
                $date = explode(":", $date);

                $return = showTime($date[0], $date[1]);
            }

            if ($op == 'full') {
                $date = explode(" ", $date);
                $f = $date[0];
                $t = $date[1];

                $fecha = explode("-", $f);
                $hora = explode(":", $t);
                //return $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0];
                if ($format24H == false) {
                    $return = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0] . ' ' . showTime($hora[0], $hora[1]);
                } else {
                    $return = $fecha[2] . '/' . $fecha[1] . '/' . $fecha[0] . ' ' . $hora[0] . ":" . $hora[1];
                }
            }

            if ($op == 'flat') {
                $date = explode(" ", $date);
                $f = $date[0];
                $t = $date[1];

                $fecha = explode("-", $f);


                $return = $fecha[0] . $fecha[1] . $fecha[2];
            }
        } else {
            $return = "";
        }


        return $return;
    }

}
if (!function_exists('saveFloat')) {

    function saveFloat($monto) {//se7ho
        return str_replace(',', '.', str_replace('.', '', $monto));
    }

}


if (!function_exists('saveDate')) {

    function saveDate($date, $separador = "/") {
        $date = explode($separador, $date);
        return $date[2] . '-' . $date[1] . '-' . $date[0];
    }

}


if (!function_exists('saveDateTime')) {

    function saveDateTime($date) {
        $f = explode(" ", $date);

        $dateTime = saveDate($f[0]) . " " . $f[1];
        return $dateTime;
    }

}

if (!function_exists('verificarCuenta')){
    function verificarCuenta($banco, $oficina, $digitos, $num_cuenta) {
        $pesos1 = array(3, 2, 7, 6, 5, 4, 3, 2);
        $pesos2 = array(3, 2, 7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
        $cuenta = $banco . $oficina . $digitos . $num_cuenta;
        if (strlen($cuenta) != 20) {
            return false;
        }
        $campos1 = $banco . $oficina;
        $campos2 = $oficina . $num_cuenta;
        $digitos1 = (int)$campos1;
        $digitos2 = (int)$campos2;
        $suma1 = 0;
        $suma2 = 0;
        for ($i = 0; $i < 8; $i++) {
            $digito = (int)(($digitos1 / pow(10.0, (7 - $i) * 1.0))) % 10;
            $suma1 += $pesos1[$i] * $digito;
        }
        for ($i = 0; $i < 14; $i++) {
            $digito = (int)(($digitos2 / pow(10.0, (13 - $i) * 1.0))) % 10;
            $suma2 += $pesos2[$i] * $digito;
        }
        $digito1 = (11 - ($suma1 % 11));
        $digito2 = (11 - ($suma2 % 11));
        if ($digito1 >= 10 || $digito1 < 1) {
            $digito1 = $digito1 % 10;
        }
        if ($digito2 >= 10 || $digito2 < 1) {
            $digito2 = $digito2 % 10;
        }
        //echo $digito1 . '-' . $digito2;
        $cuentaValidada = $banco . $oficina . $digito1 . $digito2 . $num_cuenta;
        return $cuenta == $cuentaValidada;
    }    
}
