<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Feriados;

class FeriadosController extends Controller {

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Feriados_active = Feriados::get();

            $Feriados_active = Datatables::of($Feriados_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("delete", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('feriados.delete', $row->crypt_id) . '" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('delete') . '</a> ';
                        }




                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Feriados_active->original['data'];

            // dd($data2);

            return view('feriados.index', compact('data1', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'fecha' => 'required',
                            'nombre' => 'required',
                                ], [
                            'fecha.required' => __('El Campo Fecha es Requerido'),
                            'nombre.required' => __('El Campo Nombre es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {
                    //dd("thdfghd");
                    $Feriado = new Feriados();
                    $Feriado->fecha = $this->saveDate($request->fecha);
                    $Feriado->nombre = Upper($request->nombre);

                    $Feriado->user_id = Auth::user()->id;
                    $Feriado->ip = $this->getIp();
                    $Feriado->fecha_registro = now();

                    $Feriado->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                return view('feriados.create');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function delete($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {


                $Feriado = Feriados::find(Encryptor::decrypt($id));
                $Feriado->delete();
                return redirect()->route('feriados');
            } else {
                return Redirect::to('/');
            }
        } else {
            return Redirect::to('/');
        }
    }

}
