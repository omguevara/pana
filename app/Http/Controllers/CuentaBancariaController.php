<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Sesion;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use App\Models\Bancos;
use DataTables;
use PDF;
use App\Models\CuentaBancaria;

class CuentaBancariaController extends Controller
{
    public function get_data_cliente($tipo_doc, $doc) {
        if (\Request::ajax()) {

            $Cliente = Clientes::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();

            if ($Cliente == null) {

                $Cliente = \App\Models\Pilotos::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();
                if ($Cliente == null) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Datos No Encontrado');
                    $result['data'] = null;
                } else {
                    $C = new \App\Models\Clientes();
                    $C->tipo_documento = $tipo_doc;
                    $C->documento = $doc;

                    $C->razon_social = Upper($Cliente->nombres . ' ' . $Cliente->apellidos);
                    $C->telefono = ($Cliente->telefono==null ? "":$Cliente->telefono);
                    $C->correo = ($Cliente->correo==null ? "":$Cliente->correo);
                    $C->direccion = "";
                    
                    $C->tipo_id = 4; // general

                    $C->user_id = Auth::user()->id;
                    $C->ip = $this->getIp();
                    $C->fecha_registro = now();
                    $C->save();

                    $Cliente = $C;

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Datos Encontrados');
                    $result['data'] = $Cliente->toArray();
                }
            } else {
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Datos Encontrados');
                $result['data'] = $Cliente->toArray();
            }
            return response()->json($result);
        }
    }

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $cuentas_bancarias_active = CuentaBancaria::with('getBancos')->get();

            $cuentas_bancarias_active = Datatables::of($cuentas_bancarias_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('cuentas_bancarias.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }
                        if ($row->activo == true) {
                            if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('cuentas_bancarias.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"> <li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                            }
                        } else {
                            if (in_array("active", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('cuentas_bancarias.active', $row->crypt_id) . '" class="actions_users btn btn-success btn-sm"> <li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                            }
                        }
                        return $actionBtn;
                    })
                    ->addColumn('estatus', function($row){
                        $estatus = ($row->activo == true) ? $estatus = 'Activo' : $estatus = 'Inactivo';
                        return $estatus;
                    })

                    ->rawColumns(['action'])
                    ->make(true);

            $cuentas = $cuentas_bancarias_active->original['data'];

            // dd($data2);

            return view('cuentas_bancarias.index', compact('cuentas', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                    $request->all(), [
                    'numero_cuenta' => 'required|numeric|max:99999999999999999999',
                    'banco_id' => 'required',
                        ], [
                        'numero_cuenta.required' => __('El Número de Cuenta es Requerido'),
                        'numero_cuenta.max' => __('El Numero de Cuenta no puede ser mayor de 20 Digitos'),
                        'banco_id.required' => __('El Banco es Requerido'),
                    ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $banco = substr($request->numero_cuenta, 0, 4);
                    $oficina = substr($request->numero_cuenta, 4, 4);
                    $digitos = substr($request->numero_cuenta, 8, 2);
                    $num_cuenta = substr($request->numero_cuenta, 10);

                    $resultado = verificarCuenta($banco, $oficina, $digitos, $num_cuenta);
                    if ($resultado != true) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('La Cuenta ingresada no es válida');
                        
                        return $result;
                    }

                    $check_banco = \App\Models\Bancos::find(Encryptor::decrypt($request->banco_id));

                    if ($banco != $check_banco->codigo) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El banco seleccionado no coincide con la cuenta ingresada');
                        
                        return $result;
                    }

                    $CuentaBancaria = \App\Models\CuentaBancaria::where("numero_cuenta", $request->numero_cuenta)->count();

                    if ($CuentaBancaria == 0) {
                        $CuentaBancaria = new \App\Models\CuentaBancaria();
                        $CuentaBancaria->numero_cuenta = $request->numero_cuenta;
                        $CuentaBancaria->banco_id = Encryptor::decrypt($request->banco_id);
                        
                        $CuentaBancaria->user_id = Auth::user()->id;
                        $CuentaBancaria->ip = $request->ip();
                        $CuentaBancaria->fecha_registro = now();
                        
                        $CuentaBancaria->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('La Cuenta ya Existe');
                    }
                }
                return $result;
            } else {
                $Bancos = \App\Models\Bancos::where("activo", 1)->get()->pluck('nombre', 'crypt_id');
                return view('cuentas_bancarias.create', compact('Bancos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                    $request->all(), [
                    'numero_cuenta' => 'required|numeric|max:99999999999999999999',
                    'banco_id' => 'required',
                        ], [
                        'numero_cuenta.required' => __('El Número de Cuenta es Requerido'),
                        'numero_cuenta.max' => __('El Numero de Cuenta no puede ser mayor de 20 Digitos'),
                        'banco_id.required' => __('El Banco es Requerido'),
                    ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {
                        
                        $banco = substr($request->numero_cuenta, 0, 4);
                        $oficina = substr($request->numero_cuenta, 4, 4);
                        $digitos = substr($request->numero_cuenta, 8, 2);
                        $num_cuenta = substr($request->numero_cuenta, 10);

                        $resultado = verificarCuenta($banco, $oficina, $digitos, $num_cuenta);
                        if ($resultado != true) {
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = __('La Cuenta ingresada no es válida');
                            
                            return $result;
                        }

                        $check_banco = \App\Models\Bancos::find(Encryptor::decrypt($request->banco_id));

                        if ($banco != $check_banco->codigo) {
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = __('El banco seleccionado no coincide con la cuenta ingresada');
                            
                            return $result;
                        }


                    $CuentaBancaria = \App\Models\CuentaBancaria::where("numero_cuenta", $request->document)->where("id", "!=", Encryptor::decrypt($id))->count();

                    if ($CuentaBancaria == 0) {
                        $CuentaBancaria = CuentaBancaria::find(Encryptor::decrypt($id));
                        $CuentaBancaria->numero_cuenta = $request->numero_cuenta;
                        $CuentaBancaria->banco_id = Encryptor::decrypt($request->banco_id);

                        $CuentaBancaria->user_id = Auth::user()->id;
                        $CuentaBancaria->ip = $request->ip();
                        $CuentaBancaria->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('La Cuenta ya Existe');
                    }

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Procesed Correctly');
                }
                return $result;
            } else {
                $Bancos = \App\Models\Bancos::where("activo", 1)->get()->pluck('nombre', 'crypt_id');
                $CuentaBancaria = CuentaBancaria::with('getBancos')->find(Encryptor::decrypt($id));
                return view('cuentas_bancarias.edit', compact('CuentaBancaria', 'Bancos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $CuentaBancaria = CuentaBancaria::find(Encryptor::decrypt($id));
                $CuentaBancaria->activo = false;

                $CuentaBancaria->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesed Correctly');

                return $result;
            } else {

                $CuentaBancaria = CuentaBancaria::find(Encryptor::decrypt($id));
                return view('cuentas_bancarias.disable', compact('CuentaBancaria'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $CuentaBancaria = CuentaBancaria::find(Encryptor::decrypt($id));
                $CuentaBancaria->activo = true;

                $CuentaBancaria->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesed Correctly');

                return $result;
            } else {

                $CuentaBancaria = CuentaBancaria::find(Encryptor::decrypt($id));
                return view('cuentas_bancarias.active', compact('CuentaBancaria'));
            }
        } else {
            return Redirect::to('/');
        }
    }
}
