<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Hangares;

class InfoSeguridadController extends Controller
{
    public function save(Request $request){
        //dd($request);
        $v = Validator::make($request->all(),
            [
                "s_aad" => "required",
                "s_ona" => "required",
                "s_onm" => "required",
                "s_ats" => "required",
                "s_ac" => "required",
                "s_seh" => "required",
                "s_avsec" => "required",
                "s_gnb_a" => "required",
                "s_seniat" => "required",
                "s_saime"=>"required",
                "s_ds"=>"required",
                "s_gnb_rn"=>"required",
                "s_pnb"=>"required",
                "s_sebin"=>"required",
                "s_ns_avsec"=>"required",
                "s_na_avsec"=>"required",
                "s_edmp_o"=>"required",
                "s_edmp_i"=>"required",
                "s_edmm_o"=>"required",
                "s_edmm_i"=>"required",
                "s_mrpc_o"=>"required",
                "s_mrpc_i"=>"required",
                "s_mb_o"=>"required",
                "s_mb_i"=>"required",
                "s_rp_o"=>"required",
                "s_rp_i"=>"required",
                "s_rb_o"=>"required",
                "s_rb_i"=>"required",
                "s_c_o"=>"required",
                "s_c_i"=>"required",
                "s_g_o"=>"required",
                "s_g_i"=>"required",
                "s_mech_o"=>"required",
                "s_mech_i"=>"required",
                "s_nsavseci"=>"required",
                "s_nsavsecr"=>"required",
                "s_naavseci"=>"required",
                "s_naavsecr"=>"required"
            ], 
            [
                "s_aad.required" => "required",
                "s_ona.required" => "required",
                "s_onm.required" => "required",
                "s_ats.required" => "required",
                "s_ac.required" => "required",
                "s_seh.required" => "required",
                "s_avsec.required" => "required",
                "s_gnb_a.required" => "required",
                "s_seniat.required" => "required",
                "s_saime.required"=>"required",
                "s_ds.required"=>"required",
                "s_gnb_rn.required"=>"required",
                "s_pnb.required"=>"required",
                "s_sebin.required"=>"required",
                "s_ns_avsec.required"=>"required",
                "s_na_avsec.required"=>"required",
                "s_edmp_o.required"=>"required",
                "s_edmp_i.required"=>"required",
                "s_edmm_o.required"=>"required",
                "s_edmm_i.required"=>"required",
                "s_mrpc_o.required"=>"required",
                "s_mrpc_i.required"=>"required",
                "s_mb_o.required"=>"required",
                "s_mb_i.required"=>"required",
                "s_rp_o.required"=>"required",
                "s_rp_i.required"=>"required",
                "s_rb_o.required"=>"required",
                "s_rb_i.required"=>"required",
                "s_c_o.required"=>"required",
                "s_c_i.required"=>"required",
                "s_g_o.required"=>"required",
                "s_g_i.required"=>"required",
                "s_mech_o.required"=>"required",
                "s_mech_i.required"=>"required",
                "s_nsavseci.required"=>"required",
                "s_nsavsecr.required"=>"required",
                "s_naavseci.required"=>"required",
                "s_naavsecr.required"=>"required"
            ]
        );

        if (!$v->fails()) {//si no fallan las validaciones
            $ProInfoSeguridad=new Proc_Info_Seguridad();

            $ProInfoSeguridad->s_aad=$request->s_aad;//1. HORAS DE FUNCIONAMIENTO
            $ProInfoSeguridad->s_ona=$request->s_ona;
            $ProInfoSeguridad->s_onm=$request->s_onm;
            $ProInfoSeguridad->s_ats=$request->s_ats;
            $ProInfoSeguridad->s_ac=$request->s_ac;
            $ProInfoSeguridad->s_seh=$request->s_seh;
            $ProInfoSeguridad->s_avsec=$request->s_avsec;
            $ProInfoSeguridad->s_gnb_a=$request->s_gnb_a;
            $ProInfoSeguridad->s_seniat=$request->s_seniat;
            $ProInfoSeguridad->s_saime=$request->s_saime;
            $ProInfoSeguridad->s_ds=$request->s_ds;
            $ProInfoSeguridad->s_gnb_rn=$request->s_gnb_rn;
            $ProInfoSeguridad->s_pnb=$request->s_pnb;
            $ProInfoSeguridad->s_sebin=$request->s_sebin;
            $ProInfoSeguridad->s_ns_avsec=$request->s_ns_avsec;
            $ProInfoSeguridad->s_na_avsec=$request->s_na_avsec;//2. PERSONAL Y EQUIPO DE AVSEV
            $ProInfoSeguridad->s_edmp_o=$request->s_edmp_o;
            $ProInfoSeguridad->s_edmp_i=$request->s_edmp_i;
            $ProInfoSeguridad->s_edmm_o=$request->s_edmm_o;
            $ProInfoSeguridad->s_edmm_i=$request->s_edmm_i;
            $ProInfoSeguridad->s_mrpc_o=$request->s_mrpc_o;
            $ProInfoSeguridad->s_mrpc_i=$request->s_mrpc_i;
            $ProInfoSeguridad->s_mb_o=$request->s_mb_o;
            $ProInfoSeguridad->s_mb_i=$request->s_mb_i;
            $ProInfoSeguridad->s_rp_o=$request->s_rp_o;
            $ProInfoSeguridad->s_rp_i=$request->s_rp_i;
            $ProInfoSeguridad->s_rb_o=$request->s_rb_o;
            $ProInfoSeguridad->s_rb_i=$request->s_rb_i;
            $ProInfoSeguridad->s_c_o=$request->s_c_o;
            $ProInfoSeguridad->s_c_i=$request->s_c_i;
            $ProInfoSeguridad->s_g_o=$request->s_g_o;
            $ProInfoSeguridad->s_g_i=$request->s_g_i;
            $ProInfoSeguridad->s_mech_o=$request->s_mech_o;
            $ProInfoSeguridad->s_mech_i=$request->s_mech_i;
            $ProInfoSeguridad->s_nsavseci=$request->s_nsavseci;
            $ProInfoSeguridad->s_nsavsecr=$request->s_nsavsecr;
            $ProInfoSeguridad->s_naavseci=$request->s_naavseci;
            $ProInfoSeguridad->s_naavsecr=$request->s_naavsecr;
            $ProInfoSeguridad->s_cls=$request->s_cls;//boolean
            $ProInfoSeguridad->s_pls=$request->s_pls;
            $ProInfoSeguridad->s_pimsacad=$request->s_pimsacad;
            $ProInfoSeguridad->s_pccmsacad=$request->s_pccmsacad;
            $ProInfoSeguridad->ip=$request->ip();
            $ProInfoSeguridad->user_id=Auth::id();
            $ProInfoSeguridad->aeropuerto_id=1;
            $ProInfoSeguridad->save();

            $LogProcInfoSeguridad=new Log_Proc_Info_Seguridad();
            $LogProcInfoSeguridad->s_aad=$request->s_aad;//1. HORAS DE FUNCIONAMIENTO
            $LogProcInfoSeguridad->s_ona=$request->s_ona;
            $LogProcInfoSeguridad->s_onm=$request->s_onm;
            $LogProcInfoSeguridad->s_ats=$request->s_ats;
            $LogProcInfoSeguridad->s_ac=$request->s_ac;
            $LogProcInfoSeguridad->s_seh=$request->s_seh;
            $LogProcInfoSeguridad->s_avsec=$request->s_avsec;
            $LogProcInfoSeguridad->s_gnb_a=$request->s_gnb_a;
            $LogProcInfoSeguridad->s_seniat=$request->s_seniat;
            $LogProcInfoSeguridad->s_saime=$request->s_saime;
            $LogProcInfoSeguridad->s_ds=$request->s_ds;
            $LogProcInfoSeguridad->s_gnb_rn=$request->s_gnb_rn;
            $LogProcInfoSeguridad->s_pnb=$request->s_pnb;
            $LogProcInfoSeguridad->s_sebin=$request->s_sebin;
            $LogProcInfoSeguridad->s_ns_avsec=$request->s_ns_avsec;//2. PERSONAL Y EQUIPO DE AVSEV
            $LogProcInfoSeguridad->s_na_avsec=$request->s_na_avsec;
            $LogProcInfoSeguridad->s_edmp_o=$request->s_edmp_o;
            $LogProcInfoSeguridad->s_edmp_i=$request->s_edmp_i;
            $LogProcInfoSeguridad->s_edmm_o=$request->s_edmm_o;
            $LogProcInfoSeguridad->s_edmm_i=$request->s_edmm_i;
            $LogProcInfoSeguridad->s_mrpc_o=$request->s_mrpc_o;
            $LogProcInfoSeguridad->s_mrpc_i=$request->s_mrpc_i;
            $LogProcInfoSeguridad->s_mb_o=$request->s_mb_o;
            $LogProcInfoSeguridad->s_mb_i=$request->s_mb_i;
            $LogProcInfoSeguridad->s_rp_o=$request->s_rp_o;
            $LogProcInfoSeguridad->s_rp_i=$request->s_rp_i;
            $LogProcInfoSeguridad->s_rb_o=$request->s_rb_o;
            $LogProcInfoSeguridad->s_rb_i=$request->s_rb_i;
            $LogProcInfoSeguridad->s_c_o=$request->s_c_o;
            $LogProcInfoSeguridad->s_c_i=$request->s_c_i;
            $LogProcInfoSeguridad->s_g_o=$request->s_g_o;
            $LogProcInfoSeguridad->s_g_i=$request->s_g_i;
            $LogProcInfoSeguridad->s_mech_o=$request->s_mech_o;
            $LogProcInfoSeguridad->s_mech_i=$request->s_mech_i;
            $LogProcInfoSeguridad->s_nsavseci=$request->s_nsavseci;
            $LogProcInfoSeguridad->s_nsavsecr=$request->s_nsavsecr;
            $LogProcInfoSeguridad->s_naavseci=$request->s_naavseci;
            $LogProcInfoSeguridad->s_naavsecr=$request->s_naavsecr;
            $LogProcInfoSeguridad->s_cls=$request->s_cls;//boolean
            $LogProcInfoSeguridad->s_pls=$request->s_pls;
            $LogProcInfoSeguridad->s_pimsacad=$request->s_pimsacad;
            $LogProcInfoSeguridad->s_pccmsacad=$request->s_pccmsacad;
            $LogProcInfoSeguridad->ip=$request->ip();
            $LogProcInfoSeguridad->user_id=Auth::id();
            $LogProcInfoSeguridad->aeropuerto_id=1;
            $LogProcInfoSeguridad->tip_operacion='I';
            $LogProcInfoSeguridad->save();

            $message = array('status' => true, 'type' => 'success', 'title' => 'REGISTRADO', 'message' => 'Datos Registrado');
        } else {
            $message = array('status' => false, 'type' => 'warning', 'title' => 'ERROR', 'message' => $v->errors()->first());
        }
        return $message;
    }
}
