<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Exports\ProformasExport;
use App\Exports\FacturasExport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Exports\ExportFacturas;
use App\Models\ProformaPago;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Vuelos;

class RecaudacionController extends Controller {

    public function libro_ventas($fechas = null) {
        if (\Request::ajax()) {
            dd("Hola");
        }
    }

    public function view_fact($id, $tasa = null, $num = null) {


        $Proforma = \App\Models\Proforma::find(Encryptor::decrypt($id));

        $Proforma->factura_id = $Proforma->id;
        if ($num != null) {

            $isNumFact = \App\Models\Proforma::where('nro_factura', $num)->where('id', '!=', $Proforma->id)->count();
            if ($isNumFact == 0) {
                $Proforma->nro_factura = $num;
            } else {

                $msg = __('El Número de Factura ya Esta Registrado');
                return view('errors.general', compact('msg'));
            }
        }
        if ($tasa != null) {

            $Proforma->tasa_euro = saveFloat($tasa);
        }

        $Proforma->save();

        $Vuelos = Vuelos::where("proforma_id", Encryptor::decrypt($id))->update(["factura_id" => Encryptor::decrypt($id)]);

        return view('reportes.proforma.second', compact('Proforma'));
    }

    public function pagos(Request $request){
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $clientes = \App\Models\Clientes::get()->pluck('full_razon_social', 'crypt_id');

                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("pertenece_baer", true)
                        ->whereIn("id", Auth::user()->lista_aeropuertos)
                        ->get()
                ->pluck("full_nombre", "crypt_id");

                return view('recaudacion.efectuar_pago', ['Aeropuertos' => $Aeropuertos, 'numero_factura' => '', 'clientes' => $clientes]);
            } else {
                $Validator = Validator::make(
                    $request->all(), [
                        'idproforma' => 'required',
                        'pagar' => 'required',
                    ], [
                        'pagar.required' => 'Campo Requerido',
                    ]
                );
                if ($Validator->fails()) {
                    $message['icon'] = 'error';
                    $message['title'] = '¡Error!';
                    $message['message'] = $Validator->errors()->first();

                    return $message;
                }else {
                    $Proforma = \App\Models\Proforma::find(Encryptor::decrypt($request->idproforma));
                    $monto = ((float) saveFloat($request->pagar));

                    if ($monto <= round($Proforma->deuda_actual, 2) ) {

                        $pago = new ProformaPago();
                        $pago->proforma_id = $Proforma->id;
                        $pago->fecha_pago = Carbon::now();
                        $pago->user_id = Auth::user()->user_id;
                        $pago->monto = $monto;
                        $pago->save();

                        $Proforma = \App\Models\Proforma::find(Encryptor::decrypt($request->idproforma));
                        if ($Proforma->deuda_actual == 0) {
                            $Proforma->pagado = true;
                            $Proforma->save();
                        }

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesado Correctamente');
                        $result['data'] = \App\Models\Proforma::find(Encryptor::decrypt($request->idproforma));

                        return $result;
                    } else {
                        $message['icon'] = 'error';
                        $message['title'] = '¡Error!';
                        $message['message'] = __('El monto ingresado es mayor al total a pagar');
                        return $message;
                    }



                }
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }
    public function obtenerProforma(Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $query = \App\Models\Proforma::where('anulada', false)->orderBy('id', 'asc');
                if ($request->estatus_id != null) {

                    $estatus = $request->estatus_id == 1 ? $estatus = true : $estatus = false;

                    $query->where('pagado', $estatus);
                }
                if ($request->cliente_id != '') {
                    $query->where('cliente_id', Encryptor::decrypt($request->cliente_id));
                }
                if ($request->rango_fecha != ''){
                    $rango_fecha = $request->rango_fecha;
                    $rango_fecha = explode(' - ', $rango_fecha);

                    $fecha_inicio = $rango_fecha[0];
                    $fecha_fin = $rango_fecha[1];

                    $query->whereBetween('fecha_proforma', [saveDate($fecha_inicio), saveDate($fecha_fin)]);
                }
                $query->whereIn("aeropuerto_id", Auth::user()->lista_aeropuertos);
                if ($request->aeropuerto_id != ''){
                    $query->where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id));
                }

                $proforma = $query->get();
                $data = Datatables::of($proforma)

                        ->addColumn('boton', function ($row) {
                            $button = '';
                            if ($row->pagado==false){

                            $button = '<button type="button" onclick="datosModalProforma(\'' . $row->crypt_id . '\', \'' . $row->fecha_proforma2 . '\' , \'' . $row->aeropuerto->nombre . '\', \'' . $row->aeronave->full_nombre_tn . '\', \'' . muestraFloat($row->total) . '\', \'' . muestraFloat($row->total_monto_abonado) . '\')" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#proformaModal" title="Proforma"><i class="fas fa-file-invoice-dollar"></i> Pagar</button>';
                               }
                            return $button;
                        })
                        ->addColumn('rif', function($row){
                            return $row->cliente->rif;
                        })
                        ->addColumn('total2', function($row){
                            return '€ '.muestraFloat($row->total);
                        })
                        ->addColumn('total_monto_abonado2', function($row){
                            return '€ '.muestraFloat($row->total_monto_abonado);
                        })
                        ->addColumn('class', function ($row) {
                            $class = "";
                            if($row->deuda_actual == 0){
                                if ($row->pagado == true) {
                                    $class = "alert-success";
                                }
                            }else{
                                if ($row->total_monto_abonado == 0) {
                                    $class = "alert-warning";
                                }else {
                                    $class = "alert-info";
                                }
                            }
                            return $class;
                        })

                        ->rawColumns(['boton'])
                    // ->make(true)
                ;

                return $data->toJson();
            }else{
                return exit;
            }
        }
        else{
            return Redirect::to('/dashboard');
        }
    }
    public function view_proforma($id) {
        $Proforma = \App\Models\Proforma::with('getDetalle')->find(Encryptor::decrypt($id));
        // dd($Proforma);
        return view('reportes.proforma.first', compact('Proforma'));
    }

    public function reporte_proforma() {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }
                return view('reporte_proformas.index', ['Aeropuertos' => $Aeropuertos]);
            }
        }else {
            exit;
        }
    }
    public function reporte_cupones() {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }
                return view('reporte_tickets.index', ['Aeropuertos' => $Aeropuertos]);
            }
        }else {
            exit;
        }
    }

    public function emitir_factura(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("pertenece_baer", true)
                        ->whereIn("id", Auth::user()->lista_aeropuertos)
                        ->get()
                ->pluck("full_nombre", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                $numero_factura = \App\Models\Proforma::max("nro_factura");
                $numero_factura = $numero_factura == null ? 0 : $numero_factura;

                return view('recaudacion.facturar', compact('Aeronaves', 'Aeropuertos', 'numero_factura'));
            } else {
                if ($request->isMethod('post')) {
                    dd($request->all());
                } else {
                    $Proformas = \App\Models\Proforma::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                            ->where("aeronave_id", Encryptor::decrypt($request->aeronave_id))
                            //->whereNull("factura_id")
                            ->get(); //dd($Vuelos);
                    //dd($Proformas);
                    $data = Datatables::of($Proformas)
                            ->addColumn('action', function ($row) {
                                $actionBtn = '';
                                if ($row->anulada == true) {
                                    /*
                                      if ($row->factura_id == null) {//SI ES PROFORMA
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';
                                      //$actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';
                                      //$actionBtn .= $row->tipo_documento;
                                      }else{//SI ES FACTURA
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', [$row->crypt_id, muestraFloat($row->tasa_euro), $row->nro_factura]) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-xs"> <li class="fa fa-check"></li> </a> ';
                                      //$actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', [$row->crypt_id, muestraFloat($row->tasa_euro), $row->nro_factura]) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-danger btn-xs"> <li class="fa fa-eye"></li> </a> ';
                                      //$actionBtn .= $row->tipo_documento;
                                      }
                                     */
                                } else {
                                    if ($row->factura_id == null) {//SI NO ESTA FACTURADA
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="factProforma(\'' . url('view-fact', $row->crypt_id) . '\', \'' . $row->pagado . '\')" href="javascript:void(0)" title="Facturar" class=" btn btn-success btn-xs"> <li class="fa fa-save"></li> </a> ';
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="anulaProforma(\'' . route('facturar_pospago.anular_prof', $row->crypt_id) . '\')" href="javascript:void(0)" title="Anular Proforma" class=" btn btn-warning btn-xs"> <li class="fa fa-trash"></li> </a> ';
                                        //$actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';
                                        //$actionBtn .= $row->tipo_documento;
                                    } else {//SI ES FACTURA
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', [$row->crypt_id, muestraFloat($row->tasa_euro), $row->nro_factura]) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-xs"> <li class="fa fa-check"></li> </a> ';
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="anularFactura(\'' . url('anular-fact', $row->crypt_id) . '\')" href="javascript:void(0)" title="Anular Factura" class=" btn btn-danger btn-xs"> <li class="fa fa-trash"></li> </a> ';
                                        //$actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', [$row->crypt_id, muestraFloat($row->tasa_euro), $row->nro_factura]) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-danger btn-xs"> <li class="fa fa-eye"></li> </a> ';
                                        //$actionBtn .= $row->tipo_documento;
                                    }
                                    //$actionBtn .= '<a onClick="seeProforma(\'' . route('view_proforma', $row->crypt_id) . '\')" href="javascript:void(0)" title="Ver Vuelo" class=" btn btn-warning btn-sm"> <li class="fa fa-eye"></li> </a> ';
                                    /*
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="sendProformaEmail(\'' . $row->crypt_id . '\')" href="javascript:void(0)" title="Enviar Proforma por Correo" class=" btn btn-primary btn-xs"> <li class="fa fa-mail-bulk"></li> </a> ';



                                      if ($row->factura_id == null) {//si esta en proforma
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="factProforma(\'' . url('view-fact', $row->crypt_id) . '\')" href="javascript:void(0)" title="Facturar" class=" btn btn-success btn-xs"> <li class="fa fa-save"></li> </a> ';
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="anulaProforma(\'' . route('facturar_pospago.anular_prof', $row->crypt_id) . '\')" href="javascript:void(0)" title="Anular Proforma" class=" btn btn-warning btn-xs"> <li class="fa fa-trash"></li> </a> ';
                                      } else {
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', [$row->crypt_id, muestraFloat($row->tasa_euro), $row->nro_factura]) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-xs"> <li class="fa fa-check"></li> </a> ';
                                      $actionBtn .= '<a data-toggle="tooltip" onClick="anularFactura(\'' . url('anular-fact', $row->crypt_id) . '\')" href="javascript:void(0)" title="Anular Factura" class=" btn btn-danger btn-xs"> <li class="fa fa-trash"></li> </a> ';

                                      $actionBtn .= '<a data-toggle="tooltip" onClick="setAjax(this, event)" href="' . route('facturar_pospago.pagar_factura', $row->crypt_id) . '" title="Pagar Factura" class=" btn btn-warning btn-xs"><li class="fa fa-money-bill"></li> </a> ';
                                      }
                                     *
                                     */
                                }
                                return $actionBtn;
                            })
                            ->addIndexColumn()
                            ->rawColumns(['action'])
                            ->make(true);

                    $data = $data->original['data'];

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $data;
                    return $result;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function pagar_factura($id) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Factura = \App\Models\Proforma::find(\App\Helpers\Encryptor::decrypt($id));
                return view('recaudacion.pagos', compact('Factura'));
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function anular_fact($id) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Factura = \App\Models\Proforma::find(\App\Helpers\Encryptor::decrypt($id));
                $Factura->anulada = true;
                $Factura->save();

                $Vuelos = \App\Models\Vuelos::where("proforma_id", \App\Helpers\Encryptor::decrypt($id))->update(["factura_id" => null, "proforma_id" => null]);
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function anular_prof($id) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {

                $Factura = \App\Models\Proforma::find(\App\Helpers\Encryptor::decrypt($id));
                $Factura->anulada = true;
                $Factura->save();

                $Vuelos = \App\Models\Vuelos::where("proforma_id", \App\Helpers\Encryptor::decrypt($id))->update(["factura_id" => null, "proforma_id" => null]);

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = null;
                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function delete_item($id) {
        if (\Request::ajax()) {

            $Detalle = \App\Models\VuelosDetalle::find(Encryptor::decrypt($id))->delete();

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            $result['data'] = null;
            return $result;
        }
    }

    public function add_item() {
        if (\Request::ajax()) {
            $data = \Request::all();

            $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($data['id']));

            $data['exento_dosa'] = true;
            $data['exento_tasa'] = true;
            $data['serv_extra']['ids'] = [];
            $data['serv_extra']['cant'] = [];
            $serv_extra = [];

            foreach ($data['data'] as $key => $value) {
                $d = explode(":", $value);
                $data['serv_extra']['ids'][] = Encryptor::decrypt($d[0]);
                $data['serv_extra']['cant'][$d[0]] = $d[1];
            }
            $data['tipo_vuelo_id'] = 2; //Vuelos General
            $data['nacionalidad_id'] = $Vuelo->tipo_vuelo_id; //nacionalidad_id; // NAcional
            $data['aeropuerto_id'] = $Vuelo->aeropuerto_id;
            $data['hora_vuelo'] = $Vuelo->hora_operacion;

            $data['hora_llegada_est'] = $Vuelo->hora_llegada;
            $data['hora_salida_est'] = $Vuelo->hora_salida;

            $data['aeronave_id'] = $Vuelo->aeronave_id;

            $data['cant_pasajeros'] = $Vuelo->pasajeros_embarcados;
            $data['fecha_vuelo'] = $Vuelo->fecha_operacion;
            $data['hora_vuelo'] = $Vuelo->hora_operacion;

            $servicios_facturar = $this->getProdServ($data);
            //dd($servicios_facturar);
            foreach ($servicios_facturar['data'] as $value) {
                $Detalle = new \App\Models\VuelosDetalle();

                $Detalle->aplica_categoria = $value['aplica_categoria'];
                $Detalle->aplica_estacion = $value['aplica_estacion'];
                $Detalle->aplica_peso = $value['aplica_peso'];
                $Detalle->aplicable = '';
                $Detalle->bs = $value['bs'];
                $Detalle->cant = $value['cant'];
                $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                $Detalle->codigo = $value['codigo'];
                $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                $Detalle->default_carga = $value['default_carga'];
                $Detalle->default_dosa = $value['default_dosa'];
                $Detalle->default_tasa = $value['default_tasa'];
                $Detalle->descripcion = $value['descripcion'];
                $Detalle->formula = $value['formula'];
                $Detalle->formula2 = $value['formula2'];
                $Detalle->full_descripcion = $value['full_descripcion'];
                $Detalle->iva = $value['iva'];
                $Detalle->iva2 = $value['iva2'];
                $Detalle->iva_aplicado = $value['iva_aplicado'];
                $Detalle->moneda = $value['moneda'];
                $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                $Detalle->nomenclatura = $value['nomenclatura'];
                $Detalle->peso_inicio = $value['peso_inicio'];
                $Detalle->peso_fin = $value['peso_fin'];
                $Detalle->precio = $value['precio'];
                $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                $Detalle->tasa = $value['tasa'];
                $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                $Detalle->turno_id = $value['turno_id'];
                $Detalle->valor_dollar = $value['valor_dollar'];
                $Detalle->valor_euro = $value['valor_euro'];
                $Detalle->valor_petro = $value['valor_petro'];
                $Detalle->orden = $value['orden'];
                $Detalle->vuelo_id = $Vuelo->id;

                $Detalle->save();
            }


            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            $result['data'] = null;
            return $result;
        }
    }

    public function edit_vuelo($id, Request $request) {
        if (\Request::ajax()) {
            //$Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
            if ($request->isMethod('get')) {
                $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($id));

                if ($Vuelo->hora_salida != null) {
                    $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_salida);
                } else {
                    if ($Vuelo->hora_llegada != null) {
                        //dd($Vuelo->hora_llegada);
                        $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_llegada);
                    } else {
                        $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->fecha_operacion . '00:00');
                    }
                }


                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $Vuelo->tipo_vuelo_id; // NAcional
                $data['aeropuerto_id'] = $Vuelo->aeropuerto_id;

                $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                $data['aeronave_id'] = $Vuelo->aeronave_id;

                $data['cant_pasajeros'] = $Vuelo->pasajeros_embarcados;

                if ($Vuelo->hora_llegada != null) {
                    $data['hora_llegada'] = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_llegada)->format("Y-m-d H:i");
                } else {
                    //$data['hora_llegada'] = null;
                }

                if ($Vuelo->hora_salida != null) {
                    $data['hora_salida'] = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_salida)->format("Y-m-d H:i");
                } else {
                    //$data['hora_salida'] = null;
                }




                $data['exento_dosa'] = false;
                $data['exento_tasa'] = false;

                $prodserv_extra = $this->getProdservicios2('extra', $data);
                //dd($prodserv_extra);
                return view('recaudacion.edit', compact('Vuelo', 'prodserv_extra'));
            } else {
                //dd($request->all());
                $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($id));

                $Vuelo->tipo_vuelo_id = $request->nacionalidad_id;
                $Vuelo->fecha_operacion = $this->saveDate($request->fecha_operacion);
                $Vuelo->pasajeros_desembarcados = $request->pax_desembarcados;
                $Vuelo->pasajeros_embarcados = $request->pax_embarcados;

                $Vuelo->hora_llegada = $request->hora_llegada == null ? null : Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format('Y-m-d H:i');
                ;
                $Vuelo->hora_salida = $request->hora_salida == null ? null : Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format('Y-m-d H:i');

                $Vuelo->cobrar_tasa = $request->cobrar_tasa;
                $Vuelo->cobrar_dosa = $request->cobrar_dosa;

                $Vuelo->observaciones = $request->observacion == null ? '' : $request->observacion;
                //$Vuelo->prodservicios_extra = json_encode($prod_servd);

                $Vuelo->user_id = Auth::user()->id;
                $Vuelo->fecha_registro = now();
                $Vuelo->ip = $this->getIp();

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional

                $data['aeropuerto_id'] = $Vuelo->aeropuerto_id;

                if ($request->cobrar_tasa == true) {
                    $hora_operacion = explode(' ', $request->hora_salida);
                    $data['hora_vuelo'] = $hora_operacion[1];
                } else {
                    if ($request->cobrar_dosa == true) {
                        $hora_operacion = explode(' ', $request->hora_llegada);
                        $data['hora_vuelo'] = $hora_operacion[1];
                    }
                }

                if ($request->hora_llegada != null) {
                    $data['hora_llegada'] = saveDateTime($request->hora_llegada);
                }

                if ($request->hora_salida != null) {
                    $data['hora_salida'] = saveDateTime($request->hora_salida);
                }

                $data['aeronave_id'] = $Vuelo->aeronave_id;

                $data['cant_pasajeros'] = $request->pax_embarcados;
                $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion) . " 00:00:00";
                $prodservicios_extra = [];
                if ($request->item == null) {
                    $data['serv_extra'] = [];
                } else {
                    $data['serv_extra']['ids'] = [];

                    foreach ($request->item as $key => $value) {
                        //[{"1":29},{"1":35}]
                        $prodservicios_extra[] = [$value['cant'] => \App\Helpers\Encryptor::decrypt($key)];

                        $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($key);
                        $data['serv_extra']['cant'][$key] = $value['cant'];
                    }
                }
                //dd($data);
                $Vuelo->prodservicios_extra = json_encode($prodservicios_extra);
                $Vuelo->save();
                //$data['serv_extra'] = $request->prod_servd==null ? []:$request->prod_servd;
                //$data['cobrar_dosa'] = $request->cobrar_dosa;
                //$data['cobrar_tasa'] = $request->cobrar_tasa;

                $data['exento_dosa'] = !$request->cobrar_dosa;
                $data['exento_tasa'] = !$request->cobrar_tasa;
                //dd($data);
                $servicios_facturar = $this->getProdServ($data);

                $Detalle = \App\Models\VuelosDetalle::where("vuelo_id", $Vuelo->id)->delete();
                foreach ($servicios_facturar['data'] as $value) {
                    //dd($value);
                    $Detalle = new \App\Models\VuelosDetalle();

                    $Detalle->aplica_categoria = $value['aplica_categoria'];
                    $Detalle->aplica_estacion = $value['aplica_estacion'];
                    $Detalle->aplica_peso = $value['aplica_peso'];
                    $Detalle->aplicable = $value['aplicable'];
                    $Detalle->bs = $value['bs'];
                    $Detalle->cant = $value['cant'];
                    $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                    $Detalle->codigo = $value['codigo'];
                    $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                    $Detalle->default_carga = $value['default_carga'];
                    $Detalle->default_dosa = $value['default_dosa'];
                    $Detalle->default_tasa = $value['default_tasa'];
                    $Detalle->descripcion = $value['descripcion'];
                    $Detalle->formula = $value['formula'];
                    $Detalle->formula2 = $value['formula2'];
                    $Detalle->full_descripcion = $value['full_descripcion'];
                    $Detalle->iva = $value['iva'];
                    $Detalle->iva2 = $value['iva2'];
                    $Detalle->iva_aplicado = $value['iva_aplicado'];
                    $Detalle->moneda = $value['moneda'];
                    $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                    $Detalle->nomenclatura = $value['nomenclatura'];
                    $Detalle->peso_inicio = $value['peso_inicio'];
                    $Detalle->peso_fin = $value['peso_fin'];
                    $Detalle->precio = $value['precio'];
                    $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                    $Detalle->tasa = $value['tasa'];
                    $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                    $Detalle->turno_id = $value['turno_id'];
                    $Detalle->valor_dollar = $value['valor_dollar'];
                    $Detalle->valor_euro = $value['valor_euro'];
                    $Detalle->valor_petro = $value['valor_petro'];
                    $Detalle->orden = $value['orden'];
                    $Detalle->vuelo_id = $Vuelo->id;

                    $Detalle->save();
                }

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Actualizado Correctamente";

                return $result;
            }
        }
    }

    public function exonera_vuelo($id) {
        if (\Request::ajax()) {

            $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($id));
            $Vuelo->exonerado = true;
            $Vuelo->save();

            $VueloDetalle = \App\Models\VuelosDetalle::where("vuelo_id", Encryptor::decrypt($id));
            $VueloDetalle->delete();

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            $result['data'] = null;
            return $result;
        }
    }

    public function proforma_personalizada(Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("pertenece_baer", true)
                        ->where("activo", true)
                        ->whereIn("id", Auth::user()->lista_aeropuertos)
                        ->get()
                        ->pluck("full_nombre", "crypt_id");

                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre_tn", "crypt_id");
                return view('recaudacion.personalizada', compact('Aeropuertos', 'Aeronaves'));
            } else {
                if (\Request::isMethod('post')) {
                    $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                        $Cliente->tipo_documento = $request->type_document;
                        $Cliente->documento = $request->document;

                        $Cliente->user_id = Auth::user()->id;
                        $Cliente->ip = $this->getIp();
                        $Cliente->fecha_registro = now();
                    }

                    $Cliente->razon_social = Upper($request->razon);
                    $Cliente->telefono = $request->phone;
                    $Cliente->correo = $request->correo;
                    $Cliente->direccion = $request->direccion;
                    $Cliente->tipo_id = 4; // general

                    $Cliente->save();

                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                    $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                    $data['cant_pasajeros'] = $request->pax;
                    if (isset($request->aeropuerto_id)) {
                        $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                    } else {
                        $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                    }
                    $data['hora_llegada_est'] = saveDateTime($request->hora_llegada_est);
                    $data['hora_salida_est'] = saveDateTime($request->hora_salida_est);
                    /*
                    if ($data['hora_salida_est'] <= $data['hora_llegada_est']) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = "La hora de salida debe ser posterior a la hora de llegada.";
                        $result['data'] = '';

                        return $result;
                    }
                    */
                    $servicios_llegada['data'] = [];
                    if ($request->llegada != null) {
                        if ($request->carga_ll != null) {
                            $data['carga'] = saveFloat($request->carga_ll);
                        }
                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                        $data['hora_vuelo'] = $request->hora_llegada;

                        $data['serv_extra'] = [];
                        if ($request->in != null) {
                            $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_llegada . ' ' . $request->hora_llegada);
                            $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                            $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                            $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                            $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                            $prodserv_extra = $this->getProdservicios2('extra', $data);
                            $data['serv_extra']['ids'] = [];

                            foreach ($prodserv_extra as $value) {
                                foreach ($request->in as $key => $value2) {
                                    if (strpos($value['codigo'], $key) === 0) {

                                        $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                                        $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                                    }
                                }
                            }
                        }


                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                        $data['hora_vuelo'] = $request->hora_llegada;

                        $data['hora_llegada'] = saveDateTime($request->fecha_llegada . ' ' . $request->hora_llegada);
                        $data['exento_dosa'] = false;
                        $data['exento_tasa'] = true;

                        $servicios_llegada = $this->getProdServ($data);
                        $list = [];
                        foreach ($servicios_llegada['data'] as $key => $value) {

                            if (in_array($value['codigo'], $list)) {
                                unset($servicios_llegada['data'][$key]);
                            } else {
                                $list[] = $value['codigo'];
                            }
                        }
                    }

                    $servicios_salida['data'] = [];
                    if ($request->salida != null) {
                        if ($request->carga_ss != null) {
                            $data['carga'] = saveFloat($request->carga_ss);
                        }
                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                        $data['hora_vuelo'] = $request->hora_salida;

                        $data['serv_extra'] = [];
                        if ($request->ou != null) {
                            $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_salida . ' ' . $request->hora_salida);
                            $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                            $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                            $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                            $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                            $prodserv_extra = $this->getProdservicios2('extra', $data);
                            $data['serv_extra']['ids'] = [];

                            foreach ($prodserv_extra as $value) {
                                foreach ($request->ou as $key => $value2) {
                                    if (strpos($value['codigo'], $key) === 0) {

                                        $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                                        $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                                    }
                                }
                            }
                        }


                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                        $data['hora_vuelo'] = $request->hora_salida;




                        $data['hora_salida'] = saveDateTime($request->fecha_salida . ' ' . $request->hora_salida);
                        $data['exento_dosa'] = true;
                        $data['exento_tasa'] = false;

                        $carga = $request->carga == 0 ? $data['carga'] = 0 : $carga['carga'] = saveFloat($request->carga);


                        $servicios_salida = $this->getProdServ($data);
                        $list = [];
                        foreach ($servicios_salida['data'] as $key => $value) {

                            if (in_array($value['codigo'], $list)) {
                                unset($servicios_salida['data'][$key]);
                            } else {
                                $list[] = $value['codigo'];
                            }
                        }
                    }

                    if ($request->llegada != null && $request->salida != null) {
                        $fecha_llegada = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_llegada . ' ' . $request->hora_llegada);
                        $fecha_salida = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_salida . ' ' . $request->hora_salida);
                        /*if ($fecha_salida <= $fecha_llegada) {
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = "La hora de salida debe ser posterior a la hora de llegada.";
                            $result['data'] = '';
        
                            return $result; 
                        }*/
                    }


                    $prodServs = [];
                    if (count($servicios_llegada['data']) > 0) {
                        foreach ($servicios_llegada['data'] as $key1=>$value) {
                            $prodServs[$key1] = $value;
                            foreach ($servicios_salida['data'] as $key => $value2) {
                                if ($value['codigo'] == $value2['codigo'] && $value['codigo']!= '2.2.1') {
                                    $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                                    $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                                    unset($servicios_salida['data'][$key]);
                                }
                            }
                        }
                        foreach ($servicios_salida['data'] as $key => $value2) {
                            $prodServs[] = $value2;
                        }
                    } else {
                        if (count($servicios_salida['data']) > 0) {
                            foreach ($servicios_salida['data'] as $key1=>$value) {
                                $prodServs[$key1] = $value;
                                foreach ($servicios_llegada['data'] as $value2) {
                                    if ($value['codigo'] == $value2['codigo'] && $value['codigo']!= '2.2.1') {
                                        $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                                        $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                                        unset($servicios_llegada['data'][$key]);
                                    }
                                }
                            }
                            foreach ($servicios_llegada['data'] as $key => $value2) {
                                $prodServs[] = $value2;
                            }
                        }
                    }

                    $existentes = [];
                    foreach($prodServs as $key=>$value){
                        if ( in_array($value['codigo'], $existentes ) ){
                            unset($prodServs[$key]);
                        }
                        $existentes[]=$value['codigo'];
                    }




                    $Proforma = new \App\Models\Proforma();
                    $Proforma->fecha_proforma = now();

                    $Proforma->cliente_id = $Cliente->id;
                    $Proforma->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                    $Proforma->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                    $Proforma->nro_proforma = 0;
                    $Proforma->tasa_dolar = $this->DOLAR;
                    $Proforma->tasa_euro = $this->EURO;
                    $Proforma->tasa_petro = $this->PETRO;
                    $Proforma->formato_proforma = "reportes.proforma.first";

                    $Proforma->user_id = Auth::user()->id;
                    $Proforma->fecha_registro = now();
                    $Proforma->ip = $this->getIp();
                    $Proforma->tipo_proforma = 3;
                    $Proforma->temp_alt = $request->temp_alt;
                    $Proforma->tipo_vuelo_id = $request->nacionalidad_id;
                    $Proforma->observacion = $request->observacion;
                    $Proforma->save();

                    $Proforma->nro_proforma = $Proforma->id;
                    $Proforma->save();

                    foreach ($prodServs as $key=>$value) {
                        $insertar = true;

                        if (isset($request->prod_servd_remove)) {
                            foreach ($request->prod_servd_remove as $value2) {
                                if ($value2==$value['codigo']) {
                                    $insertar = false;
                                    break;
                                }
                            }
                        }
                        if ($insertar == true) {
                            $ProformaDetalle = new \App\Models\ProformaDetalle();

                            $ProformaDetalle->proforma_id = $Proforma->id;
                            $ProformaDetalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                            $ProformaDetalle->cantidad = $value['cant'];
                            $ProformaDetalle->codigo = $value['codigo'];
                            $ProformaDetalle->tipo_id = 1;
                            $ProformaDetalle->descripcion = $value['descripcion'] . ' ' . $value['iva2'];
                            $ProformaDetalle->nomenclatura = $value['nomenclatura'];
                            $ProformaDetalle->precio = $value['precio'];
                            $ProformaDetalle->iva = $value['iva_aplicado'];
                            $ProformaDetalle->save();
                        }
                    }

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Registrado correctamente";
                    $result['data'] = $Proforma;

                    return $result;
                } else {
                    dd('ss');
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function proforma_personalizada_avc(Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {

                $services[]=['codigo'=>'1.3.1','servicio'=>"Uso programado por itinerario de vuelo (Por hora o fracción de hora.)"];
                $services[]=['codigo'=>'1.3.2', 'servicio'=>"Demora sobre itinerrio de vuelo por secuencia de equipos (cuarenta y circo (45) minutos despúes de la hora de salida.)"];

                $services[]=['codigo'=>'1.6.1','servicio'=>"Hasta 80.000 Kg. (Por peso máximo de despegue de la aeronave)"];
                $services[]=['codigo'=>'1.6.2','servicio'=>"De 80.001 kg. a 160.000 kg."];
                $services[]=['codigo'=>'1.6.3','servicio'=>"De 160.001 kg. en adelante."];
                $services[]=['codigo'=>'1.15.1','servicio'=>"Vuelos nacionales (Por hora o fracción, después de transcurridos 45 minutos)"];
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("pertenece_baer", true)
                        ->where("activo", true)
                        ->whereIn("id", Auth::user()->lista_aeropuertos)
                        ->get()
                        ->pluck("full_nombre", "crypt_id");

                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre_tn", "crypt_id");
                $Cliente  = \App\Models\Clientes::where("activo", 1)->orderBy("documento")->get()->pluck("full_razon_social", "crypt_id");
                return view('facturacion_aviacion_comercial.index', compact('Aeropuertos', 'Aeronaves','Cliente','services'));
            } else {
                if (\Request::isMethod('post'))
                {

                }else {

                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function proforma_operacion_detalle($llegada, $salida) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Llegada = \App\Models\Vuelos::find(Encryptor::decrypt($llegada));
                $Salida = \App\Models\Vuelos::find(Encryptor::decrypt($salida));

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = array("llegada" => $Llegada, "salida" => $Salida);
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function proforma_operacion_lista(Request $request) {
        if (\Request::ajax()) {
            if ($request->aeropuerto != null) {

                $where [] = [function ($q) {
                        $q->whereNull('proforma_id');
                        $q->where('activo', 1);
                        $q->where('aprobado', 1);
                    }];
                //dd($request->all());
                if (\Request::all()['aeropuerto'] != null) {
                    $params = Encryptor::decrypt(\Request::all()['aeropuerto']);
                    $where [] = ["aeropuerto_id", $params];
                }
                if (\Request::all()['nave'] != null) {
                    $params = Encryptor::decrypt(\Request::all()['nave']);
                    $where [] = ["aeronave_id", $params];
                }
                if (\Request::all()['rango'] != null) {
                    $params = \Request::all()['rango'];
                    $where [] = [function ($q) use ($params) {
                            $fechas = explode(" - ", $params);
                            $f1[0] = $this->saveDate($fechas[0]);
                            $f1[1] = $this->saveDate($fechas[1]);
                            $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                        }];
                }
            } else {
                $where [] = [DB::raw("1"), "2"];
            }

            if (Auth::user()->aeropuerto_id != null) {
                $datos = Vuelos::whereIn('aeropuerto_id', Auth::user()->lista_aeropuertos)
                        ->where($where)
                        ->limit(100)
                        ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||COALESCE(hora_operacion, '00:00')||':00') AS timestamp) "), "asc")
                        ->get();
            } else {
                $datos = Vuelos::where($where)
                        ->limit(100)
                        ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||COALESCE(hora_operacion, '00:00')||':00') AS timestamp) "), "asc")
                        ->get();
            }


            $data_add = [];
            $operacion = [];
            foreach ($datos as $key => $value) {
                if (!in_array($value->id, $data_add)) {
                    $data_add[] = $value->id;

                    $where = [];
                    $where [] = ['aeropuerto_id', $value->aeropuerto_id];
                    $where [] = ['aeronave_id', $value->aeronave_id];
                    $where [] = ['activo', true];
                    $where [] = ['aprobado', true];

                    $where [] = ['aeronave_id', $value->aeronave_id];

                    if ($value->tipo_operacion_id == 1) { //si es llegada
                        $where [] = ['tipo_operacion_id', 2];
                        $where[] = [
                            function ($q) use ($value) {
                                $q->where(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), '>', $value->fecha_operacion . ' ' . $value->hora_operacion);
                            }
                        ];
                        $data_ext = Vuelos::where($where)
                                ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), "asc")
                                ->first();
                        if ($data_ext != null) {
                            $operacion[$key]['responsable'] = $data_ext->observaciones_facturacion2[0] . $data_ext->observaciones_facturacion2[1] . ' ' . $data_ext->observaciones_facturacion2[2];
                            $operacion[$key]['aeronave'] = $value->aeronave->full_nombre_tn;

                            $operacion[$key]['button'] = '<button type="button" onClick="getDetail(\'' . $value->crypt_id . '\', \'' . $data_ext->crypt_id . '\', \''. $value->aeronave->crypt_id .'\')" class="btn btn-info btn-xs"><li class="fa fa-eye"></li></button>';

                            $data_add[] = $data_ext->id;
                            if ($value->tipo_vuelo_id == $data_ext->tipo_vuelo_id) {
                                $operacion[$key]['tipo'] = $value->tipo_vuelo;
                            } else {
                                $operacion[$key]['tipo'] = $value->tipo_vuelo . '/' . $data_ext->tipo_vuelo;
                            }

                            $operacion[$key]['fecha_llegada'] = $value->fecha_operacion2;
                            $operacion[$key]['hora_llegada'] = $value->hora_operacion;

                            $operacion[$key]['fecha_salida'] = $data_ext->fecha_operacion2;
                            $operacion[$key]['hora_salida'] = $data_ext->hora_operacion;

                            $operacion[$key]['pax'] = $data_ext->pasajeros_embarcados;

                            $operacion[$key]['obs'] = $value->observaciones . ' ' . $data_ext->observaciones;
                        } else {
                            unset($operacion[$key]['responsable']);
                        }
                    } else {
                        $where [] = ['tipo_operacion_id', 1];
                        $where[] = [
                            function ($q) use ($value) {
                                $q->where(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), '<', $value->fecha_operacion . ' ' . $value->hora_operacion);
                            }
                        ];
                        $data_ext = Vuelos::where($where)
                                ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||hora_operacion||':00') AS timestamp) "), "desc")
                                ->first();

                        //dd($data_ext->tipo_vuelo);

                        if ($data_ext != false) {
                            $operacion[$key]['aeronave'] = $value->aeronave->full_nombre_tn;
                            $operacion[$key]['button'] = '<button type="button" onClick="getDetail(\'' . $data_ext->crypt_id . '\', \'' . $value->crypt_id . '\')" class="btn btn-info btn-xs"><li class="fa fa-eye"></li></button>';
                            $data_add[] = $data_ext->id;
                            if ($value->tipo_vuelo_id == $data_ext->tipo_vuelo_id) {
                                $operacion[$key]['tipo'] = $value->tipo_vuelo;
                            } else {
                                $operacion[$key]['tipo'] = $value->tipo_vuelo . '/' . $data_ext->tipo_vuelo;
                            }
                            //$operacion[$key]['fecha_operacion2'] = $value->fecha_operacion2;
                            if ($value->observaciones_facturacion2[0] == "") {
                                $operacion[$key]['responsable'] = $data_ext->observaciones_facturacion2[0] . $data_ext->observaciones_facturacion2[1] . ' ' . $data_ext->observaciones_facturacion2[2];
                            } else {

                                $operacion[$key]['responsable'] = $value->observaciones_facturacion2[0] . $value->observaciones_facturacion2[1] . ' ' . $value->observaciones_facturacion2[2];
                            }

                            $operacion[$key]['fecha_llegada'] = $data_ext->fecha_operacion2;
                            $operacion[$key]['hora_llegada'] = $data_ext->hora_operacion;

                            $operacion[$key]['fecha_salida'] = $value->fecha_operacion2;
                            $operacion[$key]['hora_salida'] = $value->hora_operacion;

                            $operacion[$key]['pax'] = $value->pasajeros_embarcados;

                            $operacion[$key]['obs'] = $value->observaciones . ' ' . $data_ext->observaciones;
                        } else {
                            //$operacion[$key]['responsable'] = $value->observaciones_facturacion2[0] . $value->observaciones_facturacion2[1] . ' ' . $value->observaciones_facturacion2[2];
                            unset($operacion[$key]['responsable']);
                        }
                    }




                    //$operacion[$key]['fecha_operacion2'] = $value->fecha_operacion2;
                }
            }


            $data = Datatables::of($operacion)
                    ->addIndexColumn()
                    ->rawColumns(['button'])
            ;
            return $data->toJson();
        } else {
            return Redirect::to('/dashboard');
        }
    }

    function proforma_operacion(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre_tn", "crypt_id");
                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }

                return view('recaudacion.operacion', compact('Aeronaves', 'Aeropuertos'));
            } else {
                // dd($request->all());

                $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                if ($Cliente == null) {
                    $Cliente = new \App\Models\Clientes();
                    $Cliente->tipo_documento = $request->type_document;
                    $Cliente->documento = $request->document;

                    $Cliente->user_id = Auth::user()->id;
                    $Cliente->ip = $this->getIp();
                    $Cliente->fecha_registro = now();
                }

                $Cliente->razon_social = Upper($request->razon);
                $Cliente->telefono = $request->phone;
                $Cliente->correo = $request->correo;
                $Cliente->direccion = $request->direccion;
                $Cliente->tipo_id = 4; // general

                $Cliente->save();
                // dd($Cliente->id);

                $Vuelo = Vuelos::find(Encryptor::decrypt($request->vuelos[0]));

                $Proforma = new \App\Models\Proforma();
                $Proforma->fecha_proforma = now();
                $Proforma->cliente_id = $Cliente->id;
                $Proforma->aeronave_id = $Vuelo->aeronave_id;
                $Proforma->aeropuerto_id = $Vuelo->aeropuerto_id;
                $Proforma->nro_proforma = 0;
                $Proforma->tipo_vuelo_id = $request->nacionalidad_id;
                $Proforma->tasa_dolar = $this->DOLAR;
                $Proforma->tasa_euro = $this->EURO;
                $Proforma->tasa_petro = $this->PETRO;
                $Proforma->formato_proforma = "reportes.proforma.first";
                $Proforma->tipo_proforma = 2;

                $Proforma->user_id = Auth::user()->id;
                $Proforma->fecha_registro = now();
                $Proforma->ip = $this->getIp();

                $Proforma->save();

                $Proforma->nro_proforma = $Proforma->id;
                $Proforma->save();

                $Vuelos = [];
                foreach ($request->vuelos as $value) {
                    $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($value));
                    $Vuelo->proforma_id = $Proforma->id;
                    $Vuelo->save();

                    foreach ($Vuelo->getDetalle as $value2) {
                        $ProformaDetalle = new \App\Models\ProformaDetalle();

                        $ProformaDetalle->proforma_id = $Proforma->id;
                        $ProformaDetalle->prodservicio_id = $value2->prodservicio_id;
                        $ProformaDetalle->cantidad = $value2->cant;
                        $ProformaDetalle->codigo = $value2->codigo;
                        $ProformaDetalle->tipo_id = 1;
                        $ProformaDetalle->descripcion = $value2->descripcion . ' ' . $value2->iva2;
                        $ProformaDetalle->nomenclatura = $value2->nomenclatura;
                        $ProformaDetalle->precio = $value2->precio;
                        $ProformaDetalle->iva = $value2->iva_aplicado;
                        $ProformaDetalle->save();
                    }
                }
                $result['status'] = 1;
                $result['data'] = $Proforma->toArray();
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                return $result;

                ////dd($Vuelos[0]->getDetalle);
                //return view('reportes.proforma.first', compact('Proforma'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function facturar_pospago(Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('get')) {

                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre_tn", "crypt_id");

                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }

                return view('recaudacion.pospago', compact('Aeronaves', 'Aeropuertos'));
            } else {
                if ($request->isMethod('put')) {
                    //dd($request->all());
                    $fechas = explode(" - ", $request->rango);
                    $fecha_desde = $this->saveDate($fechas[0]);
                    $fecha_hasta = $this->saveDate($fechas[1]);

                    $where = [];
                    $where[] = ["activo", true];
                    $where[] = ["aprobado", true];
                    if ($request->aeronave_id != null) {
                        $where[] = ["aeronave_id", Encryptor::decrypt($request->aeronave_id)];
                    }
                    $Vuelos = Vuelos::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                            ->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$fecha_desde, $fecha_hasta])
                            ->where($where)
                            //->whereNull("factura_id")
                            //->whereNull("proforma_id")
                            //->where("exonerado", false)
                            ->orderBy('aeronave_id')
                            ->orderBy(DB::raw("CAST(CONCAT(fecha_operacion||' '||COALESCE(hora_operacion, '00:00')||':00') AS timestamp) "), "asc")
                            ->get(); //dd($fecha_hasta);
                    //  dd($Vuelos->toArray());exit;
                    $data = Datatables::of($Vuelos)
                            ->addColumn('editable', function ($row) {
                                if ($row->factura_id == null && $row->proforma_id == null && $row->exonerado == null) {
                                    return true;
                                } else {
                                    return false;
                                }
                            })
                            ->addColumn('action', function ($row) {
                                $actionBtn = '';
                                $actionBtn = '<a onClick="seeDetalleVuelo(\'' . route('view_detail_fly', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Ver Servicios Aplicados" class=" btn btn-warning btn-xs mt-2"> <li class="fa fa-eye"></li> </a> ';

                                if ($row->factura_id == null && $row->proforma_id == null && $row->exonerado == null) {
                                    //$actionBtn = '<a onClick="seeDetalleVuelo(\'' . route('view_detail_fly', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Ver Servicios Aplicados" class=" btn btn-warning btn-xs mt-2"> <li class="fa fa-eye"></li> </a> ';
                                    //$actionBtn .= '<a onClick="editVuelo(\'' . route('facturar_pospago.edit_vuelo', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Editar el Vuelo" class=" btn btn-success btn-sm"> <li class="fa fa-edit"></li> </a> ';
                                    //$actionBtn .= '<a onClick="exonerarVuelo(\'' . route('facturar_pospago.exonera_vuelo', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Exonerar Vuelo" class=" btn btn-danger btn-sm mt-2"> <li class="fa fa-pause"></li> </a> ';
                                    $actionBtn .= '<a onClick="seeNota(\'' . $row->observaciones_facturacion . '\')" href="javascript:void(0)" title="Responsable de Facturación" data-toggle="tooltip" class=" btn btn-info btn-xs mt-2"> <li class="fa fa-sticky-note"></li> </a> ';
                                } else {

                                }

                                return $actionBtn;
                            })
                            /*
                              ->filter(function ($instance) {
                              $instance->collection = $instance->collection->filter(function ($row) {
                              dd($row['total']);
                              return $row['total'] > 0;
                              });
                              })
                             */
                            ->addIndexColumn()
                            ->rawColumns(['action'])
                            ->make(true);

                    $data = $data->original['data'];
                    if (count($data) > 0) {
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                    } else {
                        $result['type'] = 'warning';
                        $result['message'] = __('No se Encontraron Datos');
                    }
                    $result['status'] = 1;
                    $result['data'] = $data;

                    return $result;
                } else {
                    if ($request->isMethod('post')) {
                        //dd($request->all());

                        $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                        if ($Cliente == null) {
                            $Cliente = new \App\Models\Clientes();
                            $Cliente->tipo_documento = $request->type_document;
                            $Cliente->documento = $request->document;

                            $Cliente->user_id = Auth::user()->id;
                            $Cliente->ip = $this->getIp();
                            $Cliente->fecha_registro = now();
                        }

                        $Cliente->razon_social = Upper($request->razon);
                        $Cliente->telefono = $request->phone;
                        $Cliente->correo = $request->correo;
                        $Cliente->direccion = $request->direccion;
                        $Cliente->tipo_id = 4; // general

                        $Cliente->save();

                        $Vuelo = Vuelos::find(Encryptor::decrypt($request->vuelos[0]));

                        $Proforma = new \App\Models\Proforma();
                        $Proforma->fecha_proforma = now();
                        $Proforma->cliente_id = $Cliente->id;
                        $Proforma->aeronave_id = $Vuelo->aeronave_id;
                        $Proforma->aeropuerto_id = $Vuelo->aeropuerto_id;
                        $Proforma->nro_proforma = 0;
                        $Proforma->tasa_dolar = $this->DOLAR;
                        $Proforma->tasa_euro = $this->EURO;
                        $Proforma->tasa_petro = $this->PETRO;
                        $Proforma->formato_proforma = "reportes.proforma.first";

                        $Proforma->user_id = Auth::user()->id;
                        $Proforma->fecha_registro = now();
                        $Proforma->ip = $this->getIp();

                        $Proforma->save();

                        $Proforma->nro_proforma = $Proforma->id;
                        $Proforma->save();

                        $Vuelos = [];
                        foreach ($request->vuelos as $value) {
                            $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($value));
                            $Vuelo->proforma_id = $Proforma->id;
                            $Vuelo->save();

                            foreach ($Vuelo->getDetalle as $value2) {
                                $ProformaDetalle = new \App\Models\ProformaDetalle();

                                $ProformaDetalle->proforma_id = $Proforma->id;
                                $ProformaDetalle->prodservicio_id = $value2->prodservicio_id;
                                $ProformaDetalle->cantidad = $value2->cant;
                                $ProformaDetalle->codigo = $value2->codigo;
                                $ProformaDetalle->tipo_id = 1;
                                $ProformaDetalle->descripcion = $value2->descripcion . ' ' . $value2->iva2;
                                $ProformaDetalle->nomenclatura = $value2->nomenclatura;
                                $ProformaDetalle->precio = $value2->precio;
                                $ProformaDetalle->iva = $value2->iva_aplicado;
                                $ProformaDetalle->save();
                            }
                        }

                        $result['status'] = 1;
                        $result['data'] = $Proforma->toArray();
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        return $result;

                        ////dd($Vuelos[0]->getDetalle);
                        //return view('reportes.proforma.first', compact('Proforma'));
                    } else {
                        exit;
                    }
                }
            }
        } else {
            //    else {
            return Redirect::to('/dashboard');
            // }
        }
    }

    public function send_prof_email($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Proforma = \App\Models\Proforma::find(Encryptor::decrypt($id));

                $hideBotonPrint = true;
                $proforma = PDF::loadView("reportes.proforma.first", compact('Proforma', 'hideBotonPrint'));

                $emails = json_decode($request->emails_to_send);

                //dd($emails);
                $subject = "Proforma de Vuelo";
                $for = $emails;
                $datos = $Proforma->toArray();
                //dd($datos);
                Mail::send('email.send_proforma', $datos, function ($msj) use ($subject, $for, $proforma, $Proforma) {

                    $msj->subject($subject);
                    $msj->to($for);
                    $msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                });
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Correos Enviados Correctamente";
                $result['data'] = "";

                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function send_resumen_email(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id))->toArray();
                $query = \App\Models\Proforma::where('anulada', false)->orderBy('id', 'asc');
                if ($request->estatus_id != '') {

                    $estatus = $request->estatus_id == 1 ? $estatus = true : $estatus = false;

                    $query->where('pagado', $estatus);
                }
                if ($request->cliente_id != '') {
                    $query->where('cliente_id', Encryptor::decrypt($request->cliente_id));
                }
                if ($request->rango_fecha != ''){
                    $rango_fecha = $request->rango_fecha;
                    $rango_fecha = explode(' - ', $rango_fecha);

                    $fecha_inicio = $rango_fecha[0];
                    $fecha_fin = $rango_fecha[1];

                    $query->whereBetween('fecha_proforma', [$fecha_inicio, $fecha_fin]);
                }
                $datos = [
                    'cliente' => $cliente,
                    'rango_fecha' => $request->rango_fecha,
                ];
                $proforma = $query->get();



                $pdf_proforma = PDF::loadView("reportes.recaudacion.first", compact('proforma', 'cliente'));

                $emails = json_decode($request->emails_to_send);

                //dd($emails);
                $subject = "Resumen de Proformas";
                $for = $emails;

                Mail::send('email.send_resumen_proforma', $datos, function ($msj) use ($subject, $for, $pdf_proforma) {

                    $msj->subject($subject);
                    $msj->to($for);
                    $msj->attachData($pdf_proforma->output(), "RESUMEN.pdf");
                });
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Correos Enviados Correctamente";
                $result['data'] = "";

                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }


                return view('recaudacion.create', compact('Pilotos', 'Aeronaves', 'Aeropuertos'));
            } else {
                if ($request->isMethod('post')) {

                    //dd($request->all());

                    $prod_servd = [];
                    if (is_array($request->prod_servd)) {
                        foreach ($request->prod_servd as $value) {
                            $p = explode(":", $value);
                            $prod_servd[][$p[0]] = Encryptor::decrypt($p[1]);
                        }
                    }
                    $ids = [];
                    if (isset($request->vuelos_checked)) {
                        if (is_array($request->vuelos_checked)) {
                            foreach ($request->vuelos_checked as $value) {
                                $Vuelos = Vuelos::find(Encryptor::decrypt($value));
                                $ids[] = $Vuelos->id;
                                $Vuelos->procesado_recaudacion = true;
                                $Vuelos->save();
                            }
                        }
                    } else {

                        $last_operation = Vuelos::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                                ->where("activo", true)
                                ->orderBy("id", "desc")
                                ->first();
                        $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                        $Vuelos = new Vuelos();

                        $Vuelos->numero_operacion = $num_operation;
                        $Vuelos->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                        $Vuelos->tipo_operacion_id = 2; //DESPEGUE
                        $Vuelos->tipo_vuelo_id = $request->nacionalidad_id;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                        $Vuelos->hora_operacion = $request->hora_operacion;
                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);
                        //$Vuelos->origen_destino_id = Encryptor::decrypt($request->origen_destino_id);
                        $Vuelos->cant_pasajeros = $request->cant_pasajeros;
                        $Vuelos->procesado_recaudacion = true;
                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = json_encode($prod_servd);

                        $Vuelos->user_id = Auth::user()->id;
                        $Vuelos->fecha_registro = now();
                        $Vuelos->ip = $this->getIp();
                        ;

                        $Vuelos->save();
                        $ids[] = $Vuelos->id;
                    }

                    //if ($request->tipo_operacion_id == 2) {//SI ES UN DESPEGUE LIBERO EL PUESTO
                    $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelos->aeronave_id)->update(["aeronave_id" => null]);
                    //}

                    $servicios_facturar = json_decode($request->servicios_facturar);
                    foreach ($servicios_facturar as $value) {
                        //var_dump($value);
                        $Detalle = new \App\Models\VuelosDetalle();

                        $Detalle->aplica_categoria = $value->aplica_categoria;
                        $Detalle->aplica_estacion = $value->aplica_estacion;
                        $Detalle->aplica_peso = $value->aplica_peso;
                        $Detalle->aplicable = $value->aplicable;
                        $Detalle->bs = $value->bs;
                        $Detalle->cant = $value->cant;
                        $Detalle->categoria_aplicada = $value->categoria_aplicada;
                        $Detalle->codigo = $value->codigo;
                        $Detalle->prodservicio_id = Encryptor::decrypt($value->crypt_id);
                        $Detalle->default_carga = $value->default_carga;
                        $Detalle->default_dosa = $value->default_dosa;
                        $Detalle->default_tasa = $value->default_tasa;
                        $Detalle->descripcion = $value->descripcion;
                        $Detalle->formula = $value->formula;
                        $Detalle->formula2 = $value->formula2;
                        $Detalle->full_descripcion = $value->full_descripcion;
                        $Detalle->iva = $value->iva;
                        $Detalle->iva2 = $value->iva2;
                        $Detalle->iva_aplicado = $value->iva_aplicado;
                        $Detalle->moneda = $value->moneda;
                        $Detalle->nacionalidad_id = $value->nacionalidad_id;
                        $Detalle->nomenclatura = $value->nomenclatura;
                        $Detalle->peso_inicio = $value->peso_inicio;
                        $Detalle->peso_fin = $value->peso_fin;
                        $Detalle->precio = $value->precio;
                        $Detalle->prodservicio_extra = $value->prodservicio_extra;
                        $Detalle->tasa = $value->tasa;
                        $Detalle->tipo_vuelo_id = $value->tipo_vuelo_id;
                        $Detalle->turno_id = $value->turno_id;
                        $Detalle->valor_dollar = $value->valor_dollar;
                        $Detalle->valor_euro = $value->valor_euro;
                        $Detalle->valor_petro = $value->valor_petro;
                        $Detalle->orden = $value->orden;
                        $Detalle->vuelo_id = $Vuelos->id;

                        $Detalle->save();
                    }
                    $factura_id = null;

                    if ($request->op == 'facturar') {

                        $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                        if ($Cliente == null) {
                            $Cliente = new \App\Models\Clientes();
                            $Cliente->tipo_documento = $request->type_document;
                            $Cliente->documento = $request->document;

                            $Cliente->user_id = Auth::user()->id;
                            $Cliente->ip = $this->getIp();
                            $Cliente->fecha_registro = now();
                        }

                        $Cliente->razon_social = Upper($request->razon);
                        $Cliente->telefono = $request->phone;
                        $Cliente->correo = $request->correo;
                        $Cliente->direccion = $request->direccion;
                        $Cliente->tipo_id = 4; // general

                        $Cliente->save();

                        $TAQUILLA = $this->getDataTaquilla(Encryptor::decrypt($request->aeropuerto_id));
                        //dd($TAQUILLA);
                        $TAQUILLA->numero_factura = ((int) $TAQUILLA->numero_factura) + 1;
                        $TAQUILLA->numero_control = ((int) $TAQUILLA->numero_control) + 1;
                        $TAQUILLA->save();

                        $Factura = new \App\Models\Facturas();

                        $Factura->fecha_factura = now();
                        $Factura->cliente_id = $Cliente->id;
                        $Factura->nro_factura = $TAQUILLA->numero_factura;
                        $Factura->nro_documento = $TAQUILLA->numero_factura;

                        $Factura->tipo_pago_id = 1; //contado
                        $Factura->tasa_dolar = $this->DOLAR;
                        $Factura->tasa_euro = $this->EURO;
                        $Factura->tasa_petro = $this->PETRO;

                        $Factura->formato_factura = "reportes.facturas.first";
                        $Factura->observacion = Upper($request->observacion);

                        $Factura->user_id = Auth::user()->id;
                        $Factura->fecha_registro = now();
                        $Factura->ip = $this->getIp();

                        $Factura->save();

                        $ids[] = $Vuelos->id;
                        \App\Models\Vuelos::whereIn("id", $ids)->update(["factura_id" => $Factura->id]);

                        foreach ($servicios_facturar as $value) {
                            $Detalle = new \App\Models\FacturasDetalle();

                            $Detalle->cantidad = $value->cant;
                            $Detalle->factura_id = $Factura->id;
                            $Detalle->prodservicio_id = Encryptor::decrypt($value->crypt_id);
                            $Detalle->codigo = $value->codigo;
                            $Detalle->tipo_id = 0; //BUSCAR DESPUES
                            $Detalle->descripcion = $value->descripcion . ' ' . $value->iva2;
                            $Detalle->nomenclatura = $value->nomenclatura;

                            $Detalle->precio = $value->bs;
                            $Detalle->formula = $value->formula2;

                            $Detalle->iva = $value->iva_aplicado;
                            $Detalle->save();
                        }
                        $factura_id = $Factura->crypt_id;
                    }


                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Registrado Correctamente";
                    $result['data'] = $factura_id;

                    return $result;
                } else {
                    //dd($request->all());

                    if (Auth::user()->aeropuerto_id != null) {
                        $datos = Vuelos::where('aeropuerto_id', Auth::user()->aeropuerto_id)
                                ->where("aeronave_id", Encryptor::decrypt($request->aeronave_id))
                                ->where("procesado_recaudacion", false)
                                ->whereNull("factura_id")
                                ->where("activo", true)
                                ->orderBy("numero_operacion", "desc")
                                ->get();
                    } else {
                        $datos = Vuelos::where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id))
                                ->where("aeronave_id", Encryptor::decrypt($request->aeronave_id))
                                ->where("procesado_recaudacion", false)
                                ->whereNull("factura_id")
                                ->where("activo", true)
                                ->orderBy("numero_operacion", "desc")
                                ->get();
                    }

                    $data = DataTables::of($datos)
                            ->addColumn('action', function ($row) {
                                return '<input type="checkbox" class="form-control form-control-sm selectVueloFact" onClick="selectVuelo(this)" data-tipo_operacion="' . $row->tipo_operacion . '" value="' . $row->crypt_id . '">';
                                $actionBtn;
                            })
                            ->addIndexColumn()
                            ->rawColumns(['action'])
                            ->make(true);
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Procesado Correctamente";
                    $result['data'] = $data->original['data'];
                    return $result;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }
    public function down_pagos(Request $request){
        $query = \App\Models\Proforma::where('anulada', false)->orderBy('id', 'asc');

        if ($request->estatus_id != null) {

            $estatus = $request->estatus_id == 1 ? $estatus = true : $estatus = false;

            $query->where('pagado', $estatus);
        }
        if ($request->cliente_id != '') {
            $query->where('cliente_id', Encryptor::decrypt($request->cliente_id));
        }
        $cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id))->toArray();
        if ($request->rango_fecha != ''){
            $rango_fecha = $request->rango_fecha;
            $rango_fecha = explode(' - ', $rango_fecha);

            $fecha_inicio = $rango_fecha[0];
            $fecha_fin = $rango_fecha[1];

            $query->whereBetween('fecha_proforma', [$fecha_inicio, $fecha_fin]);
        }
        $query->whereIn("aeropuerto_id", Auth::user()->lista_aeropuertos);
        if ($request->aeropuerto_id != ''){
            $query->where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id));
        }
        $proforma = $query->get();

        $pdf = PDF::loadView("reportes.recaudacion.first", compact('proforma', 'cliente'));
        if ($request->correos != null) {
            $subject = "Resumen de proformas";
            $for = "lr.programming.tools@gmail.com";
            $datos = [];
            $cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id))->toArray();
            Mail::send('email.send_resumen_proforma', $cliente, function ($msj) use ($subject, $for, $pdf) {

                $msj->subject($subject);
                $msj->to($for);
                $msj->attachData($pdf->output(), "RESUMEN.pdf");
            });
        }



        return $pdf->stream('pagos.pdf');
    }

    public function ExcelProforma(){
        $aeropuerto_id = Encryptor::decrypt(request('aeropuerto_id'));
        $rango_fecha = request('rango_fecha');
        $fechas = explode(' - ', $rango_fecha);
        $fechaInicio = $fechas[0];
        $fechaFin = $fechas[1];
        return Excel::download(new ProformasExport(saveDate($fechaInicio), saveDate($fechaFin), $aeropuerto_id), 'proforma.xlsx');
    }

    public function ExcelFactura(){

        $rango_fecha = request('rango_fecha');
        $fechas = explode(' - ', $rango_fecha);
        $fechaInicio = $fechas[0];
        $fechaFin = $fechas[1];

        return Excel::download(new FacturasExport(saveDate($fechaInicio), saveDate($fechaFin)), 'Facturas.xlsx');
    }
}
