<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Models\VuelosGenerales;

class VuelosTortugaController extends Controller {

    public function send_prof_email($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $VuelosGenerales = VuelosGenerales::find(Encryptor::decrypt($id));

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = 1; // NAcional
                $data['aeropuerto_id'] = $this->TORTUGA_ID;
                $data['hora_llegada'] = $VuelosGenerales->hora_llegada;
                $data['hora_salida'] = $VuelosGenerales->hora_salida;
                $data['aeronave_id'] = $VuelosGenerales->aeronave_id;
                $data['plan_vuelo'] = $VuelosGenerales->plan_vuelo_id;
                $data['cant_pasajeros'] = $VuelosGenerales->pax_desembarcados;
                $data['fecha_vuelo'] = $VuelosGenerales->fecha_llegada;
                $data['serv_excluir'] = ['2.10.3', '2.13.2', '2.10.1', '1.6.1.1'];

                $data['exento_dosa'] = $VuelosGenerales->exento_dosa;
                $data['exento_tasa'] = $VuelosGenerales->exento_tasa;
                //dd($VuelosGenerales);
                $prodserv_dosa = $this->getProdServ($data);
                
                
                

                $proforma = PDF::loadView("reportes.facturas.proforma_general_pdf", compact('prodserv_dosa', 'VuelosGenerales'));

                $emails = json_decode($request->emails_to_send);

                //dd($emails);
                $subject = "Proforma de Vuelo";
                $for = $emails[0];
                $datos = $VuelosGenerales->toArray();

                Mail::send('email.send_proforma', $datos, function ($msj) use ($subject, $for, $proforma, $VuelosGenerales) {

                    $msj->subject($subject);
                    $msj->to($for);
                    $msj->attachData($proforma->output(), "PROFORMA_". showCode($VuelosGenerales->id).".pdf");
                });

                /*
                  // dd($request->pagada);




                  if ($request->correo != null) {

                  $datos = [];
                  $pdf = PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'));
                  try {
                  Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                  //$msj->from("mrivero52.correo@gmail.com", "Registro");
                  $msj->subject($subject);
                  $msj->to($for);
                  $msj->attachData($pdf->output(), "proforma.pdf");
                  });
                  } catch (Exception $ex) {

                  }
                 */
            } else {
                exit;
            }
        } else {
            
            return Redirect::to('/dashboard');
        }
    }

    public function agregar_pago_factura($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());

                $Vuelo = VuelosGenerales::find(Encryptor::decrypt($id));
                $Factura = \App\Models\Facturas::find($Vuelo->factura_tasa);
                \App\Models\FacturasPagos::where('factura_id', Encryptor::decrypt($id))->delete();
                \App\Models\FacturasReferencias::where('factura_id', Encryptor::decrypt($id))->delete();

                $Factura->observacion = $Factura->observacion . ', ' . $request->observacion;
                $Factura->save();
                foreach ($request->money as $key => $value) {
                    $Pagos = new \App\Models\FacturasPagos();
                    $Pagos->factura_id = $Factura->id;
                    $Pagos->forma_pago_id = Encryptor::decrypt($key);
                    $Pagos->monto = saveFloat($value);
                    $Pagos->save();
                }

                foreach ($request->pos as $key => $value) {
                    $Ref = new \App\Models\FacturasReferencias();
                    $Ref->factura_id = $Factura->id;
                    $Ref->punto_id = Encryptor::decrypt($key);
                    $Ref->referencia = ($value == null ? '' : $value);
                    $Ref->save();
                }
            } else {
                $Vuelo = VuelosGenerales::find(Encryptor::decrypt($id));
                //dd();

                $Pilotos = \App\Models\Pilotos::where("id", $Vuelo->piloto_llegada_id)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "=", $Vuelo->procedencia_id)->get()->pluck("full_nombre", "crypt_id");
                $AeropuertosAux = \App\Models\Aeropuertos::find($this->TORTUGA_ID);
                $Aeronaves = \App\Models\Aeronaves::where("id", $Vuelo->aeronave_id)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                $TAQUILLA = $this->getDataTaquilla($this->TORTUGA_ID);

                $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();

                return view('vuelos_tortuga.agregar_pago', compact('id', 'Aeropuertos', 'AeropuertosAux', 'Aeronaves', 'Pilotos', 'TAQUILLA', 'forma_pagos', 'Vuelo'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function reporte_vuelos_tortuga(Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $fechas = explode(" - ", $request->rango);
                $fecha_desde = $this->saveDate($fechas[0]);
                $fecha_hasta = $this->saveDate($fechas[1]);

                $data = VuelosGenerales::where("aeropuerto_id", $this->TORTUGA_ID)
                        ->whereBetween(DB::raw('CAST(fecha_llegada AS date)'), [$fecha_desde, $fecha_hasta])
                        ->get();

                if ($data->count() > 0) {
                    $resp['status'] = 1;
                    $resp['type'] = 'success';
                    $resp['message'] = "Datos Encontrados";
                    $resp['data'] = $data->toArray();
                } else {
                    $resp['status'] = 0;
                    $resp['type'] = 'error';
                    $resp['message'] = "Datos No Encontrados";
                    $resp['data'] = [];
                }
                return $resp;
            } else {
                $data = VuelosGenerales::where("aeropuerto_id", $this->TORTUGA_ID)
                        ->whereBetween(DB::raw('CAST(fecha_llegada AS date)'), [date('Y-m-d'), date('Y-m-d')])
                        ->get();
                return view('vuelos_tortuga.relacion', compact('data'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_piloto(Request $request) {
        // dd("dddd");
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'name_user' => 'required',
                            'surname_user' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'name_user.required' => __('El nombre es Requerido'),
                            'surname_user.required' => __('El Apellido es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Pilotos = \App\Models\Pilotos::where("documento", $request->document)->count();

                    if ($Pilotos == 0) {
                        $Pilotos = new \App\Models\Pilotos();
                        $Pilotos->tipo_documento = $request->type_document;
                        $Pilotos->documento = $request->document;
                        $Pilotos->nombres = Upper($request->name_user);
                        $Pilotos->apellidos = Upper($request->surname_user);
                        $Pilotos->telefono = $request->phone;
                        $Pilotos->correo = $request->email;

                        /*
                          $Aeronaves->user_id = Auth::user()->id;
                          $Aeronaves->ip = $this->getIp();
                          $Aeronaves->fecha_registro = now();
                         */
                        $Pilotos->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = array("id" => $Pilotos->crypt_id, 'full_nombre' => $Pilotos->nombres . " " . $Pilotos->apellidos);
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_nave(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'peso_maximo' => 'required',
                            'matricula' => 'required',
                            'nombre' => 'required',
                                ], [
                            'peso_maximo.required' => __('El Peso Maximo es Requerido'),
                            'matricula.required' => __('La Matricula es Requerida'),
                            'nombre.required' => __('El Modelo es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = \App\Models\Aeronaves::where("matricula", $request->matricula)->count();

                    if ($Aeronaves == 0) {
                        $Aeronaves = new \App\Models\Aeronaves();

                        $Aeronaves->nombre = Upper($request->nombre);
                        $Aeronaves->matricula = Upper($request->matricula);
                        $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                        $Aeronaves->maximo_pasajeros = 0;

                        $Aeronaves->user_id = Auth::user()->id;
                        $Aeronaves->ip = $this->getIp();
                        $Aeronaves->fecha_registro = now();

                        $Aeronaves->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = array("id" => $Aeronaves->crypt_id, 'full_nombre' => $Aeronaves->getFullNombreAttribute());
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function operacion($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Vuelo = VuelosGenerales::find(Encryptor::decrypt($id));
                $Vuelo->procesado_tasa = true;
                $Vuelo->procesado_dosa = true;
                $Vuelo->save();
                return redirect()->route('aviacion_general_tortuga');
            } else {
                $Vuelo = VuelosGenerales::find(Encryptor::decrypt($id));
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "=", $Vuelo->procedencia_id)->get()->pluck("full_nombre", "crypt_id");
                return view('vuelos_tortuga.procesado', compact('Vuelo', 'Pilotos', 'Aeronaves', 'Aeropuertos', 'id'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function index(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                dd($request->all());
            } else {
                //dd($request->all());
                $params = [];
                if (isset($request->fecha_registro)) {
                    if ($request->fecha_registro != null) {
                        $params['fecha_registro'] = $this->saveDate($request->fecha_registro);
                    } else {
                        if ($request->search != 1) {
                            $params['fecha_registro'] = date('Y-m-d');
                        } else {
                            $params['fecha_registro'] = false;
                        }
                    }
                } else {
                    if ($request->search != 1) {
                        $params['fecha_registro'] = date('Y-m-d');
                    } else {
                        $params['fecha_registro'] = false;
                    }
                }


                if ($request->fecha_vuelo != null) {
                    $params['fecha_vuelo'] = $this->saveDate($request->fecha_vuelo);
                }

                if ($request->piloto_id != null) {
                    $params['piloto_id'] = Encryptor::decrypt($request->piloto_id);
                }

                if ($request->matricula_id != null) {
                    $params['aeronave_id'] = Encryptor::decrypt($request->matricula_id);
                }

                $data = $this->getListVuelos($params);

                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                return view('vuelos_tortuga.index', compact('data', 'Pilotos', 'Aeronaves'));
            }
            return Redirect::to('/dashboard');
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function facturacion_general_tortuga($id = null, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {


                $params = [];
                if (isset($request->fecha_registro)) {
                    if ($request->fecha_registro != null) {
                        $params['fecha_registro'] = $this->saveDate($request->fecha_registro);
                    } else {
                        if ($request->search != 1) {
                            $params['fecha_registro'] = date('Y-m-d');
                        } else {
                            $params['fecha_registro'] = false;
                        }
                    }
                } else {
                    if ($request->search != 1) {
                        $params['fecha_registro'] = date('Y-m-d');
                    } else {
                        $params['fecha_registro'] = false;
                    }
                }


                if ($request->fecha_vuelo != null) {
                    $params['fecha_vuelo'] = $this->saveDate($request->fecha_vuelo);
                }

                if ($request->piloto_id != null) {
                    $params['piloto_id'] = Encryptor::decrypt($request->piloto_id);
                }

                if ($request->matricula_id != null) {
                    $params['aeronave_id'] = Encryptor::decrypt($request->matricula_id);
                }


                $data = $this->getListVuelosTortugaFacturar($params);
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
                return view('vuelos_tortuga.facturar_index', compact('data', 'Pilotos', 'Aeronaves'));
            } else {
                if ($request->isMethod('put')) {
                    $Vuelo = VuelosGenerales::find(Encryptor::decrypt($id));

                    $Pilotos = \App\Models\Pilotos::where("id", $Vuelo->piloto_llegada_id)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "=", $Vuelo->procedencia_id)->get()->pluck("full_nombre", "crypt_id");
                    $AeropuertosAux = \App\Models\Aeropuertos::find($this->TORTUGA_ID);
                    $Aeronaves = \App\Models\Aeronaves::where("id", $Vuelo->aeronave_id)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                    $TAQUILLA = $this->getDataTaquilla($this->TORTUGA_ID);

                    $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();

                    return view('vuelos_tortuga.factura_create', compact('id', 'Aeropuertos', 'AeropuertosAux', 'Aeronaves', 'Pilotos', 'TAQUILLA', 'forma_pagos', 'Vuelo'));
                } else {

                    $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                        $Cliente->tipo_documento = $request->type_document;
                        $Cliente->documento = $request->document;

                        $Cliente->user_id = Auth::user()->id;
                        $Cliente->ip = $this->getIp();
                        $Cliente->fecha_registro = now();
                    }

                    $Cliente->razon_social = Upper($request->razon);
                    $Cliente->telefono = $request->phone;
                    $Cliente->correo = $request->correo;
                    $Cliente->direccion = $request->direccion;
                    $Cliente->tipo_id = 4; // general

                    $Cliente->save();

                    $VuelosGenerales = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                    $VuelosGenerales->pax_embarcados = $request->cant_pasajeros;

                    $VuelosGenerales->observaciones_facturacion = $request->observacion;

                    $VuelosGenerales->save();

                    $EURO = $this->getEuro();

                    $TAQUILLA = $this->getDataTaquilla($this->TORTUGA_ID);
                    $TAQUILLA->numero_factura = $request->nro_factura;
                    $TAQUILLA->numero_control = $request->nro_control;
                    $TAQUILLA->save();

                    $Factura = new \App\Models\Facturas();

                    $Factura->fecha_factura = now();
                    $Factura->cliente_id = $Cliente->id;
                    $Factura->nro_factura = $TAQUILLA->numero_factura;
                    $Factura->nro_documento = $TAQUILLA->numero_factura;

                    $Factura->moneda_aplicada = 2;
                    $Factura->monto_moneda_aplicada = $EURO;
                    $Factura->formato_factura = "reportes.facturas.formato_general";
                    $Factura->observacion = Upper($request->observacion);
                    $Factura->tipo_id = 3; // MIXTA (TASA Y DOSA);
                    $Factura->save();

                    $VuelosGenerales->factura_tasa = $Factura->id;
                    $VuelosGenerales->factura_dosa = $Factura->id;
                    $VuelosGenerales->save();

                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['nacionalidad_id'] = 1; // NAcional
                    $data['aeropuerto_id'] = $this->TORTUGA_ID;
                    
                    
                    
                    $data['hora_llegada'] = $VuelosGenerales->hora_llegada; //$VuelosGenerales->hora_llegada;
                    $data['hora_salida'] = $VuelosGenerales->hora_salida; //$VuelosGenerales->hora_llegada;
                    $data['serv_excluir'] = ['2.10.3', '2.13.2', '2.10.1', '1.6.1.1'];
                    
                    
                    $data['aeronave_id'] = $VuelosGenerales->aeronave_id;
                    $data['plan_vuelo'] = $VuelosGenerales->plan_vuelo_id;
                    $data['cant_pasajeros'] = $VuelosGenerales->pax_desembarcados;
                    $data['fecha_vuelo'] = $VuelosGenerales->fecha_salida;
                    $data['exento_dosa'] = $VuelosGenerales->exento_dosa;
                    $data['exento_tasa'] = $VuelosGenerales->exento_tasa;

                    $prodServ = $this->getProdServ($data);

                    $Factura->observacion = (isset($prodServ['observacion']) ? $prodServ['observacion'] : "") . '. ' . $request->observacion;
                    $Factura->save();
                    foreach ($prodServ['data'] as $value) {
                        $Detalle = new \App\Models\FacturasDetalle();

                        $Detalle->cantidad = $value['cant'];
                        $Detalle->factura_id = $Factura->id;
                        $Detalle->codigo = $value['codigo'];
                        $Detalle->descripcion = $value['descripcion'] . ' ' . $value['iva2'];
                        $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                        $Detalle->nomenclatura = ($value['nomenclatura'] == null ? "" : $value['nomenclatura']);

                        $Detalle->precio = $value['bs'];
                        $Detalle->precio2 = $value['precio'];

                        $Detalle->iva = $value['iva_aplicado'];
                        $Detalle->save();
                    }

                    foreach ($request->money as $key => $value) {
                        $Pagos = new \App\Models\FacturasPagos();
                        $Pagos->factura_id = $Factura->id;
                        $Pagos->forma_pago_id = Encryptor::decrypt($key);
                        $Pagos->monto = saveFloat($value);
                        $Pagos->save();
                    }

                    foreach ($request->pos as $key => $value) {
                        $Ref = new \App\Models\FacturasReferencias();
                        $Ref->factura_id = $Factura->id;
                        $Ref->punto_id = Encryptor::decrypt($key);
                        $Ref->referencia = saveFloat($value);
                        $Ref->save();
                    }

                    $back = 'facturacion_general_tortuga';
                    $urlFactura = route('facturacion_general_tortuga.view_factura', $Factura->crypt_id);
                    
                    return view('reportes.facturas.view', compact('Factura', 'back', 'urlFactura'));

                    //dd($Vuelo = VuelosGenerales::find(Encryptor::decrypt($id)));
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function regresar_operacion($id) {
        $VuelosGenerales = \App\Models\VuelosGenerales::find((Encryptor::decrypt($id)));
        $VuelosGenerales->listo_facturar = false;
        $VuelosGenerales->save();
    }

    private function getListVuelosTortugaFacturar($params = []) {
        $where[] = ['listo_facturar', true];

        if (isset($params['fecha_registro'])) {
            if ($params['fecha_registro'] != false) {
                $where[] = [
                    function ($query) use ($params) {
                        $query->where(DB::raw("(CAST(fecha_registro AS DATE) = '" . $params['fecha_registro'] . "')"), true);
                    }
                ];
            }
        } else {
            $where[] = [
                function ($query) {
                    $query->where(DB::raw("(CAST(fecha_registro AS DATE) = '" . date('Y-m-d') . "')"), true);
                }
            ];
        }
        if (isset($params['fecha_vuelo'])) {
            $where[] = [
                function ($query) use ($params) {
                    $query->where(DB::raw("(CAST(fecha_llegada AS DATE) = '" . $params['fecha_vuelo'] . "')"), true);
                }
            ];
        }

        if (isset($params['aeronave_id'])) {
            $where[] = ['aeronave_id', $params['aeronave_id']];
        }
        if (isset($params['piloto_id'])) {
            $where[] = ['piloto_llegada_id', $params['piloto_id']];
        }


        $data = VuelosGenerales::where("aeropuerto_id", $this->TORTUGA_ID)
                /* ->where(function ($q) {
                  $q->where(DB::raw("proc_vuelos_generales.fecha_registro::date = '" . date('Y-m-d') . "' OR proc_vuelos_generales.finalizado IS FALSE"), TRUE);
                  })
                 * 
                 */
                ->where($where)
                //->where("cerrado", false)
                //->where("activo", true)
                ->orderBy("numero_operacion", "ASC")
                ->get();

        $data = Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('fecha2', function ($row) {
                            $actionBtn = $row->fecha_llegada == null ? showDate($row->fecha_salida) : showDate($row->fecha_llegada);

                            return $actionBtn;
                        })
                        ->addColumn('class', function ($row) {
                            $class = '';

                            if ($row->factura_tasa == null) {
                                $class = 'alert-warning';
                            } else {
                                $class = 'alert-info';
                            }
                            /*
                              if ($row->factura_tasa==null) {
                              if ($row->listo_facturar == true){
                              $class = 'alert-info';
                              }else{
                              $class = 'alert-success';
                              }
                              }else{
                              $class = 'alert-danger';
                              }
                             */
                            return $class;
                        })
                        ->addColumn('action', function ($row) {
                            $actionBtn = "";
                            //EDitar
                            if ($row->factura_tasa != null) {

                                //if (date('')){
                                $actionBtn .= '<a onClick="anularFacturaTortuga(\'' . Encryptor::encrypt($row->factura_tasa) . '\')" title="Anular Factura" href="javascript:void(0)" class=" btn btn-danger btn-sm mr-2"> <li class="fa fa-trash"></li> </a> ';
                                //}
                                $actionBtn .= '<a onClick="window.open(\'' . route('facturacion_general_tortuga.view_factura', Encryptor::encrypt($row->factura_tasa)) . '\')" title="Ver la Factura" href="javascript:void(0)" class=" btn btn-info btn-sm mr-2"> <li class="fa fa-eye"></li> </a> ';
                                if ($row->esta_pagado_tasa == false) {
                                    $actionBtn .= '<a onClick="agregarPago(\'' . $row->crypt_id . '\')" title="Agregar Pago a la Factura" href="javascript:void(0)" class=" btn btn-success btn-sm mr-2"> <li class="fa fa-money-bill"></li> </a> ';
                                }
                            } else {
                                $actionBtn .= '<a onClick="factVueloTort(\'' . $row->crypt_id . '\')" title="Facturar Vuelo" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-file-invoice-dollar"></li> </a> ';
                                $actionBtn .= '<a onClick="backOper(\'' . $row->crypt_id . '\')" title="Retornar a Operación" href="javascript:void(0)" class=" btn btn-danger btn-sm mr-2"> <li class="fa fa-step-backward"></li> </a> ';
                            }



                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true)
                ->original['data'];

        return $data;
    }

    public function anular_factura_vuelo($id) {


        $Factura = \App\Models\Facturas::find(Encryptor::decrypt($id));
        $Factura->anulada = true;
        $Factura->save();

        $VuelosGenerales = \App\Models\VuelosGenerales::where("factura_tasa", $Factura->id)
                ->orWhere("factura_dosa", $Factura->id)
                /* where(function ($q) use($Factura) {
                  $q->where(DB::raw("(proc_facturas.factura_tasa = " . $Factura->id . " OR proc_facturas.factura_dosa = ".$Factura->id.")"), TRUE);
                  })
                 */
                ->update(["factura_tasa" => null, "factura_dosa" => null, "listo_facturar" => true]);
    }

    private function getListVuelos($params = []) {
        $where[] = [DB::raw('1'), 1];

        if (isset($params['fecha_registro'])) {
            if ($params['fecha_registro'] != false) {
                $where[] = [
                    function ($query) use ($params) {
                        $query->where(DB::raw("(CAST(fecha_registro AS DATE) = '" . $params['fecha_registro'] . "')"), true);
                    }
                ];
            }
        } else {
            $where[] = [
                function ($query) {
                    $query->where(DB::raw("(CAST(fecha_registro AS DATE) = '" . date('Y-m-d') . "')"), true);
                }
            ];
        }
        if (isset($params['fecha_vuelo'])) {
            $where[] = [
                function ($query) use ($params) {
                    $query->where(DB::raw("(CAST(fecha_llegada AS DATE) = '" . $params['fecha_vuelo'] . "')"), true);
                }
            ];
        }

        if (isset($params['aeronave_id'])) {
            $where[] = ['aeronave_id', $params['aeronave_id']];
        }
        if (isset($params['piloto_id'])) {
            $where[] = ['piloto_llegada_id', $params['piloto_id']];
        }


        $data = VuelosGenerales::where("aeropuerto_id", $this->TORTUGA_ID)
                /*
                  ->where(function ($q) {
                  $q->where(DB::raw("proc_vuelos_generales.fecha_registro::date = '" . date('Y-m-d') . "' OR proc_vuelos_generales.finalizado IS FALSE"), TRUE);
                  })
                 */
                //->where("cerrado", false)
                //->where("activo", true)
                ->where($where)
                ->orderBy("numero_operacion", "DESC")
                ->get();
        //->toSql();
        //dd($where);
        $data = Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('fecha2', function ($row) {
                            $actionBtn = $row->fecha_llegada == null ? showDate($row->fecha_salida) : showDate($row->fecha_llegada);

                            return $actionBtn;
                        })
                        ->addColumn('class', function ($row) {
                            $class = '';
                            if ($row->activo == true) {
                                if ($row->listo_facturar == true) {
                                    if ($row->factura_tasa != null || $row->factura_dosa != null) {
                                        $class = 'alert-info'; // LISTO PARA FACTURAR
                                    } else {
                                        $class = 'alert-warning'; // LISTO PARA FACTURAR
                                    }
                                } else { // 
                                    $class = 'alert-success';
                                }
                            } else { // ANULADAS
                                $class = 'alert-danger';
                            }

                            return $class;
                        })
                        ->addColumn('action', function ($row) {
                            $actionBtn = "";
                            //EDitar
                            if ($row->activo == true && $row->finalizado == false && $row->listo_facturar == false) {
                                $actionBtn .= '<a onClick="registerTortoise(\'' . $row->crypt_id . '\')" title="Editar" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-edit"></li> </a> ';
                            }
                            //Eliminar
                            if ($row->activo == true && $row->finalizado == false && $row->listo_facturar == false) {
                                $actionBtn .= '<a onClick="anularRegistroVueloTortuga(\'' . $row->crypt_id . '\')" title="Anular Registro" href="javascript:void(0)" class=" btn btn-danger btn-sm mr-2"> <li class="fa fa-trash"></li> </a> ';
                            }
                            //Liusto a Facturar
                            if ($row->activo == true && $row->finalizado == false && $row->listo_facturar == false) {
                                $actionBtn .= '<a onClick="listoFacturar(\'' . $row->crypt_id . '\')" title="Preparar para su Facturación" href="javascript:void(0)" class=" btn btn-success btn-sm mr-2"> <li class="fa fa-location-arrow"></li> </a> ';
                                if ($row->procesado_tasa == false) {
                                    $actionBtn .= '<a onClick="setAjax(this, event)" title="Registrar Operación" href="' . route('aviacion_general_tortuga.operacion', $row->crypt_id) . '" class=" btn btn-success btn-sm mr-2"> <li class="fa fa-plane"></li> </a> ';
                                }
                            }

                            if ($row->factura_tasa != null || $row->factura_dosa != null) {
                                $actionBtn .= '<a onClick="verFacturar(\'' . $row->crypt_id . '\')" title="Ver la Factura" href="javascript:void(0)" class=" btn btn-info btn-sm mr-2"> <li class="fa fa-file-invoice-dollar"></li> </a> ';
                                if ($row->procesado_tasa == false) {
                                    $actionBtn .= '<a onClick="setAjax(this, event)" title="Registrar Operación" href="' . route('aviacion_general_tortuga.operacion', $row->crypt_id) . '" class=" btn btn-success btn-sm mr-2"> <li class="fa fa-plane"></li> </a> ';
                                }
                            }


                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true)
                ->original['data'];
        return $data;
    }

    public function store($id = null, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //  dd($request->all());


                $Validator = \Validator::make(
                                $request->all(), [
                            'piloto_id' => 'required',
                            'aeronave_id' => 'required',
                            'origen_id' => 'required',
                            'plan_vuelo' => 'required',
                            'fecha' => 'required',
                            'cant_pasajeros' => 'required',
                                ], [
                            'piloto_id.required' => __('El Piloto es Requerido'),
                            'aeronave_id.unique' => __('La Aeronave es Requerida'),
                            'origen_id.required' => __('El Origen es Requerido'),
                            'plan_vuelo.required' => __('El Plan de Vuelo es Requerido'),
                            'fecha.required' => __('La fecha de Vuelo es Requerida'),
                            'cant_pasajeros.required' => __('La Cant. de Pasajeros es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {



                    /*                     * ******* DATA VUELOS GENERALES **** */
                    if ($id == null) {


                        $VuelosGeneralesNumControl = \App\Models\VuelosGenerales::where("activo", 1)
                                ->where("aeropuerto_id", $this->TORTUGA_ID)
                                ->orderBy("id", "DESC")
                                ->first();
                        $VuelosGeneralesNumControl = $VuelosGeneralesNumControl == null ? 1 : $VuelosGeneralesNumControl->numero_operacion + 1;

                        $VuelosGenerales = new \App\Models\VuelosGenerales();
                    } else {
                        $VuelosGenerales = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                        $VuelosGeneralesNumControl = $VuelosGenerales->numero_operacion;
                    }
                    $VuelosGenerales->fecha_registro = now();
                    $VuelosGenerales->fecha_llegada = $this->saveDate($request->fecha);
                    $VuelosGenerales->user_id = Auth::user()->id;
                    $VuelosGenerales->aeropuerto_id = $this->TORTUGA_ID;
                    $VuelosGenerales->numero_operacion = $VuelosGeneralesNumControl;

                    $VuelosGenerales->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                    $VuelosGenerales->piloto_llegada_id = Encryptor::decrypt($request->piloto_id);
                    if ($request->plan_vuelo == 1) {
                        $VuelosGenerales->hora_llegada = $request->hora_llegada_toque_1;
                    } else {
                        $VuelosGenerales->hora_llegada = $request->hora_llegada_estadia;
                    }

                    $VuelosGenerales->tipo_llegada_id = 1; //NACIONAL

                    $VuelosGenerales->procedencia_id = Encryptor::decrypt($request->origen_id);
                    $VuelosGenerales->pax_desembarcados = $request->cant_pasajeros;
                    $VuelosGenerales->piloto_salida_id = Encryptor::decrypt($request->piloto_id);
                    $VuelosGenerales->fecha_salida = $this->saveDate($request->fecha);

                    $VuelosGenerales->exento_tasa = $request->cobrar_tasa == 1 ? false : true;
                    $VuelosGenerales->exento_dosa = $request->cobrar_dosa == 1 ? false : true;

                    if ($request->plan_vuelo == 1) {
                        $VuelosGenerales->hora_salida = $request->hora_salida_toque_1;
                    } else {
                        $VuelosGenerales->hora_salida = $request->hora_salida_estadia;
                    }


                    $VuelosGenerales->tipo_salida_id = 1; //NACIONAL

                    $VuelosGenerales->destino_id = Encryptor::decrypt($request->origen_id);
                    $VuelosGenerales->pax_embarcados = $request->cant_pasajeros;
                    $VuelosGenerales->observaciones_operaciones = $request->observacion;
                    $VuelosGenerales->observaciones_facturacion = "";

                    $VuelosGenerales->plan_vuelo_id = $request->plan_vuelo;

                    $VuelosGenerales->activo = 1;
                    //$VuelosGenerales->prodservicios_extra = 0;


                    $VuelosGenerales->ip = $this->getIp();
                    //$VuelosGenerales->finalizado = now();

                    $VuelosGenerales->save();

                    \App\Models\VuelosTortuga::where("vuelo_general_id", $VuelosGenerales->id)->delete();

                    if ($request->plan_vuelo == 1) {
                        $Tortuga = new \App\Models\VuelosTortuga();
                        $Tortuga->vuelo_general_id = $VuelosGenerales->id;
                        $Tortuga->hora_llegada = $request->hora_llegada_toque_2;
                        $Tortuga->hora_salida = $request->hora_salida_toque_2;
                        $Tortuga->save();
                    }







                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Procesado Correctamente');
                    $result['data'] = null;
                }
                return $result;
            } else {

                if ($id != null) {
                    $VuelosGenerales = VuelosGenerales::find(Encryptor::decrypt($id))->toArray();
                } else {
                    $VuelosGenerales = [];
                }
                //dd($VuelosGenerales);
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "!=", $this->TORTUGA_ID)->get()->pluck("full_nombre", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                return view('vuelos_tortuga.store', compact('Pilotos', 'Aeropuertos', 'Aeronaves', 'VuelosGenerales', 'id'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());
                /*                 * **** DATA CLIENTE **** */
                $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                if ($Cliente == null) {
                    $Cliente = new \App\Models\Clientes();
                    $Cliente->tipo_documento = $request->type_document;
                    $Cliente->documento = $request->document;

                    $Cliente->user_id = Auth::user()->id;
                    $Cliente->ip = $this->getIp();
                    $Cliente->fecha_registro = now();
                }

                $Cliente->razon_social = Upper($request->razon);
                $Cliente->telefono = $request->phone;
                $Cliente->correo = $request->correo;
                $Cliente->direccion = $request->direccion;
                $Cliente->tipo_id = 4; // general

                $Cliente->save();

                /*                 * ******* DATA VUELOS GENERALES **** */
                $VuelosGeneralesNumControl = \App\Models\VuelosGenerales::where("activo", 1)
                        ->where("aeropuerto_id", $this->TORTUGA_ID)
                        ->orderBy("id", "DESC")
                        ->first();
                $VuelosGeneralesNumControl = $VuelosGeneralesNumControl == null ? 1 : $VuelosGeneralesNumControl->numero_operacion + 1;

                $VuelosGenerales = new \App\Models\VuelosGenerales();

                $VuelosGenerales->fecha_registro = now();
                $VuelosGenerales->fecha_llegada = $this->saveDate($request->fecha);
                $VuelosGenerales->user_id = Auth::user()->id;
                $VuelosGenerales->aeropuerto_id = $this->TORTUGA_ID;
                $VuelosGenerales->numero_operacion = $VuelosGeneralesNumControl;

                $VuelosGenerales->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                $VuelosGenerales->piloto_llegada_id = Encryptor::decrypt($request->piloto_id);
                $VuelosGenerales->hora_llegada = $request->hora_llegada;
                $VuelosGenerales->tipo_llegada_id = 1; //NACIONAL

                $VuelosGenerales->procedencia_id = Encryptor::decrypt($request->origen_id);
                $VuelosGenerales->pax_desembarcados = $request->cant_pasajeros;
                $VuelosGenerales->piloto_salida_id = Encryptor::decrypt($request->piloto_id);
                $VuelosGenerales->fecha_salida = $this->saveDate($request->fecha);
                $VuelosGenerales->hora_salida = $request->hora_salida;
                $VuelosGenerales->tipo_salida_id = 1; //NACIONAL

                $VuelosGenerales->destino_id = Encryptor::decrypt($request->origen_id);
                $VuelosGenerales->pax_embarcados = $request->cant_pasajeros;
                $VuelosGenerales->observaciones_operaciones = $request->observacion;
                $VuelosGenerales->observaciones_facturacion = $request->observacion;
                $VuelosGenerales->activo = 1;

                $VuelosGenerales->plan_vuelo_id = $request->plan_vuelo;
                //$VuelosGenerales->prodservicios_extra = 0;
                $VuelosGenerales->listo_facturar = true;

                $VuelosGenerales->ip = $this->getIp();
                //$VuelosGenerales->finalizado = now();

                $VuelosGenerales->save();

                $EURO = $this->getEuro();

                $TAQUILLA = $this->getDataTaquilla($this->TORTUGA_ID);
                $TAQUILLA->numero_factura = $request->nro_factura;
                $TAQUILLA->numero_control = $request->nro_control;
                $TAQUILLA->save();

                $Factura = new \App\Models\Facturas();

                $Factura->fecha_factura = now();
                $Factura->cliente_id = $Cliente->id;
                $Factura->nro_factura = $request->nro_factura;
                $Factura->nro_documento = $request->nro_control;

                $Factura->moneda_aplicada = 2;
                $Factura->monto_moneda_aplicada = $EURO;
                $Factura->formato_factura = "reportes.facturas.formato_general";
                $Factura->observacion = Upper($request->observacion);
                $Factura->tipo_id = 3; // MIXTA (TASA Y DOSA);

                $Factura->save();

                $VuelosGenerales->factura_tasa = $Factura->id;
                $VuelosGenerales->factura_dosa = $Factura->id;
                $VuelosGenerales->save();

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = 1; // NAcional
                $data['aeropuerto_id'] = $this->TORTUGA_ID;
                $data['hora_llegada'] = $request->hora_llegada;
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['plan_vuelo'] = $request->plan_vuelo;
                $data['cant_pasajeros'] = $request->cant_pasajeros;
                $data['fecha_vuelo'] = $request->fecha_vuelo;

                $prodServ = $this->getProdServ($data);
                //dd($prodServ);
                foreach ($prodServ as $value) {
                    $Detalle = new \App\Models\FacturasDetalle();

                    $Detalle->cantidad = $value['cant'];
                    $Detalle->factura_id = $Factura->id;
                    $Detalle->codigo = $value['codigo'];
                    $Detalle->descripcion = $value['descripcion'] . ' ' . $value['iva2'];
                    $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                    $Detalle->nomenclatura = ($value['nomenclatura'] == null ? "" : $value['nomenclatura']);
                    $Detalle->precio = $value['bs'];
                    $Detalle->precio2 = $value['precio'];
                    $Detalle->iva = $value['iva_aplicado'];
                    $Detalle->save();
                }

                foreach ($request->money as $key => $value) {
                    $Pagos = new \App\Models\FacturasPagos();
                    $Pagos->factura_id = $Factura->id;
                    $Pagos->forma_pago_id = Encryptor::decrypt($key);
                    $Pagos->monto = saveFloat($value);
                    $Pagos->save();
                }

                foreach ($request->pos as $key => $value) {
                    $Ref = new \App\Models\FacturasReferencias();
                    $Ref->factura_id = $Factura->id;
                    $Ref->punto_id = Encryptor::decrypt($key);
                    $Ref->referencia = saveFloat($value);
                    $Ref->save();
                }

                $back = 'aviacion_general_tortuga_create';
                $urlFactura = route('aviacion_general_tortuga.view_factura', $Factura->crypt_id);

                return view('reportes.facturas.view', compact('Factura', 'back', 'urlFactura'));
            } else {


                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "!=", $this->TORTUGA_ID)->get()->pluck("full_nombre", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                $TAQUILLA = $this->getDataTaquilla($this->TORTUGA_ID);

                $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
                //dd("sss");
                //
                return view('vuelos_tortuga.create', compact('Aeropuertos', 'Aeronaves', 'Pilotos', 'TAQUILLA', 'forma_pagos'));
            }
            return Redirect::to('/dashboard');
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function anular_vuelo($id) {
        $VuelosGenerales = \App\Models\VuelosGenerales::find((Encryptor::decrypt($id)));
        $VuelosGenerales->activo = false;
        $VuelosGenerales->save();
    }

    public function lista_facturar($id) {
        $VuelosGenerales = \App\Models\VuelosGenerales::find((Encryptor::decrypt($id)));
        $VuelosGenerales->listo_facturar = true;
        $VuelosGenerales->save();
    }

    public function view_factura($id, $copia = 0) {
        $Factura = \App\Models\Facturas::find((Encryptor::decrypt($id)));
        //dd(Encryptor::decrypt($id));
        return view($Factura->formato_factura, compact('Factura', 'copia'));
    }

    public function get_serv(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'piloto_id' => 'required',
                            'aeronave_id' => 'required',
                            'origen_id' => 'required',
                            'plan_vuelo' => 'required',
                            'fecha' => 'required',
                            'cant_pasajeros' => 'required'
                                ], [
                            'piloto_id.required' => __('El Piloto es Requerido'),
                            'aeronave_id.unique' => __('La Aeronave es Requerida'),
                            'origen_id.required' => __('El Origen es Requerido'),
                            'plan_vuelo.required' => __('El Plan de Vuelo es Requerido'),
                            'fecha.required' => __('La fecha de Vuelo es Requerida'),
                            'cant_pasajeros.required' => __('La Cant. de Pasajeros es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {



                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['nacionalidad_id'] = 1; // NAcional
                    $data['aeropuerto_id'] = $this->TORTUGA_ID;
                    $data['hora_llegada'] = "08:00";
                    $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                    $data['plan_vuelo'] = $request->plan_vuelo;
                    $data['cant_pasajeros'] = $request->cant_pasajeros;
                    $data['fecha_vuelo'] = $request->fecha_vuelo;

                    $prodServ = $this->getProdServ($data);

                    //dd($prodServ);
                    //dd($prodServ);
                    /*




                      $prodserv_tasa = $this->getProdservicios2('tasa', $data);
                      $prodserv_tasa[0]['tipo'] = "TASA";
                      $prodserv_tasa[0]['cant'] = "1";


                      $serv_excluir = ['2.10.3', '2.13.2'];
                      if ($request->plan_vuelo == 1) {// SI ES DOBLE TOQUE
                      foreach ($prodserv_dosa as $key => $value) {
                      if (in_array($value['codigo'], $serv_excluir)) {
                      unset($prodserv_dosa[$key]);
                      } else {
                      if ($value['codigo'] == '2.2.1') {
                      $prodserv_dosa[$key]['cant'] = 1;
                      } else {
                      $prodserv_dosa[$key]['cant'] = 2;
                      }

                      $prodserv_dosa[$key]['tipo'] = "DOSA";
                      }
                      }
                      } else {//ESTADIA
                      foreach ($prodserv_dosa as $key => $value) {
                      if (in_array($value['codigo'], $serv_excluir)) {
                      unset($prodserv_dosa[$key]);
                      } else {
                      if ($value['codigo'] == "2.2.1") {
                      $prodserv_dosa[$key]['cant'] = 8;
                      } else {
                      $prodserv_dosa[$key]['cant'] = 2;
                      }
                      $prodserv_dosa[$key]['tipo'] = "DOSA";
                      }
                      }
                      }
                      $prodserv_dosa[] = $prodserv_tasa[0];
                     */
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Procesado Correctamente');
                    $result['data'] = $prodServ['data'];
                }
                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function get_serv_2($id) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {

                $Vuelo = VuelosGenerales::find(Encryptor::decrypt($id));
                
                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = 1; // NAcional
                $data['aeropuerto_id'] = $this->TORTUGA_ID;
                
                $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->fecha_llegada);



                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                //$data['fecha_vuelo'] = $this->saveDate($request->fecha) . " 00:00:00";
                //Carbon::createFromFormat('Y-m-d H:i:s', '2016-01-23 11:53:20');





                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                $data['aeronave_id'] = $Vuelo->aeronave_id;
                $data['plan_vuelo'] = 2; // Doble Toque
                $data['cant_pasajeros'] = $Vuelo->pax_desembarcados;

               
                $data['hora_llegada'] = $Vuelo->hora_llegada;
                $data['hora_salida'] = $Vuelo->hora_salida;
               





                $data['exento_dosa'] = $Vuelo->exento_dosa;
                $data['exento_tasa'] = $Vuelo->exento_tasa;

                //dd($data);

                $prodServ = $this->getProdServ($data);
                
                
                
                

                

                

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesado Correctamente');
                $result['data'] = $prodServ['data'];

                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

}
