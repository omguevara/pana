<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use DataTables;
use PDF;
use App\Models\Facturas;

class FacturasController extends Controller {

    public function view_invoice($id) {
        $id = Encryptor::decrypt($id);
        $Factura = Facturas::find($id);

        $Reservaciones = \App\Models\Reservaciones::where("solicitud_id", $id)->get();
        if ($Factura->impresa == false) {
            $Factura->impresa = true;
            $Factura->save();
            return view('reportes.facturas.' . $Factura->formato_factura, compact('Factura', 'Reservaciones'));
        } else {
            dd("Factura Impresa");
        }
        //dd($Factura->getClienteAttribute->getRifAttribute());
        //dd($Factura->getDetalle);
    }

    public function view_factura($id) {
        //dd($id);
        $Factura = Facturas::find((Encryptor::decrypt($id)));
        return view($Factura->formato_factura, compact('Factura'));
    }

    public function create($id, Request $request) {
        if (\Request::ajax()) {

            //dd($request->all());

            $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
            if ($Cliente == null) {
                $Cliente = new \App\Models\Clientes();
                $Cliente->tipo_documento = $request->type_document;
                $Cliente->documento = $request->document;

                $Cliente->user_id = Auth::user()->id;
                $Cliente->ip = $this->getIp();
                $Cliente->fecha_registro = now();
            }

            $Cliente->razon_social = Upper($request->razon);
            $Cliente->telefono = $request->phone;
            $Cliente->correo = $request->correo;
            $Cliente->direccion = $request->direccion;
            $Cliente->tipo_id = 4; // general

            $Cliente->save();

            $EURO = $this->getEuro();

            $TAQUILLA = $this->getDataTaquilla();
            $TAQUILLA->numero_factura = $TAQUILLA->numero_factura + 1;
            $TAQUILLA->numero_control = $TAQUILLA->numero_control + 1;
            $TAQUILLA->save();

            $Factura = new Facturas();

            $Factura->fecha_factura = now();
            $Factura->cliente_id = $Cliente->id;
            $Factura->nro_factura = $TAQUILLA->numero_factura;
            $Factura->nro_documento = $TAQUILLA->numero_factura;

            $Factura->moneda_aplicada = 2;
            $Factura->monto_moneda_aplicada = $EURO;
            $Factura->formato_factura = "reportes.facturas.formato_general";
            $Factura->observacion = Upper($request->observacion);

            if ($request->tipo == "tasa") {
                $Factura->tipo_id = 1;
                $Factura->save();
                $tasa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));
                $vuelo = $tasa;
                $vuelo->factura_tasa = $Factura->id;
                $vuelo->save();
                $Aeropuerto = \App\Models\Aeropuertos::find($tasa->aeropuerto_id);
                $data['tipo_vuelo_id'] = 2;
                $data['nacionalidad_id'] = $tasa->tipo_salida_id;
                $data['aeropuerto_id'] = $tasa->aeropuerto_id;
                $data['aeronave_id'] = $tasa->aeronave_id;
                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $tasa->hora_salida));
                $data['cant_pasajeros'] = $tasa->pax_embarcados;
                $prodserv = $this->getProdservicios2('tasa', $data);
            } else {
                if ($request->tipo == "dosa") {
                    $Factura->tipo_id = 2;
                    $Factura->save();
                    $dosa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));
                    $vuelo = $dosa;
                    $vuelo->factura_dosa = $Factura->id;
                    $vuelo->save();
                    $Aeropuerto = \App\Models\Aeropuertos::find($dosa->aeropuerto_id);
                    $data['tipo_vuelo_id'] = 2;
                    $data['nacionalidad_id'] = $dosa->tipo_llegada_id;
                    $data['aeropuerto_id'] = $dosa->aeropuerto_id;
                    $data['aeronave_id'] = $dosa->aeronave_id;
                    $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $dosa->hora_llegada));
                    $data['cant_pasajeros'] = $dosa->pax_desembarcados;
                    $prodserv = $this->getProdservicios2('dosa', $data);
                } else {
                    dd("0");
                }
            }




            $prodservicios_extra = [];
            if ($vuelo->prodservicios_extra != null) {
                $ext = explode(",", $vuelo->prodservicios_extra);
                foreach ($ext as $value) {
                    $ext2 = explode(":", $value);
                    $ext3[] = $ext2[1];
                    $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                }
                $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
            }
            foreach ($prodserv as $value) {
                $Detalle = new \App\Models\FacturasDetalle();

                $Detalle->cantidad = 1;
                $Detalle->factura_id = $Factura->id;
                $Detalle->codigo = $value['codigo'];
                $Detalle->descripcion = $value['descripcion'] . ' ' . $value['iva2'];
                $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                $Detalle->nomenclatura = ($value['nomenclatura'] == null ? "" : $value['nomenclatura']);
                $Detalle->precio = $value['bs'] * $value['categoria_aplicada'];
                $Detalle->precio2 = $value['precio'] * $value['categoria_aplicada'];
                $Detalle->iva = $value['iva_aplicado'];
                $Detalle->save();
            }

            foreach ($prodservicios_extra as $value) {
                $Detalle = new \App\Models\FacturasDetalle();

                $Detalle->cantidad = $cantidades[$value["crypt_id"]];
                $Detalle->factura_id = $Factura->id;
                $Detalle->codigo = $value['codigo'];
                $Detalle->descripcion = $value['descripcion'] . ' ' . $value['iva2'];
                $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                $Detalle->nomenclatura = ($value['nomenclatura'] == null ? "" : $value['nomenclatura']);
                $Detalle->precio = $value['bs'] * $value['categoria_aplicada'] * $cantidades[$value["crypt_id"]];
                $Detalle->precio2 = $value['precio'] * $value['categoria_aplicada'] * $cantidades[$value["crypt_id"]];
                $Detalle->iva = $value['iva_aplicado'];
                $Detalle->save();
            }

            foreach ($request->money as $key => $value) {
                $Pagos = new \App\Models\FacturasPagos();
                $Pagos->factura_id = $Factura->id;
                $Pagos->forma_pago_id = Encryptor::decrypt($key);
                $Pagos->monto = saveFloat($value);
                $Pagos->save();
            }

            foreach ($request->pos as $key => $value) {
                $Ref = new \App\Models\FacturasReferencias();
                $Ref->factura_id = $Factura->id;
                $Ref->punto_id = Encryptor::decrypt($key);
                $Ref->referencia = saveFloat($value);
                $Ref->save();
            }

            $back='facturas';





            return view('reportes.facturas.view', compact('Factura', 'back'));
        } else {
            return Redirect::to('/dashboard');
        }
    }

    private function getProdServTortuga($datos) {
        $Aeropuerto = \App\Models\Aeropuertos::find($this->TORTUGA_ID);
        $datos['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $datos['hora_llegada']));
        $prodserv_dosa = $this->getProdservicios2('dosa', $datos);
        $datos['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", "18:00"));
        $prodserv_tasa = $this->getProdservicios2('tasa', $datos);
        $prodserv_tasa[0]['tipo'] = "TASA";
        $prodserv_tasa[0]['cant'] = "1";


        $serv_excluir = ['2.10.3', '2.13.2'];
        if ($datos['plan_vuelo'] == 1) {// SI ES DOBLE TOQUE 
            foreach ($prodserv_dosa as $key => $value) {
                if (in_array($value['codigo'], $serv_excluir)) {
                    unset($prodserv_dosa[$key]);
                } else {
                    if ($value['codigo'] == '2.2.1') {
                        $prodserv_dosa[$key]['cant'] = 1;
                    } else {
                        $prodserv_dosa[$key]['cant'] = 2;
                    }

                    $prodserv_dosa[$key]['tipo'] = "DOSA";
                }
            }
        } else {//ESTADIA
            foreach ($prodserv_dosa as $key => $value) {
                if (in_array($value['codigo'], $serv_excluir)) {
                    unset($prodserv_dosa[$key]);
                } else {
                    if ($value['codigo'] == "2.2.1") {
                        $prodserv_dosa[$key]['cant'] = 8;
                    } else {
                        $prodserv_dosa[$key]['cant'] = 2;
                    }
                    $prodserv_dosa[$key]['tipo'] = "DOSA";
                }
            }
        }
        $prodserv_dosa[] = $prodserv_tasa[0];

        return $prodserv_dosa;
    }

    public function tortuga($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                
            } else {
                return view('facturas.tortuga');
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function tasas($id, Request $request) {
        if (\Request::ajax()) {
            if ($this->getPermission(null) == true) {


                $TAQUILLA = $this->getDataTaquilla();

                $tasa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                $Aeropuerto = \App\Models\Aeropuertos::find($tasa->aeropuerto_id);
                $data['tipo_vuelo_id'] = 2;
                $data['nacionalidad_id'] = $tasa->tipo_salida_id;
                $data['aeropuerto_id'] = $tasa->aeropuerto_id;
                $data['aeronave_id'] = $tasa->aeronave_id;
                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $tasa->hora_salida));
                $data['cant_pasajeros'] = $tasa->pax_embarcados;
                $prodserv = $this->getProdservicios2('tasa', $data);
                $MonedaPago = $this->TIPOS_VUELOS_NOMEDAS[2];
                $EURO = $this->getEuro();

                $prodservicios_extra = [];
                $cantidades = [];
                if ($tasa->prodservicios_extra != null) {
                    $ext = explode(",", $tasa->prodservicios_extra);
                    foreach ($ext as $value) {
                        $ext2 = explode(":", $value);
                        $ext3[] = $ext2[1];
                        $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                    }

                    $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
                }


                $prodServs = $this->getProdservicios2("extra", $data);

                //dd();
                $listPrdServs = [];
                foreach ($prodServs as $value) {
                    $listPrdServs[$value['crypt_id']] = $value['full_descripcion'] . ' ' . $value['iva2'] . " (" . $value['moneda'] . ": " . muestraFloat($value['precio']) . ") (BS.: " . muestraFloat($value['bs']) . ")";
                }
                // dd($listPrdServs);
                $tipo = "tasa";
                $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
                return view('facturas.detalle', compact('cantidades', 'TAQUILLA', 'prodserv', 'MonedaPago', 'EURO', 'listPrdServs', 'prodservicios_extra', 'id', 'tipo', 'forma_pagos'));
            } else {
                $msg = "Taquilla No Configurada";
                return view('errors.general', compact('msg'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_prodservs($id, Request $request) {
        if (\Request::ajax()) {

            if ($this->getPermission(null) == true) {

                $TAQUILLA = $this->getDataTaquilla();

                $VuelosG = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                $prodservs[] = $request->cant . ":" . Encryptor::decrypt($request->prodservs_id);

                if ($VuelosG->prodservicios_extra != null && $VuelosG->prodservicios_extra != '') {
                    $VuelosG->prodservicios_extra = implode(",", array_merge(explode(",", $VuelosG->prodservicios_extra), $prodservs));
                } else {
                    $VuelosG->prodservicios_extra = implode(",", $prodservs);
                }

                $VuelosG->save();
                $cantidades = [];
                $tipo = $request->tipo;
                if ($request->tipo == "tasa") {
                    $tasa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                    $Aeropuerto = \App\Models\Aeropuertos::find($tasa->aeropuerto_id);
                    $data['tipo_vuelo_id'] = 2;
                    $data['nacionalidad_id'] = $tasa->tipo_salida_id;
                    $data['aeropuerto_id'] = $tasa->aeropuerto_id;
                    $data['aeronave_id'] = $tasa->aeronave_id;
                    $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $tasa->hora_salida));
                    $data['cant_pasajeros'] = $tasa->pax_embarcados;
                    $prodserv = $this->getProdservicios2('tasa', $data);
                    $MonedaPago = $this->TIPOS_VUELOS_NOMEDAS[2];
                    $EURO = $this->getEuro();

                    $prodservicios_extra = [];
                    if ($tasa->prodservicios_extra != null) {
                        $ext = explode(",", $tasa->prodservicios_extra);
                        foreach ($ext as $value) {
                            $ext2 = explode(":", $value);
                            $ext3[] = $ext2[1];
                            $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                        }
                        $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
                    }
                } else {
                    if ($request->tipo == "dosa") {
                        $dosa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));
                        $Aeropuerto = \App\Models\Aeropuertos::find($dosa->aeropuerto_id);
                        $data['tipo_vuelo_id'] = 2;
                        $data['nacionalidad_id'] = $dosa->tipo_llegada_id;
                        $data['aeropuerto_id'] = $dosa->aeropuerto_id;
                        $data['aeronave_id'] = $dosa->aeronave_id;
                        $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $dosa->hora_llegada));
                        $data['cant_pasajeros'] = $dosa->pax_desembarcados;
                        $prodserv = $this->getProdservicios2('dosa', $data);
                        $MonedaPago = $this->TIPOS_VUELOS_NOMEDAS[2];
                        $EURO = $this->getEuro();

                        if ($dosa->prodservicios_extra != null) {
                            $ext = explode(",", $dosa->prodservicios_extra);

                            foreach ($ext as $value) {
                                $ext2 = explode(":", $value);
                                $ext3[] = $ext2[1];
                                $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                            }

                            //dd($dosa->prodservicios_extra);
                            $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
                        }
                    } else {
                        dd("0");
                    }
                }

                $prodServs = $this->getProdservicios2("extra", $data);

                $listPrdServs = [];
                foreach ($prodServs as $value) {
                    $listPrdServs[$value['crypt_id']] = $value['full_descripcion'] . ' ' . $value['iva2'] . " (" . $value['moneda'] . ": " . muestraFloat($value['precio']) . ") (BS.: " . muestraFloat($value['bs']) . ")";
                }
                // dd($listPrdServs);
                $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
                return view('facturas.detalle', compact("cantidades", 'TAQUILLA', 'prodserv', 'MonedaPago', 'EURO', 'listPrdServs', 'prodservicios_extra', 'id', 'tipo', 'forma_pagos'));
            } else {
                $msg = "Taquilla No Configurada";
                return view('errors.general', compact('msg'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function del_prodservs($id, $serv, $tipo) {
        //dd("dfgdfg");
        if (\Request::ajax()) {

            if ($this->getPermission(null) == true) {

                $TAQUILLA = $this->getDataTaquilla();

                $VuelosG = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                $prodservs = Encryptor::decrypt($serv);
                //dd($VuelosG->prodservicios_extra);
                $array = explode(",", $VuelosG->prodservicios_extra);
                foreach ($array as $key => $value) {

                    $ext2 = explode(":", $value);

                    if ($ext2[1] == $prodservs) {
                        unset($array[$key]);
                    }
                }

                $VuelosG->prodservicios_extra = implode(",", $array);

                $VuelosG->save();
                $prodservicios_extra = [];
                if ($tipo == "tasa") {
                    $tasa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));

                    $Aeropuerto = \App\Models\Aeropuertos::find($tasa->aeropuerto_id);
                    $data['tipo_vuelo_id'] = 2;
                    $data['nacionalidad_id'] = $tasa->tipo_salida_id;
                    $data['aeropuerto_id'] = $tasa->aeropuerto_id;
                    $data['aeronave_id'] = $tasa->aeronave_id;
                    $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $tasa->hora_salida));
                    $data['cant_pasajeros'] = $tasa->pax_embarcados;
                    $prodserv = $this->getProdservicios2('tasa', $data);
                    $MonedaPago = $this->TIPOS_VUELOS_NOMEDAS[2];
                    $EURO = $this->getEuro();

                    if ($tasa->prodservicios_extra != null) {
                        $ext = explode(",", $tasa->prodservicios_extra);
                        foreach ($ext as $value) {
                            $ext2 = explode(":", $value);
                            $ext3[] = $ext2[1];
                            $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                        }

                        $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
                    }
                } else {
                    if ($tipo == "dosa") {
                        $dosa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));
                        $Aeropuerto = \App\Models\Aeropuertos::find($dosa->aeropuerto_id);
                        $data['tipo_vuelo_id'] = 2;
                        $data['nacionalidad_id'] = $dosa->tipo_llegada_id;
                        $data['aeropuerto_id'] = $dosa->aeropuerto_id;
                        $data['aeronave_id'] = $dosa->aeronave_id;
                        $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $dosa->hora_llegada));
                        $data['cant_pasajeros'] = $dosa->pax_desembarcados;
                        $prodserv = $this->getProdservicios2('dosa', $data);
                        $MonedaPago = $this->TIPOS_VUELOS_NOMEDAS[2];
                        $EURO = $this->getEuro();

                        if ($dosa->prodservicios_extra != null) {
                            $ext = explode(",", $dosa->prodservicios_extra);
                            foreach ($ext as $value) {
                                $ext2 = explode(":", $value);
                                $ext3[] = $ext2[1];
                                $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                            }
                            //dd($dosa->prodservicios_extra);
                            $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
                        }
                    } else {
                        dd("0");
                    }
                }

                $prodServs = $this->getProdservicios2("extra", $data);

                $listPrdServs = [];
                foreach ($prodServs as $value) {
                    $listPrdServs[$value['crypt_id']] = $value['full_descripcion'] . ' ' . $value['iva2'] . " (" . $value['moneda'] . ": " . muestraFloat($value['precio']) . ") (BS.: " . muestraFloat($value['bs']) . ")";
                }
                // dd($listPrdServs);
                $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
                return view('facturas.detalle', compact('cantidades', 'TAQUILLA', 'prodserv', 'MonedaPago', 'EURO', 'listPrdServs', 'prodservicios_extra', 'id', 'tipo', 'forma_pagos'));
            } else {
                $msg = "Taquilla No Configurada";
                return view('errors.general', compact('msg'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function dosas($id, Request $request) {
        if (\Request::ajax()) {
            if ($this->getPermission(null) == true) {
                $TAQUILLA = $this->getDataTaquilla();
                $dosa = \App\Models\VuelosGenerales::find(Encryptor::decrypt($id));
                $Aeropuerto = \App\Models\Aeropuertos::find($dosa->aeropuerto_id);
                $data['tipo_vuelo_id'] = 2;
                $data['nacionalidad_id'] = $dosa->tipo_llegada_id;
                $data['aeropuerto_id'] = $dosa->aeropuerto_id;
                $data['aeronave_id'] = $dosa->aeronave_id;
                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $dosa->hora_llegada));
                $data['cant_pasajeros'] = $dosa->pax_desembarcados;
                $prodserv = $this->getProdservicios2('dosa', $data);
                $MonedaPago = $this->TIPOS_VUELOS_NOMEDAS[2];
                $EURO = $this->getEuro();

                $prodservicios_extra = [];
                $cantidades = [];
                //dd($this->getProdservicios2('extra', $data, [32]));

                if ($dosa->prodservicios_extra != null) {
                    $ext = explode(",", $dosa->prodservicios_extra);

                    foreach ($ext as $value) {
                        $ext2 = explode(":", $value);
                        $ext3[] = $ext2[1];
                        $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                    }
                    $cantidades[Encryptor::encrypt($ext2[1])] = $ext2[0];
                    $prodservicios_extra = $this->getProdservicios2('extra', $data, $ext3);
                }


                $prodServs = $this->getProdservicios2("extra", $data);

                //dd();
                $listPrdServs = [];
                foreach ($prodServs as $value) {
                    $listPrdServs[$value['crypt_id']] = $value['full_descripcion'] . ' ' . $value['iva2'] . " (" . $value['moneda'] . ": " . muestraFloat($value['precio']) . ") (BS.: " . muestraFloat($value['bs']) . ")";
                }

                $tipo = "dosa";
                $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
                return view('facturas.detalle', compact('cantidades', 'TAQUILLA', 'prodserv', 'MonedaPago', 'EURO', 'listPrdServs', 'prodservicios_extra', 'id', 'tipo', 'forma_pagos'));
            } else {
                $msg = "Taquilla No Configurada";
                return view('errors.general', compact('msg'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    function invoice($id) {
        $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
        return view('facturas.invoice', compact('forma_pagos'));
    }

}
