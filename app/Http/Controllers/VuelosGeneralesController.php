<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Exports\ExportFacturas;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\VuelosGenerales;

class VuelosGeneralesController extends Controller {

    function proforma(Request $request) {
        
        
        //dd($request->all());
        
        
        $Cliente = \App\Models\Clientes::where('tipo_documento', $request->prof_type_document)->where('documento', $request->prof_document)->first();
        if ($Cliente == null) {
            $Cliente = new \App\Models\Clientes();
            $Cliente->tipo_documento = $request->prof_type_document;
            $Cliente->documento = $request->prof_document;

            $Cliente->user_id = Auth::user()->id;
            $Cliente->ip = $this->getIp();
            $Cliente->fecha_registro = now();
        }

        $Cliente->razon_social = Upper($request->prof_razon);
        $Cliente->telefono = $request->prof_phone;
        $Cliente->correo = "";
        $Cliente->direccion = $request->prof_direccion;
        $Cliente->tipo_id = 4; // general

        $Cliente->save();
        
        //dd(VuelosGenerales::find(Encryptor::decrypt($id)));
        $VuelosGenerales = VuelosGenerales::find(Encryptor::decrypt($request->prof_id));
        /*
          $data['tipo_vuelo_id'] = 2; //Vuelos General
          $data['nacionalidad_id'] = 1; // NAcional
          $data['aeropuerto_id'] = $this->TORTUGA_ID;
          $data['hora_llegada'] =  "08:00" ;
          $data['aeronave_id'] = $VuelosGenerales->aeronave_id;
          $data['plan_vuelo'] = $VuelosGenerales->plan_vuelo_id;
          $data['cant_pasajeros'] = $VuelosGenerales->pax_desembarcados;


          $Aeropuerto = \App\Models\Aeropuertos::find($this->TORTUGA_ID);
          $data['turno_id'] = 1;
          $prodserv_dosa = $this->getProdservicios2('dosa', $data);
          $data['turno_id'] = 1;
          $prodserv_tasa = $this->getProdservicios2('tasa', $data);
          $prodserv_tasa[0]['tipo'] = "TASA";
          $prodserv_tasa[0]['cant'] = "1";
         */




        $data['tipo_vuelo_id'] = 2; //Vuelos General
        $data['nacionalidad_id'] = 1; // NAcional
        $data['aeropuerto_id'] = $this->TORTUGA_ID;
        $data['hora_llegada'] = $VuelosGenerales->hora_llegada;
        $data['hora_salida'] = $VuelosGenerales->hora_salida;
        $data['aeronave_id'] = $VuelosGenerales->aeronave_id;
        $data['plan_vuelo'] = $VuelosGenerales->plan_vuelo_id;
        $data['cant_pasajeros'] = $VuelosGenerales->pax_desembarcados;
        $data['fecha_vuelo'] = $VuelosGenerales->fecha_llegada;
        $data['serv_excluir']=['2.10.3', '2.13.2', '2.10.1', '1.6.1.1'];

        $data['exento_dosa'] = $VuelosGenerales->exento_dosa;
        $data['exento_tasa'] = $VuelosGenerales->exento_tasa;
        //dd($VuelosGenerales);
        $prodserv_dosa = $this->getProdServ($data);



        return view('reportes.facturas.proforma_general', compact('prodserv_dosa', 'VuelosGenerales'));
    }

    private function getListVuelosAdmin($fecha, $request) {
        $where[] = [DB::raw("1"), 1];


        if ($request->piloto_id != null) {
            $where[] = [
                function ($q) use ($request) {
                    $q->where(DB::raw("(piloto_llegada_id = " . Encryptor::decrypt($request->piloto_id) . " OR piloto_salida_id =" . Encryptor::decrypt($request->piloto_id) . ")"), TRUE);
                }
            ];
        }

        if ($request->matricula_id != null) {
            $where[] = ["aeronave_id", Encryptor::decrypt($request->matricula_id)];
        }

        if ($request->aeropuerto_id != null) {
            $where[] = ["aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id)];
        }

        $data = VuelosGenerales::whereDate("fecha_registro", $fecha)
                ->where($where)
                ->orderBy("numero_operacion", "ASC")
                ->get();
        $data = Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function ($row) {
                            $actionBtn = "";
                            $actionBtn .= '<a onClick="editOperationVuelo(\'' . $row->crypt_id . '\')" title="Actualizar" href="javascript:void(0)" class=" btn btn-success btn-sm mr-2"> <li class="fa fa-edit"></li> </a> ';
                            $actionBtn .= '<a onClick="getDataVuelo(\'' . $row->crypt_id . '\')" title="Ver Detalle" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-eye"></li> </a> ';



                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true)
                ->original['data'];
        return $data;
    }

    private function getListVuelos($route_id, $fecha = null) {

        $fecha = $fecha == null ? date('d/m/Y') : $fecha;
        $fecha = $this->saveDate($fecha);

        $data = VuelosGenerales::where("aeropuerto_id", Auth::user()->aeropuerto_id)
                ->where(function ($q) {
                    $q->where(DB::raw("proc_vuelos_generales.fecha_registro::date = '" . date('Y-m-d') . "' OR proc_vuelos_generales.finalizado IS FALSE"), TRUE);
                })
                ->where("finalizado", false)
                ->where("activo", true)
                /*
                  ->where(function ($where) use ($fecha){
                  $where->where(DB::raw("proc_vuelos_generales.fecha_llegada = '" . $fecha . "' OR proc_vuelos_generales.fecha_salida ='".$fecha."'"), TRUE);
                  })
                 */
                ->orderBy("numero_operacion", "ASC")
                ->get();
        $data = Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('fecha2', function ($row) {
                            $actionBtn = $row->fecha_llegada == null ? showDate($row->fecha_salida) : showDate($row->fecha_llegada);


                            return $actionBtn;
                        })
                        ->addColumn('action', function ($row) use ($route_id) {
                            $actionBtn = "";

                            if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="editOperation(\'' . $row->crypt_id . '\')" title="Editar" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-edit"></li> </a> ';
                            }
                            if (in_array("add_prodserv", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="addProdserv(\'' . $row->crypt_id . '\')" title="Agregar Servicios" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-cogs"></li> </a> ';
                            }
                            if (in_array("add_position", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= ' <a onClick="addPosition(\'' . $row->crypt_id . '\')" title="Agregar Posición" href="javascript:void(0)" class=" btn btn-primary btn-sm"> <li class="fa fa-search-location"></li> </a> ';
                            }

                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true)
                ->original['data'];
        return $data;
    }

    private function getListVuelosFacturar($route_id) {



        $data = VuelosGenerales::where("aeropuerto_id", Auth::user()->aeropuerto_id)
                ->where("finalizado", false)
                ->where("activo", true)
                ->orderBy("numero_operacion", "DESC")
                ->get();
        $data = Datatables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function ($row) use ($route_id) {
                            $actionBtn = "";
                            if ($row->aeropuerto_id != $this->TORTUGA_ID) {
                                if ($row->destino_id != null) {
                                    if ($row->factura_tasa == null) {

                                        if ($row->exento_tasa == false) {
                                            //if (in_array("add_tasa", Auth::user()->getActions()[$route_id])) {
                                            $actionBtn .= '<a onClick="setTasa(\'' . $row->crypt_id . '\')" title="Tasa" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-plane-departure"></li> </a> ';
                                            //}
                                        } else {
                                            $actionBtn .= 'Exenta Tasa ';
                                        }
                                    }
                                }
                                if ($row->procedencia_id != null) {
                                    if ($row->factura_dosa == null) {
                                        if ($row->exento_dosa == false) {

                                            //if (in_array("add_dosa", Auth::user()->getActions()[$route_id])) {
                                            $actionBtn .= '<a onClick="setDosa(\'' . $row->crypt_id . '\')" title="Dosa" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-plane-arrival"></li> </a> ';
                                            //}
                                        } else {
                                            $actionBtn .= 'Exenta Dosa ';
                                        }
                                    }
                                }
                            } else {
                                $actionBtn .= '<a onClick="setTortuga(\'' . $row->crypt_id . '\')" title="Facturar " href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fa fa-plane"></li> </a> ';
                            }


                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true)
                ->original['data'];
        return $data;
    }

    public function listado_facturas(Request $request) {
        if ($request->isMethod('get') && \Request::ajax()) {

            $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
            $Tipos = ["TASAS" => "TASAS", "DOSAS" => "DOSAS"];


            return view('vuelos_generales.listado_facturas', compact('Aeropuertos', 'Tipos'));
        } else {
            if ($request->isMethod('post') && !\Request::ajax()) {
                return Excel::download(new ExportFacturas(), 'facturas.xlsx');
            } else {
                return Redirect::to('/dashboard');
            }
        }
    }

    public function refresh_data($route_id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $data = $this->getListVuelos(Encryptor::decrypt($route_id));
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $data;

                return $result;
            } else {
                return Redirect::to('/dashboard');
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_arrive(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());
                $Validator = \Validator::make(
                                $request->all(), [
                            'matricula_id' => 'required',
                            'piloto_id' => 'required',
                            'hora_llegada' => 'required',
                            'tipo_llegada_id' => 'required',
                            'procedencia_id' => 'required',
                            'pax_desembarcados' => 'required',
                                ], [
                            'matricula_id.required' => __('La Aeronave es Requerida'),
                            'piloto_id.required' => __('El Piloto es Requerido'),
                            'hora_llegada.required' => __('La Hora de Llegada es Requerida'),
                            'tipo_llegada_id.required' => __('El Tipo de LLegada es Requerido'),
                            'procedencia_id.required' => __('La Procedencia es Requerida'),
                            'pax_desembarcados.required' => __('Los Pasajeros Desembarcados Son Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {


                    $VuelosGeneralesNumControl = \App\Models\VuelosGenerales::where("activo", 1)
                            ->where("aeropuerto_id", Auth::user()->aeropuerto_id)
                            ->orderBy("id", "DESC")
                            ->first();
                    $VuelosGeneralesNumControl = $VuelosGeneralesNumControl == null ? 1 : $VuelosGeneralesNumControl->numero_operacion + 1;

                    $VuelosGenerales = new \App\Models\VuelosGenerales();

                    $VuelosGenerales->fecha_registro = now();
                    $VuelosGenerales->fecha_llegada = date('Y-m-d');
                    $VuelosGenerales->user_id = Auth::user()->id;
                    $VuelosGenerales->aeropuerto_id = Auth::user()->aeropuerto_id;
                    $VuelosGenerales->numero_operacion = $VuelosGeneralesNumControl;

                    $VuelosGenerales->aeronave_id = Encryptor::decrypt($request->matricula_id);
                    $VuelosGenerales->piloto_llegada_id = Encryptor::decrypt($request->piloto_id);
                    $VuelosGenerales->hora_llegada = $request->hora_llegada;
                    $VuelosGenerales->tipo_llegada_id = $request->tipo_llegada_id;

                    $VuelosGenerales->procedencia_id = Encryptor::decrypt($request->procedencia_id);
                    $VuelosGenerales->pax_desembarcados = $request->pax_desembarcados;
                    $VuelosGenerales->piloto_salida_id = Encryptor::decrypt($request->piloto_id);
                    //$VuelosGenerales->fecha_salida = 0;
                    //$VuelosGenerales->hora_salida = $request->nombre;
                    //$VuelosGenerales->tipo_salida_id = $request->matricula;
                    //$VuelosGenerales->destino_id = $request->peso_maximo;
                    //$VuelosGenerales->pax_embarcados = 0;
                    //$VuelosGenerales->observaciones_operaciones = $request->nombre;
                    //$VuelosGenerales->observaciones_facturacion = $request->matricula;
                    //$VuelosGenerales->activo = $request->peso_maximo;
                    //$VuelosGenerales->prodservicios_extra = 0;


                    $VuelosGenerales->ip = $this->getIp();
                    //$VuelosGenerales->finalizado = now();

                    $VuelosGenerales->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $VuelosGenerales->toArray();
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_leave(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());
                $Validator = \Validator::make(
                                $request->all(), [
                            'matricula_id' => 'required',
                            'piloto_id' => 'required',
                            'hora_salida' => 'required',
                            'tipo_salida_id' => 'required',
                            'destino_id' => 'required',
                            'pax_embarcados' => 'required',
                                ], [
                            'matricula_id.required' => __('La Aeronave es Requerida'),
                            'piloto_id.required' => __('El Piloto es Requerido'),
                            'hora_salida.required' => __('La Hora de Salida es Requerida'),
                            'tipo_salida_id.required' => __('El Tipo de Salida es Requerido'),
                            'destino_id.required' => __('El Destino es Requerido'),
                            'pax_embarcados.required' => __('Los Pasajeros Embarcados Son Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {


                    $VuelosGeneralesNumControl = \App\Models\VuelosGenerales::where("activo", 1)
                            ->where("aeropuerto_id", Auth::user()->aeropuerto_id)
                            ->orderBy("id", "DESC")
                            ->first();
                    $VuelosGeneralesNumControl = $VuelosGeneralesNumControl == null ? 1 : $VuelosGeneralesNumControl->numero_operacion + 1;

                    $VuelosGenerales = new \App\Models\VuelosGenerales();

                    $VuelosGenerales->fecha_registro = now();
                    $VuelosGenerales->fecha_salida = date('Y-m-d');
                    $VuelosGenerales->user_id = Auth::user()->id;
                    $VuelosGenerales->aeropuerto_id = Auth::user()->aeropuerto_id;
                    $VuelosGenerales->numero_operacion = $VuelosGeneralesNumControl;

                    $VuelosGenerales->piloto_llegada_id = Encryptor::decrypt($request->piloto_id);

                    $VuelosGenerales->aeronave_id = Encryptor::decrypt($request->matricula_id);
                    $VuelosGenerales->piloto_salida_id = Encryptor::decrypt($request->piloto_id);
                    $VuelosGenerales->hora_salida = $request->hora_salida;
                    $VuelosGenerales->tipo_salida_id = $request->tipo_salida_id;

                    $VuelosGenerales->destino_id = Encryptor::decrypt($request->destino_id);
                    $VuelosGenerales->pax_embarcados = $request->pax_embarcados;

                    //$VuelosGenerales->fecha_salida = 0;
                    //$VuelosGenerales->hora_salida = $request->nombre;
                    //$VuelosGenerales->tipo_salida_id = $request->matricula;
                    //$VuelosGenerales->destino_id = $request->peso_maximo;
                    //$VuelosGenerales->pax_embarcados = 0;
                    //$VuelosGenerales->observaciones_operaciones = $request->nombre;
                    //$VuelosGenerales->observaciones_facturacion = $request->matricula;
                    //$VuelosGenerales->activo = $request->peso_maximo;
                    //$VuelosGenerales->prodservicios_extra = 0;


                    $VuelosGenerales->ip = $this->getIp();
                    //$VuelosGenerales->finalizado = now();

                    $VuelosGenerales->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $VuelosGenerales->toArray();
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_nave(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $values = $request->all();
                $values['matricula'] = Upper($values['matricula']);
                $Validator = \Validator::make(
                                $values, [
                            'peso_maximo' => 'required',
                            'matricula' => 'required|unique:maest_aeronaves,matricula',
                            'nombre' => 'required',
                                ], [
                            'peso_maximo.required' => __('El Peso Maximo es Requerido'),
                            'matricula.required' => __('La Matrícula es Requerida'),
                            'matricula.unique' => __('La Matrícula ya Esta Registrada'),
                            'nombre.required' => __('El Modelo es Requerido'),
                                ]
                );
                //dd("sss");
                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = \App\Models\Aeronaves::where("matricula", $request->matricula)->count();

                    if ($Aeronaves == 0) {
                        $Aeronaves = new \App\Models\Aeronaves();

                        $Aeronaves->nombre = Upper($request->nombre);
                        $Aeronaves->matricula = Upper($request->matricula);
                        $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                        $Aeronaves->maximo_pasajeros = 0;

                        $Aeronaves->user_id = Auth::user()->id;
                        $Aeronaves->ip = $this->getIp();
                        $Aeronaves->fecha_registro = now();

                        $Aeronaves->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = array("id" => $Aeronaves->crypt_id, 'full_nombre' => $Aeronaves->getFullNombreAttribute());
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_piloto(Request $request) {
        // dd("dddd");
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'name_user' => 'required',
                            'surname_user' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'name_user.required' => __('El nombre es Requerido'),
                            'surname_user.required' => __('El Apellido es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Pilotos = \App\Models\Pilotos::where("documento", $request->document)->count();

                    if ($Pilotos == 0) {
                        $Pilotos = new \App\Models\Pilotos();
                        $Pilotos->tipo_documento = $request->type_document;
                        $Pilotos->documento = $request->document;
                        $Pilotos->nombres = Upper($request->name_user);
                        $Pilotos->apellidos = Upper($request->surname_user);
                        $Pilotos->telefono = $request->phone;
                        $Pilotos->correo = $request->email;

                        /*
                          $Aeronaves->user_id = Auth::user()->id;
                          $Aeronaves->ip = $this->getIp();
                          $Aeronaves->fecha_registro = now();
                         */
                        $Pilotos->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = array("id" => $Pilotos->crypt_id, 'full_nombre' => $Pilotos->full_name);
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_prodserv($id, Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Operacion = VuelosGenerales::find(Encryptor::decrypt($id));

                $prodServs = [];
                if (is_array($request->prodservs_id)) {
                    foreach ($request->prodservs_id as $value) {
                        $prodServs[] = "1:" . Encryptor::decrypt($value);
                    }
                }
                $Operacion->prodservicios_extra = implode(",", $prodServs);
                $Operacion->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {


                $Operacion = VuelosGenerales::find(Encryptor::decrypt($id));
                $data = [];
                $data['tipo_vuelo_id'] = 2;
                //$Operacion->tipo_llegada_id == null ? $Operacion->tipo_salida_id : $Operacion->tipo_llegada_id;
                $data['cant_pasajeros'] = $Operacion->tipo_llegada_id == null ? ($Operacion->pax_embarcados == null ? 0 : $Operacion->pax_embarcados) : $Operacion->pax_desembarcados;
                $data['nacionalidad_id'] = $data['tipo_vuelo_id'];
                $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                $data['aeronave_id'] = $Operacion->aeronave_id;
                $prodServs = $this->getProdservicios2("extra", $data);
                //dd($prodServs);
                //dd();
                $listPrdServs = [];
                $selected = [];
                foreach ($prodServs as $value) {
                    $listPrdServs[$value['crypt_id']] = $value['full_descripcion'] . ' ' . $value['iva2'] . " (" . $value['moneda'] . ": " . muestraFloat($value['precio']) . ") (BS.: " . muestraFloat($value['bs']) . ")";

                    if (in_array(Encryptor::decrypt($value['crypt_id']), explode(",", $Operacion->prodservicios_extra))) {
                        $selected[$value['crypt_id']] = ["selected" => "selected"];
                    }
                }

                //dd($prodServs);



                return view('vuelos_generales.prodservs', compact('Operacion', 'prodServs', 'listPrdServs', 'selected'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function online_reg($id = null, Request $request) {
        if (\Request::ajax()) {
            if ($id != null) {

                $VuelosGeneralesNumControl = \App\Models\VuelosGenerales::where("activo", 1)
                        ->where("aeropuerto_id", Auth::user()->aeropuerto_id)
                        ->orderBy("id", "DESC")
                        ->first();
                $VuelosGeneralesNumControl = $VuelosGeneralesNumControl == null ? 1 : $VuelosGeneralesNumControl->numero_operacion + 1;

                $Online = \App\Models\RegistroOnline::find(Encryptor::decrypt($id));
                //dd($VuelosGeneralesNumControl);
                $VuelosGenerales = new \App\Models\VuelosGenerales();
                $VuelosGenerales->numero_operacion = $VuelosGeneralesNumControl;
                $VuelosGenerales->user_id = Auth::user()->id;
                $VuelosGenerales->aeropuerto_id = Auth::user()->aeropuerto_id;
                $VuelosGenerales->fecha_registro = now();
                $VuelosGenerales->aeronave_id = $Online->aeronave_id;
                $VuelosGenerales->piloto_salida_id = $Online->piloto_id;
                $VuelosGenerales->piloto_llegada_id = $Online->piloto_id;
                if ($Online->origen_id == Auth::user()->aeropuerto_id) {//TASA
                    $VuelosGenerales->fecha_salida = $Online->fecha;

                    $VuelosGenerales->hora_salida = $Online->hora;
                    $VuelosGenerales->tipo_salida_id = $Online->tipo_id;

                    $VuelosGenerales->destino_id = $Online->destino_id;
                    $VuelosGenerales->pax_embarcados = $Online->pasajeros;

                    $Online->procesado_origen = true;
                } else { // DOSA
                    $VuelosGenerales->fecha_llegada = $Online->fecha;

                    $VuelosGenerales->hora_llegada = $Online->hora;
                    $VuelosGenerales->tipo_llegada_id = $Online->tipo_id;

                    $VuelosGenerales->procedencia_id = $Online->origen_id;
                    $VuelosGenerales->pax_desembarcados = $Online->pasajeros;

                    $Online->procesado_destino = true;
                }

                $Online->save();

                $VuelosGenerales->ip = $this->getIp();
                $VuelosGenerales->prodservicios_extra = $Online->prodservicios_extra;

                $VuelosGenerales->save();

                return true;
            } else {
                $where = function ($cond) {
                    $cond->orWhere('origen_id', Auth::user()->aeropuerto->id);
                    $cond->orWhere('destino_id', Auth::user()->aeropuerto->id);
                };

                $RegistroOnline = \App\Models\RegistroOnline::
                        where($where)
                        //where("fecha", ">=", date("Y-m-d"))
                        ->get();

                \App\Models\RegistroOnline::where($where)->where("fecha", date("Y-m-d"))->update(["visto_origen" => true, "visto_destino" => true]);

                $RegistroOnline = Datatables::of($RegistroOnline)
                        ->addIndexColumn()
                        ->addColumn('tipo', function ($row) {
                            if ($row->origen_id == Auth::user()->aeropuerto_id) {
                                $actionBtn = "TASA";
                            } else {
                                if ($row->destino_id == Auth::user()->aeropuerto_id) {
                                    $actionBtn = "DOSA";
                                } else {
                                    $actionBtn = "S/I";
                                }
                            }


                            return $actionBtn;
                        })
                        ->addColumn('action', function ($row) {
                            $actionBtn = "";
                            if ($row->origen_id == Auth::user()->aeropuerto_id) { // TASA
                                if ($row->procesado_origen == false) {
                                    $actionBtn .= '<a onClick="getRegOnline(\'' . $row->crypt_id . '\')" title="Procesar" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fas fa-plus-circle"></li> </a> ';
                                }
                            } else { //DOSA
                                if ($row->procesado_destino == false) {
                                    $actionBtn .= '<a onClick="getRegOnline(\'' . $row->crypt_id . '\')" title="Procesar" href="javascript:void(0)" class=" btn btn-primary btn-sm mr-2"> <li class="fas fa-plus-circle"></li> </a> ';
                                }
                            }

                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                $data1 = $RegistroOnline->original['data'];
                return view('vuelos_generales.online_reg', compact('data1'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_position($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());

                $Operacion = VuelosGenerales::find(Encryptor::decrypt($id));
                $Operacion->puesto_id = Encryptor::decrypt($request->puesto);
                $Operacion->save();

                $Puestos = \App\Models\Puestos::where("aeronave_id", $Operacion->aeronave_id)->update(["aeronave_id" => null]);

                $Puesto = \App\Models\Puestos::find(Encryptor::decrypt($request->puesto));
                $Puesto->aeronave_id = $Operacion->aeronave_id;
                $Puesto->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $Operacion->save();

                return $result;
            } else {



                $Operacion = VuelosGenerales::find(Encryptor::decrypt($id));
                $Aeropuerto = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);
                // dd($Ubicaciones->ubicaciones);
                if ($Aeropuerto->ubicaciones == null) {
                    $msg = __("EL Aeropuerto no Posee Asignado Ninguna Ubicación");
                    return view('errors.general', compact('msg'));
                }
                //dd($Ubicaciones->toArray());
                return view('vuelos_generales.add_position', compact('Operacion', 'Aeropuerto'));
                //dd($Operacion);
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Operacion = VuelosGenerales::find(Encryptor::decrypt($id));
                $Operacion->aeronave_id = Encryptor::decrypt($request->matricula_id);
                $Operacion->observaciones_operaciones = $request->observacion;
                if ($request->tipo == 'dosa') {

                    $Operacion->piloto_llegada_id = Encryptor::decrypt($request->piloto_llegada_id);
                    $Operacion->hora_llegada = $request->hora_llegada;
                    $Operacion->tipo_llegada_id = $request->tipo_llegada_id;

                    $Operacion->procedencia_id = Encryptor::decrypt($request->procedencia_id);
                    $Operacion->pax_desembarcados = $request->pax_desembarcados;

                    $Operacion->piloto_salida_id = Encryptor::decrypt($request->piloto_llegada_id);
                } else {
                    if ($request->tipo == 'tasa') {

                        $Operacion->piloto_salida_id = Encryptor::decrypt($request->piloto_salida_id);
                        $Operacion->hora_salida = $request->hora_salida;
                        $Operacion->tipo_salida_id = $request->tipo_salida_id;

                        $Operacion->destino_id = Encryptor::decrypt($request->destino_id);
                        $Operacion->pax_embarcados = $request->pax_embarcados;

                        $Operacion->piloto_llegada_id = Encryptor::decrypt($request->piloto_salida_id);
                    } else {
                        return Redirect::to('/dashboard');
                    }
                }
                $Operacion->save();
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $Operacion->save();

                return $result;
            } else {

                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "!=", Auth::user()->id)->get()->pluck("full_nombre", "crypt_id");
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
                
                $Operacion = VuelosGenerales::find(Encryptor::decrypt($id));
                return view('vuelos_generales.edit', compact('Operacion', 'Pilotos', 'Aeropuertos', 'Aeronaves'));
                //dd($Operacion);
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function registro_aviacion_general_get($id) {

        $VuelosGenerales = VuelosGenerales::find(Encryptor::decrypt($id));
        return view('vuelos_generales.see', compact('VuelosGenerales'));
    }

    public function registro_aviacion_general_update($id) {

        $VuelosGenerales = VuelosGenerales::find(Encryptor::decrypt($id));
        return view('vuelos_generales.update', compact('VuelosGenerales'));
    }

    public function registro_aviacion_general(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $VuelosGenerales = VuelosGenerales::find(Encryptor::decrypt($request->id));
                $VuelosGenerales->exento_tasa = $request->exento_tasa;
                $VuelosGenerales->exento_dosa = $request->exento_dosa;
                $VuelosGenerales->save();
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = "";
                return $result;
            } else {
                if ($request->isMethod('put')) {


                    $fecha = ($request->fecha == null ? date("Y-m-d") : $this->saveDate($request->fecha));

                    $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("pertenece_baer", true)->get()->pluck("full_nombre", "crypt_id");
                    $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                    $piloto = $request->piloto_id;
                    $aeropuerto = $request->aeropuerto_id;
                    $aeronave = $request->matricula_id;
                    //dd($fecha);

                    $data = $this->getListVuelosAdmin($fecha, $request);
                    return view('vuelos_generales.admin', compact('Aeropuertos', 'Aeronaves', 'Pilotos', 'data', 'fecha', 'piloto', 'aeropuerto', 'aeronave'));
                } else {

                    $fecha = date("Y-m-d");

                    $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("pertenece_baer", true)->get()->pluck("full_nombre", "crypt_id");
                    $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");


                    $piloto = null;
                    $aeropuerto = null;
                    $aeronave = null;
                    //dd($fecha);

                    $data = $this->getListVuelosAdmin($fecha, $request);




                    return view('vuelos_generales.admin', compact('Aeropuertos', 'Aeronaves', 'Pilotos', 'data', 'fecha', 'piloto', 'aeropuerto', 'aeronave'));
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function index(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                return Redirect::to('/dashboard');
            } else {

                if (Auth::user()->aeropuerto_id != null) {

                    $name_route = \Request::route()->getName();
                    $name_route = explode(".", $name_route);
                    $name_route = $name_route[0];
                    $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;

                    $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")->where("id", "!=", Auth::user()->id)->get()->pluck("full_nombre", "crypt_id");
                    $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                    $data = $this->getListVuelos($route_id);
                    $crypt_route_id = Encryptor::encrypt($route_id);

                    return view('vuelos_generales.index', compact('Aeropuertos', 'Aeronaves', 'Pilotos', 'data', 'route_id', 'crypt_route_id'));
                } else {
                    $msg = __("EL Usuario no Posee Asignado Ningún Aeropuerto");
                    return view('errors.general', compact('msg'));
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function facturas(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $ip = $this->getIp();
                if ($request->cliente_id == null) {

                    $Cliente = \App\Models\Clientes::where('documento', $request->document)->first();

                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                    }



                    $Cliente->tipo_documento = $request->type_document;
                    $Cliente->documento = $request->document;

                    //$Cliente->user_id = Auth::user()->id;
                    $Cliente->user_id = 1;
                    $Cliente->fecha_registro = now();
                    $Cliente->ip = $ip;
                } else {
                    $Cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id));
                }

                $Cliente->direccion = $request->direccion;
                $Cliente->tipo_id = 3;

                $Cliente->razon_social = Upper($request->razon);
                $Cliente->telefono = $request->phone;
                $Cliente->correo = (isset($request->correo) ?: "$request->correo");

                $Cliente->direccion = $request->direccion;
                $Cliente->abreviatura = "";
                $Cliente->save();

                $DOLAR = $this->getDolar();
                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $VuelosGenerales = \App\Models\VuelosGenerales::find(Encryptor::decrypt($request->crypt));
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];

                $Factura = new \App\Models\Facturas();
                $Factura->fecha_factura = now();
                $Factura->cliente_id = $Cliente->id;
                $Factura->solicitud_id = $VuelosGenerales->id;
                $Factura->moneda_aplicada = $MonedaPago;
                $Factura->monto_moneda_aplicada = ${$MonedaPago};
                $Factura->formato_factura = 'formato_tasa_general';
                $Factura->save();

                $Origen = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);
                $data_tasa = $VuelosGenerales->toArray();
                $data_tasa['tipo_vuelo_id'] = 1;
                $data_tasa['placa'] = $data_tasa['aeronave']['matricula'];
                $data_tasa['cant_pasajeros'] = $data_tasa['cantidad_pasajeros'];

                //$data_tasa['origen_id'] = Encryptor::encrypt(Auth::user()->aeropuerto_id);
                $data_tasa['nacionalidad_id'] = 1;
                $data_tasa['turno_id'] = 1;

                $tasa = $this->getProdservicios("tasa", $data_tasa);
                dd($tasa);
                foreach ($tasa as $value) {
                    $FacturaDetalle = new \App\Models\FacturasDetalle();
                    $FacturaDetalle->factura_id = $Factura->id;
                    $FacturaDetalle->codigo = $value['codigo'];
                    $FacturaDetalle->descripcion = $value['descripcion'];
                    $FacturaDetalle->precio = $value['bs'];
                    $FacturaDetalle->precio2 = $value['precio'];
                    $FacturaDetalle->iva = $value['iva'];
                    $FacturaDetalle->save();
                }

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $Factura->crypt_id;
                return $result;
            } else {

                if (Auth::user()->aeropuerto_id != null) {
                    $name_route = \Request::route()->getName();
                    $name_route = explode(".", $name_route);
                    $name_route = $name_route[0];
                    $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;

                    $data1 = $this->getListVuelosFacturar($route_id);
                    // dd( Auth::user()->getActions());
                    return view('vuelos_generales.facturas', compact('data1', 'route_id'));
                } else {
                    $msg = __("EL Usuario no Posee Asignado Ningún Aeropuerto");
                    return view('errors.general', compact('msg'));
                }


                /*
                  $where = function ($cond) {
                  $cond->orWhere('origen_id', Auth::user()->aeropuerto->id);
                  $cond->orWhere('destino_id', Auth::user()->aeropuerto->id);
                  };
                  $registros = \App\Models\VuelosGenerales::where($where)
                  ->whereDate("fecha_registro", date("Y-m-d"))
                  ->where("activo", 1)
                  ->get();
                  $data_active = Datatables::of($registros)
                  ->addIndexColumn()
                  ->make(true);

                  $data1 = $data_active->original['data'];
                  $name_route = \Request::route()->getName();
                  $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
                  return view('vuelos_generales.facturas', compact('data1', 'route_id'));
                 */
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function lista(Request $request) {
        if (\Request::ajax()) {
            if ($this->getPermission($request->header('segurityHashPana'))) {
                $where = function ($cond) {
                    $cond->orWhere('origen_id', Auth::user()->aeropuerto->id);
                    $cond->orWhere('destino_id', Auth::user()->aeropuerto->id);
                };
                $registros = \App\Models\VuelosGenerales::where($where)
                        ->whereDate("fecha_registro", date("Y-m-d"))
                        ->where("activo", 1)
                        ->get();
                $data_active = Datatables::of($registros)
                        ->addIndexColumn()
                        ->make(true);

                $data1 = $data_active->original['data'];
                $name_route = \Request::route()->getName();
                $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
                return view('vuelos_generales.lista', compact('data1', 'route_id'));
            } else {
                $msg = "Error en la Seguridad de la Taquilla, Por Favor llame al Administrador";
                return view('errors.general', compact('msg'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document2' => 'required',
                            'document2' => 'required',
                            'piloto' => 'required',
                            'nacionalidad_id' => 'required',
                            'fecha' => 'required',
                            'hora_inicio' => 'required',
                            'origen_id' => 'required',
                            'destino_id' => 'required',
                            'cant_pasajeros' => 'required',
                            'placa' => 'required'
                                ], [
                            'type_document2.required' => __('Debe Indicar El Documento del Piloto'),
                            'document2.required' => __('Debe Indicar El Documento del Piloto'),
                            'piloto.required' => __('El Nombre del Piloto es Requerido'),
                            'nacionalidad_id.required' => __('Debe Indicar El Tipo de Vuelo (Nacional o Internacional)'),
                            'fecha.required' => __('Debe Indicar la Fecha'),
                            'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                            'origen_id.required' => __('Debe Indicar el Origen del Vuelo'),
                            'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                            'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros '),
                            'placa.required' => __('Debe Indicar la Matricula de la Aeronave')
                                ]
                );
                if ($Validator->fails()) {

                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    //Pilotos
                    if ($request->piloto_id == null) {
                        $Pilotos = \App\Models\Pilotos::where('documento', $request->document2)->first();
                        if ($Pilotos == null) {
                            $Pilotos = new \App\Models\Pilotos();
                        }

                        $Pilotos->tipo_documento = $request->type_document2;
                        $Pilotos->documento = $request->document2;

                        // $Cliente->correo = "";
                        //$Cliente->user_id = Auth::user()->id;
                    } else {
                        $Pilotos = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                    }
                    //dd($request->piloto_id);
                    $Pilotos->nombres = Upper($request->piloto);
                    $Pilotos->apellidos = "";
                    $Pilotos->telefono = "";
                    $Pilotos->correo = "";
                    $Pilotos->save();

                    /*                     * ***** AERONAVES******* */
                    $Aeronaves = \App\Models\Aeronaves::where('matricula', $request->placa)->first();

                    $VuelosGenerales = new \App\Models\VuelosGenerales();

                    $DOLAR = $this->getDolar();
                    $EURO = $this->getEuro();
                    $PETRO = $this->getPetro();

                    $VuelosGenerales->piloto_id = $Pilotos->id;
                    $VuelosGenerales->tipo_vuelo_id = $request->nacionalidad_id;
                    $VuelosGenerales->fecha_vuelo = date('Y-m-d');
                    $VuelosGenerales->hora_vuelo = $request->hora_inicio;
                    $VuelosGenerales->origen_id = Encryptor::decrypt($request->origen_id);
                    $VuelosGenerales->destino_id = Encryptor::decrypt($request->destino_id);
                    $VuelosGenerales->aeronave_id = $Aeronaves->id;
                    $VuelosGenerales->cantidad_pasajeros = $request->cant_pasajeros;
                    $VuelosGenerales->tipo_registro_id = 1; // SISTEMA
                    $VuelosGenerales->valor_dolar = $DOLAR;
                    $VuelosGenerales->valor_euro = $EURO;
                    $VuelosGenerales->valor_petro = $PETRO;
                    $VuelosGenerales->observaciones = $request->observacion == null ? "" : $request->observacion;
                    $VuelosGenerales->prodservicios_extra = "";

                    $VuelosGenerales->fecha_registro = now();
                    $VuelosGenerales->user_id = Auth::user()->id;
                    $VuelosGenerales->ip = $this->getIp();

                    $VuelosGenerales->save();

                    $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));
                    $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));

                    $data_tasa = $request->all();
                    $data_tasa['tipo_vuelo_id'] = 2;
                    $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));
                    $tasa = $this->getProdservicios("tasa", $data_tasa);

                    $data_dosa = $request->all();
                    $data_dosa['tipo_vuelo_id'] = -2;
                    $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));
                    $dosa = $this->getProdservicios("dosa", $data_dosa);

                    $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];
                    $Reservaciones = $VuelosGenerales;

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');

                    //return PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
                    // dd($dosa);
                }
                return $result;
            } else {
                if (Auth::user()->aeropuerto_id != null) {

                    $O = \App\Models\Aeropuertos::where("id", Auth::user()->aeropuerto_id)->where("activo", 1)->get();
                    foreach ($O as $value) {
                        $ORIGEN[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
                    }
                } else {
                    $O = \App\Models\Aeropuertos::where("activo", 1)->get();
                    foreach ($O as $value) {
                        $ORIGEN[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
                    }
                }


                $D = \App\Models\Aeropuertos::where("activo", 1)->get();
                foreach ($D as $value) {
                    $DESTINO [$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
                }



                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();
                return view('vuelos_generales.add', compact('ORIGEN', 'DESTINO', 'EURO', 'PETRO'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function tasas(Request $request) {
        if (\Request::ajax()) {
            if ($this->getPermission($request->header('segurityHashPana'))) {
                if ($request->isMethod('post')) {
                    //dd($request->all());
                    //Pilotos
                    if ($request->piloto_id == null) {
                        $Pilotos = \App\Models\Pilotos::where('documento', $request->document2)->first();
                        if ($Pilotos == null) {
                            $Pilotos = new \App\Models\Pilotos();
                        }

                        $Pilotos->tipo_documento = $request->type_document2;
                        $Pilotos->documento = $request->document2;

                        // $Cliente->correo = "";
                        //$Cliente->user_id = Auth::user()->id;
                    } else {
                        $Pilotos = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                    }
                    //dd($request->piloto_id);
                    $Pilotos->nombres = Upper($request->piloto);
                    $Pilotos->apellidos = "";
                    $Pilotos->telefono = "";
                    $Pilotos->correo = "";
                    $Pilotos->save();

                    /*                     * ***** AERONAVES******* */
                    $Aeronaves = \App\Models\Aeronaves::where('matricula', $request->placa)->first();

                    $prodservExtra = array();
                    $extra = array();
                    if (is_array($request->prodservicios)) {
                        foreach ($request->prodservicios as $value) {
                            $prodservExtra[] = Encryptor::decrypt($value);
                        }
                    }



                    $VuelosGenerales = new \App\Models\VuelosGenerales();

                    $DOLAR = $this->getDolar();
                    $EURO = $this->getEuro();
                    $PETRO = $this->getPetro();

                    $VuelosGenerales->piloto_id = $Pilotos->id;
                    $VuelosGenerales->tipo_vuelo_id = 1;
                    $VuelosGenerales->fecha_vuelo = date('Y-m-d');
                    $VuelosGenerales->hora_vuelo = $request->hora_inicio;
                    $VuelosGenerales->origen_id = Auth::user()->aeropuerto_id;
                    $VuelosGenerales->destino_id = Encryptor::decrypt($request->destino_id);
                    $VuelosGenerales->aeronave_id = $Aeronaves->id;
                    $VuelosGenerales->cantidad_pasajeros = $request->cant_pasajeros;
                    $VuelosGenerales->tipo_registro_id = 2; // ONLINE
                    $VuelosGenerales->valor_dolar = $DOLAR;
                    $VuelosGenerales->valor_euro = $EURO;
                    $VuelosGenerales->valor_petro = $PETRO;
                    $VuelosGenerales->observaciones = $request->observacion == null ? "" : $request->observacion;
                    $VuelosGenerales->prodservicios_extra = implode(",", $prodservExtra);

                    $VuelosGenerales->fecha_registro = now();
                    $VuelosGenerales->ip = $this->getIp();

                    $VuelosGenerales->save();

                    /*
                      $subject = "Solicitud de Permiso de Vuelo Registrado ";
                      if ($request->correo != null) {
                      $for = $request->correo;
                      $datos = [];
                      $pdf = PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'));
                      Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                      //$msj->from("mrivero52.correo@gmail.com", "Registro");
                      $msj->subject($subject);
                      $msj->to($for);
                      $msj->attachData($pdf->output(), "proforma.pdf");
                      });
                      }

                     */
                    $ip = $this->getIp();
                    $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];
                    $Origen = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);

                    $data_tasa = $request->all();
                    $data_tasa['tipo_vuelo_id'] = 2;
                    $data_tasa['origen_id'] = Encryptor::encrypt(Auth::user()->aeropuerto_id);
                    $data_tasa['nacionalidad_id'] = 1;
                    $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                    $tasa = $this->getProdservicios("tasa", $data_tasa);

                    $prodservExtra = array();
                    $extra = array();
                    if (is_array($request->prodservicios)) {
                        foreach ($request->prodservicios as $value) {
                            $prodservExtra[] = Encryptor::decrypt($value);
                        }
                    }
                    if (count($prodservExtra) > 0) {
                        //dd($prodservExtra);

                        $data_extra = $request->all();
                        $data_extra['tipo_vuelo_id'] = 2;
                        $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                        $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                        //$extra =  $this->getProdservicios("extra", $request);
                    }

                    /*                     * ***** CLIENTES******* */
                    //$Cliente = \App\Models\Clientes::where('documento', $request->document)->first();
                    //if ($Cliente == null) {
                    if ($request->cliente_id == null) {

                        $Cliente = \App\Models\Clientes::where('documento', $request->document)->first();

                        if ($Cliente == null) {
                            $Cliente = new \App\Models\Clientes();
                        }



                        $Cliente->tipo_documento = $request->type_document;
                        $Cliente->documento = $request->document;

                        //$Cliente->user_id = Auth::user()->id;
                        $Cliente->user_id = 1;
                        $Cliente->fecha_registro = now();
                        $Cliente->ip = $ip;
                    } else {
                        $Cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id));
                    }



                    $Cliente->direccion = $request->direccion;
                    $Cliente->tipo_id = 3;

                    $Cliente->razon_social = Upper($request->razon);
                    $Cliente->telefono = $request->phone;
                    $Cliente->correo = (isset($request->correo) ?: "$request->correo");

                    $Cliente->direccion = $request->direccion;
                    $Cliente->abreviatura = "";
                    $Cliente->save();

                    $Factura = new \App\Models\Facturas();
                    $Factura->fecha_factura = now();
                    $Factura->cliente_id = $Cliente->id;
                    $Factura->solicitud_id = $VuelosGenerales->id;
                    $Factura->moneda_aplicada = $MonedaPago;
                    $Factura->monto_moneda_aplicada = ${$MonedaPago};
                    $Factura->formato_factura = 'formato_tasa_general';
                    $Factura->save();

                    foreach ($tasa as $value) {
                        $FacturaDetalle = new \App\Models\FacturasDetalle();
                        $FacturaDetalle->factura_id = $Factura->id;
                        $FacturaDetalle->codigo = $value['codigo'];
                        $FacturaDetalle->descripcion = $value['descripcion'];
                        $FacturaDetalle->precio = $value['bs'];
                        $FacturaDetalle->precio2 = $value['precio'];
                        $FacturaDetalle->iva = $value['iva'];
                        $FacturaDetalle->save();
                    }

                    foreach ($extra as $value) {
                        $FacturaDetalle = new \App\Models\FacturasDetalle();
                        $FacturaDetalle->factura_id = $Factura->id;
                        $FacturaDetalle->codigo = $value['codigo'];
                        $FacturaDetalle->descripcion = $value['descripcion'];
                        $FacturaDetalle->precio = $value['bs'];
                        $FacturaDetalle->precio2 = $value['precio'];
                        $FacturaDetalle->iva = $value['iva'];
                        $FacturaDetalle->save();
                    }

                    foreach ($request->money as $key => $value) {
                        $Pagos = new \App\Models\FacturasPagos();

                        $Pagos->factura_id = $Factura->id;
                        $Pagos->forma_pago_id = Encryptor::decrypt($key);

                        $Pagos->monto = saveFloat($value);

                        $Pagos->save();
                    }

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Registrado Exitosamente";
                    $result['data'] = $Factura->crypt_id;
                    return $result;
                    //dd("fff");
                } else {
                    $D = \App\Models\Aeropuertos::where("activo", 1)->where("id", "!=", Auth::user()->aeropuerto->id)->get();
                    foreach ($D as $value) {
                        $DESTINO[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
                        //$DESTINO_EXT[$value->crypt_id] = ["data-baer" => ($value->pertenece_baer == true ? 1 : 0), "data-categoria" => $value->categoria->porcentaje];
                    }
                    $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();

                    $DOLAR = $this->getDolar();
                    $EURO = $this->getEuro();
                    $PETRO = $this->getPetro();

                    return view('vuelos_generales.tasas', compact('DESTINO', 'forma_pagos', 'DOLAR', 'EURO', 'PETRO'));
                }

                return view('vuelos_generales.lista', compact('data1'));
            } else {
                $msg = "Error en la Seguridad de la Taquilla, Por Favor Contacte al Administrador";
                return view('errors.general', compact('msg'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function reserva_general(Request $request) {
        $msg = "";
        if ($request->isMethod('post')) {
            $Validator = \Validator::make(
                            $request->all(), [
                        'type_document2' => 'required',
                        'document2' => 'required',
                        'piloto' => 'required',
                        'nacionalidad_id' => 'required',
                        'fecha' => 'required',
                        'hora_inicio' => 'required',
                        'origen_id' => 'required',
                        'destino_id' => 'required',
                        'cant_pasajeros' => 'required',
                        'placa' => 'required'
                            ], [
                        'type_document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'piloto.required' => __('El Nombre del Piloto es Requerido'),
                        'nacionalidad_id.required' => __('Debe Indicar El Tipo de Vuelo (Nacional o Internacional)'),
                        'fecha.required' => __('Debe Indicar la Fecha'),
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'origen_id.required' => __('Debe Indicar el Origen del Vuelo'),
                        'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                        'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros '),
                        'placa.required' => __('Debe Indicar la Matricula de la Aeronave')
                            ]
            );
            if ($Validator->fails()) {
                $msg = [];
                foreach ($Validator->messages()->all() as $value) {
                    $msg[] = $value;
                }
                return view('registros.fail', compact('msg'));
            } else {

                //Pilotos
                if ($request->piloto_id == null) {
                    $Pilotos = \App\Models\Pilotos::where('documento', $request->document2)->first();
                    if ($Pilotos == null) {
                        $Pilotos = new \App\Models\Pilotos();
                    }

                    $Pilotos->tipo_documento = $request->type_document2;
                    $Pilotos->documento = $request->document2;

                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;
                } else {
                    $Pilotos = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                }
                //dd($request->piloto_id);
                $Pilotos->nombres = Upper($request->piloto);
                $Pilotos->apellidos = "";
                $Pilotos->telefono = "";
                $Pilotos->correo = "";
                $Pilotos->save();

                /*                 * ***** AERONAVES******* */
                $Aeronaves = \App\Models\Aeronaves::where('matricula', $request->placa)->first();

                $prodservExtra = array();
                $extra = array();
                if (is_array($request->prodservicios)) {
                    foreach ($request->prodservicios as $value) {
                        $prodservExtra[] = Encryptor::decrypt($value);
                    }
                }



                $VuelosGenerales = new \App\Models\VuelosGenerales();

                $DOLAR = $this->getDolar();
                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $VuelosGenerales->piloto_id = $Pilotos->id;
                $VuelosGenerales->tipo_vuelo_id = $request->nacionalidad_id;
                $VuelosGenerales->fecha_vuelo = date('Y-m-d');
                $VuelosGenerales->hora_vuelo = $request->hora_inicio;
                $VuelosGenerales->origen_id = Encryptor::decrypt($request->origen_id);
                $VuelosGenerales->destino_id = Encryptor::decrypt($request->destino_id);
                $VuelosGenerales->aeronave_id = $Aeronaves->id;
                $VuelosGenerales->cantidad_pasajeros = $request->cant_pasajeros;
                $VuelosGenerales->tipo_registro_id = 2; // ONLINE
                $VuelosGenerales->valor_dolar = $DOLAR;
                $VuelosGenerales->valor_euro = $EURO;
                $VuelosGenerales->valor_petro = $PETRO;
                $VuelosGenerales->observaciones = $request->observacion == null ? "" : $request->observacion;
                $VuelosGenerales->prodservicios_extra = implode(",", $prodservExtra);

                $VuelosGenerales->fecha_registro = now();
                $VuelosGenerales->ip = $this->getIp();

                $VuelosGenerales->save();

                if ($request->img != null) {
                    try {
                        $fileName = "pago_img_" . $VuelosGenerales->id . '.' . $request->img->extension();
                        $request->img->move(storage_path('pagos'), $fileName);
                    } catch (Exception $ex) {
                        
                    }
                }


                $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));
                $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));

                $data_tasa = $request->all();
                $data_tasa['tipo_vuelo_id'] = 2;
                $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));
                $tasa = $this->getProdservicios("tasa", $data_tasa);

                $data_dosa = $request->all();
                $data_dosa['tipo_vuelo_id'] = 2;
                $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));
                $dosa = $this->getProdservicios("dosa", $data_dosa);

                if (count($prodservExtra) > 0) {
                    $data_extra = $request->all();
                    $data_extra['tipo_vuelo_id'] = 2;
                    $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));
                    $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                }
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];
                $Reservaciones = $VuelosGenerales;
                return PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
                // dd($dosa);
            }
        } else {
            $O = \App\Models\Aeropuertos::where("activo", 1)->get();
            foreach ($O as $value) {
                $ORIGEN[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
            }

            $D = \App\Models\Aeropuertos::where("activo", 1)->get();
            foreach ($O as $value) {
                $DESTINO [$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
            }

            $EURO = $this->getEuro();
            $PETRO = $this->getPetro();

            return view('vuelos_generales.reserva_general', compact('ORIGEN', 'DESTINO', 'EURO', 'PETRO', 'msg'));
        }
    }

}
