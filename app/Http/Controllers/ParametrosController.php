<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DataTables;
use App\Helpers\Encryptor;

class ParametrosController extends Controller {
    
    public function params(Request $request){
        if (\Request::ajax()) {
            //dd($this->getDolar());
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'value' => 'required',
                                ], [
                            'value.required' => __('Falto Agregar el Valor'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {


                    $IVA = \App\Models\Parametros::where('nombre', $request->param)->first();
                    $IVA->valor = saveFloat($request->value);
                    $IVA->save();
                    
                    /*
                    $Audit = new \App\Models\AuditParametros();
                    $Audit->fecha = now();
                    $Audit->parametro = $request->param;
                    $Audit->valor = $request->value;
                    $Audit->user_id = Auth::user()->id;
                    ;
                    $ip = $this->getIp();
                    $Audit->ip = $ip;
                    $Audit->save();
                    */
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $params= \App\Models\Parametros::whereIn('nombre', ['IVA',  'TASA_CAMBIO_DOLLAR', 'TASA_CAMBIO_EURO', 'TASA_CAMBIO_PETRO'])->get();
                $GET_PARAMS_ONLINE= \App\Models\Parametros::where('nombre', 'GET_PARAMS_ONLINE')->first();
                return view('parametros.params', compact('params', 'GET_PARAMS_ONLINE'));
            }
        } else {
            return Redirect::to('/');
        }
    }
    
    
    public function iva(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'iva' => 'required',
                                ], [
                            'iva.required' => __('El Iva Es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {


                    $IVA = \App\Models\Parametros::where('nombre', 'IVA')->first();
                    $IVA->valor = saveFloat($request->iva);
                    $IVA->save();

                    $Audit = new \App\Models\AuditParametros();
                    $Audit->fecha = now();
                    $Audit->parametro = 'IVA';
                    $Audit->valor = $request->iva;
                    $Audit->user_id = Auth::user()->id;
                    ;
                    $ip = $this->getIp();
                    $Audit->ip = $ip;
                    $Audit->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $IVA = \App\Models\Parametros::where('nombre', 'IVA')->first()->valor;
                return view('parametros.iva', compact('IVA'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function tasa_dolar(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'tasa' => 'required',
                                ], [
                            'tasa.required' => __('La Tasa Es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {


                    $TASA = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_DOLLAR')->first();
                    $TASA->valor = saveFloat($request->tasa);
                    $TASA->save();

                    $Audit = new \App\Models\AuditParametros();
                    $Audit->fecha = now();
                    $Audit->parametro = 'TASA CAMBIO DOLLAR';
                    $Audit->valor = $request->tasa;
                    $Audit->user_id = Auth::user()->id;
                    $ip = $this->getIp();
                    $Audit->ip = $ip;
                    $Audit->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $TASA = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_DOLLAR')->first()->valor;
                return view('parametros.tasa_dolar', compact('TASA'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function tasa_euro(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'tasa' => 'required',
                                ], [
                            'tasa.required' => __('La Tasa Es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {


                    $TASA = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_EURO')->first();
                    $TASA->valor = saveFloat($request->tasa);
                    $TASA->save();

                    $Audit = new \App\Models\AuditParametros();
                    $Audit->fecha = now();
                    $Audit->parametro = 'TASA CAMBIO EURO';
                    $Audit->valor = $request->tasa;
                    $Audit->user_id = Auth::user()->id;
                    $ip = $this->getIp();
                    $Audit->ip = $ip;
                    $Audit->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $TASA = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_EURO')->first()->valor;
                return view('parametros.tasa_euro', compact('TASA'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
