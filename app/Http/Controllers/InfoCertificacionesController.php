<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Hangares;


class InfoCertificacionesController extends Controller
{
    public function Savecertificacion(Request $request){
            //dd($request->c_tpd);
        $v = Validator::make($request->all(),
            [
                "f1" => "required",
                "f2" => "required",
                "f3" => "required",
                "f4" => "required",
                "c_sms" => "required",
                "c_avsev" => "required",
                "c_mp" => "required",
                "c_tpd" => "required",
                "p_pm" => "required",
                "p_pc" => "required",
                "p_pe" => "required",
                "p_tai" => "required",
                "p_cpaf" => "required",
                "p_ca" => "required",
                "p_gma" => "required",
                "p_cc" => "required",
                "p_lf" => "required",
                "p_sl" => "required",
                "e_ef" => "required",
                "e_e" => "required"
            ], 
            [
                "f1.required" => "required",
                "f2.required" => "required",
                "f3.required" => "required",
                "f4.required" => "required",
                "c_sms.required" => "required",
                "c_avsev.required" => "required",
                "c_mp.required" => "required",
                "c_tpd.required" => "required",
                "p_pm.required" => "required",
                "p_pc.required" => "required",
                "p_pe.required" => "required",
                "p_tai.required" => "required",
                "p_cpaf.required" => "required",
                "p_ca.required" => "required",
                "p_gma.required" => "required",
                "p_cc.required" => "required",
                "p_lf.required" => "required",
                "p_sl.required" => "required",
                "e_ef.required" => "required",
                "e_e.required" => "required"
            ]
        );

        if (!$v->fails()) {//si no fallan las validaciones
            $Certificaciones=new Certificaciones();

            $Certificaciones->f1=$request->f1;
            $Certificaciones->ff1=$request->ff1;
            $Certificaciones->f2=$request->f2;
            $Certificaciones->ff2=$request->ff2;
            $Certificaciones->f3=$request->f3;
            $Certificaciones->ff3=$request->ff3;
            $Certificaciones->f4=$request->f4;
            $Certificaciones->ff4=$request->ff4;
            $Certificaciones->f5=$request->f5;
            $Certificaciones->ff5=$request->ff5;

            $Certificaciones->c_sms=$request->c_sms;
            $Certificaciones->f_c_sms=$request->f_c_sms;
            
            $Certificaciones->c_avsev=$request->c_avsev;
            $Certificaciones->f_c_avsev=$request->f_c_avsev;

            $Certificaciones->c_mp=$request->c_mp;
            $Certificaciones->f_c_mp=$request->f_c_mp;

            $Certificaciones->c_tpd=$request->c_tpd;
            $Certificaciones->f_c_tpd=$request->f_c_tpd;
            
            $Certificaciones->p_pm=$request->p_pm;
            $Certificaciones->f_p_pm=$request->f_p_pm;

            $Certificaciones->p_pc=$request->p_pc;
            $Certificaciones->f_p_pc=$request->f_p_pc;
            
            $Certificaciones->p_pe=$request->p_pe;
            $Certificaciones->f_p_pe=$request->f_p_pe;
            
            $Certificaciones->p_tai=$request->p_tai;
            $Certificaciones->f_p_tai=$request->f_p_tai;
            
            $Certificaciones->p_cpaf=$request->p_cpaf;
            $Certificaciones->f_p_cpaf=$request->f_p_cpaf;
            
            $Certificaciones->p_ca=$request->p_ca;
            $Certificaciones->f_p_ca=$request->f_p_ca;
            
            $Certificaciones->p_gma=$request->p_gma;
            $Certificaciones->f_p_gma=$request->f_p_gma;
            
            $Certificaciones->p_cc=$request->p_cc;
            $Certificaciones->f_p_cc=$request->f_p_cc;
            
            $Certificaciones->p_lf=$request->p_lf;
            $Certificaciones->f_p_lf=$request->f_p_lf;
            
            $Certificaciones->p_sl=$request->p_sl;
            $Certificaciones->f_p_sl=$request->f_p_sl;
            
            $Certificaciones->e_ef=$request->e_ef;
            $Certificaciones->f_e_ef=$request->f_e_ef;
            
            $Certificaciones->e_e=$request->e_e;
            $Certificaciones->f_e_e=$request->f_e_e;

            $Certificaciones->ip=$request->ip();
            $Certificaciones->user_id=Auth::id();
            $Certificaciones->save();

            

            $message = array('status' => true, 'type' => 'success', 'title' => 'REGISTRADO', 'message' => 'Datos Registrado');
        } else {
            $message = array('status' => false, 'type' => 'warning', 'title' => 'ERROR', 'message' => $v->errors()->first());
        }
        return $message;
    }
}
