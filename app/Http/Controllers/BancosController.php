<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Sesion;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use App\Models\Bancos;
use DataTables;
use PDF;

class BancosController extends Controller
{
    public function get_data_cliente($tipo_doc, $doc) {
        if (\Request::ajax()) {

            $Cliente = Bancos::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();

            if ($Cliente == null) {

                $Cliente = \App\Models\Pilotos::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();
                if ($Cliente == null) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Datos No Encontrado');
                    $result['data'] = null;
                } else {
                    $C = new \App\Models\Clientes();
                    $C->tipo_documento = $tipo_doc;
                    $C->documento = $doc;

                    $C->razon_social = Upper($Cliente->nombres . ' ' . $Cliente->apellidos);
                    $C->telefono = ($Cliente->telefono==null ? "":$Cliente->telefono);
                    $C->correo = ($Cliente->correo==null ? "":$Cliente->correo);
                    $C->direccion = "";
                    
                    $C->tipo_id = 4; // general

                    $C->user_id = Auth::user()->id;
                    $C->ip = $this->getIp();
                    $C->fecha_registro = now();
                    $C->save();

                    $Cliente = $C;

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Datos Encontrados');
                    $result['data'] = $Cliente->toArray();
                }
            } else {
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Datos Encontrados');
                $result['data'] = $Cliente->toArray();
            }
            return response()->json($result);
        }
    }

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
       
            $Bancos_active = Bancos::get();

            // dd($Bancos_active);

            $Bancos_active = Datatables::of($Bancos_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('bancos.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }
                        if ($row->activo == true) {
                            if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('bancos.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"> <li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                            }
                        } else {
                            if (in_array("active", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('bancos.active', $row->crypt_id) . '" class="actions_users btn btn-success btn-sm"> <li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                            }
                        }
                        
                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Bancos_active->original['data'];

            return view('bancos.index', compact('data1', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'codigo' => 'required|numeric|max:9999',
                            'nombre' => 'required',
                          
                                ], [
                            'codigo.required' => __('El Codigo es Requerido'),
                            'codigo.max' => __('El Codigo no puede ser mayor de 4 Digitos'),
                            'nombre.required' => __('El Nombre es Requerido'),
                              
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Bancos = \App\Models\Bancos::where("codigo", $request->codigo)->count();

                    if ($Bancos == 0) {



                        $Bancos = new \App\Models\Bancos();
                        $Bancos->codigo = $request->codigo;
                        $Bancos->nombre = $request->nombre;
                        $Bancos->activo = true;
                        
                        $Bancos->user_id = Auth::user()->id;
                        $Bancos->ip = $this->getIp();
                        $Bancos->fecha_creacion = now();
                        
                        $Bancos->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Banco ya Existe');
                    }
                }
                return $result;
            } else {
                return view('bancos.create');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                    $request->all(), [
                            'codigo' => 'required|numeric|max:9999',
                            'nombre' => 'required',
                        ], [
                            'codigo.required' => __('El Codigo es Requerido'),
                            'codigo.max' => __('El Codigo no puede ser mayor de 4 Digitos'),
                            'nombre.required' => __('El Nombre es Requerido'),
                        ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {
                    $Bancos = \App\Models\Bancos::where("codigo", $request->codigo)->where("id", "!=", Encryptor::decrypt($id))->count();
                    if ($Bancos == 0) {
                        $Bancos = Bancos::find(Encryptor::decrypt($id));
                        $Bancos->codigo = $request->codigo;
                        $Bancos->nombre = $request->nombre;
                        $Bancos->activo = true;
                        $Bancos->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Banco ya Existe');
                    }
                }
                return $result;
            } else {
                $Bancos = Bancos::find(Encryptor::decrypt($id));
                return view('bancos.edit', compact('Bancos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Bancos = Bancos::find(Encryptor::decrypt($id));
                $Bancos->activo = false;

                $Bancos->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesed Correctly');

                return $result;
            } else {

                $Bancos = Bancos::find(Encryptor::decrypt($id));
                return view('bancos.disable', compact('Bancos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Bancos = Bancos::find(Encryptor::decrypt($id));
                $Bancos->activo = true;

                $Bancos->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesed Correctly');

                return $result;
            } else {
                $Bancos = Bancos::find(Encryptor::decrypt($id));
                return view('bancos.active', compact('Bancos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    
}
