<?php

namespace App\Http\Controllers;

use App\Helpers\Encryptor;
use App\Models\Movimiento;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Support\Facades\Auth;

class MovimientoController extends Controller
{
    public function index(Request $request){
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $query = \App\Models\Movimiento::with('getOrigenDestino')->orderBy('fecha', 'desc');
                    if (Auth::user()->aeropuerto_id != null) {
                        $query->where("aeropuerto_id", Auth::user()->aeropuerto_id);
                    }

                $movimientos = $query->get();

                
                if (Auth::user()->aeropuerto_id != null) {
                    $Aerolineas = \App\Models\AeropuertoAerolinea::with('aerolineas')->where("aeropuerto_id", Auth::user()->aeropuerto_id)->get()->pluck('aerolineas.full_razon_social', 'crypt_id');
                    $AeropuertosUser = Auth::user()->aeropuerto_id;
                }else {
                    // $Aerolineas = ["0" => "Seleccione"];
                    $Aerolineas= \App\Models\Clientes::where("activo", 1)
                        ->where('tipo_id', $this->TIPOS_CLIENTE_AEROLINEA)
                        ->get()
                        ->pluck('full_razon_social', 'crypt_id')
                    ;
                    $AeropuertosUser = \App\Models\Aeropuertos::where('maest_aeropuertos.activo', true)
                        ->where('maest_aeropuertos.pertenece_baer', '=', true)
                        ->get()
                        ->pluck("full_nombre", "crypt_id")
                    ;
                }
                 
                if ($Aerolineas->count() == 0) {
                    $Aerolineas = [null => 'Seleccione'];
                    $message = [
                        'type' => 'info',
                        'message' => 'El aeropuerto que tiene asignado no tiene aerolineas asociadas',
                        'data' => [],
                    ];
                }

                $OrigenDestino = \App\Models\Aeropuertos::where('maest_aeropuertos.activo', true)
                    ->where('maest_aeropuertos.pertenece_baer', '=', true)
                    ->get()
                    ->pluck("full_nombre", "crypt_id")
                ;
                $observaciones = [
                    '' => 'Seleccione',
                    'A Tiempo' => 'A Tiempo / On time',
                    'Chequeo de Documento' => 'Chequeo de Documento / Document Check',
                    'Demorado' => 'Demorado / Delayed',
                    'Despegando' => 'Despegando / Taking off',
                    'Abordando' => 'Abordando / On boarding',
                    'Arribó' => 'Arribó / Arrived',
                    'Cancelado' => 'Cancelado / Canceled',
                    'Despegó' => 'Despegó / Took off',
                ];

                if (isset($message)) {
                    return view('Movimientos.index', ['movimientos' => $movimientos, 'Aerolineas' => $Aerolineas, 'OrigenDestino'=>$OrigenDestino, 'AeropuertosUser' => $AeropuertosUser, "observaciones" => $observaciones ])->with('error', $message);
                }else{
                    return view('Movimientos.index', ['movimientos' => $movimientos, 'Aerolineas' => $Aerolineas, 'OrigenDestino'=>$OrigenDestino, 'AeropuertosUser' => $AeropuertosUser, "observaciones" => $observaciones ]);
                }
            }else{
                if (\Request::isMethod('post')) {
                    // dd($request);
                    $movimiento = new Movimiento();
                    $movimiento->aeropuerto_id = Auth::user()->aeropuerto_id != null ? Auth::user()->aeropuerto_id : Encryptor::decrypt($request->aeropuerto_id);
                    $movimiento->aerolinea_id = Encryptor::decrypt($request->aerolinea);
                    $movimiento->tipo_operacion_id = $request->tipo_operacion;
                    $movimiento->numero_vuelo = $request->nro_vuelo;
                    $movimiento->tipo_recorrido_id = $request->tipo_recorrido;
                    $movimiento->origen_destino_id = Encryptor::decrypt($request->aeropuerto_origen_destino);
                    $movimiento->fecha = saveDate($request->fecha);
                    $movimiento->hora = $request->hora;
                    $movimiento->puerta = $request->puerta;
                    $movimiento->observaciones = $request->observaciones;
                    
                    $movimiento->user_id = Auth::user()->id;
                    $movimiento->ip = $request->ip();
                    $movimiento->fecha_creacion = now();
                    $movimiento->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Registrado Correctamente";
                    $result['data'] = $movimiento;

                    return $result;
                }else{
                    if ($request->movimiento_id != '') {
                        $movimiento = Movimiento::find(Encryptor::decrypt($request->movimiento_id));
                        
                        $movimiento->aeropuerto_id = Auth::user()->aeropuerto_id != null ? Auth::user()->aeropuerto_id : Encryptor::decrypt($request->aeropuerto_id);
                        $movimiento->aerolinea_id = Encryptor::decrypt($request->aerolinea);
                        $movimiento->tipo_operacion_id = $request->tipo_operacion;
                        $movimiento->tipo_recorrido_id = $request->tipo_recorrido;
                        $movimiento->numero_vuelo = $request->nro_vuelo;
                        $movimiento->origen_destino_id = Encryptor::decrypt($request->aeropuerto_origen_destino);
                        $movimiento->fecha = saveDate($request->fecha);
                        $movimiento->hora = $request->hora;
                        $movimiento->puerta = $request->puerta;
                        $movimiento->observaciones = $request->observaciones;
                        
                        // $movimiento->user_id = Auth::user()->id;
                        // $movimiento->ip = $request->ip();
                        // $movimiento->fecha_creacion = now();
                        $movimiento->save();
    
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Actualizado Correctamente";
                        $result['data'] = $movimiento;
    
                        return $result;
                    }
                }
            }
        } else {
            exit;
        }
    }


    public function informacionAeropuerto($codigo_oaci = ""){
        if ($codigo_oaci == ""){

            $Aeropuertos = \App\Models\Aeropuertos::where("pertenece_baer", true)
        ->where('activo', true)
        ->get();
        
        // dd($Aeropuertos);
                return view('aeropuertos.list', ['Aeropuertos'=>$Aeropuertos ]);
            exit;

        }else{

            $Aeropuerto = \App\Models\Aeropuertos2::where("pertenece_baer", true)->where("codigo_oaci", Upper($codigo_oaci))->first();
            
            
            if ($Aeropuerto!= null){

                $movimientosNacS = \App\Models\Movimiento::where("aeropuerto_id", $Aeropuerto->id)
                    ->orderBy("fecha")
                    ->with('getOrigenDestino', 'getAerolinea')
                    ->where('tipo_recorrido_id', 1) //Nac - Int
                    ->where('tipo_operacion_id', 2)
                    // ->where("fecha", date("Y-m-d"))
                ->get();
                
                $movimientosNacL = \App\Models\Movimiento::where("aeropuerto_id", $Aeropuerto->id)
                ->orderBy("fecha")
                ->with('getOrigenDestino', 'getAerolinea')
                ->where('tipo_recorrido_id', 1) //Nac - Int
                ->where('tipo_operacion_id', 1) // Llegada
                // ->where("fecha", date("Y-m-d"))
                ->get();

                $movimientosInterL = \App\Models\Movimiento::where("aeropuerto_id", $Aeropuerto->id)
                    ->orderBy("fecha")
                    ->with('getOrigenDestino', 'getAerolinea')
                    ->where('tipo_recorrido_id', '!=',1)
                    ->where('tipo_operacion_id', 1)
                    // ->where("fecha", date("Y-m-d"))
                ->get();

                $movimientosInterS = \App\Models\Movimiento::where("aeropuerto_id", $Aeropuerto->id)
                    ->orderBy("fecha")
                    ->with('getOrigenDestino', 'getAerolinea')
                    ->where('tipo_recorrido_id', '!=',1)
                    ->where('tipo_operacion_id', 2)
                    // ->where("fecha", date("Y-m-d"))
                ->get();

                // dd($movimientos->toArray());
                  $Videos = \App\Models\Video::where("activo", true)->where("aeropuerto_id", $Aeropuerto->id)->pluck('nombre_video');
            
                return view('Movimientos.movimientos', ['Videos'=>$Videos, 'movimientosNacS' => $movimientosNacS, 'movimientosNacL' => $movimientosNacL, 'movimientosInterL' => $movimientosInterL , 'movimientosInterS' => $movimientosInterS , 'Aeropuerto'=>$Aeropuerto ]);
            }
            exit;

        }
    }


    public function obtener_lista(){
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $query = \App\Models\Movimiento::with('getOrigenDestino', 'getAerolinea', 'getAeropuerto')->orderBy('fecha', 'desc');
                if (Auth::user()->aeropuerto_id != null) {
                    $query->where("aeropuerto_id", Auth::user()->aeropuerto_id);
                }

                $movimientos = $query->get();
                
                $data = Datatables::of($movimientos)
                    ->addColumn('boton', function ($row) {
                        $button = '<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#anularModal" title="Anular"><i class="fas fa-ban"></i></button>';
                        $buttonEdit = '<button type="button" class="btn btn-success btn-xs ml-2" onclick="editMovimiento(\'' . $row->crypt_id . '\', \'' . $row->fecha . '\', \'' . $row->hora . '\', \'' . $row->tipo_operacion_id . '\', \'' . $row->getAerolinea->crypt_id . '\', \'' . $row->numero_vuelo . '\', \'' . $row->getOrigenDestino->crypt_id . '\', \'' . $row->puerta . '\', \'' . $row->observaciones . '\', \'' . $row->getAeropuerto->crypt_id . '\' , \'' . $row->tipo_recorrido_id . '\' )" data-toggle="modal" data-target="#movimientoModal" title="Editar"><i class="fas fa-edit"></i></button>';
                        return $button.$buttonEdit;
                    })
                    ->rawColumns(['boton'])
                    ->addColumn('tipo_operacion', function ($row) {
                        $tipo_operacion = ($row->tipo_operacion_id == 1) ? $tipo_operacion = 'Llegada' : $tipo_operacion = 'Salida';
                        return $tipo_operacion;
                    })
                    ->addColumn('tipo_recorrido', function ($row) {
                        $tipo_recorrido = ($row->tipo_recorrido_id == 1) ? $tipo_recorrido = 'Nacional' : $tipo_recorrido = 'Internacional';
                        return $tipo_recorrido;
                    })
                    ->addColumn('fecha2', function ($row) {
                        return showDate($row->fecha);
                    })
                    // ->make(true)
                ;

                return $data->toJson();
            }else{
                // return exit;
            }
        } else {
            exit;
        }
        
    }
    
    public function showVideos($codigo_oaci){
        $videos =[];
        return view('Movimientos.videos'. compact('videos'));
    }


    public function lista_videos(Request $request){
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $query = \App\Models\Movimiento::with('getOrigenDestino')->orderBy('fecha', 'desc');
                    if (Auth::user()->aeropuerto_id != null) {
                        $query->where("aeropuerto_id", Auth::user()->aeropuerto_id);
                    }

                $movimientos = $query->get();

                $Aerolineas= \App\Models\Clientes::where("activo", 1)
                    ->where('tipo_id', $this->TIPOS_CLIENTE_AEROLINEA)
                    ->get()
                    ->pluck('full_razon_social', 'crypt_id')
                ;
                if (Auth::user()->aeropuerto_id != null) {
                    $AeropuertosUser = Auth::user()->aeropuerto_id;
                } else {
                    $AeropuertosUser = \App\Models\Aeropuertos::where('maest_aeropuertos.activo', true)
                        ->where('maest_aeropuertos.pertenece_baer', '=', true)
                        ->get()
                        ->pluck("full_nombre", "crypt_id")
                    ;
                }
                $OrigenDestino = \App\Models\Aeropuertos::where('maest_aeropuertos.activo', true)
                    ->where('maest_aeropuertos.pertenece_baer', '=', true)
                    ->get()
                    ->pluck("full_nombre", "crypt_id")
                ;
                return view('Movimientos.index_videos', ['movimientos' => $movimientos, 'Aerolineas' => $Aerolineas, 'OrigenDestino'=>$OrigenDestino, 'AeropuertosUser' => $AeropuertosUser ]);
            }else{
                if (\Request::isMethod('post')) {
                    // $request->validate([
                    //     'video' => 'required|mimetypes:video/mp4,video/avi|max:100000',
                    // ]);
            
                    $video = new Video();
                    $video->aeropuerto_id = Auth::user()->aeropuerto_id != null ? Auth::user()->aeropuerto_id : Encryptor::decrypt($request->aeropuerto_id);
                    $video->comercio = $request->comercio == null ? 'BAER' : Upper($request->comercio);
                    $video->fecha_expiracion = saveDate($request->fecha_expiracion);
                    $video->publicidad = $request->tipo_anuncio;


                    $video->user_id = Auth::user()->id;
                    $video->ip = $request->ip();
                    $video->fecha_creacion = now();
                    $video->save();

                    if ($request->file('video') != null) {
                        $file = $request->file('video');
                        // dd($request->img);
                        try {

                            $fileName = "video_" . $video->id . '.' . $file->getClientOriginalExtension();
                            $video->nombre_video = $fileName;
                            $video->save();
                            $file->move(public_path('dist'.DIRECTORY_SEPARATOR.'videos'), $fileName);
                        } catch (Exception $ex) {
                            
                        }
                    }                  

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Registrado Correctamente";
                    $result['data'] = $video;

                    return $result;
                }else{
                    if ($request->video_id != '') {
                        
                        $video = Video::find(Encryptor::decrypt($request->video_id));
                        $video->aeropuerto_id = Auth::user()->aeropuerto_id != null ? Auth::user()->aeropuerto_id : Encryptor::decrypt($request->aeropuerto_id);
                        $video->comercio = $request->comercio;
                        $video->fecha_expiracion = saveDate($request->fecha_expiracion);
                        $video->publicidad = $request->tipo_anuncio;


                        $video->user_id = Auth::user()->id;
                        $video->ip = $request->ip();
                        $video->save();
                        
    
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Actualizado Correctamente";
                        $result['data'] = $video;
    
                        return $result;
                    }
                }
            }
        } else {
            exit;
        }
    }
    public function obtener_videos(){
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $query = \App\Models\Video::with('getAeropuerto')->orderBy('fecha_creacion', 'desc');
                // with('getOrigenDestino', 'getAerolinea', 'getAeropuerto')->orderBy('fecha', 'desc');
                if (Auth::user()->aeropuerto_id != null) {
                    $query->where("aeropuerto_id", Auth::user()->aeropuerto_id);
                }

                $videos = $query->get();
                
                $data = Datatables::of($videos)
                    ->addColumn('boton', function ($row) {
                        $publicidad = ($row->publicidad == true) ? $publicidad = '1' : $publicidad = '0';

                        $button = '<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#anularModal" title="Anular"><i class="fas fa-ban"></i></button>';
                        $buttonEdit = '<button type="button" class="btn btn-success btn-xs ml-2" onclick="editVideo(\'' . $row->crypt_id . '\', \'' . $row->getAeropuerto->crypt_id . '\', \'' . $row->comercio . '\', \'' . $row->fecha_expiracion . '\', \'' . $row->nombre_video . '\', \'' . $publicidad . '\' )" data-toggle="modal" data-target="#videoModal" title="Editar"><i class="fas fa-edit"></i></button>';
                        return $button.$buttonEdit;
                    })
                    ->addColumn('estatus', function ($row) {
                        if ($row->estatus == true) {
                            $estatus = '<i class="text-danger fas fa-ban"></i>';
                        } else {
                            $estatus = '<i class="text-success far fa-check-circle"></i>';
                        }
                        return $estatus;
                    })
                    ->addColumn('fecha', function($row){
                        return showDate($row->fecha_expiracion);
                    })
                    ->addColumn('publicidad', function($row){
                        $publicidad = ($row->publicidad == true) ? $publicidad = 'SÍ' : $publicidad = 'NO';
                        return $publicidad;
                    })
                    ->addColumn('video', function($row){
                        $text = '<div role="button" id="'.$row->crypt_id.'" data-toggle="modal" data-target="#playVideoModal" class="text-primary text-decoration-none" onclick="playVideo(\'' . $row->nombre_video . '\')">'.$row->nombre_video.'</div>';
                        return $text;
                    })
                    ->rawColumns(['boton', 'estatus', 'video'])
                    // ->make(true)
                ;

                return $data->toJson();
            }else{
                // return exit;
            }
        } else {
            exit;
        }
        
    }
}
