<?php

namespace App\Http\Controllers;

use App\Helpers\Encryptor;
use App\Models\ConcesionEquipo;
use App\Models\InventarioEquipo;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\InventarioEquipoExport;
use App\Models\AlarmaInventarioDetalle;
use App\Models\CostoReparacion;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\DataTables;

class InventarioEquipoController extends Controller
{
    public function inventario_equipos(Request $request){
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $concesionEquipos = ConcesionEquipo::get()->pluck('nombre', 'crypt_id');

                $alarmaEquipos = AlarmaInventarioDetalle::with('equipos')->whereBetween('fecha_revision', [now()->format('Y-m-d'), now()->addWeeks(2)->format('Y-m-d')] )
                                                            ->where('procesado', false)->orderBy('id', 'asc')->get();
                return view('inventario_equipos.index', ['concesionEquipos' => $concesionEquipos, 'alarmaEquipos' => $alarmaEquipos]);

            }else{
                if ($request->isMethod('post')) {
                    $irreparable = (isset($request->irreparable)) ? true : false;
                    if ($irreparable == true) {
                        $request->validate([
                            'fecha_factura' => 'required',
                            'factura' => 'required',
                        ]);
                        if(saveFloat($request->unitario) == 0 || saveFloat($request->totalP) == 0 ){
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = "Debe ingresar una cantidad válida";
                            $result['data'] = [];

                            return $result;
                        }
                    }
                    if ($request->estado == 1) {
                        $estado = true;
                        $html_estado = '1';
                    }elseif ($request->estado == false){
                        $estado = false;
                        $html_estado = '0';
                    }else{
                        $estado = null;
                        $html_estado = '-1';
                    }

                    $inventarioEquipo = new InventarioEquipo();
                    $inventarioEquipo->serial = $request->serial;
                    $inventarioEquipo->modelo = $request->modelo;
                    $inventarioEquipo->cant = $request->cantidad;
                    $inventarioEquipo->descripcion = $request->descripcion;
                    $inventarioEquipo->concesion_equipo_id = Encryptor::decrypt($request->concesion);
                    $inventarioEquipo->ubicacion_actual = $request->ubicacion_actual;
                    $inventarioEquipo->estado = $estado;
                    $inventarioEquipo->irreparable = $irreparable;
                    $inventarioEquipo->user_id = Auth::user()->id;
                    $inventarioEquipo->fecha_registro = now();
                    $inventarioEquipo->ip = $this->getIp();
                    $inventarioEquipo->save();
                    $inventarioEquipo = InventarioEquipo::with('concesiones')->find($inventarioEquipo->id);

                    if ($irreparable == true) {
                        $costoReparacion = new CostoReparacion();
                        $costoReparacion->inventario_id = $inventarioEquipo->id;
                        $costoReparacion->fecha_factura = saveDate($request->fecha_factura);
                        $costoReparacion->numero_factura = $request->factura;
                        $costoReparacion->precio_unitario = saveFloat($request->unitario);
                        $costoReparacion->total = saveFloat($request->totalP);
                        $costoReparacion->save();
                    }

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Registrado Correctamente";
                    $result['data'] = $inventarioEquipo;
                    $result['data']['estado'] = $html_estado;

                    return $result;
                }elseif($request->isMethod('put')) {
                    $irreparable = (isset($request->irreparable)) ? true : false;
                    if ($irreparable == true) {
                        $request->validate([
                            'fecha_factura' => 'required',
                            'factura' => 'required',
                        ]);
                        if(saveFloat($request->unitario) == 0 || saveFloat($request->totalP) == 0 ){
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = "Debe ingresar una cantidad válida";
                            $result['data'] = [];

                            return $result;
                        }
                    }
                    if ($request->estado == 1) {
                        $estado = true;
                        $html_estado = '1';
                    }elseif ($request->estado == false){
                        $estado = false;
                        $html_estado = '0';
                    }else{
                        $estado = null;
                        $html_estado = '-1';
                    }
                    $inventarioEquipo = InventarioEquipo::with('concesiones')->find(Encryptor::decrypt($request->equipo_id));
                    // var_dump($request);
                    $inventarioEquipo->serial = $request->serial;
                    $inventarioEquipo->modelo = $request->modelo;
                    $inventarioEquipo->cant = $request->cantidad;
                    $inventarioEquipo->descripcion = $request->descripcion;
                    $inventarioEquipo->concesion_equipo_id = Encryptor::decrypt($request->concesion);
                    $inventarioEquipo->ubicacion_actual = $request->ubicacion_actual;
                    $inventarioEquipo->estado = $estado;
                    $inventarioEquipo->user_id = Auth::user()->id;
                    $inventarioEquipo->fecha_registro = now();
                    $inventarioEquipo->ip = $this->getIp();
                    $inventarioEquipo->save();

                    if ($irreparable == true) {
                        $costoReparacion = CostoReparacion::where('inventario_id',$inventarioEquipo->id)->first();
                        $costoReparacion->inventario_id = $inventarioEquipo->id;
                        $costoReparacion->fecha_factura = saveDate($request->fecha_factura);
                        $costoReparacion->numero_factura = $request->factura;
                        $costoReparacion->precio_unitario = saveFloat($request->unitario);
                        $costoReparacion->total = saveFloat($request->totalP);
                        $costoReparacion->save();
                    }

                    $inventarioEquipo = InventarioEquipo::with('concesiones')->find(Encryptor::decrypt($request->equipo_id));


                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Actualizado Correctamente";
                    $result['data'] = $inventarioEquipo;
                    $result['data']['estado'] = $html_estado;

                    return $result;
                }
            }
        }else{
            return Redirect::to('/dashboard');
        }
    }
    public function get_inventario(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $query = InventarioEquipo::with('concesiones', 'costo_reparacion')->orderBy('id', 'asc');

                $inventario = $query->get();

                $data = Datatables::of($inventario)
                        ->addIndexColumn()
                        ->addColumn('boton', function ($row) {
                            $idEquipos = $row->toArray();
                            $btnAction = null;
                            $idEquipos = isset($idEquipos['concesion']['crypt_id']) ? $idEquipos['concesion']['crypt_id'] : '';
                            if ($row->estado === true) {
                                $estado = '1';
                            }elseif($row->estado === false){
                                $estado = '0';
                            }else{
                                $estado = '-1';
                            }
                            if ($row->irreparable == true) {
                                $fecha_factura = $row->costo_reparacion->fecha_factura;
                                $numero_factura = $row->costo_reparacion->numero_factura;
                                $precio_unitario = $row->costo_reparacion->precio_unitario;
                                $total = $row->costo_reparacion->total;
                            }else{
                                $fecha_factura = null;
                                $numero_factura = null;
                                $precio_unitario = null;
                                $total = null;
                                $btnAction = '<button type="button" onclick="editModal(\'' . $row->crypt_id . '\', \'' . $row->serial . '\', \'' . $row->modelo . '\', \'' . $row->descripcion . '\', \'' . $row->cant . '\', \'' . $row->concesiones->crypt_id . '\', \'' . $row->ubicacion_actual . '\', \'' . $estado . '\', \''. $row->irreparable . '\', \''. $fecha_factura . '\', \''. $numero_factura . '\', \''. $precio_unitario . '\', \''. $total . '\' )" class="ml-2 btn btn-info btn-xs"  title="Modificar"><i class="far fa-edit"></i></button>';
                                $btnAction .= '<button type="button" onclick="setAlarmModal(\'' . $row->crypt_id . '\', \'' . $row->serial . '\', \'' . $row->modelo . '\', \'' . $row->descripcion . '\', \'' . $row->cant . '\', \'' . $row->concesiones->crypt_id . '\', \'' . $row->ubicacion_actual . '\', \'' . $estado . '\' )" class="ml-2 btn btn-warning btn-xs"  title="Establecer alarma"><i class="far fa-clock"></i></button>';
                            }

                            return  $btnAction;
                        })
                        // ->addColumn('class', function ($row) {
                        //     $class = "";
                        //     if ($row->verificado === true) {
                        //          $class = "alert-success";
                        //     } else {
                        //         if ($row->verificado === false) {
                        //             $class = "alert-warning";
                        //         } else {
                        //             $class = "alert-danger";

                        //         }
                        //     }
                        //     return $class;
                        // })
                        ->addColumn('class', function ($row) {
                            $class = "";
                            if ($row->irreparable === true) {
                                $class = "alert-danger";
                            } else {
                                $class = "alert-success";
                            }
                            return $class;
                        })
                        ->rawColumns(['boton'])
                //->make(true)
                ;

                return $data->toJson();
            }else {
                if ($request->isMethod('post')) {
                    $concesionEquipo = [];
                    $request->validate([
                        'nombre_concesion' => 'required'
                    ]);
                    $checkConcesion = ConcesionEquipo::where('nombre', Upper($request->nombre_concesion))->get();

                    if ($checkConcesion->count() > 0) {
                        $response = [
                            'type' => 'info',
                            'message' => __('La concesión ingresada ya se encuentra registrada, por favor verifique.'),
                            'data' => $concesionEquipo
                        ];
                    }else{
                        $concesionEquipo = new ConcesionEquipo();
                        $concesionEquipo->nombre = Upper($request->nombre_concesion);
                        $concesionEquipo->save();

                        $response = [
                            'type' => 'success',
                            'message' => __('Concesión registrada correctamente.'),
                            'data' => $concesionEquipo
                        ];
                    }
                    return response()->json($response);
                }
            }
        }
    }
    public function search_concesion(Request $request){
        $concesionEquipos = ConcesionEquipo::where('nombre', Upper($request->nombre_concesion))->get();

        if ($concesionEquipos->count() > 0) {
            $response = [
                'type' => 'info',
                'message' => __('La concesión ya se encuentra registrada, por favor verifique.'),
                'data' => $concesionEquipos
            ];
        }else {
            $response = [
                'type' => 'success',
                'message' => __('La concesión no se encuentra registrada, por favor regístrela.'),
                'data' => []
            ];
        }
        return response()->json($response);
    }

    public function ExcelInventarioEquipo(Request $request){
        // $aeropuerto_id = Encryptor::decrypt(request('aeropuerto_id'));
        // $rango_fecha = request('rango_fecha');
        // $rango_fecha = request('rango_fecha');
        // $fechas = explode(' - ', $rango_fecha);
        // $fechaInicio = $fechas[0];
        // $fechaFin = $fechas[1];
        // return Excel::download(new ProformasExport(saveDate($fechaInicio), saveDate($fechaFin), $aeropuerto_id), 'proforma.xlsx');
        return Excel::download(new InventarioEquipoExport($request->filtro), 'proforma.xlsx');
    }
    public function set_alarm(Request $request, $id = null){
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $query = AlarmaInventarioDetalle::orderBy('id', 'asc');

                $alarma = $query->get();

                $data = Datatables::of($alarma)
                    ->addIndexColumn()
                    ->addColumn('boton', function ($row) {
                        $alarma_id = $row->toArray();

                        $alarma_id = isset($alarma_id['crypt_id']) ? $alarma_id['crypt_id'] : '';

                        $btnAction = '<button type="button" onclick="setAlarmModal(\'' . $row->crypt_id . '\')" class="ml-2 btn btn-info btn-xs"  title="Modificar"><i class="far fa-edit"></i></button>';

                        return  $btnAction;
                    })
                    ->addColumn('fecha', function ($row) {
                        return  showDate($row->fecha_revision);
                    })
                    // ->addColumn('class', function ($row) {
                    //     $class = "";
                    //     if ($row->verificado === true) {
                    //          $class = "alert-success";
                    //     } else {
                    //         if ($row->verificado === false) {
                    //             $class = "alert-warning";
                    //         } else {
                    //             $class = "alert-danger";

                    //         }
                    //     }
                    //     return $class;
                    // })
                    ->rawColumns(['boton'])
                    //->make(true)
                ;

                return $data->toJson();
            }else{
                if($request->isMethod('post')){
                    $request->validate([
                        'inventario_equipo_id' => 'required',
                        'fecha_revision' => 'required',
                        'descripcion' => 'required',
                    ]);

                    $alarma = new AlarmaInventarioDetalle();
                    $alarma->inventario_equipo_id = Encryptor::decrypt($request->inventario_equipo_id);
                    $alarma->fecha_revision = $request->fecha_revision;
                    $alarma->descripcion = $request->descripcion;
                    $alarma->save();

                    $response = [
                        'type' => 'success',
                        'message' => __('Alarma registrada correctamente.'),
                        'data' => $alarma
                    ];
                    return response()->json($response);
                }elseif($request->isMethod('put')){
                    $request->validate([
                        'fecha_revision' => 'required',
                        'descripcion' => 'required',
                        'alarm_id' => 'required',
                    ]);
                    $alarma = AlarmaInventarioDetalle::find(Encryptor::decrypt($request->alarm_id));

                    $alarma->fecha_revision = $request->fecha_revision;
                    $alarma->descripcion = $request->descripcion;
                    $alarma->save();

                    $response = [
                        'type' => 'success',
                        'message' => 'Alerta actualizada exitosamente.',
                        'data' => []
                    ];
                    return response()->json($response);
                }
                elseif ($request->isMethod('delete')) {
                    $request->validate([
                        'id' => 'required',
                    ]);
                    $alarma = AlarmaInventarioDetalle::find(Encryptor::decrypt($request->id));
                    $alarma->delete();

                    $response = [
                        'type' => 'success',
                        'message' => 'Alerta eliminada exitosamente.',
                        'data' => []
                    ];
                    return response()->json($response);
                }
            }
        }
    }
    public function search_alarm($id){
        $alarma = AlarmaInventarioDetalle::where('inventario_equipo_id', Encryptor::decrypt($id))->get();

        // if ($alarma->count() > 0) {
        //     $response = [
        //         'type' => 'success',
        //         'message' => 'Alerta encontrada exitosamente.',
        //         'data' => $alarma
        //     ];
        // }else{
        //     $response = [
        //         'type' => 'error',
        //         'message' => 'Alerta no ha sido encontrada.',
        //         'data' => []
        //     ];
        // }
        // return response()->json($response);

        $data = Datatables::of($alarma)
            ->addIndexColumn()
            ->addColumn('boton', function ($row) {
                $idEquipos = $row->toArray();
                // dd($idEquipos);

                $idEquipos = isset($idEquipos['crypt_id']) ? $idEquipos['crypt_id'] : '';

                $btnAction = '<button type="button" onclick="editAlarmModal(\'' . $row->crypt_id . '\', \'' . showDate($row->fecha_revision) . '\', \'' . $row->descripcion . '\')" class="ml-2 btn btn-info btn-xs"  title="Modificar"><i class="far fa-edit"></i></button>';

                if ($row->irreparable == false) {
                    $btnAction .= '<button type="button" onclick="deleteAlarmModal(\'' . $row->crypt_id . '\')" class="ml-2 btn btn-danger btn-xs"  title="Eliminar alarma"><i class="far fa-trash-alt"></i></button>';
                    $btnAction .= '<button type="button" onclick="setObservacionModal()" class="ml-2 btn btn-success btn-xs"  title="Añadir observación"><i class="fas fa-calendar-check"></i></button>';
                }

                return  $btnAction;
            })
            ->addColumn('fecha', function ($row){
                return showDate($row->fecha_revision);
            })
            ->rawColumns(['boton'])
        ;
        return $data->toJson();
    }
}

