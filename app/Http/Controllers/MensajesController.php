<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Mensajes;

class MensajesController extends Controller {

    public function params(Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {



                $Mensajes = Mensajes::where("para_id", Auth::user()->id)->where("visto", false)->get();
                if ($Mensajes->count() > 0) {
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Mensajes');
                    $result['data'] = $Mensajes;
                } else {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Sin Mensajes');
                }


                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/');
        }
    }

}
