<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\AeropuertoAerolinea;
use App\Models\Aeropuertos;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Models\Clientes as Aerolineas;
class AeropuertosController extends Controller {

    public function api_aeropuertos(Request $request) {
        if (\Request::isMethod('post')) {
            //dd($request->ip());
            /*
             * Falta Rango de Fechas
             * 
             */
            if (1 == 1) {
                //if ($request->ip() == '54.86.50.139') {
                //dd($request->header()['authorization'][0]);
                if ($request->header()['authorization'][0] == "123") {

                    $where1[] = [DB::raw('1'), 1];
                    if (isset($request->aeropuerto)) {
                        $where1[] = ['codigo_oaci', $request->aeropuerto];
                    }

                    $ListAeropuertos = \App\Models\Aeropuertos::where("activo", "1")
                            ->where("pertenece_baer", 1)
                            ->where($where1)
                            ->orderBy('id', "asc")
                            ->get()
                            ->pluck('nombre', 'codigo_oaci')
                            ->toArray()
                    ;
                    //dd($ListAeropuertos);

                    $where2[] = [DB::raw('1'), 1];
                    if (isset($request->aeropuerto)) {
                        $where2[] = ['maest_aeropuertos.codigo_oaci', $request->aeropuerto];
                    }

                    if (isset($request->f1)) {
                        $f1 = $request->f1;
                        $where2[] = [function ($q) use ($f1) {
                                $q->where('proc_vuelos.fecha_operacion', '>=', $f1);
                            }];
                    }

                    if (isset($request->f2)) {
                        $f2 = $request->f2;
                        $where2[] = [function ($q) use ($f2) {
                                $q->where('proc_vuelos.fecha_operacion', '<=', $f2);
                            }];
                    }

                    $Vuelos = \App\Models\Aeropuertos::leftJoin("proc_vuelos", function ($join) {
                                $join->on("proc_vuelos.aeropuerto_id", "=", "maest_aeropuertos.id");
                                $join->on("proc_vuelos.activo", '=', DB::raw('true'));
                            })
                            ->where("maest_aeropuertos.activo", "1")
                            ->where("maest_aeropuertos.pertenece_baer", 1)
                            ->where($where2)
                            ->selectRaw("maest_aeropuertos.id, maest_aeropuertos.codigo_oaci,  count(proc_vuelos.id) as cant")
                            ->groupBy("maest_aeropuertos.id", "maest_aeropuertos.codigo_oaci")
                            ->orderBy("maest_aeropuertos.id", "asc")
                            ->get()
                            //->toSql()
                            ->pluck("cant", 'codigo_oaci')
                            ->toArray()
                    ;
                    //  dd($Vuelos);
                    arsort($Vuelos);
                    $data = array();
                    $ListColors = [];
                    $cods = [];
                    $cants = [];
                    $Aeropuertos = [];
                    foreach ($Vuelos as $key => $value) {
                        if ($value > 0) {
                            $ListColors[] = 'blue';
                            $cods[] = $key;
                            $cants[] = $value;

                            $Aeropuertos[$key] = $ListAeropuertos[$key];
                        }
                    }



                    $Naves = \App\Models\Aeronaves::leftJoin("proc_vuelos", function ($join) {
                                $join->on("proc_vuelos.aeronave_id", "=", "maest_aeronaves.id");
                                $join->on("proc_vuelos.activo", '=', DB::raw('true'));
                            })
                            ->leftJoin("maest_aeropuertos", 'maest_aeropuertos.id', '=', 'proc_vuelos.aeropuerto_id')
                            ->where($where2)
                            ->selectRaw("maest_aeronaves.id, maest_aeronaves.matricula, maest_aeronaves.nombre,  count(proc_vuelos.id) as cant")
                            ->groupBy("maest_aeronaves.id", "maest_aeronaves.matricula", "maest_aeronaves.nombre")
                            ->orderBy("cant", "desc")
                            ->take(20)
                            ->get()
                            ->pluck("cant", 'full_nombre_tn')
                            ->toArray()
                    ;
                    arsort($Naves);

                    $Estados = \App\Models\Estados::leftJoin("maest_aeropuertos", "maest_aeropuertos.estado_id", "=", "conf_estados.id")
                            ->leftJoin("proc_vuelos", function ($join) {
                                $join->on("proc_vuelos.aeropuerto_id", "=", "maest_aeropuertos.id");
                                $join->on("proc_vuelos.activo", '=', DB::raw('true'));
                            })
                            ->where($where2)
                            ->selectRaw("conf_estados.id, conf_estados.name_state, count(distinct(proc_vuelos.id)) as cant")
                            ->groupBy("conf_estados.id", "conf_estados.name_state")
                            ->orderBy("cant", "desc")
                            ->get()
                            ->pluck("cant", 'name_state')
                            ->toArray()
                    ;

                    return array("codigo" => $cods, "cantidades" => $cants, "colors" => $ListColors, "aeropuertos" => $Aeropuertos, "Naves" => $Naves, "Estados" => $Estados);
                } else {
                    return array();
                }
            } else {
                return array();
            }
        }
    }

    public function ver_mapa(Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {

                $InfoSeguridad = \App\Models\InfoSeguridad::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                $InfoCertificacion = \App\Models\InfoCertificaciones::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                $InfoOperaciones = \App\Models\InfoOperaciones::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                $InfoBomberos = \App\Models\InfoBomberos::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                return view('aeropuertos.ver_mapa', compact('InfoSeguridad', 'InfoCertificacion', 'InfoOperaciones', 'InfoBomberos'));
            } else {
                if (\Request::isMethod('post')) {
                    switch ($request->form) {
                        case 'seguridad':
                            $this->Seguridad($request);
                            break;
                        case 'certificacion':
                            $this->Certificacion($request);
                            break;
                        case 'operaciones':
                            $this->Operaciones($request);
                            break;
                        case 'bomberos':
                            $this->Bomberos($request);
                            break;
                    }

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            if (\Request::isMethod('put')) {
                $pdf = true;
                $Aeropuerto1 = Aeropuertos::find(Auth::user()->aeropuerto_id);
                switch ($request->form) {
                    case 'seguridad':
                        $InfoSeguridad = \App\Models\InfoSeguridad::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                        return PDF::loadView("aeropuertos.caracteristicas.seguridad", compact('InfoSeguridad', 'pdf', 'Aeropuerto1'))->stream("seguridad.pdf");
                        break;
                    case 'certificacion':
                        $InfoCertificacion = \App\Models\InfoCertificaciones::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                        return PDF::loadView("aeropuertos.caracteristicas.certificacion", compact('InfoCertificacion', 'pdf', 'Aeropuerto1'))->stream("certificacion.pdf");

                        break;
                    case 'operaciones':
                        $InfoOperaciones = \App\Models\InfoOperaciones::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                        return PDF::loadView("aeropuertos.caracteristicas.operaciones", compact('InfoOperaciones', 'pdf', 'Aeropuerto1'))
                                        ->setPaper('a4', 'landscape')
                                        ->stream("operaciones.pdf")

                        ;

                        break;
                    case 'bomberos':
                        $InfoBomberos = \App\Models\InfoBomberos::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
                        return PDF::loadView("aeropuertos.caracteristicas.ssei", compact('InfoBomberos', 'pdf', 'Aeropuerto1'))
                                        ->setPaper('a4', 'landscape')
                                        ->stream("bomberos.pdf")

                        ;

                        break;
                }
            }
            return Redirect::to('/');
        }
    }

    private function Bomberos($request) {
        $Bomberos = \App\Models\InfoBomberos::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
        $tipo = "actualizacion";
        if ($Bomberos == null) {
            $Bomberos = new \App\Models\InfoBomberos();
            $tipo = "creacion";
        }

        $Bomberos->baer_cant = $request->baer_cant; //1. HORAS DE FUNCIONAMIENTO
        $Bomberos->otros_cant = $request->otros_cant;
        $Bomberos->requi_cant = $request->requi_cant;
        
        $Bomberos->bomba_a = $request->bomba_a;
        $Bomberos->hidro_a = $request->hidro_a;
        $Bomberos->tanqu_a = $request->tanqu_a;
        
        $Bomberos->mam1_vci = $request->mam1_vci;
        $Bomberos->mam2_vci = $request->mam2_vci;
        $Bomberos->mam3_vci = $request->mam3_vci;
        $Bomberos->mam4_vci = $request->mam4_vci;
        $Bomberos->mam5_vci = $request->mam5_vci;
        $Bomberos->mam6_vci = $request->mam6_vci;
        $Bomberos->mam_obs1_vci = $request->mam_obs1_vci;
        $Bomberos->mam_obs2_vci = $request->mam_obs2_vci;
        $Bomberos->mam_obs3_vci = $request->mam_obs3_vci;
        $Bomberos->mam_obs4_vci = $request->mam_obs4_vci;
        $Bomberos->mam_obs5_vci = $request->mam_obs5_vci;
        $Bomberos->mam_obs6_vci = $request->mam_obs6_vci;
        $Bomberos->mam1_ambu = $request->mam1_ambu; //2. PERSONAL Y EQUIPO DE AVSEV
        $Bomberos->mam2_ambu = $request->mam2_ambu;
        $Bomberos->mam3_ambu = $request->mam3_ambu;
        $Bomberos->mam4_ambu = $request->mam4_ambu;
        $Bomberos->mam5_ambu = $request->mam5_ambu;
        $Bomberos->mam6_ambu = $request->mam6_ambu;
        $Bomberos->mam_obs1_ambu = $request->mam_obs1_ambu;
        $Bomberos->mam_obs2_ambu = $request->mam_obs2_ambu;
        $Bomberos->mam_obs3_ambu = $request->mam_obs3_ambu;
        $Bomberos->mam_obs4_ambu = $request->mam_obs4_ambu;
        $Bomberos->mam_obs5_ambu = $request->mam_obs5_ambu;
        $Bomberos->mam_obs6_ambu = $request->mam_obs6_ambu;
        $Bomberos->con_a = $request->con_a;
        $Bomberos->con_b = $request->con_b;
        $Bomberos->con_c = $request->con_c;
        $Bomberos->pqseco = $request->pqseco;
        $Bomberos->traalu_dis = $request->traalu_dis;
        $Bomberos->traalu_req = $request->traalu_req;
        $Bomberos->traest_dis = $request->traest_dis;
        $Bomberos->traest_req = $request->traest_req;
        $Bomberos->eqprocres_dis = $request->eqprocres_dis;
        $Bomberos->eqprocres_req = $request->eqprocres_req;
        $Bomberos->sisci_op = $request->sisci_op;
        $Bomberos->sisci_inop = $request->sisci_inop; //boolean
        $Bomberos->sisci_nopose = $request->sisci_nopose;
        $Bomberos->herrdesc1 = $request->herrdesc1;
        $Bomberos->herrdesc2 = $request->herrdesc2;
        $Bomberos->herrdesc3 = $request->herrdesc3;
        $Bomberos->herrdesc4 = $request->herrdesc4;
        $Bomberos->herrdesc5 = $request->herrdesc5;
        $Bomberos->herrdesc6 = $request->herrdesc6;
        $Bomberos->herrdesc7 = $request->herrdesc7;
        $Bomberos->herrdesc8 = $request->herrdesc8;
        $Bomberos->herrdesc9 = $request->herrdesc9;
        $Bomberos->herrdesc10 = $request->herrdesc10;
        $Bomberos->herrcond1 = $request->herrcond1;
        $Bomberos->herrcond2 = $request->herrcond2;
        $Bomberos->herrcond3 = $request->herrcond3;
        $Bomberos->herrcond4 = $request->herrcond4;
        $Bomberos->herrcond5 = $request->herrcond5;
        $Bomberos->herrcond6 = $request->herrcond6;
        $Bomberos->herrcond7 = $request->herrcond7;
        $Bomberos->herrcond8 = $request->herrcond8;
        $Bomberos->herrcond9 = $request->herrcond9;
        $Bomberos->herrcond10 = $request->herrcond10;
        $Bomberos->ip = $request->ip();

        $Bomberos->user_id = Auth::user()->id;

        $Bomberos->aeropuerto_id = Auth::user()->aeropuerto_id;
        $Bomberos->save();
    }

    private function Operaciones($request) {
        // dd($request);
        $Operaciones = \App\Models\InfoOperaciones::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
        $tipo = "actualizacion";
        if ($Operaciones == null) {
            $Operaciones = new \App\Models\InfoOperaciones();
            $tipo = "creacion";
        }

        $Operaciones->orientacion = $request->orientacion;
        $Operaciones->largo = $request->largo;
        $Operaciones->ancho = $request->ancho;
        $Operaciones->pc1_pstas = $request->pc1_pstas;
        $Operaciones->pc2_pstas = $request->pc2_pstas;
        $Operaciones->pc3_pstas = $request->pc3_pstas;
        $Operaciones->tora = $request->tora;
        $Operaciones->toda = $request->toda;
        $Operaciones->asda = $request->asda;
        $Operaciones->lda = $request->lda;
        $Operaciones->flexible = $request->flexible;
        $Operaciones->rigido = $request->rigido;
        $Operaciones->otro = $request->otro;
        $Operaciones->demarcacion = $request->demarcacion;
        $Operaciones->condicion = $request->condicion;

        //2
        $Operaciones->agrietamiento = $request->agrietamiento;
        $Operaciones->ahuellamiento = $request->ahuellamiento;
        $Operaciones->disgregacion = $request->disgregacion;
        $Operaciones->cont_neumatica = $request->cont_neumatica;
        $Operaciones->piel_cocodrilo = $request->piel_cocodrilo;
        $Operaciones->pres_vegetacion = $request->pres_vegetacion;
        $Operaciones->detritos = $request->detritos;
        $Operaciones->drenaje = $request->drenaje;
        $Operaciones->otras = $request->otras;
        $Operaciones->otras_text = $request->otras_text;
        $Operaciones->comercial_area = $request->comercial_area;
        $Operaciones->comercial_puesto = $request->comercial_puesto;
        $Operaciones->comercial_pcn1 = $request->comercial_pcn1;
        $Operaciones->comercial_pavimento = $request->comercial_pavimento;
        $Operaciones->comercial_condicion = $request->comercial_condicion;
        $Operaciones->general_area = $request->general_area;
        $Operaciones->general_puesto = $request->general_puesto;
        $Operaciones->general_pcn2 = $request->general_pcn2;
        $Operaciones->general_pavimento = $request->general_pavimento;
        $Operaciones->general_condicion = $request->general_condicion;
        $Operaciones->carga_area = $request->carga_area;
        $Operaciones->carga_puesto = $request->carga_puesto;
        $Operaciones->carga_pcn3 = $request->carga_pcn3;
        $Operaciones->carga_pavimento = $request->carga_pavimento;
        $Operaciones->carga_condicion = $request->carga_condicion;

        //3
        $Operaciones->calle_a = $request->calle_a;
        $Operaciones->calle_b = $request->calle_b;
        $Operaciones->calle_c = $request->calle_c;
        $Operaciones->calle_d = $request->calle_d;
        $Operaciones->calle_e = $request->calle_e;
        $Operaciones->calle_f = $request->calle_f;
        $Operaciones->calle_g = $request->calle_g;
        $Operaciones->calle_h = $request->calle_h;
        $Operaciones->calle_i = $request->calle_i;
        $Operaciones->calle_j = $request->calle_j;
        $Operaciones->calle_k = $request->calle_k;

        //4
        if ($request->lbp === 'null') {
            $Operaciones->lbp = null;
        } else {
            $Operaciones->lbp = $request->lbp;
        }
        if ($request->lep === 'null') {
            $Operaciones->lep = null;
        } else {
            $Operaciones->lep = $request->lep;
        }
        if ($request->papi === 'null') {
            $Operaciones->papi = null;
        } else {
            $Operaciones->papi = $request->papi;
        }
        if ($request->lcr === 'null') {
            $Operaciones->lcr = null;
        } else {
            $Operaciones->lcr = $request->lcr;
        }
        if ($request->lp === 'null') {
            $Operaciones->lp = null;
        } else {
            $Operaciones->lp = $request->lp;
        }
        if ($request->lp2 === 'null') {
            $Operaciones->lp2 = null;
        } else {
            $Operaciones->lp2 = $request->lp2;
        }
        // $Operaciones->lbp = $request->lbp;
        // $Operaciones->lep = $request->lep;
        // $Operaciones->papi = $request->papi;
        // $Operaciones->lcr = $request->lcr;
        // $Operaciones->lp = $request->lp;
        // $Operaciones->lp2 = $request->lp2;
        $Operaciones->obs_balizaje = $request->obs_balizaje;

        //5
        if ($request->vor === 'null') {
            $Operaciones->vor = null;
        } else {
            $Operaciones->vor = $request->vor;
        }
        if ($request->dme === 'null') {
            $Operaciones->dme = null;
        } else {
            $Operaciones->dme = $request->dme;
        }
        if ($request->ils === 'null') {
            $Operaciones->ils = null;
        } else {
            $Operaciones->ils = $request->ils;
        }
        if ($request->ndb === 'null') {
            $Operaciones->ndb = null;
        } else {
            $Operaciones->ndb = $request->ndb;

        }
        if ($request->radio_o === 'null') {
            $Operaciones->radio_o = null;
        } else {
            $Operaciones->radio_o = $request->radio_o;
        }
        // $Operaciones->vor = $request->vor;
        // $Operaciones->dme = $request->dme;
        // $Operaciones->ils = $request->ils;
        // $Operaciones->ndb = $request->ndb;
        // $Operaciones->radio_o = $request->radio_o;
        $Operaciones->na_otras_esp = $request->na_otras_esp;

        //6
        $Operaciones->ser_comb_avgas_cap = $request->ser_comb_avgas_cap;
        $Operaciones->ser_comb_avgas_exis = $request->ser_comb_avgas_exis;
        $Operaciones->ser_comb_jeta_cap = $request->ser_comb_jeta_cap;
        $Operaciones->ser_comb_jeta_exis = $request->ser_comb_jeta_exis;
        $Operaciones->serv_comb_otro_cap = $request->serv_comb_otro_cap;
        $Operaciones->serv_comb_otro_exis = $request->serv_comb_otro_exis;
        $Operaciones->sev_comb_otro_text = $request->sev_comb_otro_text;
        $Operaciones->serv_comb_cs_op = $request->serv_comb_cs_op;
        $Operaciones->serv_comb_cs_ino = $request->serv_comb_cs_ino;
        $Operaciones->serv_comb_hdtt_op = $request->serv_comb_hdtt_op;
        $Operaciones->serv_comb_hdtt_ino = $request->serv_comb_hdtt_ino;
        $Operaciones->serv_comb_obs = $request->serv_comb_obs;

        //7
        $Operaciones->emp_serv_empre = $request->emp_serv_empre;
        $Operaciones->emp_serv_cant_equi = $request->emp_serv_cant_equi;

        //8
        $Operaciones->cerc_per_tip_mtl = $request->cerc_per_tip_mtl;
        $Operaciones->cerc_per_long_ttl = $request->cerc_per_long_ttl;
        $Operaciones->cerc_per_long_atl = $request->cerc_per_long_atl;
        $Operaciones->cerc_per_obs = $request->cerc_per_obs;

        //9
        $Operaciones->tml_pax_lbby = $request->tml_pax_lbby;
        $Operaciones->tml_pax_cheq = $request->tml_pax_cheq;
        $Operaciones->tml_pax_emb = $request->tml_pax_emb;
        $Operaciones->tml_pax_desm = $request->tml_pax_desm;
        $Operaciones->tml_pax_illum = $request->tml_pax_illum;
        $Operaciones->tml_pax_sani = $request->tml_pax_sani;
        $Operaciones->tml_pax_estaci = $request->tml_pax_estaci;
        $Operaciones->tml_pax_pot = $request->tml_pax_pot;
        $Operaciones->tml_pax_trat = $request->tml_pax_trat;
        $Operaciones->tml_pax_clima = $request->tml_pax_clima;
        $Operaciones->tml_pax_area_verde = $request->tml_pax_area_verde;
        $Operaciones->tml_pax_serv_trans = $request->tml_pax_serv_trans;

        //10
        if ($request->ssa === 'null') {
            $Operaciones->ssa = null;
        } else {
            $Operaciones->ssa = $request->ssa;
        }
        if ($request->saime === 'null') {
            $Operaciones->saime = null;
        } else {
            $Operaciones->saime = $request->saime;
        }
        if ($request->ve === 'null') {
            $Operaciones->ve = null;
        } else {
            $Operaciones->ve = $request->ve;
        }
        if ($request->sta === 'null') {
            $Operaciones->sta = null;
        } else {
            $Operaciones->sta = $request->sta;
        }
        if ($request->sa === 'null') {
            $Operaciones->sa = null;
        } else {
            $Operaciones->sa = $request->sa;
        }
        if ($request->vm === 'null') {
            $Operaciones->vm = null;
        } else {
            $Operaciones->vm = $request->vm;
        }
        if ($request->oais === 'null') {
            $Operaciones->oais = null;
        } else {
            $Operaciones->oais = $request->oais;
        }
        if ($request->insai === 'null') {
            $Operaciones->insai = null;
        } else {
            $Operaciones->insai = $request->insai;
        }
        if ($request->spe === 'null') {
            $Operaciones->spe = null;
        } else {
            $Operaciones->spe = $request->spe;
        }
        if ($request->sc === 'null') {
            $Operaciones->sc = null;
        } else {
            $Operaciones->sc = $request->sc;
        }
        if ($request->sds === 'null') {
            $Operaciones->sds = null;
        } else {
            $Operaciones->sds = $request->sds;
        }
        // $Operaciones->ssa = $request->ssa;
        // $Operaciones->saime = $request->saime;
        // $Operaciones->ve = $request->ve;
        // $Operaciones->sta = $request->sta;
        // $Operaciones->sa = $request->sa;
        // $Operaciones->vm = $request->vm;
        // $Operaciones->oais = $request->oais;
        // $Operaciones->insai = $request->insai;
        // $Operaciones->spe = $request->spe;
        // $Operaciones->sc = $request->sc;
        // $Operaciones->sds = $request->sds;

        $Operaciones->ip = $request->ip();
        $Operaciones->user_id = Auth::user()->id;
        $Operaciones->aeropuerto_id = Auth::user()->aeropuerto_id;

        $Operaciones->save();
    }

    private function Certificacion($request) {
        $Certificaciones = \App\Models\InfoCertificaciones::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
        $tipo = "actualizacion";
        if ($Certificaciones == null) {
            $Certificaciones = new \App\Models\InfoCertificaciones();
            $tipo = "creacion";
        }
        $Certificaciones->fase_all = $request->fase_all;
        $Certificaciones->f1 = $request->f1;
        $Certificaciones->ff1 = $request->ff1;
        $Certificaciones->f2 = $request->f2;
        $Certificaciones->ff2 = $request->ff2;
        $Certificaciones->f3 = $request->f3;
        $Certificaciones->ff3 = $request->ff3;
        $Certificaciones->f4 = $request->f4;
        $Certificaciones->ff4 = $request->ff4;
        $Certificaciones->f5 = $request->f5;
        $Certificaciones->ff5 = $request->ff5;

        $Certificaciones->c_sms = $request->c_sms;
        $Certificaciones->f_c_sms = $request->f_c_sms;

        $Certificaciones->c_avsev = $request->c_avsev;
        $Certificaciones->f_c_avsev = $request->f_c_avsev;

        $Certificaciones->c_mp = $request->c_mp;
        $Certificaciones->f_c_mp = $request->f_c_mp;

        $Certificaciones->c_tpd = $request->c_tpd;
        $Certificaciones->f_c_tpd = $request->f_c_tpd;

        $Certificaciones->p_pm = $request->p_pm;
        $Certificaciones->f_p_pm = $request->f_p_pm;

        $Certificaciones->p_pc = $request->p_pc;
        $Certificaciones->f_p_pc = $request->f_p_pc;

        $Certificaciones->p_pe = $request->p_pe;
        $Certificaciones->f_p_pe = $request->f_p_pe;

        $Certificaciones->p_tai = $request->p_tai;
        $Certificaciones->f_p_tai = $request->f_p_tai;

        $Certificaciones->p_cpaf = $request->p_cpaf;
        $Certificaciones->f_p_cpaf = $request->f_p_cpaf;

        $Certificaciones->p_ca = $request->p_ca;
        $Certificaciones->f_p_ca = $request->f_p_ca;

        $Certificaciones->p_gma = $request->p_gma;
        $Certificaciones->f_p_gma = $request->f_p_gma;

        $Certificaciones->p_cc = $request->p_cc;
        $Certificaciones->f_p_cc = $request->f_p_cc;

        $Certificaciones->p_lf = $request->p_lf;
        $Certificaciones->f_p_lf = $request->f_p_lf;

        $Certificaciones->p_sl = $request->p_sl;
        $Certificaciones->f_p_sl = $request->f_p_sl;

        $Certificaciones->e_ef = $request->e_ef;
        $Certificaciones->f_e_ef = $request->f_e_ef;

        $Certificaciones->e_e = $request->e_e;
        $Certificaciones->f_e_e = $request->f_e_e;

        $Certificaciones->mmpp_pc = $request->mmpp_pc;
        $Certificaciones->mmpp_psn = $request->mmpp_psn;
        $Certificaciones->mmpp_obs = $request->mmpp_obs;

        $Certificaciones->sms_pc = $request->sms_pc;
        $Certificaciones->sms_psn = $request->sms_psn;
        $Certificaciones->sms_obs = $request->sms_obs;

        $Certificaciones->ffhh_pc = $request->ffhh_pc;
        $Certificaciones->ffhh_psn = $request->ffhh_psn;
        $Certificaciones->ffhh_obs = $request->ffhh_obs;

        $Certificaciones->fac_pc = $request->fac_pc;
        $Certificaciones->fac_psn = $request->fac_psn;
        $Certificaciones->fac_obs = $request->fac_obs;

        $Certificaciones->conapdis = $request->conapdis;
        $Certificaciones->conapdis_obs = $request->conapdis_obs;

        $Certificaciones->fuc = $request->fuc;
        $Certificaciones->fec = $request->fec;
        $Certificaciones->npavsec = $request->npavsec;

        $Certificaciones->npadsa = $request->npadsa;
        $Certificaciones->npadss = $request->npadss;
        $Certificaciones->fccrae = $request->fccrae;
        $Certificaciones->fccrsv = $request->fccrsv;
        $Certificaciones->fcciae = $request->fcciae;
        $Certificaciones->fccisv = $request->fccisv;
        $Certificaciones->fsciae = $request->fsciae;
        $Certificaciones->fscisv = $request->fscisv;
        $Certificaciones->fscrae = $request->fscrae;
        $Certificaciones->fscrsv = $request->fscrsv;

        $Certificaciones->ip = $request->ip();
        $Certificaciones->user_id = Auth::user()->id;
        $Certificaciones->aeropuerto_id = Auth::user()->aeropuerto_id;
        $Certificaciones->save();

        $CertificacionesLog = new \App\Models\InfoCertificacionesLog();
        $CertificacionesLog->fase_all = $request->fase_all;
        $CertificacionesLog->f1 = $request->f1;
        $CertificacionesLog->ff1 = $request->ff1;
        $CertificacionesLog->f2 = $request->f2;
        $CertificacionesLog->ff2 = $request->ff2;
        $CertificacionesLog->f3 = $request->f3;
        $CertificacionesLog->ff3 = $request->ff3;
        $CertificacionesLog->f4 = $request->f4;
        $CertificacionesLog->ff4 = $request->ff4;
        $CertificacionesLog->f5 = $request->f5;
        $CertificacionesLog->ff5 = $request->ff5;

        $CertificacionesLog->c_sms = $request->c_sms;
        $CertificacionesLog->f_c_sms = $request->f_c_sms;

        $CertificacionesLog->c_avsev = $request->c_avsev;
        $CertificacionesLog->f_c_avsev = $request->f_c_avsev;

        $CertificacionesLog->c_mp = $request->c_mp;
        $CertificacionesLog->f_c_mp = $request->f_c_mp;

        $CertificacionesLog->c_tpd = $request->c_tpd;
        $CertificacionesLog->f_c_tpd = $request->f_c_tpd;

        $CertificacionesLog->p_pm = $request->p_pm;
        $CertificacionesLog->f_p_pm = $request->f_p_pm;

        $CertificacionesLog->p_pc = $request->p_pc;
        $CertificacionesLog->f_p_pc = $request->f_p_pc;

        $CertificacionesLog->p_pe = $request->p_pe;
        $CertificacionesLog->f_p_pe = $request->f_p_pe;

        $CertificacionesLog->p_tai = $request->p_tai;
        $CertificacionesLog->f_p_tai = $request->f_p_tai;

        $CertificacionesLog->p_cpaf = $request->p_cpaf;
        $CertificacionesLog->f_p_cpaf = $request->f_p_cpaf;

        $CertificacionesLog->p_ca = $request->p_ca;
        $CertificacionesLog->f_p_ca = $request->f_p_ca;

        $CertificacionesLog->p_gma = $request->p_gma;
        $CertificacionesLog->f_p_gma = $request->f_p_gma;

        $CertificacionesLog->p_cc = $request->p_cc;
        $CertificacionesLog->f_p_cc = $request->f_p_cc;

        $CertificacionesLog->p_lf = $request->p_lf;
        $CertificacionesLog->f_p_lf = $request->f_p_lf;

        $CertificacionesLog->p_sl = $request->p_sl;
        $CertificacionesLog->f_p_sl = $request->f_p_sl;

        $CertificacionesLog->e_ef = $request->e_ef;
        $CertificacionesLog->f_e_ef = $request->f_e_ef;

        $CertificacionesLog->e_e = $request->e_e;
        $CertificacionesLog->f_e_e = $request->f_e_e;

        $CertificacionesLog->ip = $request->ip();
        $CertificacionesLog->user_id = Auth::user()->id;
        $CertificacionesLog->aeropuerto_id = Auth::user()->aeropuerto_id;
        $CertificacionesLog->save();
    }

    private function Seguridad($request) {

        $ProInfoSeguridad = \App\Models\InfoSeguridad::where("aeropuerto_id", Auth::user()->aeropuerto_id)->first();
        $tipo = "actualizacion";

        if ($ProInfoSeguridad == null) {
            $ProInfoSeguridad = new \App\Models\InfoSeguridad();
            $tipo = "creacion";
        }

        $ProInfoSeguridad->s_aad = $request->s_aad; //1. HORAS DE FUNCIONAMIENTO
        $ProInfoSeguridad->s_ona = $request->s_ona;
        $ProInfoSeguridad->s_onm = $request->s_onm;
        $ProInfoSeguridad->s_ats = $request->s_ats;
        $ProInfoSeguridad->s_ac = $request->s_ac;
        $ProInfoSeguridad->s_seh = $request->s_seh;
        $ProInfoSeguridad->s_avsec = $request->s_avsec;
        $ProInfoSeguridad->s_gnb_a = $request->s_gnb_a;
        $ProInfoSeguridad->s_seniat = $request->s_seniat;
        $ProInfoSeguridad->s_saime = $request->s_saime;
        $ProInfoSeguridad->s_ds = $request->s_ds;
        $ProInfoSeguridad->s_gnb_rn = $request->s_gnb_rn;
        $ProInfoSeguridad->s_pnb = $request->s_pnb;
        $ProInfoSeguridad->s_sebin = $request->s_sebin;
        $ProInfoSeguridad->s_ns_avsec = $request->s_ns_avsec;
        $ProInfoSeguridad->s_na_avsec = $request->s_na_avsec; //2. PERSONAL Y EQUIPO DE AVSEV
        $ProInfoSeguridad->s_edmp_o = $request->s_edmp_o;
        $ProInfoSeguridad->s_edmp_i = $request->s_edmp_i;
        $ProInfoSeguridad->s_edmm_o = $request->s_edmm_o;
        $ProInfoSeguridad->s_edmm_i = $request->s_edmm_i;
        $ProInfoSeguridad->s_mrpc_o = $request->s_mrpc_o;
        $ProInfoSeguridad->s_mrpc_i = $request->s_mrpc_i;
        $ProInfoSeguridad->s_mb_o = $request->s_mb_o;
        $ProInfoSeguridad->s_mb_i = $request->s_mb_i;
        $ProInfoSeguridad->s_rp_o = $request->s_rp_o;
        $ProInfoSeguridad->s_rp_i = $request->s_rp_i;
        $ProInfoSeguridad->s_rb_o = $request->s_rb_o;
        $ProInfoSeguridad->s_rb_i = $request->s_rb_i;
        $ProInfoSeguridad->s_c_o = $request->s_c_o;
        $ProInfoSeguridad->s_c_i = $request->s_c_i;
        $ProInfoSeguridad->s_g_o = $request->s_g_o;
        $ProInfoSeguridad->s_g_i = $request->s_g_i;
        $ProInfoSeguridad->s_mech_o = $request->s_mech_o;
        $ProInfoSeguridad->s_mech_i = $request->s_mech_i;
        $ProInfoSeguridad->s_nsavseci = $request->s_nsavseci;
        $ProInfoSeguridad->s_nsavsecr = $request->s_nsavsecr;
        $ProInfoSeguridad->s_naavseci = $request->s_naavseci;
        $ProInfoSeguridad->s_naavsecr = $request->s_naavsecr;
        if ($request->s_cls === 'null') {
            $ProInfoSeguridad->s_cls = null;
        } else {
            $ProInfoSeguridad->s_cls = $request->s_cls;
        }
        if ($request->s_pls === 'null') {
            $ProInfoSeguridad->s_pls = null;
        } else {
            $ProInfoSeguridad->s_pls = $request->s_pls;
        }
        if ($request->s_pimsacad === 'null') {
            $ProInfoSeguridad->s_pimsacad = null;
        } else {
            $ProInfoSeguridad->s_pimsacad = $request->s_pimsacad;
        }
        if ($request->s_pccmsacad === 'null') {
            $ProInfoSeguridad->s_pccmsacad = null;
        } else {
            $ProInfoSeguridad->s_pccmsacad = $request->s_pccmsacad;
        }
        
        $ProInfoSeguridad->ip = $request->ip();
        $ProInfoSeguridad->user_id = Auth::user()->id;
        $ProInfoSeguridad->aeropuerto_id = Auth::user()->aeropuerto_id;
        $ProInfoSeguridad->save();

        /*         * **************** */
        $pdf = true;
        $Aeropuerto = Auth::user()->aeropuerto_id;
        $Aeropuerto1 = Aeropuertos::find($Aeropuerto);

        $InfoSeguridad = \App\Models\InfoSeguridad::where("aeropuerto_id", $Aeropuerto)->first();
        $pdf = PDF::loadView("aeropuertos.caracteristicas.seguridad", compact('InfoSeguridad', 'pdf', 'Aeropuerto1'));

        if ($tipo = "actualizacion") {
            /*
              $ProInfoSeguridad->s_aad = $request->s_aad;
              $ProInfoSeguridad->s_ona = $request->s_ona;
              $ProInfoSeguridad->s_onm = $request->s_onm;
              $ProInfoSeguridad->s_ats = $request->s_ats;
              $ProInfoSeguridad->s_ac = $request->s_ac;
              $ProInfoSeguridad->s_seh = $request->s_seh;
              $ProInfoSeguridad->s_avsec = $request->s_avsec;
              $ProInfoSeguridad->s_gnb_a = $request->s_gnb_a;
              $ProInfoSeguridad->s_seniat = $request->s_seniat;
              $ProInfoSeguridad->s_saime = $request->s_saime;
              $ProInfoSeguridad->s_ds = $request->s_ds;
              $ProInfoSeguridad->s_gnb_rn = $request->s_gnb_rn;
              $ProInfoSeguridad->s_pnb = $request->s_pnb;
              $ProInfoSeguridad->s_sebin = $request->s_sebin;
              $ProInfoSeguridad->s_ns_avsec = $request->s_ns_avsec;
              $ProInfoSeguridad->s_na_avsec = $request->s_na_avsec; //2. PERSONAL Y EQUIPO DE AVSEV
              $ProInfoSeguridad->s_edmp_o = $request->s_edmp_o;
              $ProInfoSeguridad->s_edmp_i = $request->s_edmp_i;
              $ProInfoSeguridad->s_edmm_o = $request->s_edmm_o;
              $ProInfoSeguridad->s_edmm_i = $request->s_edmm_i;
              $ProInfoSeguridad->s_mrpc_o = $request->s_mrpc_o;
              $ProInfoSeguridad->s_mrpc_i = $request->s_mrpc_i;
              $ProInfoSeguridad->s_mb_o = $request->s_mb_o;
              $ProInfoSeguridad->s_mb_i = $request->s_mb_i;
              $ProInfoSeguridad->s_rp_o = $request->s_rp_o;
              $ProInfoSeguridad->s_rp_i = $request->s_rp_i;
              $ProInfoSeguridad->s_rb_o = $request->s_rb_o;
              $ProInfoSeguridad->s_rb_i = $request->s_rb_i;
              $ProInfoSeguridad->s_c_o = $request->s_c_o;
              $ProInfoSeguridad->s_c_i = $request->s_c_i;
              $ProInfoSeguridad->s_g_o = $request->s_g_o;
              $ProInfoSeguridad->s_g_i = $request->s_g_i;
              $ProInfoSeguridad->s_mech_o = $request->s_mech_o;
              $ProInfoSeguridad->s_mech_i = $request->s_mech_i;
              $ProInfoSeguridad->s_nsavseci = $request->s_nsavseci;
              $ProInfoSeguridad->s_nsavsecr = $request->s_nsavsecr;
              $ProInfoSeguridad->s_naavseci = $request->s_naavseci;
              $ProInfoSeguridad->s_naavsecr = $request->s_naavsecr;
              $ProInfoSeguridad->s_cls = $request->s_cls; //boolean
              $ProInfoSeguridad->s_pls = $request->s_pls;
              $ProInfoSeguridad->s_pimsacad = $request->s_pimsacad;
              $ProInfoSeguridad->s_pccmsacad = $request->s_pccmsacad;

             */
        }




        $emails = "oficina.seguridad.integra@baer.gob.ve";
        /*
          $emails = [];
          $emails[] = "mrivero@baer.gob.ve";
          $emails[] = "ofigueroa@baer.gob.ve";
          $emails[] = "oj.figueroa@gmail.com";
          $emails[] = "miguelrivero52@gmail.com";
         */
        $subject = "Registro de Seguridad del Aeropuerto";
        $for = $emails;
        $datos = $InfoSeguridad->toArray();
        $datos['aeropuerto'] = $Aeropuerto1->toArray();

        $datos['tipo_registro'] = $tipo;
        $datos['modulo'] = 'Seguridad Aeroportuaria';

        Mail::send('email.send_info_aeropuerto', $datos, function ($msj) use ($subject, $for, $pdf, $InfoSeguridad, $datos) {

            $msj->subject($subject);
            $msj->to($for);
            $msj->attachData($pdf->output(), $datos['aeropuerto']['codigo_oaci'] . "_SEGURIDAD.pdf");
        });
        /*         * ******************* */
        /*
        $ProInfoSeguridadLog = new \App\Models\InfoSeguridadLog();
        $ProInfoSeguridadLog->s_aad = $request->s_aad; //1. HORAS DE FUNCIONAMIENTO
        $ProInfoSeguridadLog->s_ona = $request->s_ona;
        $ProInfoSeguridadLog->s_onm = $request->s_onm;
        $ProInfoSeguridadLog->s_ats = $request->s_ats;
        $ProInfoSeguridadLog->s_ac = $request->s_ac;
        $ProInfoSeguridadLog->s_seh = $request->s_seh;
        $ProInfoSeguridadLog->s_avsec = $request->s_avsec;
        $ProInfoSeguridadLog->s_gnb_a = $request->s_gnb_a;
        $ProInfoSeguridadLog->s_seniat = $request->s_seniat;
        $ProInfoSeguridadLog->s_saime = $request->s_saime;
        $ProInfoSeguridadLog->s_ds = $request->s_ds;
        $ProInfoSeguridadLog->s_gnb_rn = $request->s_gnb_rn;
        $ProInfoSeguridadLog->s_pnb = $request->s_pnb;
        $ProInfoSeguridadLog->s_sebin = $request->s_sebin;
        $ProInfoSeguridadLog->s_ns_avsec = $request->s_ns_avsec;
        $ProInfoSeguridadLog->s_na_avsec = $request->s_na_avsec; //2. PERSONAL Y EQUIPO DE AVSEV
        $ProInfoSeguridadLog->s_edmp_o = $request->s_edmp_o;
        $ProInfoSeguridadLog->s_edmp_i = $request->s_edmp_i;
        $ProInfoSeguridadLog->s_edmm_o = $request->s_edmm_o;
        $ProInfoSeguridadLog->s_edmm_i = $request->s_edmm_i;
        $ProInfoSeguridadLog->s_mrpc_o = $request->s_mrpc_o;
        $ProInfoSeguridadLog->s_mrpc_i = $request->s_mrpc_i;
        $ProInfoSeguridadLog->s_mb_o = $request->s_mb_o;
        $ProInfoSeguridadLog->s_mb_i = $request->s_mb_i;
        $ProInfoSeguridadLog->s_rp_o = $request->s_rp_o;
        $ProInfoSeguridadLog->s_rp_i = $request->s_rp_i;
        $ProInfoSeguridadLog->s_rb_o = $request->s_rb_o;
        $ProInfoSeguridadLog->s_rb_i = $request->s_rb_i;
        $ProInfoSeguridadLog->s_c_o = $request->s_c_o;
        $ProInfoSeguridadLog->s_c_i = $request->s_c_i;
        $ProInfoSeguridadLog->s_g_o = $request->s_g_o;
        $ProInfoSeguridadLog->s_g_i = $request->s_g_i;
        $ProInfoSeguridadLog->s_mech_o = $request->s_mech_o;
        $ProInfoSeguridadLog->s_mech_i = $request->s_mech_i;
        $ProInfoSeguridadLog->s_nsavseci = $request->s_nsavseci;
        $ProInfoSeguridadLog->s_nsavsecr = $request->s_nsavsecr;
        $ProInfoSeguridadLog->s_naavseci = $request->s_naavseci;
        $ProInfoSeguridadLog->s_naavsecr = $request->s_naavsecr;
        $ProInfoSeguridadLog->s_cls = $request->s_cls; //boolean
        $ProInfoSeguridadLog->s_pls = $request->s_pls;
        $ProInfoSeguridadLog->s_pimsacad = $request->s_pimsacad;
        $ProInfoSeguridadLog->s_pccmsacad = $request->s_pccmsacad;
        $ProInfoSeguridadLog->ip = $request->ip();
        $ProInfoSeguridadLog->user_id = Auth::user()->id;
        $ProInfoSeguridadLog->aeropuerto_id = Auth::user()->aeropuerto_id;
        $ProInfoSeguridadLog->tipo = $tipo;
        $ProInfoSeguridadLog->save();
         * 
         */
    }

    public function ver_info_mapa() {

        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Estados = \App\Models\Estados::get();
                $Aeropuertos = Aeropuertos::where("pertenece_baer", 1)->orderBy("nombre")->get();
                
                return view('aeropuertos.ver_info_mapa', compact('Estados', 'Aeropuertos'));
            } else {
                exit;
            }
        } else {

            if (\Request::isMethod('put')) {
                $request = \Request::all();
                $pdf = true;
                $Aeropuerto = $request['aeropuerto'];
                
                $Aeropuerto1 = Aeropuertos::find(Encryptor::decrypt($Aeropuerto));
                switch ($request['form']) {
                    case 'seguridad':
                        $InfoSeguridad = \App\Models\InfoSeguridad::where("aeropuerto_id", Encryptor::decrypt($Aeropuerto))->first();
                        if (Auth::user()->aeropuerto_id == null) {
                            if ($InfoSeguridad!=null){
                                $InfoSeguridad->new_info = false;
                                $InfoSeguridad->save();
                            }
                        }

                        return PDF::loadView("aeropuertos.caracteristicas.seguridad", compact('InfoSeguridad', 'pdf', 'Aeropuerto1'))->stream("seguridad.pdf");
                        break;
                    case 'certificacion':
                        $InfoCertificacion = \App\Models\InfoCertificaciones::where("aeropuerto_id", Encryptor::decrypt($Aeropuerto))->first();
                        if (Auth::user()->aeropuerto_id == null) {
                            if ($InfoCertificacion!=null){
                                $InfoCertificacion->new_info = false;
                                $InfoCertificacion->save();
                            }
                        }
                        return PDF::loadView("aeropuertos.caracteristicas.certificacion", compact('InfoCertificacion', 'pdf', 'Aeropuerto1'))->stream("certificacion.pdf");

                        break;
                    case 'operaciones':
                        $InfoOperaciones = \App\Models\InfoOperaciones::where("aeropuerto_id", Encryptor::decrypt($Aeropuerto))->first();
                        if (Auth::user()->aeropuerto_id == null) {
                            if ($InfoOperaciones!=null){
                                $InfoOperaciones->new_info = false;
                                $InfoOperaciones->save();
                            }
                        }
                        return PDF::loadView("aeropuertos.caracteristicas.operaciones", compact('InfoOperaciones', 'pdf', 'Aeropuerto1'))->stream("operaciones.pdf");

                        break;
                    case 'ssei':
                        $InfoBomberos = \App\Models\InfoBomberos::where("aeropuerto_id", Encryptor::decrypt($Aeropuerto))->first();
                        if (Auth::user()->aeropuerto_id == null) {
                            if ($InfoBomberos!=null){
                                $InfoBomberos->new_info = false;
                                $InfoBomberos->save();
                            }
                        }
                        return PDF::loadView("aeropuertos.caracteristicas.ssei", compact('InfoBomberos', 'pdf', 'Aeropuerto1'))->stream("ssei.pdf");

                        break;
                    default:
                        return '<h1>SIN INFORMACIÓN </h1>';
                        break;
                }
            }
            return Redirect::to('/');
        }
    }

    function index() {
        if (\Request::ajax()) {
            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Aereolineas_active = Aeropuertos::where("activo", 1)->get();

            $Aereolineas_disable = Aeropuertos::where("activo", 0)->get();
            $Aereolineas_active = Datatables::of($Aereolineas_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.edit', $row->crypt_id) . '" title="Editar" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> </a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.disable', $row->crypt_id) . '" title="Inactivar" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> </a> ';
                        }
                        if (in_array("aerolineas", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.airlines', $row->crypt_id) . '" class=" btn btn-warning btn-sm"><li class="fa fa-plane"></li> </a> ';
                        }

                        if ($row->pertenece_baer == true) {
                            if (in_array("holidays", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.holidays', $row->crypt_id) . '" title="Registrar Feriados al Aeropuerto" class=" btn btn-info btn-sm"><li class="fa fa-calendar"></li> </a> ';
                            }
                            if (in_array("ubicaciones", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.ubicaciones', $row->crypt_id) . '"  class=" btn btn-success btn-sm"><li class="fa fa-sort-down"></li> ' . __('Ubicacioes') . '</a> ';
                            }
                            if (in_array("lugares", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.lugares', $row->crypt_id) . '" class=" btn btn-info btn-sm"><li class="fa fa-sort-down"></li> ' . __('Lugares') . '</a> ';
                            }
                            if (in_array("puestos", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.puestos', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-sort-down"></li> ' . __('Puestos') . '</a> ';
                            }
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $Aereolineas_disable = Datatables::of($Aereolineas_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeropuertos.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Aereolineas_active->original['data'];
            $data2 = $Aereolineas_disable->original['data'];

            return view('aeropuertos.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'codigo_oaci' => 'required|unique:maest_aeropuertos,codigo_oaci',
                            'codigo_iata' => 'required|unique:maest_aeropuertos,codigo_iata',
                            'nombre' => 'required',
                            'categoria_id' => 'required'
                                ], [
                            'codigo_oaci.required' => __('El Codigo OACI es Requerido'),
                            'codigo_oaci.unique' => __('El Codigo OACI Esta Registrado'),
                            'codigo_iata.unique' => __('El Codigo IATA esta Registrado'),
                            'codigo_iata.required' => __('El Codigo IATA es Requerido'),
                            'nombre.required' => __('El Nombre es Requerido'),
                            'categoria_id.required' => __('La Categoria es Requerido'),
                            'pais_id.required' => __('El País es Requerido'),
                            'estado_id.required' => __('El Estado es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeropuertos = new Aeropuertos();
                    $Aeropuertos->codigo_oaci = $request->codigo_oaci;
                    $Aeropuertos->codigo_iata = $request->codigo_iata;
                    $Aeropuertos->nombre = $request->nombre;
                    $Aeropuertos->categoria_id = $request->categoria_id;
                    $Aeropuertos->estado_id = $request->estado_id;
                    $Aeropuertos->pais_id = $request->pais_id;

                    $Aeropuertos->pertenece_baer = $request->pertenece_baer;
                    $Aeropuertos->iva = $request->iva;

                    $Aeropuertos->hora_ocaso = $request->hora_ocaso;
                    $Aeropuertos->hora_amanecer = $request->hora_amanecer;

                    $Aeropuertos->user_id = Auth::user()->id;
                    $Aeropuertos->ip = $this->getIp();
                    $Aeropuertos->fecha_registro = now();

                    $Aeropuertos->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $clasificaciones = \App\Models\Categorias::pluck("nombre", "id");
                $estados = \App\Models\Estados::pluck("name_state", "id");
                $paises = \App\Models\Paises::pluck("name_country", "id");
                return view('aeropuertos.create', compact('clasificaciones', 'estados', 'paises'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function holidays($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {



                \App\Models\FeriadosAeropuertos::where("aeropuerto_id", Encryptor::decrypt($id))->delete();

                foreach ($request->data as $value) {



                    $FeriadosAeropuertos = new \App\Models\FeriadosAeropuertos();
                    $FeriadosAeropuertos->aeropuerto_id = Encryptor::decrypt($id);
                    $FeriadosAeropuertos->nombre = $value['nombre'];
                    $FeriadosAeropuertos->fecha = $this->saveDate($value['fecha']);

                    $FeriadosAeropuertos->user_id = Auth::user()->id;
                    $FeriadosAeropuertos->ip = $this->getIp();
                    $FeriadosAeropuertos->fecha_registro = now();

                    $FeriadosAeropuertos->save();
                }





                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $aereopuerto = Aeropuertos::find(Encryptor::decrypt($id));
                //dd($aereopuerto->getFeriados);
                return view('aeropuertos.holidays', compact('aereopuerto'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'codigo_oaci' => 'required|unique:maest_aeropuertos,codigo_oaci,' . Encryptor::decrypt($id),
                            'codigo_iata' => 'required|unique:maest_aeropuertos,codigo_iata,' . Encryptor::decrypt($id),
                            'nombre' => 'required',
                            'categoria_id' => 'required'
                                ], [
                            'codigo_oaci.required' => __('El Codigo OACI es Requerido'),
                            'codigo_iata.unique' => __('El Codigo IATA es Requerido'),
                            'codigo_oaci.unique' => __('El Codigo OACI Esta Registrado'),
                            'nombre.required' => __('El Nombre es Requerido'),
                            'categoria_id.required' => __('La Clasificacion es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeropuertos = Aeropuertos::find(Encryptor::decrypt($id));
                    $Aeropuertos->codigo_oaci = $request->codigo_oaci;
                    $Aeropuertos->codigo_iata = $request->codigo_iata;
                    $Aeropuertos->nombre = $request->nombre;
                    $Aeropuertos->categoria_id = $request->categoria_id;
                    $Aeropuertos->estado_id = $request->estado_id;
                    $Aeropuertos->pais_id = $request->pais_id;
                    
                    $Aeropuertos->hora_ocaso = $request->hora_ocaso;
                    $Aeropuertos->hora_amanecer = $request->hora_amanecer;

                    $Aeropuertos->pertenece_baer = $request->pertenece_baer;
                    $Aeropuertos->iva = $request->iva;

                    $Aeropuertos->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $aereopuerto = Aeropuertos::find(Encryptor::decrypt($id));
                $estados = \App\Models\Estados::orderBy("name_state")->pluck("name_state", "id");
                $clasificaciones = \App\Models\Categorias::pluck("nombre", "id");
                $paises = \App\Models\Paises::pluck("name_country", "id");
                return view('aeropuertos.edit', compact('aereopuerto', 'clasificaciones', 'estados', 'paises'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Aeropuertos = Aeropuertos::find(Encryptor::decrypt($id));
                $Aeropuertos->activo = 0;

                $Aeropuertos->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $aereopuerto = Aeropuertos::find(Encryptor::decrypt($id));
                return view('aeropuertos.disable', compact('aereopuerto'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Aeropuertos = Aeropuertos::find(Encryptor::decrypt($id));
                $Aeropuertos->activo = 1;

                $Aeropuertos->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $aereopuerto = Aeropuertos::find(Encryptor::decrypt($id));
                return view('aeropuertos.active', compact('aereopuerto'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function airlines($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                    $request->all(), [
                        'aerolineas' => 'required|array',                    
                    ], [
                        'aerolineas.required' => __('La aerolinea es Requerida'),                        
                    ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    AeropuertoAerolinea::where("aeropuerto_id", Encryptor::decrypt($id))->delete();
    
                    foreach ($request->aerolineas as $value) {
                        $AeropuertoAerolinea = new AeropuertoAerolinea();
                        $AeropuertoAerolinea->aeropuerto_id = Encryptor::decrypt($id);
                        $AeropuertoAerolinea->aerolinea_id = Encryptor::decrypt($value);

                        $AeropuertoAerolinea->save();
                    }

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
    
                    return $result;
                }

            } else {
                $aereopuerto = Aeropuertos::find(Encryptor::decrypt($id));
                $Aerolineas = Aerolineas::where("activo", 1)->where('tipo_id', $this->TIPOS_CLIENTE_AEROLINEA)->get()->pluck("razon_social", "crypt_id");
                
                
                $AerolineaSelected = AeropuertoAerolinea::where("aeropuerto_id", Encryptor::decrypt($id))->get()->pluck('aerolinea_crypt_id')->toArray();
                // dd($AerolineaSelected);
                
                return view('aeropuertos.airlines', compact('aereopuerto', 'Aerolineas', 'AerolineaSelected'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function search_airlines(Request $request){
        $aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
        if (AeropuertoAerolinea::where('aeropuerto_id', $aeropuerto_id)->exists() === true) {
            $aeropuerto = AeropuertoAerolinea::with('aerolineas')
                ->where("aeropuerto_id", $aeropuerto_id)
                ->get()
            ;
            $aerolineas = [];
            foreach ($aeropuerto as $value) {
                array_push($aerolineas, $value->aerolineas);
            }

            $response = [
                'type' => 'success',
                'message' => '',
                'data' => $aerolineas,
            ];
            return response()->json($response, 200);
        }else {
            $response = [
                'type' => 'info',
                'message' => 'El aeropuerto no tiene aerolineas asignadas',
                'data' => [],
            ];
            return response()->json($response, 200);
        }
    }

}
