<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use Illuminate\Support\Str;
use App\Models\Aeropuertos;
use App\Models\Ubicaciones;
use App\Models\Lugares;

class LugaresController extends Controller {

    public function lugares($id, Request $request) {


        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                //dd($request->all());

                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            'ubicacion_id' => 'required',
                                ], [
                            'nombre.required' => __('El nombre es Requerido'),
                            'ubicacion_id.required' => __('La Ubicación es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Lugares = new Lugares();
                    $Lugares->ubicacion_id = Encryptor::decrypt($request->ubicacion_id);
                    $Lugares->nombre = Upper($request->nombre);

                    $Lugares->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                if ($request->isMethod('delete')) {

                    $Lugares = Lugares::find(Encryptor::decrypt($id));
                    $Lugares->activo = false;
                    $Lugares->save();
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    return $result;
                } else {
                    $Aeropuertos = Aeropuertos::find(Encryptor::decrypt($id));
                    $Ubicaciones = Ubicaciones::where("activo", 1)->where('aeropuerto_id', Encryptor::decrypt($id))->get()->pluck("nombre", "crypt_id");
                    $Lugares = Lugares::join("maest_ubicaciones", "maest_ubicaciones.id", "=", "maest_lugares.ubicacion_id")
                            ->where("maest_lugares.activo", 1)
                            ->where("maest_ubicaciones.activo", 1)
                            ->where('maest_ubicaciones.aeropuerto_id', Encryptor::decrypt($id))
                            ->select("maest_lugares.id", "maest_lugares.nombre", "maest_ubicaciones.nombre as ubicacion")
                            ->get()
                            ->toArray();
                  //  dd($Lugares);

                    return view('aeropuertos.lugares', compact('Aeropuertos', 'Ubicaciones', 'Lugares'));
                }
            }
        } else {
            return Redirect::to('/');
        }
    }

}
