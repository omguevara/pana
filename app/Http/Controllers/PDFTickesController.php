<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
// use Barryvdh\DomPDF\Facade as PDF;
use PDF;


class PDFTickesController extends Controller
{
    public function generatePdf()
    {
        //$Tickes = \App\Models\VentaTicketsPago::with('getMetodoPago')->get();
        $usuario = auth()->user();
        $aeropuerto = \App\Models\Aeropuertos::find($usuario->aeropuerto_id);
        $nombre_aeropuerto = $aeropuerto->nombre;
        $fecha_actual = date('Y-m-d');
        $aeropuerto_id = $usuario->aeropuerto_id;
        $Tickes = \App\Models\VentaTickets::with('detalle.ticket.lote.tiempo', 'usuarios')
        ->whereDate('fecha', $fecha_actual)
        ->where('aeropuerto_id', $aeropuerto_id)
        ->get();
    //    dd($Tickes->toArray());

        $hideBotonPrint = true;
        $pdf = PDF::loadView('tickets.pdf_ventas', compact('Tickes', 'hideBotonPrint', 'nombre_aeropuerto', 'fecha_actual'));
        // return $pdf->download('archivo.pdf');
        return $pdf->stream('archivo.pdf');

        // return view('tickets.pdf_ventas', compact('Tickes', 'hideBotonPrint', 'nombre_aeropuerto'));
    }
}
