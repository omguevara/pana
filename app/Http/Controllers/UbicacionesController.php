<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use Illuminate\Support\Str;
use App\Models\Aeropuertos;
use App\Models\Ubicaciones;

class UbicacionesController extends Controller
{
    
    public function ubicaciones($id, Request $request) {
        

        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                //dd($request->all());

                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            
                                ], [
                            'nombre.required' => __('El nombre es Requerido')
                            
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Ubicaciones = new Ubicaciones();
                    $Ubicaciones->aeropuerto_id = Encryptor::decrypt($id);
                    $Ubicaciones->nombre = Upper($request->nombre);
                    

                    $Ubicaciones->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                if ($request->isMethod('delete')) {
                    
                    $Ubicaciones  = Ubicaciones::find(Encryptor::decrypt($id));
                    $Ubicaciones->activo= false;
                    $Ubicaciones->save();
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    return $result;
                } else {
                    $Aeropuertos = Aeropuertos::find(Encryptor::decrypt($id));
                    $Ubicaciones = Ubicaciones::where("activo", 1)->where('aeropuerto_id', Encryptor::decrypt($id))->get()->toArray();

                    
                    
                    return view('aeropuertos.ubicaciones', compact('Aeropuertos', 'Ubicaciones'));
                }
            }
        } else {
            return Redirect::to('/');
        }
    }
    
}
