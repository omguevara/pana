<?php

namespace App\Http\Controllers;

use App\Models\Concesiones;
use App\Exports\ConcesionesExport;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Locales;

class ExportController extends Controller
{
    public function ExcelHistorial($id){

        return Excel::download(new ConcesionesExport($id), 'Local.xlsx');
        // $concesiones = Concesiones::where('local_id', 1)->with('empresas', 'locales.usos_local', 'actividades')->get();
        // // dd($concesiones);
        // $spreadsheet = new Spreadsheet();
        // $sheet = $spreadsheet->getActiveSheet();
        // $sheet->setCellValue('A3', 'Nombre');
        // $sheet->setCellValue('B3', 'Correo electrónico');
        // $sheet->setCellValue('C3', 'Fecha de creación');

        // $data = [];
        // foreach ($concesiones as $concesion) {
        //     $data[] = [
        //         $concesion->local_id,
        //         $concesion->empresa_id,
        //         // $user->created_at->format('Y-m-d H:i:s')
        //     ];
        // }
        // $sheet->fromArray($data, null, 'A5');

        // $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
		// $drawing->setName('Paid');
		// $drawing->setDescription('Paid');
		// // $drawing->setPath('banner.jpeg'); // put your path and image here
		// $drawing->setCoordinates('B15');
		// $drawing->setOffsetX(110);
		// $drawing->setRotation(25);
		// $drawing->getShadow()->setVisible(true);
		// $drawing->getShadow()->setDirection(45);
		// $drawing->setWorksheet($spreadsheet->getActiveSheet());
        
        // $drawing = new Drawing();
        // $drawing->setName('Logo');
        // $drawing->setDescription('Logo');
        // $drawing->setPath('banner.jpeg');
        // $drawing->setCoordinates('A1');
        // $drawing->setOffsetX(5);
        // $drawing->setOffsetY(5);
        // $drawing->setWidth(100);
        // $drawing->setHeight(100);
        
        // $sheet->getRowDimension('1')->setRowHeight(100);
        // $sheet->getColumnDimension('A')->setWidth(25);
        
        // $styleArray = array(
        //     'font'  => array(
        //         'bold'  => true,
        //         'color' => array('rgb' => 'FF0000'),
        //         'size'  => 15,
        //         'name'  => 'Verdana'
        //     ));
        // $sheet->getStyle('A1')->applyFromArray($styleArray);
        // $sheet
        // ->getStyle('A1:C1')
        //  ->getFill()
        //  ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
        //  ->getStartColor()
        //  ->setARGB('000000');

        // $spreadsheet->getActiveSheet()->getProtection()->setSheet(true); 
        
		
		// $spreadsheet->getActiveSheet()->getProtection()->setPassword('123465');
		// $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
		// $spreadsheet->getActiveSheet()->getProtection()->setSort(false);
		// $spreadsheet->getActiveSheet()->getProtection()->setInsertRows(false);
		// $spreadsheet->getActiveSheet()->getProtection()->setFormatCells(false);

        // $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(50);

        

        
        


        // $writer = new Xlsx($spreadsheet);
        // $writer->save('user_list.xlsx');

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="user_list.xlsx"');
        header('Cache-Control: max-age=0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
