<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Exports\ExportFacturas;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\VuelosGenerales;

class PospagoVuelosGeneralesController extends Controller {

    public function facturacion(Request $request) {
        if ($request->ajax()) {
            if ($request->isMethod('post')) {
                $servicios_facturar = json_decode($request->servicios_facturar);

                //

                /*
                  $extras = [];
                  if (isset($request->prod_servd)) {
                  foreach ($request->prod_servd as $value) {
                  $d = explode(":", $value);
                  $d_extras[$d[1]] = $d[0];
                  $extras[] = Encryptor::decrypt($d[1]);
                  }
                  }


                  $data['tipo_vuelo_id'] = 2; //Vuelos General
                  $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                  $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);



                  $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                  //$data['fecha_vuelo'] = $this->saveDate($request->fecha) . " 00:00:00";
                  //Carbon::createFromFormat('Y-m-d H:i:s', '2016-01-23 11:53:20');





                  $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                  $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                  $data['plan_vuelo'] = 2; // Doble Toque
                  $data['cant_pasajeros'] = $request->cant_pasajeros;

                  if ($request->applyParking != null) {
                  $data['hora_llegada'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format("Y-m-d H:i");
                  $data['hora_salida'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format("Y-m-d H:i");
                  }





                  $data['exento_dosa'] = !$request->cobrar_dosa;
                  $data['exento_tasa'] = !$request->cobrar_tasa;
                  //dd($data);
                  $prodServ = $this->getProdServ($data);

                  $prodserv_extra = [];
                  if (count($extras) > 0) {
                  $prodserv_extra = $this->getProdservicios2('extra', $data, $extras);
                  foreach ($prodserv_extra as $value) {
                  $value['cant'] = $d_extras[$value['crypt_id']];
                  $prodServ['data'][] = $value;
                  }
                  }

                  if (count($prodServ['data']) > 0) {

                 */
                $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha);
                $VuelosGeneralesNumControl = \App\Models\VuelosGenerales::where("activo", 1)
                        ->where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                        ->orderBy("id", "DESC")
                        ->first();
                $VuelosGeneralesNumControl = $VuelosGeneralesNumControl == null ? 1 : $VuelosGeneralesNumControl->numero_operacion + 1;

                $VuelosGenerales = new \App\Models\VuelosGenerales();

                $VuelosGenerales->fecha_registro = now();
                $VuelosGenerales->fecha_llegada = $fecha_vuelo->format("Y-m-d H:m");
                $VuelosGenerales->user_id = Auth::user()->id;
                $VuelosGenerales->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                ;
                $VuelosGenerales->numero_operacion = $VuelosGeneralesNumControl;
                $VuelosGenerales->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                $VuelosGenerales->piloto_llegada_id = Encryptor::decrypt($request->piloto_id);
                $VuelosGenerales->hora_llegada = "";
                $VuelosGenerales->tipo_llegada_id = $request->nacionalidad_id;

                //$VuelosGenerales->procedencia_id = 0;
                $VuelosGenerales->pax_desembarcados = $request->cant_pasajeros;
                $VuelosGenerales->piloto_salida_id = Encryptor::decrypt($request->piloto_id);
                $VuelosGenerales->fecha_salida = $fecha_vuelo->format("Y-m-d H:m");

                $VuelosGenerales->exento_tasa = $request->cobrar_tasa == 1 ? false : true;
                $VuelosGenerales->exento_dosa = $request->cobrar_dosa == 1 ? false : true;

                //$VuelosGenerales->destino_id = Encryptor::decrypt($request->origen_id);
                $VuelosGenerales->pax_embarcados = $request->cant_pasajeros;
                $VuelosGenerales->observaciones_operaciones = $request->observacion;
                $VuelosGenerales->observaciones_facturacion = "";

                $VuelosGenerales->activo = 1;
                //$VuelosGenerales->prodservicios_extra = 0;


                $VuelosGenerales->ip = $this->getIp();
                //$VuelosGenerales->finalizado = now();

                $VuelosGenerales->save();

                //dd($servicios_facturar);
                foreach ($servicios_facturar as $value) {
                    //var_dump($value);
                    $Detalle = new \App\Models\VuelosGeneralesDetalle();

                    $Detalle->aplica_categoria = $value->aplica_categoria;
                    $Detalle->aplica_estacion = $value->aplica_estacion;
                    $Detalle->aplica_peso = $value->aplica_peso;
                    $Detalle->aplicable = $value->aplicable;
                    $Detalle->bs = $value->bs;
                    $Detalle->cant = $value->cant;
                    $Detalle->categoria_aplicada = $value->categoria_aplicada;
                    $Detalle->codigo = $value->codigo;
                    $Detalle->prodservicio_id = Encryptor::decrypt($value->crypt_id);
                    $Detalle->default_carga = $value->default_carga;
                    $Detalle->default_dosa = $value->default_dosa;
                    $Detalle->default_tasa = $value->default_tasa;
                    $Detalle->descripcion = $value->descripcion;
                    $Detalle->formula = $value->formula;
                    $Detalle->formula2 = $value->formula2;
                    $Detalle->full_descripcion = $value->full_descripcion;
                    $Detalle->iva = $value->iva;
                    $Detalle->iva2 = $value->iva2;
                    $Detalle->iva_aplicado = $value->iva_aplicado;
                    $Detalle->moneda = $value->moneda;
                    $Detalle->nacionalidad_id = $value->nacionalidad_id;
                    $Detalle->nomenclatura = $value->nomenclatura;
                    $Detalle->peso_inicio = $value->peso_inicio;
                    $Detalle->peso_fin = $value->peso_fin;
                    $Detalle->precio = $value->precio;
                    $Detalle->prodservicio_extra = $value->prodservicio_extra;
                    $Detalle->tasa = $value->tasa;
                    $Detalle->tipo_vuelo_id = $value->tipo_vuelo_id;
                    $Detalle->turno_id = $value->turno_id;
                    $Detalle->valor_dollar = $value->valor_dollar;
                    $Detalle->valor_euro = $value->valor_euro;
                    $Detalle->valor_petro = $value->valor_petro;
                    $Detalle->vuelo_id = $VuelosGenerales->id;

                    $Detalle->save();
                }          //json_decode($request->servicios_facturar)

                $result['data'] = ["id" => ""];
                if (isset($request->facturar)) {

                    $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                        $Cliente->tipo_documento = $request->type_document;
                        $Cliente->documento = $request->document;

                        $Cliente->user_id = Auth::user()->id;
                        $Cliente->ip = $this->getIp();
                        $Cliente->fecha_registro = now();
                    }

                    $Cliente->razon_social = Upper($request->razon);
                    $Cliente->telefono = $request->phone;
                    $Cliente->correo = $request->correo;
                    $Cliente->direccion = $request->direccion;
                    $Cliente->tipo_id = 4; // general

                    $Cliente->save();

                    $EURO = $this->getEuro();

                    $Factura = new \App\Models\Facturas();

                    $Factura->fecha_factura = now();
                    $Factura->cliente_id = $Cliente->id;
                    $Factura->nro_factura = 18;
                    $Factura->nro_documento = 18;

                    $Factura->moneda_aplicada = 2;
                    $Factura->monto_moneda_aplicada = $EURO;
                    $Factura->formato_factura = "reportes.facturas.formato_general";
                    $Factura->observacion = "";
                    $Factura->tipo_id = 3; // MIXTA (TASA Y DOSA);
                    $Factura->save();

                    foreach ($servicios_facturar as $value) {
                        $Detalle = new \App\Models\FacturasDetalle();

                        $Detalle->cantidad = $value->cant;
                        $Detalle->factura_id = $Factura->id;
                        $Detalle->codigo = $value->codigo;
                        $Detalle->descripcion = $value->descripcion . ' ' . $value->iva2;
                        $Detalle->prodservicio_id = Encryptor::decrypt($value->crypt_id);
                        $Detalle->nomenclatura = $value->nomenclatura;

                        $Detalle->precio = $value->bs;
                        $Detalle->precio2 = $value->precio;

                        $Detalle->iva = $value->iva_aplicado;
                        $Detalle->save();
                    }



                    $VuelosGenerales->factura_tasa = $Factura->id;
                    $VuelosGenerales->factura_dosa = $Factura->id;
                    $VuelosGenerales->save();

                    $result['data'] = ["id" => $Factura->crypt_id];
                }
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Vuelo Registrado Realizada');

                return $result;
            } else {
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
                //$Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre_tn", "crypt_id");


                $AeronavesAll = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get();
                $Aeronaves = [];
                $AeronavesProp = [];
                foreach ($AeronavesAll as $value) {
                    $Aeronaves[$value->crypt_id] = $value->full_nombre_tn;
                    $AeronavesProp[$value->crypt_id] = ["data-base" => $value->estacion_crypt_id];
                }

                //
                if (Auth::user()->profile_id == 13 || Auth::user()->profile_id == 1) {// SI ES RECAUDACION DE BAER * MASTER
                    /*
                      $Aeropuertos = \App\Models\Aeropuertos:://::join('conf_taquillas','conf_taquillas.aeropuerto_id','=','maest_aeropuertos.id')
                      where('maest_aeropuertos.activo', true)
                      ->where('maest_aeropuertos.id', '!=', $this->TORTUGA_ID)
                      ->get()
                      ->pluck("full_nombre", "crypt_id");
                     */

                    $AeropuertosAll = \App\Models\Aeropuertos::leftJoin('conf_taquillas', 'conf_taquillas.aeropuerto_id', '=', 'maest_aeropuertos.id')
                            ->where('maest_aeropuertos.activo', true)
                            //->where('maest_aeropuertos.id', '!=', $this->TORTUGA_ID)
                            ->where('maest_aeropuertos.pertenece_baer', '=', true)
                            ->select("maest_aeropuertos.*", "conf_taquillas.id as taquilla_id", "conf_taquillas.numero_factura", "conf_taquillas.numero_control")
                            ->get()
                            ->toArray()
                    ;
                    $Aeropuertos = [];
                    $AeropuertosProp = [];
                    foreach ($AeropuertosAll as $value) {
                        $Aeropuertos[$value['crypt_id']] = $value['full_nombre'];
                        if ($value['taquilla_id'] == null) {
                            $AeropuertosProp[$value['crypt_id']] = ["disabled" => "disabled", "title" => "Aeropuerto sin Taquilla Asignada"];
                        } else {
                            $AeropuertosProp[$value['crypt_id']] = ["data-numero_factura" => showCode($value['numero_factura']), "data-numero_control" => showCode($value['numero_control'])];
                        }
                    }
                } else {
                    $AeropuertosAll = \App\Models\Aeropuertos::leftJoin('conf_taquillas', 'conf_taquillas.aeropuerto_id', '=', 'maest_aeropuertos.id')
                            ->where('maest_aeropuertos.activo', true)
                            //->where('maest_aeropuertos.id', '!=', $this->TORTUGA_ID)
                            ->where('maest_aeropuertos.id', '=', Auth::user()->aeropuerto_id)
                            ->select("maest_aeropuertos.*", "conf_taquillas.id as taquilla_id", "conf_taquillas.numero_factura", "conf_taquillas.numero_control")
                            ->get()
                            ->toArray()
                    ;
                    $Aeropuertos = [];
                    $AeropuertosProp = [];
                    foreach ($AeropuertosAll as $value) {
                        $Aeropuertos[$value['crypt_id']] = $value['full_nombre'];
                        if ($value['taquilla_id'] == null) {
                            $AeropuertosProp[$value['crypt_id']] = ["disabled" => "disabled", "title" => "Aeropuerto sin Taquilla Asignada"];
                        } else {
                            $AeropuertosProp[$value['crypt_id']] = ["data-numero_factura" => showCode($value['numero_factura']), "data-numero_control" => showCode($value['numero_control'])];
                        }
                    }
                    $TAQUILLA = $this->getDataTaquilla(Encryptor::decrypt(Auth::user()->aeropuerto_id));
                    if ($TAQUILLA != null) {
                        $msg = __('El Aeropuerto no Posee Taquilla Creada');
                        return view('errors.general', compact('Pilotos', 'Aeronaves', 'Aeropuertos'));
                    }
                }


                //dd(key($Aeropuertos));

                return view('pospago_aviacion_general.create', compact('Pilotos', 'Aeronaves', 'AeronavesProp', 'Aeropuertos', 'AeropuertosProp'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_piloto(Request $request) {
        // dd("dddd");
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'name_user' => 'required',
                            'surname_user' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'name_user.required' => __('El nombre es Requerido'),
                            'surname_user.required' => __('El Apellido es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Pilotos = \App\Models\Pilotos::where("documento", $request->document)->count();

                    if ($Pilotos == 0) {
                        $Pilotos = new \App\Models\Pilotos();
                        $Pilotos->tipo_documento = $request->type_document;
                        $Pilotos->documento = $request->document;
                        $Pilotos->nombres = Upper($request->name_user);
                        $Pilotos->apellidos = Upper($request->surname_user);
                        $Pilotos->telefono = $request->phone;
                        $Pilotos->correo = $request->email;

                        /*
                          $Aeronaves->user_id = Auth::user()->id;
                          $Aeronaves->ip = $this->getIp();
                          $Aeronaves->fecha_registro = now();
                         */
                        $Pilotos->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = array("id" => $Pilotos->crypt_id, 'full_nombre' => $Pilotos->full_name);
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function add_nave(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $values = $request->all();
                $values['matricula'] = Upper($values['matricula']);
                $Validator = \Validator::make(
                                $values, [
                            'peso_maximo' => 'required',
                            'matricula' => 'required|unique:maest_aeronaves,matricula',
                            'nombre' => 'required',
                                ], [
                            'peso_maximo.required' => __('El Peso Maximo es Requerido'),
                            'matricula.required' => __('La Matrícula es Requerida'),
                            'matricula.unique' => __('La Matrícula ya Esta Registrada'),
                            'nombre.required' => __('El Modelo es Requerido'),
                                ]
                );
                //dd("sss");
                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = \App\Models\Aeronaves::where("matricula", $request->matricula)->count();

                    if ($Aeronaves == 0) {
                        $Aeronaves = new \App\Models\Aeronaves();

                        $Aeronaves->nombre = Upper($request->nombre);
                        $Aeronaves->matricula = Upper($request->matricula);
                        $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                        $Aeronaves->maximo_pasajeros = 0;

                        $Aeronaves->user_id = Auth::user()->id;
                        $Aeronaves->ip = $this->getIp();
                        $Aeronaves->fecha_registro = now();

                        $Aeronaves->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = array("id" => $Aeronaves->crypt_id, 'full_nombre' => $Aeronaves->getFullNombreAttribute());
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function get_serv(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $extras = [];
                if (isset($request->prod_servd)) {
                    $data['minutos'] = [];
                    foreach ($request->prod_servd as $value) {
                        $d = explode(":", $value);
                        if ($d[0] != "") {
                            $d_extras[$d[1]] = $d[0];
                            $extras[] = Encryptor::decrypt($d[1]);

                            $s = \App\Models\Prodservicios::find(Encryptor::decrypt($d[1]));
                            if (str_contains($s->formula, '[MINUTOS]')) {
                                $data['minutos'][] = [Encryptor::decrypt($d[1]), $d[0]];
                            }
                            // dd($data['minutos']);
                        }
                    }
                }
                // dd($d_extras);

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);

                $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha);

                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                //$data['fecha_vuelo'] = $this->saveDate($request->fecha) . " 00:00:00";
                //Carbon::createFromFormat('Y-m-d H:i:s', '2016-01-23 11:53:20');





                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['plan_vuelo'] = 2; // Doble Toque
                $data['cant_pasajeros'] = $request->cant_pasajeros;

                if ($request->applyParking != null) {
                    $data['hora_llegada'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format("Y-m-d H:i");
                    $data['hora_salida'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format("Y-m-d H:i");
                }





                $data['exento_dosa'] = !$request->cobrar_dosa;
                $data['exento_tasa'] = !$request->cobrar_tasa;

                //dd($data);


                $diaSemana = Carbon::createFromFormat("Y-m-d H:i:s", $data['fecha_vuelo']);

                $isHoliday = \App\Models\Feriados::where("fecha", $diaSemana)->count();
                $setFeriado = false;

                if ($diaSemana->dayOfWeekIso == 7 || $isHoliday > 0) {//si es domingo o es feriado
                    $setFeriado = true;
                    
                }

                if ($setFeriado == false) {
                    $f = Carbon::createFromFormat("Y-m-d H:i:s", $data['fecha_vuelo'])->format("Y-m-d");
                    if (in_array($f, $Aeropuerto->feriados->pluck("fecha")->toArray())) {
                        $setFeriado = true;
                    }
                }

                $prodServ = $this->getProdServ($data);

                $prodserv_extra = [];
                if (count($extras) > 0) {
                    $prodserv_extra = $this->getProdservicios2('extra', $data, $extras);
                    foreach ($prodserv_extra as $value) {
                        $value['cant'] = $d_extras[$value['crypt_id']];
                        
                        $value['bs'] = $value['bs'] * 1.2;
                        $value['precio'] = $value['precio'] * 1.2;
                        $value['formula2'] = $value['formula2'] . " * ALTA MOV.(+20%) ";
                        
                        $prodServ['data'][] = $value;
                    }
                }

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly111');
                $result['data'] = $prodServ['data'];

                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function get_serv_extra(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha);
                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);

                $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha);
                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";

                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['plan_vuelo'] = 2; // Doble Toque
                $data['cant_pasajeros'] = $request->cant_pasajeros;

                if ($request->applyParking != null) {
                    $data['hora_llegada'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format("Y-m-d H:i");
                    $data['hora_salida'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format("Y-m-d H:i");
                }

                $data['exento_dosa'] = !$request->cobrar_dosa;
                $data['exento_tasa'] = !$request->cobrar_tasa;

                $prodserv_extra = $this->getProdservicios2('extra', $data);

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $prodserv_extra;

                return $result;
            }
        }
    }

    public function facturar_pospago(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $fechas = explode(" - ", $request->rango);
                $fecha_desde = $this->saveDate($fechas[0]);
                $fecha_hasta = $this->saveDate($fechas[1]);

                $datos = \App\Models\VuelosGenerales::where("aeronave_id", Encryptor::decrypt($request->aeronave_id))
                        ->where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id))
                        ->whereBetween(DB::raw('CAST(fecha_llegada AS date)'), [$fecha_desde, $fecha_hasta])
                        ->whereNull("factura_tasa")
                        ->get();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $datos->toArray();

                return $result;
            } else {
                if (Auth::user()->profile_id == 13 || Auth::user()->profile_id == 1) {// SI ES RECAUDACION DE BAER * MASTER
                    $Aeropuertos = \App\Models\Aeropuertos::
                            where('maest_aeropuertos.activo', true)
                            //->where('maest_aeropuertos.id', '!=', $this->TORTUGA_ID)
                            ->where('maest_aeropuertos.pertenece_baer', '=', true)
                            ->get()
                            ->pluck('full_nombre', 'crypt_id');
                } else {
                    $Aeropuertos = \App\Models\Aeropuerto::
                            where('maest_aeropuertos.activo', true)
                            ->where('maest_aeropuertos.id', '=', Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck('full_nombre', 'crypt_id');
                }
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck('full_nombre', 'crypt_id');
                return view('pospago_aviacion_general.form', compact('Aeropuertos', 'Aeronaves'));
            }
        }
    }

}
