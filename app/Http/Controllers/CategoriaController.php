<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Categorias;

class CategoriaController extends Controller {

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Clasificacions_active = Categorias::where("activo", 1)->get();

            $Clasificacions_disable = Categorias::where("activo", 0)->get();

            $Clasificacions_active = Datatables::of($Clasificacions_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('categorias.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('categorias.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                        }




                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            //dd("o,O");
            $Clasificacions_disable = Datatables::of($Clasificacions_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('categorias.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            //dd("HSFGSDFFGSDFG");
            $data1 = $Clasificacions_active->original['data'];
            $data2 = $Clasificacions_disable->original['data'];

            //dd("dddd");
            return view('categorias.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                           
                            'nombre' => 'required',
                            'porcentaje' => 'required'
                                ], [
                            
                            'nombre.required' => __('El Nombre es Requerido'),
                            'porcentaje.required' => __('El Porcentaje es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Clasificacion = new Categorias();
                    
                    $Clasificacion->nombre = $request->nombre;
                    $Clasificacion->porcentaje = $this->saveFloat($request->porcentaje)/100;
                    

                    $Clasificacion->user_id = Auth::user()->id;
                    $Clasificacion->ip = $this->getIp();
                    $Clasificacion->fecha_registro = now();

                    $Clasificacion->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                
                return view('categorias.create');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                          
                            'nombre' => 'required',
                            'porcentaje' => 'required'
                                ], [
                           
                            'nombre.required' => __('El Nombre es Requerido'),
                            'porcentaje.required' => __('La Clasificaci��n es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Clasificacion = Categorias::find(Encryptor::decrypt($id));
                    
                    $Clasificacion->nombre = $request->nombre;
                    $Clasificacion->porcentaje = $this->saveFloat($request->porcentaje)/100;
                    $Clasificacion->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $Clasificacion = Categorias::find(Encryptor::decrypt($id));
                return view('categorias.edit', compact('Clasificacion'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Clasificacion = Categorias::find(Encryptor::decrypt($id));
                $Clasificacion->activo = 0;

                $Clasificacion->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $clasificacion = Categorias::find(Encryptor::decrypt($id));
                return view('categorias.disable', compact('clasificacion'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Clasificacion = Categorias::find(Encryptor::decrypt($id));
                $Clasificacion->activo = 1;

                $Clasificacion->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $clasificacion = Categorias::find(Encryptor::decrypt($id));
                return view('categorias.active', compact('clasificacion'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
