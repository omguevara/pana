<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Profile;

class ProfilesController extends Controller {

    function index() {
        if (\Request::ajax()) {
            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Profiles_active = Profile::where("status", 1)->get();

            $Profiles_disable = Profile::where("status", 0)->get();
            $Profiles_active = Datatables::of($Profiles_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" title="Editar Perfil" data-toggle="tooltip" href="' . route('profiles.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li></a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" title="Deshabilitar Perfil" data-toggle="tooltip" href="' . route('profiles.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li></a> ';
                        }

                        if (in_array("permissions", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" title="Administrar Permisos al Perfil" data-toggle="tooltip" href="' . route('profiles.permissions', $row->crypt_id) . '" class=" btn btn-warning btn-sm"><li class="fa fa-user-lock"></li> </a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $Profiles_disable = Datatables::of($Profiles_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('profiles.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Profiles_active->original['data'];
            $data2 = $Profiles_disable->original['data'];

            return view('profiles.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'code' => 'required|unique:seg_profiles,code',
                            'name_profile' => 'required'
                                ], [
                            'code.required' => __('Code is Required'),
                            'code.unique' => __('Code is Registered'),
                            'name_profile.required' => __('Name Profile is Required')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Profile = new Profile();
                    $Profile->code = $request->code;
                    $Profile->name_profile = Upper($request->name_profile);
                    $Profile->description = $request->description;
                    $Profile->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                return view('profiles.create');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'code' => 'required|unique:seg_profiles,code,' . Encryptor::decrypt($id),
                            'name_profile' => 'required'
                                ], [
                            'code.required' => __('Code is Required'),
                            'code.unique' => __('Code is Registered'),
                            'name_profile.required' => __('Name Profile is Required')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Profile = Profile::find(Encryptor::decrypt($id));
                    $Profile->code = $request->code;
                    $Profile->name_profile = Upper($request->name_profile);
                    $Profile->description = $request->description;
                    $Profile->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $profile = Profile::find(Encryptor::decrypt($id));
                return view('profiles.edit', compact('profile'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Profile = Profile::find(Encryptor::decrypt($id));
                $Profile->status = 0;

                $Profile->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $profile = Profile::find(Encryptor::decrypt($id));
                return view('profiles.disable', compact('profile'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Profile = Profile::find(Encryptor::decrypt($id));
                $Profile->status = 1;

                $Profile->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $profile = Profile::find(Encryptor::decrypt($id));
                return view('profiles.active', compact('profile'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function permissions($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());

                \App\Models\ProfileProcess::where("profile_id", Encryptor::decrypt($id))->delete();

                foreach ($request->process as $key => $value) {
                    
                    $ProfileProcess = new \App\Models\ProfileProcess();
                    $ProfileProcess->profile_id = Encryptor::decrypt($id);
                    $ProfileProcess->process_id = $key;
                    if (isset($value['actions'])){
                        $ProfileProcess->actions = implode("|", $value['actions']);
                    }else{
                        $ProfileProcess->actions = "";
                    }
                    //$ProfileProcess->actions = implode("|", $value['actions']);
                    $ProfileProcess->date_register = now();
                    $ProfileProcess->save();
                }
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
                /*
                  $Profile = Profile::find(Encryptor::decrypt($id));
                  $Profile->status = 1;

                  $Profile->save();

                  $result['status'] = 1;
                  $result['type'] = 'success';
                  $result['message'] = __('Processed Correctly');

                  return $result;

                 */
            } else {
                $Menu = \App\Models\Menu::orderBy("order")->get();

                $profile = Profile::find(Encryptor::decrypt($id));

                //dd($profile->getOptions);
                return view('profiles.permissions', compact('Menu', 'profile'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
