<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Models\RegistroOnline;

class RegistroOnlineController extends Controller {

    public function checkRegOnline() {

        if (\Request::ajax()) {

            $where[] = [function ($cond) {
                    $cond->orWhere('origen_id', Auth::user()->aeropuerto->id);
                    $cond->orWhere('destino_id', Auth::user()->aeropuerto->id);
                }];

            $where[] = [function ($cond) {

                    $cond->orWhere(DB::raw("
                        (
                            CASE
                                WHEN origen_id = " . Auth::user()->aeropuerto_id . "  THEN  (visto_origen  IS FALSE)
                                WHEN destino_id = " . Auth::user()->aeropuerto_id . " THEN  (visto_destino IS FALSE)    
                                ELSE NULL
                            END
                            )
                         "), 'TRUE');
                }];

            //$RegistroOnline = RegistroOnline::where($where)->where("fecha", ">=", date("Y-m-d"))->count();
            $RegistroOnline = RegistroOnline::where($where)->count();



            /*
              (
              CASE
              WHEN origen_id = Auth::user()->aeropuerto->id THEN  procesado_origen IS FALSE
              ELSE NULL
              END
              )
             */
            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            //$result['data'] = rand(0, 10);
            $result['data'] = $RegistroOnline;
            return $result;
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function reserva_general(Request $request) {

        $msg = "";
        if ($request->isMethod('post')) {


            $Validator = \Validator::make(
                            $request->all(), [
                        'type_document2' => 'required',
                        'document2' => 'required',
                        'piloto' => 'required',
                        'nacionalidad_id' => 'required',
                        'fecha' => 'required',
                        'hora_inicio' => 'required',
                        'cant_pasajeros' => 'required',
                        'placa' => 'required',
                        'captcha' => 'required|captcha'
                            ], [
                        'type_document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'piloto.required' => __('El Nombre del Piloto es Requerido'),
                        'nacionalidad_id.required' => __('Debe Indicar El Tipo de Vuelo (Nacional o Internacional)'),
                        'fecha.required' => __('Debe Indicar la Fecha'),
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros '),
                        'placa.required' => __('Debe Indicar la Matricula de la Aeronave'),
                        'captcha.required' => __('El Captcha es Requerido'),
                        'captcha.captcha' => __('El Captcha  no Coincide'),
                            ]
            );
            // dd($request->captcha);
            if ($Validator->fails()) {
                $result['status'] = 0;
                $result['type'] = 'error';
                $result['message'] = $Validator->errors()->first();
            } else {


                if ($request->tipo_operacion_id == 1) {//SI SE REGISTRA UN ARRIBO
                    $Esta = \App\Models\Vuelos::where("activo", 1)
                            ->where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id))
                            //->where('tipo_operacion_id', $request->tipo_operacion_id)
                            ->where('aeronave_id', Encryptor::decrypt($request->aeronave_id))
                            ->orderBy("id", "desc")
                            ->first();
                    if ($Esta != null) {
                        if ($Esta->tipo_operacion_id == 1) {// SI EL ULTIMO REGISTRO FUE UN ARRIBO
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = "La Aeronave Ya Posee Un Registro de un Arribo";
                            return $result;
                        }
                    }
                }

                if ($request->piloto_id == null) {
                    $Piloto = new \App\Models\Pilotos();
                    $Piloto->tipo_documento = $request->type_document2;
                    $Piloto->documento = $request->document2;
                    $Piloto->nombres = Upper($request->piloto);
                    //$Piloto->apellidos = Upper($request->surname_user);
                    //$Piloto->telefono = $request->phone;
                    //$Piloto->correo = $request->email;


                    $Piloto->save();
                } else {
                    $Piloto = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                }

                $last_operation = \App\Models\Vuelos::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                        ->where("activo", true)
                        ->orderBy("id", "desc")
                        ->first();
                $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                $Vuelos = new \App\Models\Vuelos();

                $Vuelos->numero_operacion = $num_operation;
                $Vuelos->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                //$Vuelos->tipo_operacion_id = $request->tipo_operacion_id;

                if ($request->tipo_operacion_id == 1) {//SI ES LLEGADA
                    $Vuelos->pasajeros_desembarcados = $request->cant_pasajeros;

                    $Vuelos->pasajeros_embarcados = 0;

                    $Vuelos->hora_llegada = \Carbon\Carbon::createFromFormat('Y-m-d H:i', $request->fecha . ' ' . $request->hora_inicio)->format('Y-m-d H:i');

                    $Vuelos->hora_salida = null;
                } else { //SI ES SALIDA
                    $Vuelos->pasajeros_desembarcados = 0;

                    $Vuelos->pasajeros_embarcados = $request->cant_pasajeros;
                    ;

                    $Vuelos->hora_llegada = null;

                    $Vuelos->hora_salida = \Carbon\Carbon::createFromFormat('Y-m-d H:i', $request->fecha . ' ' . $request->hora_inicio)->format('Y-m-d H:i');
                    ;
                }









                $Vuelos->tipo_vuelo_id = $request->nacionalidad_id;
                //$Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                $Vuelos->fecha_operacion = $request->fecha;
                //$Vuelos->hora_operacion = $request->hora_inicio;
                $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                $Vuelos->piloto_id = $Piloto->id;
                //$Vuelos->origen_destino_id = null;
                //$Vuelos->cant_pasajeros = $request->cant_pasajeros;
                //$Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                $Vuelos->prodservicios_extra = "[]";


                $Vuelos->user_id = 1;
                $Vuelos->fecha_registro = now();
                $Vuelos->ip = $this->getIp();
                ;

                $Vuelos->save();


                $servicios_facturar = $Vuelos->getDetalleByRecord();

                foreach ($servicios_facturar['data'] as $value) {
                    //dd($value);
                    $Detalle = new \App\Models\VuelosDetalle();

                    $Detalle->aplica_categoria = $value['aplica_categoria'];
                    $Detalle->aplica_estacion = $value['aplica_estacion'];
                    $Detalle->aplica_peso = $value['aplica_peso'];
                    $Detalle->aplicable = $value['aplicable'];
                    $Detalle->bs = $value['bs'];
                    $Detalle->cant = $value['cant'];
                    $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                    $Detalle->codigo = $value['codigo'];
                    $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                    $Detalle->default_carga = $value['default_carga'];
                    $Detalle->default_dosa = $value['default_dosa'];
                    $Detalle->default_tasa = $value['default_tasa'];
                    $Detalle->descripcion = $value['descripcion'];
                    $Detalle->formula = $value['formula'];
                    $Detalle->formula2 = $value['formula2'];
                    $Detalle->full_descripcion = $value['full_descripcion'];
                    $Detalle->iva = $value['iva'];
                    $Detalle->iva2 = $value['iva2'];
                    $Detalle->iva_aplicado = $value['iva_aplicado'];
                    $Detalle->moneda = $value['moneda'];
                    $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                    $Detalle->nomenclatura = $value['nomenclatura'];
                    $Detalle->peso_inicio = $value['peso_inicio'];
                    $Detalle->peso_fin = $value['peso_fin'];
                    $Detalle->precio = $value['precio'];
                    $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                    $Detalle->tasa = $value['tasa'];
                    $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                    $Detalle->turno_id = $value['turno_id'];
                    $Detalle->valor_dollar = $value['valor_dollar'];
                    $Detalle->valor_euro = $value['valor_euro'];
                    $Detalle->valor_petro = $value['valor_petro'];
                    $Detalle->orden = $value['orden'];
                    $Detalle->vuelo_id = $Vuelos->id;

                    $Detalle->save();
                }




                if ($request->tipo_operacion_id == 2) {//SI ES UN DESPEGUE LIBERO EL PUESTO
                    $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelos->aeronave_id)->update(["aeronave_id" => null]);
                }


                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Registrado Correctamente";

                /*
                  $Aeronave = \App\Models\Aeronaves::where("matricula", Upper($request->placa))->where("activo", 1)->first();

                  $DOLAR = $this->getDolar();
                  $EURO = $this->getEuro();
                  $PETRO = $this->getPetro();

                  $prodservExtra = array();
                  $prodservExtra2 = array();
                  $extra = array();
                  if (is_array($request->prodservicios)) {
                  foreach ($request->prodservicios as $value) {
                  if (!in_array(Encryptor::decrypt($value), $prodservExtra)) {
                  $prodservExtra[] = "1:" . Encryptor::decrypt($value);
                  $prodservExtra2[] = Encryptor::decrypt($value);
                  }
                  }
                  }


                  if ($request->piloto_id == null) {
                  $Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                  if ($Capitanes == null) {
                  $Capitanes = new \App\Models\Pilotos();
                  }

                  $Capitanes->tipo_documento = $request->type_document2;
                  $Capitanes->documento = $request->document2;
                  } else {
                  $Capitanes = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                  }
                  //dd($request->piloto_id);
                  $Capitanes->nombres = Upper($request->piloto);
                  $Capitanes->apellidos = "";
                  $Capitanes->telefono = "";

                  $Capitanes->correo = "";

                  $Capitanes->save();

                  $RegistroOnline = new RegistroOnline();
                  $RegistroOnline->fecha_registro = now();

                  $RegistroOnline->tasa_petro = $PETRO;
                  $RegistroOnline->tasa_euro = $EURO;

                  $RegistroOnline->piloto_id = $Capitanes->id;
                  $RegistroOnline->tipo_id = $request->nacionalidad_id;
                  $RegistroOnline->fecha = $this->saveDate($request->fecha);
                  $RegistroOnline->hora = $request->hora_inicio;

                  $RegistroOnline->aeronave_id = $Aeronave->id;
                  $RegistroOnline->pasajeros = $request->cant_pasajeros;
                  $RegistroOnline->origen_id = Encryptor::decrypt($request->origen_id);
                  $RegistroOnline->destino_id = Encryptor::decrypt($request->destino_id);
                  $RegistroOnline->observacion = $request->observacion;

                  $RegistroOnline->prodservicios_extra = (is_array($prodservExtra) ? implode(",", $prodservExtra) : "");

                  $RegistroOnline->save();

                  if ($request->img != null) {
                  // dd($request->img);
                  try {


                  $fileName = "pago_img_" . $RegistroOnline->id . '.' . $request->img->extension();
                  $RegistroOnline->img = $fileName;
                  $RegistroOnline->save();
                  //$request->img->move(public_path('pagos'), $fileName);
                  $request->img->move(storage_path('pagos'), $fileName);
                  //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                  //$request->img->move(public_path('pagos'), $fileName);
                  } catch (Exception $ex) {

                  }
                  //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                  }







                  $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));

                  $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));

                  $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];

                  $data_tasa = $request->all();
                  $data_tasa['tipo_vuelo_id'] = 2;
                  $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                  $tasa = $this->getProdservicios("tasa", $data_tasa);

                  $data_dosa = $request->all();
                  $data_dosa['tipo_vuelo_id'] = 2;
                  $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));

                  $dosa = $this->getProdservicios("dosa", $data_dosa);

                  if (count($prodservExtra) > 0) {
                  //dd($prodservExtra);

                  $data_extra = $request->all();
                  $data_extra['tipo_vuelo_id'] = 2;
                  $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                  $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra2);
                  }




                  return PDF::loadView("RegistroOnline.proforma_pdf", compact('RegistroOnline', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
                 */
                /*
                  // dd($request->pagada);
                  $subject = "Solicitud de Permiso de Vuelo Registrado ";

                  if ($request->correo != null) {
                  $for = $request->correo;
                  $datos = [];
                  $pdf = PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'));
                  try {
                  Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                  //$msj->from("mrivero52.correo@gmail.com", "Registro");
                  $msj->subject($subject);
                  $msj->to($for);
                  $msj->attachData($pdf->output(), "proforma.pdf");
                  });
                  } catch (Exception $ex) {

                  }
                  }
                 */
                //dd($request->img);
            }

            return $result;
        }
        $O = \App\Models\Aeropuertos::where("activo", 1)->get();
        foreach ($O as $value) {
            $ORIGEN[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
        }

        $D = \App\Models\Aeropuertos::where("activo", 1)->get();
        foreach ($O as $value) {
            $DESTINO [$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
        }


        $EURO = $this->getEuro();
        $PETRO = $this->getPetro();

        return view('RegistroOnline.reserva_general', compact('ORIGEN', 'DESTINO', 'EURO', 'PETRO', 'msg'));
    }

}
