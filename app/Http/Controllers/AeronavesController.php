<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use DataTables;
use PDF;
use App\Models\Aeronaves;
use App\Models\Pilotos;

class AeronavesController extends Controller {
    protected $get_route_aeronave_id = 7;
    
    function get_aeronaves() {
        if (\Request::ajax()) {
            $request =\Request::all();
            $where = [];
            $where[] = [DB::raw("1"), "1"];
            
            if ($request['search']!=null){
                $matricula = $request["search"];
                $where[] = [function ($q) use($matricula){ 
                    
                    $q->where('matricula','ILIKE', '%'.$matricula.'%');
                    
                }];
            }
            
            $datos = Aeronaves::where($where)
                        ->limit(20)
                        ->orderBy("id", "desc")
                        ->get();
            
            $data = Datatables::of($datos)
                    ->addIndexColumn()
                    
                    ->addColumn('selection', function ($row) {
                            $actionBtn = '<buttom onClick="selectNave(\'' . $row->crypt_id . '\', \'' . $row->full_nombre_tn . '\')" class="btn  bg-success"><i class="fas fa-check"></i></buttom>';
                            return $actionBtn;
                        })
                    ->rawColumns(['selection'])
            //->make(true)

            ;
            // dd($data);

            /*      //  dd("sss");
              $data = $data->toJson();
              foreach($data as $key=>$value){
              dd($value->aeropuerto);

              }
             */
            return $data->toJson();
            
            
        }else{
            exit;
        }
    }
    function get_pilotos() {
        if (\Request::ajax()) {
            $request =\Request::all();
            $where = [];
            $where[] = [DB::raw("1"), "1"];
            
            if ($request['search']!=null){
                $full_name = Upper($request["search"]);
                $where[] = [function ($q) use($full_name){ 
                    $q->orWhere('tipo_documento','ILIKE', '%'.$full_name.'%');
                    $q->orWhere('documento','ILIKE', '%'.$full_name.'%');
                    $q->orWhere('nombres','ILIKE', '%'.$full_name.'%');
                    $q->orWhere('apellidos','ILIKE', '%'.$full_name.'%');
                }];
            }
            
            $datos = Pilotos::where($where)
                        ->limit(20)
                        ->orderBy("id", "desc")
                        ->get();
            
            $data = Datatables::of($datos)
                ->addIndexColumn()
                ->addColumn('selection', function ($row) {
                    $actionBtn = '<buttom onClick="selectPilot(\'' . $row->crypt_id . '\', \'' . $row->full_name . '\')" class="btn  bg-success"><i class="fas fa-check"></i></buttom>';
                    return $actionBtn;
                })
                ->rawColumns(['selection'])
                //->make(true)

            ;
            // dd($data);

            /*      //  dd("sss");
              $data = $data->toJson();
              foreach($data as $key=>$value){
              dd($value->aeropuerto);

              }
             */
            return $data->toJson();
            
            
        }else{
            exit;
        }
    }
    
    function get_placa($placa) {


        $Datos = Aeronaves::where("matricula", Upper($placa))->where("activo", 1)->first();
        if ($Datos == null) {
            $resp['status'] = 0;
            $resp['data'] = null;
        } else {
            $resp['status'] = 1;
            $resp['data'] = $Datos->toArray();
        }
        return response()->json($resp);
    }
    
    function get_aeronave($id) {
        $Datos = Aeronaves::find(Encryptor::decrypt($id));
        $data = $Datos->toArray();
        /*
        $Vuelos = \App\Models\Vuelos::where("activo", 1)
                ->where("aeronave_id", Encryptor::decrypt($id) )
                ->where("aeropuerto_id", Encryptor::decrypt($aeropuerto_id) )
                ->take(10)
                ->orderBy("id", "desc")
                ->get();
        
        
        $data['Vuelos'] = $Vuelos->count() > 0 ? $Vuelos->toArray():[];
        */
        $resp['status'] = 1;
        $resp['data'] = $data;
        $resp['message'] = 'Aeronave Encontrada';
        $resp['type'] = 'success';
        return response()->json($resp);
    }
    
    
    function index() {

        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $name_route = \Request::route()->getName();
                $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
                //dd(Auth::user()->getActions()[$route_id]);
                //dd(route('users.edit', 1));
                //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
                
                
                /* $Aeronavess_active = Aeronaves::where("activo", 1)->get();

                $Aeronavess_disable = Aeronaves::where("activo", 0)->get(); */

                /* $Aeronavess_active = Datatables::of($Aeronavess_active)
                        ->addIndexColumn()
                        ->addColumn('action', function ($row) use ($route_id) {
                            $actionBtn = "";

                            if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeronaves.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                            }

                            if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeronaves.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                            }
                            return $actionBtn;
                        })
                        ->addColumn('homologado', function ($row)  {
                            $actionBtn = "";
                        

                            if ($row->homologacion != null) {
                                $actionBtn .= '<a  href="' . route('get_homologacion', $row->crypt_id) . '" class=" btn btn-info btn-sm"><li class="fa fa-arrow-alt-circle-down"></li> ' . __('Homologación') . '</a> ';
                            }


                            return $actionBtn;
                        })
                        ->rawColumns(['action', 'homologado'])
                        ->make(true);

                 $Aeronavess_disable = Datatables::of($Aeronavess_disable)
                        ->addIndexColumn()
                        ->addColumn('action', function ($row) use ($route_id) {
                            $actionBtn = "";

                            if (in_array("active", Auth::user()->getActions()[$route_id])) {
                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeronaves.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                            }



                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);*/

                // $data1 = $Aeronavess_active->original['data'];
                // $data2 = $Aeronavess_disable->original['data'];

                // dd($data2);

                return view('aeronaves.index', compact(/* 'data1', 'data2',*/ 'route_id'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            'matricula' => 'required|unique:maest_aeronaves,matricula',
                            'peso_maximo' => 'required',
                            'maximo_pasajeros' => 'required',
                            'estacion_id' => 'required'
                                ], [
                            'nombre.required' => __('El Nombre es Requerido'),
                            'matricula.unique' => __('Matricula ya Registrada'),
                            'matricula.required' => __('La Matricula es Requerida'),
                            'peso_maximo.required' => __('El Peso Máximo es Requerido'),
                            'maximo_pasajeros.required' => __('La Cantidad Máxima de Pasajeros es Requerido'),
                            'estacion_id.required' => __('El Estacionamiento Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = new Aeronaves();
                    $Aeronaves->nombre = Upper($request->nombre);
                    $Aeronaves->matricula = Upper($request->matricula);
                    $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                    $Aeronaves->peso_maximo_certificado = saveFloat($request->peso_maximo_certificado) * 1000;
                    $Aeronaves->observaciones = $request->observaciones;
                    $Aeronaves->maximo_pasajeros = $request->maximo_pasajeros;
                    $Aeronaves->estacion_id = Encryptor::decrypt($request->estacion_id);


                    $Aeronaves->user_id = Auth::user()->id;
                    $Aeronaves->ip = $this->getIp();
                    $Aeronaves->fecha_registro = now();

                    $Aeronaves->save();




                    if ($request->file('homologacion') != null) {
                        $file = $request->file('homologacion');
                        // dd($request->img);
                        try {


                            $fileName = "homologacion_" . $Aeronaves->id . '.' . $file->getClientOriginalExtension();
                            $Aeronaves->homologacion = $file->getClientOriginalExtension();
                            $Aeronaves->save();
                            $file->move(storage_path('homologacion'), $fileName);
                            //$request->img->move(public_path('pagos'), $fileName);
                            //$request->img->move(storage_path('homologacion'), $fileName);
                            //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                            //$request->img->move(public_path('pagos'), $fileName);
                        } catch (Exception $ex) {
                            
                        }
                        //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                    }



                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $Aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->get()->pluck('full_nombre', 'crypt_id');
                return view('aeronaves.create', compact('Aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function add(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            'matricula' => 'required|unique:maest_aeronaves,matricula',
                            'peso_maximo' => 'required',
                            //'maximo_pasajeros2' => 'required',
                                ], [
                            'nombre.required' => __('El Nombre es Requerido'),
                            'matricula.required' => __('La Matricula es Requerida'),
                            'matricula.unique' => __('La Matricula ya Esta Registrada'),        
                            'peso_maximo.required' => __('El Peso Máximo es Requerido'),
                          //  'maximo_pasajeros2.required' => __('La Cantidad Máxima de Pasajeros es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = new Aeronaves();
                    $Aeronaves->nombre = $request->nombre;
                    $Aeronaves->matricula = Upper($request->matricula);
                    $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                    $Aeronaves->maximo_pasajeros = 0;
                    $Aeronaves->estacion_id  = 19; //OTRO AEROPUERTO



                    $Aeronaves->user_id = 1;
                    $Aeronaves->ip = $this->getIp();
                    $Aeronaves->fecha_registro = now();

                    $Aeronaves->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $Aeronaves->toArray();
                }
                return $result;
            } else {
                return 1;
            }
        } else {
            return Redirect::to('/');
        }
    }
    
    public function plus(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $data =$request->all();
                $data['matricula']= Upper( $data['matricula']);
                $Validator = \Validator::make(
                                $data, [
                            'nombre' => 'required',
                            'matricula' => 'required|unique:maest_aeronaves,matricula',
                            'peso_maximo' => 'required',
                            
                                ], [
                            'nombre.required' => __('El Nombre es Requerido'),
                            'matricula.required' => __('La Matrícula es Requerida'),
                            'matricula.unique' => __('La Matrícula ya Esta Registrada'),        
                            'peso_maximo.required' => __('El Peso Máximo es Requerido'),
                            
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = new Aeronaves();
                    $Aeronaves->nombre = $request->nombre;
                    $Aeronaves->matricula = Upper($request->matricula);
                    $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                    $Aeronaves->estacion_id = 19; //19 ES OTRO AEROPUERTO
                    



                    $Aeronaves->user_id = Auth::user()->id;
                    $Aeronaves->ip = $this->getIp();
                    $Aeronaves->fecha_registro = now();

                    $Aeronaves->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $Aeronaves->toArray();
                }
                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {



                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            'matricula' => 'required|unique:maest_aeronaves,matricula,'. Encryptor::decrypt($id),
                            'peso_maximo' => 'required',
                            'maximo_pasajeros' => 'required',
                            'estacion_id' => 'required'
                                ], [
                            'nombre.required' => __('El Nombre es Requerido'),
                            'matricula.unique' => __('Matricula ya registrada '),
                            'peso_maximo.required' => __('El Peso Máximo es Requerido'),
                            'maximo_pasajeros.required' => __('La Cantidad Máxima de Pasajeros es Requerido'),
                            'estacion_id.required' => __('El Estacionamiento Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
                    $Aeronaves->nombre = $request->nombre;
                    $Aeronaves->matricula = $request->matricula;
                    $Aeronaves->peso_maximo = saveFloat($request->peso_maximo) * 1000;
                    $Aeronaves->peso_maximo_certificado = saveFloat($request->peso_maximo_certificado) * 1000;
                    $Aeronaves->observaciones = $request->observaciones;
                    $Aeronaves->maximo_pasajeros = $request->maximo_pasajeros;
                    $Aeronaves->estacion_id = $request->estacion_id;


                    $Aeronaves->save();


                    if ($request->file('homologacion') != null) {
                        $file = $request->file('homologacion');
                        // dd($request->img);
                        try {


                            $fileName = "homologacion_" . $Aeronaves->id . '.' . $file->getClientOriginalExtension();
                            $Aeronaves->homologacion = $file->getClientOriginalExtension();
                            $Aeronaves->save();
                            $file->move(storage_path('homologacion'), $fileName);
                            //$request->img->move(public_path('pagos'), $fileName);
                            //$request->img->move(storage_path('homologacion'), $fileName);
                            //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                            //$request->img->move(public_path('pagos'), $fileName);
                        } catch (Exception $ex) {
                            
                        }
                        //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                    }


                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
                $Aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->get()->pluck('full_nombre', 'id');
                return view('aeronaves.edit', compact('Aeronaves', 'Aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function get_homologacion($id) {
        $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
        return \Response::download(storage_path('homologacion') . DIRECTORY_SEPARATOR . 'homologacion_' . $Aeronaves->id . '.' . $Aeronaves->homologacion);
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
                $Aeronaves->activo = 0;

                $Aeronaves->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
                return view('aeronaves.disable', compact('Aeronaves'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
                $Aeronaves->activo = 1;

                $Aeronaves->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Aeronaves = Aeronaves::find(Encryptor::decrypt($id));
                return view('aeronaves.active', compact('Aeronaves'));
            }
        } else {
            return Redirect::to('/');
        }
    }
    public function get_aeronaves_active(){
        $name_route = \Request::route()->getName();
        // $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
        $route_id = $this->get_route_aeronave_id;
        $request = \Request::all();
        $where = [];
        $where[] = [DB::raw("1"), "1"];
        
        if ($request['search']['value']!=null){
            $matricula = Upper($request["search"]['value']);
            $where[] = [function ($q) use($matricula){ 
                $q->Where('matricula','ILIKE', '%'.$matricula.'%');
            }];
        }
        
        $query = Aeronaves::where($where)->where("activo", 1)->take(40)->get();
        $data = Datatables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($route_id) {
                $actionBtn = "";

                if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                    $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeronaves.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                }

                if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                    $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeronaves.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                }
                return $actionBtn;
            })
            ->addColumn('homologado', function ($row)  {
                $actionBtn = "";
            
                if ($row->homologacion != null) {
                    $actionBtn .= '<a  href="' . route('get_homologacion', $row->crypt_id) . '" class=" btn btn-info btn-sm"><li class="fa fa-arrow-alt-circle-down"></li> ' . __('Homologación') . '</a> ';
                }


                return $actionBtn;
            })
            ->rawColumns(['action', 'homologado'])
            // ->make(true)
        ;
        return $data->toJson();
    }
    public function get_aeronaves_inactive(){
        $name_route = \Request::route()->getName();
        // $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
        $route_id = $this->get_route_aeronave_id;
        
        $request = \Request::all();
        $where = [];
        $where[] = [DB::raw("1"), "1"];
        
        if ($request['search']['value']!=null){
            $matricula = Upper($request["search"]['value']);
            $where[] = [function ($q) use($matricula){ 
                $q->Where('matricula','ILIKE', '%'.$matricula.'%');
            }];
        }
        $query = Aeronaves::where($where)->where("activo", 0)->take(40)->get();

        $data = Datatables::of($query)
            ->addIndexColumn()
            ->addColumn('action', function ($row) use ($route_id) {
                $actionBtn = "";
                
                if (in_array("active", Auth::user()->getActions()[$route_id])) {
                    $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aeronaves.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                }
                    
                return $actionBtn;
            })
            ->rawColumns(['action'])
            // ->make(true)
        ;
        return $data->toJson();
    }

}
