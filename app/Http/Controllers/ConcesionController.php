<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Actividades;
use App\Models\Locales;
use App\Models\LocalesUsos;
use App\Models\Concesiones;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Helpers\Encryptor;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ConcesionesExport;
class ConcesionController extends Controller {

    public function showFiles($id) {
        if (\Request::ajax()) {

            $local = Concesiones::find(Encryptor::decrypt($id));

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Archivos Cargados Correctamente');
            $result['data'] = $local;

            return $result;
        } else {
            exit;
        }
    }

    public function eraseFile($id, $nombre) {
        if (\Request::ajax()) {
            $ds = DIRECTORY_SEPARATOR;
            $concesion = Encryptor::decrypt($id);
            $path = storage_path('Comercializacion' . $ds . 'Concesiones' . $ds . $concesion . $ds . $nombre);
            /*
              $destination = storage_path('app' . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR);
              $filename = $arraysFiles[$file]['route'];
             */
            $pathToFile = $path;
            //return response()->file($pathToFile);

            unlink($pathToFile);
            
            
            $Concesion = Concesiones::find($concesion );
            
            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Archivos Borrado Correctamente');
            $result['data'] = $Concesion;

            return $result;
        } else {
            exit;
        }

        //return view('usuarios.dashboard');
    }

    public function seeFile($id, $nombre) {

        $ds = DIRECTORY_SEPARATOR;
        $concesion = Encryptor::decrypt($id);
        $path = storage_path('Comercializacion' . $ds . 'Concesiones' . $ds . $concesion . $ds . $nombre);
        /*
          $destination = storage_path('app' . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR);
          $filename = $arraysFiles[$file]['route'];
         */
        $pathToFile = $path;
        //return response()->file($pathToFile);

        return response()->download($pathToFile, 'file.pdf');
        //return view('usuarios.dashboard');
    }

    public function uploadFile(Request $request) {
        if (\Request::ajax()) {

            $ds = DIRECTORY_SEPARATOR;
            $concesion = Encryptor::decrypt($request->conc);
            $path = storage_path('Comercializacion' . $ds . 'Concesiones' . $ds . $concesion);

            if (!\Illuminate\Support\Facades\Storage::exists($path)) {
                \Illuminate\Support\Facades\Storage::makeDirectory($path); //creates directory
            }
            $file = $request->file('archivo_pdf');
            $fileName = Upper($request->nombre . '.' . $file->getClientOriginalExtension());
            
            if (file_exists($path . $ds . $fileName)) {
                $result['status'] = 0;
                $result['type'] = 'error';
                $result['message'] = __('Ya Existe un Documento con ese Nombre');
            } else {
                try {
                    $file->move($path, $fileName);
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Archivo Guardado Correctamente');
                } catch (Exception $ex) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Error al Guardar el ARchivo');
                }
            }

            return $result;
        } else {
            exit;
        }
    }

    public function index() {
        if (\Request::ajax()) {
            $aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                    ->where("pertenece_baer", true)
                    ->where("activo", true)
                    ->whereIn("id", Auth::user()->lista_aeropuertos)
                    ->get()
                    ->pluck("full_nombre", "crypt_id");

            $usos = LocalesUsos::orderBy('orden')->orderBy('uso')->pluck("uso", 'id');
            $actividades = Actividades::orderBy('orden')->orderBy('actividad')->pluck("actividad", 'id');

            return view('Concesiones.index', ['actividades' => $actividades, 'usos' => $usos, 'aeropuertos' => $aeropuertos]);
        } else {
            exit;
        }
    }

    function mostrarHistorial() {

        $id = \Request::all(['local_id']);

        $id = $id == null ? 0 : $id;

        $historialConcesiones = Concesiones::where('local_id', $id)->with('empresas', 'locales.usos_local', 'actividades')->get();

        $dataConcesion = Datatables::of($historialConcesiones);
        return $dataConcesion->toJson();
    }

    public function storeConcesion(Request $request) {
        if ($request->ajax()) {
            $Validator = Validator::make(
                            $request->all(), [
                        'ingresos_brutos' => 'required',
                        'nombre' => 'required',
                        'canon_fijo' => 'required',
                        'duracion' => 'required',
                        'convenio_euro_integrado' => 'required',
                            ], [
                        'ingresos_brutos.required' => 'Campo Requerido',
                        'nombre.required' => 'Campo Requerido',
                        'canon_fijo.required' => 'Campo Requerido',
                        'duracion.required' => 'Campo Requerido',
                        'convenio_euro_integrado.required' => 'Campo Requerido',
                            ]
            );
            if ($Validator->fails()) {
                $message['icon'] = 'error';
                $message['title'] = '¡Error!';
                $message['message'] = $Validator->errors()->first();
            } else {
                if ($request->actividad == $this->OTRA_ACTIVIDAD) {
                    $request->validate([
                        'otro_actividad' => 'required',
                    ]);
                    $otro_actividad = $request->otro_actividad;
                } else {
                    $otro_actividad = null;
                }
                if ($request->empresa_id == null) {
                    $request->validate([
                        'razon_social' => 'required',
                        'direccion' => 'required',
                    ]);
                    $cliente = \App\Models\Clientes::where('documento', $request->rif)->where('tipo_documento', "J")->first();
                    if ($cliente == null) {
                        $cliente = new \App\Models\Clientes();
                        //$cliente->tipo_documento = $request->tipo_documento;
                        $cliente->tipo_documento = $request->tipo_rif;
                        $cliente->documento = Upper($request->rif);
                        $cliente->razon_social = $request->razon_social;
                        $cliente->telefono = "";
                        $cliente->direccion = $request->direccion;
                        $cliente->tipo_id = 4;

                        $cliente->user_id = Auth::user()->id;
                        $cliente->fecha_registro = Carbon::now();
                        $cliente->ip = $request->ip();

                        $cliente->save();
                    }
                    $empresa_id = $cliente->id;
                } else {
                    $empresa_id = Encryptor::decrypt($request->empresa_id);
                }
                $duracion = $request->duracion;
                $duracion = explode(' - ', $duracion);
                
                $duracion[1] = saveDate($duracion[1]);
                $duracion[0] = saveDate($duracion[0]);

                $condicion[] = ['fecha_inicio', '<=', $duracion[1]];
                $condicion[] = ['fecha_fin', '>=', $duracion[0]];
                $condicion[] = ['local_id', Encryptor::decrypt($request->local_id)];

                if ($request->concesion_id != '') {
                    $condicion[] = ['id', '!=', Encryptor::decrypt($request->concesion_id)];

                    $concesion = Concesiones::find(Encryptor::decrypt($request->concesion_id));

                    $msg = __('La concesión ha sido actualizada correctamente.');
                } else {
                    $concesion = new Concesiones();
                    $msg = __('La concesión ha sido registrada correctamente.');
                }
                $searchConcesiones = Concesiones::where($condicion)
                        ->exists();

                if ($searchConcesiones == true) {

                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Ya Posee una Concesión para ese Rango de Fechas');

                    return response()->json($result);
                }

                $concesion->local_id = Encryptor::decrypt($request->local_id);
                $concesion->empresa_id = $empresa_id;
                $concesion->actividad_id = $request->actividad;
                $concesion->otra_actividad = $otro_actividad;
                
                $concesion->cedula = $request->cedula;
                $concesion->nombre = $request->nombre;
                $concesion->telefono = $request->telefono;
                $concesion->correo = $request->correo;
                
                $concesion->cedula2 = $request->cedula2;
                $concesion->nombre2 = $request->nombre2;
                $concesion->telefono2 = $request->telefono2;
                $concesion->correo2 = $request->correo2;
                
                
                
                
                $concesion->ingresos_brutos = $request->ingresos_brutos;
                $concesion->convenio_euro_integrado = $request->convenio_euro_integrado;
                $concesion->canon_fijo = $request->canon_fijo;
                $concesion->fecha_inicio = $duracion[0];
                $concesion->fecha_fin = $duracion[1];
                $concesion->user_id = Auth::user()->id;
                $concesion->fecha_registro = Carbon::now();
                $concesion->ip = $request->ip();
                $concesion->save();

                $data = Concesiones::with('locales.usos_local', 'empresas', 'actividades')->find($concesion->id);

                $message = ['type' => 'success', 'status' => 1, 'message' => $msg, 'data' => $data];
            }



            return response()->json($message);
        } else {
            return to_route('concesiones.index');
        }
    }

    public function updateLocal(Request $request) {
        $request->validate([
            'numero_local' => 'required|max:255',
            'uso' => 'required',
            'metros' => 'required|max:255',
        ]);

        $maest_local = Locales::find(Encryptor::decrypt($request->local_id));
        $maest_local->otro_uso = Upper($request->otro_uso);

        $maest_local->numero = Upper($request->numero_local);
        $maest_local->metros = $request->metros;
        $maest_local->uso_id = $request->uso;
        $maest_local->observacion = Upper($request->observacion);
        $maest_local->verificado = ($request->verificado == -1 ? null : $request->verificado);

        $maest_local->user_id = Auth::user()->id;
        $maest_local->ip = $request->ip();
        $maest_local->save();

        $message = $maest_local->id == true ? ['icon' => 'success', 'title' => '¡Éxito!', 'message' => 'El local ha sido actualizado correctamente.', 'data' => $maest_local] : ['icon' => 'error', 'title' => '¡Error!', 'message' => 'Ha ocurrido un error.'];

        return response()->json($message);
    }

    public function searchCliente($tipo, $rif) {
        $cliente = \App\Models\Clientes::where('tipo_documento', $tipo )->where('documento',  'ilike' , $rif )->first();

        $message = isset($cliente) == true ? ['icon' => 'success', 'title' => '¡Éxito!', 'message' => 'Cliente encontrado exitosamente.', 'data' => $cliente] : ['icon' => 'info', 'title' => '!Atención!', 'data' => [], 'message' => 'El cliente no ha sido encontrado, puede registrarlo a continuación.'];

        return response()->json($message);
    }

    public function searchLocal($id) {
        $local = Locales::with('usos_local', 'concesion.empresas')->find(Encryptor::decrypt($id));

        $message['status'] = 1;
        $message['data'] = $local;
        $message['message'] = 'Local encontrado exitosamente.';
        $message['type'] = 'success';

        return response()->json($message);
    }

    public function searchConcesiones($id) {
        $concesion = Concesiones::with('locales.usos_local', 'empresas', 'actividades')->find(Encryptor::decrypt($id));
        $msg['data'] = $concesion;
        if ($concesion !== null) {

            $msg['icon'] = "success";
            $msg['title'] = "¡Éxito!";
            $msg['message'] = "Concesión encontrada exitosamente.";
        } else {
            $msg['icon'] = "info";
            $msg['title'] = "¡Atención!";
            $msg['message'] = "La concesion no ha sido encontrada.";
        }
        return response()->json($msg);
    }

    public function listConcesiones(Request $request) {

        $query = Locales::with('usos_local', 'concesion.empresas', 'concesion.actividades')->orderBy('id', 'asc');
        if ($request->disponibles == '1') {
            $query->whereDoesntHave('concesion');
        }
        if ($request->aeropuerto_id != '') {
            $query->where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id));
        }

        $locales = $query->get();

        $data = Datatables::of($locales)
                ->addIndexColumn()
                ->addColumn('empresa', function ($row) {
                    $empresa = "";
                    if (isset($row->concesion)) {

                        if (isset($row->concesion->empresas)) {
                            $empresa = $row->concesion->empresas->full_razon_social;
                        } else {
                            $empresa = $row->concesion->nombre_empresa;
                        }
                    } else {
                        $empresa = "";
                    }
                    return $empresa;
                })
                ->addColumn('actividad', function ($row) {
                    $actividad = "";
                    if (isset($row->concesion)) {

                        $actividad = $row->concesion->actividades->actividad;
                    } else {
                        $actividad = "";
                    }
                    return $actividad;
                })
                ->addColumn('canon_fijo', function ($row) {
                    $canon_fijo = "";
                    if (isset($row->concesion)) {
                        $canon_fijo = $row->concesion->canon_fijo;
                    } else {
                        $canon_fijo = "";
                    }
                    return $canon_fijo;
                })
                ->addColumn('ingresos_brutos', function ($row) {
                    $ingresos_brutos = "";
                    if (isset($row->concesion)) {
                        $ingresos_brutos = $row->concesion->ingresos_brutos;
                    } else {
                        $ingresos_brutos = "";
                    }
                    return $ingresos_brutos;
                })
                ->addColumn('fecha_inicio', function ($row) {
                    $fecha_inicio = "";
                    if (isset($row->concesion)) {
                        $fecha_inicio = showDate($row->concesion->fecha_inicio);
                    } else {
                        $fecha_inicio = "";
                    }
                    return $fecha_inicio;
                })
                ->addColumn('fecha_fin', function ($row) {
                    $fecha_fin = "";
                    if (isset($row->concesion)) {
                        $fecha_fin = showDate($row->concesion->fecha_fin);
                    } else {
                        $fecha_fin = "";
                    }
                    return $fecha_fin;
                })
                ->addColumn('boton', function ($row) {
                    $idconcesion = $row->toArray();
                    
                    $idconcesion = isset($idconcesion['concesion']['crypt_id']) ? $idconcesion['concesion']['crypt_id'] : '';
                    $modalHistorial = '<button type="button" onclick="datosModalHistorial(\'' . $row->crypt_id . '\', \'' . $row->numero . '\')" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#historialModal" title="Historial"><i class="fas fa-book"></i></button>';

                    $modalLocalConcesion = '<button type="button" onclick="datosModal(\'' . $row->crypt_id . '\', \'' . $idconcesion . '\')" class="ml-2 btn btn-info btn-xs"  title="Modificar"><i class="far fa-edit"></i></button>';

                    return $modalHistorial . $modalLocalConcesion;
                })
                ->addColumn('class', function ($row) {
                    $class = "";
                    if ($row->verificado === true) {
                         $class = "alert-success";
                    } else {
                        if ($row->verificado === false) {
                            $class = "alert-warning";
                        } else {
                            $class = "alert-danger";
                            
                        }
                    }
                    return $class;
                })
                ->rawColumns(['boton'])
        //->make(true)
        ;

        return $data->toJson();
    }

    public function listHistorialConcesiones(Request $request, $id) {

        $concesiones = Concesiones::where('local_id', Encryptor::decrypt($id))->with('empresas', 'actividades')->orderBy('id', 'asc')->get();
        $data = Datatables::of($concesiones)
                ->addIndexColumn()
                ->addColumn('empresa', function ($row) {
                    $empresa = "";

                    if (isset($row->empresas)) {
                        $empresa = $row->empresas->full_razon_social;
                    } else {
                        $empresa = $row->nombre_empresa;
                    }

                    return $empresa;
                })
                ->addColumn('actividad', function ($row) {
                    $actividad = "";
                    $actividad = $row->actividades->actividad;
                    return $actividad;
                })
                ->addColumn('canon_fijo', function ($row) {
                    $canon_fijo = "";
                    $canon_fijo = $row->canon_fijo;
                    return $canon_fijo;
                })
                ->addColumn('ingresos_brutos', function ($row) {
                    $ingresos_brutos = "";

                    $ingresos_brutos = $row->ingresos_brutos;

                    return $ingresos_brutos;
                })
                ->addColumn('fecha_inicio', function ($row) {
                    $fecha_inicio = "";

                    $fecha_inicio = showDate($row->fecha_inicio);

                    return $fecha_inicio;
                })
                ->addColumn('fecha_fin', function ($row) {
            $fecha_fin = "";

            $fecha_fin = showDate($row->fecha_fin);

            return $fecha_fin;
        })


        //->make(true)
        ;
        return $data->toJson();
    }
    
    public function ExcelHistorial($id){
        $local = Locales::with("aeropuerto")->find(Encryptor::decrypt($id) );
        
        return Excel::download(new ConcesionesExport($id), $local->aeropuerto->codigo_oaci.'_LOCAL_'.$local->numero.'.xlsx');
    }    

}
