<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Clientes as Aerolineas;

class AerolieasController extends Controller {

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Aerolineass_active = Aerolineas::where("activo", 1)->where('tipo_id', $this->TIPOS_CLIENTE_AEROLINEA)->get();

            $Aerolineass_disable = Aerolineas::where("activo", 0)->where('tipo_id', $this->TIPOS_CLIENTE_AEROLINEA)->get();

            $Aerolineass_active = Datatables::of($Aerolineass_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aerolineas.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aerolineas.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                        }




                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $Aerolineass_disable = Datatables::of($Aerolineass_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";
                        
                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aerolineas.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }
                        


                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Aerolineass_active->original['data'];
            $data2 = $Aerolineass_disable->original['data'];
            
           // dd($data2);

            return view('aerolineas.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'tipo_documento' => 'required',
                            'documento' => 'required',
                            'razon_social' => 'required',
                            'telefono' => 'required',
                            'abreviatura' => 'required',
                            'direccion' => 'required'
                                ], [
                            'tipo_documento.required' => __('El Tipo de Documento es Requerido'),
                            'documento.unique' => __('El Documento es Requerido'),
                            'razon_social.required' => __('La Razón Social es Requerido'),
                            'telefono.required' => __('El Teléfono es Requerido'),
                            'abreviatura.required' => __('Las Siglas es Requerido'),
                            'direccion.required' => __('La Dirección es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aerolineas = new Aerolineas();
                    $Aerolineas->tipo_documento = $request->tipo_documento;
                    $Aerolineas->documento = $request->documento;
                    $Aerolineas->razon_social = $request->razon_social;
                    $Aerolineas->telefono = $request->telefono;
                    $Aerolineas->abreviatura = $request->abreviatura;
                    $Aerolineas->direccion = $request->direccion;

                    $Aerolineas->tipo_id = $this->TIPOS_CLIENTE_AEROLINEA;

                    $Aerolineas->user_id = Auth::user()->id;
                    $Aerolineas->ip = $this->getIp();
                    $Aerolineas->fecha_registro = now();

                    $Aerolineas->save();
                    
                    
                    
                    if ($request->file('imagen') != null) {
                        $file = $request->file('imagen');
                        // dd($request->img);
                        try {


                            $fileName = "aerolinea_" . $Aerolineas->id . '.' . $file->getClientOriginalExtension();
                            $Aerolineas->imagen = $file->getClientOriginalExtension();
                            $Aerolineas->save();
                            $file->move(public_path('dist'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'aerolineas'), $fileName);
                            //$file->move(storage_path('homologacion'), $fileName);
                            //$request->img->move(public_path('pagos'), $fileName);
                            //$request->img->move(storage_path('homologacion'), $fileName);
                            //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                            //$request->img->move(public_path('pagos'), $fileName);
                        } catch (Exception $ex) {
                            
                        }
                        //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                    }
                    
                    

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                return view('aerolineas.create');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                
                $Validator = \Validator::make(
                                $request->all(), [
                            'tipo_documento' => 'required',
                            'documento' => 'required',
                            'razon_social' => 'required',
                            'telefono' => 'required',
                            'abreviatura' => 'required',
                            'direccion' => 'required'
                                ], [
                            'tipo_documento.required' => __('El Tipo de Documento es Requerido'),
                            'documento.unique' => __('El Documento es Requerido'),
                            'razon_social.required' => __('La Razón Social es Requerido'),
                            'telefono.required' => __('El Teléfono es Requerido'),
                            'abreviatura.required' => __('Las Siglas es Requerido'),
                            'direccion.required' => __('La Dirección es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Aerolineas = Aerolineas::find(Encryptor::decrypt($id));
                    $Aerolineas->tipo_documento = $request->tipo_documento;
                    $Aerolineas->documento = $request->documento;
                    $Aerolineas->razon_social = $request->razon_social;
                    $Aerolineas->telefono = $request->telefono;
                    $Aerolineas->abreviatura = $request->abreviatura;
                    $Aerolineas->direccion = $request->direccion;

                    $Aerolineas->tipo_id = $this->TIPOS_CLIENTE_AEROLINEA;

                    $Aerolineas->user_id = Auth::user()->id;
                    $Aerolineas->ip = $this->getIp();
                    $Aerolineas->fecha_registro = now();

                    $Aerolineas->save();
                    
                    
                     if ($request->file('imagen') != null) {
                        $file = $request->file('imagen');
                        // dd($request->img);
                        try {


                            $fileName = "aerolinea_" . $Aerolineas->id . '.' . $file->getClientOriginalExtension();
                            $Aerolineas->imagen = $file->getClientOriginalExtension();
                            $Aerolineas->save();
                            $file->move(public_path('dist'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'aerolineas'), $fileName);
                            //$file->move(storage_path('homologacion'), $fileName);
                            //$request->img->move(public_path('pagos'), $fileName);
                            //$request->img->move(storage_path('homologacion'), $fileName);
                            //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                            //$request->img->move(public_path('pagos'), $fileName);
                        } catch (Exception $ex) {
                            
                        }
                        //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                    }
                    

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $Aerolineas = Aerolineas::find(Encryptor::decrypt($id));
                
                return view('aerolineas.edit', compact('Aerolineas'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Aerolineas = Aerolineas::find(Encryptor::decrypt($id));
                $Aerolineas->activo = 0;

                $Aerolineas->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Aerolineas = Aerolineas::find(Encryptor::decrypt($id));
                return view('aerolineas.disable', compact('Aerolineas'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Aerolineas = Aerolineas::find(Encryptor::decrypt($id));
                $Aerolineas->activo = 1;

                $Aerolineas->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Aerolineas = Aerolineas::find(Encryptor::decrypt($id));
                return view('aerolineas.active', compact('Aerolineas'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
