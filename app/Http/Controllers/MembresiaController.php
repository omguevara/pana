<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Membresia;
use Maatwebsite\Excel\Facades\Excel;

class MembresiaController extends Controller {

    public function reporte() {
        return Excel::download(new \App\Exports\Membresias(), 'Membresias_Activas.xlsx');
    }

    public function datatable(Request $request) {
        $Membresias_active = Membresia::join('maest_aeronaves', 'maest_membresias.aeronave_id', '=', 'maest_aeronaves.id')
                ->select("maest_membresias.*", "maest_aeronaves.matricula", "maest_aeronaves.nombre")
                ->where('fecha_hasta', '>=', date('Y-m-d'))
                ->get();
        //dd($Membresias_active->toArray());

        $Membresias_active = Datatables::of($Membresias_active)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $actionBtn = "";
                    //$actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('membresias.see', $row->crypt_id) . '" class=" btn btn-warning btn-sm"><li class="fa fa-eye"></li> ' . __('Ver') . '</a> ';
                    $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('membresias.edit', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-edit"></li> </a> ';

                    return $actionBtn;
                })
                ->rawColumns(['action'])
        //->make(true)
        ;

        return $Membresias_active->toJson();
    }

    public function datatable2(Request $request) {
        $Membresias_inactive = Membresia::join('maest_aeronaves', 'maest_membresias.aeronave_id', '=', 'maest_aeronaves.id')
                ->where('fecha_hasta', '<', date('Y-m-d'))
                ->get();
        //dd($Membresias_active->toArray());

        $Membresias_inactive = Datatables::of($Membresias_inactive)
        ->addIndexColumn()
        ->addColumn('action', function ($row) {
        $actionBtn = "";

        //$actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('membresias.see', $row->crypt_id) . '" class=" btn btn-warning btn-sm"><li class="fa fa-eye"></li> ' . __('Ver') . '</a> ';




        return $actionBtn;
        })
        ->rawColumns(['action'])
        //->make(true)
        ;

        return $Membresias_inactive->toJson();
    }

    function index() {

        if (\Request::ajax()) {

            return view('membresias.index');
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());
                $Validator = \Validator::make(
                                $request->all(), [
                            'rango' => 'required',
                            'aeronave_id' => 'required',
                            'empresa' => 'required',
                            'tipo_membresia_id' => 'required',
                            'exonerar' => 'required',
                                ], [
                            'rango.required' => __('El Campo Rango es Requerido'),
                            'nombre.required' => __('La Aeronave es Requerida'),
                            'empresa.required' => __('La Empresa es Requerida'),
                            'tipo_membresia_id.required' => __('El Tipo de Membresis es Requerida'),
                            'exonerar.required' => __('Los Servicios a Exonerar son Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {
                    //dd($request->All());
                    $aeronave = Encryptor::decrypt($request->aeronave_id);
                    $fechas = explode(" - ", $request->rango);
                    $fecha_desde = $this->saveDate($fechas[0]);
                    $fecha_hasta = $this->saveDate($fechas[1]);

                    $Membresia = Membresia::where('aeronave_id', $aeronave)
                            ->where('fecha_desde', '<=', $fecha_hasta)
                            ->where('fecha_hasta', '>=', $fecha_desde)
                            ->count();
                    if ($Membresia > 0) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('La Aeronave ya Posee una Membresia para ese Rango de Fechas');
                    } else {





                        $Membresias = new Membresia();
                        $Membresias->aeronave_id = $aeronave;
                        $Membresias->fecha_desde = $fecha_desde;
                        $Membresias->fecha_hasta = $fecha_hasta;
                        $Membresias->tipo_membresia_id = $request->tipo_membresia_id;
                        $Membresias->empresa = $request->empresa;
                        $Membresias->exonerar = implode("|", $request->exonerar);
                        $Membresias->observacion = $request->observacion;

                        
                        if ($request->opciones == '1') {
                            $activo = true;
                        } else {
                            $activo = false;
                        }
                        
                        $Membresias->activo = $activo;

                        $Membresias->user_id = Auth::user()->id;
                        $Membresias->ip = $this->getIp();
                        $Membresias->fecha_registro = now();

                        $Membresias->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                    }
                }
                return $result;
            } else {

                return view('membresias.create');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                //dd($request->all());
                $Validator = \Validator::make(
                                $request->all(), [
                            'rango' => 'required',
                            'empresa' => 'required',
                            'tipo_membresia_id' => 'required',
                            'exonerar' => 'required',
                                ], [
                            'rango.required' => __('El Campo Rango es Requerido'),
                            'empresa.required' => __('La Empresa es Requerida'),
                            'tipo_membresia_id.required' => __('El Tipo de Membresis es Requerida'),
                            'exonerar.required' => __('Los Servicios a Exonerar son Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {
                    //dd($request->All());
                    $aeronave = Encryptor::decrypt($request->aeronave_id);
                    $fechas = explode(" - ", $request->rango);
                    $fecha_desde = $this->saveDate($fechas[0]);
                    $fecha_hasta = $this->saveDate($fechas[1]);

                    $Membresia = Membresia::where('aeronave_id', $aeronave)
                            ->where('fecha_desde', '<=', $fecha_hasta)
                            ->where('fecha_hasta', '>=', $fecha_desde)
                            ->count();
                    if ($Membresia > 0) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('La Aeronave ya Posee una Membresia para ese Rango de Fechas');
                    } else {




                        $Membresias = Membresia::find(Encryptor::decrypt($id));

                        $Membresias->fecha_desde = $fecha_desde;
                        $Membresias->fecha_hasta = $fecha_hasta;
                        $Membresias->tipo_membresia_id = $request->tipo_membresia_id;
                        $Membresias->empresa = $request->empresa;
                        $Membresias->exonerar = implode("|", $request->exonerar);
                        $Membresias->observacion = $request->observacion;

                        if ($request->opciones == '1') {
                            $activo = true;
                          } else {
                            $activo = false;
                          }
                          
                          $Membresias->activo = $activo;

                        $Membresias->user_id = Auth::user()->id;
                        $Membresias->ip = $this->getIp();
                        $Membresias->fecha_registro = now();

                        $Membresias->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                    }
                }
                return $result;
            } else {
                $Membresia = Membresia::join('maest_aeronaves', 'maest_membresias.aeronave_id', '=', 'maest_aeronaves.id')
                        ->select("maest_membresias.*", "maest_aeronaves.matricula", "maest_aeronaves.nombre")
                        ->where('maest_membresias.id', '=', Encryptor::decrypt($id))
                        ->first();
                // dd(Encryptor::decrypt($id));
                return view('membresias.edit', compact('Membresia'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function delete($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {


                $Feriado = Membresias::find(Encryptor::decrypt($id));
                $Feriado->delete();
                return redirect()->route('feriados');
            } else {
                return Redirect::to('/');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function verify(Request $request){
        $membresia = Membresia::with('aeronaves')->find(Encryptor::decrypt($request->id));

        if ($membresia != null) {
            $matricula = $membresia->aeronaves->matricula;
            $fecha_desde = showDate($membresia->fecha_desde);
            $fecha_hasta = showDate($membresia->fecha_hasta);
            $response = [
                'type' => 'success',
                'message' => 'Membresía encontrada',
                'data' => $membresia,
                "html" => "<div id='info' class='info-box bg-gradient-warning mt-2 info-pulse'>
                            <span class='info-box-icon'><i class='fa fa-info'></i></span>
                            <div class='info-box-content'>
                              <span class='info-box-text'>INFORMACIÓN:</span>
                              <span id='text-info' class='info-box-number' style=''>La Aeronave Posee Membresía<br>Fecha Desde: $fecha_desde, Fecha Hasta: $fecha_hasta<br>Empresa: $membresia->empresa<br>Tipo: $membresia->tipo<br>Exonerar: $membresia->exonerar2 <br></span>
                            </div>
                          </div>"
            ];
        } else {
            $response = null;
        }
        
        return response()->json($response);
    }
}
