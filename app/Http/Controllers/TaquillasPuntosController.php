<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use Illuminate\Support\Str;
use App\Models\Taquillas;
use App\Models\TaquillasPuntos;

class TaquillasPuntosController extends Controller {

    public function add_points($id, Request $request) {


        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                //dd($request->all());

                $Validator = \Validator::make(
                                $request->all(), [
                            'serial' => 'required',
                            'banco_id' => 'required'
                                ], [
                            'serial.required' => __('El Serial es Requerido'),
                            'banco_id.required' => __('El Banco es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $TaquillasP = new TaquillasPuntos();
                    $TaquillasP->taquilla_id = Encryptor::decrypt($id);
                    $TaquillasP->serial = $request->serial;
                    $TaquillasP->banco_id = Encryptor::decrypt($request->banco_id);

                    $TaquillasP->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                if ($request->isMethod('delete')) {
                    
                    //TaquillasPuntos::find(Encryptor::decrypt($id))->delete();
                    $TaquillasPuntos = TaquillasPuntos::find(Encryptor::decrypt($id));
                    $TaquillasPuntos->activo = false;
                    $TaquillasPuntos->save();
                            
                            
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    return $result;
                } else {
                    $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                    $Puntos = TaquillasPuntos::where('taquilla_id', Encryptor::decrypt($id))
                            ->where('activo', true)
                            ->get()->toArray();

                    $aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->where("pertenece_baer", 1)->orderBy("nombre")->get()->pluck("nombre", "crypt_id");
                    $bancos = \App\Models\Bancos::orderBy("nombre")->get()->pluck("nombre", "crypt_id");
                    return view('taquillas.add_points', compact('Taquillas', 'Puntos', 'bancos', 'aeropuertos'));
                }
            }
        } else {
            return Redirect::to('/');
        }
    }

}
