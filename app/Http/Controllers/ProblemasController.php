<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Problemas;

class ProblemasController extends Controller {

    public function index($id = null) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                if ($id != null) {

                    $id = \App\Helpers\Encryptor::decrypt($id);
                    if ($id != 0) {
                        $datos = Problemas::find($id);
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Datos Encontrados";
                        $result['data'] = $datos->toArray();

                        return $result;
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = "Error Al Consultar";
                        $result['data'] = [];

                        return $result;
                    }
                } else {

                    if (Auth::User()->aeropuerto_id == null) {
                        $Errors = Problemas::orderBy('fecha_registro')->get();
                        $Errors = Problemas::get();
                    } else {
                        $Errors = Problemas::where("aeropuerto_id", Auth::User()->aeropuerto_id)->orderBy('fecha_registro')->get();
                    }


                    $Errors = Datatables::of($Errors)
                            ->addIndexColumn()
                            ->addColumn('action', function ($row) {
                                $actionBtn = "";
                                /*
                                  if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                                  $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('aerolineas.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                                  }
                                 */

                                $actionBtn .= '<a onClick="getDetailError(this, event)" href="' . route('problemas', $row->crypt_id) . '" class="actions_users btn btn-info btn-sm"><li class="fa fa-eye"></li></a> ';

                                return $actionBtn;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
                    $data = $Errors->original['data'];
                }
                return view('problemas.index', compact('data'));
            } else {
                if (\Request::isMethod('post')) {

                    $Validator = \Validator::make(
                                    \Request::all(), [
                                'modulo' => 'required',
                                'descripcion' => 'required',
                                    ], [
                                'modulo.required' => __('El "Módulo" es Requerido'),
                                'descripcion.required' => __('la Descripción es Requerido')
                                    ]
                    );
                    $request = \Request::all();
                    if ($Validator->fails()) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = $Validator->errors()->first();
                    } else {
                        $Problemas = new Problemas();
                        $Problemas->user_id = Auth::User()->id;
                        $Problemas->aeropuerto_id = Auth::User()->aeropuerto_id == null ? 1 : Auth::User()->aeropuerto_id;
                        $Problemas->modulo = $request['modulo'];
                        $Problemas->descripcion = clean($request['descripcion']);
                        $Problemas->fecha_registro = now();
                        $Problemas->ip = $this->getIp();
                        $Problemas->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Registrado Correctamente";
                    }
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/');
        }
    }

}
