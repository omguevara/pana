<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use DataTables;
use PDF;
use App\Models\Prodservicios;
use App\Models\Aeronaves;

class ProdserviciosController extends Controller {

    private $CantDec=[1=>8,2=>2];

    public function get_total_serv(Request $request) {
        if (\Request::ajax()) {
            $data['serv_extra'] = [];
            if ($request->isMethod('post')) {
                if (isset($request->newPortal)) {

                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['carga'] = isset($request->carga) ? saveFloat($request->carga)*1000:0;
                    $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                    $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                    $data['cant_pasajeros'] = $request->pax;
                    $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion);
                    $data['hora_vuelo'] = $request->hora_operacion;
                    if (isset($request->aeropuerto_id)) {
                        $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                    } else {
                        $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                    }
                    if ($request->prod_servd == null) {
                        $data['serv_extra'] = [];
                    } else {
                        $data['serv_extra']['ids'] = [];
                        foreach ($request->prod_servd as $value) {

                            $servp = explode(":", $value);
                            $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($servp[1]);
                            $data['serv_extra']['cant'][$servp[1]] = $servp[0];
                        }
                    }
                    if (isset($request->estacionamiento)) {
                        $data['hora_llegada_est'] = saveDateTime($request->hora_llegada);
                        $data['hora_salida_est'] = saveDateTime($request->hora_salida);
                    }
                    if ($request->tipo_id == 1) { // si es llegada
                        $data['hora_llegada'] = saveDateTime($request->fecha_operacion . ' ' . $request->hora_operacion);
                        $data['exento_dosa'] = false;
                        $data['exento_tasa'] = true;
                    } else { // si es salida
                        $data['hora_salida'] = saveDateTime($request->fecha_operacion . ' ' . $request->hora_operacion);
                        $data['exento_dosa'] = true;
                        $data['exento_tasa'] = false;
                    }

                    $servicios_facturar = $this->getProdServ($data);


                    $list = [];
                    foreach ($servicios_facturar['data'] as $key => $value) {

                        if (in_array($value['codigo'], $list)) {
                            unset($servicios_facturar['data'][$key]);
                        } else {
                            $list[] = $value['codigo'];
                        }
                    }


                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $servicios_facturar ['data'];


                    return $result;
                } else {
                    //dd($request->all());
                    /*
                      "prod_servd" => array:3 [
                      0 => "1:VFB1YUtMSUM0VWx0ZnN6MFVwWTMrUT09"
                      1 => "1:OSsrdUtucUVFVGljV25xMWRMMzRidz09"
                      2 => "1:SXF4L0pyOWpoZjVCenpvcDRJTVhrdz09"
                      ]
                      "type_document" => "V"
                      "document" => "16345339"
                      "razon" => "MIGUEL RIVERO"
                      "phone" => "(0412)-753.33.74"
                      "correo" => "miguel@gmail.com"
                      "direccion" => "CARACAS"
                      "" => "SG5ld056WXJlOWxRUEhXRjFkenBUZz09"
                      "aeronave_id" => "VjNUaGlBcm0zWC8rZGZuWnNOSEdGZz09"
                      "piloto_id" => "bGYrQldBVEo2ZldwbDhkQmRvY3R6dz09"
                      "" => "1"
                      "fecha_operacion" => "13/05/2023"
                      "cobrar_tasa" => "1"
                      "cobrar_dosa" => "1"
                      "pax_desembarcados" => "3"
                      "pax_embarcados" => "3"
                      "hora_llegada" => "13/05/2023 08:00"
                      "hora_salida" => "13/05/2023 16:00"
                      "observacion" => null

                     */

                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional

                    $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);

                    if ($request->cobrar_tasa == true) {
                        $hora_operacion = explode(' ', $request->hora_salida);
                        $data['hora_vuelo'] = $hora_operacion[1];
                    } else {
                        if ($request->cobrar_dosa == true) {
                            $hora_operacion = explode(' ', $request->hora_llegada);
                            $data['hora_vuelo'] = $hora_operacion[1];
                        }
                    }

                    if ($request->hora_llegada != null) {
                        $data['hora_llegada'] = saveDateTime($request->hora_llegada);
                    }

                    if ($request->hora_salida != null) {
                        $data['hora_salida'] = saveDateTime($request->hora_salida);
                    }

                    $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);

                    $data['cant_pasajeros'] = $request->pax_embarcados;
                    $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion) . " 00:00:00";

                    if ($request->prod_servd == null) {
                        $data['serv_extra'] = [];
                    } else {
                        $data['serv_extra']['ids'] = [];
                        foreach ($request->prod_servd as $value) {

                            $servp = explode(":", $value);
                            $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($servp[1]);
                            $data['serv_extra']['cant'][$servp[1]] = $servp[0];
                        }
                    }


                    //$data['serv_extra'] = $request->prod_servd==null ? []:$request->prod_servd;
                    //$data['cobrar_dosa'] = $request->cobrar_dosa;
                    //$data['cobrar_tasa'] = $request->cobrar_tasa;

                    $data['exento_dosa'] = !$request->cobrar_dosa;
                    $data['exento_tasa'] = !$request->cobrar_tasa;

                    //dd($data);

                    $servicios_facturar = $this->getProdServ($data);
                    //dd($servicios_facturar);
                    $list = [];
                    foreach ($servicios_facturar['data'] as $key => $value) {

                        if (in_array($value['codigo'], $list)) {
                            unset($servicios_facturar['data'][$key]);
                        } else {
                            $list[] = $value['codigo'];
                        }
                    }


                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $servicios_facturar ['data'];

                    return $result;
                }
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function get_total_serv2(Request $request) {
        if (\Request::ajax()) {
            $data['serv_extra'] = [];
            if ($request->isMethod('post')) {

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['cant_pasajeros'] = $request->pax;
                if (isset($request->aeropuerto_id)) {
                    $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                } else {
                    $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                }

                $data['hora_llegada_est'] = saveDateTime($request->hora_llegada_est);
                $data['hora_salida_est'] = saveDateTime($request->hora_salida_est);

                $servicios_llegada['data'] = [];
                if ($request->llegada != null) {
                    if ($request->carga_ll != null) {
                        $data['carga'] = saveFloat($request->carga_ll);
                    }
                    $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                    $data['hora_vuelo'] = $request->hora_llegada;

                    $data['serv_extra'] = [];
                    if ($request->in != null) {
                        $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_llegada . ' ' . $request->hora_llegada);
                        $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                        $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                        $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                        $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                        $prodserv_extra = $this->getProdservicios2('extra', $data);
                        $data['serv_extra']['ids'] = [];

                        foreach ($prodserv_extra as $value) {
                            foreach ($request->in as $key => $value2) {
                                if (strpos($value['codigo'], $key) === 0) {

                                    $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                                    $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                                }
                            }
                        }
                    }


                    $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                    $data['hora_vuelo'] = $request->hora_llegada;




                    $data['hora_llegada'] = saveDateTime($request->fecha_llegada . ' ' . $request->hora_llegada);
                    $data['exento_dosa'] = false;
                    $data['exento_tasa'] = true;


                    $servicios_llegada = $this->getProdServ($data);
                    $list = [];
                    foreach ($servicios_llegada['data'] as $key => $value) {

                        if (in_array($value['codigo'], $list)) {
                            unset($servicios_llegada['data'][$key]);
                        } else {
                            $list[] = $value['codigo'];
                        }
                    }
                }

                $servicios_salida['data'] = [];
                if ($request->salida != null) {
                    if ($request->carga_ss != null) {
                        $data['carga'] = saveFloat($request->carga_ss);
                    }
                    $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                    $data['hora_vuelo'] = $request->hora_salida;

                    $data['serv_extra'] = [];
                    if ($request->ou != null) {
                        $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_salida . ' ' . $request->hora_salida);
                        $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                        $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                        $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                        $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                        $prodserv_extra = $this->getProdservicios2('extra', $data);
                        $data['serv_extra']['ids'] = [];

                        foreach ($prodserv_extra as $value) {
                            foreach ($request->ou as $key => $value2) {
                                if (strpos($value['codigo'], $key) === 0) {

                                    $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                                    $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                                }
                            }
                        }
                    }


                    $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                    $data['hora_vuelo'] = $request->hora_salida;




                    $data['hora_salida'] = saveDateTime($request->fecha_salida . ' ' . $request->hora_salida);
                    $data['exento_dosa'] = true;
                    $data['exento_tasa'] = false;

                    $servicios_salida = $this->getProdServ($data);
                    $list = [];
                    foreach ($servicios_salida['data'] as $key => $value) {

                        if (in_array($value['codigo'], $list)) {
                            unset($servicios_salida['data'][$key]);
                        } else {
                            $list[] = $value['codigo'];
                        }
                    }
                }

//dd($servicios_salida['data']);
                $prodServs = [];
                if (count($servicios_llegada['data']) > 0) {
                    foreach ($servicios_llegada['data'] as $key1=>$value) {
                        $prodServs[$key1] = $value;
                        foreach ($servicios_salida['data'] as $key => $value2) {
                            if ($value['codigo'] == $value2['codigo'] && $value['codigo']!= '2.2.1') {
                                $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                                $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                                unset($servicios_salida['data'][$key]);
                            }
                        }
                    }

                    foreach ($servicios_salida['data'] as $key => $value2) {
                        $prodServs[] = $value2;
                    }
                } else {
                    if (count($servicios_salida['data']) > 0) {
                        foreach ($servicios_salida['data'] as $key1=>$value) {
                            $prodServs[$key1] = $value;
                            foreach ($servicios_llegada['data'] as $value2) {
                                if ($value['codigo'] == $value2['codigo'] && $value['codigo']!= '2.2.1') {
                                    $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                                    $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                                    unset($servicios_llegada['data'][$key]);

                                }
                            }
                        }
                        foreach ($servicios_llegada['data'] as $key => $value2) {
                            $prodServs[] = $value2;
                        }
                    }
                }


                $existentes = [];
                foreach($prodServs as $key=>$value){
                    if ( in_array($value['codigo'], $existentes ) ){
                        unset($prodServs[$key]);
                    }
                    $existentes[]=$value['codigo'];
                }

                $hora_llegada_est = Carbon::parse($data['hora_llegada_est']);
                $startDatef = $hora_llegada_est->format('Y-m-d');

                $hora_salida_est = Carbon::parse($data['hora_salida_est']);
                $endDatef = $hora_salida_est->format('Y-m-d');

                $startDate = Carbon::parse($startDatef);
                $endDate = Carbon::parse($endDatef);
                $days = [];

                for ($date = $startDate; $date->lte($endDate); $date->addDay()) {
                    $days[] = $date->format('Y-m-d');
                }
                $feri=0;
                foreach ($days as $day) {
                    $h=DB::table('maest_feriados')->where('fecha',$day)->count();
                    if($h==1){
                        $feri=1;
                        break;
                    }elseif(Carbon::parse($day)->format('l')=='Sunday'){
                        $feri=1;
                        break;
                    }
                }

                $result['data2']=$feri;
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $prodServs;

                return $result;
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function get_total_serv_Avc(Request $request) {
        if (\Request::ajax()) {
            $data['serv_extra'] = [];
            if ($request->isMethod('post')) {
                $data['tipo_vuelo_id'] = 1; //Vuelos comercial
                $data['nacionalidad_id'] = $request->tipo_id; // NAcional o interncional
                $data['aeropuerto_id']=Encryptor::decrypt($request->aeropuerto_id);
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['tipo_operacion']=$request->tipo_id;//1nacional 2internacional
                $data['cant_pasajeros'] = $request->cant_pax;

                $parametros=['[CANT_PASAJEROS]'=>$request->cant_pax];

                $where=['nacionalidad_id'=>$request->tipo_id,'tipo_vuelo_id'=>1];
                $r=$this->Get_Services_Avc($where,$parametros);

                //  // if (isset($request->aeropuerto_id)) {
                    //     //     $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                    //     // } else {
                    //     //     $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                    //     // }

                    //     $data['hora_llegada_est'] = saveDateTime($request->hora_salida);
                    //     $data['hora_salida_est'] = saveDateTime($request->hora_salida);



                    //     $servicios_llegada['data'] = [];
                    //     if ($request->llegada != null) {
                    //         if ($request->carga_ll != null) {
                    //             $data['carga'] = saveFloat($request->carga_ll);
                    //         }
                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                    //         $data['hora_vuelo'] = $request->hora_llegada;

                    //         $data['serv_extra'] = [];
                    //         if ($request->in != null) {
                    //             $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_llegada . ' ' . $request->hora_llegada);
                    //             $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                    //             $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                    //             $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                    //             $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                    //             $prodserv_extra = $this->getProdservicios2('extra', $data);
                    //             $data['serv_extra']['ids'] = [];

                    //             foreach ($prodserv_extra as $value) {
                    //                 foreach ($request->in as $key => $value2) {
                    //                     if (strpos($value['codigo'], $key) === 0) {

                    //                         $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                    //                         $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                    //                     }
                    //                 }
                    //             }
                    //         }


                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                    //         $data['hora_vuelo'] = $request->hora_llegada;




                    //         $data['hora_llegada'] = saveDateTime($request->fecha_llegada . ' ' . $request->hora_llegada);
                    //         $data['exento_dosa'] = false;
                    //         $data['exento_tasa'] = true;


                    //         $servicios_llegada = $this->getProdServ($data);//esta ejecuta la funcion para obtener valor del servicio
                    //         $list = [];
                    //         foreach ($servicios_llegada['data'] as $key => $value) {

                    //             if (in_array($value['codigo'], $list)) {
                    //                 unset($servicios_llegada['data'][$key]);
                    //             } else {
                    //                 $list[] = $value['codigo'];
                    //             }
                    //         }
                    //     }

                    //     $servicios_salida['data'] = [];
                    //     if ($request->salida != null) {
                    //         if ($request->carga_ss != null) {
                    //             $data['carga'] = saveFloat($request->carga_ss);
                    //         }
                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                    //         $data['hora_vuelo'] = $request->hora_salida;

                    //         $data['serv_extra'] = [];
                    //         if ($request->ou != null) {
                    //             $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_salida . ' ' . $request->hora_salida);
                    //             $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                    //             $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                    //             $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                    //             $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                    //             $prodserv_extra = $this->getProdservicios2('extra', $data);
                    //             $data['serv_extra']['ids'] = [];

                    //             foreach ($prodserv_extra as $value) {
                    //                 foreach ($request->ou as $key => $value2) {
                    //                     if (strpos($value['codigo'], $key) === 0) {

                    //                         $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                    //                         $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                    //                     }
                    //                 }
                    //             }
                    //         }


                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                    //         $data['hora_vuelo'] = $request->hora_salida;




                    //         $data['hora_salida'] = saveDateTime($request->fecha_salida . ' ' . $request->hora_salida);
                    //         $data['exento_dosa'] = true;
                    //         $data['exento_tasa'] = false;


                    //         $servicios_salida = $this->getProdServ($data);
                    //         $list = [];
                    //         foreach ($servicios_salida['data'] as $key => $value) {

                    //             if (in_array($value['codigo'], $list)) {
                    //                 unset($servicios_salida['data'][$key]);
                    //             } else {
                    //                 $list[] = $value['codigo'];
                    //             }
                    //         }
                    //     }


                    //     $prodServs = [];
                    //     if (count($servicios_llegada['data']) > 0) {
                    //         foreach ($servicios_llegada['data'] as $key1=>$value) {
                    //             $prodServs[$key1] = $value;
                    //             foreach ($servicios_salida['data'] as $key => $value2) {
                    //                 if ($value['codigo'] == $value2['codigo']) {
                    //                     $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                    //                     $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                    //                     unset($servicios_salida['data'][$key]);
                    //                 }
                    //             }
                    //         }

                    //         foreach ($servicios_salida['data'] as $key => $value2) {
                    //             $prodServs[] = $value2;
                    //         }
                    //     } else {
                    //         if (count($servicios_salida['data']) > 0) {
                    //             foreach ($servicios_salida['data'] as $key1=>$value) {
                    //                 $prodServs[$key1] = $value;
                    //                 foreach ($servicios_llegada['data'] as $value2) {
                    //                     if ($value['codigo'] == $value2['codigo']) {
                    //                         $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                    //                         $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                    //                         unset($servicios_llegada['data'][$key]);

                    //                     }
                    //                 }
                    //             }
                    //             foreach ($servicios_llegada['data'] as $key => $value2) {
                    //                 $prodServs[] = $value2;
                    //             }
                    //         }
                    //     }

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data1'] = $r['data1'];
                $result['data2'] = $r['data2'];

                return response()->json($result, 200);
            }
        } else {
            return Redirect::to('/');
        }
    }

    /*public function get_total_serv_Avc(Request $request) {
        if (\Request::ajax()) {
            $data['serv_extra'] = [];
            if ($request->isMethod('post')) {
                $data['tipo_vuelo_id'] = 1; //Vuelos comercial
                $data['nacionalidad_id'] = $request->tipo_id; // NAcional o interncional
                $data['aeropuerto_id']=Encryptor::decrypt($request->aeropuerto_id);
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['tipo_operacion']=$request->tipo_id;//1nacional 2internacional
                $data['cant_pasajeros'] = $request->cant_pax;

                $parametros=['[CANT_PASAJEROS]'=>$request->cant_pax];

                $where=['nacionalidad_id'=>$request->tipo_id,'tipo_vuelo_id'=>1];
                $r=$this->Get_Services_Avc($where,$parametros);

                //  // if (isset($request->aeropuerto_id)) {
                    //     //     $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                    //     // } else {
                    //     //     $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                    //     // }

                    //     $data['hora_llegada_est'] = saveDateTime($request->hora_salida);
                    //     $data['hora_salida_est'] = saveDateTime($request->hora_salida);



                    //     $servicios_llegada['data'] = [];
                    //     if ($request->llegada != null) {
                    //         if ($request->carga_ll != null) {
                    //             $data['carga'] = saveFloat($request->carga_ll);
                    //         }
                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                    //         $data['hora_vuelo'] = $request->hora_llegada;

                    //         $data['serv_extra'] = [];
                    //         if ($request->in != null) {
                    //             $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_llegada . ' ' . $request->hora_llegada);
                    //             $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                    //             $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                    //             $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                    //             $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                    //             $prodserv_extra = $this->getProdservicios2('extra', $data);
                    //             $data['serv_extra']['ids'] = [];

                    //             foreach ($prodserv_extra as $value) {
                    //                 foreach ($request->in as $key => $value2) {
                    //                     if (strpos($value['codigo'], $key) === 0) {

                    //                         $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                    //                         $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                    //                     }
                    //                 }
                    //             }
                    //         }


                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_llegada);
                    //         $data['hora_vuelo'] = $request->hora_llegada;




                    //         $data['hora_llegada'] = saveDateTime($request->fecha_llegada . ' ' . $request->hora_llegada);
                    //         $data['exento_dosa'] = false;
                    //         $data['exento_tasa'] = true;


                    //         $servicios_llegada = $this->getProdServ($data);//esta ejecuta la funcion para obtener valor del servicio
                    //         $list = [];
                    //         foreach ($servicios_llegada['data'] as $key => $value) {

                    //             if (in_array($value['codigo'], $list)) {
                    //                 unset($servicios_llegada['data'][$key]);
                    //             } else {
                    //                 $list[] = $value['codigo'];
                    //             }
                    //         }
                    //     }

                    //     $servicios_salida['data'] = [];
                    //     if ($request->salida != null) {
                    //         if ($request->carga_ss != null) {
                    //             $data['carga'] = saveFloat($request->carga_ss);
                    //         }
                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                    //         $data['hora_vuelo'] = $request->hora_salida;

                    //         $data['serv_extra'] = [];
                    //         if ($request->ou != null) {
                    //             $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_salida . ' ' . $request->hora_salida);
                    //             $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                    //             $data['hora_vuelo'] = $fecha_vuelo->format("H:i");
                    //             $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                    //             $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                    //             $prodserv_extra = $this->getProdservicios2('extra', $data);
                    //             $data['serv_extra']['ids'] = [];

                    //             foreach ($prodserv_extra as $value) {
                    //                 foreach ($request->ou as $key => $value2) {
                    //                     if (strpos($value['codigo'], $key) === 0) {

                    //                         $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                    //                         $data['serv_extra']['cant'][$value['crypt_id']] = $value2;
                    //                     }
                    //                 }
                    //             }
                    //         }


                    //         $data['fecha_vuelo'] = $this->saveDate($request->fecha_salida);
                    //         $data['hora_vuelo'] = $request->hora_salida;




                    //         $data['hora_salida'] = saveDateTime($request->fecha_salida . ' ' . $request->hora_salida);
                    //         $data['exento_dosa'] = true;
                    //         $data['exento_tasa'] = false;


                    //         $servicios_salida = $this->getProdServ($data);
                    //         $list = [];
                    //         foreach ($servicios_salida['data'] as $key => $value) {

                    //             if (in_array($value['codigo'], $list)) {
                    //                 unset($servicios_salida['data'][$key]);
                    //             } else {
                    //                 $list[] = $value['codigo'];
                    //             }
                    //         }
                    //     }


                    //     $prodServs = [];
                    //     if (count($servicios_llegada['data']) > 0) {
                    //         foreach ($servicios_llegada['data'] as $key1=>$value) {
                    //             $prodServs[$key1] = $value;
                    //             foreach ($servicios_salida['data'] as $key => $value2) {
                    //                 if ($value['codigo'] == $value2['codigo']) {
                    //                     $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                    //                     $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                    //                     unset($servicios_salida['data'][$key]);
                    //                 }
                    //             }
                    //         }

                    //         foreach ($servicios_salida['data'] as $key => $value2) {
                    //             $prodServs[] = $value2;
                    //         }
                    //     } else {
                    //         if (count($servicios_salida['data']) > 0) {
                    //             foreach ($servicios_salida['data'] as $key1=>$value) {
                    //                 $prodServs[$key1] = $value;
                    //                 foreach ($servicios_llegada['data'] as $value2) {
                    //                     if ($value['codigo'] == $value2['codigo']) {
                    //                         $prodServs[$key1]['cant'] += $servicios_salida['data'][$key]['cant'];
                    //                         $prodServs[$key1]['precio'] = $prodServs[$key1]['precio']*$prodServs[$key1]['cant'] ;
                    //                         unset($servicios_llegada['data'][$key]);

                    //                     }
                    //                 }
                    //             }
                    //             foreach ($servicios_llegada['data'] as $key => $value2) {
                    //                 $prodServs[] = $value2;
                    //             }
                    //         }
                    //     }

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data1'] = $r['data1'];
                $result['data2'] = $r['data2'];

                return response()->json($result, 200);
            }
        } else {
            return Redirect::to('/');
        }
    }*/

    public function Get_Services_Avc($where = [],$parametros=[]){
        $services= Prodservicios::where('activo',true);

        foreach ($where as $key => $value) {
            $services->where($key,$value);
        }
        $servi=$services->get();

        $baseI=0;
        $exento=0;
        $descuento=0;
        $subtotal=0;
        $iva=0;
        $total=0;
        $result=[];
        foreach ($servi as $key => $value) {
            $formula=str_replace('[VALOR_PETRO]',$value->valor_petro,$value->formula);
            $formula=str_replace('[VALOR_EURO]',$value->valor_euro,$formula);
            $formula=str_replace('[VALOR_DOLLAR]',$value->valor_dollar,$formula);
            foreach ($parametros as $key2 => $value2) {
                $formula=str_replace($key2,$value2,$formula);
            }
            $formApl=$formula;
            eval('$formula='.$formula.';');

            if($value->iva!=0){//1
                $baseI += $formula;
                $iva +=$formula * ($value->iva / 100);
            }else{
                $exento +=$formula;
            }
            $result[]=[
                'id'=>$value->id,
                'codigo'=>$value->codigo,
                'servicio'=>$value->descripcion,
                'formulaApl'=>$formApl,
                'formula'=> $this->FloatServices($formula,$this->CantDec[$where['nacionalidad_id']])
            ];

        }
        $subtotal += $baseI + $exento;
        $total += $subtotal + $iva;
        $totales=[
            'BaseImponible'=> $this->FloatServices($baseI,$this->CantDec[$where['nacionalidad_id']]) ,
            'Exento'=>$this->FloatServices($exento,$this->CantDec[$where['nacionalidad_id']]),
            'Descuento'=>$this->FloatServices($descuento,$this->CantDec[$where['nacionalidad_id']]),
            'Subtotal'=>$this->FloatServices($subtotal,$this->CantDec[$where['nacionalidad_id']]),
            'Iva'=>$this->FloatServices($iva,$this->CantDec[$where['nacionalidad_id']]),
            'Total'=>$this->FloatServices($total,$this->CantDec[$where['nacionalidad_id']])
        ];

        $response = [
            'data1' => $result,
            'data2'=> $totales
        ];
        return $response;
    }

    public function Get_Services_Carga_Avc(Request $request, $where = [],$parametros=[]){
        $aeronave=Aeronaves::find(Encryptor::decrypt($request->aeronave_id));
        $parametros=[
            '[PESO_AVION]'=>$aeronave->peso_maximo,
            '[EMBARQUE]'=>$request->embarque_emb,//EMBARQUE
            '[DESEMBARQUE]'=>$request->embarque_desm//DESEMBARQUE
        ];
        $where=['nacionalidad_id'=>$request->tipo_idC,'tipo_vuelo_id'=>1,'default_carga'=>1];
        $r=$this->Get_Services_Avc($where,$parametros);
        $result['status'] = 1;
        $result['type'] = 'success';
        $result['message'] = __('Processed Correctly');
        $result['data1'] = $r['data1'];
        $result['data2'] = $r['data2'];

        return response()->json($result, 200);
    }

    public function Get_Services_Dosa_Avc(Request $request){

    }

    public function Services_List_Avc($id, Request $request){
        $r=Prodservicios::where('prodservicio_extra',true)->where('nacionalidad_id',$id)->where('tipo_vuelo_id',1)->get();
        return response()->json($r, 200);
    }

    public function get_prodservicios(Request $request) {
        if (\Request::ajax()) {
            $data['serv_extra'] = [];
            if ($request->isMethod('post')) {
                //dd($request->all());

                if (isset($request->prod_servd)) {
                    if (is_array($request->prod_servd)) {
                        $data['serv_extra']['ids'] = [];
                        foreach ($request->prod_servd as $value) {
                            $detalle = explode(":", $value);
                            $cant = (int) $detalle[0];
                            if ($cant > 0) {
                                $data['serv_extra']['ids'][] = Encryptor::decrypt($detalle[1]);
                                $data['serv_extra']['cant'][$detalle[1]] = $detalle[0];
                            }
                        }
                        if (count($data['serv_extra']['ids']) == 0) {
                            unset($data['serv_extra']['ids']);
                        }
                    }
                }
                //dd($data['serv_extra']);
                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                $data['hora_vuelo'] = $request->hora_operacion;
                $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                $data['plan_vuelo'] = 2; // Doble Toque
                $data['cant_pasajeros'] = $request->cant_pasajeros;
                $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion) . " 00:00:00";

                $data['exento_dosa'] = !$request->cobrar_dosa;
                $data['exento_tasa'] = !$request->cobrar_tasa;

                $data['hora_llegada'] = $this->saveDate(substr($request->hora_llegada, 0, 10)) . " " . substr($request->hora_llegada, 11, 5);
                $data['hora_salida'] = $this->saveDate(substr($request->hora_salida, 0, 10)) . " " . substr($request->hora_salida, 11, 5);

                $prodServ = $this->getProdServ($data);

                $cods = [];
                foreach ($prodServ['data'] as $value) {
                    $cods[] = $value['codigo'];
                }
                sort($cods);
                $servs = [];
                foreach ($cods as $value) {
                    foreach ($prodServ['data'] as $value2) {
                        if ($value2['codigo'] == $value) {
                            $servs[] = $value2;
                        }
                    }
                }

                $prodServ['data'] = $servs;
                //dd($prodServ);




                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $prodServ['data'];

                return $result;
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function FloatServices($valor,$decimal){
        return number_format($valor,$decimal,',','.');
    }

    public function SingleServiceAVC(Request $request){

        $aeronave=Aeronaves::find(Encryptor::decrypt($request->aeronave_id));
        $parametros=[
            '[PESO_AVION]'=>$aeronave->peso_maximo,
            '[TIP_VUELO]'=>$request->tipo_iddtv,//1=>NAC 2=>INTER
            ''=>''
        ];

        $servicios=$request->proservicios;
        dd($request);
        dd($servicios);

    }

    public function get_servicios_operacion(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                if ($request->hora_salida != null) {
                    $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida);
                }else {
                    if ($request->hora_llegada != null) {
                        $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada);
                    } else {
                        $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_operacion . '00:00');
                    }
                }

                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);

                $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                $data['aeronave_id'] = Encryptor::decrypt($request->crypt_aeronave_id);

                $data['cant_pasajeros'] = $request->pax;

                if ($request->applyParking != null) {
                    $data['hora_llegada'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format("Y-m-d H:i");
                    $data['hora_salida'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format("Y-m-d H:i");
                }

                $data['exento_dosa'] = !$request->cobrar_dosa;
                $data['exento_tasa'] = !$request->cobrar_tasa;

                $prodserv_extra = $this->getProdservicios2('extra', $data);

                foreach ($prodserv_extra as $ind =>$value) {
                    // $esta = false;
                    foreach ($request->in as $key => $value2) {

                        if (strpos($value['codigo'], $key) === 0) {
                            // $esta = true;
                            // $cod = $key;
                            $Detalle = new \App\Models\VuelosDetalle();

                            $Detalle->aplica_categoria = $value['aplica_categoria'];
                            $Detalle->aplica_estacion = $value['aplica_estacion'];
                            $Detalle->aplica_peso = $value['aplica_peso'];
                            $Detalle->aplicable = $value['aplicable'];
                            $Detalle->bs = $value['bs'];
                            $Detalle->cant = $value2;
                            $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                            $Detalle->codigo = $value['codigo'];
                            $Detalle->prodservicio_id = \App\Helpers\Encryptor::decrypt($value['crypt_id']);
                            $Detalle->default_carga = $value['default_carga'];
                            $Detalle->default_dosa = $value['default_dosa'];
                            $Detalle->default_tasa = $value['default_tasa'];
                            $Detalle->descripcion = $value['descripcion'];
                            $Detalle->formula = $value['formula'];
                            $Detalle->formula2 = $value['formula2'];
                            $Detalle->full_descripcion = $value['full_descripcion'];
                            $Detalle->iva = $value['iva'];
                            $Detalle->iva2 = $value['iva2'];
                            $Detalle->iva_aplicado = $value['iva_aplicado'];
                            $Detalle->moneda = $value['moneda'];
                            $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                            $Detalle->nomenclatura = $value['nomenclatura'];
                            $Detalle->peso_inicio = $value['peso_inicio'];
                            $Detalle->peso_fin = $value['peso_fin'];
                            $Detalle->precio = $value['precio'];
                            $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                            $Detalle->tasa = $value['tasa'];
                            $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                            $Detalle->turno_id = $value['turno_id'];
                            $Detalle->valor_dollar = $value['valor_dollar'];
                            $Detalle->valor_euro = $value['valor_euro'];
                            $Detalle->valor_petro = $value['valor_petro'];
                            $Detalle->orden = $value['orden'];
                            $Detalle->vuelo_id = Encryptor::decrypt($request->vuelos[1]);

                            $Detalle->save();
                            break;
                        }
                    }

                    // if ($esta==true){
                    //     $prodserv_extra[$ind]['cant']= $request->in[$cod];
                    // }else{
                    //     unset($prodserv_extra[$ind]);
                    // }

                }

                $Llegada = \App\Models\Vuelos::find(Encryptor::decrypt($request->vuelos[0]));
                $Salida = \App\Models\Vuelos::find(Encryptor::decrypt($request->vuelos[1]));

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = array("llegada" => $Llegada, "salida" => $Salida);
                return $result;


                // $result['status'] = 1;
                // $result['type'] = 'success';
                // $result['message'] = __('Processed Correctly');
                // $result['data'] = $prodserv_extra;

                // return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/');
        }
    }

    function get_servs_ext(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                if (isset($request->newPortal)) {
                    //dd($request->all());
                    $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_operacion . ' ' . $request->hora_operacion);
                    $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                    $data['hora_vuelo'] = $fecha_vuelo->format("H:i");


                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                    if ($request->aeropuerto_id == null) {
                        $Aeropuerto = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);
                        $data['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                    } else {
                        $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);
                        $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                    }



                    $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                    $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);

                    $data['cant_pasajeros'] = $request->pax;

                    if ($request->applyParking != null) {
                        $data['hora_llegada'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format("Y-m-d H:i");
                        $data['hora_salida'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format("Y-m-d H:i");
                    }
                    if ($request->tipo_id == 1) {
                        $data['exento_dosa'] = false;
                        $data['exento_tasa'] = true;
                    } else {
                        $data['exento_dosa'] = true;
                        $data['exento_tasa'] = false;
                    }
                    $prodserv_extra = $this->getProdservicios2('extra', $data);
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $prodserv_extra;

                    /*
                      $Validator = \Validator::make(
                      $request->all(), [
                      'type_document' => 'required',
                      'document' => 'required',
                      'name_user' => 'required',
                      'surname_user' => 'required',
                      ], [
                      'type_document.required' => __('El Tipo de Doc es Requerido'),
                      'document.required' => __('El Documento es Requerida'),
                      'name_user.required' => __('El nombre es Requerido'),
                      'surname_user.required' => __('El Apellido es Requerido'),
                      ]
                      );

                      if ($Validator->fails()) {
                      $result['status'] = 0;
                      $result['type'] = 'error';
                      $result['message'] = $Validator->errors()->first();
                      } else {
                     */
                    //dd($request->all());
                } else {
                    if ($request->hora_salida != null) {
                        $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida);
                    } else {
                        if ($request->hora_llegada != null) {
                            $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada);
                        } else {
                            $fecha_vuelo = Carbon::createFromFormat('d/m/Y H:i', $request->fecha_operacion . '00:00');
                        }
                    }


                    $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                    $data['hora_vuelo'] = $fecha_vuelo->format("H:i");

                    $data['tipo_vuelo_id'] = 2; //Vuelos General
                    $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                    $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);

                    $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);
                    $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                    $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);

                    $data['cant_pasajeros'] = $request->cant_pasajeros;

                    if ($request->applyParking != null) {
                        $data['hora_llegada'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format("Y-m-d H:i");
                        $data['hora_salida'] = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format("Y-m-d H:i");
                    }

                    $data['exento_dosa'] = !$request->cobrar_dosa;
                    $data['exento_tasa'] = !$request->cobrar_tasa;

                    $prodserv_extra = $this->getProdservicios2('extra', $data);

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    $result['data'] = $prodserv_extra;
                }
                return $result;
                //}
                //return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/');
        }
    }

    function get_prod_serv_ext(Request $request) {

        if ($request->origen_id != null) {
            $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));
            $data_extra = $request->all();
            $data_extra['tipo_vuelo_id'] = 2;
            $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));
        } else {
            if ($request->destino_id != null) {
                $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));
                $data_extra = $request->all();
                $data_extra['tipo_vuelo_id'] = 2;
                $data_extra['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));
            } else {
                $data_extra = array();
            }
        }



        $data = $this->getProdservicios("extra", $data_extra);
        if (count($data) > 0) {
            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Servicios Encontrados');
            $result['data'] = $data;
        } else {
            $result['status'] = 0;
            $result['type'] = 'error';
            $result['message'] = __('Servicios no Encontrados');
            $result['data'] = null;
        }
        return response()->json($result);
    }

    function get_prod_serv_ext2(Request $request) {
        $data_extra['tipo_vuelo_id'] = 2;
        $Aeropuerto = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);
        $data_extra = $request->all();
        $data_extra['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", date("H:i")));

        $data = $this->getProdservicios("extra", $data_extra);
        if (count($data) > 0) {
            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Servicios Encontrados');
            $result['data'] = $data;
        } else {
            $result['status'] = 0;
            $result['type'] = 'error';
            $result['message'] = __('Servicios no Encontrados');
            $result['data'] = null;
        }
        return response()->json($result);
    }

    function index() {
        if (\Request::ajax()) {
            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Prodservicios_active = Prodservicios::where("activo", 1)->get();

            $Prodservicios_disable = Prodservicios::where("activo", 0)->get();
            $Prodservicios_active = Datatables::of($Prodservicios_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('prodservicios.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('prodservicios.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                        }

                        if (in_array("permissions", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('prodservicios.permissions', $row->crypt_id) . '" class=" btn btn-warning btn-sm"><li class="fa fa-user-lock"></li> ' . __('Permissions') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $Prodservicios_disable = Datatables::of($Prodservicios_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('prodservicios.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Prodservicios_active->original['data'];
            $data2 = $Prodservicios_disable->original['data'];

            return view('prodservicios.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'codigo' => 'required',
                            'descripcion' => 'required',
                            'valor_dollar' => 'required',
                            'valor_euro' => 'required',
                            'valor_petro' => 'required',
                            'iva' => 'required',
                            'tipo_vuelo_id' => 'required',
                            'prodservicio_extra' => 'required',
                            'aplica_estacion' => 'required',
                            'aplica_categoria' => 'required',
                            'aplica_peso' => 'required',
                            'peso_inicio' => 'required',
                            'peso_fin' => 'required',
                            'default_tasa' => 'required',
                            'default_dosa' => 'required',
                            'default_carga' => 'required',
                            'formula' => 'required'
                                ], [
                            'codigo.required' => __('El Código es Requerido'),
                            'descripcion.required' => __('La Descripción es Requerida'),
                            'valor_dollar.required' => __('El Precio en Dolar es Requerido'),
                            'valor_euro.required' => __('El Precio en Euro es Requerido'),
                            'aplica_categoria.required' => __('Debe Indicar si Aplica para la Categoría'),
                            'valor_petro.required' => __('El Precio en Petro es Requerido'),
                            'iva.required' => __('El IVA es Requerido'),
                            'tipo_vuelo_id.required' => __('El Tipo de Vuelo es Requerido'),
                            'prodservicio_extra.required' => __(''),
                            'aplica_estacion.required' => __(''),
                            'aplica_peso.required' => __('La Opción Aplica al Peso es Requerido')
                                /*

                                  'peso_inicio.required' => __('El Codigo OACI es Requerido'),
                                  'peso_fin.required' => __('El Codigo IATA es Requerido'),
                                  'default_tasa.required' => __('El Nombre es Requerido'),
                                  'default_dosa.required' => __('La Categoria es Requerido'),

                                  'default_carga.required' => __('El Codigo OACI es Requerido'),
                                  'formula.required' => __('El Codigo IATA es Requerido')
                                 */
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Prodservicios = new Prodservicios();

                    $Prodservicios->codigo = $request->codigo;
                    $Prodservicios->nomenclatura = $request->nomenclatura;
                    $Prodservicios->descripcion = Upper($request->descripcion);
                    $Prodservicios->valor_dollar = saveFloat($request->valor_dollar);
                    $Prodservicios->valor_euro = saveFloat($request->valor_euro);

                    $Prodservicios->valor_petro = saveFloat($request->valor_petro);
                    $Prodservicios->iva = $request->iva;

                    $Prodservicios->tipo_vuelo_id = $request->tipo_vuelo_id;

                    $Prodservicios->nacionalidad_id = $request->nacionalidad_id;
                    $Prodservicios->turno_id = $request->turno_id;
                    $Prodservicios->prodservicio_extra = $request->prodservicio_extra;
                    $Prodservicios->aplica_categoria = $request->aplica_categoria;
                    $Prodservicios->aplica_estacion = $request->aplica_estacion;

                    $Prodservicios->aplica_peso = $request->aplica_peso;
                    $Prodservicios->peso_inicio = saveFloat($request->peso_inicio) * 1000;
                    $Prodservicios->peso_fin = saveFloat($request->peso_fin) * 1000;

                    $Prodservicios->default_tasa = $request->default_tasa;
                    $Prodservicios->default_dosa = $request->default_dosa;
                    $Prodservicios->default_carga = $request->default_carga;
                    $Prodservicios->formula = $request->formula;

                    $Prodservicios->user_id = Auth::user()->id;
                    $Prodservicios->ip = $this->getIp();
                    $Prodservicios->fecha_registro = now();

                    $Prodservicios->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $clasificaciones = \App\Models\Categorias::pluck("nombre", "id");
                return view('prodservicios.create', compact('clasificaciones'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $Validator = \Validator::make(
                                $request->all(), [
                            'codigo' => 'required',
                            'descripcion' => 'required',
                            'valor_dollar' => 'required',
                            'valor_euro' => 'required',
                            'valor_petro' => 'required',
                            'iva' => 'required',
                            'tipo_vuelo_id' => 'required',
                            'prodservicio_extra' => 'required',
                            'aplica_categoria' => 'required',
                            'aplica_estacion' => 'required',
                            'aplica_peso' => 'required',
                            'peso_inicio' => 'required',
                            'peso_fin' => 'required',
                            'default_tasa' => 'required',
                            'default_dosa' => 'required',
                            'default_carga' => 'required',
                            'formula' => 'required'
                                ], [
                            'codigo.required' => __('El Código es Requerido'),
                            'descripcion.required' => __('La Descripción es Requerida'),
                            'valor_dollar.required' => __('El Precio en Dolar es Requerido'),
                            'valor_euro.required' => __('El Precio en Euro es Requerido'),
                            'valor_petro.required' => __('El Precio en Petro es Requerido'),
                            'iva.required' => __('El IVA es Requerido'),
                            'aplica_categoria.required' => __('Debe Indicar si Aplica para la Categoría'),
                            'tipo_vuelo_id.required' => __('El Tipo de Vuelo es Requerido'),
                            'prodservicio_extra.required' => __(''),
                            'aplica_estacion.required' => __(''),
                            'aplica_peso.required' => __('La Opción Aplica al Peso es Requerido')
                                /*

                                  'peso_inicio.required' => __('El Codigo OACI es Requerido'),
                                  'peso_fin.required' => __('El Codigo IATA es Requerido'),
                                  'default_tasa.required' => __('El Nombre es Requerido'),
                                  'default_dosa.required' => __('La Categoria es Requerido'),

                                  'default_carga.required' => __('El Codigo OACI es Requerido'),
                                  'formula.required' => __('El Codigo IATA es Requerido')
                                 */
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Prodservicios = Prodservicios::find(Encryptor::decrypt($id));
                    $Prodservicios->codigo = $request->codigo;
                    $Prodservicios->nomenclatura = $request->nomenclatura;
                    $Prodservicios->descripcion = Upper($request->descripcion);
                    $Prodservicios->valor_dollar = saveFloat($request->valor_dollar);
                    $Prodservicios->valor_euro = saveFloat($request->valor_euro);

                    $Prodservicios->valor_petro = saveFloat($request->valor_petro);
                    $Prodservicios->iva = $request->iva;

                    $Prodservicios->tipo_vuelo_id = $request->tipo_vuelo_id;

                    $Prodservicios->nacionalidad_id = $request->nacionalidad_id;
                    $Prodservicios->turno_id = $request->turno_id;
                    $Prodservicios->prodservicio_extra = $request->prodservicio_extra;
                    $Prodservicios->aplica_categoria = $request->aplica_categoria;
                    $Prodservicios->aplica_estacion = $request->aplica_estacion;

                    $Prodservicios->aplica_peso = $request->aplica_peso;
                    $Prodservicios->peso_inicio = saveFloat($request->peso_inicio) * 1000;
                    ;

                    $Prodservicios->peso_fin = saveFloat($request->peso_fin) * 1000;
                    ;

                    $Prodservicios->default_tasa = $request->default_tasa;
                    $Prodservicios->default_dosa = $request->default_dosa;
                    $Prodservicios->default_carga = $request->default_carga;
                    $Prodservicios->formula = $request->formula;

                    $Prodservicios->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $prodservicio = Prodservicios::find(Encryptor::decrypt($id));

                return view('prodservicios.edit', compact('prodservicio'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Prodservicios = Prodservicios::find(Encryptor::decrypt($id));
                $Prodservicios->activo = 0;

                $Prodservicios->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $prodservicio = Prodservicios::find(Encryptor::decrypt($id));
                return view('prodservicios.disable', compact('prodservicio'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Prodservicios = Prodservicios::find(Encryptor::decrypt($id));
                $Prodservicios->activo = 1;

                $Prodservicios->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $prodservicio = Prodservicios::find(Encryptor::decrypt($id));
                return view('prodservicios.active', compact('prodservicio'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
