<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Helpers\Encryptor;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use App\Models\Vuelos;

class ReportesController extends Controller {

    function create_img(Request $request) {


        $img = $request->img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $request->name . '_' . Auth::user()->id . '.png';
        $success = file_put_contents($file, $data);
    }

    function vuelos() {

        
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
        $drawing->setName('Paid');
        $drawing->setDescription('Paid');
        $drawing->setPath(public_path('dist' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'banner.jpeg')); // put your path and image here
        $drawing->setCoordinates('A1');
        $drawing->setOffsetX(0);
        $drawing->setRotation(0);
        $drawing->getShadow()->setVisible(true);
        $drawing->getShadow()->setDirection(0);
        $drawing->setWorksheet($spreadsheet->getActiveSheet());

        $sheet->getRowDimension('1')->setRowHeight(70);
        $sheet->getRowDimension('8')->setRowHeight(100);
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->getStyle('A8:AK8')->getAlignment()->setVertical('center');
        $sheet->getStyle('A8:AK8')->getAlignment()->setWrapText(true);

        $sheet
                ->getStyle('A7:AK7')
                ->getBorders()
                ->getOutline()
                ->setBorderStyle(Border::BORDER_MEDIUM)
                ->setColor(new Color('000000'));

        $styleArray3 = ['borders' =>
            ['allBorders' =>
                ['borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,],
            ]
        ];
        $sheet->getStyle("A8:AK8")->applyFromArray($styleArray3);

        $styleArray = array('font' => array('bold' => true, 'color' => array('rgb' => '000000'), 'size' => 8, 'name' => 'Verdana'));
        $sheet->getStyle('A8:AK8')->applyFromArray($styleArray);

        $sheet
                ->getStyle('A7:D7')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('00b0f0');

        $sheet
                ->getStyle('A8:D8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('00b0f0');

        $sheet
                ->getStyle('E8:H8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('I8:J8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff0000');

        $sheet
                ->getStyle('K8:L8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff33cc');

        $sheet
                ->getStyle('M8:N8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('00b050');

        $sheet
                ->getStyle('O8:P8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('Q8:R8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff0000');

        $sheet
                ->getStyle('S8:U8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('V8:X8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ff0000');

        $sheet
                ->getStyle('Y8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('c5e0b4');

        $sheet
                ->getStyle('Z8:AK8')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ffd966');

        $sheet
                ->getStyle('Z7:AK7')
                ->getFill()
                ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('ffd966');

        // $spreadsheet->getActiveSheet()->getProtection()->setSheet(true); 
        $spreadsheet->getActiveSheet()->getStyle('A8:AK8')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        // $spreadsheet->getActiveSheet()->getProtection()->setPassword('123465');
        // $spreadsheet->getActiveSheet()->getProtection()->setSheet(true);
        // $spreadsheet->getActiveSheet()->getProtection()->setSort(false);
        // $spreadsheet->getActiveSheet()->getProtection()->setInsertRows(false);
        // $spreadsheet->getActiveSheet()->getProtection()->setFormatCells(false);
        $sheet->getColumnDimension('A')->setWidth(40);
        $sheet->getColumnDimension('B')->setWidth(40);
        $sheet->getColumnDimension('C')->setWidth(30);
        $sheet->getColumnDimension('D')->setWidth(15);

        $sheet->getStyle('Z8:AK8')->getAlignment()->setTextRotation(90); //rotar celdas

        $sheet->mergeCells('A7:D7')->setCellValue('A7', 'DATOS PROPIETARIO / RESPONSABLE DE LA AERONAVE');
        $sheet->mergeCells('Z7:AK7')->setCellValue('Z7', 'SERVICIOS AEROPORTUARIOS PRESTADOS');

        $sheet->setCellValue('A8', 'NOMBRE O RAZÓN SOCIAL');
        $sheet->setCellValue('B8', 'DIRECCIÓN FISCAL');
        $sheet->setCellValue('C8', 'N° DOCUMENTO(RIF / CÉDULA DE IDENTIDAD)');
        $sheet->setCellValue('D8', 'TELÉFONO');
        $sheet->setCellValue('E8', 'MATRICULA AERONAVE');
        $sheet->setCellValue('F8', 'TIPO AERONAVE');
        $sheet->setCellValue('G8', 'PESO MÁXIMO DE DESPEGUE(MTOW)');
        $sheet->setCellValue('H8', 'TIPO OPERACIÓN(NACIONAL O INTERNACIONAL)');
        $sheet->setCellValue('I8', 'RUTA LL');
        $sheet->setCellValue('J8', 'RUTA SA');
        $sheet->setCellValue('K8', 'OPS LL NAC');
        $sheet->setCellValue('L8', 'OPS SA NAC');
        $sheet->setCellValue('M8', 'OPS LL INT');
        $sheet->setCellValue('N8', 'OPS SA INT');
        $sheet->setCellValue('O8', 'FECHA LLEGADA');
        $sheet->setCellValue('P8', 'HORA DE LLEGADA(HLV)');
        $sheet->setCellValue('Q8', 'PASAJEROS DESEMBARCADOS LL NAC');
        $sheet->setCellValue('R8', 'PASAJEROS DESEMBARCADOS LL INT');
        $sheet->setCellValue('S8', 'FECHA SALIDA');
        $sheet->setCellValue('T8', 'HORA DE SALIDA(HLV)');
        $sheet->setCellValue('U8', 'PASAJEROS EMBARCADOS');
        $sheet->setCellValue('V8', 'PASAJEROS EMBARCADOS SA INT');
        $sheet->setCellValue('W8', 'PASAJEROS EMBARCADOS SA NAC');
        $sheet->setCellValue('X8', 'plataforma');
        $sheet->setCellValue('Y8', 'DISPOSICIÓN FINAL DE DESECHOS SÓLIDOS');
        $sheet->setCellValue('Z8', 'OBSERVACIONES');
        $sheet->setCellValue('AA8', 'DESINFECCIÓN DE AERONAVES');
        $sheet->setCellValue('AB8', 'GUIA DE AERONAVES(FOLLOW ME)');
        $sheet->setCellValue('AC8', 'TRASLADOS DE PASAJEROS Y EQUIPAJES DESDE Y HACIA AERONAVES ');
        $sheet->setCellValue('AD8', 'ASISTENCIA DE VUELO');
        $sheet->setCellValue('AE8', 'ELABORACIÓN DE PLAN DE VUELO');
        $sheet->setCellValue('AF8', 'SEÑALEROS, PLATAFORMA Y CALAS DE AERONAVES');
        $sheet->setCellValue('AG8', 'ASPIRADO DE AERONAVES ');
        $sheet->setCellValue('AH8', 'REMOLQUE DE AERONAVES ');
        $sheet->setCellValue('AI8', 'USO DEL SALON VIP');
        $sheet->setCellValue('AJ8', 'SERVICIO DE LIMPIEZA DE BAÑOS ');
        $sheet->setCellValue('AK8', ' SERVICIO DE UNIDAD DE POTENCIA ELECTRICA ');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        
        $Vuelos = Vuelos::orderBy("id", "desc")->take(10)->get();
        $data = [];
        foreach ($Vuelos as $value) {
            
            $data[] = [
                $value->piloto->nombres . ' ' . $value->piloto->apellidos,
                $value->piloto->nombres . ' ' . $value->piloto->apellidos,
                $value->piloto->documento2,
                $value->piloto->telefono,
                $value->aeronave->matricula,
                $value->aeronave->nombre,
                $value->aeronave->peso_maximo,
                $value->tipo_vuelo,
                $value->tipo_vuelo, //ruta ll
                $value->tipo_vuelo, //ruta sa
                $value->tipo_vuelo, //ops ll nac
                $value->tipo_vuelo, //ops sa nac
                $value->tipo_vuelo, //ops ll int
                $value->tipo_vuelo, //ops sa int
                $value->hora_llegada, //fecha llegada
                $value->hora_llegada2, //hora de llegada (HLV)
                $value->pasajeros_desembarcados, //PASAJEROS DESEMBARCADOS LL NAC
                $value->pasajeros_desembarcados, //PASAJEROS DESEMBARCADOS LL INT
                $value->fecha_operacion2, //FECHA SALIDA
                $value->hora_salida2, //HORA DE SALIDA (HLV)
                $value->pasajeros_desembarcados, //PASAJEROS EMBARCADOS
                $value->pasajeros_desembarcados, //PASAJEROS EMBARCADOS SA INT
                $value->pasajeros_desembarcados//PASAJEROS EMBARCADOS SA NAC
            ];
        }
        $sheet->fromArray($data, null, 'A9');
        
        
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename = "vuelos_registrados_.xlsx"');
        header('Cache-Control: max-age = 0');

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

}
