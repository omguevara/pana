<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Browser;
use DataTables;
use App;
use App\Helpers\Encryptor;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use PDF;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserController extends Controller {

    protected $ROLES_BY_USER = ["6" => ["7", "8"]];

    public function manual_pdf() {

        $destination = storage_path('manual' . DIRECTORY_SEPARATOR);
        // dd(Auth::user());
        $filename = "manual_" . Auth::user()->profile_id . ".pdf";
        //$filename = "manual1.pdf";
        $pathToFile = $destination . $filename;
        //dd($pathToFile);  
        if (file_exists($pathToFile)) {
            return response()->file($pathToFile);
        } else {
            return "Manual en Contrucción";
        }
    }

    public function change_password2($id, Request $request) {
        if (!(\Request::ajax())) {
            $id = Encryptor::decrypt($id);
            $msg = "";
            if (\Request::isMethod('post')) {
                $data = \Request::all();
    
                $request->validate([
                    'password' => 'required|min:8|same:password2',
                        ], [
                    "password.required" => "La Contraseña es Requerida",
                    "password.min" => "Mínimo 8 Caracteres",
                    "password.same" => "Las Contraseñas no son Iguales"
                ]);
    
                $Usuario = User::find($id);
                if ($Usuario != null) {
                    // Verificar si el usuario está activo
                    if ($Usuario->active == 0) {
                        // Activar al usuario
                        $Usuario->active = 1;
                        $Usuario->save();
                    }
    
                    $Usuario->chg_pwd = false;
                    $Usuario->password = \Hash::make($request->password);
                    ;
                    $Usuario->save();
                    \App\Models\FailedLogin::where('user_id', $Usuario->id)->whereDate('date_in', '=', date('Y-m-d'))->delete();
                    $msg = "";
    
                    //dd($emails);
                    $subject = "Contraseña Recuperada Correctamente";
                    
                    $datos = $Usuario->toArray();
                    $for = $Usuario->email;
                    //dd($datos);
                    Mail::send('email.pass_recovery_ok', $datos, function ($msj) use ($subject, $for) {
    
                        $msj->subject($subject);
                        $msj->to($for);
                        //$msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                    });
                    $msg = [];
                    $msg['status']='1';
                    $msg['type']='info';
                    $msg['message']='Contraseña Actualizada Correctamente';
                    return view('users.login', compact('msg'));
                    
                }
            } else {
    
                $Usuario = User::find($id);
                if ($Usuario->chg_pwd == false) {
                    return Redirect::to('/');
                }
            }
    
            return view('users.recovery_password', compact('Usuario'));
        } else {
            return Redirect::to('/');
        }
    }
    

    public function forgot_password() {
        if (!(\Request::ajax())) {
            $msg = "";
            if (\Request::isMethod('post')) {
                $msg = "Se le Ha Enviado un Correo con Instrucciones Para Recuperar su Contraseña";
                $data = \Request::all();
                $Usuario = User::where(DB::raw('LOWER(email)'), Lower($data['email']))->first();
                if ($Usuario != null) {


                    $Usuario->chg_pwd = true;
                    $Usuario->save();

                    //dd($emails);
                    $subject = "Recuperación Contraseña";
                    $for = $data['email'];
                    $datos = $Usuario->toArray();

                    //dd($datos);
                    Mail::send('email.pass_recovery', $datos, function ($msj) use ($subject, $for) {

                        $msj->subject($subject);
                        $msj->to($for);
                        //$msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                    });
                }
            }

            //return view('users.forgot_password', compact('msg'));
            return view('users.login', compact('msg'));
        } else {
            return Redirect::to('/');
        }
    }

    public function manual() {
        if (\Request::ajax()) {
            return view('users.manual');
        } else {
            return Redirect::to('/');
        }
    }

    public function list_backups(Request $request) {

        $ruta = storage_path() . DIRECTORY_SEPARATOR . "backups";
        $noFile = ['.', '..'];
        $directorio = opendir($ruta);

        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                //$ruta = "/home/pananetv/backups/";


                $files = [];
                while ($archivo = readdir($directorio)) { //obtenemos un archivo y luego otro sucesivamente
                    if (!in_array($archivo, $noFile)) {
                        $parts = explode("_", $archivo);
                        $parts2 = explode(".", $parts[2]);

                        $fecha = explode("-", $parts[1]);
                        $hora = explode(":", $parts2[0]);

                        $files[] = ["nombre" => "DB_" . $fecha[0] . "_" . $fecha[1] . "_" . $fecha[2] . " " . $hora[0] . ":" . $hora[1] . ":" . $hora[2]];
                    }
                }
                closedir($directorio);
                /*
                 * 
                  asort($files);
                  $backups = [];
                  foreach ($files as $value) {
                  $backups = $value;
                  }
                  dd($backups);
                 */
                return view('users.backups', compact('files'));
            } else {
                exit;
            }
        } else {
            if ($request->isMethod('post')) {
                $f = explode(" ", $request->file);
                $fecha = explode("_", $f[0]);
                $hora = explode(":", $f[1]);

                $file = "pana_" . $fecha[1] . "-" . $fecha[2] . '-' . $fecha[3] . '_' . $hora[0] . ':' . $hora[1] . ':' . $hora[2] . '.sql.gz';

                $d = "BK_" . $file;

                return response()->download($ruta . DIRECTORY_SEPARATOR . $file, $d);
            } else {
                return Redirect::to('/');
            }
        }
    }

    public function select() {
        return view('users.select');
    }

    public function reporte() {
        $data = User::where("active", true)->orderBy("document")->get();
        $title = "Listado General de Usuarios";

        //dd($data);  //estacion  impresora
        return PDF::loadView("reportes.generales.usuarios", compact('data', 'title'))->stream("usuarios.pdf");
    }

    function index() {

        if (\Request::ajax()) {
            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            if (Auth::user()->id == 1) {
                $data_active = User::where("active", 1)->get();
            } else {
                if (Auth::user()->aeropuerto_id == null) {
                    $data_active = User::where("active", 1)->where("id", "!=", "1")->get();
                } else {
                    $data_active = User::where("active", 1)->where("aeropuerto_id", "=", Auth::user()->aeropuerto_id)->get();
                }
            }

            $data_active = Datatables::of($data_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('users.edit', $row->crypt_id) . '" class="actions_users btn btn-success btn-xs mr-1" title="Editar"> <span class="fa fa-edit"></span> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('users.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-xs mr-1" title="Desactivar"><span class="fa fa-trash"></span> ';
                        }
                        if (in_array("change_password", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('users.change_password', $row->crypt_id) . '" class="actions_users btn btn-warning btn-xs" title="Cambiar Clave"><span class="fa fa-lock"></span> ';
                        }


                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            if (Auth::user()->id == 1) {
                $data_disable = User::where("active", 0)->get();
            } else {
                if (Auth::user()->aeropuerto_id == null) {
                    $data_disable = User::where("active", 0)->where("id", "!=", "1")->get();
                } else {
                    $data_disable = User::where("active", 0)->where("aeropuerto_id", "=", Auth::user()->aeropuerto_id)->get();
                }
            }
            //$data_disable = User::where("active", 0)->get();

            $data_disable = Datatables::of($data_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('users.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }


                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            $data1 = $data_active->original['data'];
            $data2 = $data_disable->original['data'];
            //dd($data1);
            return view('users.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Validator = \Validator::make(
                                $request->all(), [
                            'document' => 'required|unique:seg_users,document',
                            'email' => 'required|unique:seg_users,email',
                            'name_user' => 'required',
                            'password' => 'required',
                            'phone' => 'required',
                            'profile_id' => 'required'
                                ], [
                            'document.required' => __('El Documento es Requerido'),
                            'email.required' => __('El Email es Requerido'),
                            'name_user.required' => __('El Nombre Requerido'),
                            'password.required' => __('La Clave es Requerido'),
                            'phone.required' => __('El Telefono Requerido'),
                            'profile_id.required' => __('El Perfil es Requerido'),
                            'document.unique' => __('El Documento ya esta Registrado'),
                            'email.unique' => __('El Correo ya esta Registrado')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $pwd = Str::random(10);

                    $data = new User();
                    $data->document = $request->type_document . $request->document;
                    $data->name_user = $request->name_user;
                    $data->surname_user = $request->surname_user;

                    $data->username = strtolower($request->type_document . $request->document);
                    $data->password = \Hash::make($pwd);
                    $data->phone = $request->phone;
                    $data->email = $request->email;

                    $data->profile_id = Encryptor::decrypt($request->profile_id);
                    $data->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);

                    $data->change_password = true;

                    $data->user_id = Auth::user()->id;
                    $data->ip = $this->getIp();
                    $data->register_date = now();

                    $data->save();

                    \App\Models\UsuariosAeropuertos::where("usuario_id", $data->id)->delete();
                    if (isset($request->aeropuerto_extra_id)) {
                        foreach ($request->aeropuerto_extra_id as $value) {
                            $extra = new \App\Models\UsuariosAeropuertos();
                            $extra->usuario_id = $data->id;
                            $extra->aeropuerto_id = Encryptor::decrypt($value);
                            $extra->save();
                            ;
                        }
                    }




                    //dd($emails);
                    $subject = "Registro de Usuario";
                    $for = $request->email;
                    $datos = $data->toArray();
                    $datos['pwd'] = $pwd;
                    //dd($datos);
                    Mail::send('email.create_user', $datos, function ($msj) use ($subject, $for, $pwd) {

                        $msj->subject($subject);
                        $msj->to($for);
                        //$msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                    });

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                if (Auth::user()->aeropuerto_id == null) {

                    $aeropuertos = \App\Models\Aeropuertos::where("activo", 1)
                            ->where("pertenece_baer", 1)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    $profiles = \App\Models\Profile::where("status", 1)
                    ->where("id", "!=", 1)
                    ->orderBy('name_profile')
                    ->get()
                    ->pluck("name_profile", "crypt_id");
                } else {
                    if (is_array($this->ROLES_BY_USER[Auth::user()->profile_id])) {

                        $profiles = \App\Models\Profile::where("status", DB::raw("true"))
                                ->whereIn("id", $this->ROLES_BY_USER[Auth::user()->profile_id])
                                ->orderBy('name_profile')
                                ->pluck("name_profile", "id");

                        $aeropuertos = \App\Models\Aeropuertos::where("activo", DB::raw("true"))
                                ->where("pertenece_baer", DB::raw("true"))
                                ->where("id", Auth::user()->aeropuerto_id)
                                ->pluck("nombre", "id");
                    } else {

                        $msg = "El Usuario no Posee un Grupo de Perfiles Asociados";
                        return view('error.generic', compact('msg'));
                    }
                }
                //$aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->where("pertenece_baer", 1)->pluck("nombre", "id");
                return view('users.create', compact('profiles', 'aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $data = User::find(Encryptor::decrypt($id));
                $data->document = strtolower($request->type_document . $request->document);
                $data->name_user = $request->name_user;
                $data->surname_user = $request->surname_user;

                $data->phone = $request->phone;
                $data->email = $request->email;

                $data->profile_id = Encryptor::decrypt($request->profile_id);
                $data->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);

                $data->user_id = Auth::user()->id;
                $data->ip = $this->getIp();
                $data->register_date = now();

                $data->save();

                \App\Models\UsuariosAeropuertos::where("usuario_id", $data->id)->delete();
                if (isset($request->aeropuerto_extra_id)) {
                    foreach ($request->aeropuerto_extra_id as $value) {
                        $extra = new \App\Models\UsuariosAeropuertos();
                        $extra->usuario_id = $data->id;
                        $extra->aeropuerto_id = Encryptor::decrypt($value);
                        $extra->save();
                        ;
                    }
                }


                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {
                if (Auth::user()->aeropuerto_id == null) {

                    $aeropuertos = \App\Models\Aeropuertos::where("activo", 1)
                            ->where("pertenece_baer", 1)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    $profiles = \App\Models\Profile::where("status", 1)
                            ->where("id", "!=", 1)
                            ->orderBy('name_profile')
                            ->get()
                            ->pluck("name_profile", "crypt_id");
                } else {
                    if (is_array($this->ROLES_BY_USER[Auth::user()->profile_id])) {

                        $profiles = \App\Models\Profile::where("status", DB::raw("true"))
                                ->whereIn("id", $this->ROLES_BY_USER[Auth::user()->profile_id])
                                ->orderBy('name_profile')
                                ->pluck("name_profile", "crypt_id");

                        $aeropuertos = \App\Models\Aeropuertos::where("activo", DB::raw("true"))
                                ->where("pertenece_baer", DB::raw("true"))
                                ->where("id", Auth::user()->aeropuerto_id)
                                ->get()
                                ->pluck("full_nombre", "crypt_id");
                    } else {

                        $msg = "El Usuario no Posee un Grupo de Perfiles Asociados";
                        return view('error.generic', compact('msg'));
                    }
                }


                // $profiles = \App\Models\Profile::where("status", 1)->where("id", "!=", 1)->pluck("name_profile", "id");
                // $aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->where("pertenece_baer", 1)->pluck("nombre", "id");
                $data = User::find(Encryptor::decrypt($id));

                return view('users.edit', compact('data', 'profiles', 'aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $data = User::find(Encryptor::decrypt($id));
                $data->active = 1;

                $data->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {
                $profiles = \App\Models\Profile::where("status", 1)->pluck("name_profile", "id");
                $data = User::find(Encryptor::decrypt($id));

                return view('users.active', compact('data', 'profiles'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $data = User::find(Encryptor::decrypt($id));
                $data->active = 0;

                $data->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {
                $profiles = \App\Models\Profile::where("status", 1)->pluck("name_profile", "id");
                $data = User::find(Encryptor::decrypt($id));

                return view('users.disable', compact('data', 'profiles'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function change_pwd(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'before_passwd' => 'required',
                            'passwd' => 'required',
                            're_passwd' => 'required|same:passwd',
                                ], [
                            'before_passwd.required' => __('La Contraseña Actual es Requerida'),
                            'passwd.required' => __('La Nueva Contraseña es Requerida'),
                            're_passwd.required' => __('La Confirmación de la Contraseña es Requerida'),
                            're_passwd.same' => __('Las Contraseñas no Concuerdan'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $data = User::find(Auth::user()->id);
                    // dd($request->passwd);

                    if (\Hash::check($request->before_passwd, $data->password)) {
                        $data->password = \Hash::make($request->passwd);
                        $data->change_password = false;

                        $data->save();

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Actualizada Correctamente');
                    } else {

                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('La Contraseña Actual No Concuerda');
                    }
                }
                return $result;
            } else {
                $data = User::find(Auth::user()->id);
                $profiles = \App\Models\Profile::where("status", 1)->pluck("name_profile", "id");
                return view('users.change_pwd', compact('data', 'profiles'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function change_password($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $data = User::find(Encryptor::decrypt($id));
                // dd($request->passwd);
                $data->password = \Hash::make($request->passwd);

                $data->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                //dd($emails);
                $subject = "Registro de Usuario";
                $for = $data->email;
                $datos = $data->toArray();
                $datos['pwd'] = $request->passwd;
                //dd($datos);
                Mail::send('email.create_user', $datos, function ($msj) use ($subject, $for) {

                    $msj->subject($subject);
                    $msj->to($for);
                    //$msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                });

                return $result;
            } else {
                $data = User::find(Encryptor::decrypt($id));
                $profiles = \App\Models\Profile::where("status", 1)->pluck("name_profile", "id");
                return view('users.passwd', compact('data', 'profiles'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    function set_theme($theme) {
        $User = User::find(Auth::user()->id);
        $User->theme = $theme;
        $User->save();
    }

    /*
     * Logout
     */

    public function logout() {
        if (Auth::check() == true) {
            \App\Models\LoggedUser::where('user_id', Auth::user()->id)->delete();

            $AccessHistory = \App\Models\AccessHistory::where('user_id', Auth::user()->id)->whereNull('date_out');
            if ($AccessHistory != null) {
                $AccessHistory->update(['date_out' => date('Y-m-d H:i:s')]);
            }
            //$this->endLogin(Auth::user()->id);
        } else {
            $LoggedUser = \App\Models\LoggedUser::where('platform', Browser::platformName())->where('browser', Browser::browserName())
                    ->where('browser_engine', Browser::browserEngine())->where('ip', \Request::ip())
                    ->first();
            if ($LoggedUser != null) {
                $AccessHistory = \App\Models\AccessHistory::where('user_id', $LoggedUser->user_id)->whereNull('date_out');
                if ($AccessHistory != null) {
                    $AccessHistory->update(['date_out' => date('Y-m-d H:i:s')]);
                }
                $LoggedUser->delete();
            }
        }
        session()->flush();
        Auth::logout();

        return redirect()->route('login');
    }

    /*
     * Login
     */

    function login(Request $request) {

        //dd(route('facturas'));
        //dd(csrf_token());
        //dd(Request::session()->get('key'));
        //dd($request->all());
        //dd(FailedLogin::where('user_id', 1)->whereDate('date_in', '=', date('Y-m-d'))->count());
        //dd(Auth::check());
        //dd($this->searchREP('V', 22));
        //dd(Auth::check());
        // $user = User::find(1);
        // $user->password = Hash::make('password');
        // $user->save();

        if (Auth::check() == false) {
            if ($request->isMethod('post')) {
                //dd("O,o");
                $Validator = \Validator::make(
                                $request->all(), [
                            'username' => 'required',
                            'password' => 'required',
                            
                            //'g-recaptcha-response' => 'required|recaptchav3:register,0.5'
                                ], [
                            'username.required' => __('Username is Required'),
                            'password.required' => __('Password is Required'),
                            
                            'g-recaptcha-response.recaptchav3' => __('El Captcha  no Coincide'),
                                ]
                );
                //dd($Validator->passes());
                //dd($request->captcha);
                if ($Validator->fails()) {
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $User = User::where('username', strtolower($request->username))->first();

                    if ($User == null) { // if user exists
                        $result['type'] = 'error';
                        $result['message'] = __('Wrong Data');
                    } else {

                        /*                         * ************************************** */


                        $is_logged = \App\Models\LoggedUser::where('user_id', $User->id)->first();

                        $continue = true;
                        if ($is_logged != null) {//if exists login
                            $now = Carbon::now();
                            $to = Carbon::createFromFormat('Y-m-d H:i:s', $is_logged->date_in);
                            $diff = $now->diffInMinutes($to);

                            if ($diff > 3) {
                                $AccessHistory = \App\Models\AccessHistory::where('user_id', $is_logged->user_id)->whereNull('date_out');
                                if ($AccessHistory != null) {
                                    $AccessHistory->update(['date_out' => date('Y-m-d H:i:s')]);
                                }
                                $is_logged->delete();
                                $continue = true;
                            } else {
                                $continue = false;
                            }
                        }
                        if ($continue == true) {
                            if ($User->active == 1) {// if user is blocked
                                $userdata = array(
                                    'username' => strtolower($request->username),
                                    'password' => $request->password
                                );
                                if ($User->getProfile->status == true) {
                                    if (Auth::attempt($userdata)) {

                                        $User->chg_pwd = false;
                                        $User->save();

                                        $LoggedUser = new \App\Models\LoggedUser();
                                        $LoggedUser->user_id = $User->id;
                                        $LoggedUser->date_in = now();
                                        $LoggedUser->platform = Browser::platformName();
                                        $LoggedUser->browser = Browser::browserName();
                                        $LoggedUser->browser_engine = Browser::browserEngine();
                                        $LoggedUser->ip = $this->getIp();
                                        $LoggedUser->save();

                                        $AccessHistory = new \App\Models\AccessHistory();
                                        $AccessHistory->user_id = $User->id;
                                        $AccessHistory->date_in = now();
                                        $AccessHistory->ip = $this->getIp();
                                        $AccessHistory->save();

                                        \App\Models\FailedLogin::where('user_id', $User->id)->whereDate('date_in', '=', date('Y-m-d'))->delete();

                                        return Redirect::to('/dashboard');
                                    } else {
                                        $FailedLogin = new \App\Models\FailedLogin();
                                        $FailedLogin->user_id = $User->id;
                                        $FailedLogin->date_in = now();
                                        $FailedLogin->ip = $this->getIp();
                                        $FailedLogin->save();

                                        if (\App\Models\FailedLogin::where('user_id', $User->id)->whereDate('date_in', '=', date('Y-m-d'))->count() >= 3) {
                                            //$User->update(['active' => 0]);
                                            $User->active = 0;
                                            $User->save();
                                            $result['type'] = 'error';
                                            $result['message'] = __('User Lock');
                                        } else {
                                            $result['type'] = 'error';
                                            $result['message'] = __('Wrong Data');
                                        }
                                    }
                                } else {
                                    $result['type'] = 'error';
                                    $result['message'] = __('Inactive Profile');
                                }
                            } else {
                                $result['type'] = 'error';
                                $result['message'] = __('User blocked');
                            }
                        } else {
                            $result['type'] = 'error';
                            $result['message'] = __('has an active session');
                        }
                    }
                }

                return Redirect::to('login')->with('msg', $result);
            } else {
                return view('users.login');
            }
        } else {

            return redirect()->route('dashboard');
        }
    }

    public function dashboard() {
        //App::setLocale('en');
        //App::setSessionlifetime(5);

        $Menu = User::find(Auth::user()->id)->getMenu();
        $lifetime = config("session.lifetime");
        //$Estatus = \App\Models\Esta
        //dd($Taq->getPerifericos);
        //dd(Auth::user());
        //dd($Menu);
        return view('users.home', compact('Menu', 'lifetime'));
    }

    function regenerate_session() {
        session()->regenerate();
    }

    function logged_users() {
        if (\Request::ajax()) {
            $LoggedUser = \App\Models\LoggedUser::orderBy("id", "desc")->get();

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd($route_id);
            $data1 = Datatables::of($LoggedUser)
                    ->addIndexColumn()
                    ->addColumn('user', function ($row) use ($route_id) {
                        return \App\Models\User::find($row->user_id)->name_user . ' ' . \App\Models\User::find($row->user_id)->surname_user;
                    })
                    ->addColumn('fecha', function ($row) {
                        return showDate($row->date_in, 'full'); //$this->showDate($row->date_in, "full");
                    })
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("kill", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('logged_users.kill', $row->crypt_id) . '" class=" btn btn-primary btn-sm"> <li class="fa fa-times"></li> ' . __('Kill') . '</a> ';
                        }







                        return $actionBtn;
                    })
                    ->rawColumns(['user', 'fecha', 'action'])
                    ->make(true)
            ;
            //dd($data1->original['data']);
            $data1 = $data1->original['data'];
            return view('users.logged_users', compact('data1'));
        } else {
            return Redirect::to('/');
        }
    }

    function kill($id) {
        $id = Encryptor::decrypt($id);
        $logged = \App\Models\LoggedUser::find($id)->delete();
        return redirect()->route('logged_users');
    }

    function restricted_access() {
        if (\Request::ajax()) {
            $RestrictedAccess = \App\Models\RestrictedAccess::orderBy("id", "desc")->get();

            $data1 = Datatables::of($RestrictedAccess)
                    ->addIndexColumn()
                    ->addColumn('user', function ($row) {
                        return \App\Models\User::find($row->user_id)->name_user . ' ' . \App\Models\User::find($row->user_id)->surname_user;
                    })
                    ->addColumn('modulo', function ($row) {
                        return \App\Models\Process::find($row->process_id)->name_process;
                    })
                    ->addColumn('fecha', function ($row) {
                        return showDate($row->date_in, 'full'); //$this->showDate($row->date_in, "full");
                    })
                    ->rawColumns(['user', 'modulo', 'fecha'])
                    ->make(true)
            ;
            $data1 = $data1->original['data'];
            return view('users.restricted_access', compact('data1'));
        } else {
            return Redirect::to('/');
        }
    }

    function access_history() {
        if (\Request::ajax()) {
            $AccessHistory = \App\Models\AccessHistory::orderBy("id", "desc")->get();

            $data1 = Datatables::of($AccessHistory)
                    ->addIndexColumn()
                    ->addColumn('user', function ($row) {
                        return \App\Models\User::find($row->user_id)->name_user . ' ' . \App\Models\User::find($row->user_id)->surname_user;
                    })
                    ->addColumn('fecha1', function ($row) {
                        return showDate($row->date_in, 'full'); //$this->showDate($row->date_in, "full");
                    })
                    ->addColumn('fecha2', function ($row) {
                        return showDate(($row->date_out == null ? now() : $row->date_out), 'full'); //$this->showDate($row->date_in, "full");
                    })
                    ->rawColumns(['user', 'fecha1', 'fecha2'])
                    ->make(true)
            ;
            $data1 = $data1->original['data'];
            return view('users.access_history', compact('data1'));
        } else {
            return Redirect::to('/');
        }
    }

}
