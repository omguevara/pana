<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Exports\ExportFacturas;
use App\Exports\VuelosExport;
use App\Exports\VuelosExportBaer;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Vuelos;

class VuelosController extends Controller {

    public function revision($id = null) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {

                if ($id == null) {
                    $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("activo", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    return view('vuelos.revision', compact('Aeronaves', 'Aeropuertos'));
                } else {
                    exit;
                }
            } else {
                if (\Request::isMethod('put')) {
                    $where [] = [function ($q) {
                            $q->whereNull('ok');
                        }];
                    $input = \Request::all()['search'];
                    if (\Request::all()['aeropuerto'] != null) {
                        $params = Encryptor::decrypt(\Request::all()['aeropuerto']);
                        $where [] = ["aeropuerto_id", $params];
                    }
                    if (\Request::all()['nave'] != null) {
                        $params = Encryptor::decrypt(\Request::all()['nave']);
                        $where [] = ["aeronave_id", $params];
                    }
                    if (\Request::all()['fechas'] != null) {
                        $params = \Request::all()['fechas'];
                        $where [] = [function ($q) use ($params) {
                                $fechas = explode(" - ", $params);
                                $f1[0] = $this->saveDate($fechas[0]);
                                $f1[1] = $this->saveDate($fechas[1]);
                                $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                            }];
                    }
                    $datos = Vuelos::where($where)
                            ->limit(100)
                            ->orderBy("id", "desc")
                            ->get();
                    $data = Datatables::of($datos)
                            ->addIndexColumn()
                            ->filter(function ($instance) use ($input) {
                                if ($input != null) {
                                    $instance->collection = $instance->collection->filter(function ($row) use ($input) {
                                        if (Str::contains(Str::lower($row['aeronave']['full_nombre_tn']), Str::lower($input))) {
                                            return true;
                                        } else {
                                            if (Str::contains(Str::lower($row['piloto']['full_name']), Str::lower($input))) {
                                                return true;
                                            } else {
                                                if (Str::contains(Str::lower($row['aeropuerto']['full_nombre']), Str::lower($input))) {
                                                    return true;
                                                } else {
                                                    if (Str::contains(Str::lower($row['fecha_operacion2']), Str::lower($input))) {
                                                        return true;
                                                    } else {
                                                        
                                                    }
                                                }
                                            }
                                        }
                                        return false;
                                    });
                                }
                            })
                            ->addColumn('action', function ($row) {
                                $actionBtn = "";
                                $actionBtn .= '<a data-id="' . $row->crypt_id . '" onClick="getDataFly(this, event)" href="' . route('revision', $row->crypt_id) . '" title="Ver Registro" class=" btn btn-success btn-sm"> <li class="fa fa-eye"></li> </a> ';
                                return $actionBtn;
                            })
                            ->rawColumns(['action'])
                    //->make(true)

                    ;
                    return $data->toJson();

                    //dd($datos);
                } else {

                    $Vuelo = Vuelos::find(Encryptor::decrypt(\Request::all()['id']));
                    $Vuelo->ok = \Request::all()['op'];
                    $Vuelo->obs_rev = \Request::all()['msg'];
                    $Vuelo->save();
                }
            }
        } else {
            if (\Request::isMethod('get')) {
                $Vuelo = Vuelos::find(Encryptor::decrypt($id));
                return view('vuelos.vuelo_pdf_price', compact('Vuelo'));
            }
            return Redirect::to('/dashboard');
        }
    }

    public function flights() {

        if (\Request::ajax()) {
            //dd(Auth::user()->lista_aeropuertos);
            $Vuelos = Vuelos::where("activo", 1)
                    ->whereDate("fecha_operacion", date('Y-m-d'))
                    ->where("tipo_operacion_id", 2)
                    ->whereIn("orig_dest_id", Auth::user()->lista_aeropuertos)
                    ->whereNotIn("aeropuerto_id", Auth::user()->lista_aeropuertos)
                    ->where("informado", false)
                    ->get()
            ;

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = "";
            $result['data'] = $Vuelos->toArray();
            return $result;
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function get($id = 123456) {
        // dd("sss");
        //return Vuelos::where("piloto_id", 40)->get()->toArray();
        $Vuelo = Vuelos::find(40)->toArray();
        unset($Vuelo['aeropuerto']['categoria']);
        unset($Vuelo['aeropuerto']['Ubicaciones']);
        unset($Vuelo['aeropuerto']['feriados']);
        unset($Vuelo['aeropuerto']['get_categoria']);
        unset($Vuelo['aeropuerto']['get_ubicaciones']);
        unset($Vuelo['aeropuerto']['get_feriados']);
        unset($Vuelo['usuario_create']);
        unset($Vuelo['usuario_edit']);
        unset($Vuelo['get_aeronave']);
        unset($Vuelo['get_piloto']);
        unset($Vuelo['get_aeropuerto']);
        unset($Vuelo['get_usuario_create']);
        unset($Vuelo['get_usuario_edit']);

        $Vuelo['detalle'] = [];
        foreach ($Vuelo['get_detalle'] as $key => $value) {
            unset($Vuelo['get_detalle'][$key]);
            $Vuelo['detalle'][$key]['full_descripcion'] = $value['full_descripcion'];
        }


        return $Vuelo;
    }

    public function see_fly($id) {
        $Vuelo = Vuelos::find(Encryptor::decrypt($id));

        return view('vuelos.vuelo_pdf', compact('Vuelo'))
        ;

        /*
         * $data =$Vuelo->toArray();
          return PDF::loadView('vuelo_pdf', $data)
          ->stream('archivo.pdf');
         */
    }

    public function flys(Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                // $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                // ->where("pertenece_baer", true)
                // ->where("activo", true)
                // ->whereIn("id", Auth::user()->lista_aeropuertos)
                // ->get()
                // ->pluck("full_nombre", "crypt_id");
                
                
                // $ListAeropuertos = \App\Models\Aeropuertos::where("activo", "1")
                //         ->where("pertenece_baer", 1)
                //         ->orderBy('id', "asc")
                //         ->get()
                //         ->pluck('nombre', 'codigo_oaci')
                // ;
                // $ListColors1 = [];
                // $ListColors2 = [];
                // foreach ($ListAeropuertos as $value) {
                //     $ListColors1[] = 'blue';
                //     $ListColors2[] = 'green';
                // }
                // dd($request);
                $Vuelos = \App\Models\Aeropuertos::leftJoin("proc_vuelos", function ($join) {
                            $join->where("proc_vuelos.aeropuerto_id",  DB::raw("maest_aeropuertos.id"));
                            $join->where("proc_vuelos.activo", '=', DB::raw('true'));
                            $join->whereBetween("proc_vuelos.fecha_operacion", [\Carbon\Carbon::now()->firstOfMonth()->format('Y/m/d'), \Carbon\Carbon::now()->endOfMonth()->format('Y/m/d')]);
                        })
                        ->where("maest_aeropuertos.activo", "1")
                        ->where("maest_aeropuertos.pertenece_baer", 1)
                        ->selectRaw("maest_aeropuertos.id, maest_aeropuertos.nombre, maest_aeropuertos.codigo_oaci, count(proc_vuelos.id) as cant_vuelos, SUM(COALESCE(proc_vuelos.pasajeros_embarcados, 0)) as cant_pax")
                        ->groupBy("maest_aeropuertos.id", "maest_aeropuertos.nombre", "maest_aeropuertos.codigo_oaci")
                        ->orderBy("cant_vuelos", "desc")
                        ->get()
                        //->toSql()
                        //->pluck("cant")
                        ;
                        $date = \Carbon\Carbon::now()->firstOfMonth()->format('d/m/Y') . ' - ' . \Carbon\Carbon::now()->endOfMonth()->format('d/m/Y');
                        $color_blue = '#0000ff';
                        $color_green = '#008000';
                        // $step = 0x001111;
                        $cant_vuelos_color = [];
                        $aeropuertos = [];
                        $cant_vuelos = [];
                        $total_vuelos = 0;

                        $cant_pax_color = [];
                        $cant_pax = [];
                        $total_pax = 0;

                        foreach ($Vuelos as $key => $value) {
                            // $color_blue_hex = str_replace('#', '', $color_blue);
                            // $color_blue_hex_new = dechex(hexdec($color_blue_hex) - $key * $step);
                            // $color_blue_hex_new_padded = str_pad($color_blue_hex_new, 6, '0', STR_PAD_LEFT);
                            // $cant_vuelos_color[] = '#' . $color_blue_hex_new_padded;

                            $cant_vuelos_color[] = $color_blue;
                            $aeropuertos[] = $value->codigo_oaci;
                            $cant_vuelos[] = $value->cant_vuelos;
                            $total_vuelos += $value->cant_vuelos;

                            $cant_pax_color[] = $color_green;
                            $cant_pax[] = $value->cant_pax;
                            $total_pax += $value->cant_pax;
                        }


                        //dd($cant_vuelos_color);
                //->toSql()
                //return $Vuelos;
               
                return view('reportes.vuelos.flys', compact( 'Vuelos',  'cant_vuelos_color', 'aeropuertos', 'cant_vuelos', 'cant_pax_color', 'cant_pax', 'total_vuelos', 'total_pax', 'date' ));
            } else {
                if (\Request::isMethod('put')) {
                    $ListAeropuertos = \App\Models\Aeropuertos::where("activo", "1")
                            ->where("pertenece_baer", 1)
                            ->orderBy('id', "asc")
                            ->get()
                            ->pluck('codigo_oaci')
                    ;
                    $ListColors = [];
                    foreach ($ListAeropuertos as $value) {
                        $ListColors[] = 'blue';
                    }

                    $Vuelos = \App\Models\Aeropuertos::leftJoin("proc_vuelos", function ($join) {
                                $join->on("proc_vuelos.aeropuerto_id", "=", "maest_aeropuertos.id");
                                $join->on("proc_vuelos.activo", '=', DB::raw('true'));
                            })
                            ->where("maest_aeropuertos.activo", "1")
                            ->where("maest_aeropuertos.pertenece_baer", 1)
                            ->selectRaw("maest_aeropuertos.id, count(proc_vuelos.id) as cant")
                            ->groupBy("maest_aeropuertos.id")
                            ->orderBy("maest_aeropuertos.id", "asc")
                            ->get()
                            //->toSql()
                            ->pluck("cant")
                    ;

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "";
                    $result['data'] = array('aeropuertos' => $ListAeropuertos, 'color' => $ListColors, 'vuelos' => $Vuelos);
                    return $result;
                }
                if (\Request::isMethod('post')) {

                    //$where[] = [DB::raw("1"), '1'];
                    $duracion = $request->duracion;
                    $duracion = explode(' - ', $duracion);
                    $where= [];
                    $where= [];
                    $duracion[0] = saveDate($duracion[0]);
                    $duracion[1] = saveDate($duracion[1]);
                    
                    $where[] = [function ($query) use($duracion) {
                        $query->whereBetween("proc_vuelos.fecha_operacion", [$duracion[0], $duracion[1]]);
                        
                    }];
                    
                    //if ($request->has('tipo_aviacion')){
                    if ($request->tipo_aviacion!= null){
                        $where[] = [function ($query) use ($request) {
                            $query->where('proc_vuelos.tipo_aviacion_id', $request->tipo_aviacion);
                                    
                        }];
                    }
                    
                    //if ($request->has('tipo_vuelo')){
                    if ($request->tipo_vuelo!=null){
                        $where[] = [function ($query) use ($request) {
                            
                            $query->where('proc_vuelos.tipo_vuelo_id', $request->tipo_vuelo);
                            
                        }];
                    }

                    if ($request->tipo_operacion!=null){    
                    //if ($request->has('tipo_operacion')){
                        $where[] = [function ($query) use ($request) {
                            
                            $query->where('proc_vuelos.tipo_operacion_id', $request->tipo_operacion);
                            
                        }];
                    }
                    
                   // if ($request->has('tipo_aviacion') && $request->has('tipo_operacion') && $request->has('tipo_vuelo') && $request->has('duracion')) {
                    /*
                        $whereAviacion = function ($query) use ($request) {
                            if ($request->tipo_aviacion != 'all') {
                                $query->where('proc_vuelos.tipo_aviacion_id', $request->tipo_aviacion);
                            }
                        };
                        $whereVuelo = function ($query) use ($request) {
                            if ($request->tipo_vuelo != 'all') {
                                $query->where('proc_vuelos.tipo_vuelo_id', $request->tipo_vuelo);
                            }
                        };
                        */
                        //$join->on($where);
                        $Vuelos = \App\Models\Aeropuertos::leftJoin("proc_vuelos", function ($join) use($where) {
                            $join->where("proc_vuelos.aeropuerto_id", "=", DB::raw("maest_aeropuertos.id"));
                            $join->where("proc_vuelos.activo", '=', DB::raw('true'));
                            $join->where($where);
                        })
                        ->where("maest_aeropuertos.activo", "1")
                        ->where("maest_aeropuertos.pertenece_baer", 1)
                        ->selectRaw("maest_aeropuertos.id, maest_aeropuertos.nombre, maest_aeropuertos.codigo_oaci, count(proc_vuelos.id) as cant_vuelos, SUM(COALESCE(proc_vuelos.pasajeros_embarcados, 0)) as cant_pax")
                        ->groupBy("maest_aeropuertos.id", "maest_aeropuertos.nombre", "maest_aeropuertos.codigo_oaci")
                        ->orderBy("cant_vuelos", "desc")
                        ->get()
                        ;
                        
                        //dd($Vuelos);
                        $color_blue = '#0000ff';
                        $color_green = '#008000';
                        // $step = 0x001111;
                        $cant_vuelos_color = [];
                        $aeropuertos = [];
                        $cant_vuelos = [];
                        $total_vuelos = 0;

                        $cant_pax_color = [];
                        $cant_pax = [];
                        $total_pax = 0;

                        foreach ($Vuelos as $key => $value) {
                            // $color_blue_hex = str_replace('#', '', $color_blue);
                            // $color_blue_hex_new = dechex(hexdec($color_blue_hex) - $key * $step);
                            // $color_blue_hex_new_padded = str_pad($color_blue_hex_new, 6, '0', STR_PAD_LEFT);
                            // $cant_vuelos_color[] = '#' . $color_blue_hex_new_padded;

                            $cant_vuelos_color[] = $color_blue;
                            $aeropuertos[] = $value->codigo_oaci;
                            $cant_vuelos[] = $value->cant_vuelos;
                            $total_vuelos += $value->cant_vuelos;

                            $cant_pax_color[] = $color_green;
                            $cant_pax[] = $value->cant_pax;
                            $total_pax += $value->cant_pax;
                        }

                        $response = [
                            'status' => 'success',
                            'message' => 'Los filtros han sido aplicados.',
                            'data' => array(
                                "aeropuertos"=> $aeropuertos,
                                "cant_vuelos" => $cant_vuelos,
                                "cant_vuelos_color" => $cant_vuelos_color,
                                "cant_pax_color" => $cant_pax_color,
                                "cant_pax" => $cant_pax,
                                "total_vuelos" => $total_vuelos,
                                "total_pax" => $total_pax
                                
                            )
                        ];


                        return response()->json($response);
                   // }
                    
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function search_plane(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
                return view('vuelos.search', compact('Aeronaves'));
            } else {
                if ($request->isMethod('post')) {
                    $fechas = explode(" - ", $request->rango);
                    $fecha_desde = $this->saveDate($fechas[0]);
                    $fecha_hasta = $this->saveDate($fechas[1]);
                    $Vuelos = Vuelos::where("aeronave_id", Encryptor::decrypt($request->aeronave_id))
                            ->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$fecha_desde, $fecha_hasta])
                            ->where("activo", true)
                            ->where("aprobado", true)
                            ->orderBy('id')
                            ->get();
                    //dd($Vuelos->toArray());



                    return array("status" => $Vuelos->count() > 0 ? 1 : 0, "data" => $Vuelos->toArray()); // $Vuelos->toArray();
                } else {
                    exit;
                }
            }
        } else {
            if ($request->isMethod('put')) {
                return Excel::download(new VuelosExport(), 'VUELOS.xlsx');
            }
            return Redirect::to('/dashboard');
        }
    }

    /*
     * 
     */

    function vuelos_tortuga(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");

                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
                return view('vuelos.tortuga', compact('Pilotos', 'Aeronaves'));
            } else {
                if ($request->isMethod('post')) {
                    //dd($request->all());

                    $Validator = \Validator::make(
                                    $request->all(), [
                                'fecha_operacion' => 'required',
                                'hora_llegada' => 'required',
                                'hora_salida' => 'required',
                                'aeronave_id' => 'required',
                                'piloto_id' => 'required',
                                'cant_pasajeros' => 'required',
                                    ], [
                                'fecha_operacion.required' => 'Campo Requerido',
                                'hora_llegada.required' => 'Campo Requerido',
                                'hora_salida.required' => 'Campo Requerido',
                                'aeronave_id.required' => 'Campo Requerido',
                                'piloto_id.required' => 'Campo Requerido',
                                'cant_pasajeros.required' => 'Campo Requerido',
                                    ]
                    );

                    if ($Validator->fails()) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = $Validator->errors()->first();
                    } else {
                        $hora_operacion = explode(' ', $request->hora_llegada);

                        $last_operation = Vuelos::where("aeropuerto_id", $this->TORTUGA_ID)
                                ->where("activo", true)
                                ->orderBy("id", "desc")
                                ->first();
                        $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                        $Vuelos = new Vuelos();

                        $Vuelos->numero_operacion = $num_operation;
                        $Vuelos->aeropuerto_id = $this->TORTUGA_ID;
                        $Vuelos->tipo_operacion_id = 1; //LLEGADA
                        $Vuelos->tipo_vuelo_id = 1; //NACIONAL
                        $Vuelos->procesado_recaudacion = true;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);

                        $Vuelos->hora_operacion = $hora_operacion[1];
                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);
                        $Vuelos->origen_destino_id = $this->TORTUGA_ID;
                        $Vuelos->cant_pasajeros = $request->cant_pasajeros;
                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = '[]';

                        $Vuelos->user_id = Auth::user()->id;
                        $Vuelos->fecha_registro = now();
                        $Vuelos->ip = $this->getIp();

                        $Vuelos->save();

                        $data['tipo_vuelo_id'] = 2; //Vuelos General
                        $data['nacionalidad_id'] = 1; // NAcional

                        $data['aeropuerto_id'] = $this->TORTUGA_ID;
                        $hora_operacion = explode(' ', $request->hora_llegada);
                        $data['hora_vuelo'] = $hora_operacion[1];
                        $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                        $data['plan_vuelo'] = 2; // Doble Toque
                        $data['cant_pasajeros'] = $request->cant_pasajeros;
                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion) . " 00:00:00";
                        $data['serv_extra'] = [];
                        $data['exento_dosa'] = !$request->cobrar_dosa;

                        $data['exento_tasa'] = true;
                        $servicios_facturar = $this->getProdServ($data);
                        //dd($servicios_facturar);
                        foreach ($servicios_facturar['data'] as $value) {
                            //dd($value);
                            $Detalle = new \App\Models\VuelosDetalle();

                            $Detalle->aplica_categoria = $value['aplica_categoria'];
                            $Detalle->aplica_estacion = $value['aplica_estacion'];
                            $Detalle->aplica_peso = $value['aplica_peso'];
                            $Detalle->aplicable = $value['aplicable'];
                            $Detalle->bs = $value['bs'];
                            $Detalle->cant = $value['cant'];
                            $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                            $Detalle->codigo = $value['codigo'];
                            $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                            $Detalle->default_carga = $value['default_carga'];
                            $Detalle->default_dosa = $value['default_dosa'];
                            $Detalle->default_tasa = $value['default_tasa'];
                            $Detalle->descripcion = $value['descripcion'];
                            $Detalle->formula = $value['formula'];
                            $Detalle->formula2 = $value['formula2'];
                            $Detalle->full_descripcion = $value['full_descripcion'];
                            $Detalle->iva = $value['iva'];
                            $Detalle->iva2 = $value['iva2'];
                            $Detalle->iva_aplicado = $value['iva_aplicado'];
                            $Detalle->moneda = $value['moneda'];
                            $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                            $Detalle->nomenclatura = $value['nomenclatura'];
                            $Detalle->peso_inicio = $value['peso_inicio'];
                            $Detalle->peso_fin = $value['peso_fin'];
                            $Detalle->precio = $value['precio'];
                            $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                            $Detalle->tasa = $value['tasa'];
                            $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                            $Detalle->turno_id = $value['turno_id'];
                            $Detalle->valor_dollar = $value['valor_dollar'];
                            $Detalle->valor_euro = $value['valor_euro'];
                            $Detalle->valor_petro = $value['valor_petro'];
                            $Detalle->orden = $value['orden'];
                            $Detalle->vuelo_id = $Vuelos->id;

                            $Detalle->save();
                        }



                        /*                         * *********************** */
                        $hora_operacion = explode(' ', $request->hora_salida);
                        $last_operation = Vuelos::where("aeropuerto_id", $this->TORTUGA_ID)
                                ->where("activo", true)
                                ->orderBy("id", "desc")
                                ->first();
                        $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                        $Vuelos = new Vuelos();

                        $Vuelos->numero_operacion = $num_operation;
                        $Vuelos->aeropuerto_id = $this->TORTUGA_ID;
                        $Vuelos->tipo_operacion_id = 2; //SALIDA
                        $Vuelos->tipo_vuelo_id = 1; //NACIONAL
                        $Vuelos->procesado_recaudacion = true;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                        $Vuelos->hora_operacion = $hora_operacion[1];
                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);
                        $Vuelos->origen_destino_id = $this->TORTUGA_ID;
                        $Vuelos->cant_pasajeros = $request->cant_pasajeros;
                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = '[]';

                        $Vuelos->user_id = Auth::user()->id;
                        $Vuelos->fecha_registro = now();
                        $Vuelos->ip = $this->getIp();
                        ;

                        $Vuelos->save();

                        $data['tipo_vuelo_id'] = 2; //Vuelos General
                        $data['nacionalidad_id'] = 1; // NAcional
                        $data['aeropuerto_id'] = $this->TORTUGA_ID;

                        $data['hora_vuelo'] = $hora_operacion[1];
                        $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);
                        $data['plan_vuelo'] = 2; // Doble Toque
                        $data['cant_pasajeros'] = $request->cant_pasajeros;
                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion) . " 00:00:00";
                        $data['serv_extra'] = [];
                        $data['exento_dosa'] = true;
                        $data['exento_tasa'] = !$request->cobrar_tasa;

                        $data['hora_llegada'] = $this->saveDate(substr($request->hora_llegada, 0, 10)) . " " . substr($request->hora_llegada, 11, 5);
                        $data['hora_salida'] = $this->saveDate(substr($request->hora_salida, 0, 10)) . " " . substr($request->hora_salida, 11, 5);

                        $servicios_facturar = $this->getProdServ($data);

                        foreach ($servicios_facturar['data'] as $value) {
                            //dd($value);
                            $Detalle = new \App\Models\VuelosDetalle();

                            $Detalle->aplica_categoria = $value['aplica_categoria'];
                            $Detalle->aplica_estacion = $value['aplica_estacion'];
                            $Detalle->aplica_peso = $value['aplica_peso'];
                            $Detalle->aplicable = $value['aplicable'];
                            $Detalle->bs = $value['bs'];
                            $Detalle->cant = $value['cant'];
                            $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                            $Detalle->codigo = $value['codigo'];
                            $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                            $Detalle->default_carga = $value['default_carga'];
                            $Detalle->default_dosa = $value['default_dosa'];
                            $Detalle->default_tasa = $value['default_tasa'];
                            $Detalle->descripcion = $value['descripcion'];
                            $Detalle->formula = $value['formula'];
                            $Detalle->formula2 = $value['formula2'];
                            $Detalle->full_descripcion = $value['full_descripcion'];
                            $Detalle->iva = $value['iva'];
                            $Detalle->iva2 = $value['iva2'];
                            $Detalle->iva_aplicado = $value['iva_aplicado'];
                            $Detalle->moneda = $value['moneda'];
                            $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                            $Detalle->nomenclatura = $value['nomenclatura'];
                            $Detalle->peso_inicio = $value['peso_inicio'];
                            $Detalle->peso_fin = $value['peso_fin'];
                            $Detalle->precio = $value['precio'];
                            $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                            $Detalle->tasa = $value['tasa'];
                            $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                            $Detalle->turno_id = $value['turno_id'];
                            $Detalle->valor_dollar = $value['valor_dollar'];
                            $Detalle->valor_euro = $value['valor_euro'];
                            $Detalle->valor_petro = $value['valor_petro'];
                            $Detalle->orden = $value['orden'];
                            $Detalle->vuelo_id = $Vuelos->id;

                            $Detalle->save();
                        }



                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Registrado Correctamente";
                    }
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function view_detail_fly($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Vuelo = Vuelos::join("seg_users", 'seg_users.id', '=', 'proc_vuelos.user_id')
                        //->select("*", "seg_users.document", "seg_users.name_user", "seg_users.surname_user", "seg_users.phone")
                        ->where("proc_vuelos.id", Encryptor::decrypt($id))
                        ->selectRaw("proc_vuelos.*, CONCAT(seg_users.name_user,' ',seg_users.surname_user) AS usuario")
                        ->first();
                /*                 * ******* */
                // dd($Vuelo->fecha_operacion.' '.$Vuelo->hora_operacion);

                if ($Vuelo->hora_operacion == null) {
                    if ($Vuelo->tipo_operacion_id == 1) {//si es salida
                        $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_salida);
                    } else {
                        $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_llegada);
                    }
                } else {
                    $fecha_vuelo = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->fecha_operacion . ' ' . $Vuelo->hora_operacion . ':00');
                }

                $data['fecha_vuelo'] = $fecha_vuelo->format("Y-m-d") . " 00:00:00";
                $data['hora_vuelo'] = $fecha_vuelo->format("H:i");

                $data['tipo_vuelo_id'] = 2; //Vuelos General
                $data['nacionalidad_id'] = $Vuelo->tipo_vuelo_id; // NAcional

                $data['aeropuerto_id'] = $Vuelo->aeropuerto_id;
                $Aeropuerto = \App\Models\Aeropuertos::find($data['aeropuerto_id']);

                // dd($Vuelo->aeropuerto_id);

                $data['turno_id'] = $Aeropuerto->getTurno((int) str_replace(":", "", $data['hora_vuelo']));

                $data['aeronave_id'] = $Vuelo->aeronave_id;

                $data['cant_pasajeros'] = $Vuelo->pasajeros_embarcados;

                $data['hora_llegada'] = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_llegada)->format("Y-m-d H:i:s");
                $data['hora_salida'] = Carbon::createFromFormat('Y-m-d H:i:s', $Vuelo->hora_salida)->format("Y-m-d H:i:s");

                if ($Vuelo->tipo_operacion_id == 1) {
                    $data['exento_dosa'] = false;
                    $data['exento_tasa'] = true;
                } else {
                    $data['exento_dosa'] = true;
                    $data['exento_tasa'] = false;
                }

                $prodserv_extra = $this->getProdservicios2('extra', $data);

                /*                 * ******* */
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Registro Encontrado";
                $result['data'] = $Vuelo->toArray();
                $result['data_extra'] = $prodserv_extra;
                ;
                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    private function getVuelosReg($params = []) {
        $where[] = [DB::raw('1'), 1];

        if (Auth::user()->aeropuerto_id != null) {
            $where[] = ['aeropuerto_id', Auth::user()->aeropuerto_id];
        } else {
            if (isset($params['aeropuerto_id'])) {
                $where[] = ['aeropuerto_id', $params['aeropuerto_id']];
            }
        }
        if (isset($params['aeronave_id'])) {
            $where[] = ['aeronave_id', $params['aeronave_id']];
        }
        if (isset($params['fecha_operacion'])) {
            $where[] = ['fecha_operacion', $params['fecha_operacion']];
        } else {
            $where[] = ['fecha_operacion', date('Y-m-d')];
        }
        if (isset($params['tipo_vuelo_id'])) {
            $where[] = ['tipo_vuelo_id', $params['tipo_vuelo_id']];
        }



        return Vuelos::where($where)->orderBy("id", "desc")->get();
    }

    public function get_vuelos_hoy() {
        $Vuelos = \App\Models\Vuelos::orderBy("id", "desc")
                ->whereIn("aeropuerto_id", Auth::user()->lista_aeropuertos)
                ->whereDate("fecha_registro", ">=", Carbon::createFromFormat("Y-m-d", date('Y-m-d'))->subDay()->format("Y-m-d"))
                ->orderBy("id")
                ->get()
        //->toSql()
        ; //Carbon::createFromFormat("Y-m-d", date('Y-m-d') )->subDay()->format("Y-m-d")
        $data = Datatables::of($Vuelos)
                ->addIndexColumn()
                ->addColumn('class', function ($row) {
                    $class = "";
                    if ($row->activo == false) {
                        $class = "alert-danger";
                    } else {
                        if ($row->exonerado == true) {
                            $class = "alert-warning";
                        } else {

                            if ($row->aprobado == true) {

                                if ($row->proforma_id != null) {
                                    $class = "alert-info";
                                } else {
                                    $class = "alert-success";
                                }
                            } else {
                                
                            }
                        }
                    }
                    return $class;
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = "";

                    $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.see', [$row->crypt_id, 'vuelos']) . '" title="Ver Vuelo" class=" btn   btn-xs btn-success "> <li class="fa fa-eye"></li> </a> ';
                    if ($row->activo == true) {
                        if ($row->proforma_id == null) {
                            $actionBtn .= '<a onClick="getDataEdit(this, event)" href="' . route('get_vuelos.edit_new', $row->crypt_id) . '" title="Editar Vuelo" class=" btn btn-primary btn-xs"> <li class="fa fa-edit"></li> </a> ';
                            $actionBtn .= '<a onClick="setAddAdons(this, event)" href="' . route('get_vuelos.edit_new', $row->crypt_id) . '" title="Agregar Complementos Al  Vuelo" class=" btn btn-info btn-xs"> <li class="fa fa-cubes"></li> </a> ';
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.disable', [$row->crypt_id, 'vuelos']) . '" title="Anular Vuelo" class=" btn btn-danger btn-xs"><li class="fa fa-trash"></li> </a> ';
                            $actionBtn .= '<a target="_blank" href="' . route('vuelos.see_fly', $row->crypt_id) . '" title="Reporte Vuelo" class=" btn btn-warning btn-xs"><li class="fa fa-file"></li> </a> ';
                        }
                    }
                    return $actionBtn;
                })
                ->rawColumns(['action'])
        //->make(true)
        ;
        return $data->toJson();
    }

    public function save_complementos() {
        if (\Request::ajax()) {
            //dd(\Request::all());

            $data = \Request::all();
            $id = Encryptor::decrypt($data['id']);
            switch ($data['complemento']) {
                case 'pax':
                    \App\Models\VuelosPasajeros::where("vuelo_id", $id)->delete();
                    if (isset($data['paxs'])) {
                        foreach ($data['paxs'] as $value) {
                            $VuelosPasajeros = new \App\Models\VuelosPasajeros();
                            $VuelosPasajeros->documento = Upper($value['cedula']);
                            $VuelosPasajeros->nombres = Upper($value['nombre']);
                            $VuelosPasajeros->apellidos = Upper($value['apellido']);
                            $VuelosPasajeros->genero = Upper($value['genero']);
                            $VuelosPasajeros->vuelo_id = $id;
                            $VuelosPasajeros->save();
                        }
                    }
                    $result['message'] = "Pasajeros Registrados Correctamente";
                    break;
                case 'responsable':

                    $Cliente = \App\Models\Clientes::where('tipo_documento', $data['type_document'])->where('documento', $data['document'])->first();
                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                        $Cliente->tipo_documento = $data['type_document'];
                        $Cliente->documento = $data['document'];

                        $Cliente->user_id = Auth::user()->id;
                        $Cliente->ip = $this->getIp();
                        $Cliente->fecha_registro = now();
                    }

                    $Cliente->razon_social = Upper($data['razon']);
                    $Cliente->telefono = $data['phone'];
                    $Cliente->correo = $data['correo'];
                    $Cliente->direccion = $data['direccion'];
                    $Cliente->tipo_id = 4; // general

                    $Cliente->save();

                    $observaciones_facturacion = $data['type_document'] . '|' . $data['document'] . '|' . $Cliente->razon_social . '|' . $Cliente->telefono . '|' . $Cliente->direccion;

                    $Vuelo = Vuelos::find($id);
                    $Vuelo->observaciones_facturacion = $observaciones_facturacion;
                    $Vuelo->save();
                    $result['message'] = "Responsable Registrado Correctamente";

                    break;
                case 'plat':
                    $Vuelo = Vuelos::find($id);
                    $Vuelo->puesto_id = Encryptor::decrypt($data['puesto']);
                    $Vuelo->save();

                    $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelo->aeronave_id)->update(["aeronave_id" => null]);

                    $Puesto = \App\Models\Puestos::find(Encryptor::decrypt($data['puesto']));
                    $Puesto->aeronave_id = $Vuelo->aeronave_id;
                    $Puesto->save();

                    \App\Models\AeronavesPuestos::where("vuelo_id", $Vuelo->id)->delete();

                    $AeronavesPuestos = new \App\Models\AeronavesPuestos();
                    $AeronavesPuestos->vuelo_id = $Vuelo->id;
                    $AeronavesPuestos->puesto_id = Encryptor::decrypt($data['puesto']);
                    $AeronavesPuestos->fecha_entrada = $Vuelo->fecha_operacion . ' ' . $Vuelo->hora_operacion;
                    $AeronavesPuestos->user_id = Auth::user()->id;
                    $AeronavesPuestos->fecha_registro = now();
                    $AeronavesPuestos->ip = $this->getIp();
                    $AeronavesPuestos->save();

                    $result['message'] = "Plataforma Asignada Correctamente";

                    break;
                case 'orig_dest':
                    if ($data['op'] == 1) {

                        $Vuelo = Vuelos::find($id);
                        $Vuelo->orig_dest_id = Encryptor::decrypt($data['orig_dest']);
                        $Vuelo->save();
                    } else {
                        $Aeropuertos = new \App\Models\Aeropuertos();
                        $Aeropuertos->codigo_oaci = Upper($data['codigo_oaci']);
                        $Aeropuertos->codigo_iata = Upper($data['codigo_oaci']);
                        $Aeropuertos->nombre = Upper($data['nombre']);
                        $Aeropuertos->categoria_id = 1;

                        $Aeropuertos->pertenece_baer = false;
                        $Aeropuertos->iva = 0;

                        $Aeropuertos->hora_ocaso = '19:00';
                        $Aeropuertos->hora_amanecer = '06:00';

                        $Aeropuertos->pais_id = Encryptor::decrypt($data['pais_id']);

                        $Aeropuertos->user_id = Auth::user()->id;
                        $Aeropuertos->ip = $this->getIp();
                        $Aeropuertos->fecha_registro = now();

                        $Aeropuertos->save();

                        $Vuelo = Vuelos::find($id);
                        $Vuelo->orig_dest_id = Encryptor::decrypt($data['orig_dest']);
                        $Vuelo->save();
                    }
                    $result['message'] = "Aeropuerto Asignado Correctamente";
                    break;
            }
            $result['status'] = 1;
            $result['type'] = 'success';

            return $result;
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function edit_vuelo_new($id) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                return array("status" => "1", "type" => "success", "message" => "Registrado Correctamente", "data" => Vuelos::find(Encryptor::decrypt($id)));
            } else {
                
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function get_plataformas($id) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Vuelo = Vuelos::find(Encryptor::decrypt($id));
                $Aeropuerto = \App\Models\Aeropuertos::find($Vuelo->aeropuerto_id);
                return view('vuelos.plataformas', compact('Aeropuerto', 'Vuelo'));
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function create(Request $request) {

        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("pertenece_baer", true)
                        ->where("activo", true)
                        ->whereIn("id", Auth::user()->lista_aeropuertos)
                        ->get()
                        ->pluck("full_nombre", "crypt_id");

                $AeropuertosAll = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("activo", true)
                        ->get()
                //->pluck("full_nombre", "crypt_id")
                ;

                $Paises = \App\Models\Paises::orderBy("name_country")
                        ->get()
                        ->pluck("name_country", "crypt_id");
                $Venezuela = Encryptor::encrypt(1);
                //return view('vuelos.create', compact('listPilotosropuertos'));
                //return view('vuelos.create_new', compact('listPilotosropuertos'));
                //return view('vuelos.create_new_portal', compact('Venezuela', 'Paises', 'listPilotosropuertos', 'AeropuertosAll'));

                if (Auth::user()->aeropuerto_id == null) {
                    if (in_array(Auth::user()->id, [4, 8])) {
                        return view('vuelos.create', compact('Aeropuertos'));
                    } else {
                        return view('vuelos.create_new_portal', compact('Venezuela', 'Paises', 'Aeropuertos', 'AeropuertosAll'));
                    }
                    //return view('vuelos.create_new_portal', compact('Venezuela', 'Paises', 'Aeropuertos', 'AeropuertosAll'));
                } else {
                    if (in_array(Auth::user()->aeropuerto_id, $this->aeropuertosVersion2)) {
                        return view('vuelos.create_new_portal', compact('Venezuela', 'Paises', 'Aeropuertos', 'AeropuertosAll'));
                    } else {
                        return view('vuelos.create', compact('Aeropuertos'));
                    }
                }
            } else {
                if ($request->isMethod('post')) {
                    if (isset($request->newPortal)) {
                        // dd($request->all());
                        $carga = $request->carga == 0 ? 0 : saveFloat($request->carga); 
                        if ($request->aeropuerto_id == null) {
                            $aeropuerto_id = Auth::user()->aeropuerto_id;
                        } else {
                            $aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                        }

                        $prod_servd = [];
                        if (is_array($request->prod_servd)) {
                            foreach ($request->prod_servd as $value) {
                                $p = explode(":", $value);
                                $prod_servd[][$p[0]] = Encryptor::decrypt($p[1]);
                            }
                        }



                        if (Encryptor::decrypt($request->id) == 0) {
                            $last_operation = Vuelos::where("aeropuerto_id", $aeropuerto_id)
                                    //->where("activo", true)
                                    ->orderBy("id", "desc")
                                    ->first();
                            $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                            $Vuelos = new Vuelos();

                            $Vuelos->user_id = Auth::user()->id;
                            $Vuelos->fecha_registro = now();
                            $Vuelos->ip = $this->getIp();
                        } else {
                            $Vuelos = Vuelos::find(Encryptor::decrypt($request->id));
                            $num_operation = $Vuelos->numero_operacion;

                            $Vuelos->user_edit_id = Auth::user()->id;
                        }



                        $Vuelos->aeropuerto_id = $aeropuerto_id;
                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);
                        $Vuelos->numero_operacion = $num_operation;
                        
                        $Vuelos->carga = $carga;
                        $Vuelos->tipo_vuelo_id = $request->nacionalidad_id;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                        $Vuelos->hora_operacion = $request->hora_operacion;
                        $Vuelos->pasajeros_desembarcados = $request->pax;
                        $Vuelos->pasajeros_embarcados = $request->pax;

                        $Vuelos->hora_llegada = Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format('Y-m-d H:i');
                        $Vuelos->hora_salida = Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format('Y-m-d H:i');

                        if ($request->tipo_id == 1) {//si es llegada
                            $Vuelos->cobrar_tasa = false;
                            $Vuelos->cobrar_dosa = true;
                        } else { // si es salida
                            $Vuelos->cobrar_tasa = true;
                            $Vuelos->cobrar_dosa = false;
                        }

                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = json_encode($prod_servd);

                        $Vuelos->observaciones_facturacion = "";
                        $Vuelos->aprobado = true;
                        $Vuelos->tipo_aviacion_id = $request->tipo_aviacion_id;
                        $Vuelos->tipo_operacion_id = $request->tipo_id;
                        $Vuelos->orig_dest_id = Encryptor::decrypt($request->orig_dest_id) == 0 ? null : Encryptor::decrypt($request->orig_dest_id);

                        ;

                        $Vuelos->save();

                        if ($request->tipo_id == 2) {
                            $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelos->aeronave_id)->update(["aeronave_id" => null]);

                            \App\Models\AeronavesPuestos::where("vuelo_id", $Vuelos->aeronave_id)->update(["fecha_salida" => $this->saveDate($request->fecha_operacion) . ' ' . $request->hora_operacion]);
                        }



                        /**                         * **************** */
                        /*
                          $Aeropuerto = \App\Models\Aeropuertos::find($aeropuerto_id);
                          $Aeropuerto = $Aeropuerto->toArray();
                          $subject = "Registro de Vuelo";
                          $for = "mrivero@baer.gob.ve";
                          $datos = $Vuelos->toArray();
                          Mail::send('email.send_mr', $Aeropuerto, function ($msj) use ($subject, $for) {
                          $msj->subject($subject);
                          $msj->to($for);
                          //$msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                          });
                         */
                        /*                         * ****************** */


                        $data['tipo_vuelo_id'] = 2; //Vuelos General
                        $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional
                        $data['aeropuerto_id'] = $aeropuerto_id;
                        $data['hora_vuelo'] = $request->hora_operacion;
                        $data['carga'] = isset($request->carga) ? saveFloat($request->carga)*1000:0;
                        
                        if (isset($request->estacionamiento)) {
                        $data['hora_llegada_est'] = saveDateTime($request->hora_llegada);
                        $data['hora_salida_est'] = saveDateTime($request->hora_salida);
                        }
                        //$data['hora_llegada_est'] = saveDateTime($request->hora_llegada);
                        //$data['hora_salida_est'] = saveDateTime($request->hora_salida);

                        $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);

                        $data['cant_pasajeros'] = $request->pax;
                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion);
                        $data['hora_vuelo'] = $request->hora_operacion;

                        if ($request->prod_servd == null) {
                            $data['serv_extra'] = [];
                        } else {
                            $data['serv_extra']['ids'] = [];
                            foreach ($request->prod_servd as $value) {

                                $servp = explode(":", $value);
                                $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($servp[1]);
                                $data['serv_extra']['cant'][$servp[1]] = $servp[0];
                            }
                        }

                        if ($request->tipo_id == 1) {//si es llegada
                            $data['exento_dosa'] = false;
                            $data['exento_tasa'] = true;
                            $data['hora_llegada'] = saveDateTime($request->fecha_operacion . ' ' . $request->hora_operacion);
                        } else { // si es salida
                            $data['exento_dosa'] = true;
                            $data['exento_tasa'] = false;
                            $data['hora_salida'] = saveDateTime($request->fecha_operacion . ' ' . $request->hora_operacion);
                        }

                        $servicios_facturar = $this->getProdServ($data);
                        \App\Models\VuelosDetalle::where("vuelo_id", $Vuelos->id)->delete();
                        //dd($servicios_facturar);
                        foreach ($servicios_facturar['data'] as $value) {
                            //dd($value);
                            $reg = true;
                            if (isset($request->prod_servd_remove)) {
                                if (in_array($value['codigo'], $request->prod_servd_remove)) {
                                    $reg = false;
                                }
                            }
                            if ($reg == true) {



                                $Detalle = new \App\Models\VuelosDetalle();

                                $Detalle->aplica_categoria = $value['aplica_categoria'];
                                $Detalle->aplica_estacion = $value['aplica_estacion'];
                                $Detalle->aplica_peso = $value['aplica_peso'];
                                $Detalle->aplicable = $value['aplicable'];
                                $Detalle->bs = $value['bs'];
                                $Detalle->cant = $value['cant'];
                                $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                                $Detalle->codigo = $value['codigo'];
                                $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                                $Detalle->default_carga = $value['default_carga'];
                                $Detalle->default_dosa = $value['default_dosa'];
                                $Detalle->default_tasa = $value['default_tasa'];
                                $Detalle->descripcion = $value['descripcion'];
                                $Detalle->formula = $value['formula'];
                                $Detalle->formula2 = $value['formula2'];
                                $Detalle->full_descripcion = $value['full_descripcion'];
                                $Detalle->iva = $value['iva'];
                                $Detalle->iva2 = $value['iva2'];
                                $Detalle->iva_aplicado = $value['iva_aplicado'];
                                $Detalle->moneda = $value['moneda'];
                                $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                                $Detalle->nomenclatura = $value['nomenclatura'];
                                $Detalle->peso_inicio = $value['peso_inicio'];
                                $Detalle->peso_fin = $value['peso_fin'];
                                $Detalle->precio = $value['precio'];
                                $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                                $Detalle->tasa = $value['tasa'];
                                $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                                $Detalle->turno_id = $value['turno_id'];
                                $Detalle->valor_dollar = $value['valor_dollar'];
                                $Detalle->valor_euro = $value['valor_euro'];
                                $Detalle->valor_petro = $value['valor_petro'];
                                $Detalle->orden = $value['orden'];
                                $Detalle->vuelo_id = $Vuelos->id;

                                $Detalle->save();
                            }
                        }

                        $result['status'] = 1;
                        $result['type'] = 'success';
                        if (Encryptor::decrypt($request->id) == 0) {
                            $result['message'] = "Registrado Correctamente";
                            $result['close_modal'] = false;
                        } else {
                            $result['message'] = "Actualizado Correctamente";
                            $result['close_modal'] = true;
                        }


                        return $result;
                    } else {
                        $Cliente = \App\Models\Clientes::where('tipo_documento', $request->type_document)->where('documento', $request->document)->first();
                        if ($Cliente == null) {
                            $Cliente = new \App\Models\Clientes();
                            $Cliente->tipo_documento = $request->type_document;
                            $Cliente->documento = $request->document;

                            $Cliente->user_id = Auth::user()->id;
                            $Cliente->ip = $this->getIp();
                            $Cliente->fecha_registro = now();
                        }

                        $Cliente->razon_social = Upper($request->razon);
                        $Cliente->telefono = $request->phone;
                        $Cliente->correo = $request->correo;
                        $Cliente->direccion = $request->direccion;
                        $Cliente->tipo_id = 4; // general

                        $Cliente->save();

                        $observaciones_facturacion = $request->type_document . '|' . $request->document . '|' . $Cliente->razon_social . '|' . $Cliente->telefono . '|' . $Cliente->direccion;

                        $prod_servd = [];
                        if (is_array($request->prod_servd)) {
                            foreach ($request->prod_servd as $value) {
                                $p = explode(":", $value);
                                $prod_servd[][$p[0]] = Encryptor::decrypt($p[1]);
                            }
                        }
                        $last_operation = Vuelos::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                                ->where("activo", true)
                                ->orderBy("id", "desc")
                                ->first();
                        $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                        $Vuelos = new Vuelos();

                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);
                        $Vuelos->numero_operacion = $num_operation;
                        $Vuelos->tipo_vuelo_id = $request->nacionalidad_id;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                        $Vuelos->pasajeros_desembarcados = $request->pax_desembarcados;
                        $Vuelos->pasajeros_embarcados = $request->pax_embarcados;

                        $Vuelos->hora_llegada = $request->hora_llegada == null ? null : Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format('Y-m-d H:i');
                        ;
                        $Vuelos->hora_salida = $request->hora_salida == null ? null : Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format('Y-m-d H:i');

                        $Vuelos->cobrar_tasa = $request->cobrar_tasa;
                        $Vuelos->cobrar_dosa = $request->cobrar_dosa;

                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = json_encode($prod_servd);

                        $Vuelos->observaciones_facturacion = $observaciones_facturacion;
                        $Vuelos->aprobado = true;

                        $Vuelos->user_id = Auth::user()->id;
                        $Vuelos->fecha_registro = now();
                        $Vuelos->ip = $this->getIp();
                        ;

                        $Vuelos->save();

                        /**                         * **************** */
                        $Aeropuerto = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->aeropuerto_id));
                        $Aeropuerto = $Aeropuerto->toArray();

                        $subject = "Registro de Vuelo";
                        $for = "mrivero@baer.gob.ve";
                        $datos = $Vuelos->toArray();
                        Mail::send('email.send_mr', $Aeropuerto, function ($msj) use ($subject, $for) {
                            $msj->subject($subject);
                            $msj->to($for);
                            //$msj->attachData($proforma->output(), "PROFORMA_" . showCode($Proforma->id) . ".pdf");
                        });
                        /*                         * ***************** */
                        $data['tipo_vuelo_id'] = 2; //Vuelos General
                        $data['nacionalidad_id'] = $request->nacionalidad_id; // NAcional

                        $data['aeropuerto_id'] = Encryptor::decrypt($request->aeropuerto_id);

                        if ($request->cobrar_tasa == true) {
                            $hora_operacion = explode(' ', $request->hora_salida);
                            $data['hora_vuelo'] = $hora_operacion[1];
                        } else {
                            if ($request->cobrar_dosa == true) {
                                $hora_operacion = explode(' ', $request->hora_llegada);
                                $data['hora_vuelo'] = $hora_operacion[1];
                            }
                        }

                        if ($request->hora_llegada != null) {
                            $data['hora_llegada'] = saveDateTime($request->hora_llegada);
                        }

                        if ($request->hora_salida != null) {
                            $data['hora_salida'] = saveDateTime($request->hora_salida);
                        }

                        $data['aeronave_id'] = Encryptor::decrypt($request->aeronave_id);

                        $data['cant_pasajeros'] = $request->pax_embarcados;
                        $data['fecha_vuelo'] = $this->saveDate($request->fecha_operacion) . " 00:00:00";

                        if ($request->prod_servd == null) {
                            $data['serv_extra'] = [];
                        } else {
                            $data['serv_extra']['ids'] = [];
                            foreach ($request->prod_servd as $value) {

                                $servp = explode(":", $value);
                                $data['serv_extra']['ids'][] = \App\Helpers\Encryptor::decrypt($servp[1]);
                                $data['serv_extra']['cant'][$servp[1]] = $servp[0];
                            }
                        }


                        //$data['serv_extra'] = $request->prod_servd==null ? []:$request->prod_servd;
                        //$data['cobrar_dosa'] = $request->cobrar_dosa;
                        //$data['cobrar_tasa'] = $request->cobrar_tasa;

                        $data['exento_dosa'] = !$request->cobrar_dosa;
                        $data['exento_tasa'] = !$request->cobrar_tasa;

                        //dd($data);
                        $servicios_facturar = $this->getProdServ($data);
                        //dd($servicios_facturar);
                        foreach ($servicios_facturar['data'] as $value) {
                            //dd($value);
                            $reg = true;
                            if (isset($request->prod_servd_remove)) {
                                if (in_array($value['codigo'], $request->prod_servd_remove)) {
                                    $reg = false;
                                }
                            }
                            if ($reg == true) {



                                $Detalle = new \App\Models\VuelosDetalle();

                                $Detalle->aplica_categoria = $value['aplica_categoria'];
                                $Detalle->aplica_estacion = $value['aplica_estacion'];
                                $Detalle->aplica_peso = $value['aplica_peso'];
                                $Detalle->aplicable = $value['aplicable'];
                                $Detalle->bs = $value['bs'];
                                $Detalle->cant = $value['cant'];
                                $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                                $Detalle->codigo = $value['codigo'];
                                $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                                $Detalle->default_carga = $value['default_carga'];
                                $Detalle->default_dosa = $value['default_dosa'];
                                $Detalle->default_tasa = $value['default_tasa'];
                                $Detalle->descripcion = $value['descripcion'];
                                $Detalle->formula = $value['formula'];
                                $Detalle->formula2 = $value['formula2'];
                                $Detalle->full_descripcion = $value['full_descripcion'];
                                $Detalle->iva = $value['iva'];
                                $Detalle->iva2 = $value['iva2'];
                                $Detalle->iva_aplicado = $value['iva_aplicado'];
                                $Detalle->moneda = $value['moneda'];
                                $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                                $Detalle->nomenclatura = $value['nomenclatura'];
                                $Detalle->peso_inicio = $value['peso_inicio'];
                                $Detalle->peso_fin = $value['peso_fin'];
                                $Detalle->precio = $value['precio'];
                                $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                                $Detalle->tasa = $value['tasa'];
                                $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                                $Detalle->turno_id = $value['turno_id'];
                                $Detalle->valor_dollar = $value['valor_dollar'];
                                $Detalle->valor_euro = $value['valor_euro'];
                                $Detalle->valor_petro = $value['valor_petro'];
                                $Detalle->orden = $value['orden'];
                                $Detalle->vuelo_id = $Vuelos->id;

                                $Detalle->save();
                            }
                        }


                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Registrado Correctamente";

                        return $result;
                    }
                    dd($request->all());
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function index($f = null, $aeropuerto = null) {
        $request = new Request();

        if (\Request::ajax()) {
            if ($request->isMethod('get')) {

                $cond = [];

                if ($f != null) {
                    $cond['fecha_operacion'] = $f;
                }
                if ($aeropuerto != null) {
                    $cond['aeropuerto_id'] = Encryptor::decrypt($aeropuerto);
                }



                $datos = $this->getVuelosReg($cond);
                // dd($datos);
                $datos = Datatables::of($datos)
                        ->addIndexColumn()
                        ->addColumn('class', function ($row) {
                            $class = "";
                            /*
                              if ($row->activo == false) {
                              $class = "alert-danger";
                              }
                             * 
                             */
                            return $class;
                        })
                        ->addColumn('action', function ($row) {
                            $actionBtn = "";
                            /*
                              if (date('Y-m-d')==$row->fecha_operacion){
                              $actionBtn .= "editar";
                              }
                              $actionBtn .= "VEr";
                             */
                            /*
                              if (in_array("see", Auth::user()->getActions()[$route_id])) {
                              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.see', $row->crypt_id) . '" title="Ver Vuelo" class=" btn btn-warning btn-sm"> <li class="fa fa-eye"></li> </a> ';
                              }
                              if ($row->activo == true) {

                              $ToDay = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
                              $dateReg = Carbon::createFromFormat('Y-m-d', substr($row->fecha_registro, 0, 10));
                              if ($ToDay->eq($dateReg)) {
                              if ($row->factura_id == null && $row->procesado_recaudacion == false) {
                              if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.edit', $row->crypt_id) . '" title="Editar Vuelo" class=" btn btn-primary btn-sm"> <li class="fa fa-edit"></li> </a> ';
                              }

                              if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.disable', $row->crypt_id) . '" title="Anular Vuelo" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> </a> ';
                              }
                              if ($row->tipo_operacion_id == 1) { //SI ES UN ARRIBO
                              if (in_array("position", Auth::user()->getActions()[$route_id])) {
                              $Aeropuerto = \App\Models\Aeropuertos::find($row->aeropuerto_id);
                              //dd($Aeropuerto->Ubicaciones->count()));
                              if ($Aeropuerto->Ubicaciones->count() > 0) {
                              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.position', $row->crypt_id) . '" title="Agregar Puesto" class=" btn btn-success btn-sm"><li class="fa fa-home"></li> </a> ';
                              } else {

                              $actionBtn .= '<a  href="javascript:void(0)" title="El Aeropuerto no Posee Puestos Registrados" class=" btn btn-success btn-sm disabled"><li class="fa fa-home"></li> </a> ';
                              }
                              }
                              }
                              }
                              }
                              }
                             */




                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                $datos = $datos->original['data'];

                //dd($datos);

                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }



                return view('vuelos.index', compact('datos', 'f', 'Aeropuertos', 'aeropuerto'));
            } else {
                if ($request->isMethod('post')) {

                    $Validator = \Validator::make(
                                    $request->all(), [
                                'aeropuerto_id' => 'required',
                                'tipo_operacion_id' => 'required',
                                'nacionalidad_id' => 'required',
                                'fecha_operacion' => 'required',
                                'hora_operacion' => 'required',
                                'aeronave_id' => 'required',
                                'piloto_id' => 'required',
                                'origen_destino_id' => 'required',
                                'cant_pasajeros' => 'required',
                                    ], [
                                'aeropuerto_id.required' => 'Campo Requerido',
                                'tipo_operacion_id.required' => 'Campo Requerido',
                                'nacionalidad_id.required' => 'Campo Requerido',
                                'fecha_operacion.required' => 'Campo Requerido',
                                'hora_operacion.required' => 'Campo Requerido',
                                'aeronave_id.required' => 'Campo Requerido',
                                'piloto_id.required' => 'Campo Requerido',
                                'origen_destino_id.required' => 'Campo Requerido',
                                'cant_pasajeros.required' => 'Campo Requerido',
                                'observacion.required' => 'Campo Requerido',
                                    ]
                    );

                    if ($Validator->fails()) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = $Validator->errors()->first();
                    } else {


                        if ($request->tipo_operacion_id == 1) {//SI SE REGISTRA UN ARRIBO
                            $Esta = Vuelos::where("activo", 1)
                                    ->where('aeropuerto_id', Encryptor::decrypt($request->aeropuerto_id))
                                    //->where('tipo_operacion_id', $request->tipo_operacion_id)
                                    ->where('aeronave_id', Encryptor::decrypt($request->aeronave_id))
                                    ->orderBy("id", "desc")
                                    ->first();
                            if ($Esta != null) {
                                if ($Esta->tipo_operacion_id == 1) {// SI EL ULTIMO REGISTRO FUE UN ARRIBO
                                    $result['status'] = 0;
                                    $result['type'] = 'error';
                                    $result['message'] = "La Aeronave Ya Posee Un Registro de un Arribo";
                                    return $result;
                                }
                            }
                        }

                        $prod_servd = [];
                        if (is_array($request->prod_servd)) {
                            foreach ($request->prod_servd as $value) {
                                $p = explode(":", $value);
                                $prod_servd[][$p[0]] = Encryptor::decrypt($p[1]);
                            }
                        }
                        $last_operation = Vuelos::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                                ->where("activo", true)
                                ->orderBy("id", "desc")
                                ->first();
                        $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                        $Vuelos = new Vuelos();

                        $Vuelos->numero_operacion = $num_operation;
                        $Vuelos->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                        $Vuelos->tipo_operacion_id = $request->tipo_operacion_id;
                        $Vuelos->tipo_vuelo_id = $request->nacionalidad_id;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                        $Vuelos->hora_operacion = $request->hora_operacion;
                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);
                        $Vuelos->origen_destino_id = Encryptor::decrypt($request->origen_destino_id);
                        $Vuelos->cant_pasajeros = $request->cant_pasajeros;
                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = json_encode($prod_servd);

                        $Vuelos->user_id = Auth::user()->id;
                        $Vuelos->fecha_registro = now();
                        $Vuelos->ip = $this->getIp();
                        ;

                        $Vuelos->save();

                        if ($request->tipo_operacion_id == 2) {//SI ES UN DESPEGUE LIBERO EL PUESTO
                            $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelos->aeronave_id)->update(["aeronave_id" => null]);
                        }


                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Registrado Correctamente";
                    }
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function edit_vuelo($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {
                $Vuelo = Vuelos::find(Encryptor::decrypt($id));
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");

                $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");
                
                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    $OrigenDestino = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("id", "!=", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    $OrigenDestino = \App\Models\Aeropuertos::orderBy("nombre")
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }



                return view('vuelos.edit', compact('Pilotos', 'Aeronaves', 'Aeropuertos', 'OrigenDestino', 'Vuelo'));
            } else {
                if ($request->isMethod('post')) {

                    $Validator = \Validator::make(
                                    $request->all(), [
                                'aeropuerto_id' => 'required',
                                'aeronave_id' => 'required',
                                'piloto_id' => 'required',
                                'nacionalidad_id' => 'required',
                                'fecha_operacion' => 'required',
                                    ], [
                                'aeropuerto_id.required' => 'Campo Requerido',
                                'aeronave_id.required' => 'Campo Requerido',
                                'piloto_id.required' => 'Campo Requerido',
                                'nacionalidad_id.required' => 'Campo Tipo de Operacion Requerido',
                                'fecha_operacion.required' => 'Campo Requerido',
                                    ]
                    );

                    if ($Validator->fails()) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = $Validator->errors()->first();
                    } else {

                        //dd($request->prod_servd);
                        $prod_servd = [];
                        if (is_array($request->prod_servd)) {
                            foreach ($request->prod_servd as $value) {
                                $p = explode(":", $value);
                                $prod_servd[][$p[0]] = Encryptor::decrypt($p[1]);
                            }
                        }

                        $carga = $request->carga == 0 ? 0 : saveFloat($request->carga); 
                        $Vuelos = Vuelos::find(Encryptor::decrypt($id));
                        $prod_servd = [];
                        if (is_array($request->prod_servd)) {
                            foreach ($request->prod_servd as $value) {
                                $p = explode(":", $value);
                                $prod_servd[][$p[0]] = Encryptor::decrypt($p[1]);
                            }
                        }
                        $last_operation = Vuelos::where("aeropuerto_id", Encryptor::decrypt($request->aeropuerto_id))
                                ->where("activo", true)
                                ->orderBy("id", "desc")
                                ->first();
                        $num_operation = $last_operation == null ? 1 : $last_operation->numero_operacion + 1;

                        $Vuelos = Vuelos::find(Encryptor::decrypt($id));
                        $Vuelos->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                        $Vuelos->aeronave_id = Encryptor::decrypt($request->aeronave_id);
                        $Vuelos->piloto_id = Encryptor::decrypt($request->piloto_id);

                        $Vuelos->tipo_vuelo_id = $request->nacionalidad_id;
                        $Vuelos->carga = $carga;
                        $Vuelos->fecha_operacion = $this->saveDate($request->fecha_operacion);
                        $Vuelos->pasajeros_desembarcados = $request->pax_desembarcados;
                        $Vuelos->pasajeros_embarcados = $request->pax_embarcados;

                        $Vuelos->hora_llegada = $request->hora_llegada == null ? null : Carbon::createFromFormat('d/m/Y H:i', $request->hora_llegada)->format('Y-m-d H:i');
                        ;
                        $Vuelos->hora_salida = $request->hora_salida == null ? null : Carbon::createFromFormat('d/m/Y H:i', $request->hora_salida)->format('Y-m-d H:i');

                        $Vuelos->cobrar_tasa = $request->cobrar_tasa;
                        $Vuelos->cobrar_dosa = $request->cobrar_dosa;

                        $Vuelos->observaciones = $request->observacion == null ? '' : $request->observacion;
                        $Vuelos->prodservicios_extra = json_encode($prod_servd);

                        $Vuelos->aprobado = true;

                        $Vuelos->user_edit_id = Auth::user()->id;

                        $Vuelos->save();

                        \App\Models\VuelosDetalle::where("vuelo_id", Encryptor::decrypt($id))->delete();
                        //dd($request->all());

                        $exento_dosa = $request->cobrar_dosa == 0 ? false : true;
                        $exento_tasa = $request->cobrar_tasa == 0 ? false : true;

                        $servicios_facturar = $Vuelos->getDetalleByRecord($exento_tasa, $exento_dosa);
                        // dd($servicios_facturar);
                        foreach ($servicios_facturar['data'] as $value) {
                            //dd($value);
                            $Detalle = new \App\Models\VuelosDetalle();
                            $Detalle->aplica_categoria = $value['aplica_categoria'];
                            $Detalle->aplica_estacion = $value['aplica_estacion'];
                            $Detalle->aplica_peso = $value['aplica_peso'];
                            $Detalle->aplicable = $value['aplicable'];
                            $Detalle->bs = $value['bs'];
                            $Detalle->cant = $value['cant'];
                            $Detalle->categoria_aplicada = $value['categoria_aplicada'];
                            $Detalle->codigo = $value['codigo'];
                            $Detalle->prodservicio_id = Encryptor::decrypt($value['crypt_id']);
                            $Detalle->default_carga = $value['default_carga'];
                            $Detalle->default_dosa = $value['default_dosa'];
                            $Detalle->default_tasa = $value['default_tasa'];
                            $Detalle->descripcion = $value['descripcion'];
                            $Detalle->formula = $value['formula'];
                            $Detalle->formula2 = $value['formula2'];
                            $Detalle->full_descripcion = $value['full_descripcion'];
                            $Detalle->iva = $value['iva'];
                            $Detalle->iva2 = $value['iva2'];
                            $Detalle->iva_aplicado = $value['iva_aplicado'];
                            $Detalle->moneda = $value['moneda'];
                            $Detalle->nacionalidad_id = $value['nacionalidad_id'];
                            $Detalle->nomenclatura = $value['nomenclatura'];
                            $Detalle->peso_inicio = $value['peso_inicio'];
                            $Detalle->peso_fin = $value['peso_fin'];
                            $Detalle->precio = $value['precio'];
                            $Detalle->prodservicio_extra = $value['prodservicio_extra'];
                            $Detalle->tasa = $value['tasa'];
                            $Detalle->tipo_vuelo_id = $value['tipo_vuelo_id'];
                            $Detalle->turno_id = $value['turno_id'];
                            $Detalle->valor_dollar = $value['valor_dollar'];
                            $Detalle->valor_euro = $value['valor_euro'];
                            $Detalle->valor_petro = $value['valor_petro'];
                            $Detalle->orden = $value['orden'];
                            $Detalle->vuelo_id = $Vuelos->id;

                            $Detalle->save();
                        }


                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Actualizado Correctamente";
                    }
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function see_vuelo($id, $back = 'get_vuelos') {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {

                $Vuelo = Vuelos::find(Encryptor::decrypt($id));
                //$routeBack = route("get_vuelos", [$Vuelo->aeropuerto->crypt_id, $Vuelo->fecha_operacion . ' : ' . $Vuelo->fecha_operacion, $Vuelo->aeronave->crypt_id]);
                $routeBack = route($back);
                return view('vuelos.see_vuelos', compact('Vuelo', 'routeBack'));
            } else {
                exit;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function position_vuelo($id, Request $request) {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $Vuelo = Vuelos::find(Encryptor::decrypt($id));
                $Aeropuerto = \App\Models\Aeropuertos::find($Vuelo->aeropuerto_id);
                // dd($Ubicaciones->ubicaciones);
                if ($Aeropuerto->ubicaciones->count() == 0) {
                    $msg = __("EL Aeropuerto no Posee Asignado Ninguna Ubicación");
                    return view('errors.general', compact('msg'));
                }


                return view('vuelos.position_vuelos', compact('Vuelo', 'Aeropuerto'));
            } else {
                if (\Request::isMethod('post')) {
                    $Vuelos = Vuelos::find(Encryptor::decrypt($id));
                    $Vuelos->puesto_id = Encryptor::decrypt($request->puesto);
                    $Vuelos->save();

                    $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelos->aeronave_id)->update(["aeronave_id" => null]);

                    $Puesto = \App\Models\Puestos::find(Encryptor::decrypt($request->puesto));
                    $Puesto->aeronave_id = $Vuelos->aeronave_id;
                    $Puesto->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Procesado Correctamente";
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function disable_vuelo($id, $back = 'get_vuelos') {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $back = route($back);
                $Vuelo = Vuelos::find(Encryptor::decrypt($id));
                return view('vuelos.disable_vuelos', compact('Vuelo', 'back'));
            } else {
                if (\Request::isMethod('post')) {
                    $data = \Request::all();
                    $Vuelos = Vuelos::find(Encryptor::decrypt($id));
                    $Vuelos->activo = false;
                    $Vuelos->motivo_anulacion = $data['motivo_anulacion'];
                    $Vuelos->save();

                    $Puestos = \App\Models\Puestos::where("aeronave_id", $Vuelos->aeronave_id)->update(["aeronave_id" => null]);

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Procesado Correctamente";
                    return $result;
                } else {
                    exit;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function exonera_vuelo($id) {
        if (\Request::ajax()) {

            $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($id));
            $Vuelo->exonerado = true;
            $Vuelo->save();

            $VueloDetalle = \App\Models\VuelosDetalle::where("vuelo_id", Encryptor::decrypt($id));
            $VueloDetalle->delete();

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            $result['data'] = null;
            return $result;
        }
    }

    public function aprobar($id) {
        if (\Request::ajax()) {

            $Vuelo = \App\Models\Vuelos::find(Encryptor::decrypt($id));
            $Vuelo->aprobado = true;
            $Vuelo->save();

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            $result['data'] = null;
            return $result;
        }
    }

    public function delete_item($id) {
        if (\Request::ajax()) {

            $Detalle = \App\Models\VuelosDetalle::find(Encryptor::decrypt($id))->delete();

            $result['status'] = 1;
            $result['type'] = 'success';
            $result['message'] = __('Processed Correctly');
            $result['data'] = null;
            return $result;
        }
    }

    public function edit_status_vuelo() {
        if (\Request::ajax()) {
            if (\Request::isMethod('get')) {
                $where [] = ['activo', true];
                $where [] = ['fecha_operacion', date('Y-m-d')];

                if (Auth::user()->aeropuerto_id != null) {
                    $where [] = ['aeropuerto_id', Auth::user()->aeropuerto_id];
                }

                $datos = Vuelos::where($where)
                        ->orderBy("id", "desc")
                        ->get();

                $data = Datatables::of($datos)
                        ->addIndexColumn()
                        ->addColumn('class', function ($row) {
                            $class = "";
                            if ($row->aprobado == true) {
                                $class = "alert-info";
                            }
                            return $class;
                        })
                        ->addColumn('action', function ($row) {
                            $actionBtn = "";

                            if ($row->aprobado == true) {
                                $actionBtn .= '<a data-toggle="tooltip" onClick="editStatusVuelo(\'' . $row->crypt_id . '\', \'' . $row->aeronave->full_nombre . '\', \'' . $row->piloto->full_name . '\')" href="javascript:void(0)" title="Editar el Estatus del Vuelo" class=" btn btn-primary btn-sm"> <li class="fa fa-edit"></li></a> ';
                            } else {
                                
                            }
                            return $actionBtn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                $data = $data->original['data'];
                return view('vuelos.change_status_vuelos', compact('data'));
            } else {
                if (\Request::isMethod('post')) {
                    $Validator = \Validator::make(
                                    \Request::all(), [
                                'id' => 'required',
                                'status' => 'required',
                                    ], [
                                'id.required' => 'No se Detecto Vuelo',
                                'status.required' => 'Campo "Estatus" es Requerido',
                                    ]
                    );

                    if ($Validator->fails()) {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = $Validator->errors()->first();
                    } else {
                        $request = \Request::all();
                        $Vuelo = Vuelos::find(\App\Helpers\Encryptor::decrypt($request['id']));
                        if (Auth::user()->aeropuerto_id != null) {
                            if (Auth::user()->aeropuerto_id != $Vuelo->aeropuerto_id) {
                                $result['status'] = 0;
                                $result['type'] = 'error';
                                $result['message'] = "Error en la Permisología";

                                return $result;
                            }
                        }
                        $Vuelo->aprobado = $request['status'];
                        $Vuelo->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = "Vuelo Modificado Correctamente";
                    }


                    return $result;
                }
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

    public function get_lista_vuelos() {
        if (\Request::ajax()) {
            //dd(\Request::all());
            
            //dd($this->getDatesForparking('2023-04-30', '08:00:00', '2023-05-07', '16:00:00'));
            /*
            $horas = DB::table('horas')->get();
            foreach($horas as $value){
                $h1 =explode(" ", $value->f1);
                $h2 =explode(" ", $value->f2);
                      
                $r = $this->getDatesForparking($h1[0], $h1[1], $h2[0], $h2[1]);
                
                DB::statement("UPDATE horas SET cant2 = ".$r['total']." where id =".$value->id);
            }
            */
            
            
            
            
            
            

            $input = \Request::all()['search'];
            $where [] = [DB::raw("1"), "1"];

            if (\Request::all()['aeropuerto'] != null) {
                $params = Encryptor::decrypt(\Request::all()['aeropuerto']);
                $where [] = ["aeropuerto_id", $params];
            }
            if (\Request::all()['nave'] != null) {
                $params = Encryptor::decrypt(\Request::all()['nave']);
                $where [] = ["aeronave_id", $params];
            }
            if (\Request::all()['fechas'] != null) {
                $params = \Request::all()['fechas'];
                $where [] = [function ($q) use ($params) {
                        $fechas = explode(" - ", $params);
                        $f1[0] = $this->saveDate($fechas[0]);
                        $f1[1] = $this->saveDate($fechas[1]);
                        $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                    }];
            }



            if (Auth::user()->aeropuerto_id != null) {
                $datos = Vuelos::whereIn('aeropuerto_id', Auth::user()->lista_aeropuertos)
                        ->where($where)
                        ->limit(100)
                        ->orderBy("id", "desc")
                        ->get();
            } else {
                $datos = Vuelos::where($where)
                        ->limit(100)
                        ->orderBy("id", "desc")
                        ->get();
            }

            $data = Datatables::of($datos)
                    ->addIndexColumn()
                    ->filter(function ($instance) use ($input) {
                        if ($input != null) {
                            $instance->collection = $instance->collection->filter(function ($row) use ($input) {
                                if (Str::contains(Str::lower($row['aeronave']['full_nombre_tn']), Str::lower($input))) {
                                    return true;
                                } else {
                                    if (Str::contains(Str::lower($row['piloto']['full_name']), Str::lower($input))) {
                                        return true;
                                    } else {
                                        if (Str::contains(Str::lower($row['aeropuerto']['full_nombre']), Str::lower($input))) {
                                            return true;
                                        } else {
                                            if (Str::contains(Str::lower($row['fecha_operacion2']), Str::lower($input))) {
                                                return true;
                                            } else {
                                                
                                            }
                                        }
                                    }
                                }
                                return false;
                            });
                        }
                    })
                    ->addColumn('class', function ($row) {
                        $class = "";
                        if ($row->activo == false) {
                            $class = "alert-danger";
                        } else {
                            if ($row->exonerado == true) {
                                $class = "alert-warning";
                            } else {

                                if ($row->aprobado == true) {

                                    if ($row->proforma_id != null) {
                                        $class = "alert-info";
                                    } else {
                                        $class = "alert-success";
                                    }
                                } else {
                                    
                                }
                            }
                        }
                        return $class;
                    })
                    ->addColumn('action', function ($row) {
                        $actionBtn = "";

                        $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.see', $row->crypt_id) . '" title="Ver Vuelo" class=" btn btn-success btn-sm"> <li class="fa fa-eye"></li> </a> ';
                        if ($row->activo == true) {
                            if ($row->aprobado == false) {
                                $ToDay = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
                                $dateReg = Carbon::createFromFormat('Y-m-d', $row->fecha_operacion);
                                if ($resp = true) {
                                    if ($row->factura_id == null && $row->proforma_id == null) {
                                        $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.edit', $row->crypt_id) . '" title="Editar Vuelo" class=" btn btn-primary btn-sm"> <li class="fa fa-edit"></li> </a> ';

                                        $actionBtn .= '<a onClick="exonerarVuelo(\'' . route('get_vuelos.exonera_vuelo', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Exonerar Vuelo" class=" btn btn-warning btn-sm "> <li class="fa fa-pause"></li> </a> ';
                                        if ($row->exonerado == false) {
                                            $actionBtn .= '<a onClick="aprobarVuelo(\'' . route('get_vuelos.aprobar', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Aprobar Vuelo" class=" btn btn-info btn-sm "> <li class="fa fa-check"></li></a> ';

                                            $actionBtn .= '<a onClick="seeDetalleVuelo(\'' . route('view_detail_fly', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Ver Servicios Aplicados" class=" btn btn-secondary btn-sm"> <li class="fa fa-cogs"></li> </a> ';
                                        }

                                        $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.disable', $row->crypt_id) . '" title="Anular Vuelo" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> </a> ';
                                    } else {
                                        //dd($row->factura_id);
                                        if ($row->factura_id != null) {
                                            $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li></a> ';
                                        }
                                    }
                                } else {
                                    //dd($row->factura_id);
                                    if ($row->factura_id != null) {
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li></a> ';
                                    }
                                }
                            } else {
                                if ($row->factura_id != null) {
                                    $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-primary btn-sm"> <li class="fa fa-file-invoice"></li></a> ';
                                }
                            }
                        }
                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
            //->make(true)

            ;
            // dd($data);

            /*      //  dd("sss");
              $data = $data->toJson();
              foreach($data as $key=>$value){
              dd($value->aeropuerto);

              }
             */
            return $data->toJson();
        }
    }

    
      public function get_lista_vuelos2() {
        if (\Request::ajax()) {
            
            $input = \Request::all()['search'];
            $where [] = [DB::raw("1"), "1"];

            if (\Request::all()['aeropuerto'] != null) {
                $params = Encryptor::decrypt(\Request::all()['aeropuerto']);
                $where [] = ["aeropuerto_id", $params];
            }
            if (\Request::all()['nave'] != null) {
                $params = Encryptor::decrypt(\Request::all()['nave']);
                $where [] = ["aeronave_id", $params];
            }
            if (\Request::all()['fechas'] != null) {
                $params = \Request::all()['fechas'];
                $where [] = [function ($q) use ($params) {
                        $fechas = explode(" - ", $params);
                        $f1[0] = $this->saveDate($fechas[0]);
                        $f1[1] = $this->saveDate($fechas[1]);
                        $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                    }];
            }



            if (Auth::user()->aeropuerto_id != null) {
                $datos = Vuelos::whereIn('aeropuerto_id', Auth::user()->lista_aeropuertos)
                        ->where($where)
                        ->limit(100)
                        ->orderBy("id", "desc")
                        ->get();
            } else {
                $datos = Vuelos::where($where)
                        ->limit(100)
                        ->orderBy("id", "desc")
                        ->get();
            }

            $data = Datatables::of($datos)
                    ->addIndexColumn()
                    ->filter(function ($instance) use ($input) {
                        if ($input != null) {
                            $instance->collection = $instance->collection->filter(function ($row) use ($input) {
                                if (Str::contains(Str::lower($row['aeronave']['full_nombre_tn']), Str::lower($input))) {
                                    return true;
                                } else {
                                    if (Str::contains(Str::lower($row['piloto']['full_name']), Str::lower($input))) {
                                        return true;
                                    } else {
                                        if (Str::contains(Str::lower($row['aeropuerto']['full_nombre']), Str::lower($input))) {
                                            return true;
                                        } else {
                                            if (Str::contains(Str::lower($row['fecha_operacion2']), Str::lower($input))) {
                                                return true;
                                            } else {
                                                
                                            }
                                        }
                                    }
                                }
                                return false;
                            });
                        }
                    })
                    ->addColumn('class', function ($row) {
                        $class = "";
                        if ($row->activo == false) {
                            $class = "alert-danger";
                        } else {
                            if ($row->exonerado == true) {
                                $class = "alert-warning";
                            } else {

                                if ($row->aprobado == true) {

                                    if ($row->proforma_id != null) {
                                        $class = "alert-info";
                                    } else {
                                        $class = "alert-success";
                                    }
                                } else {
                                    
                                }
                            }
                        }
                        return $class;
                    })
                    ->addColumn('action', function ($row) {
                        $actionBtn = "";

                        $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.see', $row->crypt_id) . '" title="Ver Vuelo" class=" btn btn-success btn-sm"> <li class="fa fa-eye"></li> </a> ';
                        if ($row->activo == true) {
                            if ($row->aprobado == false) {
                                $ToDay = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
                                $dateReg = Carbon::createFromFormat('Y-m-d', $row->fecha_operacion);
                                if ($resp = true) {
                                    if ($row->factura_id == null && $row->proforma_id == null) {
                                        $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.edit', $row->crypt_id) . '" title="Editar Vuelo" class=" btn btn-primary btn-sm"> <li class="fa fa-edit"></li> </a> ';

                                        $actionBtn .= '<a onClick="exonerarVuelo(\'' . route('get_vuelos.exonera_vuelo', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Exonerar Vuelo" class=" btn btn-warning btn-sm "> <li class="fa fa-pause"></li> </a> ';
                                        if ($row->exonerado == false) {
                                            $actionBtn .= '<a onClick="aprobarVuelo(\'' . route('get_vuelos.aprobar', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Aprobar Vuelo" class=" btn btn-info btn-sm "> <li class="fa fa-check"></li></a> ';

                                            $actionBtn .= '<a onClick="seeDetalleVuelo(\'' . route('view_detail_fly', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Ver Servicios Aplicados" class=" btn btn-secondary btn-sm"> <li class="fa fa-cogs"></li> </a> ';
                                        }

                                        $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.disable', $row->crypt_id) . '" title="Anular Vuelo" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> </a> ';
                                    } else {
                                        //dd($row->factura_id);
                                        if ($row->factura_id != null) {
                                            $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li></a> ';
                                        }
                                    }
                                } else {
                                    //dd($row->factura_id);
                                    if ($row->factura_id != null) {
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li></a> ';
                                    }
                                }
                            } else {
                                if ($row->factura_id != null) {
                                    $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-primary btn-sm"> <li class="fa fa-file-invoice"></li></a> ';
                                }
                            }
                        }
                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
            //->make(true)

            ;
            // dd($data);

            /*      //  dd("sss");
              $data = $data->toJson();
              foreach($data as $key=>$value){
              dd($value->aeropuerto);

              }
             */
            return $data->toJson();
        }
    }
    
    public function get_vuelos($aeropuerto_id = null, $fechas = null, $aeronave_id = null) {
        $request = new Request();
        if (\Request::isMethod('get') && \Request::ajax()) {
            $Pilotos = \App\Models\Pilotos::where("activo", 1)->orderBy("nombres")->get()->pluck("full_name", "crypt_id");
            $Aeronaves = \App\Models\Aeronaves::where("activo", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

            $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                    ->where("pertenece_baer", true)
                    ->where("activo", true)
                    ->whereIn("id", Auth::user()->lista_aeropuertos)
                    ->get()
                    ->pluck("full_nombre", "crypt_id");

            /*
              $name_route = \Request::route()->getName();
              $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
              $where [] = [DB::raw("1"), "1"];
              if ($aeropuerto_id != null) {
              $where [] = ['aeropuerto_id', Encryptor::decrypt($aeropuerto_id)];
              }
              if ($aeronave_id != null) {
              $where [] = ['aeronave_id', Encryptor::decrypt($aeronave_id)];
              }
              if ($fechas != null) {
              $fechas = explode(" : ", $fechas);
              $f1[0] = $fechas[0];
              $f1[1] = $fechas[1];
              $where[] = [
              function ($q) use ($f1) {
              $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
              }
              ];
              $fechas = showDate($f1[0]) . ' - ' . showDate($f1[1]);
              //$where2[] = ['fecha_operacion', saveDate($request->fecha_operacion)];
              } else {

              $fechas = \Carbon\Carbon::now()->firstOfMonth()->format('d/m/Y') . ' - ' . \Carbon\Carbon::now()->endOfMonth()->format('d/m/Y');
              }
              //dd($where);
              if (Auth::user()->aeropuerto_id != null) {
              $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
              ->where("pertenece_baer", true)
              ->where("id", Auth::user()->aeropuerto_id)
              ->get()
              ->pluck("full_nombre", "crypt_id");
              $datos = Vuelos::where('aeropuerto_id', Auth::user()->aeropuerto_id)
              ->where($where)
              ->limit(200)
              ->orderBy("id", "desc")
              ->get();
              } else {
              $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
              ->where("pertenece_baer", true)
              ->get()
              ->pluck("full_nombre", "crypt_id");
              $datos = Vuelos::where($where)
              ->limit(200)
              ->orderBy("id", "desc")
              ->get();
              }
              $data = Datatables::of($datos)
              ->addIndexColumn()
              ->addColumn('class', function ($row) use ($route_id) {
              $class = "";
              if ($row->activo == false) {
              $class = "alert-danger";
              } else {
              if ($row->exonerado == true) {
              $class = "alert-warning";
              } else {

              if ($row->aprobado == true) {

              if ($row->proforma_id != null) {
              $class = "alert-info";
              } else {
              $class = "alert-success";
              }
              } else {

              }
              }
              }
              return $class;
              })
              ->addColumn('action', function ($row) use ($route_id) {
              $actionBtn = "";

              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.see', $row->crypt_id) . '" title="Ver Vuelo" class=" btn btn-success btn-sm"> <li class="fa fa-eye"></li> </a> ';
              if ($row->activo == true) {

              if ($row->aprobado == false) {



              $ToDay = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
              $dateReg = Carbon::createFromFormat('Y-m-d', $row->fecha_operacion);

              if ($resp = true) {
              //if ($ToDay->eq($dateReg)) {

              if ($row->factura_id == null && $row->proforma_id == null) {


              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.edit', $row->crypt_id) . '" title="Editar Vuelo" class=" btn btn-primary btn-sm"> <li class="fa fa-edit"></li> </a> ';

              $actionBtn .= '<a onClick="exonerarVuelo(\'' . route('get_vuelos.exonera_vuelo', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Exonerar Vuelo" class=" btn btn-warning btn-sm "> <li class="fa fa-pause"></li> </a> ';
              if ($row->exonerado == false) {
              $actionBtn .= '<a onClick="aprobarVuelo(\'' . route('get_vuelos.aprobar', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Aprobar Vuelo" class=" btn btn-info btn-sm "> <li class="fa fa-check"></li></a> ';

              $actionBtn .= '<a onClick="seeDetalleVuelo(\'' . route('view_detail_fly', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Ver Servicios Aplicados" class=" btn btn-secondary btn-sm"> <li class="fa fa-cogs"></li> </a> ';
              }

              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.disable', $row->crypt_id) . '" title="Anular Vuelo" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> </a> ';


              } else {
              //dd($row->factura_id);
              if ($row->factura_id != null) {
              $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li></a> ';
              }
              }
              } else {
              //dd($row->factura_id);
              if ($row->factura_id != null) {
              $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li></a> ';
              }
              }
              } else {
              if ($row->factura_id != null) {
              $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-primary btn-sm"> <li class="fa fa-file-invoice"></li></a> ';
              }
              }
              }





              return $actionBtn;
              })
              ->rawColumns(['action'])
              ->make(true);
              $data = $data->original['data'];
             */
            return view('vuelos.get_vuelos', compact('Pilotos', 'Aeronaves', 'Aeropuertos'));
        } else {
            if (\Request::isMethod('post') && \Request::ajax()) {
                $name_route = \Request::route()->getName();
                $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
                $where [] = [DB::raw("1"), 1];
                $request = \Request::all();

                if ($request['aeropuerto_id'] != null) {
                    $where [] = ['aeropuerto_id', Encryptor::decrypt($request['aeropuerto_id'])];
                }
                if ($request['aeronave_id'] != null) {
                    $where [] = ['aeronave_id', Encryptor::decrypt($request['aeronave_id'])];
                }
                if (isset($request['piloto_id'])) {
                    if ($request['piloto_id'] != null) {
                        $where [] = ['piloto_id', Encryptor::decrypt($request['piloto_id'])];
                    }
                }
                if ($request['fecha_operacion'] != null) {
                    $fechas = explode(" - ", $request['fecha_operacion']);
                    $f1[0] = $this->saveDate($fechas[0]);
                    $f1[1] = $this->saveDate($fechas[1]);
                    $where[] = [
                        function ($q) use ($f1) {
                            $q->whereBetween(DB::raw('CAST(fecha_operacion AS date)'), [$f1[0], $f1[1]]);
                        }
                    ];

                    //$where2[] = ['fecha_operacion', saveDate($request->fecha_operacion)];
                }
                if (Auth::user()->aeropuerto_id != null) {
                    $datos = Vuelos::where('aeropuerto_id', Auth::user()->aeropuerto_id)
                            ->where($where)
                            ->orderBy("id", "desc")
                            ->get();
                } else {
                    $datos = Vuelos::where($where)
                            ->orderBy("id", "desc")
                            ->get();
                }


                $data = Datatables::of($datos)
                        ->addIndexColumn()
                        ->addColumn('class', function ($row) use ($route_id) {
                            $class = "";
                            if ($row->activo == false) {
                                $class = "alert-danger";
                            } else {
                                if ($row->exonerado == true) {
                                    $class = "alert-warning";
                                } else {

                                    if ($row->aprobado == true) {

                                        if ($row->proforma_id != null) {
                                            $class = "alert-info";
                                        } else {
                                            $class = "alert-success";
                                        }
                                    } else {
                                        
                                    }
                                }
                            }
                            return $class;
                        })
                        ->addColumn('action', function ($row) use ($route_id) {
                            $actionBtn = "";

                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.see', $row->crypt_id) . '" title="Ver Vuelo" class=" btn btn-success btn-sm"> <li class="fa fa-eye"></li> </a> ';

                            if ($row->activo == true) {
                                if ($row->aprobado == false) {


                                    $ToDay = Carbon::createFromFormat('Y-m-d', date('Y-m-d'));
                                    $dateReg = Carbon::createFromFormat('Y-m-d', $row->fecha_operacion);

                                    //if ($ToDay->eq($dateReg)) {
                                    if ($resp = true) {

                                        if ($row->factura_id == null && $row->proforma_id == null) {
                                            if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                                                $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.edit', $row->crypt_id) . '" title="Editar Vuelo" class=" btn btn-primary btn-sm"> <li class="fa fa-edit"></li> </a> ';
                                            }

                                            $actionBtn .= '<a onClick="exonerarVuelo(\'' . route('get_vuelos.exonera_vuelo', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Exonerar Vuelo" class=" btn btn-warning btn-sm "> <li class="fa fa-pause"></li> </a> ';
                                            if ($row->exonerado == false) {
                                                $actionBtn .= '<a onClick="aprobarVuelo(\'' . route('get_vuelos.aprobar', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Aprobar Vuelo" class=" btn btn-info btn-sm "> <li class="fa fa-check"></li> </a> ';
                                                $actionBtn .= '<a onClick="seeDetalleVuelo(\'' . route('view_detail_fly', $row->crypt_id) . '\')" href="javascript:void(0)" data-toggle="tooltip" title="Ver Servicios Aplicados" class=" btn btn-secondary btn-sm"> <li class="fa fa-cogs"></li> </a> ';
                                            }

                                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.disable', $row->crypt_id) . '" title="Anular Vuelo" class=" btn btn-danger btn-sm"><li class="fa fa-trash"></li> </a> ';

                                            /*
                                              if ($row->tipo_operacion_id == 1) { //SI ES UN ARRIBO
                                              if (in_array("position", Auth::user()->getActions()[$route_id])) {
                                              $Aeropuerto = \App\Models\Aeropuertos::find($row->aeropuerto_id);
                                              //dd($Aeropuerto->Ubicaciones->count()));
                                              if ($Aeropuerto->Ubicaciones->count() > 0) {
                                              $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('get_vuelos.position', $row->crypt_id) . '" title="Agregar Puesto" class=" btn btn-success btn-sm"><li class="fa fa-home"></li> </a> ';
                                              } else {

                                              $actionBtn .= '<a  href="javascript:void(0)" title="El Aeropuerto no Posee Puestos Registrados" class=" btn btn-success btn-sm disabled"><li class="fa fa-home"></li> </a> ';
                                              }
                                              }
                                              }
                                             * 
                                             */
                                        } else {

                                            if ($row->factura_id != null) {
                                                $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li> </a> ';
                                            }
                                        }
                                    } else {

                                        if ($row->factura_id != null) {
                                            $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-success btn-sm"> <li class="fa fa-check"></li> </a> ';
                                        }
                                    }
                                } else {
                                    if ($row->factura_id != null) {
                                        $actionBtn .= '<a data-toggle="tooltip" onClick="seetProforma(\'' . route('view_fact', Encryptor::encrypt($row->factura_id)) . '\')" href="javascript:void(0)" title="Ver Factura" class=" btn btn-primary btn-sm"> <li class="fa fa-file-invoice"></li> </a> ';
                                    }
                                }
                            }

                            return $actionBtn;
                        })
                        ->rawColumns(['action', 'class'])
                        ->make(true);
                $data = $data->original['data'];
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $data;

                return $result;
            } else {
                if (\Request::isMethod('put')) {
                    //return Excel::download(new VuelosExport(), 'VUELOS.xlsx');
                    $e = new \App\Exports\VuelosExportExcel();
                    return $e->fabrica();
                } else {
                    return Redirect::to('/dashboard');
                }
            }
        }
    }

    public function get_vuelos_fact($id, $id2 = null, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('get')) {

                $Vuelo1 = Vuelos::find(Encryptor::decrypt($id));
                //dd(json_decode($Vuelo1->prodservicios_extra));
                $prodservicios_extra = [];
                foreach (json_decode($Vuelo1->prodservicios_extra) as $value) {
                    foreach ($value as $key => $value2) {
                        $prodservicios_extra[$key . ":" . Encryptor::encrypt($value2)] = $key;
                    }
                }






                if (Auth::user()->aeropuerto_id != null) {
                    $datos['aeropuerto_id'] = Auth::user()->aeropuerto_id;
                } else {
                    $datos['aeropuerto_id'] = $Vuelo1->aeropuerto_id;
                }
                if ($id2 != null) {

                    $Vuelo2 = Vuelos::find(Encryptor::decrypt($id2));
                    foreach (json_decode($Vuelo2->prodservicios_extra) as $value) {
                        foreach ($value as $key => $value2) {
                            if (isset($prodservicios_extra[$key . ":" . Encryptor::encrypt($value2)])) {
                                $cant = $key + $prodservicios_extra[$key . ":" . Encryptor::encrypt($value2)];
                                unset($prodservicios_extra[$key . ":" . Encryptor::encrypt($value2)]);
                                $prodservicios_extra[$cant . ":" . Encryptor::encrypt($value2)] = $cant;
                            } else {
                                $prodservicios_extra[$key . ":" . Encryptor::encrypt($value2)] = $key;
                            }
                        }
                    }
                    if ($Vuelo2->tipo_operacion_id == 2) {//SI ES UN DESPEGUE
                        $datos['piloto_id'] = $Vuelo2->piloto_id;
                        $datos['fecha_operacion'] = $Vuelo2->fecha_operacion;
                        $datos['hora_operacion'] = $Vuelo2->hora_operacion;
                        $datos['cant_pasajeros'] = $Vuelo2->cant_pasajeros;

                        $datos['hora_llegada'] = showDate($Vuelo1->fecha_operacion) . ' ' . $Vuelo1->hora_operacion;
                        $datos['hora_salida'] = showDate($Vuelo2->fecha_operacion) . ' ' . $Vuelo2->hora_operacion;

                        //dd($Vuelo2->prodservicios_extra);
                    } else {

                        $datos['piloto_id'] = $Vuelo1->piloto_id;
                        $datos['fecha_operacion'] = $Vuelo1->fecha_operacion;
                        $datos['hora_operacion'] = $Vuelo1->hora_operacion;
                        $datos['cant_pasajeros'] = $Vuelo1->cant_pasajeros;

                        $datos['hora_salida'] = showDate($Vuelo1->fecha_operacion) . ' ' . $Vuelo1->hora_operacion;
                        $datos['hora_llegada'] = showDate($Vuelo2->fecha_operacion) . ' ' . $Vuelo2->hora_operacion;
                    }
                } else {
                    $datos['piloto_id'] = $Vuelo1->piloto_id;
                    $datos['fecha_operacion'] = $Vuelo1->fecha_operacion;
                    $datos['hora_operacion'] = $Vuelo1->hora_operacion;
                    $datos['cant_pasajeros'] = $Vuelo1->cant_pasajeros;

                    $datos['hora_llegada'] = showDate($Vuelo1->fecha_operacion) . ' ' . $Vuelo1->hora_operacion;
                    $datos['hora_salida'] = showDate($Vuelo1->fecha_operacion) . ' ' . $Vuelo1->hora_operacion;
                }


                $datos['prodservicios_extra'] = $prodservicios_extra;
                $datos['aeronave_id'] = $Vuelo1->aeronave_id;

                $datos['aeropuerto_id'] = Encryptor::encrypt($datos['aeropuerto_id']);
                $datos['piloto_id'] = Encryptor::encrypt($datos['piloto_id']);
                $datos['aeronave_id'] = Encryptor::encrypt($datos['aeronave_id']);
                $datos['fecha_operacion'] = showDate($datos['fecha_operacion']);

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');
                $result['data'] = $datos;

                return $result;
            }
        } else {
            return Redirect::to('/dashboard');
        }
    }

}
