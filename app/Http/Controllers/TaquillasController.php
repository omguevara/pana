<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use Illuminate\Support\Str;
use App\Models\Taquillas;

class TaquillasController extends Controller {

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Taquillas_active = Taquillas::where("activo", 1)->get();

            $Taquillas_disable = Taquillas::where("activo", 0)->get();
            //dd($Taquillas_active->toArray());
            $Taquillas_active = Datatables::of($Taquillas_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('taquillas.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('taquillas.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                        }

                        if (in_array("add_points", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('taquillas.add_points', $row->crypt_id) . '" class="actions_users btn btn-success btn-sm"><li class="fa fa-cash-register"></li> ' . __('Agregar Puntos') . '</a> ';
                        }




                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $Taquillas_disable = Datatables::of($Taquillas_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('taquillas.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Taquillas_active->original['data'];
            $data2 = $Taquillas_disable->original['data'];

            // dd($data2);

            return view('taquillas.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'codigo' => 'required',
                            'nombre' => 'required',
                            'aeropuerto_id' => 'required',
                            'seguridad' => 'required',
                            'numero_factura' => 'required',
                            'numero_control' => 'required'
                                ], [
                            'codigo.required' => __('El Codigo es Requerido'),
                            'nombre.unique' => __('El Nombre es Requerido'),
                            'aeropuerto_id.required' => __('El Aeropuerto es Requerido'),
                            'seguridad.required' => __('El Hash es Requerido'),
                            'numero_factura.required' => __('El Numero de Factura es Requerido'),
                            'numero_control.required' => __('El Numero de Control es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Taquillas = new Taquillas();
                    $Taquillas->codigo = $request->codigo;
                    $Taquillas->nombre = $request->nombre;
                    $Taquillas->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                    $Taquillas->seguridad = $request->seguridad;
                    $Taquillas->numero_factura = $request->numero_factura;
                    $Taquillas->numero_control = $request->numero_control;

                    $Taquillas->modelo_factura = 'reportes.facturas.formato_general';

                    $Taquillas->user_id = Auth::user()->id;
                    $Taquillas->ip = $this->getIp();
                    $Taquillas->fecha_registro = now();

                    $Taquillas->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                $hash = Str::random(40);
                $aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->where("pertenece_baer", 1)->orderBy("nombre")->get()->pluck("full_nombre", "crypt_id");

                return view('taquillas.create', compact('hash', 'aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {



                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            'seguridad' => 'required',
                            'numero_factura' => 'required',
                            'numero_control' => 'required'
                                ], [
                            'nombre.unique' => __('El Nombre es Requerido'),
                            'seguridad.required' => __('El Hash es Requerido'),
                            'numero_factura.required' => __('El Numero de Factura es Requerido'),
                            'numero_control.required' => __('El Numero de Control es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                    $Taquillas->nombre = $request->nombre;
                    $Taquillas->seguridad = $request->seguridad;
                    $Taquillas->numero_factura = $request->numero_factura;
                    $Taquillas->numero_control = $request->numero_control;

                    $Taquillas->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                $hash = Str::random(40);
                $aeropuertos = \App\Models\Aeropuertos::where("activo", 1)->where("pertenece_baer", 1)->orderBy("nombre")->get()->pluck("nombre", "crypt_id");
                return view('taquillas.edit', compact('Taquillas', 'hash', 'aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                $Taquillas->activo = 0;

                $Taquillas->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                return view('taquillas.disable', compact('Taquillas'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                $Taquillas->activo = 1;

                $Taquillas->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Taquillas = Taquillas::find(Encryptor::decrypt($id));
                return view('taquillas.active', compact('Taquillas'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
