<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Sesion;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use DataTables;
use PDF;
use App\Models\Clientes;

class ClientesController extends Controller {

    public function get_data_cliente($tipo_doc, $doc) {
        if (\Request::ajax()) {

            $Cliente = Clientes::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();

            if ($Cliente == null) {

                $Cliente = \App\Models\Pilotos::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();
                if ($Cliente == null) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Datos No Encontrado');
                    $result['data'] = null;
                } else {
                    $C = new \App\Models\Clientes();
                    $C->tipo_documento = $tipo_doc;
                    $C->documento = $doc;

                    $C->razon_social = Upper($Cliente->nombres . ' ' . $Cliente->apellidos);
                    $C->telefono = ($Cliente->telefono==null ? "":$Cliente->telefono);
                    $C->correo = ($Cliente->correo==null ? "":$Cliente->correo);
                    $C->direccion = "";
                    
                    $C->tipo_id = 4; // general

                    $C->user_id = Auth::user()->id;
                    $C->ip = $this->getIp();
                    $C->fecha_registro = now();
                    $C->save();

                    $Cliente = $C;

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Datos Encontrados');
                    $result['data'] = $Cliente->toArray();
                }
            } else {
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Datos Encontrados');
                $result['data'] = $Cliente->toArray();
            }
            return response()->json($result);
        }
    }

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Clientes_active = Clientes::get();

            $Clientes_active = Datatables::of($Clientes_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('clientes.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                       




                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Clientes_active->original['data'];

            // dd($data2);

            return view('clientes.index', compact('data1', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'razon' => 'required',
                            'phone' => 'required',
                            'correo' => 'required',
                            'direccion' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'razon.required' => __('El nombre es Requerido'),
                            'phone.required' => __('El Teléfono es Requerido'),
                            'correo.required' => __('El correo es Requerido'),
                            'direccion.required' => __('La dirección es Requerido'),        
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Clientes = \App\Models\Clientes::where("documento", $request->document)->count();

                    if ($Clientes == 0) {



                        $Clientes = new \App\Models\Clientes();
                        $Clientes->tipo_documento = $request->type_document;
                        $Clientes->documento = $request->document;
                        $Clientes->razon_social = Upper($request->razon);
                        $Clientes->direccion = Upper($request->direccion);
                        $Clientes->telefono = $request->phone;
                        $Clientes->correo = $request->correo;
                        $Clientes->tipo_id = 1;
                        
                        $Clientes->user_id = Auth::user()->id;
                        $Clientes->ip = $this->getIp();
                        $Clientes->fecha_registro = now();
                        
                        $Clientes->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Cliente ya Existe');
                    }
                }
                return $result;
            } else {
                $Clientes = \App\Models\Clientes::where("activo", 1)->get()->pluck('nombre', 'crypt_id');
                return view('clientes.create', compact('Clientes'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {



                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'razon' => 'required',
                            'phone' => 'required',
                            'correo' => 'required',
                            'direccion' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'razon.required' => __('El nombre es Requerido'),
                            'phone.required' => __('El Teléfono es Requerido'),
                            'correo.required' => __('El correo es Requerido'),
                            'direccion.required' => __('La dirección es Requerido'),        
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Clientes = \App\Models\Clientes::where("documento", $request->document)->where("id", "!=", Encryptor::decrypt($id))->count();

                    if ($Clientes == 0) {



                        $Clientes = Clientes::find(Encryptor::decrypt($id));
                        $Clientes->tipo_documento = $request->type_document;
                        $Clientes->documento = $request->document;
                        $Clientes->razon_social = Upper($request->razon);
                        $Clientes->direccion = Upper($request->direccion);
                        $Clientes->telefono = $request->phone;
                        $Clientes->correo = $request->correo;
                        $Clientes->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Procesed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }




                    /*

                      if ($request->file('homologacion') != null) {
                      $file = $request->file('homologacion');
                      // dd($request->img);
                      try {


                      $fileName = "homologacion_" . $Clientes->id . '.' . $file->getClientOriginalExtension();
                      $Clientes->homologacion = $file->getClientOriginalExtension();
                      $Clientes->save();
                      $file->move(storage_path('homologacion'), $fileName);
                      //$request->img->move(public_path('pagos'), $fileName);
                      //$request->img->move(storage_path('homologacion'), $fileName);
                      //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                      //$request->img->move(public_path('pagos'), $fileName);
                      } catch (Exception $ex) {

                      }
                      //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                      }
                     */

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Procesed Correctly');
                }
                return $result;
            } else {

                $Clientes = Clientes::find(Encryptor::decrypt($id));

                return view('clientes.edit', compact('Clientes'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Clientes = Clientes::find(Encryptor::decrypt($id));
                $Clientes->activo = 0;

                $Clientes->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesed Correctly');

                return $result;
            } else {

                $Clientes = Clientes::find(Encryptor::decrypt($id));
                return view('clientes.disable', compact('Clientes'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Clientes = Clientes::find(Encryptor::decrypt($id));
                $Clientes->activo = 1;

                $Clientes->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Procesed Correctly');

                return $result;
            } else {

                $Clientes = Clientes::find(Encryptor::decrypt($id));
                return view('clientes.active', compact('Clientes'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
