<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
use DataTables;
use PDF;
use App\Models\Pilotos;

class PilotosController extends Controller {
    /*     * ******************** */

    public function login(Request $request) {
        
        $Validator = \Validator::make(
                        $request->all(), [
                    'email' => 'required|email',
                    'pwd' => 'required',
                        ], [
                    'email.required' => __('El Correo es Requerido'),
                    'email.email' => __('Error en el Formato de Correo'),
                    'pwd.required' => __('La Contraseña es Requerida'),
                        ]
        );
        if ($Validator->fails()) {
            $result['status'] = 0;
            $result['type'] = 'error';
            $result['message'] = $Validator->errors()->first();
        } else {
            $Piloto = Pilotos::where("correo", 'ILIKE' ,Upper($request->email))->first();
            if ($Piloto != null) {
                if ($Piloto->documento == $request->pwd) {
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = "Aprobado";
                    $result['data'] = $Piloto;
                } else {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = "Datos Errados";
                }
            } else {
                $result['status'] = 0;
                $result['type'] = 'error';
                $result['message'] = "Datos Errados";
            }
        }
        return $result;
    }

    /*     * ********************* */

    public function get($id = 123456) {
        $Piloto = Pilotos::find(40);
        $Vuelos = \App\Models\Vuelos::where("piloto_id", $Piloto->id)->get()->toArray();
        $Piloto = $Piloto->toArray();
        $Piloto['Vuelos'] = $Vuelos;
        return $Piloto;
    }

    public function get_data_piloto($tipo_doc, $doc) {
        if (\Request::ajax()) {

            $Piloto = Pilotos::where("tipo_documento", $tipo_doc)->where("documento", $doc)->first();

            if ($Piloto == null) {
                $result['status'] = 0;
                $result['type'] = 'error';
                $result['message'] = __('Datos No Encontrado');
                $result['data'] = null;
            } else {
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Datos Encontrados');
                $result['data'] = $Piloto->toArray();
            }
            return response()->json($result);
        }
    }

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
            //dd(Auth::user()->getActions()[$route_id]);
            //dd(route('users.edit', 1));
            //dd(in_array("edit", Auth::user()->getActions()[$route_id]));
            $Pilotoss_active = Pilotos::where("activo", 1)->get();

            $Pilotoss_disable = Pilotos::where("activo", 0)->get();

            $Pilotoss_active = Datatables::of($Pilotoss_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('pilotos.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('pilotos.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                        }




                        return $actionBtn;
                    })
                    ->addColumn('homologado', function ($row) {
                        $actionBtn = "";

                        if ($row->homologacion != null) {
                            $actionBtn .= '<a  href="' . route('get_homologacion', $row->crypt_id) . '" class=" btn btn-info btn-sm"><li class="fa fa-arrow-alt-circle-down"></li> ' . __('Homologación') . '</a> ';
                        }


                        return $actionBtn;
                    })
                    ->rawColumns(['action', 'homologado'])
                    ->make(true);

            $Pilotoss_disable = Datatables::of($Pilotoss_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('pilotos.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Pilotoss_active->original['data'];
            $data2 = $Pilotoss_disable->original['data'];

            // dd($data2);

            return view('pilotos.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'name_user' => 'required',
                            'surname_user' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'name_user.required' => __('El nombre es Requerido'),
                            'surname_user.required' => __('El Apellido es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Pilotos = \App\Models\Pilotos::where("documento", $request->document)->count();

                    if ($Pilotos == 0) {



                        $Pilotos = new \App\Models\Pilotos();
                        $Pilotos->tipo_documento = $request->type_document;
                        $Pilotos->documento = $request->document;
                        $Pilotos->nombres = Upper($request->name_user);
                        $Pilotos->apellidos = Upper($request->surname_user);
                        $Pilotos->telefono = $request->phone;
                        $Pilotos->correo = Upper($request->email);

                        $Pilotos->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                    /*
                      if ($request->file('homologacion') != null) {
                      $file = $request->file('homologacion');
                      // dd($request->img);
                      try {


                      $fileName = "homologacion_" . $Pilotos->id . '.' . $file->getClientOriginalExtension();
                      $Pilotos->homologacion = $file->getClientOriginalExtension();
                      $Pilotos->save();
                      $file->move(storage_path('homologacion'), $fileName);
                      //$request->img->move(public_path('pagos'), $fileName);
                      //$request->img->move(storage_path('homologacion'), $fileName);
                      //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                      //$request->img->move(public_path('pagos'), $fileName);
                      } catch (Exception $ex) {

                      }
                      //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                      }
                     */
                }
                return $result;
            } else {
                $Pilotos = \App\Models\Pilotos::where("activo", 1)->get()->pluck('nombre', 'crypt_id');
                return view('pilotos.create', compact('Pilotos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function plus(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'name_user' => 'required',
                            'surname_user' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'name_user.required' => __('El nombre es Requerido'),
                            'surname_user.required' => __('El Apellido es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Pilotos = \App\Models\Pilotos::where("documento", $request->document)->count();

                    if ($Pilotos == 0) {



                        $Pilotos = new \App\Models\Pilotos();
                        $Pilotos->tipo_documento = $request->type_document;
                        $Pilotos->documento = $request->document;
                        $Pilotos->nombres = Upper($request->name_user);
                        $Pilotos->apellidos = Upper($request->surname_user);
                        $Pilotos->telefono = $request->phone;
                        $Pilotos->correo = Upper($request->email);

                        $Pilotos->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                        $result['data'] = $Pilotos->toArray();
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }
                }
                return $result;
            } else {
                exit;
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {



                $Validator = \Validator::make(
                                $request->all(), [
                            'type_document' => 'required',
                            'document' => 'required',
                            'name_user' => 'required',
                            'surname_user' => 'required',
                                ], [
                            'type_document.required' => __('El Tipo de Doc es Requerido'),
                            'document.required' => __('El Documento es Requerida'),
                            'name_user.required' => __('El nombre es Requerido'),
                            'surname_user.required' => __('El Apellido es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Pilotos = \App\Models\Pilotos::where("documento", $request->document)->where("id", "!=", Encryptor::decrypt($id))->count();

                    if ($Pilotos == 0) {



                        $Pilotos = Pilotos::find(Encryptor::decrypt($id));
                        $Pilotos->tipo_documento = $request->type_document;
                        $Pilotos->documento = $request->document;
                        $Pilotos->nombres = Upper($request->name_user);
                        $Pilotos->apellidos = Upper($request->surname_user);
                        $Pilotos->telefono = $request->phone;
                        $Pilotos->correo = Upper($request->email);
                        $Pilotos->save();
                        $result['status'] = 1;
                        $result['type'] = 'success';
                        $result['message'] = __('Processed Correctly');
                    } else {
                        $result['status'] = 0;
                        $result['type'] = 'error';
                        $result['message'] = __('El Piloto ya Existe');
                    }




                    /*

                      if ($request->file('homologacion') != null) {
                      $file = $request->file('homologacion');
                      // dd($request->img);
                      try {


                      $fileName = "homologacion_" . $Pilotos->id . '.' . $file->getClientOriginalExtension();
                      $Pilotos->homologacion = $file->getClientOriginalExtension();
                      $Pilotos->save();
                      $file->move(storage_path('homologacion'), $fileName);
                      //$request->img->move(public_path('pagos'), $fileName);
                      //$request->img->move(storage_path('homologacion'), $fileName);
                      //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                      //$request->img->move(public_path('pagos'), $fileName);
                      } catch (Exception $ex) {

                      }
                      //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                      }
                     */

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $Pilotos = Pilotos::find(Encryptor::decrypt($id));

                return view('pilotos.edit', compact('Pilotos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Pilotos = Pilotos::find(Encryptor::decrypt($id));
                $Pilotos->activo = 0;

                $Pilotos->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Pilotos = Pilotos::find(Encryptor::decrypt($id));
                return view('pilotos.disable', compact('Pilotos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Pilotos = Pilotos::find(Encryptor::decrypt($id));
                $Pilotos->activo = 1;

                $Pilotos->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Pilotos = Pilotos::find(Encryptor::decrypt($id));
                return view('pilotos.active', compact('Pilotos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
