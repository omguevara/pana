<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use App\Models\Hangares;

class HangaresController extends Controller {

    function index() {

        if (\Request::ajax()) {

            $name_route = \Request::route()->getName();
            $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;

            if (Auth::user()->aeropuerto_id == null) {
                $Hangares_active = Hangares::where("activo", 1)->get();
                $Hangares_disable = Hangares::where("activo", 0)->get();
            } else {
                $Hangares_active = Hangares::where("activo", 1)->where('aeropuerto_id', Auth::user()->aeropuerto_id)->get();
                $Hangares_disable = Hangares::where("activo", 0)->where('aeropuerto_id', Auth::user()->aeropuerto_id)->get();
            }



            $Hangares_active = Datatables::of($Hangares_active)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("edit", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('hangares.edit', $row->crypt_id) . '" class="actions_users btn btn-primary btn-sm"> <li class="fa fa-edit"></li> ' . __('Edit') . '</a> ';
                        }

                        if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('hangares.disable', $row->crypt_id) . '" class="actions_users btn btn-danger btn-sm"><li class="fa fa-trash"></li> ' . __('Disable') . '</a> ';
                        }




                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $Hangares_disable = Datatables::of($Hangares_disable)
                    ->addIndexColumn()
                    ->addColumn('action', function ($row) use ($route_id) {
                        $actionBtn = "";

                        if (in_array("active", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="setAjax(this, event)" href="' . route('hangares.active', $row->crypt_id) . '" class=" btn btn-success btn-sm"><li class="fa fa-undo"></li> ' . __('Active') . '</a> ';
                        }



                        return $actionBtn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);

            $data1 = $Hangares_active->original['data'];
            $data2 = $Hangares_disable->original['data'];

            // dd($data2);

            return view('hangares.index', compact('data1', 'data2', 'route_id'));
        } else {
            return Redirect::to('/');
        }
    }

    public function create(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                $Validator = \Validator::make(
                                $request->all(), [
                            // 'nombre' => 'required',
                            // 'propietario' => 'required',
                            // 'ubicacion' => 'required',
                            'aeropuerto_id' => 'required',
                                ], [
                            // 'nombre.required' => __('El Nombre es Requerido'),
                            // 'propietario.required' => __('El Propietario es Requerido'),
                            // 'ubicacion.required' => __('La Ubicación es Requerido'),
                            'aeropuerto_id.required' => __('El Aeropuerto es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Hangares = new Hangares();
                    // $Hangares->nombre = Upper($request->nombre);
                    // $Hangares->propietario = Upper($request->propietario);
                    $Hangares->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                    $Hangares->nombre = Upper($request->hangar);
                    $Hangares->dimension = Upper($request->dimension);
                    $Hangares->razon_social = Upper($request->razon_social);
                    
                    
                    // $Hangares->ubicacion = Upper($request->ubicacion);


                    $Hangares->activo = true;

                    $Hangares->user_id = Auth::user()->id;
                    $Hangares->ip = $this->getIp();
                    $Hangares->fecha_registro = now();

                    $Hangares->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    $Ubicaciones = \App\Models\UbicacionesHangares::where("aeropuerto_id", Auth::user()->aeropuerto_id)
                                    ->get()->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                    $Ubicaciones = \App\Models\UbicacionesHangares::get()->pluck("full_nombre", "crypt_id");
                }

                return view('hangares.create2', compact('Aeropuertos', 'Ubicaciones'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function edit($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {



                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            // 'propietario' => 'required',
                            // 'ubicacion' => 'required',
                            'aeropuerto_id' => 'required',
                                ], [
                            'nombre.required' => __('El Nombre es Requerido'),
                            // 'propietario.required' => __('El Propietario es Requerido'),
                            // 'ubicacion.required' => __('La Ubicación es Requerido'),
                            'aeropuerto_id.required' => __('El Aeropuerto es Requerido'),
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Hangares = Hangares::find(Encryptor::decrypt($id));
                    $Hangares->nombre = Upper($request->nombre);
                    $Hangares->dimension = Upper($request->dimension);
                    $Hangares->razon_social = Upper($request->razon_social);
                    $Hangares->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);


                    $Hangares->user_id = Auth::user()->id;
                    $Hangares->ip = $this->getIp();
                    $Hangares->fecha_registro = now();

                    $Hangares->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {

                $Hangar = Hangares::find(Encryptor::decrypt($id));

                if (Auth::user()->aeropuerto_id != null) {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->where("id", Auth::user()->aeropuerto_id)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                } else {
                    $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                            ->where("pertenece_baer", true)
                            ->get()
                            ->pluck("full_nombre", "crypt_id");
                }

                return view('hangares.edit', compact('Hangar', 'Aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function disable($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Hangares = Hangares::find(Encryptor::decrypt($id));
                $Hangares->activo = 0;

                $Hangares->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Hangares = Hangares::find(Encryptor::decrypt($id));
                return view('hangares.disable', compact('Hangares'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function active($id, Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {


                $Hangares = Hangares::find(Encryptor::decrypt($id));
                $Hangares->activo = 1;

                $Hangares->save();

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = __('Processed Correctly');

                return $result;
            } else {

                $Hangares = Hangares::find(Encryptor::decrypt($id));
                return view('hangares.active', compact('Hangares'));
            }
        } else {
            return Redirect::to('/');
        }
    }

}
