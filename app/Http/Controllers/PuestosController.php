<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;
use App\Helpers\Encryptor;
use Illuminate\Support\Str;
use App\Models\Aeropuertos;
use App\Models\Ubicaciones;
use App\Models\Lugares;
use App\Models\Puestos;


class PuestosController extends Controller
{
     public function puestos($id, Request $request) {


        if (\Request::ajax()) {
            if ($request->isMethod('post')) {

                //dd($request->all());

                $Validator = \Validator::make(
                                $request->all(), [
                            'nombre' => 'required',
                            'lugar_id' => 'required',
                                ], [
                            'nombre.required' => __('El nombre es Requerido'),
                            'lugar_id.required' => __('El Lugar es Requerido')
                                ]
                );

                if ($Validator->fails()) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = $Validator->errors()->first();
                } else {

                    $Puestos = new Puestos();
                    $Puestos->lugar_id = Encryptor::decrypt($request->lugar_id);
                    $Puestos->nombre = Upper($request->nombre);

                    $Puestos->save();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                }
                return $result;
            } else {
                if ($request->isMethod('delete')) {

                    $Puestos = Puestos::find(Encryptor::decrypt($id));
                    $Puestos->activo = false;
                    $Puestos->save();
                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['message'] = __('Processed Correctly');
                    return $result;
                } else {
                    $Aeropuertos = Aeropuertos::find(Encryptor::decrypt($id));
                    $L = Lugares::join("maest_ubicaciones", "maest_ubicaciones.id", "=", "maest_lugares.ubicacion_id")
                            ->where("maest_lugares.activo", 1)
                            ->where("maest_ubicaciones.activo", 1)
                            ->where('maest_ubicaciones.aeropuerto_id', Encryptor::decrypt($id))
                            ->select("maest_lugares.id", "maest_lugares.nombre", "maest_ubicaciones.nombre as ubicacion" ) 
                            ->get();
                    $Lugares = array();
                    foreach($L as $value){
                        $Lugares[$value->crypt_id]= $value->ubicacion.' :: '.$value->nombre;
                    }
                    
                    
                    
                    $Puestos = Puestos::join("maest_lugares", "maest_lugares.id", "=", "maest_puestos.lugar_id")
                            ->join("maest_ubicaciones", "maest_ubicaciones.id", "=", "maest_lugares.ubicacion_id")
                            ->where("maest_puestos.activo", 1)
                            ->where("maest_lugares.activo", 1)
                            ->where("maest_ubicaciones.activo", 1)
                            ->where('maest_ubicaciones.aeropuerto_id', Encryptor::decrypt($id))
                            ->select("maest_puestos.id", "maest_puestos.nombre", "maest_lugares.nombre as lugar", "maest_ubicaciones.nombre as ubicacion")
                            ->get()
                            ->toArray();
                   

                    return view('aeropuertos.puestos', compact('Aeropuertos', 'Lugares', 'Puestos'));
                }
            }
        } else {
            return Redirect::to('/');
        }
    }
}
