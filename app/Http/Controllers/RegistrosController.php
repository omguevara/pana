<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Helpers\Encryptor;
//use Illuminate\Support\Facades\Storage;
use DataTables;
use PDF;
use App\Models\Registros;

class RegistrosController extends Controller {

    public function dosas(Request $request) {

        if (Auth::user()->aeropuerto_id == null) {


            $msg = __('El Usuario no se le ha Asignado un Aeropuerto');
            return view('errors.generic', compact('msg'));
            $where = [[DB::raw("1"), 1]];
        }
        if ($request->isMethod('post')) {
            // dd($request->all());
            $Validator = \Validator::make(
                            $request->all(), [
                        'hora_inicio' => 'required',
                        'destino_id' => 'required',
                        'type_document2' => 'required',
                        'document2' => 'required',
                        'piloto' => 'required',
                        'cant_pasajeros' => 'required',
                        'placa' => 'required',
                        'type_document' => 'required',
                        'document' => 'required',
                        'razon' => 'required',
                        'phone' => 'required',
                        'direccion' => 'required',
                        'money' => 'required|array',
                            ], [
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                        'type_document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'document2.required' => __('Indique el Nombre del Responsable'),
                        'piloto.required' => __('El Nombre del Piloto es Requerido'),
                        'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros'),
                        'placa.required' => __('Debe Indicar la Matricula de la Aeronave'),
                        'type_document.required' => __('El Documento del Responsable es Requerido'),
                        'document.required' => __('El Documento del Responsable es Requerido'),
                        'razon.required' => __('Indique el Nombre del Responsable'),
                        'phone.required' => __('Indique el Teléfono del Responsable'),
                        'direccion.required' => __('Indique la Dirección del Responsable'),
                        'money.required' => __('Indique la Forma de Pago'),
                        'money.array' => __('Formato Errado en la Forma de Pago')
                            ]
            );

            if ($Validator->fails()) {
                $msg = [];
                foreach ($Validator->messages()->all() as $value) {
                    $msg[] = $value;
                }
                $result['status'] = 0;
                $result['type'] = 'error';
                $result['message'] = $Validator->errors()->first();
            } else {


                $Aeronave = \App\Models\Aeronaves::where("matricula", $request->placa)->where("activo", 1)->first();
                /*
                  $isReg = \App\Models\Reservaciones::where('aeronave_id', $Aeronave->id)
                  ->whereDate('fecha', date('Y-m-d'))
                  ->where('hora_inicio', $request->hora_inicio)
                  ->count();

                  if ($isReg == 0) { */

                $DOLAR = $this->getDolar();
                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $Origen = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);

                //$Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));
                //$MonedaPago = $moneda = ($request->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$request->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];

                $data_tasa = $request->all();
                $data_tasa['tipo_vuelo_id'] = 2;
                $data_tasa['origen_id'] = Encryptor::encrypt(Auth::user()->aeropuerto_id);
                $data_tasa['nacionalidad_id'] = 1;
                $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                $tasa = $this->getProdservicios("tasa", $data_tasa);

                /*
                  $data_dosa = $request->all();
                  $data_dosa['tipo_vuelo_id'] = 2;
                  $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));

                  $dosa = $this->getProdservicios("dosa", $data_dosa);
                 */
                $prodservExtra = array();
                $extra = array();
                if (is_array($request->prodservicios)) {
                    foreach ($request->prodservicios as $value) {
                        $prodservExtra[] = Encryptor::decrypt($value);
                    }
                }
                if (count($prodservExtra) > 0) {
                    //dd($prodservExtra);

                    $data_extra = $request->all();
                    $data_extra['tipo_vuelo_id'] = 2;
                    $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                    $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                    //$extra =  $this->getProdservicios("extra", $request);
                }

                $ip = $this->getIp();

                /*                 * ***** CLIENTES******* */
                //$Cliente = \App\Models\Clientes::where('documento', $request->document)->first();
                //if ($Cliente == null) {
                if ($request->cliente_id == null) {

                    $Cliente = \App\Models\Clientes::where('documento', $request->document)->first();

                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                    }



                    $Cliente->tipo_documento = $request->type_document;
                    $Cliente->documento = $request->document;

                    //$Cliente->user_id = Auth::user()->id;
                    $Cliente->user_id = 1;
                    $Cliente->fecha_registro = now();
                    $Cliente->ip = $ip;
                } else {
                    $Cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id));
                }

                $Cliente->direccion = $request->direccion;
                $Cliente->tipo_id = 3;

                $Cliente->razon_social = Upper($request->razon);
                $Cliente->telefono = $request->phone;
                $Cliente->correo = (isset($request->correo) ?: "$request->correo");

                $Cliente->direccion = $request->direccion;
                $Cliente->abreviatura = "";
                $Cliente->save();
                /*                 * ********************************************************** */

                /*                 * ***** CAPITANES ******* */
                //$Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                //if ($Capitanes == null) {
                if ($request->piloto_id == null) {
                    $Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                    if ($Capitanes == null) {
                        $Capitanes = new \App\Models\Capitanes();
                    }

                    $Capitanes->tipo_documento = $request->type_document2;
                    $Capitanes->documento = $request->document2;

                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;
                } else {
                    $Capitanes = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                }
                //dd($request->piloto_id);
                $Capitanes->nombres = Upper($request->piloto);
                $Capitanes->apellidos = "";
                $Capitanes->telefono = "";

                $Capitanes->correo = "";

                $Capitanes->save();
                /*                 * ********************************************* */
                /*                 * ***** AERONAVES******* */
                $Aeronaves = \App\Models\Aeronaves::where('matricula', $request->placa)->first();
                if ($Aeronaves == null) {
                    $Aeronaves = new \App\Models\Aeronaves();
                    $Aeronaves->nombre = $request->aeronave;
                    $Aeronaves->matricula = $request->placa;
                    $Aeronaves->peso_maximo = $request->peso_avion;
                    $Aeronaves->maximo_pasajeros = $request->maximo_pasajeros;
                    $Aeronaves->estacion_id = Encryptor::decrypt($request->estacion_id);
                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;

                    $Aeronaves->fecha_registro = now();
                    $Aeronaves->ip = $ip;
                    $Aeronaves->save();
                }
                /*                 * ***************************************** */




                /*                 * ***** SOLICITUDES******* */
                $Solicitudes = new \App\Models\Solicitudes();

                $Solicitudes->fecha = now();
                $Solicitudes->numero_solicitud = '0';
                $Solicitudes->codigo_aprobacion = "";
                $Solicitudes->cliente_id = $Cliente->id;
                $Solicitudes->correo = $request->correo;
                $Solicitudes->tipo_vuelo_id = 2; // TIPO DE VUELO 2 GENERAL
                $Solicitudes->nacionalidad_id = 1; //$request->nacionalidad_id;
                $Solicitudes->turno_id = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio)); //$request->turno_id;
                $Solicitudes->aprobado_aero = 1;
                $Solicitudes->fecha_aprobado_aero = now();
                $Solicitudes->aprobado_baer = 1;
                $Solicitudes->fecha_aprobado_baer = now();
                $Solicitudes->user_id = Auth::user()->id;
                $Solicitudes->fecha_registro = now();
                $Solicitudes->ip = $ip;
                $Solicitudes->save();
                /*                 * ****************************************** */

                /*                 * ***** RESERVACIONES ******* */


                $Reservaciones = new \App\Models\Reservaciones();

                $Reservaciones->numero_vuelo = 0; //$request->numero_vuelo;
                $Reservaciones->fecha = now();
                $Reservaciones->hora_inicio = $request->hora_inicio;
                $Reservaciones->hora_fin = "";
                $Reservaciones->solicitud_id = $Solicitudes->id;

                $Reservaciones->valor_dolar = $DOLAR;
                $Reservaciones->valor_euro = $EURO;
                $Reservaciones->valor_petro = $PETRO;

                $Reservaciones->origen_id = Auth::user()->aeropuerto_id;  // Encryptor::decrypt($request->origen_id);
                $Reservaciones->otro_origen = ""; //($request->origen_otro == null ? "" : $request->origen_otro);
                $Reservaciones->destino_id = Encryptor::decrypt($request->destino_id);
                $Reservaciones->otro_destino = ($request->destino_otro == null ? "" : $request->destino_otro);
                $Reservaciones->aeronave_id = $Aeronaves->id;
                $Reservaciones->capitan_id = $Capitanes->id;
                $Reservaciones->cantidad_pasajeros = $request->cant_pasajeros;
                $Reservaciones->carga_embarque = 0;
                $Reservaciones->carga_desembarque = 0;

                $Reservaciones->observaciones = ''; //$request->observacion;
                $Reservaciones->precio = 0; //saveFloat($request->precio);

                $Reservaciones->pagada = 1; //$request->pagada;

                $Reservaciones->save();
                /*                 * ******************************** */
                foreach ($request->money as $key => $value) {
                    $Pagos = new \App\Models\Pagos();

                    $Pagos->reservacion_id = $Reservaciones->id;
                    $Pagos->forma_pago_id = Encryptor::decrypt($key);

                    $Pagos->monto = saveFloat($value);

                    $Pagos->save();
                }
                // if ($request->pagada == 1) {
                // }
                /*                 * ***************************** */
                //dd($extra);
                foreach ($extra as $value) {
                    $ProdS = new \App\Models\ReservacionesProdservicios();

                    $ProdS->reservacion_id = $Reservaciones->id;
                    $ProdS->prodservicio_id = Encryptor::decrypt($value['crypt_id']);

                    $ProdS->monto = 0;
                    $ProdS->tipo_moneda_id = 1;
                    $ProdS->valor_moneda = 0;

                    $ProdS->save();
                }

                /*
                  if ($request->img != null) {
                  // dd($request->img);
                  $fileName = "pago_img_" . $Reservaciones->id . '.' . $request->img->extension();
                  $request->img->move(public_path('pagos'), $fileName);
                  }
                 */
                // dd($request->pagada);
                $subject = "Solicitud de Permiso de Vuelo Registrado ";

                if ($request->correo != null) {
                    $for = $request->correo;
                    $datos = [];
                    $pdf = PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'));
                    Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                        //$msj->from("mrivero52.correo@gmail.com", "Registro");
                        $msj->subject($subject);
                        $msj->to($for);
                        $msj->attachData($pdf->output(), "proforma.pdf");
                    });
                }


                $Factura = new \App\Models\Facturas();
                $Factura->fecha_factura = now();
                $Factura->cliente_id = $Cliente->id;
                $Factura->solicitud_id = $Solicitudes->id;
                $Factura->moneda_aplicada = $MonedaPago;
                $Factura->monto_moneda_aplicada = ${$MonedaPago};
                $Factura->formato_factura = 'formato_tasa_general';
                $Factura->save();

                foreach ($tasa as $value) {
                    $FacturaDetalle = new \App\Models\FacturasDetalle();
                    $FacturaDetalle->factura_id = $Factura->id;
                    $FacturaDetalle->codigo = $value['codigo'];
                    $FacturaDetalle->descripcion = $value['descripcion'];
                    $FacturaDetalle->precio = $value['bs'];
                    $FacturaDetalle->precio2 = $value['precio'];
                    $FacturaDetalle->iva = $value['iva'];
                    $FacturaDetalle->save();
                }

                foreach ($extra as $value) {
                    $FacturaDetalle = new \App\Models\FacturasDetalle();
                    $FacturaDetalle->factura_id = $Factura->id;
                    $FacturaDetalle->codigo = $value['codigo'];
                    $FacturaDetalle->descripcion = $value['descripcion'];
                    $FacturaDetalle->precio = $value['bs'];
                    $FacturaDetalle->precio2 = $value['precio'];
                    $FacturaDetalle->iva = $value['iva'];
                    $FacturaDetalle->save();
                }

                /*                 * ******************************** */
                foreach ($request->money as $key => $value) {
                    $Pagos = new \App\Models\FacturasPagos();

                    $Pagos->reservacion_id = $Factura->id;
                    $Pagos->forma_pago_id = Encryptor::decrypt($key);

                    $Pagos->monto = saveFloat($value);

                    $Pagos->save();
                }


                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Registrado Exitosamente";
                $result['data'] = $Factura->crypt_id;
                //return PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
                /* } else {
                  $result['status'] = 0;
                  $result['type'] = 'error';
                  $result['message'] = "La Aeronave ya Posee un Registro de Vuelo Para esa Fecha";
                  } */
            }
            return $result;
        } else {

            $O = \App\Models\Aeropuertos::where("activo", 1)->where("id", "!=", Auth::user()->aeropuerto->id)->get();
            foreach ($O as $value) {
                $ORIGEN[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
                //$DESTINO_EXT[$value->crypt_id] = ["data-baer" => ($value->pertenece_baer == true ? 1 : 0), "data-categoria" => $value->categoria->porcentaje];
            }
            $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();

            $DOLAR = $this->getDolar();
            $EURO = $this->getEuro();
            $PETRO = $this->getPetro();

            return view('registros.dosas', compact('ORIGEN', 'forma_pagos', 'DOLAR', 'EURO', 'PETRO'));
        }
    }

    public function tasas(Request $request) {

        if (Auth::user()->aeropuerto_id == null) {


            $msg = __('El Usuario no se le ha Asignado un Aeropuerto');
            return view('errors.generic', compact('msg'));
            $where = [[DB::raw("1"), 1]];
        }
        if ($request->isMethod('post')) {
            // dd($request->all());
            $Validator = \Validator::make(
                            $request->all(), [
                        'hora_inicio' => 'required',
                        'destino_id' => 'required',
                        'type_document2' => 'required',
                        'document2' => 'required',
                        'piloto' => 'required',
                        'cant_pasajeros' => 'required',
                        'placa' => 'required',
                        'type_document' => 'required',
                        'document' => 'required',
                        'razon' => 'required',
                        'phone' => 'required',
                        'direccion' => 'required',
                        'money' => 'required|array',
                            ], [
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                        'type_document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'document2.required' => __('Indique el Nombre del Responsable'),
                        'piloto.required' => __('El Nombre del Piloto es Requerido'),
                        'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros'),
                        'placa.required' => __('Debe Indicar la Matricula de la Aeronave'),
                        'type_document.required' => __('El Documento del Responsable es Requerido'),
                        'document.required' => __('El Documento del Responsable es Requerido'),
                        'razon.required' => __('Indique el Nombre del Responsable'),
                        'phone.required' => __('Indique el Teléfono del Responsable'),
                        'direccion.required' => __('Indique la Dirección del Responsable'),
                        'money.required' => __('Indique la Forma de Pago'),
                        'money.array' => __('Formato Errado en la Forma de Pago')
                            ]
            );

            if ($Validator->fails()) {
                $msg = [];
                foreach ($Validator->messages()->all() as $value) {
                    $msg[] = $value;
                }
                $result['status'] = 0;
                $result['type'] = 'error';
                $result['message'] = $Validator->errors()->first();
            } else {


                $Aeronave = \App\Models\Aeronaves::where("matricula", $request->placa)->where("activo", 1)->first();
                /* $isReg = \App\Models\Reservaciones::where('aeronave_id', $Aeronave->id)
                  ->whereDate('fecha', date('Y-m-d'))
                  ->where('hora_inicio', $request->hora_inicio)
                  ->count();

                  if ($isReg == 0) { */

                $DOLAR = $this->getDolar();
                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $Origen = \App\Models\Aeropuertos::find(Auth::user()->aeropuerto_id);

                //$Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));
                //$MonedaPago = $moneda = ($request->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$request->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];

                $data_tasa = $request->all();
                $data_tasa['tipo_vuelo_id'] = 2;
                $data_tasa['origen_id'] = Encryptor::encrypt(Auth::user()->aeropuerto_id);
                $data_tasa['nacionalidad_id'] = 1;
                $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                $tasa = $this->getProdservicios("tasa", $data_tasa);

                /*
                  $data_dosa = $request->all();
                  $data_dosa['tipo_vuelo_id'] = 2;
                  $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));

                  $dosa = $this->getProdservicios("dosa", $data_dosa);
                 */
                $prodservExtra = array();
                $extra = array();
                if (is_array($request->prodservicios)) {
                    foreach ($request->prodservicios as $value) {
                        $prodservExtra[] = Encryptor::decrypt($value);
                    }
                }
                if (count($prodservExtra) > 0) {
                    //dd($prodservExtra);

                    $data_extra = $request->all();
                    $data_extra['tipo_vuelo_id'] = 2;
                    $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                    $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                    //$extra =  $this->getProdservicios("extra", $request);
                }

                $ip = $this->getIp();

                /*                 * ***** CLIENTES******* */
                //$Cliente = \App\Models\Clientes::where('documento', $request->document)->first();
                //if ($Cliente == null) {
                if ($request->cliente_id == null) {

                    $Cliente = \App\Models\Clientes::where('documento', $request->document)->first();

                    if ($Cliente == null) {
                        $Cliente = new \App\Models\Clientes();
                    }



                    $Cliente->tipo_documento = $request->type_document;
                    $Cliente->documento = $request->document;

                    //$Cliente->user_id = Auth::user()->id;
                    $Cliente->user_id = 1;
                    $Cliente->fecha_registro = now();
                    $Cliente->ip = $ip;
                } else {
                    $Cliente = \App\Models\Clientes::find(Encryptor::decrypt($request->cliente_id));
                }

                $Cliente->direccion = $request->direccion;
                $Cliente->tipo_id = 3;

                $Cliente->razon_social = Upper($request->razon);
                $Cliente->telefono = $request->phone;
                $Cliente->correo = (isset($request->correo) ?: "$request->correo");

                $Cliente->direccion = $request->direccion;
                $Cliente->abreviatura = "";
                $Cliente->save();
                /*                 * ********************************************************** */

                /*                 * ***** CAPITANES ******* */
                //$Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                //if ($Capitanes == null) {
                if ($request->piloto_id == null) {
                    $Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                    if ($Capitanes == null) {
                        $Capitanes = new \App\Models\Capitanes();
                    }

                    $Capitanes->tipo_documento = $request->type_document2;
                    $Capitanes->documento = $request->document2;

                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;
                } else {
                    $Capitanes = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                }
                //dd($request->piloto_id);
                $Capitanes->nombres = Upper($request->piloto);
                $Capitanes->apellidos = "";
                $Capitanes->telefono = "";

                $Capitanes->correo = "";

                $Capitanes->save();
                /*                 * ********************************************* */
                /*                 * ***** AERONAVES******* */
                $Aeronaves = \App\Models\Aeronaves::where('matricula', $request->placa)->first();
                if ($Aeronaves == null) {
                    $Aeronaves = new \App\Models\Aeronaves();
                    $Aeronaves->nombre = $request->aeronave;
                    $Aeronaves->matricula = $request->placa;
                    $Aeronaves->peso_maximo = $request->peso_avion;
                    $Aeronaves->maximo_pasajeros = $request->maximo_pasajeros;
                    $Aeronaves->estacion_id = Encryptor::decrypt($request->estacion_id);
                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;

                    $Aeronaves->fecha_registro = now();
                    $Aeronaves->ip = $ip;
                    $Aeronaves->save();
                }
                /*                 * ***************************************** */




                /*                 * ***** SOLICITUDES******* */
                $Solicitudes = new \App\Models\Solicitudes();

                $Solicitudes->fecha = now();
                $Solicitudes->numero_solicitud = '0';
                $Solicitudes->codigo_aprobacion = "";
                $Solicitudes->cliente_id = $Cliente->id;
                $Solicitudes->correo = $request->correo;
                $Solicitudes->tipo_vuelo_id = 2; // TIPO DE VUELO 2 GENERAL
                $Solicitudes->nacionalidad_id = 1; //$request->nacionalidad_id;
                $Solicitudes->turno_id = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio)); //$request->turno_id;
                $Solicitudes->aprobado_aero = 1;
                $Solicitudes->fecha_aprobado_aero = now();
                $Solicitudes->aprobado_baer = 1;
                $Solicitudes->fecha_aprobado_baer = now();
                $Solicitudes->user_id = Auth::user()->id;
                $Solicitudes->fecha_registro = now();
                $Solicitudes->ip = $ip;
                $Solicitudes->save();
                /*                 * ****************************************** */

                /*                 * ***** RESERVACIONES ******* */


                $Reservaciones = new \App\Models\Reservaciones();

                $Reservaciones->numero_vuelo = 0; //$request->numero_vuelo;
                $Reservaciones->fecha = now();
                $Reservaciones->hora_inicio = $request->hora_inicio;
                $Reservaciones->hora_fin = "";
                $Reservaciones->solicitud_id = $Solicitudes->id;

                $Reservaciones->valor_dolar = $DOLAR;
                $Reservaciones->valor_euro = $EURO;
                $Reservaciones->valor_petro = $PETRO;

                $Reservaciones->origen_id = Auth::user()->aeropuerto_id;  // Encryptor::decrypt($request->origen_id);
                $Reservaciones->otro_origen = ""; //($request->origen_otro == null ? "" : $request->origen_otro);
                $Reservaciones->destino_id = Encryptor::decrypt($request->destino_id);
                $Reservaciones->otro_destino = ($request->destino_otro == null ? "" : $request->destino_otro);
                $Reservaciones->aeronave_id = $Aeronaves->id;
                $Reservaciones->capitan_id = $Capitanes->id;
                $Reservaciones->cantidad_pasajeros = $request->cant_pasajeros;
                $Reservaciones->carga_embarque = 0;
                $Reservaciones->carga_desembarque = 0;

                $Reservaciones->observaciones = ''; //$request->observacion;
                $Reservaciones->precio = 0; //saveFloat($request->precio);

                $Reservaciones->pagada = 1; //$request->pagada;

                $Reservaciones->save();
                /*                 * ******************************** */
                foreach ($request->money as $key => $value) {
                    $Pagos = new \App\Models\Pagos();

                    $Pagos->reservacion_id = $Reservaciones->id;
                    $Pagos->forma_pago_id = Encryptor::decrypt($key);

                    $Pagos->monto = saveFloat($value);

                    $Pagos->save();
                }
                // if ($request->pagada == 1) {
                // }
                /*                 * ***************************** */
                //dd($extra);
                foreach ($extra as $value) {
                    $ProdS = new \App\Models\ReservacionesProdservicios();

                    $ProdS->reservacion_id = $Reservaciones->id;
                    $ProdS->prodservicio_id = Encryptor::decrypt($value['crypt_id']);

                    $ProdS->monto = 0;
                    $ProdS->tipo_moneda_id = 1;
                    $ProdS->valor_moneda = 0;

                    $ProdS->save();
                }

                /*
                  if ($request->img != null) {
                  // dd($request->img);
                  $fileName = "pago_img_" . $Reservaciones->id . '.' . $request->img->extension();
                  $request->img->move(public_path('pagos'), $fileName);
                  }
                 */
                // dd($request->pagada);
                $subject = "Solicitud de Permiso de Vuelo Registrado ";

                if ($request->correo != null) {
                    $for = $request->correo;
                    $datos = [];
                    $pdf = PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'));
                    Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                        //$msj->from("mrivero52.correo@gmail.com", "Registro");
                        $msj->subject($subject);
                        $msj->to($for);
                        $msj->attachData($pdf->output(), "proforma.pdf");
                    });
                }


                $Factura = new \App\Models\Facturas();
                $Factura->fecha_factura = now();
                $Factura->cliente_id = $Cliente->id;
                $Factura->solicitud_id = $Solicitudes->id;
                $Factura->moneda_aplicada = $MonedaPago;
                $Factura->monto_moneda_aplicada = ${$MonedaPago};
                $Factura->formato_factura = 'formato_tasa_general';
                $Factura->save();

                foreach ($tasa as $value) {
                    $FacturaDetalle = new \App\Models\FacturasDetalle();
                    $FacturaDetalle->factura_id = $Factura->id;
                    $FacturaDetalle->codigo = $value['codigo'];
                    $FacturaDetalle->descripcion = $value['descripcion'];
                    $FacturaDetalle->precio = $value['bs'];
                    $FacturaDetalle->precio2 = $value['precio'];
                    $FacturaDetalle->iva = $value['iva'];
                    $FacturaDetalle->save();
                }

                foreach ($extra as $value) {
                    $FacturaDetalle = new \App\Models\FacturasDetalle();
                    $FacturaDetalle->factura_id = $Factura->id;
                    $FacturaDetalle->codigo = $value['codigo'];
                    $FacturaDetalle->descripcion = $value['descripcion'];
                    $FacturaDetalle->precio = $value['bs'];
                    $FacturaDetalle->precio2 = $value['precio'];
                    $FacturaDetalle->iva = $value['iva'];
                    $FacturaDetalle->save();
                }

                foreach ($request->money as $key => $value) {
                    $Pagos = new \App\Models\FacturasPagos();

                    $Pagos->factura_id = $Factura->id;
                    $Pagos->forma_pago_id = Encryptor::decrypt($key);

                    $Pagos->monto = saveFloat($value);

                    $Pagos->save();
                }

                $result['status'] = 1;
                $result['type'] = 'success';
                $result['message'] = "Registrado Exitosamente";
                $result['data'] = $Factura->crypt_id;
                //return PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
                /* } else {
                  $result['status'] = 0;
                  $result['type'] = 'error';
                  $result['message'] = "La Aeronave ya Posee un Registro de Vuelo Para esa Fecha";
                  }
                 */
            }
            return $result;
        } else {

            $D = \App\Models\Aeropuertos::where("activo", 1)->where("id", "!=", Auth::user()->aeropuerto->id)->get();
            foreach ($D as $value) {
                $DESTINO[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
                //$DESTINO_EXT[$value->crypt_id] = ["data-baer" => ($value->pertenece_baer == true ? 1 : 0), "data-categoria" => $value->categoria->porcentaje];
            }
            $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();

            $DOLAR = $this->getDolar();
            $EURO = $this->getEuro();
            $PETRO = $this->getPetro();

            return view('registros.tasas', compact('DESTINO', 'forma_pagos', 'DOLAR', 'EURO', 'PETRO'));
        }
    }

    public function getListReg($data = array()) {

        $where[] = [DB::raw("1"), 1];
        if (Auth::user()->aeropuerto != null) {
            $where[] = [
                function ($q) {
                    $q->orWhere('origen_id', Auth::user()->aeropuerto->id);
                    $q->orWhere('destino_id', Auth::user()->aeropuerto->id);
                }
            ];
        }

        if ($data['matricula'] != null) {
            $Nave = \App\Models\Aeronaves::where('matricula', $data['matricula'])->first();
            if ($Nave != null) {
                $where[] = ['proc_solicitudes_aeronaves.matricula', '=', $Nave->id];
            }
        }

        if ($data['fecha_v'] != null) {
            $where[] = [
                function ($q) use ($data) {
                    $q->whereDate('proc_reservaciones.fecha', $this->saveDate($data['fecha_v']));
                }
            ];
        }

        if ($data['fecha_r'] != null) {
            $where[] = [
                function ($q) use ($data) {
                    $q->whereDate('proc_solicitudes.fecha', $this->saveDate($data['fecha_r']));
                }
            ];
        } else {
            $where[] = [
                function ($q) {
                    $q->whereDate('proc_solicitudes.fecha', date("Y-m-d"));
                }
            ];
        }



        $name_route = 'registros';

        $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;

        $registros = \App\Models\Reservaciones::join("proc_solicitudes", function ($join) {
                    $join->where("proc_solicitudes.id", "=", DB::raw('proc_reservaciones.solicitud_id'));
                    $join->where("proc_solicitudes.activo", "=", DB::raw('true'));
                })
                ->leftJoin('proc_solicitudes_aeronaves', 'proc_solicitudes_aeronaves.solicitud_id', '=', 'proc_solicitudes.id')
                ->select("proc_reservaciones.*")
                ->where($where)
                ->get();

        //dd($registros->toArray());        
        $data_active = Datatables::of($registros)
                ->addIndexColumn()
                ->addColumn('status', function ($row) use ($route_id) {
                    $actionBtn = "";

                    return $actionBtn;
                })
                ->addColumn('tipo', function ($row) {

                    if (Auth::user()->aeropuerto->id == $row->origen_id) {
                        $text = "TASA";
                    } else {
                        if (Auth::user()->aeropuerto->id == $row->destino_id) {
                            $text = "DOSA";
                        } else {
                            $text = "";
                        }
                    }


                    return $text;
                })
                ->addColumn('monto', function ($row) {


                    if (Auth::user()->aeropuerto->id == $row->origen_id) {

                        $monto = $this->getProdservicios("tasa", $row, [], 'monto');
                    } else {

                        if (Auth::user()->aeropuerto->id == $row->destino_id) {
                            $monto = $this->getProdservicios("dosa", $row, [], 'monto');
                        } else {
                            $monto = 0;
                        }
                    }



                    return $monto;
                })
                ->addColumn('action', function ($row) use ($route_id) {
                    $actionBtn = "";
                    // ' . route('registros.ver', $row->id) . '

                    if (in_array("detail", Auth::user()->getActions()[$route_id])) {
                        $actionBtn .= '<a onClick="viewReport(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-eye"></li> </a> ';
                    }
                    if ($row->activo == true && $row->facturado == false) {
                        /*
                          if (in_array("add_serv", Auth::user()->getActions()[$route_id])) {
                          $actionBtn .= '<a onClick="addProds(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-plus"></li> </a> ';
                          }

                         */
                        if (in_array("facturar", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="viewImg(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-money-bill"></li> </a> ';
                        }
                        /*
                          if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                          $actionBtn .= '<a onClick="anularVuelo(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-trash"></li> </a> ';
                          }
                         * 
                         */
                    }
                    /*
                      if ($row->facturado == true) {
                      $actionBtn .= '<a onClick="addNewFact(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-file-invoice"></li> </a> ';
                      }
                     */




                    /*
                      if ($row->aprobado === null) {
                      $actionBtn .= '<select onChange="aplicar(this)" style="width:80px" class="form-control is-valid"><option value="">Seleccione</option><option value="' . $row->id . ':1">Aprobado</option><option value="' . $row->id . ':0">Negado</option></select> ';
                      }
                     */

                    return $actionBtn;
                    //return "";
                })
                ->rawColumns(['action', 'status'])
                ->make(true);

        $data1 = $data_active->original['data'];
        return $data1;
    }

    public function search_vuelos(Request $request) {
        return response()->json($this->getListReg($request->all()));
    }

    function a() {
        return PDF::loadView("registros.pdf")->stream("pdf.pdf");
    }

    public function refreshCaptcha() {
        return response()->json(['captcha' => captcha_img('flat')]);
    }

    public function reserva_general(Request $request) {

        $msg = "";
        if ($request->isMethod('post')) {


            $Validator = \Validator::make(
                            $request->all(), [
                        'type_document2' => 'required',
                        'document2' => 'required',
                        'piloto' => 'required',
                        'nacionalidad_id' => 'required',
                        'fecha' => 'required',
                        'hora_inicio' => 'required',
                        'origen_id' => 'required',
                        'destino_id' => 'required',
                        'cant_pasajeros' => 'required',
                        'placa' => 'required'
                            ], [
                        'type_document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'document2.required' => __('Debe Indicar El Documento del Piloto'),
                        'piloto.required' => __('El Nombre del Piloto es Requerido'),
                        'nacionalidad_id.required' => __('Debe Indicar El Tipo de Vuelo (Nacional o Internacional)'),
                        'fecha.required' => __('Debe Indicar la Fecha'),
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'origen_id.required' => __('Debe Indicar el Origen del Vuelo'),
                        'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                        'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros '),
                        'placa.required' => __('Debe Indicar la Matricula de la Aeronave')
                            ]
            );

            if ($Validator->fails()) {
                $msg = [];
                foreach ($Validator->messages()->all() as $value) {
                    $msg[] = $value;
                }




                return view('registros.fail', compact('msg'));
            } else {


                $Aeronave = \App\Models\Aeronaves::where("matricula", $request->placa)->where("activo", 1)->first();
                /* $isReg = \App\Models\Reservaciones::where('aeronave_id', $Aeronave->id)
                  ->whereDate('fecha', $this->saveDate($request->fecha))
                  ->count();

                  if ($isReg == 0) {
                 * 
                 */

                $DOLAR = $this->getDolar();
                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));

                $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));

                //$MonedaPago = $moneda = ($request->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$request->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2];

                $data_tasa = $request->all();
                $data_tasa['tipo_vuelo_id'] = 2;
                $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                $tasa = $this->getProdservicios("tasa", $data_tasa);

                $data_dosa = $request->all();
                $data_dosa['tipo_vuelo_id'] = 2;
                $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));

                $dosa = $this->getProdservicios("dosa", $data_dosa);

                $prodservExtra = array();
                $extra = array();
                if (is_array($request->prodservicios)) {
                    foreach ($request->prodservicios as $value) {
                        $prodservExtra[] = Encryptor::decrypt($value);
                    }
                }
                if (count($prodservExtra) > 0) {
                    //dd($prodservExtra);

                    $data_extra = $request->all();
                    $data_extra['tipo_vuelo_id'] = 2;
                    $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                    $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                    //$extra =  $this->getProdservicios("extra", $request);
                }

                $ip = $this->getIp();

                /*                 * ***** CAPITANES ******* */
                //$Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                //if ($Capitanes == null) {
                if ($request->piloto_id == null) {
                    $Capitanes = \App\Models\Pilotos::where('documento', $request->document2)->first();
                    if ($Capitanes == null) {
                        $Capitanes = new \App\Models\Pilotos();
                    }

                    $Capitanes->tipo_documento = $request->type_document2;
                    $Capitanes->documento = $request->document2;

                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;
                } else {
                    $Capitanes = \App\Models\Pilotos::find(Encryptor::decrypt($request->piloto_id));
                }
                //dd($request->piloto_id);
                $Capitanes->nombres = Upper($request->piloto);
                $Capitanes->apellidos = "";
                $Capitanes->telefono = "";

                $Capitanes->correo = "";

                $Capitanes->save();

                /*                 * ***** RESPONSABLE******* */
                //$Cliente = \App\Models\Clientes::where('documento', $request->document)->first();
                //if ($Cliente == null) {



                $Cliente = \App\Models\Clientes::where('documento', $request->document2)->first();

                if ($Cliente == null) {
                    $Cliente = new \App\Models\Clientes();
                }



                $Cliente->tipo_documento = $request->type_document2;
                $Cliente->documento = $request->document2;

                //$Cliente->user_id = Auth::user()->id;
                $Cliente->user_id = 1;
                $Cliente->fecha_registro = now();
                $Cliente->ip = $ip;

                $Cliente->direccion = "";
                $Cliente->tipo_id = 3;

                $Cliente->razon_social = Upper($request->piloto);
                $Cliente->telefono = "";
                $Cliente->correo = "";

                $Cliente->direccion = "";
                $Cliente->abreviatura = "";
                $Cliente->save();
                /*                 * ********************************************************** */


                /*                 * ********************************************* */
                /*                 * ***** AERONAVES******* */
                $Aeronaves = \App\Models\Aeronaves::where('matricula', $request->placa)->first();
                if ($Aeronaves == null) {
                    $Aeronaves = new \App\Models\Aeronaves();
                    $Aeronaves->nombre = $request->aeronave;
                    $Aeronaves->matricula = $request->placa;
                    $Aeronaves->peso_maximo = $request->peso_avion;
                    $Aeronaves->maximo_pasajeros = $request->maximo_pasajeros;
                    $Aeronaves->estacion_id = Encryptor::decrypt($request->estacion_id);
                    // $Cliente->correo = "";
                    //$Cliente->user_id = Auth::user()->id;

                    $Aeronaves->fecha_registro = now();
                    $Aeronaves->ip = $ip;
                    $Aeronaves->save();
                }
                /*                 * ***************************************** */




                /*                 * ***** SOLICITUDES******* */
                $Solicitudes = new \App\Models\Solicitudes();

                $Solicitudes->fecha = now();
                $Solicitudes->numero_solicitud = '0';
                $Solicitudes->codigo_aprobacion = "";
                $Solicitudes->responsable_id = $Cliente->id;
                $Solicitudes->correo = $request->correo;
                $Solicitudes->tipo_vuelo_id = 2; // TI{O DE VUELO 2 GENERAL
                $Solicitudes->nacionalidad_id = $request->nacionalidad_id;
                $Solicitudes->turno_id = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio)); //$request->turno_id;
                $Solicitudes->aprobado_aero = 1;
                $Solicitudes->fecha_aprobado_aero = now();
                $Solicitudes->aprobado_baer = 1;
                $Solicitudes->fecha_aprobado_baer = now();
                $Solicitudes->user_id = 1;
                $Solicitudes->fecha_registro = now();
                $Solicitudes->ip = $ip;
                $Solicitudes->save();
                /*                 * ****************************************** */

                /*                 * ***** RESERVACIONES ******* */


                $Reservaciones = new \App\Models\Reservaciones();

                $Reservaciones->numero_vuelo = $request->numero_vuelo;
                $Reservaciones->fecha = $this->saveDate($request->fecha);
                $Reservaciones->hora_inicio = $request->hora_inicio;
                $Reservaciones->hora_fin = "";
                $Reservaciones->solicitud_id = $Solicitudes->id;

                $Reservaciones->valor_dolar = $DOLAR;
                $Reservaciones->valor_euro = $EURO;
                $Reservaciones->valor_petro = $PETRO;

                $Reservaciones->origen_id = Encryptor::decrypt($request->origen_id);
                $Reservaciones->otro_origen = ($request->origen_otro == null ? "" : $request->origen_otro);
                $Reservaciones->destino_id = Encryptor::decrypt($request->destino_id);
                $Reservaciones->otro_destino = ($request->destino_otro == null ? "" : $request->destino_otro);
                $Reservaciones->aeronave_id = $Aeronaves->id;
                $Reservaciones->piloto_id = $Capitanes->id;
                $Reservaciones->cantidad_pasajeros = $request->cant_pasajeros;
                $Reservaciones->carga_embarque = 0;
                $Reservaciones->carga_desembarque = 0;

                $Reservaciones->observaciones = $request->observacion;
                $Reservaciones->precio = saveFloat($request->precio);

                $Reservaciones->pagada = $request->pagada;

                $Reservaciones->save();
                /*                 * ******************************** */

                if ($request->pagada == 1) {
                    $Pagos = new \App\Models\Pagos();

                    $Pagos->reservacion_id = $Reservaciones->id;
                    $Pagos->forma_pago_id = 6; // ONLINE

                    $Pagos->monto = saveFloat($request->precio);

                    $Pagos->save();
                }
                /*                 * ***************************** */
                //dd($extra);
                foreach ($extra as $value) {
                    $ProdS = new \App\Models\ReservacionesProdservicios();

                    $ProdS->reservacion_id = $Reservaciones->id;
                    $ProdS->prodservicio_id = Encryptor::decrypt($value['crypt_id']);

                    $ProdS->monto = 0;
                    $ProdS->tipo_moneda_id = 1;
                    $ProdS->valor_moneda = 0;

                    $ProdS->save();
                }

/*
                if ($request->img != null) {
                    try {
                        // dd($request->img);
                        //$fileName = "pago_img_" . $Reservaciones->id . '.' . $request->img->extension();
                        //dd(storage_path('pagos'));
                        //$request->img->move(public_path('pagos'), $fileName);

                        //$request->img->move(storage_path('pagos'), $fileName);
                    } catch (Exception $exc) {
                        dd("fff");
                    }


                    //Storage::disk('local')->put(storage_path('pagos').DIRECTORY_SEPARATOR.$fileName, file_get_contents($request->img));
                    //Storage::disk('local')->put('pagos'.DIRECTORY_SEPARATOR.$fileName, file_get_contents($request->img));
                }
*/
                // dd($request->pagada);
                $subject = "Solicitud de Permiso de Vuelo Registrado ";

                if ($request->correo != null) {
                    $for = $request->correo;
                    $datos = [];
                    $pdf = PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'));
                    try {
                        Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                            //$msj->from("mrivero52.correo@gmail.com", "Registro");
                            $msj->subject($subject);
                            $msj->to($for);
                            $msj->attachData($pdf->output(), "proforma.pdf");
                        });
                    } catch (Exception $ex) {
                        
                    }
                }

                //dd($request->img);

                if ($request->img != null) {
                    // dd($request->img);
                    try {
                        $fileName = "pago_img_" . $Reservaciones->id . '.' . $request->img->extension();
                        //$request->img->move(public_path('pagos'), $fileName);
                        $request->img->move(storage_path('pagos'), $fileName);
                        //Storage::disk('local')->put(public_path('pagos') . DIRECTORY_SEPARATOR . "$fileName", file_get_contents($request->img));
                        //$request->img->move(public_path('pagos'), $fileName);
                    } catch (Exception $ex) {
                        
                    }
                    //$request->img->move(storage_path('app'.DIRECTORY_SEPARATOR.'pagos'.DIRECTORY_SEPARATOR), $fileName);
                }


                return PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
                /* } else {
                  $msg = "La Aeronave ya Posee un Registro de Vuelo Para esa Fecha";
                  return view('registros.fail', compact('msg'));
                  }
                 * 
                 */
            }

            /*

              $registro = new Registros();
              $registro->fecha = $this->saveDate($request->fecha);
              $registro->hora = $request->hora_inicio;
              $registro->origen_id = Encryptor::decrypt($request->origen_id);
              $registro->destino_id = Encryptor::decrypt($request->destino_id);
              $registro->cantidad_pasajeros = $request->cantidad_pasajeros;
              $registro->carga = $request->carga;
              $registro->referencia = $request->referencia;
              $registro->contacto = $request->contacto;
              $registro->correo = $request->correo;
              $registro->telefono = $request->telefono;
              $registro->save();

              $datos['contacto'] = $request->contacto;
              $subject = "Solicitud de Permiso de Vuelo";
              $for = $request->correo;
              $msg = "";
              Mail::send('email.registro', $datos, function ($msj) use ($subject, $for) {
              //$msj->from("mrivero52.correo@gmail.com", "Registro");
              $msj->subject($subject);
              $msj->to($for);
              });

              $msg = "Registrado";

             */
        }
        $O = \App\Models\Aeropuertos::where("activo", 1)->get();
        foreach ($O as $value) {
            $ORIGEN[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
            $ORIGEN_EXT[$value->crypt_id] = ["data-iva" => $value->iva, "data-baer" => ($value->pertenece_baer == true ? 1 : 0), "data-categoria" => $value->categoria->porcentaje];
        }

        $D = \App\Models\Aeropuertos::where("activo", 1)->get();
        foreach ($O as $value) {
            $DESTINO [$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
            $DESTINO_EXT[$value->crypt_id] = ["data-baer" => ($value->pertenece_baer == true ? 1 : 0), "data-categoria" => $value->categoria->porcentaje];
        }
        /*
          $P = \App\Models\Prodservicios::where()->get();
          $Prodservicios = array();
          $Prodservicios_Extra = array();
          foreach($P as $value){
          $Prodservicios  [$value->crypt_id] = $value->descripcion;
          $Prodservicios_Extra[$value->crypt_id] = ["data-petro"=>$value->precio_petro, "data-euro"=>$value->precio_petro];
          }



          $Tasa = \App\Models\Prodservicios::where("default_tasa", 1)->get();


          $Dosa = \App\Models\Prodservicios::where("default_dosa", 1)->get();

         */




        //$ORIGEN = \App\Models\Aeropuertos::where("activo", 1)->where("pertenece_baer", 1)->get()->pluck("nombre", "crypt_id");
        //$DESTINO = \App\Models\Aeropuertos::where("activo", 1)->get()->pluck("nombre", "crypt_id");


        $EURO = $this->getEuro();
        $PETRO = $this->getPetro();

        $Bancos = \App\Models\Bancos::pluck('nombre', 'id');

        return view('registros.reserva_general', compact('ORIGEN', 'ORIGEN_EXT', 'DESTINO', 'DESTINO_EXT', 'EURO', 'Bancos', 'PETRO', 'msg'));
    }

    public function get_presupuesto_tasa(Request $request) {
        $Aeronave = \App\Models\Aeronaves::where("matricula", $request->placa)->where("activo", 1)->first();

        $isReg = \App\Models\Reservaciones::where('aeronave_id', $Aeronave->id)
                ->whereDate('fecha', $this->saveDate(($request->fecha == null ? date("d/m/Y") : $request->fecha)))
                ->where('hora_inicio', $request->hora_inicio)
                ->count();
        // dd('oooo');
        if ($isReg == 0) {

            $Validator = \Validator::make(
                            $request->all(), [
                        'hora_inicio' => 'required',
                        'destino_id' => 'required',
                        'cant_pasajeros' => 'required',
                        'placa' => 'required'
                            ], [
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                        'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros '),
                        'placa.required' => __('Debe Indicar la Matricula de la Aeronave')
                            ]
            );

            if ($Validator->fails()) {

                $message = $Validator->errors()->first();
                return view('registros.error_validation', compact('message'));
            } else {
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2]; //fijo para 
                //$MonedaPago = $moneda = ($request->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$request->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                //$valor_moneda = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_' . $moneda)->first()->valor;
                $dosa = array();

                $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));

                $data_tasa = $request->all();
                $data_tasa['tipo_vuelo_id'] = 2;
                $data_tasa['fecha'] = (isset($data_tasa['fecha']) ? $data_tasa['fecha'] : date('d/m/Y'));
                $data_tasa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));
                $data_tasa['origen_id'] = Encryptor::encrypt(Auth::user()->aeropuerto->id);
                $data_tasa['nacionalidad_id'] = 1;

                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $tasa = $this->getProdservicios("tasa", $data_tasa);

                //dd("O,o");



                $prodservExtra = array();
                $extra = array();

                //dd($request->all());

                if (is_array($request->prodservicios)) {
                    foreach ($request->prodservicios as $value) {
                        $prodservExtra[] = Encryptor::decrypt($value);
                    }
                }
                if (count($prodservExtra) > 0) {
                    //dd($prodservExtra);

                    $data_extra = $request->all();
                    $data_extra['tipo_vuelo_id'] = 2;
                    $data_extra['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));

                    $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                    //$extra =  $this->getProdservicios("extra", $request);
                }
                //dd($dosa);


                return view('registros.presupuesto', compact('tasa', 'dosa', 'extra', 'MonedaPago', 'EURO', 'PETRO'));
            }
        } else {
            return view('registros.ya_reg');
        }
    }

    public function get_presupuesto_dosa(Request $request) {
        $Aeronave = \App\Models\Aeronaves::where("matricula", $request->placa)->where("activo", 1)->first();

        $isReg = \App\Models\Reservaciones::where('aeronave_id', $Aeronave->id)
                ->whereDate('fecha', $this->saveDate(($request->fecha == null ? date("d/m/Y") : $request->fecha)))
                ->where('hora_inicio', $request->hora_inicio)
                ->count();
        // dd('oooo');
        if ($isReg == 0) {

            $Validator = \Validator::make(
                            $request->all(), [
                        'hora_inicio' => 'required',
                        'origen_id' => 'required',
                        'placa' => 'required',
                        'horas_est' => "required"
                            ], [
                        'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                        'origen_id.required' => __('Debe Indicar el Origen del Vuelo'),
                        'placa.required' => __('Debe Indicar la Matrícula de la Aeronave'),
                        'horas_est.required' => __('Debe Indicar las Horas de Estacionamiento de la Aeronave')
                            ]
            );

            if ($Validator->fails()) {

                $message = $Validator->errors()->first();
                return view('registros.error_validation', compact('message'));
            } else {
                $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2]; //fijo para 
                //$MonedaPago = $moneda = ($request->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$request->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                //$valor_moneda = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_' . $moneda)->first()->valor;
                $dosa = array();

                $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));

                $data_dosa = $request->all();
                $data_dosa['tipo_vuelo_id'] = 2;
                $data_dosa['fecha'] = (isset($data_tasa['fecha']) ? $data_tasa['fecha'] : date('d/m/Y'));
                $data_dosa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));
                $data_dosa['origen_id'] = Encryptor::encrypt(Auth::user()->aeropuerto->id);
                $data_dosa['nacionalidad_id'] = 1;

                $EURO = $this->getEuro();
                $PETRO = $this->getPetro();

                $dosa = $this->getProdservicios("dosa", $data_dosa);

                //dd("O,o");



                $prodservExtra = array();
                $extra = array();

                //dd($request->all());

                if (is_array($request->prodservicios)) {
                    foreach ($request->prodservicios as $value) {
                        $prodservExtra[] = Encryptor::decrypt($value);
                    }
                }
                if (count($prodservExtra) > 0) {
                    //dd($prodservExtra);

                    $data_extra = $request->all();
                    $data_extra['tipo_vuelo_id'] = 2;
                    $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                    $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                    //$extra =  $this->getProdservicios("extra", $request);
                }
                //dd($dosa);


                return view('registros.presupuesto', compact('tasa', 'dosa', 'extra', 'MonedaPago', 'EURO', 'PETRO'));
            }
        } else {
            return view('registros.ya_reg');
        }
    }

    public function get_presupuesto(Request $request) {


        $Aeronave = \App\Models\Aeronaves::where("matricula", Upper($request->placa))->where("activo", 1)->first();
        
        //dd($Aeronave);
        /*
          $isReg = \App\Models\Reservaciones::where('aeronave_id', $Aeronave->id)
          ->whereDate('fecha', $this->saveDate($request->fecha))
          ->count();

          if ($isReg == 0) {
         */
        $Validator = \Validator::make(
                        $request->all(), [
                    'nacionalidad_id' => 'required',
                    'fecha' => 'required',
                    'hora_inicio' => 'required',
                    'origen_id' => 'required',
                    'destino_id' => 'required',
                    'cant_pasajeros' => 'required',
                    'placa' => 'required'
                        ], [
                    'nacionalidad_id.required' => __('Debe Indicar El Tipo de Vuelo (Nacional o Internacional)'),
                    'fecha.required' => __('Debe Indicar la Fecha'),
                    'hora_inicio.required' => __('Debe Registrar la Hora de Salida'),
                    'origen_id.required' => __('Debe Indicar el Origen del Vuelo'),
                    'destino_id.required' => __('Debe Indicar el Destino del Vuelo'),
                    'cant_pasajeros.required' => __('Debe Indicar la Cantidad de Pasajeros '),
                    'placa.required' => __('Debe Indicar la Matricula de la Aeronave')
                        ]
        );

        if ($Validator->fails()) {

            $message = $Validator->errors()->first();
            return view('registros.error_validation', compact('message'));
        } else {
            $MonedaPago = $moneda = $this->TIPOS_VUELOS_NOMEDAS[2]; //fijo para 
            //$MonedaPago = $moneda = ($request->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$request->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
            //$valor_moneda = \App\Models\Parametros::where('nombre', 'TASA_CAMBIO_' . $moneda)->first()->valor;

            $Origen = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->origen_id));

            $Destino = \App\Models\Aeropuertos::find(Encryptor::decrypt($request->destino_id));

            $data_tasa = $request->all();
            $data_tasa['tipo_vuelo_id'] = 2;
            $data_tasa['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

            $data_dosa = $request->all();
            $data_dosa['tipo_vuelo_id'] = $data['tipo_vuelo_id'] = 2;
            $data_dosa['turno_id'] = $Destino->getTurno((int) str_replace(":", "", $request->hora_inicio));

            $EURO = $this->getEuro();
            $PETRO = $this->getPetro();
            
            
            $tasa = $this->getProdservicios("tasa", $data_tasa);
            
            $dosa = $this->getProdservicios("dosa", $data_dosa);
            //dd($data_dosa);




            $prodservExtra = array();
            $extra = array();
            if (is_array($request->prodservicios)) {
                foreach ($request->prodservicios as $value) {
                    $prodservExtra[] = Encryptor::decrypt($value);
                }
            }
            if (count($prodservExtra) > 0) {
                //dd($prodservExtra);

                $data_extra = $request->all();
                $data_extra['tipo_vuelo_id'] = 2;
                $data_extra['turno_id'] = $Origen->getTurno((int) str_replace(":", "", $request->hora_inicio));

                $extra = $this->getProdservicios("extra", $data_extra, $prodservExtra);
                //$extra =  $this->getProdservicios("extra", $request);
            }
            //dd($dosa);


            return view('registros.presupuesto', compact('tasa', 'dosa', 'extra', 'MonedaPago', 'EURO', 'PETRO'));
        }
        /* } else {
          return view('registros.ya_reg');
          } */
    }

    private function getPresupuesto() {
        
    }

    public function lista() {

        //dd(\App\Models\Reservaciones::get()->toArray());


        if (Auth::user()->aeropuerto != null) {
            $where = function ($cond) {
                $cond->orWhere('origen_id', Auth::user()->aeropuerto->id);
                $cond->orWhere('destino_id', Auth::user()->aeropuerto->id);
            };
        } else {
            $msg = __('El Usuario no se le ha Asignado un Aeropuerto');
            return view('errors.generic', compact('msg'));
            $where = [[DB::raw("1"), 1]];
        }




        $name_route = \Request::route()->getName();
        $route_id = \App\Models\Process::where('route', $name_route)->select('id')->first()->id;
        /*
          $registros = \App\Models\Reservaciones::join("proc_solicitudes", function ($join) {
          $join->whereDate("proc_solicitudes.fecha_registro", "=", "2022-10-26");
          $join->where("proc_solicitudes.id", "=", DB::raw('proc_reservaciones.solicitud_id'));
          $join->where("proc_solicitudes.activo", "=", DB::raw('true'));
          })
          ->select("proc_reservaciones.*")
          ->where($where)
          ->get();
         */

        $registros = \App\Models\Reservaciones::join("proc_solicitudes", function ($join) {
                    $join->whereDate("proc_solicitudes.fecha_registro", "=", "2022-10-26");
                    $join->where("proc_solicitudes.id", "=", DB::raw('proc_reservaciones.solicitud_id'));
                    $join->where("proc_solicitudes.activo", "=", DB::raw('true'));
                })
                ->select("proc_reservaciones.*")
                ->where($where)
                ->get();

        //->toSql();
        //dd($registros);
        //dd(Auth::user()->getActions()[$route_id]);
        $data_active = Datatables::of($registros)
                ->addIndexColumn()
                ->addColumn('status', function ($row) use ($route_id) {
                    $actionBtn = "";
                    /*
                      if ($row->aprobado === null) {
                      $actionBtn .= '<small><span class=""></span> En Espera</small>';
                      } else {
                      if ($row->aprobado == true) {
                      $actionBtn .= '<small><span class="fa fa-check"></span> Aprobado</small> ';
                      } else {
                      $actionBtn .= '<small><span class="fa fa-times"></span> No Aprobado</small> ';
                      }
                      }

                     */


                    return $actionBtn;
                })
                ->addColumn('tipo', function ($row) {
                    if (Auth::user()->aeropuerto->id == $row->origen_id) {
                        $text = "TASA";
                    } else {
                        if (Auth::user()->aeropuerto->id == $row->destino_id) {
                            $text = "DOSA";
                        } else {
                            $text = "";
                        }
                    }


                    return $text;
                })
                ->addColumn('monto', function ($row) {
                    if (Auth::user()->aeropuerto->id == $row->origen_id) {
                        $monto = $this->getProdservicios("tasa", $row, [], 'monto');
                    } else {
                        if (Auth::user()->aeropuerto->id == $row->destino_id) {
                            $monto = $this->getProdservicios("dosa", $row, [], 'monto');
                        } else {
                            $monto = 0;
                        }
                    }



                    return $monto;
                })
                ->addColumn('action', function ($row) use ($route_id) {
                    $actionBtn = "";
                    // ' . route('registros.ver', $row->id) . '
                    if (in_array("detail", Auth::user()->getActions()[$route_id])) {
                        $actionBtn .= '<a onClick="viewReport(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-eye"></li> </a> ';
                    }
                    if ($row->activo == true && $row->facturado == false) {
                        /*
                          if (in_array("add_serv", Auth::user()->getActions()[$route_id])) {
                          $actionBtn .= '<a onClick="addProds(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-plus"></li> </a> ';
                          }
                         */
                        if (in_array("facturar", Auth::user()->getActions()[$route_id])) {
                            $actionBtn .= '<a onClick="viewImg(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-money-bill"></li> </a> ';
                        }
                        /*
                          if (in_array("disable", Auth::user()->getActions()[$route_id])) {
                          $actionBtn .= '<a onClick="anularVuelo(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-trash"></li> </a> ';
                          }
                         */
                    }
                    /*
                      if ($row->facturado == true) {
                      $actionBtn .= '<a onClick="addNewFact(\'' . $row->crypt_id . '\')" href="javascript:void(0)" class=" btn btn-info btn-sm"><li class="fa fa-file-invoice"></li> </a> ';
                      }
                     */





                    /*
                      if ($row->aprobado === null) {
                      $actionBtn .= '<select onChange="aplicar(this)" style="width:80px" class="form-control is-valid"><option value="">Seleccione</option><option value="' . $row->id . ':1">Aprobado</option><option value="' . $row->id . ':0">Negado</option></select> ';
                      }

                     */
                    return $actionBtn;
                })
                ->rawColumns(['action', 'status'])
                ->make(true);

        $data1 = $data_active->original['data'];
        $D = \App\Models\Aeropuertos::where("activo", 1)->where("id", "!=", Auth::user()->aeropuerto->id)->get();
        foreach ($D as $value) {
            $DESTINO[$value->crypt_id] = '(' . $value->codigo_oaci . ') ' . $value->nombre;
            //$DESTINO_EXT[$value->crypt_id] = ["data-baer" => ($value->pertenece_baer == true ? 1 : 0), "data-categoria" => $value->categoria->porcentaje];
        }
        return view('registros.lista', compact('data1', 'DESTINO'));
    }

    public function registros_create(Request $request) {
        
    }

    public function see_money($id = null, Request $request) {
        if ($request->isMethod('post')) {
            $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($request->id));
            if ($Reservaciones->activo == 1) {
                if ($Reservaciones->facturado == 0) {

                    foreach ($request->money as $key => $value) {
                        $monto = saveFloat($value);
                        if ($monto > 0) {
                            $Pagos = new \App\Models\Pagos();

                            $Pagos->reservacion_id = Encryptor::decrypt($request->id);
                            $Pagos->forma_pago_id = Encryptor::decrypt($key);

                            $Pagos->monto = $monto;

                            $Pagos->save();
                        }

                        $Reservaciones->facturado = 1;
                        $Reservaciones->pagada = 1;
                        $Reservaciones->save();
                        if ($Reservaciones->origen_id == Auth::user()->aeropuerto_id) {
                            $tasa = $this->getProdservicios("tasa", $Reservaciones);
                        } else {
                            $tasa = [];
                        }

                        if ($Reservaciones->destino_id == Auth::user()->aeropuerto_id) {
                            $dosa = $this->getProdservicios("dosa", $Reservaciones);
                        } else {
                            $dosa = [];
                        }


                        $MonedaPago = ($Reservaciones->solicitud->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$Reservaciones->solicitud->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                        $extra = [];
                        if (count($Reservaciones->id_serv_ext) > 0) {
                            $extra = $this->getProdservicios("extra", $Reservaciones, $Reservaciones->id_serv_ext);
                        }
                        return PDF::loadView("registros.pdfact", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("factura_pdf.pdf");
                    }
                } else {
                    $msg = "Esta Factura fue Facturada";
                    return view('registros.fail', compact('msg'));
                }
            } else {
                $msg = "Esta Factura fue Anulada";
                return view('registros.fail', compact('msg'));
            }
        } else {
            $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($id));
            if ($Reservaciones->activo == 1) {
                if ($Reservaciones->facturado == 0) {
                    $forma_pagos = \App\Models\FormaPagos::where("activo", true)->where("form", true)->get();
                    $T = 0;
                    if ($Reservaciones->origen_id == Auth::user()->aeropuerto_id) {
                        $T += $this->getProdservicios("tasa", $Reservaciones, [], 'monto')['bs'];
                    }

                    if ($Reservaciones->destino_id == Auth::user()->aeropuerto_id) {
                        $T += $this->getProdservicios("dosa", $Reservaciones, [], 'monto')['bs'];
                    }



                    return view('registros.see_money', compact('Reservaciones', 'forma_pagos', 'T'));
                } else {
                    $msg = "Esta Factura fue Facturada";
                    return view('registros.fail', compact('msg'));
                }
            } else {
                $msg = "Esta Factura fue Anulada";
                return view('registros.fail', compact('msg'));
            }
        }
    }

    public function add_prods($id) {
        $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($id));
        if ($Reservaciones->activo == 1) {
            if ($Reservaciones->facturado == 0) {

                $data = $this->getProdservicios("extra", $Reservaciones);
                $prods = [];
                foreach ($Reservaciones->id_serv_ext as $value) {
                    $prods[] = Encryptor::encrypt($value);
                }

                return view('registros.add_prods', compact('Reservaciones', 'data', 'prods'));
            } else {
                $msg = "Este Vuelo ya Fue Facturado";
                return view('registros.fail', compact('msg'));
            }
        } else {
            $msg = "Esta Factura fue Anulada";
            return view('registros.fail', compact('msg'));
        }
    }

    public function anular_vuelo($id) {
        if (\Request::ajax()) {
            $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($id));
            if ($Reservaciones->activo == 1) {
                if ($Reservaciones->facturado == 0) {

                    $Reservaciones->activo = 0;
                    $Reservaciones->save();
                    return true;
                } else {
                    $msg = "Esta Factura no Puede Ser Anulada";
                    return view('registros.fail', compact('msg'));
                }
            } else {
                $msg = "Esta Factura fue Anulada";
                return view('registros.fail', compact('msg'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function emitir_factura($id) {
        $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($id));
        if ($Reservaciones->activo == 1) {
            if ($Reservaciones->facturado == 0) {


                $Reservaciones->facturado = 1;
                $Reservaciones->save();
                $tasa = $this->getProdservicios("tasa", $Reservaciones);
                $dosa = $this->getProdservicios("dosa", $Reservaciones);
                $MonedaPago = ($Reservaciones->solicitud->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$Reservaciones->solicitud->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
                $extra = [];
                if (count($Reservaciones->id_serv_ext) > 0) {
                    $extra = $this->getProdservicios("extra", $Reservaciones, $Reservaciones->id_serv_ext);
                }
                return PDF::loadView("registros.pdfact", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("factura_pdf.pdf");
            } else {
                $msg = "Esta Factura ya Fue Facturada";
                return view('registros.fail', compact('msg'));
            }
        } else {
            $msg = "Esta Factura fue Anulada";
            return view('registros.fail', compact('msg'));
        }
    }

    public function add_prods_save(Request $request) {
        if (\Request::ajax()) {
            $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($request->id));
            if ($Reservaciones->activo == 1) {
                if ($Reservaciones->facturado == 0) {
                    \App\Models\ReservacionesProdservicios::where("reservacion_id", Encryptor::decrypt($request->id))->delete();
                    if (isset($request->prodservicios)) {
                        foreach ($request->prodservicios as $value) {
                            $a = new \App\Models\ReservacionesProdservicios();
                            $a->reservacion_id = Encryptor::decrypt($request->id);
                            $a->prodservicio_id = Encryptor::decrypt($value);
                            $a->monto = 0;
                            $a->tipo_moneda_id = 1;
                            $a->valor_moneda = 1;

                            $a->save();
                        }
                    }
                } else {
                    $msg = "Esta Factura fue Anulada";
                    return view('registros.fail', compact('msg'));
                }
            } else {
                $msg = "Esta Factura fue Anulada";
                return view('registros.fail', compact('msg'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function ver($id) {
        $Reservaciones = \App\Models\Reservaciones::find(Encryptor::decrypt($id));

        if (Auth::user()->aeropuerto->id == $Reservaciones->origen_id) {
            $tasa = $this->getProdservicios("tasa", $Reservaciones);
            $dosa = [];
        } else {
            $tasa = [];
            if (Auth::user()->aeropuerto->id == $Reservaciones->destino_id) {
                $dosa = $this->getProdservicios("dosa", $Reservaciones);
            } else {
                $dosa = [];
            }
        }



        $MonedaPago = ($Reservaciones->solicitud->tipo_vuelo_id == 1 ? $this->TIPOS_VUELOS_NOMEDAS[$Reservaciones->solicitud->nacionalidad_id] : $this->TIPOS_VUELOS_NOMEDAS[2] );
        $extra = [];
        if (count($Reservaciones->id_serv_ext) > 0) {
            $extra = $this->getProdservicios("extra", $Reservaciones, $Reservaciones->id_serv_ext);
        }
        return PDF::loadView("registros.pdf", compact('Reservaciones', 'tasa', 'dosa', 'extra', 'MonedaPago'))->stream("proforma_pdf.pdf");
    }

    public function aprobado($status) {


        $value = explode(":", $status);
        $Registros = Registros::find($value[0]);
        $Registros->aprobado = $value[1];
        $Registros->save();
        $subject = "Solicitud de Permiso de Vuelo Fue " . ($value[1] == 0 ? "Negado" : "Aprobado");
        $datos['contacto'] = $Registros->contacto;
        $for = $Registros->correo;
        if ($value[1] == 1) {

            $pdf = PDF::loadView('email.aprobado', compact('Registros'));

            Mail::send('email.registro', $datos, function ($msj) use ($subject, $for, $pdf) {
                //$msj->from("mrivero52.correo@gmail.com", "Registro");
                $msj->subject($subject);
                $msj->to($for);
                $msj->attachData($pdf->output(), "aprobacion.pdf");
            });
        } else {

            Mail::send('email.registro', $datos, function ($msj) use ($subject, $for) {
                //$msj->from("mrivero52.correo@gmail.com", "Registro");
                $msj->subject($subject);
                $msj->to($for);
            });
        }
    }

}
