<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Encryptor;
use App\Models\LotesTickets;
use DataTables;
use App\Exports\TicketsExport;
use App\Models\TiempoTickets;
use DateTime;
use Maatwebsite\Excel\Facades\Excel;

class LotesTicketsController extends Controller {

    private $codeError = ['', '0', 0];

    public function venta($type = "") {
        if (\Request::ajax()) {
            if (\Request::isMethod('post')) {
                $request = \Request::all();
                $tickets = [];
                foreach ($request['tickets'] as $key => $value) {
                    if ($value > 0) {

                        $tickets[$key] = \App\Models\LotesDetalle::join('proc_lotes_tickets', 'proc_lotes_tickets.id', '=', 'proc_lotes_detalle.lote_id')
                                ->join('maest_tiempo_tickets', 'maest_tiempo_tickets.id', '=', 'proc_lotes_tickets.tiempo_id')
                                ->where("maest_tiempo_tickets.id", $key)
                                ->whereIn("proc_lotes_tickets.aeropuerto_id", Auth::user()->lista_aeropuertos)
                                ->where("proc_lotes_detalle.vendido", DB::raw('false'))
                                ->orderBy('proc_lotes_detalle.id')
                                ->select("proc_lotes_detalle.*")
                                ->offset(0)
                                ->take($value)
                                ->get()
                        ;
                    }
                }
                if (count($tickets) > 0) {


                    $VentaTickets = new \App\Models\VentaTickets();
                    $VentaTickets->fecha = now();
                    $VentaTickets->cliente_id = 8;
                    $VentaTickets->tasa = $this->DOLAR;
                    $VentaTickets->user_id = Auth::user()->id;
                    $VentaTickets->aeropuerto_id = Auth::user()->aeropuerto_id;
                    $VentaTickets->ip = $this->getIp();

                    $VentaTickets->save();

                    foreach ($tickets as $rows) {
                        foreach ($rows as $tickts) {
                            $ventaDetalle = new \App\Models\VentaTicketsDetalle();
                            $ventaDetalle->venta_id = $VentaTickets->id;
                            $ventaDetalle->cant = 1;
                            $ventaDetalle->ticket_id = $tickts->id;
                            $ventaDetalle->codigo = $tickts->codigo;
                            $ventaDetalle->precio = $tickts->precio;
                            $ventaDetalle->save();

                            $Cod = \App\Models\LotesDetalle::find($tickts->id);
                            $Cod->vendido = true;
                            $Cod->save();
                        }
                    }


                    foreach ($request['fp'] as $key => $value) {
                        $ventaPago = new \App\Models\VentaTicketsPago();
                        $ventaPago->venta_id = $VentaTickets->id;
                        $ventaPago->forma_pago_id = $key;
                        $ventaPago->monto = saveFloat($value);

                        $ventaPago->save();
                    }
                } else {
                    
                }
                $result['status'] = 1;
                $result['type'] = 'success';
                $result['data'] = $VentaTickets->crypt_id;
                $result['message'] = __('Guardado Correctamente');
                return $result;
            } else {
                if (\Request::isMethod('get')) {
                    $error = [];
                    if (Auth::user()->aeropuerto_id == null) {
                        $error['status'] = 0;
                        $error['type'] = 'error';
                        $error['message'] = __('El usuario seleccionado no no posee un aeropuerto asignado.');
                        
                        return view('tickets.venta', ['Tiempos' => ''])->with('error', $error);
                    }
                    $Tiempos = \App\Models\TiempoTickets::leftJoin('proc_lotes_tickets', function ($q) {
                                $q->where('proc_lotes_tickets.tiempo_id', '=', DB::raw('"maest_tiempo_tickets"."id"'));
                                $q->whereIn('proc_lotes_tickets.aeropuerto_id', Auth::user()->lista_aeropuertos);
                            })
                            ->leftJoin('proc_lotes_detalle', function ($q) {
                                $q->where('proc_lotes_detalle.lote_id', '=', DB::raw('"proc_lotes_tickets"."id"'));
                                $q->where('proc_lotes_detalle.vendido', DB::raw('false'));
                            })
                            ->select('maest_tiempo_tickets.*', DB::raw('count(proc_lotes_detalle.*) as cant'))
                            ->groupBy("maest_tiempo_tickets.id", "maest_tiempo_tickets.nombre", "maest_tiempo_tickets.precio", "maest_tiempo_tickets.icono")
                            ->orderBy("maest_tiempo_tickets.id")
                            ->get();
                    // dd($Tiempos);
                    return view('tickets.venta', compact('Tiempos'));
                } else {
                    $Tiempos = \App\Models\TiempoTickets::leftJoin('proc_lotes_tickets', function ($q) {
                                $q->where('proc_lotes_tickets.tiempo_id', '=', DB::raw('"maest_tiempo_tickets"."id"'));
                                $q->whereIn('proc_lotes_tickets.aeropuerto_id', Auth::user()->lista_aeropuertos);
                            })
                            ->leftJoin('proc_lotes_detalle', function ($q) {
                                $q->where('proc_lotes_detalle.lote_id', '=', DB::raw('"proc_lotes_tickets"."id"'));
                                $q->where('proc_lotes_detalle.vendido', DB::raw('false'));
                            })
                            ->select('maest_tiempo_tickets.*', DB::raw('count(proc_lotes_detalle.*) as cant'))
                            ->groupBy("maest_tiempo_tickets.id", "maest_tiempo_tickets.nombre", "maest_tiempo_tickets.precio", "maest_tiempo_tickets.icono")
                            ->orderBy("maest_tiempo_tickets.id")
                            ->get();

                    $result['status'] = 1;
                    $result['type'] = 'success';
                    $result['data'] = $Tiempos;
                    $result['message'] = __('');
                    return $result;
                }
            }
        } else {
            if (\Request::isMethod('get')) {

                if (substr($type, 0, 2) == "QR") {
                    $id = explode(":", $type);
                    $CODES = "BAER";
                    if (count($id) == 2) {
                        $QRS = \App\Models\VentaTickets::with("detalle")->find(Encryptor::decrypt($id[1]));
                        $CODES = "";
                        foreach ($QRS->detalle as $value2) {
                            $CODES .= $value2->codigo . "\n";
                        }
                    }
                    return view('tickets.qr', compact('CODES'));
                } else {
                    if (substr($type, 0, 5) == "CODES") {
                        $id = explode(":", $type);
                        $QRS = \App\Models\VentaTickets::with("detalle.ticket.lote")->find(Encryptor::decrypt($id[1]));
                        //$QRS = \App\Models\VentaTickets::with("detalle.ticket.lote.tiempo")->find($id[1]);
                        
                        return view('tickets.codigos', compact('QRS'));
                    }
                }
            } else {
                if (\Request::isMethod('puth')) {
                    
                }
            }
            return Redirect::to('/');
        }
    }

    public function carga(Request $request) {
        if (\Request::ajax()) {
            if ($request->isMethod('post')) {
                $ValidTime = TiempoTickets::where('id', Encryptor::decrypt($request->tiempo_id))->where('precio', saveFloat($request->precio))->exists();
                
                if ($ValidTime != true) {
                    $result['status'] = 0;
                    $result['type'] = 'error';
                    $result['message'] = __('Ha ocurrido un error al consultar la información');
                    
                    return $result;
                }

                if ($request->file('archivo') != null) {
                    $listadoCods = [];
                    $fail = false;
                    $file = $request->file('archivo');
                    try {
                        //$fileName = "lote_" . $Lote->id . '.' . $file->getClientOriginalExtension();
                        $fileName = "lote_temp" . '.' . $file->getClientOriginalExtension();
                        $file->move(storage_path('lote'), $fileName);

                        $arc = fopen(storage_path('lote') . DIRECTORY_SEPARATOR . $fileName, "r");
                        while (!feof($arc)) {
                            $linea = trim(fgets($arc));
                            $lista = explode("\r", $linea);

                            foreach ($lista as $key) {


                                if (!in_array($key, $this->codeError)) {
                                    if (!in_array($key, $listadoCods)) {
                                        $listadoCods[] = $key;
                                    } else {
                                        $fail = true;
                                        $duplicado = $key;
                                        break;
                                    }
                                }
                            }
                        }
                        //unlink(storage_path('lote') . DIRECTORY_SEPARATOR . $fileName);
                        fclose($arc);
                        if ($fail == false) {
                            $isPresent = \App\Models\LotesDetalle::whereIn('codigo', $listadoCods)->first();

                            if ($isPresent == null) {
                                $Lote = new LotesTickets();

                                $Lote->aeropuerto_id = Encryptor::decrypt($request->aeropuerto_id);
                                $Lote->tiempo_id = Encryptor::decrypt($request->tiempo_id);
                                $Lote->precio = saveFloat($request->precio);

                                $Lote->user_id = Auth::user()->id;
                                $Lote->ip = $this->getIp();
                                $Lote->fecha_registro = now();

                                $Lote->save();
                                /*
                                  $file2 = $request->file('archivo');
                                  $fileName = "lote_" . $Lote->id . '.' . $file2->getClientOriginalExtension();
                                  $file2->move(storage_path('lote'), $fileName);
                                 */
                                rename(storage_path('lote') . DIRECTORY_SEPARATOR . $fileName, storage_path('lote') . DIRECTORY_SEPARATOR . "lote_" . $Lote->id . '.' . $file->getClientOriginalExtension());
                                foreach ($listadoCods as $value) {

                                    $cod = new \App\Models\LotesDetalle();
                                    $cod->lote_id = $Lote->id;
                                    $cod->codigo = $value;
                                    $cod->precio = saveFloat($request->precio);
                                    $cod->save();
                                }
                                $result['status'] = 1;
                                $result['type'] = 'success';
                                $result['message'] = __('Processed Correctly');
                            } else {

                                $result['status'] = 0;
                                $result['type'] = 'error';
                                $result['message'] = __('Existen Códigos que ya estan registrados(' . $isPresent->codigo . ')');
                            }
                        } else {
                            $result['status'] = 0;
                            $result['type'] = 'error';
                            $result['message'] = __('Existen Códigos duplicados en el archivo(' . $duplicado . ')');
                        }
                    } catch (Exception $ex) {
                        
                    }
                }





                return $result;
            } else {
                $Tiempos = \App\Models\TiempoTickets::get()->pluck("nombre", 'crypt_id');
                $TiemposValor = \App\Models\TiempoTickets::get()->pluck("precio", 'crypt_id');
                $properties = [];

                foreach ($TiemposValor as $key => $value){
                    $properties[$key] = ['data-valor' => $value];
                }
                // dd($properties);
                $Aeropuertos = \App\Models\Aeropuertos::orderBy("nombre")
                        ->where("pertenece_baer", true)
                        ->where("activo", true)
                        ->whereIn("id", Auth::user()->lista_aeropuertos)
                        ->get()
                        ->pluck("full_nombre", "crypt_id");

                return view('tickets.carga', compact('Tiempos', 'properties', 'Aeropuertos'));
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function lista() {
        if (\Request::ajax()) {
            if (\Request::isMethod('post')) {
                $LotesTickets = \App\Models\LotesDetalle::join("proc_lotes_tickets", "proc_lotes_tickets.id", "=", "proc_lotes_detalle.lote_id")
                        ->join("maest_tiempo_tickets", "maest_tiempo_tickets.id", "=", "proc_lotes_tickets.tiempo_id")
                        ->join("maest_aeropuertos", "maest_aeropuertos.id", "=", "proc_lotes_tickets.aeropuerto_id")
                        ->select('proc_lotes_detalle.id', 'proc_lotes_detalle.codigo',
                                'proc_lotes_tickets.fecha_registro',
                                'maest_aeropuertos.codigo_oaci as oaci',
                                'maest_tiempo_tickets.nombre as tiempo',
                                'proc_lotes_detalle.precio',
                                'proc_lotes_detalle.vendido')
                        ->get()
                
                //->toSql()
                ;

                /*
                  dd($LotesTickets);
                  $LotesTickets = LotesTickets::whereIn("aeropuerto_id", Auth::user()->lista_aeropuertos)
                  ->with("aeropuerto", 'tiempo', 'detalle')
                  ->get();
                 */
        

                $data = Datatables::of($LotesTickets)
                        ->addColumn('fecha_registro2', function ($row) {

                            return showDate($row->fecha_registro, 'full');
                        })
                        ->addColumn('precio2', function ($row) {

                            return muestraFloat($row->precio);
                        })
                        ->addColumn('estado', function ($row) {
                            return ($row->vendido == true ? 'vendido' : 'disponible');
                        })
                        ->addColumn('codigo', function ($row) {
                    return "****" . substr($row->codigo, strlen($row->codigo) - 4, 4);
                });
                ;
                return $data->toJson();
            } else {

                return view('tickets.lista');
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function ExcelTickets(){
        $aeropuerto_id = Encryptor::decrypt(request('aeropuerto_id'));
        $rango_fecha = request('rango_fecha');
        $fechas = explode(' - ', $rango_fecha);
        $fechaInicio = Carbon::parse(saveDate($fechas[0]))->startOfDay();
        $fechaFin = Carbon::parse(saveDate($fechas[1]))->endOfDay();
        

        if (\Request::isMethod('get')) {
            return Excel::download(new TicketsExport($fechaInicio, $fechaFin, $aeropuerto_id), 'excel.xlsx');
        }else{
            $where [] =[DB::raw("1"), "1"];
            if ($aeropuerto_id != 0){
                $where [] =["aeropuerto_id", $aeropuerto_id];
            }
            $tickets = \App\Models\VentaTickets::with('detalle.ticket.lote', 'usuarios')
            ->where($where)
            ->whereBetween('fecha', [$fechaInicio, $fechaFin])
            ->whereHas('detalle.ticket', function ($query) {
                $query->where('vendido', true);
            })
            ->count();

            if ($tickets == 0) {
                $result['status'] = 0;
                $result['type'] = 'info';
                $result['message'] = "No hay datos registrados con los parámetros en el rango de fecha indicado";
                $result['data'] = [];

                return $result;
            }
        }
    }
}


