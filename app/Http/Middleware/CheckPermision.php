<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Browser;

class CheckPermision {

    protected $routesFree = array('home', 'theme', 'set_theme', 'dashboard', 'create_img', 'search_vuelos', 'view_invoice',
        'change_pwd', 'refresh_data', 'get_prod_serv_ext2', 'check_reg_online', 'regenerate', 'get_homologacion', 'proforma',
        "aeronaves_plus", "pilotos_plus", "get_servs_ext", 'get_prodservicios', 'get_vuelos_fact', 'view_factura', 'view_detail_fly',
        'view_proforma', 'view_fact', 'get_aeronave', 'send_prof_email', 'down_proforma', 'down_factura', 'get_total_serv', 'get_total_serv2' ,'manual_pdf',
        'flights', 'reporte_vuelos', 'get_servicios_operacion', 'down_pagos', 'reporte_proforma', 'ExcelProforma', 'ExcelFactura', 'generatePdf', 'ExcelTickets', 'reporte_cupones', 'aeronaves',
        'proforma_services','proforma_carga','proforma_dosa','list_service_avc','service_single_avc', 'inventario_equipos', 'ExcelInventarioEquipo'
    );
    protected $routesFreeSub = array("facturas", 'registro_aviacion_general', 'aviacion_general_tortuga', 'aviacion_general_tortuga_create',
        'facturacion_general_tortuga', 'facturacion_aviacion_general', 'pospago_aviacion_general', 'facturacion_pospago', 'vuelos', 'facturar_pospago',
        'get_vuelos', 'membresias', 'concesiones', 'recaudacion', 'pantallas', 'aeropuertos' );

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next) {
        //return $next($request);




        if (Auth::user()->active == 1) { // if not is blocked
            $User = \App\Models\User::find(Auth::user()->id);

            $position_point = strpos(\Request::route()->getName(), '.');
            if ($position_point === false) {
                $nameRoute = \Request::route()->getName();
                $nameRouteSub = "";
            } else {

                $nameRoute = explode(".", \Request::route()->getName());

                $nameRouteSub = $nameRoute[1];
                $nameRoute = $nameRoute[0];
            }

            $LoggedUser = \App\Models\LoggedUser::where('user_id', Auth::user()->id)->first();

            //dd(Auth::user()->id);
            if ($LoggedUser != null) {  // if exists on table logged_user
                $security = $LoggedUser->platform == Browser::platformName() &&
                        $LoggedUser->browser == Browser::browserName() &&
                        $LoggedUser->browser_engine == Browser::browserEngine() &&
                        $LoggedUser->ip == \Request::ip();
                        $security = true;

                if ($security == true) {






                    $Process = \App\Models\Process::where('route', $nameRoute)->first();
                    // dd($nameRoute);
                    if (!in_array($nameRoute, $this->routesFree)) {// if require permission
                        if ($Process != null) {
                            //dd($User->getProfile->getProcesses->pluck('route')->toArray());
                            $has_permision = in_array($nameRoute, $User->getProfile->getProcesses->pluck('route')->toArray()); // if has permision
                            //dd($User->getProfile->getProcesses->toArray());
                            if ($has_permision) {
                                //var_dump($nameRouteSub);
                                if ($nameRouteSub != "") {
                                    $has_permision = false;

                                    if (!in_array($nameRoute, $this->routesFreeSub)) {


                                        //dd($User->getProfile->getProcesses->toArray());
                                        foreach ($User->getProfile->getProcesses->toArray() as $value) {
                                            if ($value['route'] == $nameRoute) {

                                                $actions = explode("|", $value['pivot']['actions']);

                                                $has_permision = in_array($nameRouteSub, $actions);
                                                break;
                                            }
                                        }
                                        //dd($nameRoute);
                                        $has_permision = ($has_permision || in_array($nameRoute, $this->routesFreeSub));

                                        if ($has_permision) {


                                            Session::put('currentModule', $nameRoute);

                                            if (Auth::user()->change_password == true) {
                                                return redirect()->route('change_pwd');
                                            }

                                            return $next($request);
                                        } else {
                                            //return redirect()->route('errors.permission_denied');

                                            $RestrictedAccess = new \App\Models\RestrictedAccess();
                                            $RestrictedAccess->user_id = Auth::user()->id;
                                            $RestrictedAccess->process_id = $Process->id;
                                            $RestrictedAccess->date_in = now();
                                            $RestrictedAccess->ip = \Request::ip();
                                            $RestrictedAccess->save();

                                            if ($request->expectsJson()) {

                                                $result['status'] = -1;
                                                $result['title'] = __('');
                                                $result['message'] = __('Permission Denied');
                                                $result['data'] = null;
                                                $result['type_message'] = 'error';
                                                $result['redirect'] = '';
                                                return response()->json($result, 200);
                                            } else {
                                                return redirect()->route('errors.permission_denied');
                                            }
                                        }
                                    } else {
                                        if (Auth::user()->change_password == true) {
                                            return redirect()->route('change_pwd');
                                        }
                                        Session::put('currentModule', $nameRoute);
                                        return $next($request);
                                    }
                                } else {
                                    // dd(Auth::user()->change_password);
                                    if (Auth::user()->change_password == true) {
                                        return redirect()->route('change_pwd');
                                    }
                                    Session::put('currentModule', $nameRoute);
                                    return $next($request);
                                }
                            } else {

                                //dd("Modulo Restringido");
                                // return redirect()->route('errors.permission_denied');

                                $RestrictedAccess = new \App\Models\RestrictedAccess();
                                $RestrictedAccess->user_id = Auth::user()->id;
                                $RestrictedAccess->process_id = $Process->id;
                                $RestrictedAccess->date_in = now();
                                $RestrictedAccess->ip = \Request::ip();
                                $RestrictedAccess->save();

                                if ($request->expectsJson()) {

                                    $result['status'] = -1;
                                    $result['title'] = __('');
                                    $result['message'] = __('Permission Denied');
                                    $result['data'] = null;
                                    $result['type_message'] = 'error';
                                    $result['redirect'] = '';
                                    return response()->json($result, 200);
                                } else {

                                    return redirect()->route('errors.permission_denied');
                                }
                            }
                        } else {
                            dd('Modulo no Registrado');
                        }
                    } else {
                       // dd("ss");
                        return $next($request);
                    }
                } else {
                    return redirect('/logout');
                }
            } else {
                $return = __("Your session has been restarted"); // redirect('/logout');
            }
        } else {
            $return = __("Your user has been Blocked"); //return redirect('/logout');
        }
        //dd($request->expectsJson());
        if ($request->expectsJson()) {
            $result['status'] = -1;
            $result['title'] = __('');
            $result['message'] = $return;
            $result['data'] = null;
            $result['type_message'] = 'error';
            $result['redirect'] = '';
        } else {
            return redirect('/logout');
        }
    }

}
