<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Browser;

class Authenticate extends Middleware {

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request) {
        if (!$request->expectsJson()) {
            //dd(session()->get('ID_SESSION_ACTIVE'));
            $LoggedUser = \App\Models\LoggedUser::where('platform', Browser::platformName())->where('browser', Browser::browserName())
                    ->where('browser_engine', Browser::browserEngine())->where('ip', $request->ip())
                    ->first();
            if ($LoggedUser != null) {
                $AccessHistory = \App\Models\AccessHistory::where('user_id', $LoggedUser->user_id)->whereNull('date_out');
                if ($AccessHistory != null) {
                    $AccessHistory->update(['date_out' => date('Y-m-d H:i:s')]);
                }
                $LoggedUser->delete();
            }
            return route('login');
        }
    }

}
