<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Proforma extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_proforma";
    protected $appends = ['crypt_id', 'aeronave', 'aeropuerto', 'cliente', 'nro_proforma2', 'total', 'fecha_proforma2', 'tipo_documento' , 'estado', 'total_monto_abonado', 'deuda_actual'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    
    public function getEstadoAttribute() {
        return $this->anulada == true ? "ANULADA" : "ACTIVA";
    }
    
    public function getTipoDocumentoAttribute() {
        return $this->factura_id == null ? "PROFORMA" : "FACTURA";
    }

    public function getFechaProforma2Attribute() {
        return showDate($this->fecha_proforma);
    }

    public function getNroProforma2Attribute() {
        return showCode($this->id);
    }

    public function getAeronaveAttribute() {
        return $this->getAeronave;
    }

    public function getAeronave() {
        return $this->belongsTo(Aeronaves::class, 'aeronave_id');
    }

    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }

    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }

    public function getCliente1Attribute() {
        return $this->belongsTo(Clientes::class, 'cliente_id');
    }

    public function getClienteAttribute() {
        return $this->getCliente1Attribute;
    }

    public function getDetalleAttribute() {


        return $this->getDetalle;
    }

    public function getDetalle2() {
        $items = [];

        foreach ($this->getDetalle as $value) {

            if (isset($items[$value->codigo])) {

                $items[$value->codigo]->cantidad += $value->cantidad;
                $items[$value->codigo]->precio += $value->precio;
            } else {
                $items[$value->codigo] = $value;
            }
        }
        return $items;
    }

    public function getDetalle() {
        return $this->hasMany(ProformaDetalle::class, 'proforma_id')->orderBy('codigo');
        ;
    }

    public function getTotal() {

        $sum = 0;
        foreach ($this->getDetalle as $value) {
            $sum += $value->precio * (1 + ($value->iva / 100));
        }
        return round($sum, 2);
    }

    public function getTotalAttribute() {
        return $this->getTotal();
        /*
          $sum = 0;
          foreach ($this->getDetalle as $value) {
          $sum += $value->precio * (1 + ($value->iva / 100));
          }
          return $sum;
         */
    }

    public function getProformaPago() {
        return $this->hasMany(ProformaPago::class, 'proforma_id');
        ;
    }
   

    public function getTotalMontoAbonado() {
        $sum = 0;
        foreach ($this->getProformaPago as $value) {
            $sum += $value->monto;
        }
        return round($sum, 2);
    }

    public function getTotalMontoAbonadoAttribute() {
        // return 0;
        return $this->getTotalMontoAbonado();
    }
    public function getDeudaActualAttribute(){
        return $this->getTotal() - $this->getTotalMontoAbonado();
    }

    public function getBaseImponible() {
        $detalle = $this->getDetalle;
        $base_imponible = 0;
        foreach($detalle as $value){
        if ($value->iva > 0) {
            $base_imponible += $value->precio;
        } 

        } 
        return $base_imponible;
    }

    public function getExento() {
        $detalle = $this->getDetalle;
        $excento = 0;
        foreach($detalle as $value){
        if ($value->iva == 0) {
            $excento += $value->precio;
        } 

        } 
        return $excento;
    }

    public function getIva() {
        $detalle = $this->getDetalle;
        $iva = 0;
        foreach($detalle as $value){
            if ($value->iva > 0) {
                $iva += $value->precio * ($value->iva / 100);
            }
        } 
        return $iva;
    }

    public function usuarios(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function vuelos(){
        return $this->hasMany(Vuelos::class);
    }

    // public function tipo_vuelo(){
    //     $vuelos = $this->vuelos;
    //     foreach($vuelos as $vuelo){

    //     }
    // }
}
