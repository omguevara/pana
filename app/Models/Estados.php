<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estados extends Model {

    use HasFactory;

    protected $table = "conf_estados";
    protected $fillable = ['codigo', 'nombre', 'ip', 'activo', 'estacion_id'];
    public $timestamps = false;
    
    protected $hidden = [
        'mapa' 
    ];

}
