<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Clasificacion extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_clasificaciones";
    protected $appends = ['crypt_id'];
    public $timestamps = false;

}
