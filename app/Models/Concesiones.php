<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class Concesiones extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = 'proc_concesiones';
    public $timestamps = false;
    protected $appends = ['crypt_id', 'files', 'fecha_inicio2', 'fecha_fin2'];
    protected $hidden = [
        'id'
    ];
    
    
    protected $fillable = ['concesion', 'local_id', 'empresa_id', 'actividad_id', 'ingresos_brutos', 'canon_fijo', 'fecha_inicio', 'fecha_fin'];

    public function getFechaInicio2Attribute() {
        return showDate($this->fecha_inicio);
    }

    public function getFechaFin2Attribute() {
        return showDate($this->fecha_fin);
    }
    
    
    public function locales() {
        return $this->belongsTo(Locales::class, 'local_id', 'id');
    }

    public function empresas() {
        return $this->belongsTo(Clientes::class, 'empresa_id', 'id');
    }

    public function actividades() {
        return $this->belongsTo(Actividades::class, 'actividad_id', 'id');
    }

    public function getFilesAttribute() {
        $ds = DIRECTORY_SEPARATOR;
        $concesion = $this->id;
        $path = storage_path('Comercializacion' . $ds . 'Concesiones' . $ds . $concesion);
        $result = [];
        if (\Illuminate\Support\Facades\Storage::exists($path)) {
            $entry = scandir($path);
            
            foreach ($entry as $key => $value) {
                if ($value != "." && $value != "..") {
                    $result[] = Upper($value);
                }
            }
        }
        return $result;
    }

    // public function getEmpresasAttribute() {
    //     return $this->empresas;
    // }
}
