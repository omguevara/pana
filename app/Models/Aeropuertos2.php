<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Aeropuertos2 extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_aeropuertos";
    protected $appends = ['crypt_id',  'full_nombre']; 
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getUbicacionesAttribute() {
        return $this->getUbicaciones;
    }

    public function getUbicaciones() {
        return $this->hasMany(Ubicaciones::class, 'aeropuerto_id')->orderBy("maest_ubicaciones.id");
    }

    public function getFeriadosAttribute() {
        return $this->getFeriados;
    }

    public function getFeriados() {
        return $this->hasMany(FeriadosAeropuertos::class, 'aeropuerto_id');
    }

    public function getFullNombreAttribute() {
        if ($this->pais_id != 1 ) {
            return Upper($this->codigo_oaci . ' / ' . $this->nombre . ' ('.$this->getPais->name_country.')');
        }
        return Upper($this->codigo_oaci . ' / ' . $this->nombre);
    }

    public function getCategoria() {
        return $this->belongsTo(Categorias::class, 'categoria_id');
    }

    public function getCategoriaAttribute() {
        return $this->getCategoria;
    }
    
    public function getPais() {
        return $this->belongsTo(Paises::class, 'pais_id');
    }

    public function getPaisAttribute() {
        return $this->getPais;
    }
    

    public function getTurno($hora = null) {
        //  dd($hora);
        if ($hora === null) {
            return null;
        } else {

            $hora_ocaso = str_replace(":", "", $this->hora_ocaso);
            $hora_amanecer = str_replace(":", "", $this->hora_amanecer);
            return (($hora >= ((int) ($hora_ocaso)) || $hora <= ((int) ($hora_amanecer))) ? 2 : 1);
        }
    }

}
