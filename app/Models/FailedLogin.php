<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class FailedLogin extends Model
{
  use EncryptationId;

  protected $table = "seg_failed_logins";
  protected $fillable = ['user_id','date_in','ip'];
  
  protected $appends = ['crypt_id'];
  public $timestamps = false;
}
