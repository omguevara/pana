<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Hangares extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_hangares";
    protected $appends = ['crypt_id', 'aeropuerto', 'ubicacion'];
    public $timestamps = false;

    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }
    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }
    
    
    public function getUbicacionAttribute() {
        return $this->getUbicacion;
    }
    public function getUbicacion() {
        return $this->belongsTo(UbicacionesHangares::class, 'aeropuerto_id');
    }

}
