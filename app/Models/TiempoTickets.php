<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class TiempoTickets extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_tiempo_tickets";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    /*
    protected $hidden = [
        'id'
    ];
    */

}
