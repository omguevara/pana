<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class Locales extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = 'maest_locales';
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    protected $hidden = [
        'id'
    ];

    public function historialConcesiones() {
        return $this->hasMany(Concesiones::class, 'local_id');
    }

    public function concesion() {
        $today = date('Y-m-d');
        // dd($today);
        return $this->hasOne(Concesiones::class, 'local_id')->whereBetween(DB::raw("'$today'"), [DB::raw('fecha_inicio'), DB::raw('fecha_fin')]);
    }

    public function usos_local() {
        return $this->belongsTo(LocalesUsos::class, 'uso_id', 'id');
    }
    
    public function aeropuerto() {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id', 'id');
    }

}
