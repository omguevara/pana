<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Paises extends Model
{
    use HasFactory, EncryptationId;
    
    protected $table = "conf_paises";
    protected $fillable = ['codigo', 'nombre', 'ip',  'activo', 'estacion_id'];
    protected $appends = ['crypt_id'];
    
    public $timestamps = false;
    
   
}
