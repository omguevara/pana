<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Process extends Model {

    use HasFactory;

    protected $table = "seg_processes";
    protected $fillable = ['name_process', 'description', 'icon', 'route', 'menu_id'];
    
    protected $appends = ['profile', 'profile_array'];
    public $timestamps = false;

    public function getProfileArrayAttribute() {
        $list_id = array();
        foreach ($this->getProfile as $key => $value) {
            $list_id[] = $value->id;
        }
        return $list_id;
    }

    public function getProfileAttribute() {
        return $this->getProfile;
    }

    public function getProfile() {
        return $this->belongsToMany(Profile::class, 'seg_profile_processes')
                        ->withPivot('profile_id', 'process_id');
    }

}
