<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class VuelosTortuga extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_vuelos_tortuga";
    protected $appends = ['crypt_id', 'piloto', 'aeropuerto'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getPilotoAttribute() {

        return $this->getPiloto;
    }
    
    public function getPiloto() {

        return $this->belongsTo(Pilotos::class, 'piloto_id');
    }
    
    
    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }
    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }

}
