<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Solicitudes extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_solicitudes";
    protected $appends = ['crypt_id', 'responsable'];
    public $timestamps = false;
    
    protected $hidden = [
        'id'
    ];

    public function getResponsable() {
        return $this->belongsTo(Clientes::class, 'responsable_id');
    }

    public function getResponsableAttribute() {
        return $this->getResponsable;
    }
    /*
    public function getReservacionesAttribute() {
        return $this->getReservaciones;
    }
    
    public function getReservaciones() {
        return $this->hasMany(Reservaciones::class, 'solicitud_id');
    }
    */
}
