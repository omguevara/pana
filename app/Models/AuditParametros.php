<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class AuditParametros extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_audit_parametros";
    protected $fillable = [];
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    protected $hidden = [
        'id'
    ];
}
