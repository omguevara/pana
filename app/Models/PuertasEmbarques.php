<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class PuertasEmbarques extends Model
{
    use HasFactory, EncryptationId;
    
    
    protected $table = "maest_puertas_embarques";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    
    
    
    public function getAeropuerto() {
        return $this->belongsTo(Aereopuertos::class, 'aeropuerto_id');
    }
    
    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }
}
