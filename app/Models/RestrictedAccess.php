<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class RestrictedAccess extends Model {

    use EncryptationId;

    protected $table = "seg_restricted_accesses";
    protected $fillable = ['id', 'user_id', 'process_id', 'date_in', 'ip'];
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

}
