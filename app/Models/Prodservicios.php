<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Prodservicios extends Model
{
    use HasFactory, EncryptationId;
    
    
    protected $table = "maest_prod_servicios";
    protected $appends = ['crypt_id', 'full_descripcion'];
    public $timestamps = false;
    
    
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'id'
    ];
    
    public function getFullDescripcionAttribute(){
        return str_pad($this->codigo, 10, "*", STR_PAD_RIGHT) .$this->descripcion;
    }
}
