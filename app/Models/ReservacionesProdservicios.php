<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class ReservacionesProdservicios extends Model
{
    use HasFactory, EncryptationId;
    
    protected $table = "proc_reservaciones_prodservicios";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
}
