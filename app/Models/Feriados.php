<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Feriados extends Model
{
    use HasFactory,
        EncryptationId;
    
    
    protected $table = "maest_feriados";
    protected $fillable = [ 'fecha'];
    protected $appends = ['crypt_id', 'fecha2'];
    public $timestamps = false;
    
    
    
    public function getFecha2Attribute(){
        return showDate($this->fecha);
    }
}
