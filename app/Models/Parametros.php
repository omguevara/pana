<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Parametros extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "conf_parametros";
    protected $fillable = ['nombre', 'descripcion', 'valor', 'user_id', 'fecha_act', 'ip'];
    public $timestamps = false;
    protected $appends = ['crypt_id'];

}
