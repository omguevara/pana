<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class Clientes extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_clientes";
    protected $appends = ['crypt_id', 'rif', 'full_razon_social'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    
    
    public function getRifAttribute() {
        return $this->tipo_documento."-".$this->documento;
    }
    
    public function getFullRazonSocialAttribute() {
        return $this->tipo_documento."-".$this->documento." ".Upper($this->razon_social);
    }
    
    

}
