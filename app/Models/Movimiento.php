<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Movimiento extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_movimientos";
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getOrigenDestino(){
        return $this->belongsTo(Aeropuertos2::class, 'origen_destino_id');
    }
    public function getAerolinea(){
        return $this->belongsTo(Clientes::class, 'aerolinea_id');
    }
    public function getAeropuerto(){
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }
}
