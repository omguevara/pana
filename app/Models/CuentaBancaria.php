<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class CuentaBancaria extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "maest_cuenta_bancarias";  
   
    protected $appends =['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getBancos(){
        return $this->belongsTo(Bancos::class, 'banco_id');
    }
}
