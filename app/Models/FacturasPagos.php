<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class FacturasPagos extends Model {

    use HasFactory,
        EncryptationId;

    protected $appends = ['crypt_id'];
    protected $table = "proc_facturas_pagos";
    protected $hidden = [
         'id'
    ];
    public $timestamps = false;
    
    
    public function getFormaPago() {
        return $this->belongsTo(FormaPagos::class, 'forma_pago_id');
    }
    
}
