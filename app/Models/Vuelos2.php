<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use Carbon\Carbon;

class Vuelos2 extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_vuelos";
    protected $appends = ['crypt_id', 'aeropuerto', 'fecha_operacion2'];
    private $TIPO_OPERACION = [1 => "LLEGADA", 2 => "SALIDA"];
    private $TIPO_VUELO = [1 => "NACIONAL", 2 => "INTERNACIONAL"];
    private $TIPO_AVIACION = [1 => "GENERAL", 2 => "OFICIAL", 3 => "MILITAR"];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    
    
    public function getFechaOperacion2Attribute() {
        return showDate($this->fecha_operacion);
    }
    
    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }

    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos2::class, 'aeropuerto_id');
    }
    

}
