<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class ConcesionEquipo extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = 'maest_concesiones_equipos';
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    protected $hidden = [
        'id'
    ];
}
