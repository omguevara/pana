<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use Carbon\Carbon;
use DB;

class VentaTickets extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_venta_tickets";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    public function detalle() {
        return $this->hasMany(VentaTicketsDetalle::class, 'venta_id');
    }
    public function usuarios() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
