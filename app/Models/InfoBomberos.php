<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoBomberos extends Model
{
    use HasFactory;
    protected $table ='proc_info_bomberos';
    public $timestamps = false;
}
