<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class LoggedUser extends Model
{
    use EncryptationId;

    protected $table = "seg_logged_users";
    protected $fillable = [
        
        'user_id',
        'date_in',
        'ip'
    ];
    
    protected $appends = ['crypt_id'];    
    public $timestamps = false;
}
