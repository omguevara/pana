<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use Carbon\Carbon;
use DB;

class Vuelos extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_vuelos";
    protected $appends = ['crypt_id', 'aeronave', 'piloto', 'operacion', 'aeropuerto', 'tipo_vuelo', 'hora_llegada2', 'hora_salida2',
        'fecha_operacion2', 'hora_operacion2', 'servicios_extra', 'servicios_extra_select', 'total', 'observaciones_facturacion2',
        'tipo_operacion', 'tipo_aviacion', 'pasajeros', 'origen_destino', 'puesto', 
        'fecha_operacion_full', 'membresia'];
    private $TIPO_OPERACION = [1 => "LLEGADA", 2 => "SALIDA"];
    private $TIPO_VUELO = [1 => "NACIONAL", 2 => "INTERNACIONAL"];
    private $TIPO_AVIACION = [1 => "GENERAL", 2 => "OFICIAL", 3 => "MILITAR"];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getMembresiaAttribute() {
        return Membresia::where("aeronave_id", $this->aeronave_id)->whereBetween(DB::raw("'" . $this->fecha_operacion . "'"), [DB::raw("fecha_desde"), DB::raw("fecha_hasta")])->first();
    }

    public function getPuestoAttribute() {
        return $this->getPuesto;
    }

    public function getPuesto() {
        return $this->hasOne(AeronavesPuestos::class, 'vuelo_id', 'id');
    }

    public function getObservacionesFacturacion2Attribute() {


        if (trim($this->observaciones_facturacion) != '' && $this->observaciones_facturacion != null) {
            $info = explode('|', $this->observaciones_facturacion);

            //return "Documento: ".$info[0].$info[1]."<br/>Razón Social: ".$info[2]."<br/>Telefono: ".$info[3]."<br/>Direcci贸n: ".$info[4];
            return $info;
        } else {
            return array('', '', '', '', '');
        }
    }

    public function getHoraLlegada2Attribute() {
        if ($this->hora_llegada == null) {
            return "";
        } else {
            return Carbon::parse($this->hora_llegada)->format('d/m/Y H:i:00');
        }
    }

    public function getHoraSalida2Attribute() {
        if ($this->hora_salida == null) {
            return "";
        } else {
            return Carbon::parse($this->hora_salida)->format('d/m/Y H:i:00');
        }
    }

    public function getServiciosExtraAttribute() {
        //$prodServ = json_decode($this->prodservicios_extra);
        //dd($prodServ);
        $cont = 0;
        $prodServ = [];

        foreach (json_decode($this->prodservicios_extra) as $key => $value) {

            foreach ($value as $key2 => $value2) {
                $Prodservicios = Prodservicios::find($value2);

                $prodServ[$cont]['descripcion'] = $Prodservicios->descripcion;
                $prodServ[$cont]['codigo'] = $Prodservicios->codigo;
                $prodServ[$cont]['cant'] = $key2;
                $cont++;
            }
        }
        return $prodServ;
    }

    public function getServiciosExtraSelectAttribute() {

        $prodServ = [];
        foreach (json_decode($this->prodservicios_extra) as $key => $value) {

            foreach ($value as $key2 => $value2) {
                $Prodservicios = Prodservicios::find($value2);

                $prodServ[$key2 . ":" . $Prodservicios->crypt_id] = $Prodservicios->crypt_id;
            }
        }
        return $prodServ;
    }

    public function getOperacionAttribute() {
        return showCode($this->numero_operacion);
    }

    public function getFechaOperacion2Attribute() {
        return showDate($this->fecha_operacion);
    }

    public function getFechaOperacionFullAttribute() {
        if ($this->hora_operacion != null) {
            return showDate($this->fecha_operacion . ' ' . $this->hora_operacion, 'full');
        } else {
            if ($this->hora_salida == null) {
                return showDate($this->hora_llegada, "full");
            } else {
                return showDate($this->hora_salida, "full");
            }
        }
    }

    public function getHoraOperacion2Attribute() {
        if ($this->hora_operacion == null) {
            if ($this->hora_salida == null) {
                return showDate($this->hora_llegada, "hour");
            } else {
                return showDate($this->hora_salida, "hour");
            }
        } else {
            return $this->hora_operacion;
        }
    }

    public function getTipoOperacionAttribute() {
        if ($this->tipo_operacion_id == null) {
            return "";
        } else {
            return $this->TIPO_OPERACION[$this->tipo_operacion_id];
        }
    }

    public function getTipoAviacionAttribute() {
        if ($this->tipo_aviacion_id == null) {
            return "";
        }
        return $this->TIPO_AVIACION[$this->tipo_aviacion_id];
    }

    public function getTipoVueloAttribute() {
        return $this->TIPO_VUELO[$this->tipo_vuelo_id];
    }

    public function getTotalAttribute() {

        $sum = 0;
        foreach ($this->getDetalle as $value) {
            $sum += round($value->precio, 2) * (1 + ($value->iva_aplicado / 100));
        }

        return round($sum, 2);
    }

    public function getDetalle() {
        return $this->hasMany(VuelosDetalle::class, 'vuelo_id')->orderBy('orden');
    }

    public function getPasajeros() {
        return $this->hasMany(VuelosPasajeros::class, 'vuelo_id');
    }

    public function getPasajerosAttribute() {
        return $this->getPasajeros;
    }

    public function getAeronaveAttribute() {
        return $this->getAeronave;
    }

    public function getAeronave() {
        return $this->belongsTo(Aeronaves::class, 'aeronave_id');
    }

    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }

    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos2::class, 'aeropuerto_id');
    }

    public function getUsuarioCreateAttribute() {
        return $this->getUsuarioCreate;
    }

    public function getUsuarioCreate() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUsuarioEditAttribute() {
        //return $this->getUsuarioEdit;

        if ($this->getUsuarioEdit != null) {
            return $this->getUsuarioEdit;
        } else {
            return (object) array("id" => "",
                        "document" => "",
                        "name_user" => "",
                        "full_nombre" => "",
                        "surname_user" => "",
                        "username" => "",
                        "password" => "",
                        "phone" => "",
                        "email" => "",
                        "change_password" => "",
                        "active" => "",
                        "avatar" => "",
                        "profile_id" => "",
                        "aeropuerto_id" => "",
                        "theme" => "",
                        "user_id" => "",
                        "register_date" => "",
                        "ip" => "");
        }
    }

    public function getUsuarioEdit() {
        return $this->belongsTo(User::class, 'user_edit_id');
    }

    public function getOrigenDestinoAttribute() {
        if ($this->getOrigenDestino != null) {
            return $this->getOrigenDestino;
        } else {
            return (object) array("id" => "", "full_nombre" => "", "codigo_oaci" => "", "codigo_iata" => "", "nombre" => "", "activo" => "", "pertenece_baer" => "", "iva" => "",
                        "hora_ocaso" => "", "hora_amanecer" => "", "tasa_euro" => "", "tasa_petro" => "", "categoria_id" => "");
        }
    }

    public function getOrigenDestino() {
        return $this->belongsTo(Aeropuertos::class, 'orig_dest_id');
    }

    public function getPilotoAttribute() {
        return $this->getPiloto;
    }

    public function getPiloto() {

        return $this->belongsTo(Pilotos::class, 'piloto_id');
    }

    public function getDetalleByRecord($exento_tasa, $exento_dosa) {




        $data['tipo_vuelo_id'] = 2; //Vuelos General
        $data['nacionalidad_id'] = $this->tipo_vuelo_id; // NAcional

        $data['aeropuerto_id'] = $this->aeropuerto_id;

        if ($this->hora_salida != null) {
            $hora_operacion = explode(' ', $this->hora_salida);
            $data['hora_vuelo'] = $hora_operacion[1];
        } else {
            if ($this->hora_llegada != null) {
                $hora_operacion = explode(' ', $this->hora_llegada);
                $data['hora_vuelo'] = $hora_operacion[1];
            }
        }

        if ($this->hora_llegada != null) {
            $data['hora_llegada'] = $this->hora_llegada;
        } else {
            $data['hora_llegada'] = null;
        }

        if ($this->hora_salida != null) {
            $data['hora_salida'] = $this->hora_salida;
        } else {
            $data['hora_salida'] = null;
        }

        $data['aeronave_id'] = $this->aeronave_id;

        $data['cant_pasajeros'] = $this->pasajeros_embarcados;
        $data['fecha_vuelo'] = $this->fecha_operacion . " 00:00:00";

        if ($this->prodservicios_extra == null) {
            $data['serv_extra'] = [];
        } else {

            $prodservicios_extra = json_decode($this->prodservicios_extra);
            if (count($prodservicios_extra) > 0) {
                $data['serv_extra']['ids'] = [];

                // dd($prodservicios_extra);
                foreach ($prodservicios_extra as $value) {
                    // dd($value->{key($value)});
                    //$servp = explode(":", $value);
                    $data['serv_extra']['ids'][] = $value->{key($value)};
                    $data['serv_extra']['cant'][\App\Helpers\Encryptor::encrypt($value->{key($value)})] = key($value);
                }
            } else {
                $data['serv_extra'] = [];
            }
        }

        $data['exento_dosa'] = !$exento_tasa;
        $data['exento_tasa'] = !$exento_dosa;

        $servicios_facturar = app(\App\Http\Controllers\Controller::class)->{'getProdServ'}($data);

        return $servicios_facturar;
    }
    public function proforma() {

        return $this->belongsTo(Proforma::class, 'proforma_id');
    }
}
