<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Ubicaciones extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_ubicaciones";
    public $timestamps = false;
    protected $appends = ['crypt_id', 'lugares'];
    
    public function getLugaresAttribute() {
        return $this->getLugares;
    }
    
    public function getLugares() {
        return $this->hasMany(Lugares::class, 'ubicacion_id')->orderBy("maest_lugares.id");
    }

}
