<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfileProcess extends Model {

    use HasFactory;

    protected $table = "seg_profile_processes";
    protected $fillable = ['date_register', 'process_id', 'profile_id', 'actions'];
    // protected $connection = "mysql";
    // protected $appends = ['crypt_id'];
    public $timestamps = false;

}
