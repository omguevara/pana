<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class ReservacionesPagos extends Model
{
    use HasFactory, EncryptationId;
    
    protected $table = "proc_reservaciones_pagos";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
}
