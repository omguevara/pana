<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use Carbon\Carbon;
use DB;

class VentaTicketsDetalle extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_venta_tickets_detalle";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    
    public function ticket() {
        return $this->belongsTo(LotesDetalle::class, 'ticket_id');
    }
    

}
