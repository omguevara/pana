<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class AccessHistory extends Model
{
  use EncryptationId;

  protected $table = "seg_access_histories";
  protected $fillable = [
      
      'user_id',
      'date_in',
      'date_out',
      'ip'
  ];
   
  protected $appends = ['crypt_id'];
  public $timestamps = false;
}
