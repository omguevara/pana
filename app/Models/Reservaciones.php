<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class Reservaciones extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_reservaciones";
    protected $appends = ['crypt_id', 'fecha2', 'solicitud', 'origen', 'destino', 'aeronave', 'id_serv_ext', 'pago', 'piloto'];
    public $timestamps = false;
    
    protected $hidden = [
        'id'
    ];
    
    
    public function getIdServExtAttribute() {
        $result_serv_ext = [];
        $serv_ext = ReservacionesProdservicios::where("reservacion_id", $this->id)->get();
        foreach($serv_ext as $value){
            $result_serv_ext[] = $value->prodservicio_id;
        }
        return $result_serv_ext;
        
        
    }
    
    public function getPiloto(){
        return $this->belongsTo(Capitanes::class, 'capitan_id');
    }
    
    public function getPilotoAttribute(){
        return $this->getPiloto;
    }
    
    public function getPago(){
        return $this->hasMany(ReservacionesPagos::class, 'reservacion_id', 'id');
    }
    
    public function getPagoAttribute(){
        return $this->getPago;
    }
    
    public function getFecha2Attribute() {
        return showDate($this->fecha);
    }

    public function getSolicitud() {
        return $this->belongsTo(Solicitudes::class, 'solicitud_id');
    }

    public function getSolicitudAttribute() {
        return $this->getSolicitud;
    }

    public function getOrigen() {
        return $this->belongsTo(Aeropuertos::class, 'origen_id');
    }
    
    public function getOrigenAttribute() {
        return $this->getOrigen;
    }
    
    
    public function getDestino() {
        return $this->belongsTo(Aeropuertos::class, 'destino_id');
    }
    
    public function getDestinoAttribute() {
        return $this->getDestino;
    }
    
    public function getAeronave() {
        return $this->belongsTo(Aeronaves::class, 'aeronave_id');
    }
    
    public function getAeronaveAttribute() {
        return $this->getAeronave;
    }
}
