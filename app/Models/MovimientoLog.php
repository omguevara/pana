<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class MovimientoLog extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_movimientos_log";
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
}
