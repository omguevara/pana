<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Pilotos extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_pilotos";
    protected $appends = ['crypt_id', 'full_name', 'documento2'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getFullNameAttribute() {
        return $this->tipo_documento.$this->documento." ".$this->nombres." ".($this->apellidos==null ? "":$this->apellidos); 
    }
    
    public function getDocumento2Attribute() {
        return $this->tipo_documento.$this->documento; 
    }

}
