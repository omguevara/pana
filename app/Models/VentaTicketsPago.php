<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use Carbon\Carbon;
use DB;

class VentaTicketsPago extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_venta_tickets_pago";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getMetodoPago() {
        return $this->belongsTo(FormaPagos::class,'forma_pago_id');
    }
}
