<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class UsuariosAeropuertos extends Model
{
    use HasFactory,
        EncryptationId;
    
    protected $table = "proc_usuarios_aeropuertos";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
}
