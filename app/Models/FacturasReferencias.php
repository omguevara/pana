<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class FacturasReferencias extends Model
{
     use HasFactory,
        EncryptationId;

    protected $appends = ['crypt_id'];
    protected $table = "proc_facturas_referencias";
    protected $hidden = [
         'id'
    ];
    public $timestamps = false;
    
    /*
    public function getPuntoVenta() {
        return $this->belongsTo(FormaPagos::class, 'forma_pago_id');
    }
    */
}
