<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoSeguridad extends Model
{
    use HasFactory;
    protected $table ='proc_info_seguridad';
    public $timestamps = false;
}
