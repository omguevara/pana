<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Video extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_videos";
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];


    public function getAeropuerto(){
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }
}
