<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoCertificacionesLog extends Model
{
    use HasFactory;
    protected $table ='proc_info_cerfiticaciones_log';
    public $timestamps = false;
}
