<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;
use App\Models\Clientes as Aerolineas;

class AeropuertoAerolinea extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = 'proc_aeropuertos_aerolineas';
    protected $appends = ['aerolinea_crypt_id'];
    protected $hidden = [
        'id'
    ];
    public $timestamps = false;

    public function getAerolineaCryptIdAttribute(){
        return Encryptor::encrypt($this->attributes['aerolinea_id']);
    }
    
    public function aeropuertos()
    {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }

    public function aerolineas()
    {
        return $this->belongsTo(Aerolineas::class, 'aerolinea_id');
    }
}
