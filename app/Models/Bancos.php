<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
class Bancos extends Model
{
    use HasFactory, EncryptationId;

    protected $table = "maest_bancos";  
   
    protected $appends =['crypt_id'];
    public $timestamps = false;
}
