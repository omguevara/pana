<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class FacturasDetalle extends Model
{
    use HasFactory, EncryptationId;
    
    protected $appends = ['crypt_id'];
    protected $table = "proc_facturas_detalle";
    protected $hidden = [
        'id'
    ];
   public $timestamps = false;
    
    
}
