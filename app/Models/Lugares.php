<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Lugares extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_lugares";
    public $timestamps = false;
    protected $appends = ['crypt_id', 'puestos'];

    public function getPuestosAttribute() {
        return $this->getPuestos;
    }

    public function getPuestos() {
        return $this->hasMany(Puestos::class, 'lugar_id')->orderBy("maest_puestos.id");
    }

}
