<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Aeropuertos extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_aeropuertos";
    protected $appends = ['crypt_id', 'categoria', 'full_nombre', 'Ubicaciones', 'feriados', 'estado',
        'check_info', 'check_info_bomberos', 'check_info_seguridad', 'check_info_operaciones', 'check_info_certificacion'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getUbicacionesAttribute() {
        return $this->getUbicaciones;
    }

    public function getUbicaciones() {
        return $this->hasMany(Ubicaciones::class, 'aeropuerto_id')->orderBy("maest_ubicaciones.id");
    }

    public function getFeriadosAttribute() {
        return $this->getFeriados;
    }

    public function getFeriados() {
        return $this->hasMany(FeriadosAeropuertos::class, 'aeropuerto_id');
    }

    public function getFullNombreAttribute() {
        if ($this->pais_id != 1) {
            return Upper($this->codigo_oaci . ' / ' . $this->nombre . ' (' . $this->getPais->name_country . ')');
        }
        return Upper($this->codigo_oaci . ' / ' . $this->nombre);
    }

    public function getCategoria() {
        return $this->belongsTo(Categorias::class, 'categoria_id');
    }

    public function getCategoriaAttribute() {
        return $this->getCategoria;
    }
    
    public function getPais() {
        return $this->belongsTo(Paises::class, 'pais_id');
    }

    public function getPaisAttribute() {
        return $this->getPais;
    }
    
    public function getEstado() {
        return $this->belongsTo(Estados::class, 'estado_id');
    }

    public function getEstadoAttribute() {
        return $this->getEstado;
    }
    
    public function getTurno($hora = null) {
        //  dd($hora);
        if ($hora === null) {
            return null;
        } else {

            $hora_ocaso = str_replace(":", "", $this->hora_ocaso);
            $hora_amanecer = str_replace(":", "", $this->hora_amanecer);
            return (($hora >= ((int) ($hora_ocaso)) || $hora <= ((int) ($hora_amanecer))) ? 2 : 1);
        }
    }

    public function getCheckInfoAttribute() {
        $infoB = InfoBomberos::where("aeropuerto_id", $this->id)->first();
        $infoS = InfoSeguridad::where("aeropuerto_id", $this->id)->first();
        $infoO = InfoOperaciones::where("aeropuerto_id", $this->id)->first();
        $infoC = InfoCertificaciones::where("aeropuerto_id", $this->id)->first();

        $cant = 0;
        
        if ($infoB == null) {
            $infoB = false;
        } else {
            $cant++;
            $infoB = $infoB->new_info;
            
}
        
        if ($infoS == null) {
            $infoS = false;
        } else {
            $cant++;
            $infoS = $infoS->new_info;
        }
        if ($infoO == null) {
            $infoO = false;
        } else {
            $cant++;
            $infoO = $infoO->new_info;
        }
        if ($infoC == null) {
            $infoC = false;
        } else {
            $cant++;
            $infoC = $infoC->new_info;
        }

        $new_info = ($infoB == true || $infoS == true || $infoO == true || $infoC == true );
        

        return ["new_info" => $new_info, "cant" => $cant];
    }

    public function getCheckInfoBomberosAttribute() {
        $infoB = InfoBomberos::where("aeropuerto_id", $this->id)->first();
        if ($infoB == null) {
            return false;
        } else {
            return $infoB->new_info;
        }
    }

    public function getCheckInfoSeguridadAttribute() {
        $infoS = InfoSeguridad::where("aeropuerto_id", $this->id)->first();

        if ($infoS == null) {
            return false;
        } else {
            return $infoS->new_info;
        }
    }

    public function getCheckInfoOperacionesAttribute() {
        $infoO = InfoOperaciones::where("aeropuerto_id", $this->id)->first();
        if ($infoO == null) {
            return false;
        } else {
            return $infoO->new_info;
        }
    }

    public function getCheckInfoCertificacionAttribute() {
        $infoC = InfoCertificaciones::where("aeropuerto_id", $this->id)->first();
        if ($infoC == null) {
            return false;
        } else {
            return $infoC->new_info;
        }
    }

}
