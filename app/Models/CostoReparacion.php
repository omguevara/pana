<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;


class CostoReparacion extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = 'proc_costos_reparacion';
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    protected $hidden = [
        'id'
    ];

    public function inventario_equipo(){
        return $this->belongsTo(InventarioEquipo::class, 'inventario_id');
    }
}