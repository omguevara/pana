<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Membresia extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_membresias";
    //protected $fillable = [];
    protected $appends = ['crypt_id', 'fecha_desde2', 'fecha_hasta2', 'tipo', 'exonerar2'];
    protected $tipo_membresia = ["1" => "Oficial", "2" => "Convenio", "3" => "Privada"];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getFechaDesde2Attribute() {
        $f = explode("-", $this->fecha_desde);
        return $f[2] . '/' . $f[1] . '/' . $f[0];
    }

    public function getTipoAttribute() {
        $resp = "";
        if ($this->tipo_membresia_id != null) {
            $resp = $this->tipo_membresia[$this->tipo_membresia_id];
        }
        return $resp;
    }

    public function getExonerar2Attribute() {
        $resp = "";
        if ($this->exonerar != null) {
            $exonera = explode("|", $this->exonerar);
            foreach($exonera as $value){
                $resp .= $value==1 ? 'Tasa ':'Dosa ';
            }
        }
        return $resp;
    }

    public function getFechaHasta2Attribute() {
        $f = explode("-", $this->fecha_hasta);
        return $f[2] . '/' . $f[1] . '/' . $f[0];
    }

    public function aeronaves(){
        return $this->belongsTo(Aeronaves::class, 'aeronave_id');
    }

}
