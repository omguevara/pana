<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Facturas extends Model {

    use HasFactory,
        EncryptationId;

    protected $appends = ['crypt_id', 'cliente', 'tipo_pago'];
    protected $tipos_pagos = [1 => "Contado", 2 => "Crédito"];
    protected $table = "proc_facturas";
    public $timestamps = false;
    protected $hidden = [
        'password', 'id'
    ];

    public function getCliente1Attribute() {
        return $this->belongsTo(Clientes::class, 'cliente_id');
    }

    public function getClienteAttribute() {
        return $this->getCliente1Attribute;
    }

    public function getTipoPagoAttribute() {
        return $this->tipos_pagos[$this->tipo_pago_id];
    }

    public function getSolicitudAttribute() {
        return $this->getSolicitud;
    }

    public function getDetalleVuelo() {
        //dd($this->tipo_id);
        /*if ($this->tipo_id == 1) { //TASA
            $Data = VuelosGenerales::where('factura_tasa', $this->id)->first();
        } else {
            if ($this->tipo_id == 2) { //DOSA
                $Data = VuelosGenerales::where('factura_dosa', $this->id)->first();
            } else {
                if ($this->tipo_id == 3) { //MIXTA
                    $Data = VuelosGenerales::where('factura_dosa', $this->id)->first();
                } else {
                    dd("0");
                }
            }
        }*/
        $Data = Vuelos::where('factura_id', $this->id)->get();
        return $Data;
    }

    public function getDetalle() {
        return $this->hasMany(FacturasDetalle::class, 'factura_id');
    }
    
   
    
    public function getPagos() {
        return $this->hasMany(FacturasPagos::class, 'factura_id');
    }
    
    public function getTotal() {
        $sum = 0;
        foreach($this->getDetalle as $value){
            $sum += $value->precio*(1+($value->iva/100));
        }
        return $sum;
    }
    
    
    
   
           


           

    
    
    

}
