<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class InventarioEquipo extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = 'maest_inventario_equipos';
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    protected $hidden = [
        'id'
    ];

    public function concesiones(){
        return $this->belongsTo(ConcesionEquipo::class, 'concesion_equipo_id');
    }
    public function costo_reparacion(){
        return $this->hasOne(CostoReparacion::class, 'inventario_id');
    }
}
