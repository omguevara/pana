<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Profile extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "seg_profiles";
    protected $fillable = ['code', 'name_profile', 'description', 'status'];
    public $timestamps = false;
    protected $appends = ['crypt_id'];
    
    protected $hidden = [
        'id'
    ];

    public function getProcesses() {
        return $this->belongsToMany(Process::class, 'seg_profile_processes')
                        ->withPivot('process_id', 'profile_id', 'actions');
    }
    
    

    public function getOptions() {
        return $this->belongsToMany(Process::class, 'seg_profile_processes', 'profile_id', 'process_id')->withPivot("actions");
    }

    public function getMenu() {
        $Result = [];
        $Menu = Menu::orderBy('order')->get()->toArray();
        foreach ($Menu as $key => $value) {
            foreach ($value['get_process'] as $key2 => $value2) {
                if (in_array($this->id, $value2['profile_array'])) {

                    $actions = ProfileProcess::where('process_id', $value2)->where('profile_id', $this->id)->first()->actions;
                    
                    $actions = trim($actions) == "" ? [] : explode("|", $actions);
                    
                    $Result[$value['id']][$value2['id']] = $actions;
                    
                }
            }

        }
        return $Result;
    }

}
