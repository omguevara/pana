<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Problemas extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_problemas";
    protected $appends = ['crypt_id', 'aeropuerto', 'usuario', 'estatus'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    protected $Estado = ["1"=>"Registrado", "2"=>"En Evaluación", "3"=>"Resuelto"];
    
    public function getEstatusAttribute() {
        return $this->Estado[$this->estado_id];
    }

    
    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }

    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }
    
    public function getUsuarioAttribute() {
        return $this->getUsuario;
    }

    public function getUsuario() {
    return $this->belongsTo(User::class, 'user_id');
    }

}
