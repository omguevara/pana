<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class RegistroOnline extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_registro_online";
    protected $appends = ['crypt_id', 'piloto', 'aeronave', 'origen', 'destino'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    
    public function getPilotoAttribute() {
        return $this->getPiloto;
    }

    public function getPiloto() {

        return $this->belongsTo(Pilotos::class, 'piloto_id');
    }
    
    public function getAeronaveAttribute() {
        return $this->getAeronave;
    }

    public function getAeronave() {

        return $this->belongsTo(Aeronaves::class, 'aeronave_id');
    }
    
    
    
    
    
    
    public function getOrigenAttribute() {
        return $this->getOrigen;
    }

    public function getOrigen() {

        return $this->belongsTo(Aeropuertos::class, 'origen_id');
    }
    
     public function getDestinoAttribute() {
        return $this->getDestino;
    }

    public function getDestino() {

        return $this->belongsTo(Aeropuertos::class, 'destino_id');
    }
    
    
}
