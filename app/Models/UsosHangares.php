<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;

class UsosHangares extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "maest_usos_hangares";
    protected $appends = ['crypt_id'];
    protected $hidden = [
        'id'
    ];
    public $timestamps = false;
}
