<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class VuelosGenerales extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_vuelos_generales";
    protected $appends = ['crypt_id', 'usuario', 'aeropuerto', 'aeronave', 'piloto_llegada', 'tipo_llegada', 'procedencia2', 'piloto_salida',
        'tipo_salida', 'destino2', 'esta_pagado_tasa',
        'matricula', 'modelo', 'piloto', 'procedencia', 'destino', 'opciones', 'puesto', 'ubicacion', 'plan_vuelo', 'tortuga', 'factura_tortuga', 'hours',
        'cobrado', 'detalle', 'total'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    protected $TIPO_LLEGADA_ID = ["1" => "NAC", "2" => "INT"];
    protected $TIPO_SALIDA_ID = ["1" => "NAC", "2" => "INT"];

    public function getTortugaAttribute() {
        return $this->getTortuga;
    }

    public function getHoursAttribute() {
        return $this->hora_llegada . '/' . $this->hora_salida;
    }

    public function getTotalAttribute() {
        $sum = 0;
        foreach ($this->getDetalle as $value) {
            $sum += round($value->precio * (1 + ($value->iva_aplicado / 100)), 2);
        }
        return $sum;
    }

    public function getDetalleAttribute() {
        return $this->getDetalle;
    }

    public function getDetalle() {
        return $this->hasMany(VuelosGeneralesDetalle::class, 'vuelo_id');
    }

    public function getCobradoAttribute() {
        return ($this->exento_tasa == true ? '' : 'TASA') . ' ' . ($this->exento_dosa == true ? '' : 'DOSA');
    }

    public function getTortuga() {
        return $this->hasOne(VuelosTortuga::class, 'vuelo_general_id');
    }

    public function getEstaPagadoTasaAttribute() {
        if ($this->factura_tasa != null) {
            $Factura = Facturas::find($this->factura_tasa);
            return $Factura->getPagos()->where("monto", ">", 0)->count() > 0;
        } else {
            return false;
        }
    }

    public function getFacturaTortuga() {
        return $this->getFacturaTortugaAttribute;
    }

    public function getFacturaTortugaAttribute() {
        if ($this->aeropuerto_id == 15) { // SI ES LA TORTUGA
            $Factura = Facturas::find($this->factura_tasa);
            return $Factura;
        } else {
            return false;
        }
    }

    public function getPlanVueloAttribute() {

        switch ($this->plan_vuelo_id) {
            case 1:
                $nombre = 'DOBLE TOQUE';
                break;
            case 2:
                $nombre = 'ESTADÍA';
                break;
            default :
                $nombre = '';
        }
        return $nombre;
    }

    public function getUbicacionAttribute() {
        if ($this->puesto_id != null) {
            $Puesto = Puestos::find($this->puesto_id);
            $Lugar = Lugares::find($Puesto->lugar_id);
            $Ubicacion = Ubicaciones::find($Lugar->ubicacion_id);

            $resp['puesto_id'] = $Puesto->crypt_id;
            $resp['puesto_nombre'] = $Puesto->nombre;
            $resp['lugar_id'] = $Lugar->crypt_id;
            $resp['lugar_nombre'] = $Lugar->nombre;
            $resp['ubicacion_id'] = $Ubicacion->crypt_id;
            $resp['ubicacion_nombre'] = $Ubicacion->nombre;
        } else {
            $resp['puesto_id'] = "";
            $resp['puesto_nombre'] = "";
            $resp['lugar_id'] = "";
            $resp['lugar_nombre'] = "";
            $resp['ubicacion_id'] = "";
            $resp['ubicacion_nombre'] = "";
        }
        return $resp;
    }

    public function getPuestoAttribute() {
        if ($this->puesto_id != null) {
            $Puesto = Puestos::find($this->puesto_id);

            return $Puesto->nombre;
        } else {
            return "";
        }
    }

    public function getOpcionesAttribute() {
        return "";
    }

    public function getMatriculaAttribute() {
        return $this->getAeronave->matricula;
    }

    public function getModeloAttribute() {
        return $this->getAeronave->nombre;
    }

    public function getPilotoAttribute() {
        if ($this->getPilotoLlegada != null) {
            return $this->getPilotoLlegada->full_name;
        }
        return "";
    }

    public function getProcedenciaAttribute() {
        if ($this->getProcedencia2 != null) {
            return $this->getProcedencia2->codigo_oaci;
        }
        return "";
    }

    public function getDestinoAttribute() {
        if ($this->getDestino2 == null) {
            return "";
        } else {
            return $this->getDestino2->codigo_oaci;
        }
    }

    public function getNumDosaAttribute() {
        return "XXXX";
    }

    //matricula: "YV2514",
    //modelo: "piloTO",
    //piloto: "0.0039",
    //hora_llegada: "15:25",
    //tipo_llegada: "NAC",
    //procedencia: "CCS",
    //pax_desembarcados: "10",
    //hora_salida: "23:00",
    //tipo_salida: "INT",
    //destino: "SVCS",
    //pax_embarcados: "10",
    // num_dosa: "XXXX",
    //opciones:






    public function getDataDhx() {
        return $this->toArray();
    }

    public function getTipoLlegadaAttribute() {
        if ($this->tipo_llegada_id != null) {
            return $this->TIPO_LLEGADA_ID[$this->tipo_llegada_id];
        }
        return "";
    }

    public function getTipoSalidaAttribute() {
        $TipoSalida = "";
        if ($this->tipo_salida_id != null) {
            $TipoSalida = $this->TIPO_LLEGADA_ID[$this->tipo_salida_id];
        }
        return $TipoSalida;
    }

    /*     * ************************************************ */

    public function getUsuarioAttribute() {
        return $this->getUsuario;
    }

    public function getUsuario() {

        return $this->belongsTo(User::class, 'user_id');
    }

    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }

    public function getAeropuerto() {
        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }

    public function getAeronaveAttribute() {
        return $this->getAeronave;
    }

    public function getAeronave() {
        return $this->belongsTo(Aeronaves::class, 'aeronave_id');
    }

    public function getPilotoLlegadaAttribute() {
        return $this->getPilotoLlegada;
    }

    public function getPilotoLlegada() {

        return $this->belongsTo(Pilotos::class, 'piloto_llegada_id');
    }

    public function getProcedencia2Attribute() {
        return $this->getProcedencia2;
    }

    public function getProcedencia2() {
        return $this->belongsTo(Aeropuertos::class, 'procedencia_id');
    }

    public function getPilotoSalidaAttribute() {
        return $this->getPilotoSalida;
    }

    public function getPilotoSalida() {

        return $this->belongsTo(Pilotos::class, 'piloto_salida_id');
    }

    public function getDestino2Attribute() {
        return $this->getDestino2;
    }

    public function getDestino2() {
        return $this->belongsTo(Aeropuertos::class, 'destino_id');
    }

    /*
      public function getTipoRegistroAttribute() {
      return $this->TIPO_REGISTRO[$this->tipo_registro_id];
      }












      public function getDestinoAttribute() {
      return $this->getDestino;
      }
      public function getDestino() {
      return $this->belongsTo(Aeropuertos::class, 'destino_id');
      }



     */
}
