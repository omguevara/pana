<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoCertificaciones extends Model
{
    use HasFactory;
    protected $table ='proc_info_cerfiticaciones';
    public $timestamps = false;
}
