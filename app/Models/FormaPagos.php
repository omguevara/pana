<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class FormaPagos extends Model
{
    use HasFactory,
        EncryptationId;
    
    
    protected $table = "conf_forma_pagos";
    protected $fillable = [ 'nombre', 'activo'];
    protected $appends = ['crypt_id'];
    public $timestamps = false;

    
}
