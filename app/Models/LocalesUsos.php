<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalesUsos extends Model
{
    use HasFactory;
    protected $table = 'maest_locales_usos';
    public $timestamps = false;

    public function locales()
    {
        return $this->hasMany(Locales::class, 'uso_id');
    }
}
