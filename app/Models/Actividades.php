<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Actividades extends Model
{
    use HasFactory;
    protected $table = 'maest_actividades';
    public $timestamps = false;
    
    /* public function historialConcesiones()
    {
        return $this->hasMany(Concesiones::class, 'actividad_id');
    } */
    public function concesion()
    {
        $today = date('Y-m-d');
        // dd($today);
        return $this->hasOne(Concesiones::class, 'actividad_id')->whereBetween(DB::raw("'$today'"), [DB::raw('fecha_inicio'), DB::raw('fecha_fin')]);
    }
}
