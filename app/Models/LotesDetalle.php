<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class LotesDetalle extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_lotes_detalle";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    public function lote() {
        return $this->belongsTo(LotesTickets::class, 'lote_id');
    }

  

}
