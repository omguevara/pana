<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\EncryptationId;
class ProformaPago extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_proforma_pagos";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];


    public function getProforma() {
        return $this->hasMany(Proforma::class, 'proforma_id');
    }
}
