<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    use HasFactory;

    protected $table = "seg_menus";
    protected $fillable = [ 'name_menu', 'description', 'icon', 'order'];
    protected $appends = ['process'];
    public $timestamps = false;

    public function getProcessAttribute() {
        return $this->getProcess;
    }

    public function getProcess() {
        return $this->hasMany(Process::class, 'menu_id')->orderBy('order');
    }

}
