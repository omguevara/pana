<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Taquillas extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "conf_taquillas";
    protected $appends = ['crypt_id', 'aeropuerto', 'puntos'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getAeropuertoAttribute() {
        return $this->getAeropuerto;
    }
    
    
    public function getAeropuerto() {

        return $this->belongsTo(Aeropuertos::class, 'aeropuerto_id');
    }
    
    public function getPuntosAttribute(){
        return $this->getPuntos;
    }
    public function getPuntos(){
        return $this->hasMany(TaquillasPuntos::class, 'taquilla_id', 'id');
    }

}
