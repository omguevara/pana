<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use DB;


class AeronavesPuestos extends Model
{
    use HasFactory,
        EncryptationId;
    
     protected $table = "proc_aeronaves_puestos";
    protected $appends = ['crypt_id', 'fecha_entrada2', 'fecha_salida2'];
    protected $hidden = [
        'id'
    ];
    public $timestamps = false;
    
    
    
    public function getFechaSalida2Attribute() {
        return showDate(  ($this->fecha_salida == null ? date('Y-m-d H:i'):$this->fecha_salida )  );
    }
    
    public function getFechaEntrada2Attribute() {
        return showDate( $this->fecha_entrada );
    }
}
