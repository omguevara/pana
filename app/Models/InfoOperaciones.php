<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoOperaciones extends Model
{
    use HasFactory;
    protected $table ='proc_info_operaciones';
    public $timestamps = false;
}
