<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class VuelosPasajeros extends Model
{
    use HasFactory,
        EncryptationId;
    
    protected $table = "proc_vuelos_pasajeros";
    protected $appends = ['crypt_id', 'pasajero'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    
    public function getPasajeroAttribute(){
        return Upper($this->documento.' '.$this->nombres.' '.$this->apellidos. ' ('.($this->genero=="M" ? 'MASCULINO':'FEMENINO').')');
    }
}
