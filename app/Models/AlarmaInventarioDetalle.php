<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\EncryptationId;

class AlarmaInventarioDetalle extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_alarmas_inventario_detalle";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function equipos(){
        return $this->belongsTo(InventarioEquipo::class, 'inventario_equipo_id');
    }
}
