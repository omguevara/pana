<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;
use App\Helpers\Encryptor;
use DB;

class Aeronaves extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_aeronaves";
    protected $appends = ['crypt_id', 'estacion_crypt_id', 'full_nombre', 'full_nombre_tn', 'membresia', 'last_arrival'];
    
    protected $hidden = [
        'id'
    ];
    public $timestamps = false;
    
    public function getFullNombreAttribute() {
        return Upper($this->matricula.' / '.$this->nombre);
    }
    
    public function getFullNombreTnAttribute() {
        return Upper($this->matricula.' / '.$this->nombre.' TON:'.muestraFloat($this->peso_maximo/1000));
    }

    public function getEstacionCryptIdAttribute() {
        return Encryptor::encrypt($this->estacion_id);
    }
    
    public function getMembresia() {
       return $this->hasMany(Membresia::class, 'aeronave_id', 'id')
               ->whereBetween(DB::raw("'".date('Y-m-d')."'"), [DB::raw('"maest_membresias"."fecha_desde"'), DB::raw('"maest_membresias"."fecha_hasta"')]);
        
    }
    
    
    public function getMembresiaAttribute() {
       return $this->getMembresia;
        
    }
    
    
    public function getLastArrivalAttribute() {
        //User::$withoutAppends = true;
       return  Vuelos2::where("aeronave_id", $this->id)
              ->where('tipo_operacion_id', 1)
              ->where('activo', true)
              ->orderBy("id", "desc")
             
              ->first();
              
        
    }
    

}
