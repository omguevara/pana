<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoSeguridadLog extends Model
{
    use HasFactory;
    protected $table ='proc_info_seguridad_log';
    public $timestamps = false;
}
