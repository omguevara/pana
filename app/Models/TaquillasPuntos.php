<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class TaquillasPuntos extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "conf_taquillas_puntos";
    protected $appends = ['crypt_id', 'banco'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
    
    public function getBancoAttribute() {
        return $this->getBanco;
    }
    
    
    public function getBanco() {

        return $this->belongsTo(Bancos::class, 'banco_id');
    }

   

}
