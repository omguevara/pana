<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class VuelosDetalle extends Model
{
    use HasFactory,
        EncryptationId;

    protected $table = "proc_vuelos_detalle";
    protected $appends = ['crypt_id'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];
}
