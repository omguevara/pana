<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Registros extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "registros";
    protected $appends = ['crypt_id', 'origen', 'destino', 'fecha2'];
    public $timestamps = false;
    
    
    
    
    
    public function getFecha2Attribute() {
        return showDate($this->fecha);
    }
    
    public function getOrigen() {
        return $this->belongsTo(Aeropuertos::class, 'origen_id');
    }
    
    public function getOrigenAttribute() {
        return $this->getOrigen;
    }
    
    
    public function getDestino() {
        return $this->belongsTo(Aeropuertos::class, 'destino_id');
    }
    
    public function getDestinoAttribute() {
        return $this->getDestino;
    }

}
