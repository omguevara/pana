<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VuelosGeneralesEstadisticas extends Model
{
    use HasFactory;
}
