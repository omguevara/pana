<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class Puestos extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "maest_puestos";
    public $timestamps = false;
    protected $appends = ['crypt_id', 'aeronave'];

    public function getAeronave() {
        if ($this->aeronave_id!=null){
            return Aeronaves::find($this->aeronave_id); //$this->belongsTo(Aeronaves::class, 'aeronave_id');
        }else{
            return null;
        }
        
    }

    public function getAeronaveAttribute() {
        return $this->getAeronave();
    }

}
