<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Traits\EncryptationId;



class User extends Authenticatable {

    use HasApiTokens,
        HasFactory,
        Notifiable,
        EncryptationId;

    protected $table = "seg_users";
    protected $appends = ['crypt_id', 'type_document', 'number_document', 'full_nombre', 'aeropuerto', 'profile', 'lista_aeropuertos'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password', 'id'
    ];
    public $timestamps = false;
    
    public function getListaAeropuertosAttribute(){
        
        if ($this->aeropuerto_id != null){
            
            $extras = UsuariosAeropuertos::where("usuario_id", $this->id)->pluck("aeropuerto_id")->toArray();
            //dd($extras);
            $extras[] = $this->aeropuerto_id;
            
            return Aeropuertos::whereIn("id", $extras)->pluck("id")->toArray();
            
        }else{
            return Aeropuertos::pluck("id")->toArray();
        }
        
    }
    
    public function getAeropuertoAttribute(){
        if ($this->aeropuerto_id != null){
            
            return Aeropuertos::find($this->aeropuerto_id);
        }else{
            return null;
        }
    }
    
    
    
    
    public function getProfile() {
        return $this->belongsTo(Profile::class, 'profile_id');
    }
    
    public function getTypeDocumentAttribute(){
        return substr($this->document, 0, 1);
    }
    
    public function getFullNombreAttribute(){
        return Upper($this->name_user.' '.$this->surname_user) ;
    }
    
    public function getNumberDocumentAttribute(){
        return substr($this->document, 1, strlen($this->document) );
    }
    
    
    public function getActions() {
        $actions_profile = ProfileProcess::where("profile_id", $this->profile_id)->get();
        $actions = [];
        foreach ($actions_profile as $value) {

            $actions[$value->process_id] = explode("|", $value->actions);
        }
        return $actions;
    }

    public function getProfileAttribute() {
        return $this->getProfile;
    }

    public function getMenu() {
        $Menu = Menu::orderBy('order')->get()->toArray();
        foreach ($Menu as $key => $value) {
            foreach ($value['get_process'] as $key2 => $value2) {
                if (!in_array($this->profile_id, $value2['profile_array'])) {
                    unset($Menu[$key]['get_process'][$key2]);
                    unset($Menu[$key]['process'][$key2]);
                }
            }
            if (count($Menu[$key]['get_process']) == 0) {
                unset($Menu[$key]);
            }
        }
        return $Menu;
    }

}
