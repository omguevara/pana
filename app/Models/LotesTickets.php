<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\EncryptationId;

class LotesTickets extends Model {

    use HasFactory,
        EncryptationId;

    protected $table = "proc_lotes_tickets";
    protected $appends = ['crypt_id', 'fecha_registro2'];
    public $timestamps = false;
    protected $hidden = [
        'id'
    ];

    public function getFechaRegistro2Attribute() {
        return showDate($this->fecha_registro, "full");
    }
    
    public function aeropuerto() {
        return $this->belongsTo(Aeropuertos2::class, 'aeropuerto_id');
    }
    
    public function tiempo() {
        return $this->belongsTo(TiempoTickets::class, 'tiempo_id');
    }
    
    public function detalle() {
        return $this->hasMany(LotesDetalle::class, 'lote_id');
    }

    public function usuario()

    {
        return $this->belongsTo(User::class, 'user_id');

    }
    
}
