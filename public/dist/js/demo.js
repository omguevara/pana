/**
 * AdminLTE Demo Menu
 * ------------------
 * You should not use this file in production.
 * This file is for demo purposes only.
 */

/* eslint-disable camelcase */


var AJAX_ACTIVE = false;

$(function () {

    $(".princ-menu").on("click", function (event) {
        $(".princ-menu").removeClass("active");
        $(this).addClass("active");
        setAjax(this, event);
    });
});
function setAjax(obj, event) {
    event.preventDefault();
    $(window).off('keydown');

    sendAjax(obj, event);



    return false;

}

function sendAjax(obj, event) {
    if (SET_INTERVAL != null) {
        clearInterval(SET_INTERVAL);
    }
    if (AJAX_ACTIVE == false) {
        $("#main-content").html(LOADING);
        AJAX_ACTIVE = true;
        $.get(obj.href, function (response) {
            AJAX_ACTIVE = false;
            $("#main-content").html(response);

        }, 'html').fail(function (xhr) {
            $("#main-content").html("<pre>" + xhr.responseText + "</pre>");
            AJAX_ACTIVE = false;
        });
        //alert(this.href);
    } else {
        Toast.fire({
            icon: 'warning',
            title: 'Please Wait.'
        });
    }
}




function modeTheme(obj, op) {

    t1 = [];
    t1[2] = '<i class="fas fa-sun"></i>';
    t1[1] = '<i class="fas fa-moon"></i>';

    t2 = [];
    t2[1] = ['dark-mode', 'light-mode'];
    t2[2] = ['light-mode', 'dark-mode'];

    $("body").removeClass(t2[op][0]).addClass(t2[op][1]);


    //console.log(t[op]);
    $("#chgTheme").html(t1[op]);
    //dark-mode
    $(".selectTheme").removeClass("active");
    obj.classList.add("active");

    $.get(ROOT + "/set-theme/" + op);

}

// convierte una cifra de formato USA a formato VEN
// similar a la funcion php muestrafloat en $appRoot/lib/functiones.php
function muestraFloat(nStr, dec) {
    var ceros = "000000000000";
    nStr += '';
    dec = parseInt((arguments.length > 1) ? dec : 2);
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1].substr(0, dec) : ',00';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }

    return x1 + x2 + ceros.substr(0, (dec - x2.length + 1));
}


// recibe un numero con formato VEN, y la convierte en un formato 
// utilizable para ejecutar operaciones matematicas
function usaFloat(nStr) {
    nStr = nStr == '' ? '0' : nStr;
    return parseFloat(replace_caracter2(replace_caracter2(nStr, '.', ''), ',', '.'));


}

function replace_caracter2(inputString, fromString, toString) {
    return inputString.split(fromString).join(toString);
}


function showTime(hour, minute) {
    hour = parseInt(hour, 10);
    minute = parseInt(minute, 10);
    H = hour < 10 ? "0" + hour : (hour > 12 ? "0" + (hour - 12) : hour);
    M = minute < 10 ? "0" + minute : minute;
    Z = parseInt(hour, 10) < 12 ? "AM" : "PM";
    return H + ":" + M + " " + Z;


}

function dateShowTime(fecha) {
    f1 = fecha.split(" ");
    f2 = f1[1];
    f3 = f2.split(":");
    return showTime(f3[0], f3[1]);

}

function getValor(formula, valor) {

    if (formula != "" && formula != null) {

        formula = formula.replaceAll("[VALOR]", "parseFloat(" + valor + ", 10)");
        formula = formula.replaceAll("[", "");
        formula = formula.replaceAll("]", "");


        return formula;
    } else {
        return "parseFloat(" + valor + ")";

    }


}

function saveDate(f) {
    f1 = f.split("/");
    return f1[2] + '-' + f1[1] + '-' + f1[0];
}

function showDate(f1) {
    f1 = f1.split(" ");
    f1 = f1[0].split("-");
    return f1[2] + '/' + f1[1] + '/' + f1[0];

}


function closeLoading() {
    $(".overlay-wrapper").remove();
}


class StatePath {

    constructor(element) {
        this.element = element
    }

    fill(color) {
        this.element.setAttribute("fill", color)
    }

    static findByName(name) {
        return new StatePath(document.getElementById(name))
    }
}