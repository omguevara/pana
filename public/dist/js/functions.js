$(document).ready(function () {
  var map = L.map('map').setView([8.0018709, -66.1109318], 6);

  // L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', { maxZoom: 18 }).addTo(map); 
  let principal = L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', { maxZoom: 20 }).addTo(map);
  let secondary = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');
  // L.tileLayer('https://tileserver.memomaps.de/tilegen/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'Map <a href="https://memomaps.de/">memomaps.de</a> <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' }).addTo(map); 
  // L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}', { attribution: 'Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC', maxZoom: 16 }).addTo(map);
  // L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {	maxZoom: 17,	attribution: 'Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'}).addTo(map);
  // L.tileLayer('https://basemap.nationalmap.gov/arcgis/rest/services/USGSTopo/MapServer/tile/{z}/{y}/{x}', {	maxZoom: 20,	attribution: 'Tiles courtesy of the <a href="https://usgs.gov/">U.S. Geological Survey</a>'}).addTo(map);
  // L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}', {      attribution: 'Tiles &copy; Esri &mdash; Source: Esri, DeLorme, NAVTEQ, USGS, Intermap, iPC, NRCAN, Esri Japan, METI, Esri China (Hong Kong), Esri (Thailand), TomTom, 2012'    }).addTo(map);
  let airplaneIcon = L.icon({
    iconUrl: 'img/icons/marker.png',
    iconSize: [30, 30],
    iconAnchor: [19, 19],
    popupAnchor: [0, -10]
  });
  let capas = {
    'Mapa estándar' : principal,
    'Vista satélite' : secondary
  }
  // secondary.setZIndex(0);
  // Remove original control
  map.zoomControl.remove();
  L.control.layers(capas).addTo(map);
  // Add custom control
  L.Control.zoomHome = L.Control.extend({
    options: {
      position: 'topleft',
      zoomInText: '<i class="fa fa-plus" style="line-height:1.65;"></i>',
      zoomInTitle: 'Zoom in',
      zoomOutText: '<i class="fa fa-minus" style="line-height:1.65;"></i>',
      zoomOutTitle: 'Zoom out',
      zoomHomeText: '<i class="fa fa-home" style="line-height:1.65;"></i>',
      zoomHomeTitle: 'Zoom home'
    },

    onAdd: function (map) {
      var controlName = 'gin-control-zoom',
        container = L.DomUtil.create('div', controlName + ' leaflet-bar'),
        options = this.options;

      this._zoomInButton = this._createButton(options.zoomInText, options.zoomInTitle,
      controlName + '-in', container, this._zoomIn);
      this._zoomHomeButton = this._createButton(options.zoomHomeText, options.zoomHomeTitle,
      controlName + '-home', container, this._zoomHome);
      this._zoomOutButton = this._createButton(options.zoomOutText, options.zoomOutTitle,
      controlName + '-out', container, this._zoomOut);

      this._updateDisabled();
      map.on('zoomend zoomlevelschange', this._updateDisabled, this);

      return container;
    },

    onRemove: function (map) {
      map.off('zoomend zoomlevelschange', this._updateDisabled, this);
    },

    _zoomIn: function (e) {
      this._map.zoomIn(e.shiftKey ? 3 : 1);
    },

    _zoomOut: function (e) {
      this._map.zoomOut(e.shiftKey ? 3 : 1);
    },

    _zoomHome: function (e) {
      map.setView([8.0018709, -66.1109318], 6);
    },

    _createButton: function (html, title, className, container, fn) {
      var link = L.DomUtil.create('a', className, container);
      link.innerHTML = html;
      link.href = '#';
      link.title = title;

      L.DomEvent.on(link, 'mousedown dblclick', L.DomEvent.stopPropagation)
          .on(link, 'click', L.DomEvent.stop)
          .on(link, 'click', fn, this)
          .on(link, 'click', this._refocusOnMap, this);

      return link;
    },

    _updateDisabled: function () {
      var map = this._map,
        className = 'leaflet-disabled';

      L.DomUtil.removeClass(this._zoomInButton, className);
      L.DomUtil.removeClass(this._zoomOutButton, className);

      if (map._zoom === map.getMinZoom()) {
        L.DomUtil.addClass(this._zoomOutButton, className);
      }
      if (map._zoom === map.getMaxZoom()) {
        L.DomUtil.addClass(this._zoomInButton, className);
      }
    }
  });
  var zoomHome = new L.Control.zoomHome();
  // add the new control to the map
  zoomHome.addTo(map);

  // L.control.layers(capas, null, {collapsed: true}).addTo(map);

  principal.on('click', function (e) {
    principal.addTo(map);
    secondary.removeFrom(map);
  });
  secondary.on('click', function (e) {
    secondary.addTo(map);
    principal.removeFrom(map);
  });
  
  // get map locations
  $.get({
    url: "getLocations",
    dataType: "json",
    success: function(response) {
      for (let i = 0; i < response.data.length; i++) {
        // Agregar los marcadores al mapa
        let marker = L.marker([response.data[i].latitude, response.data[i].longitude], {icon: airplaneIcon}).addTo(map);
        // Agregar el contenido del botón "Ver más" a cada objeto de marcador
        marker.bindPopup(createPopupContent(response.data[i].oaci, response.data[i].descripcion));
        marker.on('click', function (e) {
          this.openPopup();
          map.flyTo(e.latlng, 15, {
            animate: true,
            duration: 1.5
          });
          // map.setView(e.latlng, 15);
        });
      }
    }
  });

  // Evento para mostrar el contenido completo al hacer clic en el botón "Ver más"
  $('#map').on('click', '#btn-ver-mas', function (event) {
    let popupContent = $(this).parent();
    popupContent.find('p').show();
    $('#more-info').removeClass('d-none');
    $('#more-info').fadeIn(1000);
    // $('#map').css('width', '60%');
    // Cambiar el ancho del mapa al hacer clic en el botón
  });
    
  // Evento para cambiar el ancho del mapa de vuelta a su valor original al cerrar la descripción del marcador
  map.on('popupclose', function (e) {
    $('#close-info').click(function() {
      $('#more-info').addClass('d-none');
      $('#more-info').fadeOut(1000);
    });
  });
  
  $('#close-info').on('click', function(){
    $('#more-info').addClass('d-none');
    $('#more-info').fadeOut(1000);
  });

  new Glide('.glide', {
    type: 'carousel',
    perView: 1,
    autoplay: 3000 // Cambiar de slide cada 3 segundos
  }).mount();
});
// Función para crear el contenido del botón "Ver más"
function createPopupContent(title, content) {
  return '<div class="popup-content">' +
           '<h3>' + title + '</h3>' +
           '<p>' + content + '</p>' +
           '<button id="btn-ver-mas">Ver más</button>' +
         '</div>';
}