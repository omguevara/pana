function verificar(banco,oficina, digitos,cuenta){
	alert(verificarCuenta(banco,oficina, digitos,cuenta));
	return false;
}

function verificarCuenta(banco,oficina, digitos,num_cuenta) {
	var pesos1 = new Array(3, 2, 7, 6, 5, 4, 3, 2);
	var pesos2 = new Array(3, 2, 7, 6, 5, 4, 3, 2, 7, 6, 5, 4, 3, 2);
	var cuenta = banco + oficina + digitos + num_cuenta;
	if (cuenta.length != 20) {
		return false;
	}
	var campos1 = banco + oficina;
	var campos2 = oficina + num_cuenta;
	var digitos1 = parseInt(campos1, 10);
	var digitos2 = parseInt(campos2, 10);
	var suma1 = 0;
	var suma2 = 0;
	for (var i = 0; i < 8; i++) {
		var digito = Math.floor(((digitos1 / Math.pow(10.0, (7 - i) * 1.0)))) % 10;
		suma1 += pesos1[i] * digito;
	}
	for (var i = 0; i < 14; i++) {
		var digito = Math.floor(((digitos2 / Math.pow(10.0, (13 - i) * 1.0)))) % 10;
		suma2 += pesos2[i] * digito;
	}
	var digito1 = (11 - (suma1 % 11));
	var digito2 = (11 - (suma2 % 11));
	if (digito1 >= 10 || digito1 < 1)  digito1 = digito1 % 10;
	if (digito2 >= 10 || digito2 < 1){
		digito2 = digito2 % 10;
	}
	//alert(digito1 +'-'+ digito2);
	var cuentaValidada = banco + oficina + digito1 + digito2 + num_cuenta;
	return cuenta == cuentaValidada;
}; 

//cuenta correcta 01750496710071495316
verificar('0175','0496','71','0071495316');

//cuenta errada 01750214450025987415
verificar('0175','0214','45','0025987415');